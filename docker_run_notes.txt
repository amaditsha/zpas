#RUN IMAGE

docker run --net=host --restart unless-stopped -d -p 8080:8080 -v /opt/eSolutions/zpas:/opt/eSolutions/zpas --name zpas zpas

# EXEC INTO CONTAINER

docker exec -it zpas-app-test /bin/sh

# STOP CONTAINER

docker stop <container-name>

# LOGS

docker logs <container-name>

docker logs <container-name> -f