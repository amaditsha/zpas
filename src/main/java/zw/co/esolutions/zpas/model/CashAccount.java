package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.enums.AccountLevelCode;
import zw.co.esolutions.zpas.enums.BalanceTypeCode;
import zw.co.esolutions.zpas.enums.CashAccountTypeCode;

import javax.persistence.*;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Audited
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class CashAccount extends Account {

    @Enumerated(value = EnumType.STRING)
    protected CashAccountTypeCode cashAccountType;

    @Enumerated(value = EnumType.STRING)
    protected AccountLevelCode level;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "settlementCurrency_codeName")),
            @AttributeOverride(name = "name", column = @Column(name = "settlementCurrency_name"))
    })
    protected CurrencyCode settlementCurrency;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name = "cash_account_entry",
            joinColumns = @JoinColumn(name = "cash_account_id"),
            inverseJoinColumns = @JoinColumn(name = "cash_entry_id")
    )
    @NotAudited
    protected List<CashEntry> cashEntry;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "cash_account_balance",
            joinColumns = @JoinColumn(name = "cash_account_id"),
            inverseJoinColumns = @JoinColumn(name = "cash_balance_id")
    )
    @NotAudited
    protected List<CashBalance> cashBalance;

    @ManyToOne(targetEntity = PaymentPartyRole.class, fetch = FetchType.LAZY)
    @NotAudited
    protected PaymentPartyRole paymentPartyRole;

    @OneToOne(fetch = FetchType.LAZY)
    @NotAudited
    protected StandingOrder relatedCreditStandingOrder;

    @OneToOne(fetch = FetchType.LAZY)
    @NotAudited
    protected StandingOrder relatedDebitStandingOrder;

    @JsonIgnore
    @ManyToMany(mappedBy = "cashAccount", fetch = FetchType.LAZY)
    @NotAudited
    protected List<CashAccountContract> cashAccountContract;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "cash_account_charges",
            joinColumns = @JoinColumn(name = "cash_account_id"),
            inverseJoinColumns = @JoinColumn(name = "charges_id")
    )
    @NotAudited
    protected List<Charges> charges;

    @ManyToOne(targetEntity = Tax.class, fetch = FetchType.LAZY)
    @NotAudited
    protected Tax tax;

    @OneToOne(mappedBy = "settlementAccount", fetch = FetchType.LAZY)
    @NotAudited
    protected CashSettlement relatedSettlementInstruction;

    @ManyToOne(targetEntity = CashSettlementInstructionPartyRole.class, fetch = FetchType.LAZY)
    @NotAudited
    protected CashSettlementInstructionPartyRole cashSettlementPartyRole;

    @ManyToOne(targetEntity = InvoiceFinancingPartyRole.class, fetch = FetchType.LAZY)
    @NotAudited
    protected InvoiceFinancingPartyRole relatedInvoiceFinancingPartyRole;

    @JsonIgnore
    @OneToMany(mappedBy = "reportedCashAccount", fetch = FetchType.LAZY)
    @NotAudited
    protected List<AccountReportedMovement> reportedMovements;

    @ManyToOne(targetEntity = CashAccountContract.class, fetch = FetchType.LAZY)
    @NotAudited
    protected CashAccountContract closedAccountContract;

    @OneToOne(fetch = FetchType.LAZY)
    @NotAudited
    protected CashStandingOrder cashStandingOrder;

    @OneToOne(targetEntity = CashAccountService.class, mappedBy = "cashAccount", fetch = FetchType.LAZY)
    @NotAudited
    protected CashAccountService cashAccountService;

    @JsonIgnore
    @OneToMany(targetEntity = Payment.class, mappedBy = "account", fetch = FetchType.LAZY)
    @NotAudited
    protected List<Payment> payment;

    @ManyToOne(targetEntity = Commission.class, fetch = FetchType.LAZY)
    @NotAudited
    protected Commission commission;

    @Column
    protected String uploadAccountsId;

    public String getAccountNumber() {
        return identification.getNumber();
    }

    public String getAccountName() {
        return identification.getName();
    }

    @Override
    public String getInstanceName() {
        return "CASH ACCOUNT: " + id;
    }

    public Optional<CashBalance> getCurrentCashBalance() {
        return Optional.ofNullable(cashBalance).map(cashBalances -> {
            return cashBalances
                    .stream()
                    .filter(cb -> cb.getType() == BalanceTypeCode.InterimAvailable)
                    .sorted(Comparator.comparing(CashBalance::getDateCreated).reversed())
                    .findFirst();
        }).orElse(Optional.empty());
    }

    public String getCurrentCashBalanceString() {
        return getCurrentCashBalance().map(cb -> {
            return "Balance: " + cb.getAmount().toString();
        }).orElse("Balance: ----");
    }

    @Override
    public String toString() {
        return "CashAccount{" +
                //"cashAccountType=" + cashAccountType +
                ", level=" + level +
                //", settlementCurrency=" + settlementCurrency +
                ", id='" + id + '\'' +
                ", virtualNumber='" + virtualNumber + '\'' +
                ", virtualCode='" + virtualCode + '\'' +
                //", baseCurrency=" + baseCurrency +
                //", reportingCurrency=" + reportingCurrency +
                ", purpose='" + purpose + '\'' +
                //", closingDate=" + closingDate +
                //", liveDate=" + liveDate +
                //", reportedPeriod=" + reportedPeriod +
                //", openingDate=" + openingDate +
                //", identification=" + identification +
                ", status=" + status +
                //", type=" + type +
                //", partyRole=" + partyRole +
                //", dateCreated=" + dateCreated +
                //", lastUpdated=" + lastUpdated +
                //", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                '}';
    }
}
