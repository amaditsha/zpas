package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import zw.co.esolutions.zpas.enums.DebitCreditCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

/**
 * Created by alfred on 28 Jan 2019
 */

@Entity
@Data
@Table(name = "payment_execution")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "paymentexecutionanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public abstract class PaymentExecution {
    @Id
    protected String id;

    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @Enumerated(EnumType.STRING)
    protected DebitCreditCode creditDebitIndicator;

    protected OffsetDateTime creationDate;

    protected OffsetDateTime acceptanceDateTime;

    @JsonIgnore
    @ManyToMany(targetEntity = Payment.class, cascade = { PERSIST, MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "payment_execution_payment",
            joinColumns = @JoinColumn(name = "payment_execution_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "payment_id", referencedColumnName = "id"))
    protected List<Payment> payment;

    @JsonIgnore
    @OneToMany(targetEntity = PaymentProcessing.class, fetch = FetchType.LAZY)
    protected List<PaymentProcessing> processingInstructions;

    protected OffsetDateTime requestedExecutionDate;

    @OneToOne(targetEntity = PaymentInvestigationCase.class, fetch = FetchType.LAZY)
    protected PaymentInvestigationCase relatedInvestigationCase;

    @ManyToOne(targetEntity = PaymentInvestigationCaseResolution.class, fetch = FetchType.LAZY)
    protected PaymentInvestigationCaseResolution relatedInvestigationCaseResolution;

    @ManyToOne(targetEntity = PaymentInstruction.class, fetch = FetchType.LAZY)
    protected PaymentInstruction next;

    @JsonIgnore
    @OneToMany(targetEntity = CurrencyExchange.class, mappedBy = "paymentExecution", fetch = FetchType.LAZY)
    protected List<CurrencyExchange> currencyExchange;

    @Override
    public String toString() {
        return "PaymentExecution{" +
                "id='" + id + '\'' +
                ", entityStatus=" + entityStatus +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityVersion=" + entityVersion +
                ", creditDebitIndicator=" + creditDebitIndicator +
                ", creationDate=" + creationDate +
                ", acceptanceDateTime=" + acceptanceDateTime +
                ", requestedExecutionDate=" + requestedExecutionDate +
                '}';
    }
}
