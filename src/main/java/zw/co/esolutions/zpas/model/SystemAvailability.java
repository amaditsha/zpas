package zw.co.esolutions.zpas.model;

import lombok.Data;


import javax.persistence.Embeddable;
import java.time.OffsetDateTime;

/**
 * Created by jnkomo on 25 Jan 2019
 */
@Data
@Embeddable
public class SystemAvailability {
    protected OffsetDateTime date;
}
