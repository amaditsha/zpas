package zw.co.esolutions.zpas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;


import javax.persistence.*;

/**
 * Created by alfred on 29 Jan 2019
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AnalyzerDef(name = "payment_identification_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class PaymentIdentification extends TradeIdentification {

    protected String executionIdentification;

    protected String endToEndIdentification;

    protected String instructionIdentification;

    protected String transactionIdentification;

    protected String clearingSystemReference;

    protected String creditorReference;

    protected String channelIdentification;

    @ManyToOne(targetEntity = Payment.class, fetch = FetchType.LAZY)
    protected Payment payment;

    @Override
    public String toString() {
        return "PaymentIdentification{" +
                "executionIdentification='" + executionIdentification + '\'' +
                ", endToEndIdentification='" + endToEndIdentification + '\'' +
                ", instructionIdentification='" + instructionIdentification + '\'' +
                ", transactionIdentification='" + transactionIdentification + '\'' +
                ", clearingSystemReference='" + clearingSystemReference + '\'' +
                ", creditorReference='" + creditorReference + '\'' +
                ", id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                ", counterpartyReference='" + counterpartyReference + '\'' +
                ", identification='" + identification + '\'' +
                ", commonIdentification='" + commonIdentification + '\'' +
                ", matchingReference='" + matchingReference + '\'' +
                ", uniqueTradeIdentifier='" + uniqueTradeIdentifier + '\'' +
                '}';
    }
}
