package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.enums.CashClearingSystemCode;
import zw.co.esolutions.zpas.enums.CashSettlementSystemCode;
import zw.co.esolutions.zpas.enums.CashSystemTypeCode;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 30 Jan 2019
 */
@Data
@Entity
//@Indexed
@AnalyzerDef(name = "cashclearingsystemanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class CashClearingSystem extends ClearingSystem {

    @Enumerated(EnumType.STRING)
    protected CashClearingSystemCode identification;

    @JsonIgnore
    @OneToMany(mappedBy = "cashClearingSystem", fetch = FetchType.LAZY)
    protected List<TransactionAdministrator> transactionAdministrator;

    @ManyToOne(targetEntity = SettlementInstructionSystemRole.class)
    protected SettlementInstructionSystemRole systemRole;

    @Enumerated(EnumType.STRING)
    protected CashSystemTypeCode type;

    @Enumerated(EnumType.STRING)
    protected CashSettlementSystemCode cashSettlementSystem;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "CASH_CLEARING_SYSTEM : " + id;
    }

    @Override
    public String toString() {
        return "CashClearingSystem{}";
    }
}
