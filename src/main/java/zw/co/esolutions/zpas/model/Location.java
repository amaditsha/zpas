package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jnkomo on 23 Jan 2019
 */
@Audited
@Data
@Table(name = "location")
@EqualsAndHashCode(exclude = {"registeredOrganisation", "operatingOrganisation"})
@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "locationanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class Location implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @OneToOne(targetEntity = Person.class, mappedBy = "placeOfBirth", fetch = FetchType.LAZY)
    protected Person nativePerson;

    @ManyToOne(targetEntity = Party.class, fetch = FetchType.LAZY)
    protected Party domiciledParty;

    @ManyToOne(targetEntity = Organisation.class, fetch = FetchType.LAZY)
    protected Organisation operatingOrganisation;

    @OneToOne(mappedBy = "placeOfRegistration", fetch = FetchType.LAZY)
    protected Organisation registeredOrganisation;

    @JsonIgnore
    @OneToMany(targetEntity = PostalAddress.class, fetch = FetchType.LAZY, mappedBy = "location", cascade = CascadeType.PERSIST)
    protected List<PostalAddress> address;

    @JsonIgnore
    @OneToMany(mappedBy = "placeOfIssue", fetch = FetchType.LAZY)
    @NotAudited
    protected List<Document> issuedDocument;

    @JsonIgnore
    @OneToMany(mappedBy = "identifiedLocation", fetch = FetchType.LAZY)
    @NotAudited
    protected List<GenericIdentification> identification;

    @OneToOne(mappedBy = "domicile", fetch = FetchType.LAZY)
    protected Party taxableParty;

    // protected Transport relatedTransport;
    // protected List<UTCOffset> timeZone;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "Location{}";
    }
}
