package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;



import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Audited
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AnalyzerDef(name = "mandatecontractanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class Mandate extends Contract {
    @NotAudited
    @JsonIgnore
    @OneToMany(mappedBy = "mandate", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    protected List<SignatureCondition> signatureConditions;

    protected String mandateIdentification;

    @OneToOne(fetch = FetchType.LAZY)
    protected Mandate originalMandate;

    @OneToOne(fetch = FetchType.LAZY)
    protected Mandate amendment;

    @NotAudited
    @JsonIgnore
    @ManyToMany(mappedBy = "mandate", fetch = FetchType.LAZY, targetEntity = MandatePartyRole.class)
    protected List<MandatePartyRole> mandatePartyRole;

    @NotAudited
    @JsonIgnore
    @OneToMany(mappedBy = "mandate", targetEntity = MandateStatus.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    protected List<MandateStatus> mandateStatus;

    @NotAudited
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = AccountContract.class)
    protected AccountContract accountContract;

//    protected Authentication authentication;

    protected String trackingDays;

    protected Boolean trackingIndicator;

    protected BigDecimal rate;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "MANDATE " + getId();
    }

    @Override
    public String toString() {
        return "Mandate{}";
    }
}
