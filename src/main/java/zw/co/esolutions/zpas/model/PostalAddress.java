package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import zw.co.esolutions.zpas.enums.AddressTypeCode;

import javax.persistence.*;

//import zw.co.esolutions.zpas.codeset.AddressTypeCode;

/**
 * Created by jnkomo on 25 Jan 2019
 */
@Audited
@EqualsAndHashCode(callSuper = true, exclude = {"location"})
@Entity
@Data
public class PostalAddress extends ContactPoint {

//    @Embedded
//    @AttributeOverrides(value = {
//            @AttributeOverride(name = "codeName", column = @Column(name = "address_type_code_name")),
//            @AttributeOverride(name = "name", column = @Column(name = "address_type_name"))
//    })
    @Enumerated(EnumType.STRING)
    protected AddressTypeCode addressType;

    protected String streetName;

    protected String streetBuildingIdentification;

    protected String postCodeIdentification;

    protected String townName;

    protected String state;

    protected String buildingName;

    protected String floor;

    protected String districtName;

    protected String regionIdentification;

    protected String countyIdentification;

    protected String postOfficeBox;

    protected String province;

    protected String department;

    protected String subDepartment;

    @ManyToOne(targetEntity = Location.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    protected Location location;

    @ManyToOne(targetEntity = Country.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @NotAudited
    protected Country country;

    protected String suiteIdentification;

    protected String buildingIdentification;

    protected String mailDeliverySubLocation;

    protected String block;

    protected String lot;

    protected String districtSubDivisionIdentification;

    @Override
    public String toString() {
        return "PostalAddress{}";
    }
}
