package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import zw.co.esolutions.zpas.enums.MemberTypeCode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class SystemMemberRole extends SystemPartyRole {

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "system_member_role_cash_balance",
            joinColumns = @JoinColumn(name = "system_member_role_id"),
            inverseJoinColumns = @JoinColumn(name = "cash_balance_id")
    )
    protected List<CashBalance> cashBalance;

    @Enumerated(EnumType.STRING)
    protected MemberTypeCode type;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY)
    protected List<SystemStatus> memberStatus;
    //protected RiskManagementLimit limit;

    @OneToOne(targetEntity = Account.class, fetch = FetchType.LAZY)
    protected Account account;

    @Override
    public String toString() {
        return "SystemMemberRole{}";
    }
}
