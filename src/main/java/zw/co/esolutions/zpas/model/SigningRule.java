package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Table(name = "signing_rule", indexes = {
        @Index(name = "signing_rule_entitystatus", columnList = "entityStatus"),
        @Index(name = "signing_rule_datecreated", columnList = "dateCreated"),
        @Index(name = "signing_rule_lastupdated", columnList = "lastUpdated"),
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "signingrule_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class SigningRule implements Auditable {
    @Id
    @Column(length = 50)
    protected String id;
    @Column
    protected Integer numberOfApprovers;
    @Column
    protected Long minAmount;
    @Column
    protected Long maxAmount;
    @Column(length = 175)
    protected String activityName;
    @Column(length = 50)
    protected String entityType;
    @Column(length = 50)
    protected String entityId;
    @Column
    protected OffsetDateTime startDate;

    @Column
    protected OffsetDateTime endDate;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @JsonIgnore
    @OneToMany(targetEntity = RuleItem.class, mappedBy = "signingRule", fetch = FetchType.EAGER)
    protected List<RuleItem> ruleItems;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", getLastUpdated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("dateCreated", getDateCreated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", getEntityStatus().name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "SIGNINGRULE: " + id;
    }

    @PrePersist
    public void initSigningRule() {
        setId(GenerateKey.generateEntityId());
        setEntityStatus(EntityStatus.PENDING_APPROVAL);
        setDateCreated(OffsetDateTime.now());
    }

    @Override
    public String toString() {
        return "SigningRule{}";
    }
}
