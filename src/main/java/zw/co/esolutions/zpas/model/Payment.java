package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.model.payments.ExtraDetail;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.SpecialPayee;
import zw.co.esolutions.zpas.utilities.util.StringUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.*;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

/**
 * Created by alfred on 23 Jan 2019
 */
@Slf4j
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AnalyzerDef(name = "payment_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class Payment extends ObligationFulfilment {

    @JsonIgnore
    @ManyToMany(targetEntity = PaymentObligation.class, cascade = {CascadeType.PERSIST}, mappedBy = "paymentOffset", fetch = FetchType.LAZY)
    protected List<PaymentObligation> paymentObligation;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "currency_of_transfer_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "currency_of_transfer_code"))
    })
    protected CurrencyCode currencyOfTransfer;

    @JsonIgnore
    @ManyToMany(targetEntity = CreditInstrument.class, fetch = FetchType.LAZY)
    @JoinTable(name = "payment_credit_instrument",
            joinColumns = {
                    @JoinColumn(name = "payment_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "credit_instrument_id")})
    protected List<CreditInstrument> creditMethod;

    @Enumerated(EnumType.STRING)
    protected PaymentTypeCode type;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "instructed_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "instructed_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "instructed_amount_currency_code"))
    })
    protected CurrencyAndAmount instructedAmount;

    @Enumerated(EnumType.STRING)
    protected PriorityCode priority;

    protected OffsetDateTime valueDate;

    @JsonIgnore
    @OneToMany(targetEntity = PaymentStatus.class, cascade = CascadeType.PERSIST, mappedBy = "payment", fetch = FetchType.LAZY)
    protected List<PaymentStatus> paymentStatus;

    @JsonIgnore
    @ManyToMany(targetEntity = PaymentPartyRole.class, cascade = {CascadeType.PERSIST}, mappedBy = "payment", fetch = FetchType.LAZY)
    protected List<PaymentPartyRole> partyRole;

    @JsonIgnore
    @OneToMany(targetEntity = Tax.class, mappedBy = "relatedPaymentSettlement", fetch = FetchType.LAZY)
    protected List<Tax> taxOnPayment;

    @JsonIgnore
    @ManyToMany(targetEntity = PaymentExecution.class, mappedBy = "payment", fetch = FetchType.LAZY)
    protected List<PaymentExecution> paymentExecution;

    protected OffsetDateTime poolingAdjustmentDate;

    protected BigDecimal equivalentAmount;

    @JsonIgnore
    @OneToMany(targetEntity = CurrencyExchange.class, mappedBy = "relatedPayment", fetch = FetchType.LAZY)
    protected List<CurrencyExchange> currencyExchange;

    @OneToOne(targetEntity = DebitAuthorisation.class, fetch = FetchType.LAZY)
    protected DebitAuthorisation relatedDebitAuthorisation;

    @ManyToOne(targetEntity = PaymentInvestigationCaseResolution.class, fetch = FetchType.LAZY)
    protected PaymentInvestigationCaseResolution relatedInvestigationCaseResolution;

    @Enumerated(EnumType.STRING)
    protected InstructionCode instructionForCreditorAgent;

    @Enumerated(EnumType.STRING)
    protected InstructionCode instructionForDebtorAgent;

    @JsonIgnore
    @OneToMany(targetEntity = PaymentIdentification.class, cascade = {CascadeType.PERSIST}, mappedBy = "payment", fetch = FetchType.LAZY)
    protected List<PaymentIdentification> paymentRelatedIdentifications;

    @ManyToOne(targetEntity = PaymentInvestigationCase.class, fetch = FetchType.LAZY, cascade = {MERGE, PERSIST})
    protected PaymentInvestigationCase relatedInvestigationCase;

    @OneToOne(targetEntity = SettlementTimeRequest.class, fetch = FetchType.LAZY)
    protected SettlementTimeRequest settlementTimeRequest;

    @OneToOne(targetEntity = PaymentDetails.class, mappedBy = "payment", cascade = {CascadeType.ALL})
    protected PaymentDetails paymentDetails;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amount_currency_code"))
    })
    protected CurrencyAndAmount amount;
//    protected List<CommercialTradeSettlement> tradeSettlement;

    protected String standardSettlementInstructions;

    @ManyToOne(targetEntity = Payment.class, fetch = FetchType.LAZY)
    protected Payment originalPayment;

    @JsonIgnore
    @OneToMany(targetEntity = Payment.class, mappedBy = "originalPayment", fetch = FetchType.LAZY)
    protected List<Payment> returnPayment;

//    protected SecuritiesSettlement relatedSecuritiesSettlement;

    @JsonIgnore
    @ManyToMany(targetEntity = Invoice.class, fetch = FetchType.LAZY)
    @JoinTable(name = "payment_invoice",
            joinColumns = @JoinColumn(name = "payment_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "invoice_id", referencedColumnName = "id"))
    protected List<Invoice> invoiceReconciliation;

    @Enumerated(EnumType.STRING)
    protected PaymentInstrumentCode paymentInstrument;

    @ManyToOne(targetEntity = CashAccount.class, fetch = FetchType.EAGER)
    protected CashAccount account;

    @ManyToOne(targetEntity = Adjustment.class, fetch = FetchType.LAZY)
    protected Adjustment adjustments;
//    protected RegisteredContract contractRegistration;

    //Borrowed from payment obligation
//    @Enumerated(EnumType.STRING)
//    protected PaymentPurposeCode purpose;

    @Enumerated(EnumType.STRING)
    protected ProprietaryPurposeCode purpose;

    @Enumerated(EnumType.STRING)
    protected SpecialPayee specialPayee;

    protected String contributionScheduleId;

    /**
     * Use this field to timeout verification process and forward the payment to pending approval
     */
    protected OffsetDateTime verificationStartTime;

    @JsonIgnore
    @ElementCollection
    protected List<String> unstructuredRemittanceInformation;

    @JsonIgnore
    @OneToMany(targetEntity = ExtraDetail.class, mappedBy = "payment", fetch = FetchType.LAZY)
    protected List<ExtraDetail> extraDetails;

    protected String verifiedBeneficiaryName;
    protected String verifiedBankName;
    protected String verifiedAccountNumber;
    protected String verifiedAccountName;
    protected String verificationNarrative;
    protected Boolean verifiedSuccessfully;

    protected boolean verifyAccountInfo;
    protected boolean stopWhenFailedVerification;
    protected boolean batchBookingEnabled;

    public String getVerifiedBeneficiaryName() {
        return Optional.ofNullable(verifiedBeneficiaryName).orElseGet(() -> {
            return getBeneficiaryName();
        });
    }

    public String getVerifiedBankName() {
        return Optional.ofNullable(verifiedBankName).orElseGet(() -> {
            return getBeneficiaryBank();
        });
    }

    public String getVerifiedAccountNumber() {
        return Optional.ofNullable(verifiedAccountNumber).orElseGet(() -> {
            return getBeneficiaryAccount();
        });
    }

    public String getVerifiedAccountName() {
        return Optional.ofNullable(verifiedAccountName).orElseGet(() -> {
            return getBeneficiaryName();
        });
    }

    public String getVerificationNarrative() {
        return Optional.ofNullable(verificationNarrative).orElseGet(() -> {
            if (getVerifiedSuccessfully()) {
                return "Account information was verified successfully.";
            } else {
                return "Account Information did not match.";
            }
        });
    }

    public Boolean getVerifiedSuccessfully() {
        return Optional.ofNullable(verifiedSuccessfully).orElse(false);
    }

    public Boolean canSendProofOfPayment() {
        return Optional.ofNullable(entityStatus).map(es -> {
            return es.equals(EntityStatus.ACTIVE) || es.equals(EntityStatus.SUCCESSFUL)
                    || es.equals(EntityStatus.PROCESSED);
        }).orElse(false);
    }

    public Boolean canRejectPayment() {
        return !getVerifiedSuccessfully() &&
                (entityStatus == EntityStatus.PENDING || entityStatus == EntityStatus.PENDING_APPROVAL ||
                        entityStatus == EntityStatus.DRAFT);
    }

    public boolean verificationNeeded() {
        if (!verifyAccountInfo && this instanceof BulkPayment) {
            return false;
        }
        if (this instanceof IndividualPayment) {
            return false;
        }
        switch (getEntityStatus()) {
            case PENDING_VERIFICATION:
                return true;
            case FAILED:
            case PENDING_APPROVAL:
            default:
                return false;
        }
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObligationFulfilment that = (ObligationFulfilment) o;
        return id.equals(that.id);
    }

    public String getBeneficiaryName() {
        return getPartyRole().stream().filter(r -> r instanceof CreditorRole)
                .map(r -> (CreditorRole) r).findFirst().map(creditorRole -> {
                    if (creditorRole.isHasCustomParty()) {
                        return creditorRole.getCustomParty().getName();
                    } else {
                        return creditorRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                            return ((Party) rolePlayer).getName();
                        }).orElse("");
                    }
                }).orElse("");
    }

    public String getDebtorAccount() {
        return Optional.ofNullable(getAccount())
                .map(cashAccount -> {
                    return Optional.ofNullable(cashAccount.getIdentification()).map(AccountIdentification::getNumber).orElse("");
                }).orElse("");
    }

    public String getBeneficiaryAccount() {
        return getPartyRole().stream().filter(r -> r instanceof CreditorRole)
                .map(r -> (CreditorRole) r).findFirst().map(creditorRole -> {
                    if (creditorRole.getCustomParty() != null) {
                        return creditorRole.getCustomParty().getAccountNumber();
                    } else {
                        return "";
                    }
                }).orElse("");
    }

    public String getBeneficiaryBank() {
        return getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof CreditorAgentRole)
                .findFirst().map(creditorAgentRole -> {
                    return creditorAgentRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        if (rolePlayer instanceof FinancialInstitution) {
                            FinancialInstitution financialInstitution = (FinancialInstitution) rolePlayer;
                            return financialInstitution.getShortName();
                        } else {
                            return "";
                        }
                    }).orElse("");
                }).orElse("");
    }

    public Optional<Party> getDebtor() {
        return getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof DebtorRole)
                .findFirst().map(debtorRole -> {
                    return debtorRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        Party party = (Party) rolePlayer;
                        return Optional.ofNullable(party);
                    }).orElse(Optional.empty());
                }).orElse(Optional.empty());
    }

    public String getOrganisationSsr(){
        return getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof DebtorRole)
                .findFirst().map(paymentPartyRole -> {
                    return paymentPartyRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        if (rolePlayer instanceof NonFinancialInstitution){
                            NonFinancialInstitution nonFinancialInstitution = (NonFinancialInstitution) rolePlayer;
                            return  nonFinancialInstitution.getSsr();
                        }else{
                            return "";
                        }
                    }).orElse("");
                }).orElse("");
    }

    public String getOrganisationBpn(){
        return getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof DebtorRole)
                .findFirst().map(paymentPartyRole -> {
                    return paymentPartyRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        if (rolePlayer instanceof NonFinancialInstitution){
                            NonFinancialInstitution nonFinancialInstitution = (NonFinancialInstitution) rolePlayer;
                            return  nonFinancialInstitution.getBusinessPartnerNumber();
                        }else{
                            return "";
                        }
                    }).orElse("");
                }).orElse("");
    }

    public Optional<FinancialInstitution> getDebtorBank() {
        return getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof DebtorAgentRole)
                .findFirst().map(debtorAgentRole -> {
                    return debtorAgentRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        if (rolePlayer instanceof FinancialInstitution) {
                            FinancialInstitution financialInstitution = (FinancialInstitution) rolePlayer;
                            return Optional.ofNullable(financialInstitution);
                        } else {
                            Optional<FinancialInstitution> result = Optional.empty();
                            return result;
                        }
                    }).orElse(Optional.empty());
                }).orElse(Optional.empty());
    }

    public Optional<FinancialInstitution> getCreditorBank() {
        return getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof CreditorAgentRole)
                .findFirst().map(creditorAgentRole -> {
                    return creditorAgentRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        if (rolePlayer instanceof FinancialInstitution) {
                            FinancialInstitution financialInstitution = (FinancialInstitution) rolePlayer;
                            return Optional.ofNullable(financialInstitution);
                        } else {
                            Optional<FinancialInstitution> result = Optional.empty();
                            return result;
                        }
                    }).orElse(Optional.empty());
                }).orElse(Optional.empty());
    }

    public void addPaymentStatus(PaymentStatus status) {
        if (this.paymentStatus == null)
            this.paymentStatus = new ArrayList<>();
        this.paymentStatus.add(status);
    }

    public void addExtraDetail(ExtraDetail extraDetail) {
        if (this.extraDetails == null)
            this.extraDetails = new ArrayList<>();
        this.extraDetails.add(extraDetail);
    }

    public String getBeneficiaryAccountAndBank() {
        final String beneficiaryAccount = getBeneficiaryAccount();
        final String beneficiaryBank = getBeneficiaryBank();
        if (beneficiaryBank.length() > 0) {
            return String.format("%s at %s", beneficiaryAccount, beneficiaryBank);
        } else {
            return beneficiaryAccount;
        }
    }

    public String getEndToEndId() {
        return getPaymentRelatedIdentifications().stream()
                .findFirst().map(paymentIdentification -> {
                    return paymentIdentification.getEndToEndIdentification();
                }).orElse("");
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Optional<PaymentStatus> getLatestPaymentStatus() {
        return this.getPaymentStatus().stream()
                .max(Comparator.comparing(Status::getDateCreated));
    }

    public String getLatestPaymentStatusString() {
        return this.getPaymentStatus().stream()
                .max(Comparator.comparing(Status::getDateCreated))
                .map(paymentStatus -> {
                    return StringUtil.formatCamelCaseToSpacedString(paymentStatus.getStatus().name());
                }).orElseGet(() -> {
                    return StringUtil.formatCamelCaseToSpacedString(Payment.this.entityStatus.name());
                });
    }

    public String getUnformattedLatestPaymentStatusString() {
        return this.getPaymentStatus().stream()
                .max(Comparator.comparing(Status::getDateCreated))
                .map(paymentStatus -> {
                    return paymentStatus.getStatus().name();
                }).orElseGet(() -> {
                    return Payment.this.entityStatus.name();
                });
    }
}
