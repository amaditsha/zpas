package zw.co.esolutions.zpas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.*;

/**
 * Created by alfred on 28 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AnalyzerDef(name = "corporate_action_fees_and_charges_entity",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class CorporateActionFeesAndCharges extends Charges {
//    protected CorporateActionEvent corporateAction;
//    protected RateAndAmount solicitationFee;
//    protected RateAndAmount earlySolicitationFeeRate;

    @OneToOne(targetEntity = Commission.class, mappedBy = "corporateActionFeesAndCharges", fetch = FetchType.LAZY)
    protected Commission commission;

    @Override
    public String toString() {
        return "CorporateActionFeesAndCharges{}";
    }
}
