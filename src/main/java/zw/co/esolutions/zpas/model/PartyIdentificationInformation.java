package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;


import zw.co.esolutions.zpas.enums.PartyTypeCode;
import zw.co.esolutions.zpas.enums.TypeOfIdentificationCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tanatsa on 23 Jan 2019
 */
@Audited
@Entity
//Indexed
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "party_identification_information")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "partyidentificationinformationanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class PartyIdentificationInformation implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @OneToOne(mappedBy = "relatedPartyIdentification", targetEntity = GenericIdentification.class, fetch = FetchType.LAZY)
    @NotAudited
    protected GenericIdentification otherIdentification;

    @ManyToOne(targetEntity = Party.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE} , fetch = FetchType.LAZY)
    protected Party identifiedParty;

    protected String taxIdentificationNumber;

    protected String nationalRegistrationNumber;

    @Enumerated(EnumType.STRING)
    protected TypeOfIdentificationCode typeOfIdentification;

    protected String declaration;

    @Enumerated(EnumType.STRING)
    protected PartyTypeCode partyType;

    @JsonIgnore
    @OneToMany(mappedBy = "partyIdentification", fetch = FetchType.EAGER)
    protected List<PartyName> partyName;

    @ManyToOne(targetEntity = DateTimePeriod.class, fetch = FetchType.LAZY)
    @NotAudited
    protected DateTimePeriod validityPeriod;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        attributesMap.put("dateCreated", getDateCreated().format(formatter) + "");
        attributesMap.put("lastUpdated", getLastUpdated().format(formatter) + "");
        attributesMap.put("entityStatus", getEntityStatus() + "");
        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "PartyIdentificationInformation{" +
                "id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                ", taxIdentificationNumber='" + taxIdentificationNumber + '\'' +
                ", nationalRegistrationNumber='" + nationalRegistrationNumber + '\'' +
                ", typeOfIdentification=" + typeOfIdentification +
                ", declaration='" + declaration + '\'' +
                ", partyType=" + partyType +
                '}';
    }
}
