package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;




import zw.co.esolutions.zpas.enums.DebitCreditCode;
import zw.co.esolutions.zpas.enums.EntryCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Table(name = "entry")
public abstract class Entry implements Auditable {
    @Id
    protected String id;

    @Enumerated(EnumType.STRING)
    protected DebitCreditCode creditDebitIndicator;

    protected OffsetDateTime entryDate;

    protected String identification;

    protected String accountOwnerTransactionIdentification;

    protected String accountServicerTransactionIdentification;

    protected Boolean reversalIndicator;

    protected OffsetDateTime valueDate;

    protected Boolean commissionWaiverIndicator;

    @Enumerated(EnumType.STRING)
    protected EntryCode entryType;

    @OneToOne(cascade = {CascadeType.PERSIST}, mappedBy = "relatedEntry", targetEntity = BankTransaction.class)
    protected BankTransaction bankTransactionCode;


    @JsonIgnore
    @OneToMany(mappedBy = "entry", fetch = FetchType.LAZY)
    protected List<Role> role;

    @JsonIgnore
    @ManyToMany(mappedBy = "entry", fetch = FetchType.LAZY)
    protected List<Account> account;

    @JsonIgnore
    @ManyToMany(mappedBy = "balanceEntry", fetch = FetchType.LAZY)
    protected List<Balance> balance;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "ENTRY: " + id;
    }

    @Override
    public String toString() {
        return "Entry{}";
    }
}
