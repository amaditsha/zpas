package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;


import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jnkomo on 23 Jan 2019
 */
@Audited
@Entity
@Data
@Table(name = "contact_point")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "contactpointanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class ContactPoint implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @ManyToOne(targetEntity = GenericIdentification.class, fetch = FetchType.LAZY)
    @NotAudited
    protected GenericIdentification identification;

    @ManyToOne(targetEntity = Party.class, fetch = FetchType.LAZY)
    protected Party relatedParty;

    protected String bICAddress;

    @JsonIgnore
    @ManyToMany(targetEntity = Document.class, mappedBy = "placeOfStorage", fetch = FetchType.LAZY)
    @NotAudited
    protected List<Document> storedDocument;

    @ManyToOne(targetEntity = PaymentObligation.class, fetch = FetchType.LAZY)
    @NotAudited
    protected PaymentObligation remittanceRelatedPayment;

    @OneToOne(fetch = FetchType.LAZY)
    protected ContactPoint mainContact;

    @OneToOne(mappedBy = "mainContact", fetch = FetchType.LAZY)
    protected ContactPoint returnAddress;

    @ManyToOne(targetEntity = PaymentProcessing.class, fetch = FetchType.LAZY)
    @NotAudited
    protected PaymentProcessing relatedPayment;

    protected Boolean temporaryIndicator;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        attributesMap.put("dateCreated", getDateCreated().format(formatter) + "");
        attributesMap.put("lastUpdated", getLastUpdated().format(formatter) + "");
        attributesMap.put("entityStatus", getEntityStatus() + "");
        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "ContactPoint{}";
    }
}
