package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.time.OffsetDateTime;
import java.util.Objects;

@Data
@Entity
@AnalyzerDef(name = "direct_debit_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class DirectDebit extends IndividualPayment {

    protected String registrationIdentification;

    @OneToOne(targetEntity = DirectDebitMandate.class, mappedBy = "relatedDirectDebit", fetch = FetchType.LAZY)
    protected DirectDebitMandate directDebitMandate;

    protected String preNotificationIdentification;

    protected OffsetDateTime preNotificationDate;

    @Override
    public String toString() {
        return "DirectDebit{}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObligationFulfilment that = (ObligationFulfilment) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
