package zw.co.esolutions.zpas.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.*;

/**
 * Created by tanatsa on 23 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//Indexed
@AnalyzerDef(name = "powerofattorneyanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class PowerOfAttorney extends Mandate {
    @OneToOne(fetch = FetchType.LAZY)
    protected Party authorisedParty;

    @OneToOne(mappedBy = "powerOfAttorney", fetch = FetchType.LAZY)
    protected PowerOfAttorneyRequirements powerOfAttorneyRequirements;

    @Override
    public String toString() {
        return "PowerOfAttorney{}";
    }
}
