package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.Indexed;

import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Indexed
public class CashEntry extends Entry {

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amountCurrency_codeName")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amountCurrency_name"))
    })
    protected CurrencyAndAmount amount;

    protected Boolean chargesIncluded;

    @JsonIgnore
    @ManyToMany(mappedBy = "cashEntry")
    protected List<CashAccount> cashAccount;

    @JsonIgnore
    @ManyToMany(mappedBy = "cashBalanceEntry")
    protected List<CashBalance> cashBalance;

    @ManyToOne(targetEntity = CurrencyExchange.class)
    protected CurrencyExchange currencyExchange;

    @JsonIgnore
    @OneToMany(targetEntity = Charges.class, mappedBy = "cashEntry", fetch = FetchType.LAZY)
    protected List<Charges> charges;

    @OneToOne(targetEntity = CashAvailability.class, mappedBy = "cashEntry", fetch = FetchType.LAZY)
    protected CashAvailability availability;

    @OneToOne(mappedBy = "cashEntry", fetch = FetchType.LAZY)
    protected Interest interest;

    @ManyToOne(targetEntity = BookEntry.class)
    protected BookEntry relatedBookEntry;

    @ManyToOne(targetEntity = BookEntry.class)
    protected BookEntry debitRelatedBookEntry;

    @ManyToOne(targetEntity = BookEntry.class)
    protected BookEntry creditRelatedBookEntry;

    @OneToOne(targetEntity = InvoiceFinancingAgreement.class, mappedBy = "resultingCashEntry", fetch = FetchType.LAZY)
    protected InvoiceFinancingAgreement relatedInvoiceFinancingTransaction;

    @ManyToOne(targetEntity = PaymentInvestigationCase.class)
    protected PaymentInvestigationCase relatedInvestigationCase;

    @ManyToOne(targetEntity = PaymentInvestigationCaseResolution.class)
    protected PaymentInvestigationCaseResolution relatedInvestigationCaseResolution;

    @Override
    public String getInstanceName() {
        return "CASH ENTRY: " + id;
    }

    @Override
    public String toString() {
        return "CashEntry{}";
    }
}
