package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;


import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import javax.persistence.Index;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alfred on 28 Jan 2019
 */

@Data
@Entity
//@Indexed
@Table(name = "line_item")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "line_item_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class LineItem implements Auditable {
    @Id
    @Column(length = 50)
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @OneToOne(targetEntity = Adjustment.class, mappedBy = "relatedLineItem", fetch = FetchType.LAZY)
    protected Adjustment financialAdjustment;

    @JsonIgnore
    @OneToMany(targetEntity = Charges.class, mappedBy = "logisticsChargeLineItem", fetch = FetchType.LAZY)
    protected List<Charges> logisticsCharge;

    @JsonIgnore
    @OneToMany(targetEntity = Charges.class, mappedBy = "lineItem", fetch = FetchType.LAZY)
    protected List<Charges> charges;

    @OneToOne(targetEntity = Charges.class, mappedBy = "netPriceChargeLineItem", fetch = FetchType.LAZY)
    protected Charges netPriceCharge;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "gross_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "gross_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "gross_amount_currency_code"))
    })
    protected CurrencyAndAmount grossAmount;

    protected String identification;

//    protected Product invoicedProduct;
//    protected ProductQuantity netWeight;
//    protected List<com.tools20022.repository.entity.ProductQuantity> billedQuantity;
//    protected List<com.tools20022.repository.entity.ProductQuantity> chargeFreeQuantity;
//    protected ProductQuantity measureQuantityStartRelatedLineItem;
//    protected ProductQuantity measureQuantityEndRelatedLineItem;

    protected OffsetDateTime measureDateTimeStart;

    protected OffsetDateTime measureDateTimeEnd;

    @ManyToOne(targetEntity = Invoice.class, fetch = FetchType.LAZY)
    protected Invoice invoice;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "net_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "net_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "net_amount_currency_code"))
    })
    protected CurrencyAndAmount netAmount;

//    protected List<com.tools20022.repository.entity.Packaging> packaging;

    protected OffsetDateTime deliveryDateTime;

//    protected List<com.tools20022.repository.entity.ProductQuantity> grossPriceQuantity;
//    protected List<com.tools20022.repository.entity.ProductQuantity> netPriceQuantity;
//    protected List<com.tools20022.repository.entity.ProductQuantity> grossWeight;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "LineItem{}";
    }
}
