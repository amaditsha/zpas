package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import zw.co.esolutions.zpas.codeset.*;

import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class AccountStatus extends Status implements Auditable {

    @Enumerated(EnumType.STRING)
    protected AccountStatusCode status;

    protected Boolean blocked;

    @Enumerated(EnumType.STRING)
    protected AccountManagementStatusCode managementStatus;

    @Enumerated(EnumType.STRING)
    protected EntryStatusCode entryStatus;

    @Enumerated(EnumType.STRING)
    protected BalanceStatusCode balanceStatus;

    @Enumerated(EnumType.STRING)
    protected ReasonBlockedCode blockedReason;

    @JsonIgnore
    @OneToMany(mappedBy = "status", fetch = FetchType.LAZY)
    protected List<Account> account;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "ACCOUNT STATUS: " + id;
    }

    @Override
    public String toString() {
        return "AccountStatus{}";
    }
}
