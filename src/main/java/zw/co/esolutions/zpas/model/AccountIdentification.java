package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import org.hibernate.search.annotations.*;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import javax.persistence.Index;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Data
@Entity
@Table(name = "account_identification", indexes = {
        @Index(name = "account_identification_entitystatus", columnList = "entityStatus"),
        @Index(name = "account_identification_datecreated", columnList = "dateCreated"),
        @Index(name = "account_identification_lastupdated", columnList = "lastUpdated")
})
@Indexed
@AnalyzerDef(name = "accountidentificationanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class AccountIdentification implements Auditable {
    @Id
    protected String id;

    @Field(index = org.hibernate.search.annotations.Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "accountidentificationanalyzer")
    protected String iBAN;

    @Field(index = org.hibernate.search.annotations.Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "accountidentificationanalyzer")
    protected String bBAN;

    @Field(index = org.hibernate.search.annotations.Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "accountidentificationanalyzer")
    protected String uPIC;

    @Field(index = org.hibernate.search.annotations.Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "accountidentificationanalyzer")
    protected String name;

    @Field(index = org.hibernate.search.annotations.Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "accountidentificationanalyzer")
    protected String number;

    @OneToOne(targetEntity = GenericIdentification.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    protected GenericIdentification proprietaryIdentification;

    @OneToOne(mappedBy = "identification", fetch = FetchType.LAZY)
    protected Account account;

    @OneToOne(targetEntity = GenericIdentification.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    protected GenericIdentification costReferencePattern;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "ACCOUNT IDENTIFICATION: " + id;
    }

    @PrePersist
    private void setDefaultEntityAttributes(){
        this.id = GenerateKey.generateEntityId();
        this.entityStatus = EntityStatus.DRAFT;
    }

    @Override
    public String toString() {
        return "AccountIdentification{" +
                "id='" + id + '\'' +
                ", iBAN='" + iBAN + '\'' +
                ", bBAN='" + bBAN + '\'' +
                ", uPIC='" + uPIC + '\'' +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
