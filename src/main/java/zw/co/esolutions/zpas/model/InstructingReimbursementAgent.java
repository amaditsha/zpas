package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

/**
 * Created by mabuza on 30 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class InstructingReimbursementAgent extends CashSettlementInstructionPartyRole {

    @Override
    public String getInstanceName() {
        return "INSTRUCTING_REIMBURSEMENT_AGENT : " + id;
    }
}
