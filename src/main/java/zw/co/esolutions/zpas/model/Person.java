package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.enums.CivilStatusCode;
import zw.co.esolutions.zpas.enums.GenderCode;
import zw.co.esolutions.zpas.enums.LanguageCode;
import zw.co.esolutions.zpas.enums.ResidentialStatusCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by tanatsa on 23 Jan 2019
 */
@Audited
@EqualsAndHashCode(callSuper = true, exclude = {"placeOfBirth", "personIdentification"})
@Entity
@Data
//Indexed
@AnalyzerDef(name = "personanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class Person extends Party {

    @Enumerated(EnumType.STRING)
    protected GenderCode gender;

    @Enumerated(EnumType.STRING)
    protected LanguageCode language;

    @NotNull
    protected OffsetDateTime birthDate;

    @OneToOne(targetEntity = Location.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    protected Location placeOfBirth;

    @NotNull
    protected String profession;

    @Enumerated(EnumType.STRING)
    protected ResidentialStatusCode residentialStatus;

    @JsonIgnore
    @OneToMany(mappedBy = "citizen", fetch = FetchType.LAZY)
    @NotAudited
    protected List<Country> nationality;

    protected String businessFunctionTitle;

    @JsonIgnore
    @OneToMany(mappedBy = "person", cascade = {CascadeType.PERSIST}, targetEntity = PersonIdentification.class)
    protected List<PersonIdentification> personIdentification;


    @ManyToOne(targetEntity = EmployingPartyRole.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @NotAudited
    protected EmployingPartyRole employingParty;

    @OneToOne(mappedBy = "person", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST})
    @NotAudited
    protected PersonProfile personProfile;

    @OneToOne(mappedBy = "person", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @NotAudited
    protected ContactPersonRole contactPersonRole;

    @Enumerated(EnumType.STRING)
    protected CivilStatusCode civilStatus;

    protected OffsetDateTime deathDate;

    protected OffsetDateTime citizenshipEndDate;

    protected OffsetDateTime citizenshipStartDate;

    @Override
    public String toString() {
        return "Person";
    }

    @Override
    public String getName() {
        final StringBuilder financialInstitutionName = new StringBuilder();
        this.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
            personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                financialInstitutionName.append(personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName());
            });
        });
        return financialInstitutionName.toString();
    }

    public List<Organisation> getEmployers() {
        return Optional.ofNullable(this.getEmployingParty()).map(employingPartyRole -> {
            final List<Organisation> employingOrganisations = employingPartyRole.getPlayer().stream().filter(epr -> epr instanceof Organisation)
                    .map(epr -> (Organisation) epr).collect(Collectors.toList());
            return employingOrganisations;
        }).orElse(new ArrayList<>());
    }
}
