package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.ExternalStatusReason1Code;
import zw.co.esolutions.zpas.enums.*;

import javax.persistence.*;
import java.util.Comparator;
import java.util.Optional;

/**
 * Created by alfred on 29 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//@Indexed
@AnalyzerDef(name = "payment_status_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class PaymentStatus extends Status {
    public PaymentStatus() {
        super();
        this.status = PaymentStatusCode.None;
        this.suspendedStatusReason = SuspendedStatusReasonCode.None;
        this.pendingSettlement = PendingSettlementStatusReasonCode.None;
        this.instructionStatus = PaymentInstructionStatusCode.None;
        this.transactionRejectionReason = TransactionReasonCode.None;
        this.notificationStatus = NotificationToReceiveStatusCode.None;
        this.transactionStatus = TransactionStatusCode.None;
        this.cashPaymentStatus = CashPaymentStatusCode.None;
        this.cancellationReason = CancellationReasonCode.None;
        this.mandateRejectionReason = MandateReasonCode.None;
        this.pendingFailingSettlement = PendingFailingSettlementCode.None;
    }

    @Enumerated(EnumType.STRING)
    protected PaymentStatusCode status;

    @ManyToOne(targetEntity = Payment.class)
    protected Payment payment;

    @Enumerated(EnumType.STRING)
    protected UnmatchedStatusReasonCode unmatchedStatusReason;

    @Enumerated(EnumType.STRING)
    protected SuspendedStatusReasonCode suspendedStatusReason;

    @Enumerated(EnumType.STRING)
    protected PendingSettlementStatusReasonCode pendingSettlement;

    @Enumerated(EnumType.STRING)
    protected PaymentInstructionStatusCode instructionStatus;

    @Enumerated(EnumType.STRING)
    protected TransactionReasonCode transactionRejectionReason;

    @ManyToOne(targetEntity = PaymentInvestigationCase.class)
    protected PaymentInvestigationCase relatedInvestigationCase;

    @Enumerated(EnumType.STRING)
    protected NotificationToReceiveStatusCode notificationStatus;

    @Enumerated(EnumType.STRING)
    protected TransactionStatusCode transactionStatus;

    @Enumerated(EnumType.STRING)
    protected CashPaymentStatusCode cashPaymentStatus;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "unsuccessful_status_reason_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "unsuccessful_status_reason_code"))
    })
    protected ExternalStatusReason1Code unsuccessfulStatusReason;

    @Enumerated(EnumType.STRING)
    protected CancellationReasonCode cancellationReason;

    @Enumerated(EnumType.STRING)
    protected MandateReasonCode mandateRejectionReason;

    @Enumerated(EnumType.STRING)
    protected PendingFailingSettlementCode pendingFailingSettlement;

    @Override
    public String toString() {
        return "PaymentStatus{" +
                "status=" + status +
                ", unmatchedStatusReason=" + unmatchedStatusReason +
                ", suspendedStatusReason=" + suspendedStatusReason +
                ", pendingSettlement=" + pendingSettlement +
                ", instructionStatus=" + instructionStatus +
                ", transactionRejectionReason=" + transactionRejectionReason +
                ", relatedInvestigationCase=" + relatedInvestigationCase +
                ", notificationStatus=" + notificationStatus +
                ", transactionStatus=" + transactionStatus +
                ", cashPaymentStatus=" + cashPaymentStatus +
                ", unsuccessfulStatusReason=" + unsuccessfulStatusReason +
                ", cancellationReason=" + cancellationReason +
                ", mandateRejectionReason=" + mandateRejectionReason +
                ", pendingFailingSettlement=" + pendingFailingSettlement +
                ", id='" + id + '\'' +
                ", entityStatus=" + entityStatus +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityVersion=" + entityVersion +
                ", statusReason=" + statusReason +
                ", statusDateTime=" + statusDateTime +
                ", statusDescription='" + statusDescription + '\'' +
                ", instructionProcessingStatus=" + instructionProcessingStatus +
                ", settlementStatus=" + settlementStatus +
                ", cancellationProcessingStatus=" + cancellationProcessingStatus +
                ", transactionProcessingStatus=" + transactionProcessingStatus +
                ", modificationProcessingStatus=" + modificationProcessingStatus +
                '}';
    }

    public StatusReason getLatestStatusReason() {
        return this.getStatusReason().stream()
                .max(Comparator.comparing(StatusReason::getDateCreated)).orElse(new StatusReason());
    }
}
