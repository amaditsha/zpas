package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Table(name = "date_time_period", indexes = {
        @Index(name = "date_time_period_entitystatus", columnList = "entityStatus"),
        @Index(name = "date_time_period_datecreated", columnList = "dateCreated"),
        @Index(name = "date_time_period_lastupdated", columnList = "lastUpdated"),
})
public class DateTimePeriod implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    protected OffsetDateTime fromDateTime;

    protected OffsetDateTime toDateTime;

    @Column(length = 50)
    protected Number numberOfDays;

    @OneToOne(targetEntity = StandingOrder.class, fetch = FetchType.LAZY)
    protected StandingOrder relatedStandingOrder;

    @OneToOne(targetEntity = PaymentInstruction.class, fetch = FetchType.LAZY)
    protected PaymentInstruction paymentInstruction;

    @OneToOne(targetEntity = Status.class, fetch = FetchType.LAZY)
    protected Status status;

    @OneToOne(mappedBy = "reportedPeriod", fetch = FetchType.LAZY)
    protected Account account;

    @ManyToOne(targetEntity = Agreement.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    protected Agreement relatedAgreement;

    @ManyToOne(targetEntity = Invoice.class, fetch = FetchType.LAZY)
    protected Invoice relatedInvoice;

    @OneToOne(targetEntity = System.class, fetch = FetchType.LAZY)
    protected System system;

    @ManyToOne(targetEntity = AccountRestriction.class, fetch = FetchType.LAZY)
    protected AccountRestriction accountRestriction;

    @JsonIgnore
    @ManyToMany(targetEntity = Limit.class, fetch = FetchType.LAZY)
    protected List<Limit> relatedLimit;

    @JsonIgnore
    @OneToMany(targetEntity = PartyIdentificationInformation.class, fetch = FetchType.LAZY)
    protected List<PartyIdentificationInformation> relatedIdentification;

    @OneToOne(targetEntity = Scheme.class, fetch = FetchType.LAZY)
    protected Scheme assessmentValidityScheme;

    @OneToOne(targetEntity = PostalAddress.class, fetch = FetchType.LAZY)
    protected PostalAddress relatedPostalAddress;

    @OneToOne(targetEntity = Adjustment.class, fetch = FetchType.LAZY)
    protected Adjustment relatedAdjustment;

    @OneToOne(targetEntity = PaymentTerms.class, fetch = FetchType.LAZY)
    protected PaymentTerms relatedPaymentTerms;

    @JsonIgnore
    @OneToMany(targetEntity = RolePlayer.class, mappedBy = "validityPeriod", fetch = FetchType.LAZY)
    protected List<RolePlayer> relatedRolePlayer;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "date.value", column = @Column(name = "related_system_availability_date")),
    })
    protected SystemAvailability relatedSystemAvailability;

    @ManyToOne(targetEntity = BankOperation.class, fetch = FetchType.LAZY)
    protected BankOperation bankOperation;
//    protected ValuationStatistics valuationStatistics;
//    protected PerformanceFactors performanceFactors;
//    protected SecuritiesPricing priceCalculationRelatedPricing;
//    protected CorporateActionOption corporateActionOption;
//    protected SecuritiesProceedsDefinition parallelTradingProceedsDefinition;
//    protected SuspensionPeriod privilegeSuspensionCorporateAction;
//    protected SuspensionPeriod withdrawalSuspensionRelatedEvent;
//    protected InterestCalculation relatedInterestCalculation;
//    protected BiddingConditions biddingConditions;
//    protected ClassAction classAction;
//    protected SuspensionPeriod bookEntryTransferSuspensionRelatedEvent;
//    protected SuspensionPeriod depositAtAgentSuspensionRelatedEvent;
//    protected SuspensionPeriod depositSuspensionRelatedEvent;
//    protected SuspensionPeriod pledgeSuspensionRelatedEvent;
//    protected SuspensionPeriod segregationPeriodRelatedEvent;
//    protected SuspensionPeriod withdrawalAtAgentSuspensionRelatedEvent;
//    protected SuspensionPeriod withdrawalInNomineeNameSuspensionRelatedEvent;
//    protected SuspensionPeriod withdrawalInStreetNameSuspensionRelatedEvent;
//    protected CorporateActionEvent bookClosureCorporateAction;
//    protected SuspensionPeriod coDepositoriesSuspensionRelatedEvent;
//    protected Debt extendiblePeriodDebt;
//    protected SecuritiesConversion securitiesConversion;
//    protected YieldCalculation yieldCalculation;
//    protected Debt customDateDebt;
//    protected TaxPeriodE taxPeriod;
//    protected TradeCertificate tradeCertificate;
//    protected PortfolioValuation relatedPortfolioValuation;
//    protected SecuritiesProceedsDefinition assentedLinePeriodProceedsDefinition;
//    protected SecuritiesProceedsDefinition sellThruIssuerProceedsDefinition;
//    protected ProductDelivery relatedProductDelivery;
//    protected CorporateActionEvent relatedCorporateAction;
//    protected Distribution exercisePeriodDistribution;
//    protected Distribution offerPeriodDistribution;
//    protected Distribution tradingPeriodDistribution;
//    protected Distribution blockingPeriodDistribution;
//    protected Guarantee guarantee;
//    protected SecuritiesPricing priceFactRelatedPricing;
//    protected Distribution cashDistribution;
//    protected ComponentSecurity componentSecurity;
//    protected TradingSession tradingSession;
//    protected FinancialInstrumentSwap financialInstrumentSwap;
//    protected SecuritiesIdentification relatedSecuritiesIdentification;

    @OneToMany(mappedBy = "validityPeriod", cascade = {CascadeType.PERSIST})
    protected List<StandingSettlementInstruction> relatedStandingSettlementInstruction;

//    protected BasicSecuritiesRegistration relatedSecuritiesRegistration;
//    protected AmountAndPeriod amount;
//    protected List<InvestmentPlan> relatedInvestmentPlan;
//    protected Issuance issuance;
//    protected PercentageAndPeriod percentage;
//    protected RedemptionSchedule redemptionSchedule;
//    protected AccountLink relatedAccountLink;

    @Version
    protected Long entityVersion;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("dateCreated", dateCreated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @PrePersist
    private void setDefaultEntityAttributes(){
        this.id = GenerateKey.generateEntityId();
        this.entityStatus = EntityStatus.ACTIVE;
    }

    @Override
    public String toString() {
        return "DateTimePeriod{" +
                "id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                '}';
    }
}
