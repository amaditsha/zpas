package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;


import zw.co.esolutions.zpas.enums.DebitCreditCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Entity
@Data
@Table(name = "cash_availability")
public class CashAvailability implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    protected OffsetDateTime date;

    protected String numberOfDays;

    @Enumerated(EnumType.STRING)
    protected DebitCreditCode creditDebitIndicator;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amountCurrency_codeName")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amountCurrency_name"))
    })
    protected CurrencyAndAmount amount;

    @OneToOne(targetEntity = CashBalance.class, mappedBy = "availability", fetch = FetchType.LAZY)
    protected CashBalance cashBalance;

    @OneToOne(targetEntity = CashEntry.class, fetch = FetchType.LAZY)
    protected CashEntry cashEntry;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "CASH AVAILABILITY: " + getId();
    }

    @Override
    public String toString() {
        return "CashAvailability{}";
    }
}
