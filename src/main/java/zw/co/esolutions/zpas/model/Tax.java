package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.*;

import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.enums.CurrencyCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Table(name = "tax", indexes = {
        @Index(name = "tax_entitystatus", columnList = "entityStatus"),
        @Index(name = "tax_datecreated", columnList = "dateCreated"),
        @Index(name = "tax_lastupdated", columnList = "lastUpdated"),
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "taxanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class Tax implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Enumerated(EnumType.STRING)
    protected TaxExemptReasonCode exemptionReason;

    @OneToOne(mappedBy = "tax", targetEntity = Country.class, fetch = FetchType.LAZY)
    protected Country country;

    //protected NetAssetValueCalculation taxLiabilityValueCalculation;

    @Enumerated(EnumType.STRING)
    protected TaxTypeCode type;

    @ManyToOne(targetEntity = Payment.class, fetch = FetchType.LAZY)
    protected Payment relatedPaymentSettlement;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "currency.name", column = @Column(name = "tax_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "tax_currency_code_name")),
            @AttributeOverride(name = "amount", column = @Column(name = "tax_amount"))
    })
    protected CurrencyAndAmount amount;

    protected BigDecimal rate;

    @OneToOne(mappedBy = "taxationConditions", fetch = FetchType.LAZY)
    protected Party taxableParty;

    // protected Party taxableParty;

    // protected NetAssetValueCalculation taxRefundValueCalculation;

    @Enumerated(EnumType.STRING)
    protected TaxationBasisCode basis;

  //  protected SecuritiesTransfer securitiesTransfer;

    @Enumerated(EnumType.STRING)
    protected RateTypeCode taxRateType;

    @JsonIgnore
    @OneToMany(mappedBy = "tax", fetch = FetchType.LAZY)
    protected List<CashAccount> taxAccount;

    protected String taxationConditions;

    @OneToOne(targetEntity = Adjustment.class, mappedBy = "tax", fetch = FetchType.LAZY)
    protected Adjustment adjustment;

    //@OneToOne
    //protected Interest interest;

    protected String identification;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "currency.name", column = @Column(name = "taxable_base_amount_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "taxable_base_amount_currency_code_name")),
            @AttributeOverride(name = "amount", column = @Column(name = "taxable_base_amount_amount"))
    })
    protected CurrencyAndAmount taxableBaseAmount;

    protected OffsetDateTime taxDate;

    protected String certificateIdentification;

    protected String administrationZone;

    protected String method;

   // protected List<TaxRecord> record;

   // protected Product product;

    @JsonIgnore
    @OneToMany(targetEntity = CurrencyExchange.class, mappedBy = "tax", fetch = FetchType.LAZY)
    protected List<CurrencyExchange> currencyExchange;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "name", column = @Column(name = "currency_name")),
            @AttributeOverride(name = "codeName", column = @Column(name = "currency_code_name"))
    })
    protected CurrencyCode currency;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "currency.name", column = @Column(name = "tax_deduction_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "tax_deduction_currency_code_name")),
            @AttributeOverride(name = "amount", column = @Column(name = "tax_deduction_amount"))
    })
    protected CurrencyAndAmount taxDeduction;

    //protected Distribution relatedCorporateActionDistribution;

    protected OffsetDateTime calculationDate;

  //  protected List<Dividend> dividend;

    @Enumerated(EnumType.STRING)
    protected WithholdingTaxRateTypeCode withholdingTaxType;

  //  protected CorporateActionEvent corporateActionEvent;

    @Enumerated(EnumType.STRING)
    protected TaxIdentificationNumberTypeCode taxIdentificationType;

    @JsonIgnore
    @ManyToMany()
    @JoinTable(name = "tax_tax_party_role",
            joinColumns = @JoinColumn(name = "tax_id"),
            inverseJoinColumns = @JoinColumn(name = "tax_party_role_id")
    )
    protected List<TaxPartyRole> partyRole;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("dateCreated", dateCreated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "Tax{}";
    }

    @PrePersist
    private void setDefaultEntityAttributes(){
        this.id = GenerateKey.generateEntityId();
        this.entityStatus = EntityStatus.DRAFT;
    }
}
