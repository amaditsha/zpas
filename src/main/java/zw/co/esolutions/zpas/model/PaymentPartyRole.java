package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@ToString(callSuper = true, exclude = { "payment", "cashAccount" })
public abstract class PaymentPartyRole extends Role {

    @JsonIgnore
    @ManyToMany(targetEntity = Payment.class, fetch = FetchType.LAZY)
    @JoinTable(name = "payment_party_role_payment",
            joinColumns = @JoinColumn(name = "payment_party_role_id"),
            inverseJoinColumns = @JoinColumn(name = "payment_id"))
    protected List<Payment> payment;

    @JsonIgnore
    @OneToMany(mappedBy = "paymentPartyRole", fetch = FetchType.LAZY)
    protected List<CashAccount> cashAccount;

    @Override
    public String toString() {
        return "PaymentPartyRole{" +
                "id='" + id + '\'' +
                ", entityStatus=" + entityStatus +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentPartyRole that = (PaymentPartyRole) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
