package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

import zw.co.esolutions.zpas.enums.BalanceAdjustmentTypeCode;
import zw.co.esolutions.zpas.enums.BalanceCounterPartyCode;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class CashBalance extends Balance {

    @Enumerated(EnumType.STRING)
    protected BalanceCounterPartyCode calculationType;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amountCurrency_codeName")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amountCurrency_name"))
    })
    protected CurrencyAndAmount amount;

    @Enumerated(EnumType.STRING)
    protected BalanceAdjustmentTypeCode balanceAdjustmentCode;

    protected OffsetDateTime cutOffDate;

    @JsonIgnore
    @ManyToMany(mappedBy = "cashBalance")
    protected List<CashAccount> cashAccount;

    @JsonIgnore
    @ManyToMany(mappedBy = "cashBalance")
    protected List<SystemMemberRole> counterparty;

    @OneToOne(fetch = FetchType.LAZY)
    protected CashAvailability availability;

    @JsonIgnore
    @ManyToMany()
    @JoinTable(name = "cash_balance_entry",
            joinColumns = @JoinColumn(name = "cash_balance_id"),
            inverseJoinColumns = @JoinColumn(name = "cash_entry_id")
    )
    protected List<CashEntry> cashBalanceEntry;

    @OneToOne(mappedBy = "contractBalance", fetch = FetchType.LAZY)
    protected RegisteredContract relatedRegisteredContract;

    @Override
    public String getInstanceName() {
        return "BALANCE: " + getId();
    }

    @Override
    public String toString() {
        return "CashBalance{" +
                "amount=" + amount +
                ", valueDate=" + valueDate +
                '}';
    }
}
