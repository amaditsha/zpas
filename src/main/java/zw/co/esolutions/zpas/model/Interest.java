package zw.co.esolutions.zpas.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Created by mabuza on 25 Jan 2019
 */
@Entity
public class Interest {
    @Id
    protected String id;

    @OneToOne(targetEntity = CashEntry.class, fetch = FetchType.LAZY)
    protected CashEntry cashEntry;

    @Override
    public String toString() {
        return "Interest{}";
    }
}
