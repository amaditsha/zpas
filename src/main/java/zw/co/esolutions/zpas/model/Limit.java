package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

import zw.co.esolutions.zpas.enums.FloorLimitTypeCode;
import zw.co.esolutions.zpas.enums.LimitTypeCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 23 Jan 2019
 */

@Data
@Entity
//@Indexed
@Table(name = "limit_entity", indexes = {
        @Index(name = "limit_status", columnList = "entityStatus")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "limitanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class Limit implements Auditable {
    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;
    @Version
    protected Long entityVersion;

    @Enumerated(EnumType.STRING)
    protected LimitTypeCode type;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount_value")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amount_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amount_code"))
    })
    protected CurrencyAndAmount amount;
    @Enumerated(EnumType.STRING)
    protected FloorLimitTypeCode creditDebitIndicator;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "used_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "used_amount_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "used_amount_code"))
    })
    protected CurrencyAndAmount usedAmount;

    protected BigDecimal usedPercentage;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name ="currency_code")),
            @AttributeOverride(name = "name", column = @Column(name ="currency_name")),
    })
    protected CurrencyCode currency;

    @JsonIgnore
    @OneToMany(targetEntity = LimitStatus.class, fetch = FetchType.LAZY)
    protected List<LimitStatus> limitStatus;

    protected BigDecimal percentage;

    @OneToOne(targetEntity = DebitCreditFacility.class, fetch = FetchType.LAZY)
    protected DebitCreditFacility relatedDebitCreditFacility;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "LIMIT" + id;
    }

    @Override
    public String toString() {
        return "Limit{}";
    }
}
