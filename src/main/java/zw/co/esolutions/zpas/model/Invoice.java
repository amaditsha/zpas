package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.codeset.ExternalBillingCompensationTypeCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.DebitCreditCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
//@Indexed
@Entity
@Table(name = "invoice", indexes = {
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "invoiceanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class Invoice implements Auditable {
    @Id
    @Column
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "credit_debit_note_amount_amount")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "credit_debit_note_amount_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "credit_debit_note_amount_currency_code_name"))
    })
    protected CurrencyAndAmount creditDebitNoteAmount;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "total_tax_amount_amount")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "total_tax_amount_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "total_tax_amount_currency_code_name"))
    })
    protected CurrencyAndAmount totalTaxAmount;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "total_invoice_amount_amount")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "total_invoice_amount_currency_code")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "total_invoice_amount_currency_code_name"))
    })
    protected CurrencyAndAmount totalInvoiceAmount;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "currency.name", column = @Column(name = "invoice_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "invoice_currency_code_name")),
            @AttributeOverride(name = "amount", column = @Column(name = "invoice_currency_amount"))
    })
    protected CurrencyCode invoiceCurrency;

    @JsonIgnore
    @OneToMany(targetEntity = DateTimePeriod.class, mappedBy = "relatedInvoice", fetch = FetchType.LAZY)
    protected List<DateTimePeriod> periodCovered;

    //protected List<CommercialTradeSettlement> tradeSettlement;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "total_charge_amount")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "total_charge_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "total_charge_currency_code_name"))
    })
    protected CurrencyAndAmount totalCharge;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "total_prepaid_amount_amount")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "total_prepaid_amount_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "total_prepaid_amount_currency_code_name"))
    })
    protected CurrencyAndAmount totalPrepaidAmount;

    @JsonIgnore
    @OneToMany(targetEntity = LineItem.class, mappedBy = "invoice", fetch = FetchType.LAZY)
    protected List<LineItem> lineItem;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "total_net_amount_amount")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "total_net_amount_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "total_net_amount_currency_code_name"))
    })
    protected CurrencyAndAmount totalNetAmount;

    @JsonIgnore
    @OneToMany(targetEntity = CurrencyExchange.class, mappedBy = "relatedInvoice",fetch = FetchType.LAZY)
    protected List<CurrencyExchange> currencyExchange;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "name", column = @Column(name = "billing_compensation_type_name")),
            @AttributeOverride(name = "codeName", column = @Column(name = "billing_compensation_type_code_name"))
    })
    protected ExternalBillingCompensationTypeCode billingCompensationType;

    @JsonIgnore
    @ManyToMany(mappedBy = "invoice", fetch = FetchType.LAZY)
    protected List<InvoicePartyRole> invoicePartyRole;

    @ManyToOne(targetEntity = Invoice.class, fetch = FetchType.LAZY)
    protected Invoice originalInvoice;

    @JsonIgnore
    @OneToMany(targetEntity = Invoice.class, mappedBy = "originalInvoice", fetch = FetchType.LAZY)
    protected List<Invoice> relatedInvoice;

    @ManyToOne(targetEntity = InvoiceFinancingAgreement.class, fetch = FetchType.LAZY)
    protected InvoiceFinancingAgreement invoiceFinancingTransaction;

    @OneToOne(targetEntity = InvoiceStatus.class, mappedBy = "invoice", fetch = FetchType.LAZY)
    protected InvoiceStatus invoiceStatus;

    @JsonIgnore
    @ManyToMany(targetEntity = Payment.class, mappedBy = "invoiceReconciliation",fetch = FetchType.LAZY)
    protected List<Payment> payment;

    @Enumerated(EnumType.STRING)
    protected DebitCreditCode creditDebitIndicator;

    @Version
    protected Long entityVersion;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "Invoice{}";
    }
}
