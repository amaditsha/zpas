package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CountryCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.PaymentPurposeCode;
import zw.co.esolutions.zpas.enums.RemittanceLocationMethodCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Created by alfred on 23 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//Indexed
@AnalyzerDef(name = "payment_obligation_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class PaymentObligation extends Obligation {

    @JsonIgnore
    @ManyToMany(targetEntity = Payment.class)
    @JoinTable(name = "payment_offset",
            joinColumns = @JoinColumn(name = "payment_obligation_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "payment_id", referencedColumnName = "id"))
    protected List<Payment> paymentOffset;

    @Enumerated(EnumType.STRING)
    protected PaymentPurposeCode purpose;

    @Enumerated(EnumType.STRING)
    protected RemittanceLocationMethodCode remittanceDeliveryMethod;

    @JsonIgnore
    @OneToMany(targetEntity = Document.class, mappedBy = "paymentObligation", fetch = FetchType.LAZY)
    protected List<Document> associatedDocument;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amount_ccy_code")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amount_ccy_name"))
    })
    protected CurrencyAndAmount amount;

    @JsonIgnore
    @OneToMany(targetEntity = ContactPoint.class, mappedBy = "remittanceRelatedPayment")
    protected List<ContactPoint> remittanceLocation;

//    protected List<InterestManagement> interest;


    //    protected CommercialTrade commercialTrade;
    protected BigDecimal percentage;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "maximum_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "maximum_amount_ccy_code")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "maximum_amount_ccy_name"))
    })
    protected CurrencyAndAmount maximumAmount;

    protected OffsetDateTime expiryDate;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "applicable_law_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "applicable_law_name"))
    })
    protected CountryCode applicableLaw;

    @JsonIgnore
    @ManyToMany(targetEntity = PaymentObligationPartyRole.class, cascade = {CascadeType.PERSIST}, mappedBy = "paymentObligation", fetch = FetchType.LAZY)
    protected List<PaymentObligationPartyRole> partyRole;
    //    protected BuyIn paymentSourceBuyIn;
//    protected CorporateActionProceedsDeliveryInstruction relatedCorporateAction;
//    protected CollateralMovement relatedCollateralMovement;
//    protected Demand paymentSourceUndertakingDemand;
//    protected SecuritiesTradeExecution executedSecuritiesTrade;
    @ManyToOne(targetEntity = CashAccountContract.class, fetch = FetchType.LAZY)
    protected CashAccountContract relatedAccountClosingTerms;
//    protected List<PortfolioTransfer> paymentSourcePortfolioTransfer;
//    protected CurrencyOption paymentSourceCurrencyOption;
//    protected ForeignExchangeTrade exchangeRateInformation;
//    protected Dividend dividend;
//    protected RepurchaseAgreement repurchaseAgreement;
//    protected Assignment relatedAssignment;
//    protected BankingTransaction bankingTransaction;

    @OneToOne(targetEntity = PaymentTerms.class)
    protected PaymentTerms paymentTerms;

    protected OffsetDateTime paymentDueDate;

    public String getObligationStatus() {
        final String obligationAuthorisationStatus = getObligationAuthorisationStatus();
        if(OffsetDateTime.now().isAfter(paymentDueDate.withHour(0).withMinute(0))) {
            switch (obligationAuthorisationStatus) {
                case "PENDING_AUTHORISATION":
                    return "EXPIRED";
                case "PARTIALLY_AUTHORISED":
                    return "PARTIALLY_EXPIRED";
                case "AUTHORISED":
                case "REJECTED":
                default:
                    return obligationAuthorisationStatus;
            }
        } else {
            return obligationAuthorisationStatus;
        }
    }

    public String getObligationAuthorisationStatus() {
        final long authorisedPayments = getPaymentOffset().stream()
                .filter(payment -> payment.getEntityStatus() == EntityStatus.PENDING_DISPATCH).count();
        final long unauthorisedPayments = getPaymentOffset().stream()
                .filter(payment -> payment.getEntityStatus() == EntityStatus.DISAPPROVED).count();
        if (unauthorisedPayments == getPaymentOffset().size()) {
            return "REJECTED";
        }
        if (authorisedPayments == 0) {
            return "PENDING_AUTHORISATION";
        } else if (authorisedPayments == getPaymentOffset().size()) {
            return "AUTHORISED";
        } else {
            return "PARTIALLY_AUTHORISED";
        }
    }

    @Override
    public String toString() {
        return "PaymentObligation{" +
                ", purpose=" + purpose +
                ", remittanceDeliveryMethod=" + remittanceDeliveryMethod +
                ", amount=" + amount +
                ", percentage=" + percentage +
                ", maximumAmount=" + maximumAmount +
                ", expiryDate=" + expiryDate +
                ", paymentDueDate=" + paymentDueDate +
                ", id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                '}';
    }
}
