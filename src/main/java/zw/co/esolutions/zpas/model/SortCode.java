package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Map;

@Entity
@Data
public class SortCode implements Auditable {
    @Id
    protected String id;

    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdate;

    @Column
    protected String sortCode;

    @ManyToOne(targetEntity = Organisation.class, fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    protected Organisation financialInstitution;

    @Override
    public String getEntityName() {
        return null;
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        return null;
    }

    @Override
    public String getAuditableAttributesString() {
        return null;
    }

    @Override
    public String getInstanceName() {
        return null;
    }

    @Override
    public String toString() {
        return "SortCode{}";
    }
}
