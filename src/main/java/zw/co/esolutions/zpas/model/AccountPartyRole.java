package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jnkomo on 23 Jan 2019
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AnalyzerDef(name = "accountpartyroleanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class AccountPartyRole extends Role {

    @JsonIgnore
    @ManyToMany(targetEntity = Account.class, fetch = FetchType.LAZY)
    @JoinTable(name = "account_account_party_role",
            joinColumns = @JoinColumn(name = "account_party_role_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id")
    )
    protected List<Account> account;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "ACCOUNT_PARTY_ROLE: " + id;
    }

    @Override
    public String toString() {
        return "AccountPartyRole{" +
                "id='" + id + '\'' +
                ", entityStatus=" + entityStatus +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityVersion=" + entityVersion +
                '}';
    }
}
