package zw.co.esolutions.zpas.model.taxclearance;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.model.NonFinancialInstitution;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Optional;

/**
 * Created by alfred on 20 May 2019
 */
@Audited
@Data
@Entity
@Table(name = "tax_clearance")
@AnalyzerDef(name = "tax_clearance_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class TaxClearance {
    @Id
    @Column(length = 50)
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    protected OffsetDateTime startDate;
    protected OffsetDateTime expiryDate;
    protected String registeredName;
    protected String bpn;

    public String getRegisteredName() {
        return Optional.ofNullable(registeredName)
                .orElseGet(() -> Optional.ofNullable(nonFinancialInstitution)
                        .map(nonFinancialInstitution1 -> nonFinancialInstitution1.getName())
                        .orElse(""));
    }

    public String getBpn() {
        return Optional.ofNullable(bpn)
                .orElseGet(() -> Optional.ofNullable(nonFinancialInstitution)
                        .map(nonFinancialInstitution1 -> nonFinancialInstitution1.getBusinessPartnerNumber())
                        .orElse(""));
    }

    @ManyToOne(targetEntity = NonFinancialInstitution.class, fetch = FetchType.LAZY)
    protected NonFinancialInstitution nonFinancialInstitution;

    @Override
    public String toString() {
        return "TaxClearance{" +
                "id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                ", startDate=" + startDate +
                ", expiryDate=" + expiryDate +
                ", registeredName='" + registeredName + '\'' +
                ", bpn='" + bpn + '\'' +
                '}';
    }
}
