package zw.co.esolutions.zpas.model.enversaudit;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.EntityTrackingRevisionListener;
import org.hibernate.envers.RevisionType;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.Serializable;

@Slf4j
public class AuditingRevisionListener implements EntityTrackingRevisionListener {
    @Override
    public void newRevision(Object revisionEntity) {
        try {
            AuditedRevisionEntity auditedRevisionEntity = (AuditedRevisionEntity) revisionEntity;
            String userName = SecurityContextHolder.getContext().getAuthentication().getName();
            auditedRevisionEntity.setUsername(userName);
        } catch (RuntimeException re) {
            log.error("An error occurred trying to log revisions. {}", re.getMessage());
        }
    }

    @Override
    public void entityChanged(Class entityClass, String entityName, Serializable entityId, RevisionType revisionType, Object revisionEntity) {
        String type = entityClass.getName();
        ((AuditedRevisionEntity)revisionEntity).addModifiedEntityType(type);

    }
}
