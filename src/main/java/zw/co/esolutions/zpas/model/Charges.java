package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.*;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

import zw.co.esolutions.zpas.enums.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Created by mabuza on 25 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AnalyzerDef(name = "charges_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class Charges extends Adjustment {

    @Enumerated(EnumType.STRING)
    protected ChargeTypeCode chargeType;

    @Enumerated(EnumType.STRING)
    protected CalculationBasisCode calculationBasis;

    @Enumerated(EnumType.STRING)
    protected ChargeBearerTypeCode bearerType;

    @JsonIgnore
    @ManyToMany(mappedBy = "charges", fetch = FetchType.LAZY)
    protected Set<CashAccount> chargesDebitAccount;

    @ManyToOne(targetEntity = CashEntry.class, fetch = FetchType.LAZY)
    protected CashEntry cashEntry;

    @Enumerated(EnumType.STRING)
    protected DebitCreditCode creditDebitIndicator;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "maximum_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "maximum_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "maximum_amount_currency_code"))
    })
    protected CurrencyAndAmount maximumAmount;

//    protected InvestmentFundTransaction investmentFundTransaction;
//    protected Transport transport;
//    protected Undertaking relatedUndertaking;

    @OneToOne(targetEntity = AccountService.class, mappedBy = "accountAdministrationCharge", fetch = FetchType.LAZY)
    protected AccountService services;

    @ManyToOne(targetEntity = LineItem.class, fetch = FetchType.LAZY)
    protected LineItem logisticsChargeLineItem;

    @ManyToOne(targetEntity = LineItem.class, fetch = FetchType.LAZY)
    protected LineItem lineItem;

    @OneToOne(targetEntity = LineItem.class, fetch = FetchType.LAZY)
    protected LineItem netPriceChargeLineItem;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "base_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "base_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "base_amount_currency_code"))
    })
    protected CurrencyAndAmount baseAmount;

    protected BigDecimal maximumRate;

    protected BigDecimal minimumRate;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "minimum_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "minimum_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "minimum_amount_currency_code"))
    })
    protected CurrencyAndAmount minimumAmount;

//    protected List<InterestCalculation> relatedInterest;
@Enumerated(EnumType.STRING)
    protected ChargePaymentMethodCode chargePaymentMethod;

    @Override
    public String toString() {
        return "Charges{}";
    }
}
