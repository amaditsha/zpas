package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.enums.InvestigationExecutionConfirmationCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Created by mabuza on 23 Jan 2019
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@DiscriminatorColumn(length=100)
//Indexed
@AnalyzerDef(name = "paymentinvestigationcaseresolutionanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class PaymentInvestigationCaseResolution extends InvestigationResolution {
    @Enumerated(EnumType.STRING)
    protected InvestigationExecutionConfirmationCode investigationStatus;
    @JsonIgnore
    @OneToOne(targetEntity = DebitAuthorisation.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE},fetch = FetchType.LAZY)
    protected DebitAuthorisation debitAuthorisationConfirmation;
    @JsonIgnore
    @OneToOne(targetEntity = CashSettlement.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected CashSettlement coverCorrection;
    @JsonIgnore
    @OneToMany(mappedBy = "relatedInvestigationCaseResolution", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected List<CashEntry> entryCorrection;
    @JsonIgnore
    @OneToMany(mappedBy = "relatedInvestigationCaseResolution", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected List<Payment> paymentCorrection;
    @JsonIgnore
    @OneToMany(mappedBy = "relatedInvestigationCaseResolution", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected List<PaymentExecution> paymentExecutionCorrection;
    @JsonIgnore
    @OneToOne(targetEntity = PaymentInvestigationCaseRejection.class,  cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected PaymentInvestigationCaseRejection investigationCaseRejection;
    @JsonIgnore
    @OneToMany(mappedBy = "relatedCaseResolution", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected List<DuplicateCase> duplicateCase;

    @Override
    public String toString() {
        return "PaymentInvestigationCaseResolution{}";
    }
}
