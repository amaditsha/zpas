package zw.co.esolutions.zpas.model;

import lombok.Data;
import zw.co.esolutions.zpas.enums.MemberStatusCode;
import zw.co.esolutions.zpas.enums.SystemStatusCode;

import javax.persistence.*;

/**
 * Created by jnkomo on 25 Jan 2019
 */
@Data
@Entity
public class SystemStatus extends Status {
    @Enumerated(EnumType.STRING)
    protected SystemStatusCode status;

    @Enumerated(EnumType.STRING)
    protected MemberStatusCode memberStatus;

    @Override
    public String toString() {
        return "SystemStatus{}";
    }
}
