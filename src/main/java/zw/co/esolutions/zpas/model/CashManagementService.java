package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Data
public class CashManagementService extends CashAccountService{
    @Id
    private String id;

    @JsonIgnore
    @OneToMany(mappedBy = "relatedCashServices")
    protected List<CashStandingOrder> standingOrder;

    @ManyToOne(targetEntity = TransactionAdministrator.class)
    protected TransactionAdministrator relatedTransactionAdministrator;

    @Override
    public String toString() {
        return "CashManagementService{}";
    }
}
