package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import zw.co.esolutions.zpas.enums.*;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public abstract class CashAccountService extends AccountService {

    @Enumerated(EnumType.STRING)
    protected CompensationMethodCode compensationMethod;

    @Enumerated(EnumType.STRING)
    protected BillingCurrencyTypeCode billingCurrency;

    @Enumerated(EnumType.STRING)
    protected BillingChargeMethodCode billingChargeMethod;

    @Enumerated(EnumType.STRING)
    protected ServicePaymentMethodCode paymentMethod;

    @JsonIgnore
    @ManyToMany(mappedBy = "services")
    protected List<CashAccountMandate> cashAccountMandate;

    @ManyToOne(targetEntity = CashAccountContract.class)
    protected CashAccountContract cashAccountContract;

    @OneToOne(mappedBy = "relatedCashAccountService", fetch = FetchType.LAZY)
    protected GenericIdentification identification;

    @OneToOne(fetch = FetchType.LAZY)
    protected CashAccount cashAccount;

    @Override
    public String getInstanceName() {
        return "CASH ACCOUNT SERVICE: " + getId();
    }

    @Override
    public String toString() {
        return "CashAccountService{}";
    }
}
