package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Created by alfred on 30 Jan 2019
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//@Indexed
@AnalyzerDef(name = "bulk_payment_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class BulkPayment extends Payment {
    @JsonIgnore
    @OneToMany(targetEntity = IndividualPayment.class, mappedBy = "bulkPayment", fetch = FetchType.LAZY)
    protected List<IndividualPayment> groups;

    protected String headerID;
    protected OffsetDateTime processingDate;
    protected String senderID;
    protected String receiverID;
    protected String fileID;
    protected String workCode;
    protected String batchResponseCode;
    protected String trailerId;
    protected String sfiVersion;

    @Override
    public String toString() {
        return "BulkPayment{" +
                "groups=" +
                ", paymentObligation=" + paymentObligation +
                ", currencyOfTransfer=" + currencyOfTransfer +
                ", creditMethod=" + creditMethod +
                ", type=" + type +
                ", instructedAmount=" + instructedAmount +
                ", priority=" + priority +
                ", valueDate=" + valueDate +
                ", paymentStatus=" +
                ", paymentInstrument=" + paymentInstrument +
                ", account=" + account +
                ", adjustments=" +
                ", id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObligationFulfilment that = (ObligationFulfilment) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
