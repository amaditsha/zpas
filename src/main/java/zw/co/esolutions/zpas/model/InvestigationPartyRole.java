package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 28 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//@Indexed
@AnalyzerDef(name = "investigationpartyroleanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class InvestigationPartyRole extends Role {
    @JsonIgnore
    @ManyToMany(mappedBy = "investigationPartyRole", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    protected List<InvestigationCase> investigationCase;

    @JsonIgnore
    @OneToMany(targetEntity = Status.class, mappedBy = "partyRole", fetch = FetchType.LAZY)
    protected List<Status> status;

    public void addInvestigationCase(InvestigationCase investigationCase) {
        if (this.investigationCase == null)
            this.investigationCase = new ArrayList<>();
        this.investigationCase.add(investigationCase);
    }

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "INVESTIGATION_PARTY_ROLE" +id;
    }

    @Override
    public String toString() {
        return "InvestigationPartyRole{" +
//                "investigationCase=" + investigationCase +
//                ", status=" + status +
                ", id='" + id + '\'' +
                ", entityStatus=" + entityStatus +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityVersion=" + entityVersion +
                ", player=" + player +
//                ", contactPersonRole=" + contactPersonRole +
//                ", entry=" + entry +
//                ", customParty=" + customParty +
                ", partyRoleCode=" + partyRoleCode +
                '}';
    }
}
