package zw.co.esolutions.zpas.model;

import lombok.Data;

import javax.persistence.Entity;

/**
 * Created by jnkomo on 30 Jan 2019
 */
@Data
@Entity
public class AccountServicerRole extends AccountPartyRole {
}
