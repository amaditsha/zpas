package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class SystemPartyRole extends Role {

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    protected List<System> relatedSystem;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "SYSTEM PARTY ROLE: " + getId();
    }

    @Override
    public String toString() {
        return "SystemPartyRole{}";
    }
}
