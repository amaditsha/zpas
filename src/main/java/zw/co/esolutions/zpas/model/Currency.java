package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Data
@Entity
@Table(name = "currency")
@AnalyzerDef(name = "currencyanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class Currency implements Auditable {
    @Id
    protected String id;

    protected String code;

    protected String name;

    protected Boolean defaultCurrency;

    protected OffsetDateTime dateCreated;

    protected OffsetDateTime startDate;

    protected OffsetDateTime endDate;

    @Version
    protected long version;

    @Override
    public String getId() {
        return id;
    }

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("dateCreated", getDateCreated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", getEntityStatus().name());

        return attributesMap;
    }

    @Override
    public String getInstanceName() {
        return "CURRENCY: " + id;
    }

    @PrePersist
    public void setDefaultAttributes(){
        this.dateCreated = OffsetDateTime.now();
        this.entityStatus= EntityStatus.ACTIVE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(code, currency.code) &&
                Objects.equals(name, currency.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name);
    }
}
