package zw.co.esolutions.zpas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;


import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tanatsa on 24 Jan 2019
 */
//Indexed
@Entity
@Data
@Table(name = "person_profile")
public class PersonProfile implements Auditable {
    @Id
    protected String id;

    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @OneToOne(fetch = FetchType.LAZY, targetEntity = Person.class)
    protected Person person;

    protected String sourceOfWealth;

    protected String salaryRange;

    protected Boolean employeeTerminationIndicator;


    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "PersonProfile{}";
    }
}
