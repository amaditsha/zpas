package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "account_regex")
public class AccountRegex {
    @Id
    protected String id;

    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdate;

    protected String regex;

    protected String name;

    @ManyToOne(targetEntity = FinancialInstitution.class)
    protected FinancialInstitution financialInstitution;

    public AccountRegex() {
    }

    public AccountRegex(String id,
                        EntityStatus entityStatus,
                        OffsetDateTime dateCreated,
                        OffsetDateTime lastUpdate,
                        String regex, String name,
                        FinancialInstitution financialInstitution) {
        this.id = id;
        this.entityStatus = entityStatus;
        this.dateCreated = dateCreated;
        this.lastUpdate = lastUpdate;
        this.regex = regex;
        this.name = name;
        this.financialInstitution = financialInstitution;
    }
}
