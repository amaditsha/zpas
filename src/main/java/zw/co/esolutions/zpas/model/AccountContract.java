package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;



import zw.co.esolutions.zpas.enums.TransactionChannelCode;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AnalyzerDef(name = "accountcontractanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class AccountContract extends Contract {

    protected OffsetDateTime targetClosingDate;

    protected Boolean urgencyFlag;

    protected Boolean removalIndicator;

    protected OffsetDateTime targetGoLiveDate;

    @JsonIgnore
    @ManyToMany(mappedBy = "accountContract")
    protected List<Account> account;

    @ManyToOne(targetEntity = AccountService.class)
    protected AccountService accountService;

    protected OffsetDateTime requestDate;

    @JsonIgnore
    @OneToMany(mappedBy = "accountContract", fetch = FetchType.LAZY)
    protected List<Mandate> accountAuthorisation;

    @Enumerated(EnumType.STRING)
    protected TransactionChannelCode transactionChannel;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "ACCOUNTCONTRACT: " + getId();
    }

    @Override
    public String toString() {
        return "AccountContract{}";
    }
}
