package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.search.annotations.*;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
//@Indexed
@Table(name = "notification"/*,
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"entityId", "clientId", "groupId"})
        }*/
    )
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

@AnalyzerDef(name = "notificationanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class Notification implements Auditable {
    @Id
    @Column(length = 50)
    private String id;

    @CreationTimestamp
    private OffsetDateTime dateCreated;


    @Field
    @Analyzer(definition = "notificationanalyzer")
    @Enumerated(EnumType.STRING)
    private EntityStatus entityStatus;


    @Field
    @Column(columnDefinition = "text")
    @Analyzer(definition = "notificationanalyzer")
    private String message;

    private boolean read;

    @Field
    @Analyzer(definition = "notificationanalyzer")
    @Enumerated(EnumType.STRING)
    private NotificationType notificationType;


    @Column(name = "entityId")
    @Field
    @Analyzer(definition = "notificationanalyzer")
    private String entityId;

    @Column
    @Field
    @Analyzer(definition = "notificationanalyzer")
    private String clientId;

    @Column
    @Field
    @Analyzer(definition = "notificationanalyzer")
    private String username;

    @Column
    @Field
    @Analyzer(definition = "notificationanalyzer")
    private String groupId;

    @Version
    protected long version;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @JsonIgnore
    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("status", Optional.ofNullable(getEntityStatus()).map(Enum::name).orElse(""));
        attributesMap.put("message", getMessage());
        attributesMap.put("isRead", isRead() + "");
        attributesMap.put("dateCreated", Optional.ofNullable(dateCreated).map(OffsetDateTime::toString).orElse(""));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

}

