package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tanatsa on 23 Jan 2019
 */

@Audited
@Data
@Entity
//Indexed
@AnalyzerDef(name = "organisationidentificationinformationanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class OrganisationIdentification extends PartyIdentificationInformation {

    protected String bICFI;

    @ElementCollection
    @Column(name = "other_bic")
    protected List<String> otherBIC = new ArrayList<String>();

    protected String anyBIC;

    @JsonIgnore
    @OneToMany(mappedBy = "organisation", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected List<OrganisationName> organisationName;

    @ManyToOne(targetEntity = Organisation.class, fetch = FetchType.LAZY)
    protected Organisation organisation;

    protected String bICNonFI;

    protected String bankPartyIdentification;


    @Override
    public String toString() {
        return "OrganisationIdentification{}";
    }
}
