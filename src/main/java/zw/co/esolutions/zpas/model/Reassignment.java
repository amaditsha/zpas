package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.enums.CaseForwardingNotificationCode;

import javax.persistence.*;

/**
 * Created by mabuza on 28 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//Indexed
@AnalyzerDef(name = "reassignmentanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class Reassignment extends InvestigationResolution {

    @Enumerated(EnumType.STRING)
    protected CaseForwardingNotificationCode justification;

    @OneToOne(targetEntity = InvestigationCase.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE},fetch = FetchType.LAZY)
    protected InvestigationCase reassignedCase;

    @Override
    public String toString() {
        return "Reassignment{}";
    }
}
