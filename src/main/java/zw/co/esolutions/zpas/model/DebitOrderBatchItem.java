package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.*;
import zw.co.esolutions.zpas.enums.DebitOrderStatus;
import zw.co.esolutions.zpas.utilities.audit.Auditable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;


@Data
@Entity
//@Indexed
@Table(name = "debit_order_batch_item")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "debitorderbatchitem",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class DebitOrderBatchItem implements Auditable {
    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    protected String status;
    protected String sourceBICFI;
    protected String sourceAccount;
    protected BigDecimal amount;
    @Enumerated(EnumType.STRING)
    protected DebitOrderStatus itemStatus;
    protected String reference;
    protected String responseCode;
    protected String transactionReference;
    protected String recordType;
    protected String destinationSortCode;
    protected String destinationAccount;
    protected String destinationAccountType;
    protected String transactionCode;
    protected String originSortCode;
    protected String originAccount;
    protected String originAccountType;
    protected String referenceID;
    protected String destinationName;
    protected String sourceName;
    protected OffsetDateTime processingDate;
    protected String unpaidReason;
    protected String narrative;
    protected String description;
    protected String amountCurrencyCode;
    protected String originCurrencyRate ;
    protected String response;
    protected String requestStatus;
    protected String nationalId;

    @ManyToOne(targetEntity = DebitOrderBatch.class)
    protected DebitOrderBatch debitOrderBatch;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        return null;
    }

    @Override
    public String getInstanceName() {
        return null;
    }

    @Override
    public String toString() {
        return "DebitOrderBatchItem{" +
                "id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", status='" + status + '\'' +
                ", sourceBICFI='" + sourceBICFI + '\'' +
                ", sourceAccount='" + sourceAccount + '\'' +
                ", amount=" + amount +
                ", itemStatus=" + itemStatus +
                ", reference='" + reference + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", transactionReference='" + transactionReference + '\'' +
                ", recordType='" + recordType + '\'' +
                ", destinationSortCode='" + destinationSortCode + '\'' +
                ", destinationAccount='" + destinationAccount + '\'' +
                ", destinationAccountType='" + destinationAccountType + '\'' +
                ", transactionCode='" + transactionCode + '\'' +
                ", originSortCode='" + originSortCode + '\'' +
                ", originAccount='" + originAccount + '\'' +
                ", originAccountType='" + originAccountType + '\'' +
                ", referenceID='" + referenceID + '\'' +
                ", destinationName='" + destinationName + '\'' +
                ", sourceName='" + sourceName + '\'' +
                ", processingDate=" + processingDate +
                ", unpaidReason='" + unpaidReason + '\'' +
                ", narrative='" + narrative + '\'' +
                ", amountCurrencyCode='" + amountCurrencyCode + '\'' +
                ", originCurrencyRate='" + originCurrencyRate + '\'' +
                ", response='" + response + '\'' +
                ", requestStatus='" + requestStatus + '\'' +
                '}';
    }
}
