package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;


import zw.co.esolutions.zpas.enums.FrequencyCode;
import zw.co.esolutions.zpas.enums.MandateClassificationCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
////Indexed
@AnalyzerDef(name = "directdebitmandateanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class DirectDebitMandate extends Mandate implements Auditable {

    @OneToOne(targetEntity = DirectDebit.class, fetch = FetchType.LAZY)
    protected DirectDebit relatedDirectDebit;

    @Enumerated(EnumType.STRING)
    protected EntityStatus status;

    protected OffsetDateTime finalCollectionDate;

    @Enumerated(EnumType.STRING)
    protected FrequencyCode frequency;

    protected OffsetDateTime firstCollectionDate;

    @OneToOne(fetch = FetchType.LAZY)
    protected PaymentProcessing mandatePaymentType;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "collection_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "collection_amount_ccy_code")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "collection_amount_ccy_name"))
    })
    protected CurrencyAndAmount collectionAmount;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "maximum_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "maximum_amount_ccy_code")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "maximum_amount_ccy_name"))
    })
    protected CurrencyAndAmount maximumAmount;

    protected Boolean preNotification;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "value", column = @Column(name = "preNotificationThreshold"))
    })
    protected Number preNotificationThreshold;

    @Enumerated(EnumType.STRING)
    protected MandateClassificationCode classification;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "point_in_time_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "point_in_time_name"))
    })
    protected FrequencyCode pointInTime;


    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "DirectDebitMandate " + getId();
    }

    @Override
    public String toString() {
        return "DirectDebitMandate{}";
    }
}
