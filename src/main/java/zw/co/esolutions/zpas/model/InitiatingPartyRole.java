package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class InitiatingPartyRole extends PaymentPartyRole {
    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        return attributesMap;
    }

    @Override
    public String getInstanceName() {
        return this.getId();
    }
}
