package zw.co.esolutions.zpas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.ISO2ALanguageCode;

import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jnkomo on 23 Jan 2019
 */

@Data
@Entity
@AnalyzerDef(name = "systemanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class System extends RolePlayer {

    @Embedded
    @AttributeOverride(name = "date", column = @Column(name = "availability_status"))
    protected SystemAvailability availability;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "status.codeName", column = @Column(name = "system_status_status_code")),
            @AttributeOverride(name = "status.name", column = @Column(name = "system_status_status_name")),
            @AttributeOverride(name = "memberStatus.codeName", column = @Column(name = "system_status_member_code")),
            @AttributeOverride(name = "memberStatus.name", column = @Column(name = "system_status_member_name"))
    })
    protected SystemStatus status;

    @OneToOne(targetEntity = DateTimePeriod.class, mappedBy = "system", fetch = FetchType.LAZY)
    protected DateTimePeriod versionValidityPeriod;

    protected OffsetDateTime systemDateTime;

    @ManyToOne(targetEntity = Account.class, fetch = FetchType.LAZY)
    protected Account account;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "system_language_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "system_language_name"))
    })
    protected ISO2ALanguageCode systemLanguage;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "SYSTEM : " + id;
    }

    @Override
    public String toString() {
        return "System{}";
    }
}
