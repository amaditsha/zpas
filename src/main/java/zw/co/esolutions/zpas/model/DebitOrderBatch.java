package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.*;
import org.hibernate.search.bridge.builtin.EnumBridge;
import zw.co.esolutions.zpas.enums.DebitOrderStatus;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
@Entity
@Indexed
@Table(name = "debit_order_batch")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "debitorderbatchanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class DebitOrderBatch implements Auditable {
    @Id
    @Column(length = 50)
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    protected String status;
    protected String fiId;

    protected OffsetDateTime debitOrderDueDate;

    @Field(index = org.hibernate.search.annotations.Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "debitorderbatchanalyzer")
    protected String batchNumber;

    protected String description;
    protected String narrative;
    protected BigDecimal totalAmount;
    protected Integer totalCount;

    @Field(index = org.hibernate.search.annotations.Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Analyzer(definition = "debitorderbatchanalyzer")
    protected String targetAccount;

    protected String targetAccountName;
    protected Integer successfulTotalCount;
    protected BigDecimal successfulTotalAmount;
    protected String trailerId;
    protected String currencyCode;
    protected String headerID;
    protected OffsetDateTime processingDate;
    protected String senderID;
    protected String receiverID;
    protected String fileID;
    protected String workCode;
    protected String fileName;
    protected String batchResponseCode;
    protected String clientId;
    protected String clientName;
    protected String version;

   // @Field(bridge=@FieldBridge(impl= EnumBridge.class))
    @Enumerated(EnumType.STRING)
   // @Analyzer(definition = "debitorderbatchanalyzer")
    protected DebitOrderStatus debitOrderStatus;

    protected String bankSortCode;

    @OneToMany(targetEntity = DebitOrderBatchItem.class, mappedBy="debitOrderBatch", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    protected List<DebitOrderBatchItem> debitOrderBatchItems;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        return null;
    }

    @Override
    public String getInstanceName() {
        return null;
    }

    @Override
    public String toString() {
        return "DebitOrderBatch{" +
                "batchNumber='" + batchNumber + '\'' +
                ", description='" + description + '\'' +
                ", totalAmount=" + totalAmount +
                ", targetAccount='" + targetAccount + '\'' +
                ", fileID='" + fileID + '\'' +
                ", clientName='" + clientName + '\'' +
                ", bankSortCode='" + bankSortCode + '\'' +
                '}';
    }
}
