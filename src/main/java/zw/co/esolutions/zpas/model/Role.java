package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.esolutions.zpas.enums.PartyRoleCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
@Entity
@Data
@ToString(exclude = { "rolePlayer" })
@Table(name = "role")
public abstract class Role implements Auditable {
    @Id
    protected String id;

    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @JsonIgnore
    @ManyToMany(targetEntity = RolePlayer.class, cascade = {PERSIST, MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "role_role_player",
            joinColumns = {
                    @JoinColumn(name = "role_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "role_player_id")})
    protected List<RolePlayer> player;

    @ManyToOne(targetEntity = ContactPersonRole.class, fetch = FetchType.LAZY)
    protected ContactPersonRole contactPersonRole;

    @ManyToOne(targetEntity = Entry.class, fetch = FetchType.LAZY)
    protected Entry entry;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST})
    protected CustomParty customParty;

    private boolean hasCustomParty;

    @Enumerated(EnumType.STRING)
    protected PartyRoleCode partyRoleCode;

    @Override
    public String toString() {
        return "Role{}";
    }

    public void addPlayer(RolePlayer rolePlayer) {
        if (this.player == null)
            this.player = new ArrayList<>();
        this.getPlayer().add(rolePlayer);
    }
}
