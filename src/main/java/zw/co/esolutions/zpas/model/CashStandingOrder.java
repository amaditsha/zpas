package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

import zw.co.esolutions.zpas.enums.DebitCreditCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class CashStandingOrder extends StandingOrder {

    protected Boolean zeroSweepIndicator;
    @Enumerated(EnumType.STRING)
    protected DebitCreditCode creditDebitIndicator;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "floorAmount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "floorAmountCurrency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "floorAmountCurrency_name"))
    })
    protected CurrencyAndAmount floorAmount;

    @OneToOne(mappedBy = "cashStandingOrder")
    protected CashAccount cashAccount;

    @OneToOne(targetEntity = CreditTransfer.class, mappedBy = "relatedStandingOrder")
    protected CreditTransfer creditTransfer;

    @ManyToOne(targetEntity = CashManagementService.class)
    protected CashManagementService relatedCashServices;

    @Override
    public String getInstanceName() {
        return "CASH STANDING ORDER: " + id;
    }

    @Override
    public String toString() {
        return "CashStandingOrder{}";
    }
}
