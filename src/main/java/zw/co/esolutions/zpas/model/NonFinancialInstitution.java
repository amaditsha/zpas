package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.model.taxclearance.TaxClearance;

import javax.persistence.*;
import java.util.List;

/**
 * Created by tanatsa on 23 Jan 2019
 */
@Audited

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Indexed
@AnalyzerDef(name = "nonfinanciainstitutionanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class NonFinancialInstitution extends Organisation {
    @OneToMany(mappedBy = "nonFinancialInstitution", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    protected List<TaxClearance> taxClearances;
    @Column
    protected Boolean enableDebitOrder;
    @Column
    protected Boolean enableAPIIntegration;

}
