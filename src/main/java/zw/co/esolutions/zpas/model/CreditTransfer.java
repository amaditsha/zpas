package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.util.Objects;

@Entity
@Data
@ToString(callSuper = true, exclude = {"relatedStandingOrder"})
public class CreditTransfer extends IndividualPayment {

    protected Boolean standingOrder;

    @OneToOne(targetEntity = CashStandingOrder.class, fetch = FetchType.LAZY)
    protected CashStandingOrder relatedStandingOrder;

//    @Override
//    public String toString() {
//        return "CreditTransfer{" +
//                "standingOrder=" + standingOrder +
//                ", relatedStandingOrder=" + relatedStandingOrder +
//                ", bulkPayment=" + bulkPayment +
//                ", id='" + id + '\'' +
//                ", dateCreated=" + dateCreated +
//                ", lastUpdated=" + lastUpdated +
//                ", entityStatus=" + entityStatus +
//                ", entityVersion=" + entityVersion +
//                '}';
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObligationFulfilment that = (ObligationFulfilment) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
