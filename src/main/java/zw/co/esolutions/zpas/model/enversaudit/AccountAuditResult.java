package zw.co.esolutions.zpas.model.enversaudit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;
import zw.co.esolutions.zpas.model.CashAccount;

import java.io.Serializable;

@Data
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AccountAuditResult implements Serializable {
    private CashAccount cashAccount;
    private AuditedRevisionEntity auditedRevisionEntity;

    public AccountAuditResult(CashAccount cashAccount, AuditedRevisionEntity auditedRevisionEntity) {
        this.cashAccount = cashAccount;
        this.auditedRevisionEntity = auditedRevisionEntity;
    }
}
