package zw.co.esolutions.zpas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;



import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mabuza on 28 Jan 2019
 */

@Data
@Entity
//@Indexed
@Table(name = "debit_authorisation")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "debitauthorisationanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class DebitAuthorisation implements Auditable {

    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    protected OffsetDateTime valueDateToDebit;

    protected Boolean debitAuthorisationDecision;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount_to_debit_amount")),
            @AttributeOverride(name = "currency.currency", column = @Column(name = "amount_to_debit_currency"))
    })
    protected CurrencyAndAmount amountToDebit;

    protected String reason;
    @OneToOne(targetEntity = Payment.class, fetch = FetchType.LAZY)
    protected Payment authorisedReturn;
    @OneToOne(targetEntity = PaymentInvestigationCaseResolution.class, fetch = FetchType.LAZY)
    protected PaymentInvestigationCaseResolution relatedInvestigationCaseResolution;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "DEBIT_AUTHORISATION" + id;
    }

    @Override
    public String toString() {
        return "DebitAuthorisation{}";
    }
}
