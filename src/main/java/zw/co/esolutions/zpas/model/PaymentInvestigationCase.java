package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import zw.co.esolutions.zpas.enums.CancellationReasonCode;
import zw.co.esolutions.zpas.enums.UnableToApplyIncorrectInfoCode;
import zw.co.esolutions.zpas.enums.UnableToApplyMissingInformationV2Code;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Indexed
@AnalyzerDef(name = "payment_investigation_case",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class PaymentInvestigationCase extends  InvestigationCase {

    @JsonIgnore
    @OneToMany(mappedBy = "relatedInvestigationCase", fetch = FetchType.LAZY)
    protected List<CashEntry> underlyingCashEntry;

    @JsonIgnore
    @OneToMany(targetEntity = PaymentStatus.class, mappedBy = "relatedInvestigationCase", cascade = {PERSIST, MERGE}, fetch = FetchType.LAZY)
    protected List<PaymentStatus> paymentStatus;

    @JsonIgnore
    @OneToMany(targetEntity = Payment.class, mappedBy = "relatedInvestigationCase",  cascade = {PERSIST, MERGE}, fetch = FetchType.LAZY)
    protected List<Payment> underlyingPayment;

    @OneToOne(targetEntity = PaymentExecution.class, mappedBy = "relatedInvestigationCase", cascade = {MERGE}, fetch = FetchType.LAZY)
    protected PaymentExecution underlyingInstruction;

    @Enumerated(EnumType.STRING)
    protected CancellationReasonCode cancellationReason;

    protected Boolean missingCoverIndication;

    protected UnableToApplyIncorrectInfoCode incorrectInformationReason;

    protected UnableToApplyMissingInformationV2Code missingInformationReason;

    @Column(length = 35)
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String caseType;


    @Override
    public String toString() {
        return "PaymentInvestigationCase{" +
                "cancellationReason=" + cancellationReason +
                ", missingCoverIndication=" + missingCoverIndication +
                ", incorrectInformationReason=" + incorrectInformationReason +
                ", missingInformationReason=" + missingInformationReason +
                ", caseType='" + caseType + '\'' +
                ", id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                ", assignmentIdentification='" + assignmentIdentification + '\'' +
                ", creationDateTime=" + creationDateTime +
                ", identification='" + identification + '\'' +
                ", reassignment=" + reassignment +
                '}';
    }

    public void addUnderlyingPayment(Payment payment) {
        if (this.underlyingPayment == null)
            this.underlyingPayment = new ArrayList<>();
        this.underlyingPayment.add(payment);
    }
}
