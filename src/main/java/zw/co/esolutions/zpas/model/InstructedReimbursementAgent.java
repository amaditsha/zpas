package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.Entity;

/**
 * Created by mabuza on 23 Jan 2019
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class InstructedReimbursementAgent extends CashSettlementInstructionPartyRole {
    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "INSTRUCTED_REIMBURSEMENT_AGENT : " + id;
    }
}
