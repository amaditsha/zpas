package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;



import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "adjustment")
//@Indexed
@AnalyzerDef(name = "adjustmentanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class Adjustment implements Auditable {

    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @Embedded
    @AttributeOverrides(value = {
                    @AttributeOverride(name = "amount", column = @Column(name = "amount_amount")),
                    @AttributeOverride(name = "currency", column = @Column(name = "amount_currency"))
            })
    protected CurrencyAndAmount amount;

    protected BigDecimal chargeRate;

    @Enumerated(EnumType.STRING)
    protected TaxationBasisCode calculationMethod;

    @JsonIgnore
    @OneToMany(targetEntity = Payment.class, mappedBy = "adjustments", fetch = FetchType.LAZY)
    protected List<Payment> payment;

    @Enumerated(EnumType.STRING)
    protected AdjustmentDirectionCode direction;

    protected String reason;

    @OneToOne(targetEntity = LineItem.class, fetch = FetchType.LAZY)
    protected LineItem relatedLineItem;

    protected Boolean allowanceChargeIndicator;

    // protected Price price;

    protected Boolean chargeIndicator;

    @Enumerated(EnumType.STRING)
    protected AdjustmentTypeCode type;

   // protected Price price;
    // protected CollateralManagement collateralManagement;

    @ManyToOne(targetEntity = Balance.class)
    protected Balance adjustedBalance;

    //protected List<ChargePartyRole> chargesPartyRole;

    @OneToOne(targetEntity = DateTimePeriod.class, mappedBy = "relatedAdjustment", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    protected DateTimePeriod effectivePeriod;

    @OneToOne(targetEntity = CurrencyExchange.class, fetch = FetchType.LAZY)
    protected CurrencyExchange currencyExchange;

    @OneToOne(targetEntity = Tax.class, fetch = FetchType.LAZY)
    protected Tax tax;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @PrePersist
    public void setCurrencyExchangeId(){
        this.id = GenerateKey.generateEntityId();
        this.entityStatus = EntityStatus.ACTIVE;
    }

    @Override
    public String toString() {
        return "Adjustment{}";
    }
}
