package zw.co.esolutions.zpas.model.payments;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;

/**
 * Created by alfred on 28 August 2019
 */
@Data
@Entity
@Table(name = "extra_detail")
@AnalyzerDef(name = "extra_detail_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class ExtraDetail {
    @Id
    @Column(length = 50)
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    protected String detail;

    @ManyToOne(targetEntity = Payment.class, fetch = FetchType.LAZY)
    protected Payment payment;
}
