package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;


import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Data
@Table(name = "generic_identification", indexes = {
        @Index(name = "generic_identification_entitystatus", columnList = "entityStatus"),
        @Index(name = "generic_identification_datecreated", columnList = "dateCreated"),
        @Index(name = "generic_identification_lastupdated", columnList = "lastUpdated"),
})
public class GenericIdentification implements Auditable {
    @Id
    protected String id;

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime lastUpdated;

    protected String identification;

    protected OffsetDateTime issueDate;

    protected OffsetDateTime expiryDate;

    @OneToOne(targetEntity = CashAccountService.class, fetch = FetchType.LAZY)
    protected CashAccountService relatedCashAccountService;

    @OneToOne(mappedBy = "type", fetch = FetchType.LAZY)
    protected Account account;

    @ManyToOne(targetEntity = Scheme.class, cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    protected Scheme scheme;

    @OneToOne(targetEntity = RegisteredContract.class, fetch = FetchType.LAZY)
    protected RegisteredContract relatedCertificate;

    @JsonIgnore
    @OneToMany(mappedBy = "identification", fetch = FetchType.LAZY)
    protected List<ContactPoint> identificationForContactPoint;

    @OneToOne(mappedBy = "proprietaryIdentification", fetch = FetchType.LAZY)
    protected AccountIdentification identificationForAccount;

    @OneToOne(mappedBy = "costReferencePattern", fetch = FetchType.LAZY)
    protected AccountIdentification identificationForAccountCostReferencePattern;

    @ManyToOne(targetEntity = Location.class, fetch = FetchType.LAZY)
    protected Location identifiedLocation;

    @OneToOne(targetEntity = PartyIdentificationInformation.class, fetch = FetchType.LAZY)
    protected PartyIdentificationInformation relatedPartyIdentification;

    @JsonIgnore
    @ManyToMany(targetEntity = BankTransaction.class, mappedBy = "proprietaryIdentification", fetch = FetchType.LAZY)
    protected List<BankTransaction> identificationForBankTransaction;

    @OneToOne(targetEntity = Document.class, fetch = FetchType.LAZY)
    protected Document identifiedDocument;

    @ManyToOne(targetEntity = StatusReason.class, fetch = FetchType.LAZY)
    protected StatusReason relatedStatusReason;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "GENERIC IDENTIFICATION: " + id;
    }

    @PrePersist
    private void setDefaultEntityAttributes(){
        this.id = GenerateKey.generateEntityId();
        this.entityStatus = EntityStatus.DRAFT;
    }

    @Override
    public String toString() {
        return "GenericIdentification{}";
    }
}
