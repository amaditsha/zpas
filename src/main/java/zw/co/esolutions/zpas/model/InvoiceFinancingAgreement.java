package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class InvoiceFinancingAgreement extends Agreement {
    @JsonIgnore
    @ManyToMany(targetEntity = InvoiceFinancingPartyRole.class)
    @JoinTable(name = "invoice_financing_agreement_party_role",
            joinColumns = @JoinColumn(name = "invoice_financing_agreement_id"),
            inverseJoinColumns = @JoinColumn(name = "invoice_financing_party_role_id")
    )
    protected List<InvoiceFinancingPartyRole> invoiceFinancingPartyRole;

    @OneToOne(fetch = FetchType.LAZY)
    protected CashEntry resultingCashEntry;

    @JsonIgnore
    @OneToMany(targetEntity = Invoice.class, mappedBy = "invoiceFinancingTransaction", fetch = FetchType.LAZY)
    protected List<Invoice> invoice;

    @Override
    public String getInstanceName() {
        return "INVOICE FINANCING AGREEMENT: " + getId();
    }

    @Override
    public String toString() {
        return "InvoiceFinancingAgreement{}";
    }
}
