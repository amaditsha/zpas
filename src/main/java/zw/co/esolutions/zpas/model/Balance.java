package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import zw.co.esolutions.zpas.enums.BalanceTypeCode;
import zw.co.esolutions.zpas.enums.DebitCreditCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Table(name = "balance")
public abstract class Balance implements Auditable {
    @Id
    protected String id;

    @Enumerated(EnumType.STRING)
    protected BalanceTypeCode type;

    protected OffsetDateTime valueDate;

    @Enumerated(EnumType.STRING)
    protected DebitCreditCode creditDebitIndicator;

    protected OffsetDateTime calculationDate;

    @JsonIgnore
    @OneToMany(mappedBy = "adjustedBalance", fetch = FetchType.LAZY)
    protected List<Adjustment> adjustment;

    @ManyToOne(targetEntity = Account.class, fetch = FetchType.LAZY)
    protected Account account;

    @JsonIgnore
    @ManyToMany(targetEntity = Entry.class, fetch = FetchType.LAZY)
    @JoinTable(name = "balance_entry",
            joinColumns = @JoinColumn(name = "balance_id"),
            inverseJoinColumns = @JoinColumn(name = "entry_id")
    )
    protected List<Entry> balanceEntry;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "BALANCE: " + id;
    }

    @Override
    public String toString() {
        return "Balance{}";
    }
}
