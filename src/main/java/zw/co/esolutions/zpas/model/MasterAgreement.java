package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CountryCode;
import zw.co.esolutions.zpas.enums.AgreementFrameworkCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//@Indexed
@AnalyzerDef(name = "masteragreementanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class MasterAgreement extends Agreement implements Auditable {

    @ManyToOne(targetEntity = CollateralAgreement.class, fetch = FetchType.LAZY)
    protected CollateralAgreement collateralAgreement;

    @Enumerated(EnumType.STRING)
    protected AgreementFrameworkCode masterAgreementType;

//    Nature of the agreement, eg, ISDA Master Agreement or bilateral agreement.
//    protected List<zw.co.esolutions.zpas.tools20022.repository.entity.Trade> governedTrades;

    @ManyToOne(targetEntity = Contract.class, fetch = FetchType.LAZY)
    protected Contract governedContract;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "governing_law_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "governing_law_name"))
    })
    protected CountryCode governingLaw;



    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "MASTERAGREEMENT " + getId();
    }

    @Override
    public String toString() {
        return "MasterAgreement{}";
    }
}
