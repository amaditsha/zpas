package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;



import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
//Indexed
@Table(name = "signature_condition", indexes = {
        @Index(name = "signature_condition_status", columnList = "entityStatus"),
        @Index(name = "signature_condition_date_created", columnList = "dateCreated"),
        @Index(name = "signature_condition_last_updated", columnList = "lastUpdated")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "signature_conditioncontractanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class SignatureCondition implements Auditable {
    @Id
    protected String id;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    protected BigDecimal requiredSignatureNumber;

    protected Boolean signatoryRightIndicator;

    @ManyToOne(targetEntity = Mandate.class, fetch = FetchType.LAZY)
    protected Mandate mandate;

    protected Boolean signatureOrderIndicator;

    protected String signatureOrder;

    protected String description;

    @JsonIgnore
    @OneToMany(mappedBy = "conditions", fetch = FetchType.LAZY)
    protected List<Signature> signature;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "SignatureCondition " + getId();
    }

    @Override
    public String toString() {
        return "SignatureCondition{}";
    }

    @PrePersist
    public void init() {
        setId(GenerateKey.generateEntityId());
        setEntityStatus(EntityStatus.PENDING_APPROVAL);
        setDateCreated(OffsetDateTime.now());
    }
}
