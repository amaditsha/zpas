package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;



import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Data
public class Scheme {
    @Id
    protected String id;

    @Column
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    protected String nameShort;

    protected String code;

    @JsonIgnore
    @OneToMany(targetEntity = GenericIdentification.class, mappedBy = "scheme", fetch = FetchType.LAZY)
    protected List<GenericIdentification> identification;

    @OneToOne(fetch = FetchType.LAZY)
    protected CreditorRole creditorRole;

    protected String version;

    protected String nameLong;

    protected String description;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "domain_value_code"))
    protected String domainValueCode;

    protected String domainValueName;

    @Override
    public String toString() {
        return "Scheme{}";
    }
}
