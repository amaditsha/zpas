package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;


import zw.co.esolutions.zpas.enums.PaymentMethodCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Created by alfred on 28 Jan 2019
 */
@Entity
@Data
public class CreditInstrument {
    @Id
    protected String id;

    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @JsonIgnore
    @ManyToMany(targetEntity = Payment.class, mappedBy = "creditMethod", fetch = FetchType.LAZY)
    protected List<Payment> relatedPayment;

    @Enumerated(EnumType.STRING)
    protected PaymentMethodCode method;

    protected String creditInstrumentIdentification;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "net_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "net_amount_ccy_code")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "net_amount_ccy_name"))
    })
    protected CurrencyAndAmount netAmount;

    protected OffsetDateTime deadline;

    @Override
    public String toString() {
        return "CreditInstrument{" +
                "id='" + id + '\'' +
                ", entityStatus=" + entityStatus +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityVersion=" + entityVersion +
                ", method=" + method +
                ", creditInstrumentIdentification='" + creditInstrumentIdentification + '\'' +
                ", netAmount=" + netAmount +
                ", deadline=" + deadline +
                '}';
    }
}
