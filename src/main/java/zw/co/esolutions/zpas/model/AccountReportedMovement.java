package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Entity
@Data
@Table(name = "account_reported_movement", indexes = {
        @Index(name = "account_reported_movement_entitystatus", columnList = "entityStatus"),
        @Index(name = "account_reported_movement_datecreated", columnList = "dateCreated"),
        @Index(name = "account_reported_movement_lastupdated", columnList = "lastUpdated")
})
public class AccountReportedMovement implements Auditable {
    @Id
    protected String id;

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "monthlyPaymentValue")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "monthlyPaymentValueCurrency_codeName")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "monthlyPaymentValueCurrency_name"))
    })
    protected CurrencyAndAmount monthlyPaymentValue;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "monthlyReceivedValue")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "monthlyReceivedValueCurrency_codeName")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "monthlyReceivedValueCurrency_name"))
    })
    protected CurrencyAndAmount monthlyReceivedValue;

    protected String monthlyTransactionNumber;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "averageBalance")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "averageBalanceCurrency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "averageBalanceCurrency_name"))
    })
    protected CurrencyAndAmount averageBalance;

    @ManyToOne(targetEntity = CashAccount.class)
    protected CashAccount reportedCashAccount;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", getLastUpdated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", getEntityStatus().name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "ACCOUNT REPORTED MOVEMENT: " + getId();
    }

    @Override
    public String toString() {
        return "AccountReportedMovement{}";
    }
}
