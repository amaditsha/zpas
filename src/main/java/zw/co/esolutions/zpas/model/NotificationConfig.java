package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.search.annotations.*;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

@AnalyzerDef(name = "notificationconfiganalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class NotificationConfig implements Auditable {
    @Id
    @Column(length = 50)
    private String id;

    @CreationTimestamp
    private OffsetDateTime dateCreated;


    @Field
    @Analyzer(definition = "notificationconfiganalyzer")
    @Enumerated(EnumType.STRING)
    private EntityStatus entityStatus;

    @Field
    @Analyzer(definition = "notificationconfiganalyzer")
    @Enumerated(EnumType.STRING)
    private NotificationType notificationType;

    @Column
    private Boolean email;
    @Column
    private Boolean sms;
    @Column
    private Boolean bell;
    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = Party.class)
    private Party party;

    @Version
    protected long version;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("status", getEntityStatus().name());
        attributesMap.put("dateCreated", dateCreated.toString());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @PrePersist
    public void init() {
        if(getId() == null) {
            setId(GenerateKey.generateEntityId());
        }
        if(getEntityStatus() == null) {
            setEntityStatus(EntityStatus.PENDING_APPROVAL);
        }
        if(getDateCreated() == null) {
            setDateCreated(OffsetDateTime.now());
        }
    }

}

