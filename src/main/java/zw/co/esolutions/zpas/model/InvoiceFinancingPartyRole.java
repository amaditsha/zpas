package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class InvoiceFinancingPartyRole extends InvoicePartyRole{
    @Id
    private String id;

    @JsonIgnore
    @OneToMany(mappedBy = "relatedInvoiceFinancingPartyRole", fetch = FetchType.LAZY)
    protected List<CashAccount> cashAccount;

    @JsonIgnore
    @ManyToMany(mappedBy = "invoiceFinancingPartyRole", fetch = FetchType.LAZY)
    protected List<InvoiceFinancingAgreement> invoiceFinancingTransaction;

    @Override
    public String getInstanceName() {
        return "INVOICE FINANCING PARTY ROLE: " + id;
    }

    @Override
    public String toString() {
        return "InvoiceFinancingPartyRole{}";
    }
}
