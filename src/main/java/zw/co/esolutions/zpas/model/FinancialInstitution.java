package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by tanatsa on 23 Jan 2019
 */
@Audited
@EqualsAndHashCode(callSuper = true)
//@Indexed
@Entity
@Data
@AnalyzerDef(name = "financiainstitutionanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class FinancialInstitution extends Organisation {
    protected String virtualSuffix;

    @NotAudited
    @OneToMany(mappedBy = "financialInstitution" ,fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    protected List<AccountRegex> accountRegex;

    @Override
    public String toString() {
        return "FinancialInstitution{" +
                "purpose='" + purpose + '\'' +
                ", registrationDate=" + registrationDate +
                '}';
    }


}
