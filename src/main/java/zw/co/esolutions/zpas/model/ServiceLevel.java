package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import zw.co.esolutions.zpas.enums.SWIFTServiceLevelCode;
import zw.co.esolutions.zpas.enums.ServiceLevelCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Data
//Indexed
@Table(name = "service_level", indexes = {
        @Index(name = "svc_level_entity_status", columnList = "entityStatus")
})
@AnalyzerDef(name = "servicelevelanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class ServiceLevel {
    @Id
    protected String id;

    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @OneToOne(targetEntity = PaymentProcessing.class, mappedBy = "serviceLevel", fetch = FetchType.LAZY)
    protected PaymentProcessing paymentProcessing;

    @Enumerated(EnumType.STRING)
    protected ServiceLevelCode code;

    @Enumerated(EnumType.STRING)
    protected SWIFTServiceLevelCode other;

    protected String bilateral;

    @Override
    public String toString() {
        return "ServiceLevel{}";
    }
}
