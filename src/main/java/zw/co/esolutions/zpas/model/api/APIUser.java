package zw.co.esolutions.zpas.model.api;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.model.RolePlayer;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "api_user ")
@AnalyzerDef(name = "api_user_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class APIUser {
    @Id
    protected String id;

    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdate;

    protected String clientId;

    protected String clientFolder;

    @OneToOne
    protected RolePlayer apiRolePlayer;

    @Override
    public String toString() {
        return "APIUser{" +
                "clientId='" + clientId + '\'' +
                ", clientFolder='" + clientFolder + '\'' +
                ", apiRolePlayer=" + apiRolePlayer +
                '}';
    }
}
