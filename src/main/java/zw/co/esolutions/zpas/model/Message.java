package zw.co.esolutions.zpas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
//@Indexed
@Table(name = "message",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"originalMsgId", "uuid"})
        })
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class Message implements Auditable {

    @Id
    @Column(length = 50)
    private String id;

    @CreationTimestamp
    private OffsetDateTime dateCreated;

    @Column
    private String uuid;

    @Column
    private String originalMsgId;

    @Column(columnDefinition = "TEXT")
    private String payload;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("dateCreated", getDateCreated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("uuid", getUuid());
        attributesMap.put("originalMsgId", getOriginalMsgId());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "Message: " + getUuid();
    }
}
