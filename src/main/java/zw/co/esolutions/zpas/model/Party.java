package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by tanatsa on 23 Jan 2019
 */
@Audited
//Indexed
@EqualsAndHashCode(callSuper = true, exclude = {"contactPoint", "residence", "domicile"})
@Data
@Entity
public abstract class Party extends RolePlayer {

    @JsonIgnore
    @OneToMany(mappedBy = "relatedParty", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    protected List<ContactPoint> contactPoint;

    @JsonIgnore
    @OneToMany(mappedBy = "identifiedParty", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected List<PartyIdentificationInformation> identification;

    @OneToOne(targetEntity = Tax.class, cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @NotAudited
    protected Tax taxationConditions;

    @OneToOne(fetch = FetchType.LAZY)
    protected Location domicile;

    @JsonIgnore
    @OneToMany(mappedBy = "domiciledParty", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    protected List<Location> residence;

    @OneToOne(mappedBy = "authorisedParty", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @NotAudited
    protected PowerOfAttorney powerOfAttorney;

   /* @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "credit_quality_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "credit_quality_name"))
    })
    protected CreditQualityCode creditQuality;*/

    @JsonIgnore
    @OneToMany(mappedBy = "linkedBeneficiaryId", fetch = FetchType.LAZY)
    @NotAudited
    protected List<Beneficiary> beneficiaries;

    @JsonIgnore
    @NotAudited
    @OneToMany(mappedBy = "linkedParty", fetch = FetchType.LAZY)
    protected List<PartyMerchantAccount> partyMerchantAccounts;

    @JsonIgnore
    @OneToMany(targetEntity = Merchant.class, fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "linkedMerchantId")
    @NotAudited
    protected List<Merchant> merchants;

     @JsonIgnore
    @OneToMany(targetEntity = NotificationConfig.class, fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "party")
    @NotAudited
     protected List<NotificationConfig> notificationConfigs;

    @OneToOne(targetEntity = TaxAuthority.class, fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "linkedPartyTaxAuthority")
    @NotAudited
    protected TaxAuthority linkedTaxAuthority;

    public String getPersonIdentificationHelper() {
        if (this instanceof Person) {
            Person debtorPerson = (Person) this;
            List<PersonIdentification> personIdentification1 = debtorPerson.getPersonIdentification();
            return Optional.ofNullable(personIdentification1).orElse(new ArrayList<>()).stream()
                    .map(personIdentification -> Optional.ofNullable(personIdentification.getIdentityCardNumber()).orElse(""))
                    .findFirst().orElse("");
        } else {
            return "";
        }
    }

    public String getOrganisationIdentificationHelper() {
        if (this instanceof Organisation) {
            Organisation debtorOrganisation = (Organisation) this;
            return debtorOrganisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getAnyBIC)
                    .findFirst().orElse("");
        } else {
            return "";
        }
    }

    protected String virtualId;

    protected String registrationFI;

    @Override

    public String toString() {
        return "Party{}";
    }

    public String getName() {
        final StringBuilder nameSb = new StringBuilder();
        this.getIdentification().stream().findFirst().ifPresent(partyIdentificationInformation -> {
            partyIdentificationInformation.getPartyName().stream().findFirst().ifPresent(partyName -> {
                final String personFullName = partyName.getName();
                nameSb.append(personFullName);
            });
        });
        return nameSb.toString();
    }

    public String getShortName() {
        return "";
    }

    public String getEmailAddress() {
        final StringBuilder stringBuilder = new StringBuilder();
        getContactPoint().stream().filter(cp -> cp instanceof ElectronicAddress)
                .map(cp -> (ElectronicAddress) cp).findFirst().ifPresent(electronicAddress -> {
            stringBuilder.append(electronicAddress.getEmailAddress());
        });
        return stringBuilder.toString();
    }

    public String getPhysicalAddress() {
        final StringBuilder stringBuilder = new StringBuilder();
        getContactPoint().stream().filter(cp -> cp instanceof PostalAddress)
                .map(pa -> (PostalAddress) pa).findFirst().ifPresent(postalAddress -> {
            stringBuilder.append(String.format("%s %s %s %s %s",
                    postalAddress.getPostOfficeBox(),
                    postalAddress.getStreetName(),
                    postalAddress.getTownName(),
                    postalAddress.getProvince(),
                    postalAddress.getCountry()));
        });
        return stringBuilder.toString();
    }

    public String getBusinessPartnerNumber() {
        return getIdentification().stream().findFirst().map(partyIdentificationInformation -> {
            return partyIdentificationInformation.getTaxIdentificationNumber();
        }).orElse("");
    }
}
