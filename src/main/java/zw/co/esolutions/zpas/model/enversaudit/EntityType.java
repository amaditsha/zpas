package zw.co.esolutions.zpas.model.enversaudit;

import javax.persistence.*;

@Entity(name = "EntityType")
public class EntityType {
    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    private AuditedRevisionEntity revision;

    private String entityClassName;

    private EntityType() {
    }

    public EntityType(AuditedRevisionEntity revision, String entityClassName) {
        this.revision = revision;
        this.entityClassName = entityClassName;
    }


}
