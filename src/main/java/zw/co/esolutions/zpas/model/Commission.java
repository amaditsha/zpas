package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

import zw.co.esolutions.zpas.enums.TaxationBasisCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Commission extends Adjustment implements Auditable {
    @JsonIgnore
    @OneToMany(mappedBy = "commission", fetch = FetchType.LAZY)
    protected List<CashAccount> account;

    @OneToOne(targetEntity = CorporateActionFeesAndCharges.class, fetch = FetchType.LAZY)
    protected CorporateActionFeesAndCharges corporateActionFeesAndCharges;

    //protected CommissionWaiver commissionWaiving;

    // protected Trade trade;

    //  protected CommissionTypeV2Code commissionType;

    @Enumerated(EnumType.STRING)
    protected TaxationBasisCode basis;

    protected String commercialAgreementReference;

    protected OffsetDateTime calculationDate;

    protected Boolean rate;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "currency.name", column = @Column(name = "commission_amount_currency_code")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "commission_amount_currency_code_name")),
            @AttributeOverride(name = "amount", column = @Column(name = "commission_amount_amount"))
    })
    protected CurrencyAndAmount commissionAmount;

    //protected List<Broker> broker;

    // protected List<CommissionPartyRole> commissionPartyRole;

    // protected SecuritiesQuoteVariable relatedQuote;

    protected Boolean splitRate;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "name", column = @Column(name = "currency_name")),
            @AttributeOverride(name = "codeName", column = @Column(name = "currency_code_name"))
    })
    protected CurrencyCode currency;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("dateCreated", dateCreated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "Commission{}";
    }
}
