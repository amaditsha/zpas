package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.*;
import java.util.List;

/**
 * Created by mabuza on 31 Jan 2019
 */
@Entity
@Data
//@Indexed
@AnalyzerDef(name = "bankoperationanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class BankOperation extends CashAccountService {
    @OneToOne(targetEntity = OperationThreshold.class, mappedBy = "bankOperation", fetch = FetchType.LAZY)
    protected OperationThreshold operationThreshold;

    @OneToOne(targetEntity = BankTransaction.class, mappedBy = "bankOperation", fetch = FetchType.LAZY)
    protected BankTransaction operationType;

    @JsonIgnore
    @OneToMany(targetEntity = DateTimePeriod.class, mappedBy = "bankOperation", fetch = FetchType.LAZY)
    protected List<DateTimePeriod> applicablePeriod;

    @Override
    public String getInstanceName() {
        return "BANK_OPERATION: " + getId();
    }

    @Override
    public String toString() {
        return "BankOperation{}";
    }
}
