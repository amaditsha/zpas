package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.InstructionCode;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
//Indexed
@AnalyzerDef(name = "paymentinstructionalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class PaymentInstruction extends PaymentExecution {

    @OneToOne(targetEntity = DateTimePeriod.class, mappedBy = "paymentInstruction", fetch = FetchType.LAZY)
    protected DateTimePeriod processingValidityTime;

    @Enumerated(EnumType.STRING)
    protected InstructionCode instructionForNextAgent;

    @OneToOne(targetEntity = CashSettlement.class, mappedBy = "relatedPaymentInstruction", fetch = FetchType.LAZY)
    protected CashSettlement settlementInstruction;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "clearing_charge_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "clearing_charge_ccy_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "clearing_charge_ccy_name"))
    })
    protected CurrencyAndAmount clearingChargeAmount;

    @OneToOne(targetEntity = StandingOrder.class, fetch = FetchType.LAZY)
    protected StandingOrder standingOrder;

    @JsonIgnore
    @OneToMany(targetEntity = PaymentExecution.class, mappedBy = "next")
    protected List<PaymentExecution> previous;

    @Override
    public String toString() {
        return "PaymentInstruction{}";
    }
}
