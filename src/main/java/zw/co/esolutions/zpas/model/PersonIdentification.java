package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;


import javax.persistence.*;
import java.util.List;

@Audited
@EqualsAndHashCode(callSuper = true, exclude = {"personName", "person"})
@Entity
@Data
//Indexed
@AnalyzerDef(name = "personidentificationalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class PersonIdentification extends PartyIdentificationInformation{

    protected String socialSecurityNumber;

    @ManyToOne(targetEntity = Person.class, fetch = FetchType.LAZY)
    protected Person person;

    @JsonIgnore
    @OneToMany(mappedBy = "identification", fetch = FetchType.LAZY, targetEntity = PersonName.class, cascade = {CascadeType.PERSIST})
    protected List<PersonName> personName;

    protected String driversLicenseNumber;

    protected String alienRegistrationNumber;

    protected String passportNumber;

    protected String identityCardNumber;

    protected String employerIdentificationNumber;

    @Override
    public String toString() {
        return "PersonIdentification{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", driversLicenseNumber='" + driversLicenseNumber + '\'' +
                ", alienRegistrationNumber='" + alienRegistrationNumber + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                ", identityCardNumber='" + identityCardNumber + '\'' +
                ", employerIdentificationNumber='" + employerIdentificationNumber + '\'' +
                '}';
    }
}
