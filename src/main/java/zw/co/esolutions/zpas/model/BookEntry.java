package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Created by mabuza on 23 Jan 2019
 */
@Entity
@Data
public class BookEntry {
    @Id
    private String id;
    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @JsonIgnore
    @OneToMany(mappedBy = "relatedBookEntry", fetch = FetchType.LAZY)
    protected List<CashEntry> cashEntry;

    @JsonIgnore
    @OneToMany(mappedBy = "debitRelatedBookEntry", fetch = FetchType.LAZY)
    protected List<CashEntry> debitEntry;

    @JsonIgnore
    @OneToMany(mappedBy = "creditRelatedBookEntry", fetch = FetchType.LAZY)
    protected List<CashEntry> creditEntry;

    protected Boolean transferAdvice;

    @OneToOne(targetEntity = CashSettlement.class, mappedBy = "bookEntry", fetch = FetchType.LAZY)
    protected CashSettlement relatedSettlementInstruction;

    @Override
    public String toString() {
        return "BookEntry{}";
    }
}
