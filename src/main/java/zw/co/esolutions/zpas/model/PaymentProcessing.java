package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.*;
import zw.co.esolutions.zpas.enums.ClearingChannelCode;
import zw.co.esolutions.zpas.enums.PaymentCategoryPurposeCode;
import zw.co.esolutions.zpas.enums.PriorityCode;
import zw.co.esolutions.zpas.enums.SequenceTypeCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Data
@NoArgsConstructor
//Indexed
@Table(name = "payment_processing")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "paymentprocessinganalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class PaymentProcessing implements Auditable {
    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;
    @Version
    protected Long entityVersion;
    @Enumerated(EnumType.STRING)
    protected PriorityCode priority;

    @OneToOne(targetEntity = ServiceLevel.class, fetch = FetchType.LAZY)
    protected ServiceLevel serviceLevel;

    @Enumerated(EnumType.STRING)
    protected ClearingChannelCode clearingChannel;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name ="local_instrument_code_name")),
            @AttributeOverride(name = "name", column = @Column(name ="local_instrument_name")),
    })
    protected ExternalCode localInstrument;

    @Enumerated(EnumType.STRING)
    protected PaymentCategoryPurposeCode categoryPurpose;

    @ManyToOne(targetEntity = PaymentExecution.class, fetch = FetchType.LAZY)
    protected PaymentExecution paymentExecution;

    @Enumerated(EnumType.STRING)
    protected SequenceTypeCode sequenceType;

    @OneToOne(fetch = FetchType.LAZY)
    protected DirectDebitMandate relatedMandate;

    @JsonIgnore
    @OneToMany(targetEntity = BankTransaction.class, mappedBy = "relatedPayment", fetch = FetchType.LAZY)
    protected List<BankTransaction> bankTransaction;

    @JsonIgnore
    @OneToMany(mappedBy = "relatedPayment", targetEntity = ContactPoint.class, fetch = FetchType.LAZY)
    protected List<ContactPoint> contactPoint;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "PAYMENT_PROCESSING" + id;
    }

    @Override
    public String toString() {
        return "PaymentProcessing{}";
    }
}
