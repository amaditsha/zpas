package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;

/**
 * Created by mabuza on 31 Jan 2019
 */
@Entity
@Data
@NoArgsConstructor
//@Indexed
@Table(name = "operation_threshold")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "operationthresholdanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class OperationThreshold {
    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;
    @Version
    protected Long entityVersion;

    @OneToOne(targetEntity = BankOperation.class, fetch = FetchType.LAZY)
    protected BankOperation bankOperation;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "mininum_amount_per_transaction_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "mininum_amount_per_transaction_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "mininum_amount_per_transaction_currency_code"))
    })
    protected CurrencyAndAmount mininumAmountPerTransaction;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "maximum_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "maximum_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "maximum_amount_currency_code"))
    })
    protected CurrencyAndAmount maximumAmount;

    @Override
    public String toString() {
        return "OperationThreshold{}";
    }
}
