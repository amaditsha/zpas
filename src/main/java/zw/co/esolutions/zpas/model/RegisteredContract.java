package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;


import zw.co.esolutions.zpas.enums.CommunicationMethodCode;
import zw.co.esolutions.zpas.enums.PriorityCode;

import javax.persistence.*;
import java.time.OffsetDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//Indexed
@AnalyzerDef(name = "registeredcontractanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class RegisteredContract extends Contract {
    @OneToOne(mappedBy = "relatedCertificate", targetEntity = GenericIdentification.class, fetch = FetchType.LAZY)
    protected GenericIdentification certificate;

    @OneToOne(targetEntity = CashBalance.class, fetch = FetchType.LAZY)
    protected CashBalance contractBalance;

    @OneToOne(targetEntity = StatusReason.class, fetch = FetchType.LAZY)
    protected StatusReason closureReason;
    //protected RegulatoryReportingRole reportingParty;

    protected String identification;

    protected OffsetDateTime deliveryDate;
//    protected RegulatoryReportingRole registrationAgent;
//protected RegulatoryReportingRole receivingParty;
    @Enumerated(EnumType.STRING)
    protected PriorityCode priority;

    protected OffsetDateTime registrationDate;

    protected OffsetDateTime closureDate;

    @OneToOne(targetEntity = PaymentTerms.class, fetch = FetchType.LAZY)
    protected PaymentTerms paymentScheduleType;

    protected OffsetDateTime submissionDate;
    @Enumerated(EnumType.STRING)
    protected CommunicationMethodCode deliveryMethod;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "submission_method_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "submission_method_name"))
    })
    protected CommunicationMethodCode submissionMethod;
    @OneToOne(targetEntity = Payment.class, fetch = FetchType.LAZY)
    protected Payment relatedPayment;

    @OneToOne(targetEntity = Document.class, fetch = FetchType.LAZY)
    protected Document attachment;

    @Override
    public String toString() {
        return "RegisteredContract{}";
    }
}
