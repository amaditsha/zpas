package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.audit.Auditable;

import javax.persistence.*;
import java.util.List;

/**
 * Created by mabuza on 23 Jan 2019
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//Indexed
@AnalyzerDef(name = "ssidatabaseprovideranalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class SSIDatabaseProvider extends SettlementPartyRole implements Auditable {

    @JsonIgnore
    @OneToMany(targetEntity = StandingSettlementInstruction.class, mappedBy = "sSIDatabaseProvider")
    protected List<StandingSettlementInstruction> standingSettlementDatabase;

    @Override
    public String getInstanceName() {
        return "SSIDATABASE_PROVIDER" + id;
    }

    @Override
    public String toString() {
        return "SSIDatabaseProvider{}";
    }
}
