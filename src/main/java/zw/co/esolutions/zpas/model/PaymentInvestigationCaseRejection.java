package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;


import zw.co.esolutions.zpas.enums.CaseAssignmentRejectionCode;
import zw.co.esolutions.zpas.enums.InvestigationRejectionCode;
import zw.co.esolutions.zpas.enums.PaymentCancellationRejectionCode;
import zw.co.esolutions.zpas.enums.PaymentModificationRejectionV2Code;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mabuza on 28 Jan 2019
 */

@Data
@Entity
//Indexed
@Table(name = "payment_investigation_case_rejection", indexes = {
        @Index(name = "payment_investigation_case_rejection_status", columnList = "entityStatus")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "paymentinvestigationcaserejectionanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class PaymentInvestigationCaseRejection implements Auditable {
    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;
    @Version
    protected Long entityVersion;
    @Enumerated(EnumType.STRING)
    protected PaymentModificationRejectionV2Code rejectedModification;
    @Enumerated(EnumType.STRING)
    protected PaymentCancellationRejectionCode rejectedCancellation;

    protected String rejectedCancellationReason;

    protected Boolean assignmentCancellationConfirmation;
    @Enumerated(EnumType.STRING)
    protected CaseAssignmentRejectionCode rejectionReason;
    @OneToOne(targetEntity = PaymentInvestigationCaseResolution.class, fetch = FetchType.LAZY)
    protected PaymentInvestigationCaseResolution relatedInvestigationCaseResolution;
    @Enumerated(EnumType.STRING)
    protected InvestigationRejectionCode investigationRejection;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "PAYMENT_INVESTIGATION_CASE_REJECTION" + id;
    }

    @Override
    public String toString() {
        return "PaymentInvestigationCaseRejection{}";
    }
}
