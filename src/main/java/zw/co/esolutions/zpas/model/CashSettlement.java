package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;


import zw.co.esolutions.zpas.enums.SettlementMethodCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 23 Jan 2019
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true, exclude = { "partyRole", "relatedTransactionAdministrator" })
@Entity
//@Indexed
@AnalyzerDef(name = "cashsettlementanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class CashSettlement extends Settlement {
    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "interbank_settlement_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "interbank_settlement_amount_ccy_code")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "interbank_settlement_amount_ccy_name"))
    })
    protected CurrencyAndAmount interbankSettlementAmount;

    protected String rTGS;

    protected OffsetDateTime creditDateTime;
    @OneToOne(targetEntity = PaymentInstruction.class, fetch = FetchType.LAZY)
    protected PaymentInstruction relatedPaymentInstruction;

    @Enumerated(EnumType.STRING)
    protected SettlementMethodCode settlementMethod;
    @OneToOne(targetEntity = CashAccount.class, fetch = FetchType.LAZY)
    protected CashAccount settlementAccount;

    protected OffsetDateTime debitDateTime;

    @JsonIgnore
    @ManyToMany
    @JoinTable( name = "cashSettlementInstructionPartyRole",
            joinColumns = @JoinColumn(name = "cashSettlementInstructionPartyRole_id")
    )
    protected List<CashSettlementInstructionPartyRole> partyRole;

    @OneToOne(targetEntity = BookEntry.class, fetch = FetchType.LAZY)
    protected BookEntry bookEntry;

    @OneToOne(fetch = FetchType.LAZY)
    protected PaymentInvestigationCaseResolution relatedInvestigationCase;
    @ManyToOne(targetEntity = Reservation.class)
    protected Reservation reservation;

    @JsonIgnore
    @ManyToMany
    @JoinTable( name = "cash_settlement_transaction_administrator",
            joinColumns = @JoinColumn(name = "transaction_administrator_id")
    )
    protected List<TransactionAdministrator> relatedTransactionAdministrator;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }
    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }
    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }
    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "CashSettlement{}";
    }
}
