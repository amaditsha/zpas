package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Data
@Entity
@Table(name = "signing_detail", indexes = {
        @Index(name = "signing_detail_entitystatus", columnList = "entityStatus"),
        @Index(name = "signing_detail_datecreated", columnList = "dateCreated"),
        @Index(name = "signing_detail_lastupdated", columnList = "lastUpdated"),
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "signingdetail_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class SigningDetail implements Auditable {
    @Id
    @Column(length = 50)
    protected String id;

    @Column(length = 75)
    protected String entityType;

    @Column(length = 50)
    protected String entityId;
    @Column(length = 50)
    protected String partyId;
    @Column(length = 50)
    protected String paymentId;
    @Column
    protected Boolean done;
    @Column
    protected Boolean holder;
    @Column
    protected OffsetDateTime dateApproved;
    @Column(length = 50)
    protected String signatureConditionId;
    @Column
    protected Integer orderingNo;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", getLastUpdated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("dateCreated", getDateCreated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", getEntityStatus().name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "SIGNINGDETAIL: " + id;
    }

    @Override
    public String toString() {
        return "SigningDetail{}";
    }
}
