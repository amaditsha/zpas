package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CurrencyCode;



import zw.co.esolutions.zpas.enums.SettlementStandingInstructionDatabaseCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 23 Jan 2019
 */

@Data
@Entity
//Indexed
@Table(name = "standing_settlement_instruction", indexes = {
        @Index(name = "standing_settlement_instruction_status", columnList = "entityStatus")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "standingsettlementinstructionanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class StandingSettlementInstruction implements Auditable {
    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;
    @Version
    protected Long entityVersion;

    @JsonIgnore
    @OneToMany(targetEntity = Settlement.class, mappedBy = "standingSettlementInstruction", fetch = FetchType.LAZY)
    protected List<Settlement> settlement;

    protected Boolean fXStandingInstruction;
    @Enumerated(EnumType.STRING)
    protected SettlementStandingInstructionDatabaseCode settlementStandingInstructionDatabase;

    protected String identification;

    protected String sSIDatabaseName;
    @ManyToOne(targetEntity = SSIDatabaseProvider.class)
    protected SSIDatabaseProvider sSIDatabaseProvider;

    @ManyToOne(targetEntity = DateTimePeriod.class)
    protected DateTimePeriod validityPeriod;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "currency_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "currency_name"))
    })
    protected CurrencyCode currency;
    @OneToOne(targetEntity = Asset.class, fetch = FetchType.LAZY)
    protected Asset asset;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "STANDING_SETTLEMENT_INSTRUCTION" + id;
    }

    @Override
    public String toString() {
        return "StandingSettlementInstruction{}";
    }
}
