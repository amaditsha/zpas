package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Data
public class Status implements Auditable {
    public Status() {
        this.entityVersion = 0L;
        this.statusDateTime = OffsetDateTime.now();
        this.instructionProcessingStatus = StatusCode.None;
        this.settlementStatus = SecuritiesSettlementStatusCode.None;
        this.transactionProcessingStatus = InstructionProcessingStatusCode.None;
        this.modificationProcessingStatus = ModificationProcessingStatusCode.None;
    }

    @Id
    protected String id;

    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @JsonIgnore
    @Column(name = "status_reason")
    @OneToMany(targetEntity = StatusReason.class, mappedBy = "status", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    protected List<StatusReason> statusReason;

    protected OffsetDateTime statusDateTime;
    @OneToOne(mappedBy = "status", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    protected DateTimePeriod validityTime;

    protected String statusDescription;
    @Enumerated(EnumType.STRING)
    protected StatusCode instructionProcessingStatus;
    @Enumerated(EnumType.STRING)
    protected SecuritiesSettlementStatusCode settlementStatus;
    @Enumerated(EnumType.STRING)
    protected CancellationProcessingStatusCode cancellationProcessingStatus;
    @Enumerated(EnumType.STRING)
    protected InstructionProcessingStatusCode transactionProcessingStatus;
    @Enumerated(EnumType.STRING)
    protected ModificationProcessingStatusCode modificationProcessingStatus;
    @ManyToOne(targetEntity = InvestigationPartyRole.class, cascade = CascadeType.ALL)
    protected InvestigationPartyRole partyRole;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @PrePersist
    private void setDefaultEntityAttributes(){
        this.id = GenerateKey.generateEntityId();
        this.entityStatus = EntityStatus.DRAFT;
    }

    @Override
    public String toString() {
        return "Status{}";
    }
}
