package zw.co.esolutions.zpas.model;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class AccountOwnerRole extends AccountPartyRole {
    @Override
    public String toString() {
        return "AccountOwnerRole{}";
    }
}
