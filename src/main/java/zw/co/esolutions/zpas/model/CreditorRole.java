package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class CreditorRole extends PaymentPartyRole {

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    protected Scheme scheme;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        return attributesMap;
    }

    @Override
    public String getInstanceName() {
        return this.getId();
    }

    @Override
    public String toString() {
        return "CreditorRole{" +
                "id='" + id + '\'' +
                ", entityStatus=" + entityStatus +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityVersion=" + entityVersion +
                '}';
    }
}
