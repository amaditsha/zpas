package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.*;
import java.util.List;

/**
 * Created by mabuza on 28 Jan 2019
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//@Indexed
@AnalyzerDef(name = "duplicate_case_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class DuplicateCase extends InvestigationResolution {

    @JsonIgnore
    @OneToMany(mappedBy = "duplicateCaseResolution", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected List<InvestigationCase> duplicatedCase;

    @ManyToOne(targetEntity = PaymentInvestigationCaseResolution.class, fetch = FetchType.LAZY)
    protected PaymentInvestigationCaseResolution relatedCaseResolution;

    @Override
    public String toString() {
        return "DuplicateCase{}";
    }
}
