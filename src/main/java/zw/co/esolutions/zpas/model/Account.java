package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.*;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Data
@Entity
@Table(name = "account")
@Audited
public abstract class Account implements Auditable {
    @Id
    protected String id;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "baseCurrency_codeName")),
            @AttributeOverride(name = "name", column = @Column(name = "baseCurrency_name"))
    })
    protected CurrencyCode baseCurrency;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "reportingCurrency_codeName")),
            @AttributeOverride(name = "name", column = @Column(name = "reportingCurrency_name"))
    })
    protected CurrencyCode reportingCurrency;

    protected String purpose;

    protected OffsetDateTime closingDate;

    protected OffsetDateTime liveDate;

    @OneToOne(targetEntity = DateTimePeriod.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @NotAudited
    protected DateTimePeriod reportedPeriod;

    protected OffsetDateTime openingDate;

    @OneToOne(targetEntity = AccountIdentification.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @NotAudited
    protected AccountIdentification identification;

    @ManyToOne(fetch = FetchType.LAZY)
    protected Account parentAccount;

    @JsonIgnore
    @OneToMany(mappedBy = "parentAccount", fetch = FetchType.LAZY)
    protected List<Account> subAccount;

    @ManyToOne(targetEntity = AccountStatus.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @NotAudited
    protected AccountStatus status;

    @OneToOne(fetch = FetchType.LAZY)
    @NotAudited
    protected SettlementPartyRole settlementPartyRole;

    @JsonIgnore
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    @NotAudited
    protected List<AccountRestriction> accountRestriction;

    @OneToOne(targetEntity = GenericIdentification.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @NotAudited
    protected GenericIdentification type;

    //protected CorporateActionPartyRole relatedCorporateActionPartyRole; - not sure if it should be included

    @JsonIgnore
    @ManyToMany(mappedBy = "account", fetch = FetchType.LAZY)
    @NotAudited
    protected List<AccountPartyRole> partyRole;

    @JsonIgnore
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    @NotAudited
    protected List<System> system;

    @NotAudited
    @JsonIgnore
    @ManyToMany(targetEntity = Entry.class, fetch = FetchType.LAZY)
    @JoinTable(name = "account_entry",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "entry_id")
    )
    protected List<Entry> entry;

    @NotAudited
    @JsonIgnore
    @ManyToMany(targetEntity = AccountContract.class, fetch = FetchType.LAZY)
    @JoinTable(name = "account_accountContract",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "account_contract_id")
    )
    protected List<AccountContract> accountContract;

    /**
     * "Rate used to calculate the difference between amounts based on the base currency and the reporting currency."
     */
    @JsonIgnore
    @OneToMany(mappedBy = "reportedAccount", fetch = FetchType.LAZY)
    @NotAudited
    protected List<CurrencyExchange> currencyExchange;

    @OneToOne(mappedBy = "account", fetch = FetchType.LAZY)
    @NotAudited
    protected SystemMemberRole systemMember;

    @OneToOne(mappedBy = "account", fetch = FetchType.LAZY)
    @NotAudited
    protected AccountService accountService;

    @JsonIgnore
    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    @NotAudited
    protected List<Balance> balance;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    protected Integer virtualNumber;
    protected String virtualCode;
    protected Boolean master;

    @Version
    @Column
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", getLastUpdated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", getEntityStatus().name());
        attributesMap.put("role", getPartyRole() + "");

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "ACCOUNT: " + id;
    }

    @PrePersist
    private void setDefaultEntityAttributes(){
        if(id == null) {
            this.id = GenerateKey.generateEntityId();
        }
        this.entityStatus = EntityStatus.PENDING_APPROVAL;
    }

    @Override
    public String toString() {
        return "Account{" +
                "baseCurrency=" + baseCurrency +
                ", reportingCurrency=" + reportingCurrency +
                ", purpose='" + purpose + '\'' +
                '}';
    }

    public FinancialInstitution getFinancialInstitution() {
        return this.getPartyRole().stream().filter(a -> a instanceof AccountServicerRole)
                .map(a -> (AccountServicerRole) a).findFirst().map(accountServicerRole -> {
                    return accountServicerRole.getPlayer().stream()
                            .filter(rolePlayer -> rolePlayer instanceof FinancialInstitution)
                            .map(rolePlayer -> (FinancialInstitution) rolePlayer)
                            .findFirst().orElse(null);
                }).orElse(null);
    }

    public Party getAccountOwner() {
        return this.getPartyRole().stream().filter(a -> a instanceof AccountOwnerRole)
                .map(a -> (AccountOwnerRole) a).findFirst().map(accountOwnerRole -> {
                    return accountOwnerRole.getPlayer().stream()
                            .filter(rolePlayer -> rolePlayer instanceof Party)
                            .map(rolePlayer -> (Party) rolePlayer)
                            .findFirst().orElse(null);
                }).orElse(null);
    }

    public String getFinancialInstitutionName() {
        return Optional.ofNullable(getFinancialInstitution()).map(f -> f.getName()).orElse("");
    }

    public String getFinancialInstitutionShortName() {
        return Optional.ofNullable(getFinancialInstitution()).map(f -> f.getShortName()).orElse("");
    }

}
