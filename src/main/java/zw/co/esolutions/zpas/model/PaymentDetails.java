package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.InstructionCode;
import zw.co.esolutions.zpas.enums.PaymentInstrumentCode;
import zw.co.esolutions.zpas.enums.PaymentTypeCode;
import zw.co.esolutions.zpas.enums.PriorityCode;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "payment_details", indexes = {
        @Index(name = "pd_pmt", columnList = "payment_id")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "paymentdetails_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
@EqualsAndHashCode(of = {"id"})
public class PaymentDetails {
    @Id
    protected String id;

    protected String debtorAgentBIC;

    protected String debtorAgentName;

    protected String creditorAgentBIC;

    protected String creditorAgentName;

    protected String debtorName;

    protected String creditorName;

    protected String debtorAccountNumber;
    protected String debtorAccountCurrency;
    protected String debtorAccountName;

    protected String creditorAccountNumber;

    protected String endToEndId;

    protected String bulkPaymentId;
    protected String privateId;
    protected String organisationId;

    protected OffsetDateTime valueDate;

    @Enumerated(EnumType.STRING)
    protected PriorityCode priority;

    @Enumerated(EnumType.STRING)
    protected ProprietaryPurposeCode purpose;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amount_currency_code"))
    })
    protected CurrencyAndAmount amount;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "instructed_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "instructed_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "instructed_amount_currency_code"))
    })
    protected CurrencyAndAmount instructedAmount;

    @JsonIgnore
    @ElementCollection
    protected List<String> unstructuredRemittanceInformation;

    @Enumerated(EnumType.STRING)
    protected PaymentInstrumentCode paymentInstrument;

    @Enumerated(EnumType.STRING)
    protected PaymentTypeCode type;

    @Enumerated(EnumType.STRING)
    protected InstructionCode instructionForCreditorAgent;

    @Enumerated(EnumType.STRING)
    protected InstructionCode instructionForDebtorAgent;

    @OneToOne(fetch = FetchType.LAZY)
    protected Payment payment;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;
}
