package zw.co.esolutions.zpas.model.audittrail;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import zw.co.esolutions.zpas.utilities.audit.Auditable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Entity
@NamedQueries({
        @NamedQuery(name="getActivity",query="SELECT a FROM Activity a"),
        @NamedQuery(name="getActivityById",query = "SELECT a FROM Activity a WHERE a.id =:id"),
        @NamedQuery(name="getActivityByName",query = "SELECT a FROM Activity a WHERE a.name = :name ORDER BY a.name ASC"),
        @NamedQuery(name="getActivityByLogged",query = "SELECT a FROM Activity a WHERE a.logged = :logged")
})
@Table(name = "activity")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Data
@Indexed
public class Activity implements Serializable, Auditable {
    @Id
    @Column(length=50)
    private String id;

    @Column(length=60)
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    private String name;

    @Column
    private boolean logged;

    @JsonIgnore
    @OneToMany(mappedBy="activity")
    private List<AuditTrail> auditTrails;

    @Version
    private long version;

    @Override
    public String getEntityName() {
        return null;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        return null;
    }

    @Override
    public String getAuditableAttributesString() {
        return null;
    }

    @Override
    public String getInstanceName() {
        return null;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", logged=" + logged +
                ", version=" + version +
                '}';
    }
}
