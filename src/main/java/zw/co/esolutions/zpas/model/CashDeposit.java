package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by alfred on 30 Jan 2019
 */

@Data
@Entity
//@Indexed
@AnalyzerDef(name = "cash_deposit_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class CashDeposit extends IndividualPayment {
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "note_denomination_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "note_denomination_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "note_denomination_currency_code"))
    })
    protected CurrencyAndAmount noteDenomination;

    protected String numberOfNotes;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "deposit_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "deposit_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "deposit_amount_currency_code"))
    })
    protected CurrencyAndAmount depositAmount;

//    protected BankingTransaction relatedBankingTransaction;


    @Override
    public String toString() {
        return "CashDeposit{}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObligationFulfilment that = (ObligationFulfilment) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
