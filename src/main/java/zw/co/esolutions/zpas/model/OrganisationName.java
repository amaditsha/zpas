package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Created by tanatsa on 23 Jan 2019
 */

@Audited
@EqualsAndHashCode(exclude = {"organisation"})
@Data
@Indexed
@Entity
@AnalyzerDef(name = "organisationnameanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class OrganisationName extends PartyName {

    @ManyToOne(targetEntity = OrganisationIdentification.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    protected OrganisationIdentification organisation;

    @Field
    @Analyzer(definition = "organisationnameanalyzer")
    protected String legalName;

    @Field
    @Analyzer(definition = "organisationnameanalyzer")
    protected String tradingName;

    @Field
    @Analyzer(definition = "organisationnameanalyzer")
    protected String shortName;

    @Override
    public String toString() {
        return "OrganisationName{" +
                "organisation=" + organisation +
                ", legalName='" + legalName + '\'' +
                ", tradingName='" + tradingName + '\'' +
                ", shortName='" + shortName + '\'' +
                ", id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                ", name='" + name + '\'' +
                '}';
    }
}
