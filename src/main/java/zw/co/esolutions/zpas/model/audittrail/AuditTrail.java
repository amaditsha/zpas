package zw.co.esolutions.zpas.model.audittrail;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Entity
@NamedQueries({
        @NamedQuery(name="getByEntityNameAndInstanceName", query="SELECT a FROM AuditTrail a WHERE a.entityName = :entityName " +
                "AND (:fiId is NULL or :fiId = 'null' or :fiId = '' or a.fiId = :fiId) " +
                "AND a.instanceName = :instanceName ORDER BY a.time DESC"),
        @NamedQuery(name="getByEntityNameAndEntityId", query="SELECT a FROM AuditTrail a WHERE a.entityName = :entityName AND a.entityId = :entityId ORDER BY a.time DESC"),
        @NamedQuery(name="getByEntityId", query="SELECT a FROM AuditTrail a WHERE a.entityId = :entityId ORDER BY a.time DESC"),
        @NamedQuery(name="getByEntityNameAndInstanceNameAndTimePeriod",query="SELECT a FROM AuditTrail a WHERE a.entityName = :entityName " +
                "AND (:fiId is NULL or :fiId = 'null' or :fiId = '' or a.fiId = :fiId) " +
                "AND a.instanceName = :instanceName AND a.time BETWEEN :startTime AND :endTime ORDER BY a.time DESC"),
        @NamedQuery(name="getByUsername", query="SELECT a FROM AuditTrail a WHERE a.username = :username " +
                "AND (:fiId is NULL or :fiId = 'null' or :fiId = '' or a.fiId = :fiId) ORDER BY a.time DESC"),
        @NamedQuery(name="getById", query="SELECT a FROM AuditTrail a WHERE a.auditTrailId = :auditTrailId ORDER BY a.time DESC"),
        @NamedQuery(name="getByUsernameAndTimePeriod",query="SELECT a FROM AuditTrail a WHERE a.username = :username " +
                "AND (:fiId is NULL or :fiId = 'null' or :fiId = '' or a.fiId = :fiId) " +
                "AND a.time BETWEEN :startTime AND :endTime ORDER BY a.time DESC"),
        @NamedQuery(name="getByActivityAndTimePeriod",query="SELECT a FROM AuditTrail a WHERE a.activity.id = :activityId " +
                "AND (:fiId is NULL or :fiId = 'null' or :fiId = '' or a.fiId = :fiId) " +
                "AND a.time BETWEEN :startTime AND :endTime ORDER BY a.time DESC"),
        @NamedQuery(name="getByTimePeriod",query="SELECT a FROM AuditTrail a WHERE a.time BETWEEN :startTime AND :endTime " +
                "AND (:fiId is NULL or :fiId = 'null' or :fiId = '' or a.fiId = :fiId) ORDER BY a.time DESC")
})
@Table(name = "audittrail")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "auditanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @Parameter(name = "language", value = "English")})})
@Data
@Indexed
public class AuditTrail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(length=200)
    private String auditTrailId;

    @Column(length=100)
    @Field
    @Analyzer(definition = "auditanalyzer")
    private String instanceName;

    @Column(length = 200)
    private String fiId;

    @Column(length = 200)
    private String clientId;

    @Column(length=200)
    @Field
    @Analyzer(definition = "auditanalyzer")
    private String entityName;

    @Column(length=200)
    @Field
    @Analyzer(definition = "auditanalyzer")
    private String entityId;

    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime time;

    @Column(length=2000)
    private String narrative;

    @Column(length=200)
    @Field
    @Analyzer(definition = "auditanalyzer")
    private String username;

    @ManyToOne
    private Activity activity;


    @Override
    public String toString() {
        return "AuditTrail{" +
                "auditTrailId='" + auditTrailId + '\'' +
                ", instanceName='" + instanceName + '\'' +
                ", fiId='" + fiId + '\'' +
                ", clientId='" + clientId + '\'' +
                ", entityName='" + entityName + '\'' +
                ", entityId='" + entityId + '\'' +
                ", time=" + time +
                ", narrative='" + narrative + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
