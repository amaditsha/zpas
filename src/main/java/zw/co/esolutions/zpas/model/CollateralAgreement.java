package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.enums.ExposureConventionTypeCode;
import zw.co.esolutions.zpas.enums.FrequencyCode;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
////Indexed
@AnalyzerDef(name = "collateralagreementanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class CollateralAgreement extends Agreement {
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "base_currency_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "base_currency_name"))
    })
    protected CurrencyCode baseCurrency;

    @JsonIgnore
    @OneToMany(mappedBy = "collateralAgreement", fetch = FetchType.LAZY)
    protected List<MasterAgreement> associatedMasterAgreement;

//    @OneToMany(mappedBy = "relatedCollateralAgreement")
//    protected List<StandingSettlementInstruction> standingSettlementInstructions;

    @Enumerated(EnumType.STRING)
    protected ExposureConventionTypeCode marginConvention;

//    protected List<ExposureTerm> exposureTerm;
     @Enumerated(EnumType.STRING)
    protected FrequencyCode callFrequency;

//    protected List<Collateral> collateral;

//    protected CollateralManagement riskCoverage;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "CollateralAgreement " + getId();
    }

    @Override
    public String toString() {
        return "CollateralAgreement{}";
    }
}
