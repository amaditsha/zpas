package zw.co.esolutions.zpas.model;

import lombok.Data;

import javax.persistence.Entity;

/**
 * Created by jnkomo on 23 Jan 2019
 */

@Data
@Entity
public class IntermediaryRole extends AccountPartyRole{

}
