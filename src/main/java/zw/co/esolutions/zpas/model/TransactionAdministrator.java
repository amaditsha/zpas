package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 30 Jan 2019
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//Indexed
@AnalyzerDef(name = "transactionadministratorentity",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class TransactionAdministrator extends SystemPartyRole {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @ManyToOne(targetEntity = CashClearingSystem.class, fetch = FetchType.LAZY)
    protected CashClearingSystem cashClearingSystem;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "currency_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "currency_name"))
    })
    protected CurrencyCode currency;

    @JsonIgnore
    @OneToMany(mappedBy = "currencyExchangeForTransactionAdministrator",fetch = FetchType.LAZY)
    protected List<CurrencyExchange> currencyExchange;

    @JsonIgnore
    @OneToMany(mappedBy = "relatedTransactionAdministrator", fetch = FetchType.LAZY)
    protected List<CashManagementService> cashManagementService;

    @JsonIgnore
    @ManyToMany(mappedBy = "relatedTransactionAdministrator", fetch = FetchType.LAZY)
    protected List<CashSettlement> settlementInstruction;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "TRANSACTION_ADMINISTRATOR : " + id;
    }

    @Override
    public String toString() {
        return "TransactionAdministrator{" +
                "id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", cashClearingSystem=" + cashClearingSystem +
                ", currency=" + currency +
                ", currencyExchange=" + currencyExchange +
                ", cashManagementService=" + cashManagementService +
                ", settlementInstruction=" + settlementInstruction +
                '}';
    }
}
