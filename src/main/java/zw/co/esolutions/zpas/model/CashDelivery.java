package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.esolutions.zpas.datatype.ActiveCurrencyAndAmount;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Data
public class CashDelivery {
    @Id
    private String id;
    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "cash_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "cash_amount_ccy_code")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "cash_amount_ccy_name"))
    })
    protected ActiveCurrencyAndAmount cashAmount;

    @Override
    public String toString() {
        return "CashDelivery{}";
    }
}
