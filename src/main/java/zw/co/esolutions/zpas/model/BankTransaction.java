package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.ExternalBankTransactionDomainCode;
import zw.co.esolutions.zpas.codeset.ExternalBankTransactionFamilyCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 31 Jan 2019
 */

@Entity
@Data
//@Indexed
@Table(name = "bank_transaction", indexes = {
        @Index(name = "bank_transaction_status", columnList = "entityStatus")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "banktransactionanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class BankTransaction implements Auditable {
    @Id
    protected String id;
    @CreationTimestamp
    protected OffsetDateTime dateCreated;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;
    @Version
    protected Long entityVersion;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name ="domain_code_name")),
            @AttributeOverride(name = "name", column = @Column(name ="domain_name")),
    })
    protected ExternalBankTransactionDomainCode domain;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name ="family_code_name")),
            @AttributeOverride(name = "name", column = @Column(name ="family_name")),
    })
    protected ExternalBankTransactionFamilyCode family;
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name ="subfamily_code_name")),
            @AttributeOverride(name = "name", column = @Column(name ="subfamily_name")),
    })
    protected ExternalBankTransactionFamilyCode subFamily;

    @JsonIgnore
    @ManyToMany(targetEntity = GenericIdentification.class, fetch = FetchType.LAZY)
    @JoinTable(name = "bank_transaction_generic_identification", joinColumns = {
            @JoinColumn(name = "bank_transaction_id")}, inverseJoinColumns = {
                    @JoinColumn(name = "generic_identification_id")})
    protected List<GenericIdentification> proprietaryIdentification;

    @OneToOne(targetEntity = BankOperation.class, fetch = FetchType.LAZY)
    protected BankOperation bankOperation;

    @OneToOne(targetEntity = Entry.class)
    protected Entry relatedEntry;

    @ManyToOne(targetEntity = PaymentProcessing.class, fetch = FetchType.LAZY)
    protected PaymentProcessing relatedPayment;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "BANK_TRANSACTION" + id;
    }

    @Override
    public String toString() {
        return "BankTransaction{}";
    }
}
