package zw.co.esolutions.zpas.model;

import lombok.Data;

import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Entity
@Data
public class IntermediaryAgentRole extends PaymentPartyRole {

    @OneToOne(targetEntity = IntermediaryAgentRole.class, mappedBy = "nextParty", fetch = FetchType.LAZY)
    protected IntermediaryAgentRole intermediaryAgentRole;

    @OneToOne(targetEntity = IntermediaryAgentRole.class, fetch = FetchType.LAZY)
    protected IntermediaryAgentRole nextParty;

    @Column
    protected Number position;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return getId();
    }

    @Override
    public String toString() {
        return "IntermediaryAgentRole{}";
    }
}
