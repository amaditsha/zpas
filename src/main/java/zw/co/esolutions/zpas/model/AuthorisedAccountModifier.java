package zw.co.esolutions.zpas.model;

import lombok.Data;

import javax.persistence.Entity;

/**
 * Created by jnkomo on 28 Jan 2019
 */
@Data
@Entity
public class AuthorisedAccountModifier extends AccountPartyRole {
}
