package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alfred on 13 Mar 2019
 */

@Table(name = "custom_party")
@Data
@Entity
@AnalyzerDef(name = "custom_party_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class CustomParty implements Auditable {
    @Id
    @Column(length = 50)
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @OneToOne(mappedBy = "customParty")
    private Role role;

    private String name;
    private String nationalId;
    private String passportNumber;
    private String accountNumber;
    private String financialInstitutionIdentification;

    @Override
    public String getEntityName() {
        return "CUSTOM_BENEFICIARY_IDENTIFICATION";
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> auditableAttributesMap = new HashMap<>();
        auditableAttributesMap.put("name", name);
        auditableAttributesMap.put("nationalId", nationalId);
        auditableAttributesMap.put("passportNumber", passportNumber);
        auditableAttributesMap.put("accountNumber", accountNumber);
        auditableAttributesMap.put("financialInstitutionIdentification", financialInstitutionIdentification);
        return auditableAttributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }
}
