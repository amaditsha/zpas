package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.*;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;
import org.hibernate.search.annotations.Index;


import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jnkomo on 27 Mar 2019
 */
@Entity
@Data
@Indexed
@Table(name = "merchant")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "merchantanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class Merchant implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String name;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String code;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String accountNumber;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String bank;

    @JsonIgnore
    @OneToMany(targetEntity = PartyMerchantAccount.class, fetch = FetchType.LAZY)
    protected List<PartyMerchantAccount> partyMerchantAccounts;

    @JsonIgnore
    @ManyToOne(targetEntity = Party.class, cascade = {CascadeType.PERSIST}, fetch = FetchType.EAGER)
    protected Party linkedMerchantId;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "MERCHANT" + id;
    }
}
