package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
//@Indexed
@AnalyzerDef(name = "cashaccountcontractanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class CashAccountContract extends AccountContract implements Auditable {
    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "cashaccountcontract_cash_account",
            joinColumns = @JoinColumn(name = "cash_account_contract_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "cash_account_id", referencedColumnName = "id")
    )
    protected List<CashAccount> cashAccount;

    @JsonIgnore
    @OneToMany(mappedBy = "closedAccountContract", fetch = FetchType.LAZY)
    protected List<CashAccount> transferCashAccount;

    @ManyToOne(targetEntity = CashAccountMandate.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    protected CashAccountMandate cashAccountMandate;

    @JsonIgnore
    @OneToMany(mappedBy = "cashAccountContract", fetch = FetchType.LAZY)
    protected List<CashAccountService> services;

    @JsonIgnore
    @OneToMany(mappedBy = "relatedAccountClosingTerms", fetch = FetchType.LAZY)
    protected List<PaymentObligation> balanceTransfer;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "CashAccountContract " + getId();
    }

    @Override
    public String toString() {
        return "CashAccountContract{}";
    }

    @PrePersist
    public void init() {
        setId(GenerateKey.generateEntityId());
        setEntityStatus(EntityStatus.PENDING_APPROVAL);
        setDateCreated(OffsetDateTime.now());
    }
}
