package zw.co.esolutions.zpas.model;

import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;


import zw.co.esolutions.zpas.enums.FrequencyCode;
import zw.co.esolutions.zpas.enums.StandingOrderTypeCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Data
@Entity
//Indexed
@Table(name = "standing_order", indexes = {
        @Index(name = "standing_order_entitystatus", columnList = "entityStatus"),
        @Index(name = "standing_order_datecreated", columnList = "dateCreated"),
        @Index(name = "standing_order_lastupdated", columnList = "lastUpdated"),
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class StandingOrder implements Auditable {
    @Id
    protected String id;

    protected String identification;

    @Enumerated(EnumType.STRING)
    protected StandingOrderTypeCode type;

    @OneToOne(mappedBy = "relatedStandingOrder", fetch = FetchType.LAZY)
    protected DateTimePeriod validityPeriod;

    protected String linkSetIdentification;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "value", column = @Column(name = "standingOrderSequence"))
    })
    protected Number standingOrderSequence;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amountCurrency_codeName")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amountCurrency_name"))
    })
    protected CurrencyAndAmount amount;

    @Enumerated(EnumType.STRING)
    protected FrequencyCode frequency;

    protected String eventDescription;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "value", column = @Column(name = "day"))
    })
    protected Number day;

    protected String timeSpecification;

    @OneToOne(targetEntity = CashAccount.class, mappedBy = "relatedCreditStandingOrder", fetch = FetchType.LAZY)
    protected CashAccount creditAccount;

    @OneToOne(targetEntity = CashAccount.class, mappedBy = "relatedDebitStandingOrder", fetch = FetchType.LAZY)
    protected CashAccount debitAccount;

    @OneToOne(mappedBy = "standingOrder", fetch = FetchType.LAZY)
    protected PaymentInstruction paymentInstructionTrigger;

    @OneToOne(fetch = FetchType.LAZY)
    protected StandingOrder includedStandingOrder;

    @OneToOne(mappedBy = "includedStandingOrder", fetch = FetchType.LAZY)
    protected StandingOrder linkSet;

    @Enumerated(EnumType.STRING)
    @Column
    protected EntityStatus entityStatus;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    protected OffsetDateTime lastUpdated;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", entityStatus.name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "STANDING ORDER: " + id;
    }

    @Override
    public String toString() {
        return "StandingOrder{}";
    }
}
