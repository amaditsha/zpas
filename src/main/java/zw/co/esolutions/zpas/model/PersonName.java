package zw.co.esolutions.zpas.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.*;

import org.hibernate.search.annotations.Index;
import zw.co.esolutions.zpas.enums.NamePrefix1Code;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by tanatsa on 23 Jan 2019
 */

@Audited
@EqualsAndHashCode(callSuper = true, exclude = {"identification"})
@Entity
@Data
@Indexed
@AnalyzerDef(name = "personnameanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class PersonName extends PartyName {

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String birthName;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    @Enumerated(EnumType.STRING)
    protected NamePrefix1Code namePrefix;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String givenName;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String middleName;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
    protected String nameSuffix;

    @ManyToOne(targetEntity = PersonIdentification.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    protected PersonIdentification identification;

    @Override
    public String toString() {
        return "PersonName{" +
                "birthName='" + birthName + '\'' +
                ", namePrefix=" + namePrefix +
                ", givenName='" + givenName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", nameSuffix='" + nameSuffix + '\'' +
                ", identification=" + identification +
                '}';
    }
}
