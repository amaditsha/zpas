package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Created by tanatsa on 23 Jan 2019
 */
@Audited
@EqualsAndHashCode(callSuper = true, exclude = {"placeOfOperation", "placeOfRegistration"})
//Indexed
@Data
@Entity
@AnalyzerDef(name = "organizationanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class Organisation extends Party {

    protected String purpose;

    protected OffsetDateTime registrationDate;

    @JsonIgnore
    @OneToMany(mappedBy = "organisation", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @NotAudited
    protected List<OrganisationIdentification> organisationIdentification;

    @ManyToOne(fetch = FetchType.LAZY)
    protected Organisation parentOrganisation;

    @JsonIgnore
    @OneToMany(mappedBy = "parentOrganisation", fetch = FetchType.LAZY)
    protected List<Organisation> branch;

    @JsonIgnore
    @OneToMany(targetEntity = Location.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    protected List<Location> placeOfOperation;

    @OneToOne(targetEntity = Location.class, cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    protected Location placeOfRegistration;

    protected String description;

    protected OffsetDateTime establishmentDate;

    @JsonIgnore
    @OneToMany(mappedBy = "financialInstitution", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @NotAudited
    protected List<SortCode> sortCodes;

    @Column
    protected String ssr;
    @Column
    protected String nssaBusinessPartnerNumber;


    @Override
    public String toString() {
        return "Organisation{}";
    }

    @Override
    public String getName() {
        final StringBuilder financialInstitutionName = new StringBuilder();
        this.getOrganisationIdentification().stream().findFirst().ifPresent(organisationIdentification -> {
            organisationIdentification.getOrganisationName().stream().findFirst().ifPresent(organisationName -> {
                financialInstitutionName.append(organisationName.getLegalName());
            });
        });
        return financialInstitutionName.toString();
    }

    @Override
    public String getShortName() {
        final StringBuilder financialInstitutionName = new StringBuilder();
        this.getOrganisationIdentification().stream().findFirst().ifPresent(organisationIdentification -> {
            organisationIdentification.getOrganisationName().stream().findFirst().ifPresent(organisationName -> {
                financialInstitutionName.append(organisationName.getShortName());
            });
        });
        return financialInstitutionName.toString();
    }

    @Override
    public String getBusinessPartnerNumber() {
        return getOrganisationIdentification().stream().findFirst().map(organisationIdentification -> {
            return organisationIdentification.getTaxIdentificationNumber();
        }).orElse("");
    }

    public String getAnyBIC() {
        return this.getOrganisationIdentification().stream().findFirst().map(organisationIdentification1 -> {
            if(organisationIdentification1.getAnyBIC() != null) {
                return organisationIdentification1.getAnyBIC();
            } else if(organisationIdentification1.getBICFI() != null) {
                return organisationIdentification1.getBICFI();
            } else if(organisationIdentification1.getBICNonFI() != null) {
                return organisationIdentification1.getBICNonFI();
            } else {
                return organisationIdentification1.getOrganisation().getId();
            }
        }).orElse("");
    }

    public String getBIC() {
        return this.getOrganisationIdentification().stream().findFirst().map(organisationIdentification1 -> {
            if(organisationIdentification1.getBICFI() != null) {
                return organisationIdentification1.getBICFI();
            } else if(organisationIdentification1.getAnyBIC() != null) {
                return organisationIdentification1.getAnyBIC();
            } else if(organisationIdentification1.getBICNonFI() != null) {
                return organisationIdentification1.getBICNonFI();
            } else {
                return organisationIdentification1.getOrganisation().getId();
            }
        }).orElse("");
    }
}
