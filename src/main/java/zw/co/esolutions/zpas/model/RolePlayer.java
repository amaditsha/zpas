package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.search.annotations.Indexed;
import zw.co.esolutions.zpas.model.api.APIUser;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static javax.persistence.CascadeType.*;
import static javax.persistence.CascadeType.MERGE;

/**
 * Created by tanatsa on 23 Jan 2019
 */
//Indexed
@Audited
@Entity
@Data
public abstract class RolePlayer implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @JsonIgnore
    @ManyToMany(targetEntity = Role.class, mappedBy = "player", cascade = {PERSIST, MERGE}, fetch = FetchType.LAZY)
    @NotAudited
    protected List<Role> role;

    @ManyToOne(targetEntity = DateTimePeriod.class, cascade = {PERSIST}, fetch = FetchType.LAZY)
    @NotAudited
    protected DateTimePeriod validityPeriod;

    @OneToOne
    @NotAudited
    protected APIUser apiUser;

    public void addRole(Role role) {
        if (this.role == null)
            this.role = new ArrayList<>();
        this.getRole().add(role);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        attributesMap.put("dateCreated", getDateCreated().format(formatter) + "");
        attributesMap.put("lastUpdated", getLastUpdated().format(formatter) + "");
        //attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", getEntityStatus() + "");
        attributesMap.put("role", getRole() + "");

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public String toString() {
        return "RolePlayer{" +
                "id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                '}';
    }
}
