package zw.co.esolutions.zpas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import org.springframework.lang.NonNull;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Data
@Entity
@Table(name = "hub_configuration", indexes = {
        @Index(name = "hub_config_entitystatus", columnList = "entityStatus"),
        @Index(name = "hub_config_datecreated", columnList = "dateCreated"),
        @Index(name = "hub_config_lastupdated", columnList = "lastUpdated"),
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "hub_configuration_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HubConfiguration implements Auditable {
    @Id
    @Column(length = 75)
    protected String BIC;

    @Column(columnDefinition = "text")
    @NonNull
    protected String uri;

    @Column(columnDefinition = "text")
    protected String clientCertPath;

    @Column(columnDefinition = "text")
    protected String clientKeyPath;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("BIC", BIC);
        attributesMap.put("uri", uri);
        attributesMap.put("clientCertPath", clientCertPath);
        attributesMap.put("clientKeyPath", clientKeyPath);
        attributesMap.put("lastUpdated", getLastUpdated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("dateCreated", getDateCreated().format(DateTimeFormatter.ISO_DATE_TIME));
        attributesMap.put("entityStatus", getEntityStatus().name());

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "HUBCONFIGURATION: " + getId();
    }

    @Override
    public String getId() {
        return BIC;
    }
}
