package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.datatype.ActiveCurrencyAndAmount;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.CopyDuplicateCode;
import zw.co.esolutions.zpas.enums.DataSetTypeCode;
import zw.co.esolutions.zpas.enums.DocumentTypeCode;
import zw.co.esolutions.zpas.enums.LanguageCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
//@Indexed
@Entity
@Table(name = "document", indexes = {
        @Index(name = "document_entitystatus", columnList = "entityStatus"),
        @Index(name = "document_datecreated", columnList = "dateCreated"),
        @Index(name = "document_lastupdated", columnList = "lastUpdated"),
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "documentanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class Document implements Auditable {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    protected OffsetDateTime issueDate;

    @Enumerated(EnumType.STRING)
    protected CopyDuplicateCode copyDuplicate;

    @JsonIgnore
    @ManyToMany(targetEntity = ContactPoint.class, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "contact_point_document",
            joinColumns = {
                    @JoinColumn(name = "document_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "contact_point_id")})
    protected List<ContactPoint> placeOfStorage;

    @ManyToOne(targetEntity = PaymentObligation.class, fetch = FetchType.LAZY)
    protected PaymentObligation paymentObligation;

    @Enumerated(EnumType.STRING)
    protected DocumentTypeCode type;

    @Column(length = 140)
    protected String status;

    @JsonIgnore
    @ManyToMany(targetEntity = DocumentPartyRole.class, cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinTable(name = "document_party_role_document",
            joinColumns = @JoinColumn(name = "document_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "document_party_role_id", referencedColumnName = "id"))
    protected List<DocumentPartyRole> partyRole;

    @Column(length = 35)
    protected String purpose;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "amount_amount")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "amount_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "amount_currency_code_name"))
    })
    protected ActiveCurrencyAndAmount amount;

    @JsonIgnore
    @OneToMany(targetEntity = Agreement.class, mappedBy = "document", fetch = FetchType.LAZY)
    protected List<Agreement> agreement;

    @ManyToOne(targetEntity = Location.class, fetch = FetchType.LAZY)
    protected Location placeOfIssue;

    protected Number documentVersion;

    //protected List<Reconciliation> reconciliation;

    //protected LetterOfCredit letterOfCredit;

    //protected List<DocumentPartyRole> partyRole;

    @Enumerated(EnumType.STRING)
    protected DataSetTypeCode dataSetType;

    //protected Transport transport;

    protected Boolean signedIndicator;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "remitted_amount_amount")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "remitted_amount_currency_name")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "remitted_amount_currency_code_name"))
    })
    protected CurrencyAndAmount remittedAmount;

    //protected List<Guarantee> guarantee;

    @Enumerated(EnumType.STRING)
    protected LanguageCode language;

    @JsonIgnore
    @OneToMany(targetEntity = Evidence.class, mappedBy = "relatedDocument")
    protected List<Evidence> evidence;

    //protected List<CommercialTrade> commercialTrade;

    //protected Presentation presentation;

    //protected RegisteredContract relatedContract;

    @OneToOne(targetEntity = GenericIdentification.class, mappedBy = "identifiedDocument", fetch = FetchType.LAZY)
    protected GenericIdentification documentIdentification;

//    protected Presentation presentation;
    @OneToOne(mappedBy = "attachment", fetch = FetchType.LAZY)
    protected RegisteredContract relatedContract;

    @Version
    protected Long entityVersion;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @Override
    public String toString() {
        return "Document{}";
    }
}
