package zw.co.esolutions.zpas.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;


import javax.persistence.*;

/**
 * Created by jnkomo on 24 Jan 2019
 */

@Audited
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class ElectronicAddress extends ContactPoint {

    protected String emailAddress;

    protected String uRLAddress;

    protected String telexAddress;

    protected String teletextAddress;

    protected String iSDNAddress;

    @Override
    public String toString() {
        return "ElectronicAddress{" +
                "emailAddress='" + emailAddress + '\'' +
                ", uRLAddress='" + uRLAddress + '\'' +
                ", telexAddress='" + telexAddress + '\'' +
                ", teletextAddress='" + teletextAddress + '\'' +
                ", iSDNAddress='" + iSDNAddress + '\'' +
                ", id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastUpdated=" + lastUpdated +
                ", entityStatus=" + entityStatus +
                ", entityVersion=" + entityVersion +
                ", bICAddress='" + bICAddress + '\'' +
                ", temporaryIndicator=" + temporaryIndicator +
                '}';
    }
}
