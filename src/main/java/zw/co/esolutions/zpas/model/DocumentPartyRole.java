package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AnalyzerDef(name = "document_party_role_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})

public class DocumentPartyRole extends Role  {

    @JsonIgnore
    @ManyToMany(targetEntity = Document.class, mappedBy = "partyRole", fetch = FetchType.LAZY)
    protected List<Document> document;

    @Override
    public String toString() {
        return "DocumentPartyRole{}";
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> auditableAttributes = new HashMap<>();

        return auditableAttributes;
    }

    @Override
    public String getInstanceName() {
        return id;
    }
}
