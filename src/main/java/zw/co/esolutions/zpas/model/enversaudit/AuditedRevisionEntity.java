package zw.co.esolutions.zpas.model.enversaudit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "AuditedRevisionEntity")
@Table(name = "audited_rev_info")
@RevisionEntity(AuditingRevisionListener.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AuditedRevisionEntity extends DefaultRevisionEntity {

    private String username;

    @OneToMany(
            mappedBy="revision",
            cascade={
                    CascadeType.PERSIST,
                    CascadeType.REMOVE
            }
    )
    private Set<EntityType> modifiedEntityTypes = new HashSet<>();

    public Set<EntityType> getModifiedEntityTypes() {
        return modifiedEntityTypes;
    }

    public void addModifiedEntityType(String entityClassName ) {
        modifiedEntityTypes.add( new EntityType( this, entityClassName ) );
    }


    public String getUsername() {
        return  username;
    }

    public void setUsername( String username){
        this.username = username;
    }


}
