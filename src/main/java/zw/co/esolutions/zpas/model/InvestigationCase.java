package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;


import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 28 Jan 2019
 */
@DiscriminatorColumn(length=100)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "investigation_case")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "investigation_case_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public abstract class InvestigationCase {
    @Id
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    protected String assignmentIdentification;

    protected OffsetDateTime creationDateTime;

    protected String identification;
    @JsonIgnore
    @OneToMany(mappedBy = "investigationCase", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    protected List<InvestigationCaseStatus> status;
    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "investigation_case_party_role",
            joinColumns = @JoinColumn(name = "investigation_case_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "investigation_party_role_id", referencedColumnName = "id"))
    protected List<InvestigationPartyRole> investigationPartyRole;

    @ManyToOne(targetEntity = DuplicateCase.class, fetch = FetchType.LAZY)
    protected DuplicateCase duplicateCaseResolution;
    @JsonIgnore
    @OneToMany(targetEntity = InvestigationResolution.class, mappedBy = "investigationCase", fetch = FetchType.LAZY)
    protected List<InvestigationResolution> investigationResolution;
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(name = "original_investigation_linked_case",
            joinColumns = @JoinColumn(name = "investigation_case_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "original_investigation_case_id", referencedColumnName = "id"))
    protected List<InvestigationCase> originalInvestigationCase;
    @JsonIgnore
    @ManyToMany(mappedBy = "originalInvestigationCase", fetch = FetchType.LAZY)
    protected List<InvestigationCase> linkedCase;

    @OneToOne(targetEntity = Reassignment.class, mappedBy = "reassignedCase", fetch = FetchType.LAZY)
    protected Reassignment reassignment;

    @Override
    public String toString() {
        return "InvestigationCase{}";
    }

    public InvestigationCaseStatus getLatestInvestigationCaseStatus() {
        return this.getStatus().stream().max(Comparator.comparing(InvestigationCaseStatus::getDateCreated)).orElse(new InvestigationCaseStatus());
    }

    public PaymentInvestigationCaseResolution getLatestInvestigationResolution() {
        return this.investigationResolution.stream()
                .filter(investigationResolution1 -> investigationResolution1 instanceof PaymentInvestigationCaseResolution)
                .map(investigationResolution1 -> (PaymentInvestigationCaseResolution) investigationResolution1)
                .sorted(Comparator.comparing(PaymentInvestigationCaseResolution::getDateCreated).reversed())
                .findFirst().orElse(null);
    }
}
