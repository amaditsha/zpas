package zw.co.esolutions.zpas.model.enversaudit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;
import zw.co.esolutions.zpas.security.model.UserRole;

import java.io.Serializable;

@Data
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserRoleAuditResult implements Serializable {
    private UserRole userRole;
    private AuditedRevisionEntity auditedRevisionEntity;

    public UserRoleAuditResult(UserRole userRole, AuditedRevisionEntity auditedRevisionEntity) {
        this.userRole = userRole;
        this.auditedRevisionEntity = auditedRevisionEntity;
    }
}
