package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.ExchangeRateTypeCode;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
//@Indexed
@Table(name = "currency_exchange", indexes = {
        @Index(name = "currency_exchange_status", columnList = "entityStatus"),
        @Index(name = "currency_exchange_date_created", columnList = "dateCreated")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "currency_exchange_analyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class CurrencyExchange implements Auditable {
    @Id
    @Column(length = 50)
    protected String id;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @Version
    protected Long entityVersion;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "original_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "original_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "original_amount_currency_code"))
    })
    protected CurrencyAndAmount originalAmount;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "unit_currency_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "unity_currency_code"))
    })
    protected CurrencyCode unitCurrency;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "quoted_currency_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "quoted_currency_code"))
    })
    protected CurrencyCode quotedCurrency;

    protected BigDecimal exchangeRate;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "amount", column = @Column(name = "resulting_amount_amount")),
            @AttributeOverride(name = "currency.codeName", column = @Column(name = "resulting_amount_currency_code_name")),
            @AttributeOverride(name = "currency.name", column = @Column(name = "resulting_amount_currency_code"))
    })
    protected CurrencyAndAmount resultingAmount;

//    protected CorporateActionEvent relatedCorporateActionEvent;
//    protected SecuritiesBalance currencyExchangeForSecuritiesBalance;

    protected OffsetDateTime quotationDate;

//    protected List<AssetHolding> calculatedAssetValue;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "source_currency_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "source_currency_code"))
    })
    protected CurrencyCode sourceCurrency;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "target_currency_code_name")),
            @AttributeOverride(name = "name", column = @Column(name = "target_currency_code"))
    })
    protected CurrencyCode targetCurrency;

    @JsonIgnore
    @OneToMany(targetEntity = CashEntry.class, mappedBy = "currencyExchange", fetch = FetchType.LAZY)
    protected List<CashEntry> currencyExchangeForCashEntry;

    @ManyToOne(targetEntity = Payment.class, fetch = FetchType.LAZY)
    protected Payment relatedPayment;

    @Enumerated(EnumType.STRING)
    protected ExchangeRateTypeCode rateType;

//    protected LiquidityManagementLimit relatedLimitManagement;
//    protected List<FixingCondition> fixingConditions;
    @ManyToOne(targetEntity = Tax.class, fetch = FetchType.LAZY)
    protected Tax tax;

    @ManyToOne(targetEntity = Invoice.class, fetch = FetchType.LAZY)
    protected Invoice relatedInvoice;
    @ManyToOne(targetEntity = TransactionAdministrator.class, fetch = FetchType.LAZY)
    protected TransactionAdministrator currencyExchangeForTransactionAdministrator;

    @ManyToOne(targetEntity = Account.class, fetch = FetchType.LAZY)
    protected Account reportedAccount;

//    protected CorporateActionCashEntitlement currencyExchangeForCorporateActionCashEntitlement;

    @ManyToOne(targetEntity = PaymentExecution.class, fetch = FetchType.LAZY)
    protected PaymentExecution paymentExecution;

//    protected Quote currencyExchangeForSecuritiesQuote;
//    protected SecuritiesConversion currencyExchangeForSecuritiesConversion;
//    protected CashDistribution currencyExchangeForCashDistribution;

    @OneToOne(mappedBy = "currencyExchange", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    protected Adjustment adjustment;

    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return id;
    }

    @PrePersist
    public void setCurrencyExchangeId(){
        if(id == null)
            this.id = GenerateKey.generateEntityId();
        this.entityStatus = EntityStatus.DRAFT;
    }

    @Override
    public String toString() {
        return "CurrencyExchange{}";
    }
}
