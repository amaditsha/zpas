package zw.co.esolutions.zpas.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public abstract class MMCode {
    @Column
    protected String codeName;
    @Column
    protected String name;
}
