package zw.co.esolutions.zpas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import zw.co.esolutions.zpas.codeset.*;

import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
//Indexed
@Table(name = "status_reason", indexes = {
        @Index(name = "status_reason_status", columnList = "entityStatus"),
        @Index(name = "status_reason_date_created", columnList = "dateCreated"),
        @Index(name = "status_reason_last_updated", columnList = "lastUpdated")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AnalyzerDef(name = "statusreasonanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @org.hibernate.search.annotations.Parameter(name = "language", value = "English")})})
public class StatusReason implements Auditable {
    @Id
    protected String id;

    @Column
    @Enumerated(EnumType.STRING)
    protected EntityStatus entityStatus;

    @CreationTimestamp
    protected OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @ManyToOne(targetEntity = Status.class, fetch = FetchType.LAZY)
    protected Status status;

    protected String reason;

    @JsonIgnore
    @ElementCollection
    protected List<String> additionalInfo;

    @Version
    protected Long entityVersion;

    @Enumerated(EnumType.STRING)
    protected NoReasonCode noSpecifiedReason;

    @Enumerated(EnumType.STRING)
    protected RejectedStatusReasonCode rejectedStatusReason;

    @Enumerated(EnumType.STRING)
    protected PendingFailingReasonCode failingReason;

    @Enumerated(EnumType.STRING)
    protected CancelledStatusReasonV2Code cancellationReason;

    @Enumerated(EnumType.STRING)
    protected PendingFailingReasonCode pendingReason;

    @Enumerated(EnumType.STRING)
    protected RejectionReasonV2Code rejectionReason;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "codeName", column = @Column(name = "acknowledged_accepted_reason_code")),
            @AttributeOverride(name = "name", column = @Column(name = "acknowledged_accepted_reason_name")),
    })
    protected AcknowledgementReasonCode acknowledgedAcceptedReason;

    @OneToOne(targetEntity = RegisteredContract.class, mappedBy = "closureReason", fetch = FetchType.LAZY)
    protected RegisteredContract relatedClosureReason;

    @JsonIgnore
    @OneToMany(targetEntity = GenericIdentification.class, mappedBy = "relatedStatusReason", fetch = FetchType.LAZY)
    protected List<GenericIdentification> dataSourceScheme;


    @Override
    public String getEntityName() {
        return this.getClass().getSimpleName().toUpperCase();
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<>();

        //add auditable attributes
        attributesMap.put("lastUpdated", lastUpdated.format(DateTimeFormatter.ISO_DATE_TIME));

        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return "StatusReasonDto: " + getId();
    }

    @Override
    public String toString() {
        return "StatusReasonDto{}";
    }

    @PrePersist
    public void init() {
        if (id == null) setId(GenerateKey.generateEntityId());
        if (status == null) setEntityStatus(EntityStatus.ACTIVE);
    }
}
