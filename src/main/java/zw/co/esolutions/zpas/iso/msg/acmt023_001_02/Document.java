
package zw.co.esolutions.zpas.iso.msg.acmt023_001_02;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdVrfctnReq" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}IdentificationVerificationRequestV02"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", propOrder = {
    "idVrfctnReq"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02")
public class Document {

    @XmlElement(name = "IdVrfctnReq", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    protected IdentificationVerificationRequestV02 idVrfctnReq;

    /**
     * Gets the value of the idVrfctnReq property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationVerificationRequestV02 }
     *     
     */
    public IdentificationVerificationRequestV02 getIdVrfctnReq() {
        return idVrfctnReq;
    }

    /**
     * Sets the value of the idVrfctnReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationVerificationRequestV02 }
     *     
     */
    public void setIdVrfctnReq(IdentificationVerificationRequestV02 value) {
        this.idVrfctnReq = value;
    }

}
