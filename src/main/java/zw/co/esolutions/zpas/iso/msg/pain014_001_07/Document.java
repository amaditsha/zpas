
package zw.co.esolutions.zpas.iso.msg.pain014_001_07;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdtrPmtActvtnReqStsRpt" type="{urn:iso:std:iso:20022:tech:xsd:pain.014.001.07}CreditorPaymentActivationRequestStatusReportV07"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07", propOrder = {
    "cdtrPmtActvtnReqStsRpt"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07")
public class Document {

    @XmlElement(name = "CdtrPmtActvtnReqStsRpt", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07", required = true)
    protected CreditorPaymentActivationRequestStatusReportV07 cdtrPmtActvtnReqStsRpt;

    /**
     * Gets the value of the cdtrPmtActvtnReqStsRpt property.
     * 
     * @return
     *     possible object is
     *     {@link CreditorPaymentActivationRequestStatusReportV07 }
     *     
     */
    public CreditorPaymentActivationRequestStatusReportV07 getCdtrPmtActvtnReqStsRpt() {
        return cdtrPmtActvtnReqStsRpt;
    }

    /**
     * Sets the value of the cdtrPmtActvtnReqStsRpt property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditorPaymentActivationRequestStatusReportV07 }
     *     
     */
    public void setCdtrPmtActvtnReqStsRpt(CreditorPaymentActivationRequestStatusReportV07 value) {
        this.cdtrPmtActvtnReqStsRpt = value;
    }

}
