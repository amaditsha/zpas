
package zw.co.esolutions.zpas.iso.msg.camt029_001_08;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RsltnOfInvstgtn" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}ResolutionOfInvestigationV08"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08", propOrder = {
    "rsltnOfInvstgtn"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
public class Document {

    @XmlElement(name = "RsltnOfInvstgtn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08", required = true)
    protected ResolutionOfInvestigationV08 rsltnOfInvstgtn;

    /**
     * Gets the value of the rsltnOfInvstgtn property.
     * 
     * @return
     *     possible object is
     *     {@link ResolutionOfInvestigationV08 }
     *     
     */
    public ResolutionOfInvestigationV08 getRsltnOfInvstgtn() {
        return rsltnOfInvstgtn;
    }

    /**
     * Sets the value of the rsltnOfInvstgtn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResolutionOfInvestigationV08 }
     *     
     */
    public void setRsltnOfInvstgtn(ResolutionOfInvestigationV08 value) {
        this.rsltnOfInvstgtn = value;
    }

}
