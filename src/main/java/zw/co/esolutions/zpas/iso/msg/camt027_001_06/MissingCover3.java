
package zw.co.esolutions.zpas.iso.msg.camt027_001_06;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MissingCover3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MissingCover3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MssngCoverInd" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.06}YesNoIndicator"/>
 *         &lt;element name="CoverCrrctn" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.06}SettlementInstruction3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MissingCover3", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06", propOrder = {
    "mssngCoverInd",
    "coverCrrctn"
})
public class MissingCover3 {

    @XmlElement(name = "MssngCoverInd", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06")
    protected boolean mssngCoverInd;
    @XmlElement(name = "CoverCrrctn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06")
    protected SettlementInstruction3 coverCrrctn;

    /**
     * Gets the value of the mssngCoverInd property.
     * 
     */
    public boolean isMssngCoverInd() {
        return mssngCoverInd;
    }

    /**
     * Sets the value of the mssngCoverInd property.
     * 
     */
    public void setMssngCoverInd(boolean value) {
        this.mssngCoverInd = value;
    }

    /**
     * Gets the value of the coverCrrctn property.
     * 
     * @return
     *     possible object is
     *     {@link SettlementInstruction3 }
     *     
     */
    public SettlementInstruction3 getCoverCrrctn() {
        return coverCrrctn;
    }

    /**
     * Sets the value of the coverCrrctn property.
     * 
     * @param value
     *     allowed object is
     *     {@link SettlementInstruction3 }
     *     
     */
    public void setCoverCrrctn(SettlementInstruction3 value) {
        this.coverCrrctn = value;
    }

}
