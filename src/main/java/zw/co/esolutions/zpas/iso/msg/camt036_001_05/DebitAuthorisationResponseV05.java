
package zw.co.esolutions.zpas.iso.msg.camt036_001_05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DebitAuthorisationResponseV05 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DebitAuthorisationResponseV05">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Assgnmt" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}CaseAssignment5"/>
 *         &lt;element name="Case" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}Case5" minOccurs="0"/>
 *         &lt;element name="Conf" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}DebitAuthorisationConfirmation2"/>
 *         &lt;element name="SplmtryData" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}SupplementaryData1" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitAuthorisationResponseV05", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05", propOrder = {
    "assgnmt",
    "_case",
    "conf",
    "splmtryData"
})
public class DebitAuthorisationResponseV05 {

    @XmlElement(name = "Assgnmt", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05", required = true)
    protected CaseAssignment5 assgnmt;
    @XmlElement(name = "Case", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05")
    protected Case5 _case;
    @XmlElement(name = "Conf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05", required = true)
    protected DebitAuthorisationConfirmation2 conf;
    @XmlElement(name = "SplmtryData", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05")
    protected List<SupplementaryData1> splmtryData;

    /**
     * Gets the value of the assgnmt property.
     * 
     * @return
     *     possible object is
     *     {@link CaseAssignment5 }
     *     
     */
    public CaseAssignment5 getAssgnmt() {
        return assgnmt;
    }

    /**
     * Sets the value of the assgnmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseAssignment5 }
     *     
     */
    public void setAssgnmt(CaseAssignment5 value) {
        this.assgnmt = value;
    }

    /**
     * Gets the value of the case property.
     * 
     * @return
     *     possible object is
     *     {@link Case5 }
     *     
     */
    public Case5 getCase() {
        return _case;
    }

    /**
     * Sets the value of the case property.
     * 
     * @param value
     *     allowed object is
     *     {@link Case5 }
     *     
     */
    public void setCase(Case5 value) {
        this._case = value;
    }

    /**
     * Gets the value of the conf property.
     * 
     * @return
     *     possible object is
     *     {@link DebitAuthorisationConfirmation2 }
     *     
     */
    public DebitAuthorisationConfirmation2 getConf() {
        return conf;
    }

    /**
     * Sets the value of the conf property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitAuthorisationConfirmation2 }
     *     
     */
    public void setConf(DebitAuthorisationConfirmation2 value) {
        this.conf = value;
    }

    /**
     * Gets the value of the splmtryData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splmtryData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplmtryData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupplementaryData1 }
     * 
     * 
     */
    public List<SupplementaryData1> getSplmtryData() {
        if (splmtryData == null) {
            splmtryData = new ArrayList<SupplementaryData1>();
        }
        return this.splmtryData;
    }

}
