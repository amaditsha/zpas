
package zw.co.esolutions.zpas.iso.msg.acmt022_001_02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificationModification2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificationModification2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}Max35Text"/>
 *         &lt;element name="OrgnlPtyAndAcctId" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}IdentificationInformation2" minOccurs="0"/>
 *         &lt;element name="UpdtdPtyAndAcctId" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}IdentificationInformation2"/>
 *         &lt;element name="AddtlInf" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}Max140Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationModification2", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02", propOrder = {
    "id",
    "orgnlPtyAndAcctId",
    "updtdPtyAndAcctId",
    "addtlInf"
})
public class IdentificationModification2 {

    @XmlElement(name = "Id", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02", required = true)
    protected String id;
    @XmlElement(name = "OrgnlPtyAndAcctId", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02")
    protected IdentificationInformation2 orgnlPtyAndAcctId;
    @XmlElement(name = "UpdtdPtyAndAcctId", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02", required = true)
    protected IdentificationInformation2 updtdPtyAndAcctId;
    @XmlElement(name = "AddtlInf", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02")
    protected String addtlInf;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the orgnlPtyAndAcctId property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationInformation2 }
     *     
     */
    public IdentificationInformation2 getOrgnlPtyAndAcctId() {
        return orgnlPtyAndAcctId;
    }

    /**
     * Sets the value of the orgnlPtyAndAcctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationInformation2 }
     *     
     */
    public void setOrgnlPtyAndAcctId(IdentificationInformation2 value) {
        this.orgnlPtyAndAcctId = value;
    }

    /**
     * Gets the value of the updtdPtyAndAcctId property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationInformation2 }
     *     
     */
    public IdentificationInformation2 getUpdtdPtyAndAcctId() {
        return updtdPtyAndAcctId;
    }

    /**
     * Sets the value of the updtdPtyAndAcctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationInformation2 }
     *     
     */
    public void setUpdtdPtyAndAcctId(IdentificationInformation2 value) {
        this.updtdPtyAndAcctId = value;
    }

    /**
     * Gets the value of the addtlInf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddtlInf() {
        return addtlInf;
    }

    /**
     * Sets the value of the addtlInf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddtlInf(String value) {
        this.addtlInf = value;
    }

}
