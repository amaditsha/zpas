
package zw.co.esolutions.zpas.iso.msg.camt030_001_05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseForwardingNotification3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseForwardingNotification3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Justfn" type="{urn:iso:std:iso:20022:tech:xsd:camt.030.001.05}CaseForwardingNotification3Code"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseForwardingNotification3", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.030.001.05", propOrder = {
    "justfn"
})
public class CaseForwardingNotification3 {

    @XmlElement(name = "Justfn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.030.001.05", required = true)
    @XmlSchemaType(name = "string")
    protected CaseForwardingNotification3Code justfn;

    /**
     * Gets the value of the justfn property.
     * 
     * @return
     *     possible object is
     *     {@link CaseForwardingNotification3Code }
     *     
     */
    public CaseForwardingNotification3Code getJustfn() {
        return justfn;
    }

    /**
     * Sets the value of the justfn property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseForwardingNotification3Code }
     *     
     */
    public void setJustfn(CaseForwardingNotification3Code value) {
        this.justfn = value;
    }

}
