
package zw.co.esolutions.zpas.iso.msg.camt036_001_05;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DbtAuthstnRspn" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}DebitAuthorisationResponseV05"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05", propOrder = {
    "dbtAuthstnRspn"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05")
public class Document {

    @XmlElement(name = "DbtAuthstnRspn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05", required = true)
    protected DebitAuthorisationResponseV05 dbtAuthstnRspn;

    /**
     * Gets the value of the dbtAuthstnRspn property.
     * 
     * @return
     *     possible object is
     *     {@link DebitAuthorisationResponseV05 }
     *     
     */
    public DebitAuthorisationResponseV05 getDbtAuthstnRspn() {
        return dbtAuthstnRspn;
    }

    /**
     * Sets the value of the dbtAuthstnRspn property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitAuthorisationResponseV05 }
     *     
     */
    public void setDbtAuthstnRspn(DebitAuthorisationResponseV05 value) {
        this.dbtAuthstnRspn = value;
    }

}
