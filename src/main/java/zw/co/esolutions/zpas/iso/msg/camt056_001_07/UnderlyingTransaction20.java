
package zw.co.esolutions.zpas.iso.msg.camt056_001_07;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnderlyingTransaction20 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnderlyingTransaction20">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrgnlGrpInfAndCxl" type="{urn:iso:std:iso:20022:tech:xsd:camt.056.001.07}OriginalGroupHeader10" minOccurs="0"/>
 *         &lt;element name="TxInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.056.001.07}PaymentTransaction89" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnderlyingTransaction20", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.056.001.07", propOrder = {
    "orgnlGrpInfAndCxl",
    "txInf"
})
public class UnderlyingTransaction20 {

    @XmlElement(name = "OrgnlGrpInfAndCxl", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.056.001.07")
    protected OriginalGroupHeader10 orgnlGrpInfAndCxl;
    @XmlElement(name = "TxInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.056.001.07")
    protected List<PaymentTransaction89> txInf;

    /**
     * Gets the value of the orgnlGrpInfAndCxl property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalGroupHeader10 }
     *     
     */
    public OriginalGroupHeader10 getOrgnlGrpInfAndCxl() {
        return orgnlGrpInfAndCxl;
    }

    /**
     * Sets the value of the orgnlGrpInfAndCxl property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalGroupHeader10 }
     *     
     */
    public void setOrgnlGrpInfAndCxl(OriginalGroupHeader10 value) {
        this.orgnlGrpInfAndCxl = value;
    }

    /**
     * Gets the value of the txInf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the txInf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTxInf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentTransaction89 }
     * 
     * 
     */
    public List<PaymentTransaction89> getTxInf() {
        if (txInf == null) {
            txInf = new ArrayList<PaymentTransaction89>();
        }
        return this.txInf;
    }

}
