
package zw.co.esolutions.zpas.iso.msg.pacs007_001_08;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FIToFIPmtRvsl" type="{urn:iso:std:iso:20022:tech:xsd:pacs.007.001.08}FIToFIPaymentReversalV08"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pacs.007.001.08", propOrder = {
    "fiToFIPmtRvsl"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pacs.007.001.08")
public class Document {

    @XmlElement(name = "FIToFIPmtRvsl", namespace = "urn:iso:std:iso:20022:tech:xsd:pacs.007.001.08", required = true)
    protected FIToFIPaymentReversalV08 fiToFIPmtRvsl;

    /**
     * Gets the value of the fiToFIPmtRvsl property.
     * 
     * @return
     *     possible object is
     *     {@link FIToFIPaymentReversalV08 }
     *     
     */
    public FIToFIPaymentReversalV08 getFIToFIPmtRvsl() {
        return fiToFIPmtRvsl;
    }

    /**
     * Sets the value of the fiToFIPmtRvsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link FIToFIPaymentReversalV08 }
     *     
     */
    public void setFIToFIPmtRvsl(FIToFIPaymentReversalV08 value) {
        this.fiToFIPmtRvsl = value;
    }

}
