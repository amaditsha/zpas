
package zw.co.esolutions.zpas.iso.msg.camt029_001_08;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResolutionOfInvestigationV08 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResolutionOfInvestigationV08">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Assgnmt" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}CaseAssignment4"/>
 *         &lt;element name="RslvdCase" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}Case4" minOccurs="0"/>
 *         &lt;element name="Sts" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}InvestigationStatus4Choice"/>
 *         &lt;element name="CxlDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}UnderlyingTransaction19" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ModDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}PaymentTransaction90" minOccurs="0"/>
 *         &lt;element name="ClmNonRctDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}ClaimNonReceipt1Choice" minOccurs="0"/>
 *         &lt;element name="StmtDtls" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}StatementResolutionEntry3" minOccurs="0"/>
 *         &lt;element name="CrrctnTx" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}CorrectiveTransaction3Choice" minOccurs="0"/>
 *         &lt;element name="RsltnRltdInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}ResolutionInformation2" minOccurs="0"/>
 *         &lt;element name="SplmtryData" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}SupplementaryData1" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResolutionOfInvestigationV08", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08", propOrder = {
    "assgnmt",
    "rslvdCase",
    "sts",
    "cxlDtls",
    "modDtls",
    "clmNonRctDtls",
    "stmtDtls",
    "crrctnTx",
    "rsltnRltdInf",
    "splmtryData"
})
public class ResolutionOfInvestigationV08 {

    @XmlElement(name = "Assgnmt", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08", required = true)
    protected CaseAssignment4 assgnmt;
    @XmlElement(name = "RslvdCase", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected Case4 rslvdCase;
    @XmlElement(name = "Sts", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08", required = true)
    protected InvestigationStatus4Choice sts;
    @XmlElement(name = "CxlDtls", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected List<UnderlyingTransaction19> cxlDtls;
    @XmlElement(name = "ModDtls", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected PaymentTransaction90 modDtls;
    @XmlElement(name = "ClmNonRctDtls", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected ClaimNonReceipt1Choice clmNonRctDtls;
    @XmlElement(name = "StmtDtls", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected StatementResolutionEntry3 stmtDtls;
    @XmlElement(name = "CrrctnTx", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected CorrectiveTransaction3Choice crrctnTx;
    @XmlElement(name = "RsltnRltdInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected ResolutionInformation2 rsltnRltdInf;
    @XmlElement(name = "SplmtryData", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected List<SupplementaryData1> splmtryData;

    /**
     * Gets the value of the assgnmt property.
     * 
     * @return
     *     possible object is
     *     {@link CaseAssignment4 }
     *     
     */
    public CaseAssignment4 getAssgnmt() {
        return assgnmt;
    }

    /**
     * Sets the value of the assgnmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseAssignment4 }
     *     
     */
    public void setAssgnmt(CaseAssignment4 value) {
        this.assgnmt = value;
    }

    /**
     * Gets the value of the rslvdCase property.
     * 
     * @return
     *     possible object is
     *     {@link Case4 }
     *     
     */
    public Case4 getRslvdCase() {
        return rslvdCase;
    }

    /**
     * Sets the value of the rslvdCase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Case4 }
     *     
     */
    public void setRslvdCase(Case4 value) {
        this.rslvdCase = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link InvestigationStatus4Choice }
     *     
     */
    public InvestigationStatus4Choice getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvestigationStatus4Choice }
     *     
     */
    public void setSts(InvestigationStatus4Choice value) {
        this.sts = value;
    }

    /**
     * Gets the value of the cxlDtls property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cxlDtls property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCxlDtls().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UnderlyingTransaction19 }
     * 
     * 
     */
    public List<UnderlyingTransaction19> getCxlDtls() {
        if (cxlDtls == null) {
            cxlDtls = new ArrayList<UnderlyingTransaction19>();
        }
        return this.cxlDtls;
    }

    /**
     * Gets the value of the modDtls property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTransaction90 }
     *     
     */
    public PaymentTransaction90 getModDtls() {
        return modDtls;
    }

    /**
     * Sets the value of the modDtls property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTransaction90 }
     *     
     */
    public void setModDtls(PaymentTransaction90 value) {
        this.modDtls = value;
    }

    /**
     * Gets the value of the clmNonRctDtls property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimNonReceipt1Choice }
     *     
     */
    public ClaimNonReceipt1Choice getClmNonRctDtls() {
        return clmNonRctDtls;
    }

    /**
     * Sets the value of the clmNonRctDtls property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimNonReceipt1Choice }
     *     
     */
    public void setClmNonRctDtls(ClaimNonReceipt1Choice value) {
        this.clmNonRctDtls = value;
    }

    /**
     * Gets the value of the stmtDtls property.
     * 
     * @return
     *     possible object is
     *     {@link StatementResolutionEntry3 }
     *     
     */
    public StatementResolutionEntry3 getStmtDtls() {
        return stmtDtls;
    }

    /**
     * Sets the value of the stmtDtls property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatementResolutionEntry3 }
     *     
     */
    public void setStmtDtls(StatementResolutionEntry3 value) {
        this.stmtDtls = value;
    }

    /**
     * Gets the value of the crrctnTx property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectiveTransaction3Choice }
     *     
     */
    public CorrectiveTransaction3Choice getCrrctnTx() {
        return crrctnTx;
    }

    /**
     * Sets the value of the crrctnTx property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectiveTransaction3Choice }
     *     
     */
    public void setCrrctnTx(CorrectiveTransaction3Choice value) {
        this.crrctnTx = value;
    }

    /**
     * Gets the value of the rsltnRltdInf property.
     * 
     * @return
     *     possible object is
     *     {@link ResolutionInformation2 }
     *     
     */
    public ResolutionInformation2 getRsltnRltdInf() {
        return rsltnRltdInf;
    }

    /**
     * Sets the value of the rsltnRltdInf property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResolutionInformation2 }
     *     
     */
    public void setRsltnRltdInf(ResolutionInformation2 value) {
        this.rsltnRltdInf = value;
    }

    /**
     * Gets the value of the splmtryData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splmtryData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplmtryData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupplementaryData1 }
     * 
     * 
     */
    public List<SupplementaryData1> getSplmtryData() {
        if (splmtryData == null) {
            splmtryData = new ArrayList<SupplementaryData1>();
        }
        return this.splmtryData;
    }

}
