
package zw.co.esolutions.zpas.iso.msg.pain013_001_07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentCondition1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentCondition1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AmtModAllwd" type="{urn:iso:std:iso:20022:tech:xsd:pain.013.001.07}TrueFalseIndicator"/>
 *         &lt;element name="EarlyPmtAllwd" type="{urn:iso:std:iso:20022:tech:xsd:pain.013.001.07}TrueFalseIndicator"/>
 *         &lt;element name="DelyPnlty" type="{urn:iso:std:iso:20022:tech:xsd:pain.013.001.07}Max140Text" minOccurs="0"/>
 *         &lt;element name="ImdtPmtRbt" type="{urn:iso:std:iso:20022:tech:xsd:pain.013.001.07}AmountOrRate1Choice" minOccurs="0"/>
 *         &lt;element name="GrntedPmtReqd" type="{urn:iso:std:iso:20022:tech:xsd:pain.013.001.07}TrueFalseIndicator"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentCondition1", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07", propOrder = {
    "amtModAllwd",
    "earlyPmtAllwd",
    "delyPnlty",
    "imdtPmtRbt",
    "grntedPmtReqd"
})
public class PaymentCondition1 {

    @XmlElement(name = "AmtModAllwd", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07")
    protected boolean amtModAllwd;
    @XmlElement(name = "EarlyPmtAllwd", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07")
    protected boolean earlyPmtAllwd;
    @XmlElement(name = "DelyPnlty", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07")
    protected String delyPnlty;
    @XmlElement(name = "ImdtPmtRbt", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07")
    protected AmountOrRate1Choice imdtPmtRbt;
    @XmlElement(name = "GrntedPmtReqd", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07")
    protected boolean grntedPmtReqd;

    /**
     * Gets the value of the amtModAllwd property.
     * 
     */
    public boolean isAmtModAllwd() {
        return amtModAllwd;
    }

    /**
     * Sets the value of the amtModAllwd property.
     * 
     */
    public void setAmtModAllwd(boolean value) {
        this.amtModAllwd = value;
    }

    /**
     * Gets the value of the earlyPmtAllwd property.
     * 
     */
    public boolean isEarlyPmtAllwd() {
        return earlyPmtAllwd;
    }

    /**
     * Sets the value of the earlyPmtAllwd property.
     * 
     */
    public void setEarlyPmtAllwd(boolean value) {
        this.earlyPmtAllwd = value;
    }

    /**
     * Gets the value of the delyPnlty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelyPnlty() {
        return delyPnlty;
    }

    /**
     * Sets the value of the delyPnlty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelyPnlty(String value) {
        this.delyPnlty = value;
    }

    /**
     * Gets the value of the imdtPmtRbt property.
     * 
     * @return
     *     possible object is
     *     {@link AmountOrRate1Choice }
     *     
     */
    public AmountOrRate1Choice getImdtPmtRbt() {
        return imdtPmtRbt;
    }

    /**
     * Sets the value of the imdtPmtRbt property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountOrRate1Choice }
     *     
     */
    public void setImdtPmtRbt(AmountOrRate1Choice value) {
        this.imdtPmtRbt = value;
    }

    /**
     * Gets the value of the grntedPmtReqd property.
     * 
     */
    public boolean isGrntedPmtReqd() {
        return grntedPmtReqd;
    }

    /**
     * Sets the value of the grntedPmtReqd property.
     * 
     */
    public void setGrntedPmtReqd(boolean value) {
        this.grntedPmtReqd = value;
    }

}
