
package zw.co.esolutions.zpas.iso.msg.pain014_001_07;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AmountOrRate1Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AmountOrRate1Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Amt" type="{urn:iso:std:iso:20022:tech:xsd:pain.014.001.07}ActiveCurrencyAndAmount"/>
 *         &lt;element name="Rate" type="{urn:iso:std:iso:20022:tech:xsd:pain.014.001.07}PercentageRate"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AmountOrRate1Choice", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07", propOrder = {
    "amt",
    "rate"
})
public class AmountOrRate1Choice {

    @XmlElement(name = "Amt", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07")
    protected ActiveCurrencyAndAmount amt;
    @XmlElement(name = "Rate", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07")
    protected BigDecimal rate;

    /**
     * Gets the value of the amt property.
     * 
     * @return
     *     possible object is
     *     {@link ActiveCurrencyAndAmount }
     *     
     */
    public ActiveCurrencyAndAmount getAmt() {
        return amt;
    }

    /**
     * Sets the value of the amt property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActiveCurrencyAndAmount }
     *     
     */
    public void setAmt(ActiveCurrencyAndAmount value) {
        this.amt = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

}
