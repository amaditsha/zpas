
package zw.co.esolutions.zpas.iso.msg.camt027_001_07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MissingCover4 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MissingCover4">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MssngCoverInd" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.07}YesNoIndicator"/>
 *         &lt;element name="CoverCrrctn" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.07}SettlementInstruction6" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MissingCover4", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.07", propOrder = {
    "mssngCoverInd",
    "coverCrrctn"
})
public class MissingCover4 {

    @XmlElement(name = "MssngCoverInd", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.07")
    protected boolean mssngCoverInd;
    @XmlElement(name = "CoverCrrctn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.07")
    protected SettlementInstruction6 coverCrrctn;

    /**
     * Gets the value of the mssngCoverInd property.
     * 
     */
    public boolean isMssngCoverInd() {
        return mssngCoverInd;
    }

    /**
     * Sets the value of the mssngCoverInd property.
     * 
     */
    public void setMssngCoverInd(boolean value) {
        this.mssngCoverInd = value;
    }

    /**
     * Gets the value of the coverCrrctn property.
     * 
     * @return
     *     possible object is
     *     {@link SettlementInstruction6 }
     *     
     */
    public SettlementInstruction6 getCoverCrrctn() {
        return coverCrrctn;
    }

    /**
     * Sets the value of the coverCrrctn property.
     * 
     * @param value
     *     allowed object is
     *     {@link SettlementInstruction6 }
     *     
     */
    public void setCoverCrrctn(SettlementInstruction6 value) {
        this.coverCrrctn = value;
    }

}
