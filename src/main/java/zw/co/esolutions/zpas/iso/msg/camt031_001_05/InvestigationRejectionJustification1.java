
package zw.co.esolutions.zpas.iso.msg.camt031_001_05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvestigationRejectionJustification1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvestigationRejectionJustification1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RjctnRsn" type="{urn:iso:std:iso:20022:tech:xsd:camt.031.001.05}InvestigationRejection1Code"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvestigationRejectionJustification1", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.031.001.05", propOrder = {
    "rjctnRsn"
})
public class InvestigationRejectionJustification1 {

    @XmlElement(name = "RjctnRsn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.031.001.05", required = true)
    @XmlSchemaType(name = "string")
    protected InvestigationRejection1Code rjctnRsn;

    /**
     * Gets the value of the rjctnRsn property.
     * 
     * @return
     *     possible object is
     *     {@link InvestigationRejection1Code }
     *     
     */
    public InvestigationRejection1Code getRjctnRsn() {
        return rjctnRsn;
    }

    /**
     * Sets the value of the rjctnRsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvestigationRejection1Code }
     *     
     */
    public void setRjctnRsn(InvestigationRejection1Code value) {
        this.rjctnRsn = value;
    }

}
