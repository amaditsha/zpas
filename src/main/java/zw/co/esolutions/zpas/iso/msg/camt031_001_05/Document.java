
package zw.co.esolutions.zpas.iso.msg.camt031_001_05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RjctInvstgtn" type="{urn:iso:std:iso:20022:tech:xsd:camt.031.001.05}RejectInvestigationV05"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.031.001.05", propOrder = {
    "rjctInvstgtn"
})
public class Document {

    @XmlElement(name = "RjctInvstgtn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.031.001.05", required = true)
    protected RejectInvestigationV05 rjctInvstgtn;

    /**
     * Gets the value of the rjctInvstgtn property.
     * 
     * @return
     *     possible object is
     *     {@link RejectInvestigationV05 }
     *     
     */
    public RejectInvestigationV05 getRjctInvstgtn() {
        return rjctInvstgtn;
    }

    /**
     * Sets the value of the rjctInvstgtn property.
     * 
     * @param value
     *     allowed object is
     *     {@link RejectInvestigationV05 }
     *     
     */
    public void setRjctInvstgtn(RejectInvestigationV05 value) {
        this.rjctInvstgtn = value;
    }

}
