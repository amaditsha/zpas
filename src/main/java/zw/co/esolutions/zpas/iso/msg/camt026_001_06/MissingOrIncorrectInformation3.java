
package zw.co.esolutions.zpas.iso.msg.camt026_001_06;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MissingOrIncorrectInformation3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MissingOrIncorrectInformation3">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AMLReq" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}AMLIndicator" minOccurs="0"/>
 *         &lt;element name="MssngInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}UnableToApplyMissing1" maxOccurs="10" minOccurs="0"/>
 *         &lt;element name="IncrrctInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}UnableToApplyIncorrect1" maxOccurs="10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MissingOrIncorrectInformation3", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06", propOrder = {
    "amlReq",
    "mssngInf",
    "incrrctInf"
})
public class MissingOrIncorrectInformation3 {

    @XmlElement(name = "AMLReq", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected Boolean amlReq;
    @XmlElement(name = "MssngInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected List<UnableToApplyMissing1> mssngInf;
    @XmlElement(name = "IncrrctInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected List<UnableToApplyIncorrect1> incrrctInf;

    /**
     * Gets the value of the amlReq property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAMLReq() {
        return amlReq;
    }

    /**
     * Sets the value of the amlReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAMLReq(Boolean value) {
        this.amlReq = value;
    }

    /**
     * Gets the value of the mssngInf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mssngInf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMssngInf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UnableToApplyMissing1 }
     * 
     * 
     */
    public List<UnableToApplyMissing1> getMssngInf() {
        if (mssngInf == null) {
            mssngInf = new ArrayList<UnableToApplyMissing1>();
        }
        return this.mssngInf;
    }

    /**
     * Gets the value of the incrrctInf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the incrrctInf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncrrctInf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UnableToApplyIncorrect1 }
     * 
     * 
     */
    public List<UnableToApplyIncorrect1> getIncrrctInf() {
        if (incrrctInf == null) {
            incrrctInf = new ArrayList<UnableToApplyIncorrect1>();
        }
        return this.incrrctInf;
    }

}
