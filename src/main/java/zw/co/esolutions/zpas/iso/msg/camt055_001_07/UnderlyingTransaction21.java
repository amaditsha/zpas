
package zw.co.esolutions.zpas.iso.msg.camt055_001_07;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnderlyingTransaction21 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnderlyingTransaction21">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrgnlGrpInfAndCxl" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}OriginalGroupHeader10" minOccurs="0"/>
 *         &lt;element name="OrgnlPmtInfAndCxl" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}OriginalPaymentInstruction29" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnderlyingTransaction21", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07", propOrder = {
    "orgnlGrpInfAndCxl",
    "orgnlPmtInfAndCxl"
})
public class UnderlyingTransaction21 {

    @XmlElement(name = "OrgnlGrpInfAndCxl", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected OriginalGroupHeader10 orgnlGrpInfAndCxl;
    @XmlElement(name = "OrgnlPmtInfAndCxl", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected List<OriginalPaymentInstruction29> orgnlPmtInfAndCxl;

    /**
     * Gets the value of the orgnlGrpInfAndCxl property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalGroupHeader10 }
     *     
     */
    public OriginalGroupHeader10 getOrgnlGrpInfAndCxl() {
        return orgnlGrpInfAndCxl;
    }

    /**
     * Sets the value of the orgnlGrpInfAndCxl property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalGroupHeader10 }
     *     
     */
    public void setOrgnlGrpInfAndCxl(OriginalGroupHeader10 value) {
        this.orgnlGrpInfAndCxl = value;
    }

    /**
     * Gets the value of the orgnlPmtInfAndCxl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orgnlPmtInfAndCxl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrgnlPmtInfAndCxl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginalPaymentInstruction29 }
     * 
     * 
     */
    public List<OriginalPaymentInstruction29> getOrgnlPmtInfAndCxl() {
        if (orgnlPmtInfAndCxl == null) {
            orgnlPmtInfAndCxl = new ArrayList<OriginalPaymentInstruction29>();
        }
        return this.orgnlPmtInfAndCxl;
    }

}
