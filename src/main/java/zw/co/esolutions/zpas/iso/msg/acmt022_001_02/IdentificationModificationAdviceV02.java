
package zw.co.esolutions.zpas.iso.msg.acmt022_001_02;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificationModificationAdviceV02 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificationModificationAdviceV02">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Assgnmt" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}IdentificationAssignment2"/>
 *         &lt;element name="OrgnlTxRef" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}OriginalTransactionReference18" minOccurs="0"/>
 *         &lt;element name="Mod" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}IdentificationModification2" maxOccurs="unbounded"/>
 *         &lt;element name="SplmtryData" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}SupplementaryData1" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationModificationAdviceV02", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02", propOrder = {
    "assgnmt",
    "orgnlTxRef",
    "mod",
    "splmtryData"
})
public class IdentificationModificationAdviceV02 {

    @XmlElement(name = "Assgnmt", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02", required = true)
    protected IdentificationAssignment2 assgnmt;
    @XmlElement(name = "OrgnlTxRef", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02")
    protected OriginalTransactionReference18 orgnlTxRef;
    @XmlElement(name = "Mod", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02", required = true)
    protected List<IdentificationModification2> mod;
    @XmlElement(name = "SplmtryData", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02")
    protected List<SupplementaryData1> splmtryData;

    /**
     * Gets the value of the assgnmt property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationAssignment2 }
     *     
     */
    public IdentificationAssignment2 getAssgnmt() {
        return assgnmt;
    }

    /**
     * Sets the value of the assgnmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationAssignment2 }
     *     
     */
    public void setAssgnmt(IdentificationAssignment2 value) {
        this.assgnmt = value;
    }

    /**
     * Gets the value of the orgnlTxRef property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalTransactionReference18 }
     *     
     */
    public OriginalTransactionReference18 getOrgnlTxRef() {
        return orgnlTxRef;
    }

    /**
     * Sets the value of the orgnlTxRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalTransactionReference18 }
     *     
     */
    public void setOrgnlTxRef(OriginalTransactionReference18 value) {
        this.orgnlTxRef = value;
    }

    /**
     * Gets the value of the mod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentificationModification2 }
     * 
     * 
     */
    public List<IdentificationModification2> getMod() {
        if (mod == null) {
            mod = new ArrayList<IdentificationModification2>();
        }
        return this.mod;
    }

    /**
     * Gets the value of the splmtryData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splmtryData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplmtryData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupplementaryData1 }
     * 
     * 
     */
    public List<SupplementaryData1> getSplmtryData() {
        if (splmtryData == null) {
            splmtryData = new ArrayList<SupplementaryData1>();
        }
        return this.splmtryData;
    }

}
