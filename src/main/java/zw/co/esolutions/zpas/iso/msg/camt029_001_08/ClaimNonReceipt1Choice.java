
package zw.co.esolutions.zpas.iso.msg.camt029_001_08;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClaimNonReceipt1Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimNonReceipt1Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Accptd" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}ClaimNonReceipt1"/>
 *         &lt;element name="Rjctd" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}ClaimNonReceiptRejectReason1Choice"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimNonReceipt1Choice", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08", propOrder = {
    "accptd",
    "rjctd"
})
public class ClaimNonReceipt1Choice {

    @XmlElement(name = "Accptd", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected ClaimNonReceipt1 accptd;
    @XmlElement(name = "Rjctd", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected ClaimNonReceiptRejectReason1Choice rjctd;

    /**
     * Gets the value of the accptd property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimNonReceipt1 }
     *     
     */
    public ClaimNonReceipt1 getAccptd() {
        return accptd;
    }

    /**
     * Sets the value of the accptd property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimNonReceipt1 }
     *     
     */
    public void setAccptd(ClaimNonReceipt1 value) {
        this.accptd = value;
    }

    /**
     * Gets the value of the rjctd property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimNonReceiptRejectReason1Choice }
     *     
     */
    public ClaimNonReceiptRejectReason1Choice getRjctd() {
        return rjctd;
    }

    /**
     * Sets the value of the rjctd property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimNonReceiptRejectReason1Choice }
     *     
     */
    public void setRjctd(ClaimNonReceiptRejectReason1Choice value) {
        this.rjctd = value;
    }

}
