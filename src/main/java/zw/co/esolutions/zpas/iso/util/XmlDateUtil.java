package zw.co.esolutions.zpas.iso.util;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.GregorianCalendar;

public class XmlDateUtil {

    public static XMLGregorianCalendar getXmlDate(OffsetDateTime offsetDateTime) {
        try {
            GregorianCalendar gCalendar = new GregorianCalendar();
            XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory
                    .newInstance().newXMLGregorianCalendar(gCalendar);
            xmlGregorianCalendar.setYear(offsetDateTime.getYear());
            xmlGregorianCalendar.setMonth(offsetDateTime.getMonthValue());
            xmlGregorianCalendar.setDay(offsetDateTime.getDayOfMonth());
            xmlGregorianCalendar.setHour(offsetDateTime.getHour());
            xmlGregorianCalendar.setMinute(offsetDateTime.getMinute());
            xmlGregorianCalendar.setSecond(offsetDateTime.getSecond());

            return xmlGregorianCalendar;
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

//    public static XMLGregorianCalendar getXmlDateWithNoTime(OffsetDateTime offsetDateTime) {
//        try {
//            GregorianCalendar gCalendar = new GregorianCalendar();
//            XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory
//                    .newInstance().newXMLGregorianCalendar(gCalendar);
//
//            LocalDate localDate = offsetDateTime.toLocalDate();
//
//            xmlGregorianCalendar.setYear(localDate.getYear());
//            xmlGregorianCalendar.setMonth(localDate.getMonthValue());
//            xmlGregorianCalendar.setDay(localDate.getDayOfMonth());
//
//            return xmlGregorianCalendar;
//        } catch (DatatypeConfigurationException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    public static XMLGregorianCalendar getXmlTimeNoDate(OffsetDateTime offsetDateTime) {
        try {
            GregorianCalendar gCalendar = new GregorianCalendar();
            XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory
                    .newInstance().newXMLGregorianCalendar(gCalendar);

            xmlGregorianCalendar.setHour(offsetDateTime.getHour());
            xmlGregorianCalendar.setMinute(offsetDateTime.getMinute());
            xmlGregorianCalendar.setSecond(offsetDateTime.getSecond());

            return xmlGregorianCalendar;
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static XMLGregorianCalendar getXmlDateNoTime(OffsetDateTime offsetDateTime) {
        try {
            GregorianCalendar gCalendar = new GregorianCalendar();
            XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory
                    .newInstance().newXMLGregorianCalendar(gCalendar);
            xmlGregorianCalendar.setYear(offsetDateTime.getYear());
            xmlGregorianCalendar.setMonth(offsetDateTime.getMonthValue());
            xmlGregorianCalendar.setDay(offsetDateTime.getDayOfMonth());
            xmlGregorianCalendar.setHour(0);
            xmlGregorianCalendar.setMinute(0);
            xmlGregorianCalendar.setSecond(0);
            xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);

            return xmlGregorianCalendar;
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static OffsetDateTime getOffsetDateTime(XMLGregorianCalendar xmlDate) {
        return OffsetDateTime.now()
                .withYear(xmlDate.getYear())
                .withMonth(xmlDate.getMonth())
                .withDayOfMonth(xmlDate.getDay())
                .withHour(xmlDate.getHour())
                .withMinute(xmlDate.getMinute())
                .withSecond(xmlDate.getSecond())
                .withNano(0);
    }

    public static OffsetDateTime getDateNoTime(XMLGregorianCalendar xmlDate) {
        return OffsetDateTime.now()
                .withYear(xmlDate.getYear())
                .withMonth(xmlDate.getMonth())
                .withDayOfMonth(xmlDate.getDay());
    }

    public static XMLGregorianCalendar getXmlDate(Date date) {
        try {
            GregorianCalendar gCalendar = new GregorianCalendar();
            gCalendar.setTime(date);
            XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory
                    .newInstance().newXMLGregorianCalendar(gCalendar);

            return xmlGregorianCalendar;
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static XMLGregorianCalendar getXmlDateNoTime(Date date) {
        try {
            XMLGregorianCalendar xmlGregorianCalendar = getXmlDate(date);
            xmlGregorianCalendar.setHour(0);
            xmlGregorianCalendar.setMinute(0);
            xmlGregorianCalendar.setSecond(0);
            xmlGregorianCalendar.setMillisecond(0);

            return xmlGregorianCalendar;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
