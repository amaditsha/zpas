
package zw.co.esolutions.zpas.iso.msg.camt026_001_06;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnderlyingTransaction4Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnderlyingTransaction4Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Initn" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}UnderlyingPaymentInstruction4"/>
 *         &lt;element name="IntrBk" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}UnderlyingPaymentTransaction3"/>
 *         &lt;element name="StmtNtry" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}UnderlyingStatementEntry2"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnderlyingTransaction4Choice", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06", propOrder = {
    "initn",
    "intrBk",
    "stmtNtry"
})
public class UnderlyingTransaction4Choice {

    @XmlElement(name = "Initn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected UnderlyingPaymentInstruction4 initn;
    @XmlElement(name = "IntrBk", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected UnderlyingPaymentTransaction3 intrBk;
    @XmlElement(name = "StmtNtry", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected UnderlyingStatementEntry2 stmtNtry;

    /**
     * Gets the value of the initn property.
     * 
     * @return
     *     possible object is
     *     {@link UnderlyingPaymentInstruction4 }
     *     
     */
    public UnderlyingPaymentInstruction4 getInitn() {
        return initn;
    }

    /**
     * Sets the value of the initn property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnderlyingPaymentInstruction4 }
     *     
     */
    public void setInitn(UnderlyingPaymentInstruction4 value) {
        this.initn = value;
    }

    /**
     * Gets the value of the intrBk property.
     * 
     * @return
     *     possible object is
     *     {@link UnderlyingPaymentTransaction3 }
     *     
     */
    public UnderlyingPaymentTransaction3 getIntrBk() {
        return intrBk;
    }

    /**
     * Sets the value of the intrBk property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnderlyingPaymentTransaction3 }
     *     
     */
    public void setIntrBk(UnderlyingPaymentTransaction3 value) {
        this.intrBk = value;
    }

    /**
     * Gets the value of the stmtNtry property.
     * 
     * @return
     *     possible object is
     *     {@link UnderlyingStatementEntry2 }
     *     
     */
    public UnderlyingStatementEntry2 getStmtNtry() {
        return stmtNtry;
    }

    /**
     * Sets the value of the stmtNtry property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnderlyingStatementEntry2 }
     *     
     */
    public void setStmtNtry(UnderlyingStatementEntry2 value) {
        this.stmtNtry = value;
    }

}
