
package zw.co.esolutions.zpas.iso.msg.camt027_001_06;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClmNonRct" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.06}ClaimNonReceiptV06"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06", propOrder = {
    "clmNonRct"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06")
public class Document {

    @XmlElement(name = "ClmNonRct", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06", required = true)
    protected ClaimNonReceiptV06 clmNonRct;

    /**
     * Gets the value of the clmNonRct property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimNonReceiptV06 }
     *     
     */
    public ClaimNonReceiptV06 getClmNonRct() {
        return clmNonRct;
    }

    /**
     * Sets the value of the clmNonRct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimNonReceiptV06 }
     *     
     */
    public void setClmNonRct(ClaimNonReceiptV06 value) {
        this.clmNonRct = value;
    }

}
