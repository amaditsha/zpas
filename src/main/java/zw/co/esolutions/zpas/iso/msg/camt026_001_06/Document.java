
package zw.co.esolutions.zpas.iso.msg.camt026_001_06;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UblToApply" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}UnableToApplyV06"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06", propOrder = {
    "ublToApply"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
public class Document {

    @XmlElement(name = "UblToApply", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06", required = true)
    protected UnableToApplyV06 ublToApply;

    /**
     * Gets the value of the ublToApply property.
     * 
     * @return
     *     possible object is
     *     {@link UnableToApplyV06 }
     *     
     */
    public UnableToApplyV06 getUblToApply() {
        return ublToApply;
    }

    /**
     * Sets the value of the ublToApply property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnableToApplyV06 }
     *     
     */
    public void setUblToApply(UnableToApplyV06 value) {
        this.ublToApply = value;
    }

}
