
package zw.co.esolutions.zpas.iso.msg.camt031_001_06;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvestigationRejection1Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InvestigationRejection1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NFND"/>
 *     &lt;enumeration value="NAUT"/>
 *     &lt;enumeration value="UKNW"/>
 *     &lt;enumeration value="PCOR"/>
 *     &lt;enumeration value="WMSG"/>
 *     &lt;enumeration value="RNCR"/>
 *     &lt;enumeration value="MROI"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InvestigationRejection1Code", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.031.001.06")
@XmlEnum
public enum InvestigationRejection1Code {

    NFND,
    NAUT,
    UKNW,
    PCOR,
    WMSG,
    RNCR,
    MROI;

    public String value() {
        return name();
    }

    public static InvestigationRejection1Code fromValue(String v) {
        return valueOf(v);
    }

}
