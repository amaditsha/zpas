
package zw.co.esolutions.zpas.iso.msg.pain013_001_07;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CdtrPmtActvtnReq" type="{urn:iso:std:iso:20022:tech:xsd:pain.013.001.07}CreditorPaymentActivationRequestV07"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07", propOrder = {
    "cdtrPmtActvtnReq"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07")
public class Document {

    @XmlElement(name = "CdtrPmtActvtnReq", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07", required = true)
    protected CreditorPaymentActivationRequestV07 cdtrPmtActvtnReq;

    /**
     * Gets the value of the cdtrPmtActvtnReq property.
     * 
     * @return
     *     possible object is
     *     {@link CreditorPaymentActivationRequestV07 }
     *     
     */
    public CreditorPaymentActivationRequestV07 getCdtrPmtActvtnReq() {
        return cdtrPmtActvtnReq;
    }

    /**
     * Sets the value of the cdtrPmtActvtnReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditorPaymentActivationRequestV07 }
     *     
     */
    public void setCdtrPmtActvtnReq(CreditorPaymentActivationRequestV07 value) {
        this.cdtrPmtActvtnReq = value;
    }

}
