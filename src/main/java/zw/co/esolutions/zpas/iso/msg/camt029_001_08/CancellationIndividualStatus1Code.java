
package zw.co.esolutions.zpas.iso.msg.camt029_001_08;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CancellationIndividualStatus1Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CancellationIndividualStatus1Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RJCR"/>
 *     &lt;enumeration value="ACCR"/>
 *     &lt;enumeration value="PDCR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CancellationIndividualStatus1Code", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
@XmlEnum
public enum CancellationIndividualStatus1Code {

    RJCR,
    ACCR,
    PDCR;

    public String value() {
        return name();
    }

    public static CancellationIndividualStatus1Code fromValue(String v) {
        return valueOf(v);
    }

}
