
package zw.co.esolutions.zpas.iso.msg.camt030_001_04;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseForwardingNotification3Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CaseForwardingNotification3Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FTHI"/>
 *     &lt;enumeration value="CANC"/>
 *     &lt;enumeration value="MODI"/>
 *     &lt;enumeration value="DTAU"/>
 *     &lt;enumeration value="SAIN"/>
 *     &lt;enumeration value="MINE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CaseForwardingNotification3Code", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.030.001.04")
@XmlEnum
public enum CaseForwardingNotification3Code {

    FTHI,
    CANC,
    MODI,
    DTAU,
    SAIN,
    MINE;

    public String value() {
        return name();
    }

    public static CaseForwardingNotification3Code fromValue(String v) {
        return valueOf(v);
    }

}
