
package zw.co.esolutions.zpas.iso.msg.pain014_001_07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentConditionStatus1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentConditionStatus1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccptdAmt" type="{urn:iso:std:iso:20022:tech:xsd:pain.014.001.07}ActiveCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="GrntedPmt" type="{urn:iso:std:iso:20022:tech:xsd:pain.014.001.07}TrueFalseIndicator"/>
 *         &lt;element name="EarlyPmt" type="{urn:iso:std:iso:20022:tech:xsd:pain.014.001.07}TrueFalseIndicator"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentConditionStatus1", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07", propOrder = {
    "accptdAmt",
    "grntedPmt",
    "earlyPmt"
})
public class PaymentConditionStatus1 {

    @XmlElement(name = "AccptdAmt", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07")
    protected ActiveCurrencyAndAmount accptdAmt;
    @XmlElement(name = "GrntedPmt", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07")
    protected boolean grntedPmt;
    @XmlElement(name = "EarlyPmt", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.014.001.07")
    protected boolean earlyPmt;

    /**
     * Gets the value of the accptdAmt property.
     * 
     * @return
     *     possible object is
     *     {@link ActiveCurrencyAndAmount }
     *     
     */
    public ActiveCurrencyAndAmount getAccptdAmt() {
        return accptdAmt;
    }

    /**
     * Sets the value of the accptdAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActiveCurrencyAndAmount }
     *     
     */
    public void setAccptdAmt(ActiveCurrencyAndAmount value) {
        this.accptdAmt = value;
    }

    /**
     * Gets the value of the grntedPmt property.
     * 
     */
    public boolean isGrntedPmt() {
        return grntedPmt;
    }

    /**
     * Sets the value of the grntedPmt property.
     * 
     */
    public void setGrntedPmt(boolean value) {
        this.grntedPmt = value;
    }

    /**
     * Gets the value of the earlyPmt property.
     * 
     */
    public boolean isEarlyPmt() {
        return earlyPmt;
    }

    /**
     * Sets the value of the earlyPmt property.
     * 
     */
    public void setEarlyPmt(boolean value) {
        this.earlyPmt = value;
    }

}
