
package zw.co.esolutions.zpas.iso.msg.camt029_001_08;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorrectiveTransaction3Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorrectiveTransaction3Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Initn" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}CorrectivePaymentInitiation3"/>
 *         &lt;element name="IntrBk" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}CorrectiveInterbankTransaction1"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorrectiveTransaction3Choice", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08", propOrder = {
    "initn",
    "intrBk"
})
public class CorrectiveTransaction3Choice {

    @XmlElement(name = "Initn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected CorrectivePaymentInitiation3 initn;
    @XmlElement(name = "IntrBk", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected CorrectiveInterbankTransaction1 intrBk;

    /**
     * Gets the value of the initn property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectivePaymentInitiation3 }
     *     
     */
    public CorrectivePaymentInitiation3 getInitn() {
        return initn;
    }

    /**
     * Sets the value of the initn property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectivePaymentInitiation3 }
     *     
     */
    public void setInitn(CorrectivePaymentInitiation3 value) {
        this.initn = value;
    }

    /**
     * Gets the value of the intrBk property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectiveInterbankTransaction1 }
     *     
     */
    public CorrectiveInterbankTransaction1 getIntrBk() {
        return intrBk;
    }

    /**
     * Sets the value of the intrBk property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectiveInterbankTransaction1 }
     *     
     */
    public void setIntrBk(CorrectiveInterbankTransaction1 value) {
        this.intrBk = value;
    }

}
