
package zw.co.esolutions.zpas.iso.msg.camt030_001_05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NtfctnOfCaseAssgnmt" type="{urn:iso:std:iso:20022:tech:xsd:camt.030.001.05}NotificationOfCaseAssignmentV05"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.030.001.05", propOrder = {
    "ntfctnOfCaseAssgnmt"
})
public class Document {

    @XmlElement(name = "NtfctnOfCaseAssgnmt", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.030.001.05", required = true)
    protected NotificationOfCaseAssignmentV05 ntfctnOfCaseAssgnmt;

    /**
     * Gets the value of the ntfctnOfCaseAssgnmt property.
     * 
     * @return
     *     possible object is
     *     {@link NotificationOfCaseAssignmentV05 }
     *     
     */
    public NotificationOfCaseAssignmentV05 getNtfctnOfCaseAssgnmt() {
        return ntfctnOfCaseAssgnmt;
    }

    /**
     * Sets the value of the ntfctnOfCaseAssgnmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotificationOfCaseAssignmentV05 }
     *     
     */
    public void setNtfctnOfCaseAssgnmt(NotificationOfCaseAssignmentV05 value) {
        this.ntfctnOfCaseAssgnmt = value;
    }

}
