
package zw.co.esolutions.zpas.iso.msg.camt026_001_06;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnableToApplyMissingInformation3Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UnableToApplyMissingInformation3Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MS01"/>
 *     &lt;enumeration value="MS02"/>
 *     &lt;enumeration value="MS03"/>
 *     &lt;enumeration value="MS04"/>
 *     &lt;enumeration value="MS05"/>
 *     &lt;enumeration value="MS06"/>
 *     &lt;enumeration value="MS07"/>
 *     &lt;enumeration value="MS08"/>
 *     &lt;enumeration value="MS09"/>
 *     &lt;enumeration value="MS10"/>
 *     &lt;enumeration value="MS11"/>
 *     &lt;enumeration value="MS12"/>
 *     &lt;enumeration value="MS13"/>
 *     &lt;enumeration value="MS14"/>
 *     &lt;enumeration value="MS15"/>
 *     &lt;enumeration value="MS16"/>
 *     &lt;enumeration value="MS17"/>
 *     &lt;enumeration value="NARR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UnableToApplyMissingInformation3Code", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
@XmlEnum
public enum UnableToApplyMissingInformation3Code {

    @XmlEnumValue("MS01")
    MS_01("MS01"),
    @XmlEnumValue("MS02")
    MS_02("MS02"),
    @XmlEnumValue("MS03")
    MS_03("MS03"),
    @XmlEnumValue("MS04")
    MS_04("MS04"),
    @XmlEnumValue("MS05")
    MS_05("MS05"),
    @XmlEnumValue("MS06")
    MS_06("MS06"),
    @XmlEnumValue("MS07")
    MS_07("MS07"),
    @XmlEnumValue("MS08")
    MS_08("MS08"),
    @XmlEnumValue("MS09")
    MS_09("MS09"),
    @XmlEnumValue("MS10")
    MS_10("MS10"),
    @XmlEnumValue("MS11")
    MS_11("MS11"),
    @XmlEnumValue("MS12")
    MS_12("MS12"),
    @XmlEnumValue("MS13")
    MS_13("MS13"),
    @XmlEnumValue("MS14")
    MS_14("MS14"),
    @XmlEnumValue("MS15")
    MS_15("MS15"),
    @XmlEnumValue("MS16")
    MS_16("MS16"),
    @XmlEnumValue("MS17")
    MS_17("MS17"),
    NARR("NARR");
    private final String value;

    UnableToApplyMissingInformation3Code(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnableToApplyMissingInformation3Code fromValue(String v) {
        for (UnableToApplyMissingInformation3Code c: UnableToApplyMissingInformation3Code.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
