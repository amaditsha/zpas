
package zw.co.esolutions.zpas.iso.msg.acmt022_001_02;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdModAdvc" type="{urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02}IdentificationModificationAdviceV02"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02", propOrder = {
    "idModAdvc"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02")
public class Document {

    @XmlElement(name = "IdModAdvc", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.022.001.02", required = true)
    protected IdentificationModificationAdviceV02 idModAdvc;

    /**
     * Gets the value of the idModAdvc property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationModificationAdviceV02 }
     *     
     */
    public IdentificationModificationAdviceV02 getIdModAdvc() {
        return idModAdvc;
    }

    /**
     * Sets the value of the idModAdvc property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationModificationAdviceV02 }
     *     
     */
    public void setIdModAdvc(IdentificationModificationAdviceV02 value) {
        this.idModAdvc = value;
    }

}
