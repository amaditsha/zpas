
package zw.co.esolutions.zpas.iso.msg.pain008_001_07;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CstmrDrctDbtInitn" type="{urn:iso:std:iso:20022:tech:xsd:pain.008.001.07}CustomerDirectDebitInitiationV07"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.008.001.07", propOrder = {
    "cstmrDrctDbtInitn"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.008.001.07")
public class Document {

    @XmlElement(name = "CstmrDrctDbtInitn", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.008.001.07", required = true)
    protected CustomerDirectDebitInitiationV07 cstmrDrctDbtInitn;

    /**
     * Gets the value of the cstmrDrctDbtInitn property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerDirectDebitInitiationV07 }
     *     
     */
    public CustomerDirectDebitInitiationV07 getCstmrDrctDbtInitn() {
        return cstmrDrctDbtInitn;
    }

    /**
     * Sets the value of the cstmrDrctDbtInitn property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerDirectDebitInitiationV07 }
     *     
     */
    public void setCstmrDrctDbtInitn(CustomerDirectDebitInitiationV07 value) {
        this.cstmrDrctDbtInitn = value;
    }

}
