
package zw.co.esolutions.zpas.iso.msg.acmt023_001_02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificationVerification2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificationVerification2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}Max35Text"/>
 *         &lt;element name="PtyAndAcctId" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}IdentificationInformation2"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationVerification2", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", propOrder = {
    "id",
    "ptyAndAcctId"
})
public class IdentificationVerification2 {

    @XmlElement(name = "Id", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    protected String id;
    @XmlElement(name = "PtyAndAcctId", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    protected IdentificationInformation2 ptyAndAcctId;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the ptyAndAcctId property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationInformation2 }
     *     
     */
    public IdentificationInformation2 getPtyAndAcctId() {
        return ptyAndAcctId;
    }

    /**
     * Sets the value of the ptyAndAcctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationInformation2 }
     *     
     */
    public void setPtyAndAcctId(IdentificationInformation2 value) {
        this.ptyAndAcctId = value;
    }

}
