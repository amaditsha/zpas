
package zw.co.esolutions.zpas.iso.msg.pain013_001_07;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentMethod7Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentMethod7Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CHK"/>
 *     &lt;enumeration value="TRF"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentMethod7Code", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.013.001.07")
@XmlEnum
public enum PaymentMethod7Code {

    CHK,
    TRF;

    public String value() {
        return name();
    }

    public static PaymentMethod7Code fromValue(String v) {
        return valueOf(v);
    }

}
