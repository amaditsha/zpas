
package zw.co.esolutions.zpas.iso.msg.camt037_001_07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DbtAuthstnReq" type="{urn:iso:std:iso:20022:tech:xsd:camt.037.001.07}DebitAuthorisationRequestV07"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07", propOrder = {
    "dbtAuthstnReq"
})
public class Document {

    @XmlElement(name = "DbtAuthstnReq", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07", required = true)
    protected DebitAuthorisationRequestV07 dbtAuthstnReq;

    /**
     * Gets the value of the dbtAuthstnReq property.
     * 
     * @return
     *     possible object is
     *     {@link DebitAuthorisationRequestV07 }
     *     
     */
    public DebitAuthorisationRequestV07 getDbtAuthstnReq() {
        return dbtAuthstnReq;
    }

    /**
     * Sets the value of the dbtAuthstnReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitAuthorisationRequestV07 }
     *     
     */
    public void setDbtAuthstnReq(DebitAuthorisationRequestV07 value) {
        this.dbtAuthstnReq = value;
    }

}
