
package zw.co.esolutions.zpas.iso.msg.camt037_001_07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnderlyingTransaction5Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnderlyingTransaction5Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Initn" type="{urn:iso:std:iso:20022:tech:xsd:camt.037.001.07}UnderlyingPaymentInstruction5"/>
 *         &lt;element name="IntrBk" type="{urn:iso:std:iso:20022:tech:xsd:camt.037.001.07}UnderlyingPaymentTransaction4"/>
 *         &lt;element name="StmtNtry" type="{urn:iso:std:iso:20022:tech:xsd:camt.037.001.07}UnderlyingStatementEntry3"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnderlyingTransaction5Choice", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07", propOrder = {
    "initn",
    "intrBk",
    "stmtNtry"
})
public class UnderlyingTransaction5Choice {

    @XmlElement(name = "Initn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07")
    protected UnderlyingPaymentInstruction5 initn;
    @XmlElement(name = "IntrBk", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07")
    protected UnderlyingPaymentTransaction4 intrBk;
    @XmlElement(name = "StmtNtry", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07")
    protected UnderlyingStatementEntry3 stmtNtry;

    /**
     * Gets the value of the initn property.
     * 
     * @return
     *     possible object is
     *     {@link UnderlyingPaymentInstruction5 }
     *     
     */
    public UnderlyingPaymentInstruction5 getInitn() {
        return initn;
    }

    /**
     * Sets the value of the initn property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnderlyingPaymentInstruction5 }
     *     
     */
    public void setInitn(UnderlyingPaymentInstruction5 value) {
        this.initn = value;
    }

    /**
     * Gets the value of the intrBk property.
     * 
     * @return
     *     possible object is
     *     {@link UnderlyingPaymentTransaction4 }
     *     
     */
    public UnderlyingPaymentTransaction4 getIntrBk() {
        return intrBk;
    }

    /**
     * Sets the value of the intrBk property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnderlyingPaymentTransaction4 }
     *     
     */
    public void setIntrBk(UnderlyingPaymentTransaction4 value) {
        this.intrBk = value;
    }

    /**
     * Gets the value of the stmtNtry property.
     * 
     * @return
     *     possible object is
     *     {@link UnderlyingStatementEntry3 }
     *     
     */
    public UnderlyingStatementEntry3 getStmtNtry() {
        return stmtNtry;
    }

    /**
     * Sets the value of the stmtNtry property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnderlyingStatementEntry3 }
     *     
     */
    public void setStmtNtry(UnderlyingStatementEntry3 value) {
        this.stmtNtry = value;
    }

}
