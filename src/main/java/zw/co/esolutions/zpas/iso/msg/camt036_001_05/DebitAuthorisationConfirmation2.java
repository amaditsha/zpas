
package zw.co.esolutions.zpas.iso.msg.camt036_001_05;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DebitAuthorisationConfirmation2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DebitAuthorisationConfirmation2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DbtAuthstn" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}YesNoIndicator"/>
 *         &lt;element name="AmtToDbt" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}ActiveCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="ValDtToDbt" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}ISODate" minOccurs="0"/>
 *         &lt;element name="Rsn" type="{urn:iso:std:iso:20022:tech:xsd:camt.036.001.05}Max140Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitAuthorisationConfirmation2", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05", propOrder = {
    "dbtAuthstn",
    "amtToDbt",
    "valDtToDbt",
    "rsn"
})
public class DebitAuthorisationConfirmation2 {

    @XmlElement(name = "DbtAuthstn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05")
    protected boolean dbtAuthstn;
    @XmlElement(name = "AmtToDbt", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05")
    protected ActiveCurrencyAndAmount amtToDbt;
    @XmlElement(name = "ValDtToDbt", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar valDtToDbt;
    @XmlElement(name = "Rsn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.036.001.05")
    protected String rsn;

    /**
     * Gets the value of the dbtAuthstn property.
     * 
     */
    public boolean isDbtAuthstn() {
        return dbtAuthstn;
    }

    /**
     * Sets the value of the dbtAuthstn property.
     * 
     */
    public void setDbtAuthstn(boolean value) {
        this.dbtAuthstn = value;
    }

    /**
     * Gets the value of the amtToDbt property.
     * 
     * @return
     *     possible object is
     *     {@link ActiveCurrencyAndAmount }
     *     
     */
    public ActiveCurrencyAndAmount getAmtToDbt() {
        return amtToDbt;
    }

    /**
     * Sets the value of the amtToDbt property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActiveCurrencyAndAmount }
     *     
     */
    public void setAmtToDbt(ActiveCurrencyAndAmount value) {
        this.amtToDbt = value;
    }

    /**
     * Gets the value of the valDtToDbt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValDtToDbt() {
        return valDtToDbt;
    }

    /**
     * Sets the value of the valDtToDbt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValDtToDbt(XMLGregorianCalendar value) {
        this.valDtToDbt = value;
    }

    /**
     * Gets the value of the rsn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRsn() {
        return rsn;
    }

    /**
     * Sets the value of the rsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRsn(String value) {
        this.rsn = value;
    }

}
