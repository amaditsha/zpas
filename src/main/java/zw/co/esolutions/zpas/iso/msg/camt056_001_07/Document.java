
package zw.co.esolutions.zpas.iso.msg.camt056_001_07;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FIToFIPmtCxlReq" type="{urn:iso:std:iso:20022:tech:xsd:camt.056.001.07}FIToFIPaymentCancellationRequestV07"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.056.001.07", propOrder = {
    "fiToFIPmtCxlReq"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.056.001.07")
public class Document {

    @XmlElement(name = "FIToFIPmtCxlReq", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.056.001.07", required = true)
    protected FIToFIPaymentCancellationRequestV07 fiToFIPmtCxlReq;

    /**
     * Gets the value of the fiToFIPmtCxlReq property.
     * 
     * @return
     *     possible object is
     *     {@link FIToFIPaymentCancellationRequestV07 }
     *     
     */
    public FIToFIPaymentCancellationRequestV07 getFIToFIPmtCxlReq() {
        return fiToFIPmtCxlReq;
    }

    /**
     * Sets the value of the fiToFIPmtCxlReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link FIToFIPaymentCancellationRequestV07 }
     *     
     */
    public void setFIToFIPmtCxlReq(FIToFIPaymentCancellationRequestV07 value) {
        this.fiToFIPmtCxlReq = value;
    }

}
