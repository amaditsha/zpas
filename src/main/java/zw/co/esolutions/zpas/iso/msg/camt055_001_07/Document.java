
package zw.co.esolutions.zpas.iso.msg.camt055_001_07;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CstmrPmtCxlReq" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}CustomerPaymentCancellationRequestV07"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07", propOrder = {
    "cstmrPmtCxlReq"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
public class Document {

    @XmlElement(name = "CstmrPmtCxlReq", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07", required = true)
    protected CustomerPaymentCancellationRequestV07 cstmrPmtCxlReq;

    /**
     * Gets the value of the cstmrPmtCxlReq property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerPaymentCancellationRequestV07 }
     *     
     */
    public CustomerPaymentCancellationRequestV07 getCstmrPmtCxlReq() {
        return cstmrPmtCxlReq;
    }

    /**
     * Sets the value of the cstmrPmtCxlReq property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerPaymentCancellationRequestV07 }
     *     
     */
    public void setCstmrPmtCxlReq(CustomerPaymentCancellationRequestV07 value) {
        this.cstmrPmtCxlReq = value;
    }

}
