
package zw.co.esolutions.zpas.iso.msg.camt037_001_07;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DebitAuthorisation2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DebitAuthorisation2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CxlRsn" type="{urn:iso:std:iso:20022:tech:xsd:camt.037.001.07}CancellationReason33Choice"/>
 *         &lt;element name="AmtToDbt" type="{urn:iso:std:iso:20022:tech:xsd:camt.037.001.07}ActiveOrHistoricCurrencyAndAmount" minOccurs="0"/>
 *         &lt;element name="ValDtToDbt" type="{urn:iso:std:iso:20022:tech:xsd:camt.037.001.07}ISODate" minOccurs="0"/>
 *         &lt;element name="AddtlCxlRsnInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.037.001.07}Max105Text" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitAuthorisation2", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07", propOrder = {
    "cxlRsn",
    "amtToDbt",
    "valDtToDbt",
    "addtlCxlRsnInf"
})
public class DebitAuthorisation2 {

    @XmlElement(name = "CxlRsn", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07", required = true)
    protected CancellationReason33Choice cxlRsn;
    @XmlElement(name = "AmtToDbt", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07")
    protected ActiveOrHistoricCurrencyAndAmount amtToDbt;
    @XmlElement(name = "ValDtToDbt", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar valDtToDbt;
    @XmlElement(name = "AddtlCxlRsnInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.037.001.07")
    protected List<String> addtlCxlRsnInf;

    /**
     * Gets the value of the cxlRsn property.
     * 
     * @return
     *     possible object is
     *     {@link CancellationReason33Choice }
     *     
     */
    public CancellationReason33Choice getCxlRsn() {
        return cxlRsn;
    }

    /**
     * Sets the value of the cxlRsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancellationReason33Choice }
     *     
     */
    public void setCxlRsn(CancellationReason33Choice value) {
        this.cxlRsn = value;
    }

    /**
     * Gets the value of the amtToDbt property.
     * 
     * @return
     *     possible object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *     
     */
    public ActiveOrHistoricCurrencyAndAmount getAmtToDbt() {
        return amtToDbt;
    }

    /**
     * Sets the value of the amtToDbt property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActiveOrHistoricCurrencyAndAmount }
     *     
     */
    public void setAmtToDbt(ActiveOrHistoricCurrencyAndAmount value) {
        this.amtToDbt = value;
    }

    /**
     * Gets the value of the valDtToDbt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValDtToDbt() {
        return valDtToDbt;
    }

    /**
     * Sets the value of the valDtToDbt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValDtToDbt(XMLGregorianCalendar value) {
        this.valDtToDbt = value;
    }

    /**
     * Gets the value of the addtlCxlRsnInf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addtlCxlRsnInf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddtlCxlRsnInf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAddtlCxlRsnInf() {
        if (addtlCxlRsnInf == null) {
            addtlCxlRsnInf = new ArrayList<String>();
        }
        return this.addtlCxlRsnInf;
    }

}
