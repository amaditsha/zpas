
package zw.co.esolutions.zpas.iso.msg.camt027_001_06;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnderlyingStatementEntry2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnderlyingStatementEntry2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrgnlGrpInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.06}OriginalGroupInformation29" minOccurs="0"/>
 *         &lt;element name="OrgnlStmtId" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.06}Max35Text" minOccurs="0"/>
 *         &lt;element name="OrgnlNtryId" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.06}Max35Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnderlyingStatementEntry2", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06", propOrder = {
    "orgnlGrpInf",
    "orgnlStmtId",
    "orgnlNtryId"
})
public class UnderlyingStatementEntry2 {

    @XmlElement(name = "OrgnlGrpInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06")
    protected OriginalGroupInformation29 orgnlGrpInf;
    @XmlElement(name = "OrgnlStmtId", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06")
    protected String orgnlStmtId;
    @XmlElement(name = "OrgnlNtryId", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.06")
    protected String orgnlNtryId;

    /**
     * Gets the value of the orgnlGrpInf property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalGroupInformation29 }
     *     
     */
    public OriginalGroupInformation29 getOrgnlGrpInf() {
        return orgnlGrpInf;
    }

    /**
     * Sets the value of the orgnlGrpInf property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalGroupInformation29 }
     *     
     */
    public void setOrgnlGrpInf(OriginalGroupInformation29 value) {
        this.orgnlGrpInf = value;
    }

    /**
     * Gets the value of the orgnlStmtId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgnlStmtId() {
        return orgnlStmtId;
    }

    /**
     * Sets the value of the orgnlStmtId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgnlStmtId(String value) {
        this.orgnlStmtId = value;
    }

    /**
     * Gets the value of the orgnlNtryId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgnlNtryId() {
        return orgnlNtryId;
    }

    /**
     * Sets the value of the orgnlNtryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgnlNtryId(String value) {
        this.orgnlNtryId = value;
    }

}
