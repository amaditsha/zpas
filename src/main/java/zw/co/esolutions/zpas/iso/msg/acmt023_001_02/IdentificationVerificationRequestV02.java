
package zw.co.esolutions.zpas.iso.msg.acmt023_001_02;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificationVerificationRequestV02 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificationVerificationRequestV02">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Assgnmt" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}IdentificationAssignment2"/>
 *         &lt;element name="Vrfctn" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}IdentificationVerification2" maxOccurs="unbounded"/>
 *         &lt;element name="SplmtryData" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}SupplementaryData1" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationVerificationRequestV02", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", propOrder = {
    "assgnmt",
    "vrfctn",
    "splmtryData"
})
public class IdentificationVerificationRequestV02 {

    @XmlElement(name = "Assgnmt", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    protected IdentificationAssignment2 assgnmt;
    @XmlElement(name = "Vrfctn", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    protected List<IdentificationVerification2> vrfctn;
    @XmlElement(name = "SplmtryData", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02")
    protected List<SupplementaryData1> splmtryData;

    /**
     * Gets the value of the assgnmt property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationAssignment2 }
     *     
     */
    public IdentificationAssignment2 getAssgnmt() {
        return assgnmt;
    }

    /**
     * Sets the value of the assgnmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationAssignment2 }
     *     
     */
    public void setAssgnmt(IdentificationAssignment2 value) {
        this.assgnmt = value;
    }

    /**
     * Gets the value of the vrfctn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vrfctn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVrfctn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentificationVerification2 }
     * 
     * 
     */
    public List<IdentificationVerification2> getVrfctn() {
        if (vrfctn == null) {
            vrfctn = new ArrayList<IdentificationVerification2>();
        }
        return this.vrfctn;
    }

    /**
     * Gets the value of the splmtryData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splmtryData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplmtryData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupplementaryData1 }
     * 
     * 
     */
    public List<SupplementaryData1> getSplmtryData() {
        if (splmtryData == null) {
            splmtryData = new ArrayList<SupplementaryData1>();
        }
        return this.splmtryData;
    }

}
