
package zw.co.esolutions.zpas.iso.msg.acmt024_001_02;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdVrfctnRpt" type="{urn:iso:std:iso:20022:tech:xsd:acmt.024.001.02}IdentificationVerificationReportV02"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.024.001.02", propOrder = {
    "idVrfctnRpt"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.024.001.02")
public class Document {

    @XmlElement(name = "IdVrfctnRpt", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.024.001.02", required = true)
    protected IdentificationVerificationReportV02 idVrfctnRpt;

    /**
     * Gets the value of the idVrfctnRpt property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationVerificationReportV02 }
     *     
     */
    public IdentificationVerificationReportV02 getIdVrfctnRpt() {
        return idVrfctnRpt;
    }

    /**
     * Sets the value of the idVrfctnRpt property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationVerificationReportV02 }
     *     
     */
    public void setIdVrfctnRpt(IdentificationVerificationReportV02 value) {
        this.idVrfctnRpt = value;
    }

}
