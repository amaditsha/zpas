
package zw.co.esolutions.zpas.iso.msg.acmt023_001_02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for IdentificationAssignment2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificationAssignment2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MsgId" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}Max35Text"/>
 *         &lt;element name="CreDtTm" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}ISODateTime"/>
 *         &lt;element name="Cretr" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}Party12Choice" minOccurs="0"/>
 *         &lt;element name="FrstAgt" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}BranchAndFinancialInstitutionIdentification5" minOccurs="0"/>
 *         &lt;element name="Assgnr" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}Party12Choice"/>
 *         &lt;element name="Assgne" type="{urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02}Party12Choice"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationAssignment2", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", propOrder = {
    "msgId",
    "creDtTm",
    "cretr",
    "frstAgt",
    "assgnr",
    "assgne"
})
public class IdentificationAssignment2 {

    @XmlElement(name = "MsgId", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    protected String msgId;
    @XmlElement(name = "CreDtTm", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creDtTm;
    @XmlElement(name = "Cretr", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02")
    protected Party12Choice cretr;
    @XmlElement(name = "FrstAgt", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02")
    protected BranchAndFinancialInstitutionIdentification5 frstAgt;
    @XmlElement(name = "Assgnr", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    protected Party12Choice assgnr;
    @XmlElement(name = "Assgne", namespace = "urn:iso:std:iso:20022:tech:xsd:acmt.023.001.02", required = true)
    protected Party12Choice assgne;

    /**
     * Gets the value of the msgId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     * Sets the value of the msgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgId(String value) {
        this.msgId = value;
    }

    /**
     * Gets the value of the creDtTm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreDtTm() {
        return creDtTm;
    }

    /**
     * Sets the value of the creDtTm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreDtTm(XMLGregorianCalendar value) {
        this.creDtTm = value;
    }

    /**
     * Gets the value of the cretr property.
     * 
     * @return
     *     possible object is
     *     {@link Party12Choice }
     *     
     */
    public Party12Choice getCretr() {
        return cretr;
    }

    /**
     * Sets the value of the cretr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Party12Choice }
     *     
     */
    public void setCretr(Party12Choice value) {
        this.cretr = value;
    }

    /**
     * Gets the value of the frstAgt property.
     * 
     * @return
     *     possible object is
     *     {@link BranchAndFinancialInstitutionIdentification5 }
     *     
     */
    public BranchAndFinancialInstitutionIdentification5 getFrstAgt() {
        return frstAgt;
    }

    /**
     * Sets the value of the frstAgt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BranchAndFinancialInstitutionIdentification5 }
     *     
     */
    public void setFrstAgt(BranchAndFinancialInstitutionIdentification5 value) {
        this.frstAgt = value;
    }

    /**
     * Gets the value of the assgnr property.
     * 
     * @return
     *     possible object is
     *     {@link Party12Choice }
     *     
     */
    public Party12Choice getAssgnr() {
        return assgnr;
    }

    /**
     * Sets the value of the assgnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Party12Choice }
     *     
     */
    public void setAssgnr(Party12Choice value) {
        this.assgnr = value;
    }

    /**
     * Gets the value of the assgne property.
     * 
     * @return
     *     possible object is
     *     {@link Party12Choice }
     *     
     */
    public Party12Choice getAssgne() {
        return assgne;
    }

    /**
     * Sets the value of the assgne property.
     * 
     * @param value
     *     allowed object is
     *     {@link Party12Choice }
     *     
     */
    public void setAssgne(Party12Choice value) {
        this.assgne = value;
    }

}
