
package zw.co.esolutions.zpas.iso.msg.pacs003_001_07;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FIToFICstmrDrctDbt" type="{urn:iso:std:iso:20022:tech:xsd:pacs.003.001.07}FIToFICustomerDirectDebitV07"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pacs.003.001.07", propOrder = {
    "fiToFICstmrDrctDbt"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pacs.003.001.07")
public class Document {

    @XmlElement(name = "FIToFICstmrDrctDbt", namespace = "urn:iso:std:iso:20022:tech:xsd:pacs.003.001.07", required = true)
    protected FIToFICustomerDirectDebitV07 fiToFICstmrDrctDbt;

    /**
     * Gets the value of the fiToFICstmrDrctDbt property.
     * 
     * @return
     *     possible object is
     *     {@link FIToFICustomerDirectDebitV07 }
     *     
     */
    public FIToFICustomerDirectDebitV07 getFIToFICstmrDrctDbt() {
        return fiToFICstmrDrctDbt;
    }

    /**
     * Sets the value of the fiToFICstmrDrctDbt property.
     * 
     * @param value
     *     allowed object is
     *     {@link FIToFICustomerDirectDebitV07 }
     *     
     */
    public void setFIToFICstmrDrctDbt(FIToFICustomerDirectDebitV07 value) {
        this.fiToFICstmrDrctDbt = value;
    }

}
