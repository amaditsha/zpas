
package zw.co.esolutions.zpas.iso.msg.pacs003_001_07;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SettlementMethod2Code.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SettlementMethod2Code">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INDA"/>
 *     &lt;enumeration value="INGA"/>
 *     &lt;enumeration value="CLRG"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SettlementMethod2Code", namespace = "urn:iso:std:iso:20022:tech:xsd:pacs.003.001.07")
@XmlEnum
public enum SettlementMethod2Code {

    INDA,
    INGA,
    CLRG;

    public String value() {
        return name();
    }

    public static SettlementMethod2Code fromValue(String v) {
        return valueOf(v);
    }

}
