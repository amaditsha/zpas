
package zw.co.esolutions.zpas.iso.msg.camt029_001_08;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvestigationStatus4Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvestigationStatus4Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Conf" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}ExternalInvestigationExecutionConfirmation1Code"/>
 *         &lt;element name="RjctdMod" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}ModificationStatusReason1Choice" maxOccurs="unbounded"/>
 *         &lt;element name="DplctOf" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}Case4"/>
 *         &lt;element name="AssgnmtCxlConf" type="{urn:iso:std:iso:20022:tech:xsd:camt.029.001.08}YesNoIndicator"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvestigationStatus4Choice", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08", propOrder = {
    "conf",
    "rjctdMod",
    "dplctOf",
    "assgnmtCxlConf"
})
public class InvestigationStatus4Choice {

    @XmlElement(name = "Conf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected String conf;
    @XmlElement(name = "RjctdMod", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected List<ModificationStatusReason1Choice> rjctdMod;
    @XmlElement(name = "DplctOf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected Case4 dplctOf;
    @XmlElement(name = "AssgnmtCxlConf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.029.001.08")
    protected Boolean assgnmtCxlConf;

    /**
     * Gets the value of the conf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConf() {
        return conf;
    }

    /**
     * Sets the value of the conf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConf(String value) {
        this.conf = value;
    }

    /**
     * Gets the value of the rjctdMod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rjctdMod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRjctdMod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ModificationStatusReason1Choice }
     * 
     * 
     */
    public List<ModificationStatusReason1Choice> getRjctdMod() {
        if (rjctdMod == null) {
            rjctdMod = new ArrayList<ModificationStatusReason1Choice>();
        }
        return this.rjctdMod;
    }

    /**
     * Gets the value of the dplctOf property.
     * 
     * @return
     *     possible object is
     *     {@link Case4 }
     *     
     */
    public Case4 getDplctOf() {
        return dplctOf;
    }

    /**
     * Sets the value of the dplctOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Case4 }
     *     
     */
    public void setDplctOf(Case4 value) {
        this.dplctOf = value;
    }

    /**
     * Gets the value of the assgnmtCxlConf property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAssgnmtCxlConf() {
        return assgnmtCxlConf;
    }

    /**
     * Sets the value of the assgnmtCxlConf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAssgnmtCxlConf(Boolean value) {
        this.assgnmtCxlConf = value;
    }

}
