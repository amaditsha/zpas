
package zw.co.esolutions.zpas.iso.msg.pain007_001_08;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CstmrPmtRvsl" type="{urn:iso:std:iso:20022:tech:xsd:pain.007.001.08}CustomerPaymentReversalV08"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.007.001.08", propOrder = {
    "cstmrPmtRvsl"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.007.001.08")
public class Document {

    @XmlElement(name = "CstmrPmtRvsl", namespace = "urn:iso:std:iso:20022:tech:xsd:pain.007.001.08", required = true)
    protected CustomerPaymentReversalV08 cstmrPmtRvsl;

    /**
     * Gets the value of the cstmrPmtRvsl property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerPaymentReversalV08 }
     *     
     */
    public CustomerPaymentReversalV08 getCstmrPmtRvsl() {
        return cstmrPmtRvsl;
    }

    /**
     * Sets the value of the cstmrPmtRvsl property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerPaymentReversalV08 }
     *     
     */
    public void setCstmrPmtRvsl(CustomerPaymentReversalV08 value) {
        this.cstmrPmtRvsl = value;
    }

}
