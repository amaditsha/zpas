
package zw.co.esolutions.zpas.iso.msg.camt055_001_07;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OriginalPaymentInstruction29 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OriginalPaymentInstruction29">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PmtCxlId" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}Max35Text" minOccurs="0"/>
 *         &lt;element name="Case" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}Case4" minOccurs="0"/>
 *         &lt;element name="OrgnlPmtInfId" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}Max35Text"/>
 *         &lt;element name="OrgnlGrpInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}OriginalGroupInformation29" minOccurs="0"/>
 *         &lt;element name="NbOfTxs" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}Max15NumericText" minOccurs="0"/>
 *         &lt;element name="CtrlSum" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}DecimalNumber" minOccurs="0"/>
 *         &lt;element name="PmtInfCxl" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}GroupCancellationIndicator" minOccurs="0"/>
 *         &lt;element name="CxlRsnInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}PaymentCancellationReason4" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TxInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.055.001.07}PaymentTransaction95" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OriginalPaymentInstruction29", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07", propOrder = {
    "pmtCxlId",
    "_case",
    "orgnlPmtInfId",
    "orgnlGrpInf",
    "nbOfTxs",
    "ctrlSum",
    "pmtInfCxl",
    "cxlRsnInf",
    "txInf"
})
public class OriginalPaymentInstruction29 {

    @XmlElement(name = "PmtCxlId", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected String pmtCxlId;
    @XmlElement(name = "Case", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected Case4 _case;
    @XmlElement(name = "OrgnlPmtInfId", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07", required = true)
    protected String orgnlPmtInfId;
    @XmlElement(name = "OrgnlGrpInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected OriginalGroupInformation29 orgnlGrpInf;
    @XmlElement(name = "NbOfTxs", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected String nbOfTxs;
    @XmlElement(name = "CtrlSum", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected BigDecimal ctrlSum;
    @XmlElement(name = "PmtInfCxl", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected Boolean pmtInfCxl;
    @XmlElement(name = "CxlRsnInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected List<PaymentCancellationReason4> cxlRsnInf;
    @XmlElement(name = "TxInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.055.001.07")
    protected List<PaymentTransaction95> txInf;

    /**
     * Gets the value of the pmtCxlId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPmtCxlId() {
        return pmtCxlId;
    }

    /**
     * Sets the value of the pmtCxlId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPmtCxlId(String value) {
        this.pmtCxlId = value;
    }

    /**
     * Gets the value of the case property.
     * 
     * @return
     *     possible object is
     *     {@link Case4 }
     *     
     */
    public Case4 getCase() {
        return _case;
    }

    /**
     * Sets the value of the case property.
     * 
     * @param value
     *     allowed object is
     *     {@link Case4 }
     *     
     */
    public void setCase(Case4 value) {
        this._case = value;
    }

    /**
     * Gets the value of the orgnlPmtInfId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgnlPmtInfId() {
        return orgnlPmtInfId;
    }

    /**
     * Sets the value of the orgnlPmtInfId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgnlPmtInfId(String value) {
        this.orgnlPmtInfId = value;
    }

    /**
     * Gets the value of the orgnlGrpInf property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalGroupInformation29 }
     *     
     */
    public OriginalGroupInformation29 getOrgnlGrpInf() {
        return orgnlGrpInf;
    }

    /**
     * Sets the value of the orgnlGrpInf property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalGroupInformation29 }
     *     
     */
    public void setOrgnlGrpInf(OriginalGroupInformation29 value) {
        this.orgnlGrpInf = value;
    }

    /**
     * Gets the value of the nbOfTxs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNbOfTxs() {
        return nbOfTxs;
    }

    /**
     * Sets the value of the nbOfTxs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNbOfTxs(String value) {
        this.nbOfTxs = value;
    }

    /**
     * Gets the value of the ctrlSum property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCtrlSum() {
        return ctrlSum;
    }

    /**
     * Sets the value of the ctrlSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCtrlSum(BigDecimal value) {
        this.ctrlSum = value;
    }

    /**
     * Gets the value of the pmtInfCxl property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPmtInfCxl() {
        return pmtInfCxl;
    }

    /**
     * Sets the value of the pmtInfCxl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPmtInfCxl(Boolean value) {
        this.pmtInfCxl = value;
    }

    /**
     * Gets the value of the cxlRsnInf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cxlRsnInf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCxlRsnInf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentCancellationReason4 }
     * 
     * 
     */
    public List<PaymentCancellationReason4> getCxlRsnInf() {
        if (cxlRsnInf == null) {
            cxlRsnInf = new ArrayList<PaymentCancellationReason4>();
        }
        return this.cxlRsnInf;
    }

    /**
     * Gets the value of the txInf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the txInf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTxInf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentTransaction95 }
     * 
     * 
     */
    public List<PaymentTransaction95> getTxInf() {
        if (txInf == null) {
            txInf = new ArrayList<PaymentTransaction95>();
        }
        return this.txInf;
    }

}
