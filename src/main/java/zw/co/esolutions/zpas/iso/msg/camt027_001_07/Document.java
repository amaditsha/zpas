
package zw.co.esolutions.zpas.iso.msg.camt027_001_07;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClmNonRct" type="{urn:iso:std:iso:20022:tech:xsd:camt.027.001.07}ClaimNonReceiptV07"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.07", propOrder = {
    "clmNonRct"
})
@XmlRootElement(name = "Document", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.07")
public class Document {

    @XmlElement(name = "ClmNonRct", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.027.001.07", required = true)
    protected ClaimNonReceiptV07 clmNonRct;

    /**
     * Gets the value of the clmNonRct property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimNonReceiptV07 }
     *     
     */
    public ClaimNonReceiptV07 getClmNonRct() {
        return clmNonRct;
    }

    /**
     * Sets the value of the clmNonRct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimNonReceiptV07 }
     *     
     */
    public void setClmNonRct(ClaimNonReceiptV07 value) {
        this.clmNonRct = value;
    }

}
