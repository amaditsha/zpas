
package zw.co.esolutions.zpas.iso.msg.camt026_001_06;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnableToApplyJustification3Choice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnableToApplyJustification3Choice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="AnyInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}YesNoIndicator"/>
 *         &lt;element name="MssngOrIncrrctInf" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}MissingOrIncorrectInformation3"/>
 *         &lt;element name="PssblDplctInstr" type="{urn:iso:std:iso:20022:tech:xsd:camt.026.001.06}TrueFalseIndicator"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnableToApplyJustification3Choice", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06", propOrder = {
    "anyInf",
    "mssngOrIncrrctInf",
    "pssblDplctInstr"
})
public class UnableToApplyJustification3Choice {

    @XmlElement(name = "AnyInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected Boolean anyInf;
    @XmlElement(name = "MssngOrIncrrctInf", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected MissingOrIncorrectInformation3 mssngOrIncrrctInf;
    @XmlElement(name = "PssblDplctInstr", namespace = "urn:iso:std:iso:20022:tech:xsd:camt.026.001.06")
    protected Boolean pssblDplctInstr;

    /**
     * Gets the value of the anyInf property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAnyInf() {
        return anyInf;
    }

    /**
     * Sets the value of the anyInf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAnyInf(Boolean value) {
        this.anyInf = value;
    }

    /**
     * Gets the value of the mssngOrIncrrctInf property.
     * 
     * @return
     *     possible object is
     *     {@link MissingOrIncorrectInformation3 }
     *     
     */
    public MissingOrIncorrectInformation3 getMssngOrIncrrctInf() {
        return mssngOrIncrrctInf;
    }

    /**
     * Sets the value of the mssngOrIncrrctInf property.
     * 
     * @param value
     *     allowed object is
     *     {@link MissingOrIncorrectInformation3 }
     *     
     */
    public void setMssngOrIncrrctInf(MissingOrIncorrectInformation3 value) {
        this.mssngOrIncrrctInf = value;
    }

    /**
     * Gets the value of the pssblDplctInstr property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPssblDplctInstr() {
        return pssblDplctInstr;
    }

    /**
     * Sets the value of the pssblDplctInstr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPssblDplctInstr(Boolean value) {
        this.pssblDplctInstr = value;
    }

}
