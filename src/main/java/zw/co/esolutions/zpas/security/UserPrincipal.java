package zw.co.esolutions.zpas.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import zw.co.esolutions.zpas.security.model.Profile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by mabuza on 13 Feb 2019
 */
public class UserPrincipal implements UserDetails {

    final List<GrantedAuthority> authorities = new ArrayList<>();

    private Profile profile;

    public UserPrincipal(Profile profile){
        super();
        this.profile = profile;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> grantedAuthorities) {
        this.authorities.addAll(grantedAuthorities);
    }

    @Override
    public String getPassword() {
        return profile.getUserPassword();
    }

    @Override
    public String getUsername() {
        return profile.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !profile.isLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return profile.isEnabled();
    }
}
