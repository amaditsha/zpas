package zw.co.esolutions.zpas.security.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.security.model.Profile;

import java.util.Optional;

/**
 * Created by mabuza on 13 Feb 2019
 */
@Transactional
@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
    Profile findByUsername(String username);
    Optional<Profile> findProfileById(String id);
    Optional<Profile> findByPersonId(String personId);

    @Modifying
    @Query("UPDATE Profile p set p.lastLoggedInAsId = :loginAsId, p.lastLoggedInAsName = :loginAsName where p.id = :profileId")
    int updateLastLoggedInAs(@Param("loginAsId") String loginAsId, @Param("loginAsName") String loginAsName, @Param("profileId") String profileId);
}
