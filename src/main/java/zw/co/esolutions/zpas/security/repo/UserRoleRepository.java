package zw.co.esolutions.zpas.security.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.security.model.UserRole;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, String> {
    @Query("SELECT ur FROM UserRole ur WHERE ur.id in :ids ORDER BY ur.dateCreated DESC")
    List<UserRole> getRolesByIdsIn(@Param("ids") List<String> ids);

    UserRole getUserRoleByName(String name);
    List<UserRole> findAllByProfiles_Username(String username);
}
