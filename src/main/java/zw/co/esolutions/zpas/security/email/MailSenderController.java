package zw.co.esolutions.zpas.security.email;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

/**
 * Created by mabuza on 05 Mar 2019
 */
@RestController
@RequestMapping("/email")
public class MailSenderController {
    private EmailConfig emailConfig;

    public MailSenderController(EmailConfig emailConfig) {
        this.emailConfig = emailConfig;
    }

    @PostMapping
    public void sendOTP(Profile profile) {
        String otp = GenerateKey.generateSecurityCode();

        // Create a mail sender
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(this.emailConfig.getHost());
        mailSender.setPort(Integer.parseInt(this.emailConfig.getPort()));
        mailSender.setUsername(this.emailConfig.getUsername());
        mailSender.setPassword(this.emailConfig.getPassword());

        // Create an email instance
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("noreply@esolutions.co.zw");
        mailMessage.setTo(profile.getEmail());
        mailMessage.setSubject("ZPAS One Time PIN");
        mailMessage.setText("Hi " + profile.getFirstNames() + ",\n" +
                "Your SmartTransaction One Time PIN(OTP) for logging in is " + otp + ". This PIN will only work once.\n");

        // Send mail
        mailSender.send(mailMessage);
    }
}
