package zw.co.esolutions.zpas.security.eventlisteners;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;

/**
 * Created by alfred on 19 Mar 2019
 */
@Slf4j
@Component
public class LoginFailureBadCredentialsListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

    @Autowired
    private ProfileService profileService;

    @Override
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
        String username = (String) event.getAuthentication().getPrincipal();
        log.error("Authentication Failure is: {}", event.getException().getMessage());
        log.info("Username is: {}. Updating failed login count.", username);
        profileService.failedLoginHandler(username).ifPresent(profile -> {
            log.info("Profile is: {}", profile);
        });
    }
}