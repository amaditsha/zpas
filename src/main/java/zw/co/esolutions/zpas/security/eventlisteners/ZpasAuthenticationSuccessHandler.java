package zw.co.esolutions.zpas.security.eventlisteners;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.integration.camel.config.util.Konstants;
import zw.co.esolutions.zpas.security.UserPrincipal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by alfred on 03 Apr 2019
 */
@Slf4j
@Component
public class ZpasAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    //Pure code
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        if(userPrincipal.isAccountNonLocked() && userPrincipal.isEnabled()) {
            log.info("User principle account not locked.");
            log.info("User principle account is enabled.");
           // super.onAuthenticationSuccess(request, response, authentication);

            request.getSession().setAttribute(Konstants.AUTHENTICATED, Konstants.AUTHENTICATED);

            redirectStrategy.sendRedirect(request, response, "/otp/page");
        } else if(!userPrincipal.isAccountNonLocked()) {
            log.info("User principle account locked.");
            new SecurityContextLogoutHandler().logout(request, null, null);
        } else if(!userPrincipal.isEnabled()) {
            log.info("User principle account disabled.");
            new SecurityContextLogoutHandler().logout(request, null, null);
        } else {
            log.info("This should not happen.");
        }
    }
}
