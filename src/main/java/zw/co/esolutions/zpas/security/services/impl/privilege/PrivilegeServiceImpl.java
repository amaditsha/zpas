package zw.co.esolutions.zpas.security.services.impl.privilege;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.security.model.Privilege;
import zw.co.esolutions.zpas.security.repo.PrivilegeRepository;
import zw.co.esolutions.zpas.security.services.iface.privilege.PrivilegeService;
import zw.co.esolutions.zpas.utilities.enums.AccessRights;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/**
 * Created by mabuza on 11 Mar 2019
 */
@Slf4j
@Service
@Transactional
public class PrivilegeServiceImpl implements PrivilegeService {
    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Override
    public List<Privilege> getAllPrivileges() {
        return privilegeRepository.findAll();
    }

    @Override
    public List<Privilege> refreshPrivileges() {
        final Set<Privilege> existingPrivileges = getAllPrivileges().stream().collect(Collectors.toSet());
        final Set<Privilege> allPrivileges = Stream.of(AccessRights.values())
                .map(accessRights -> {
                    Privilege privilege = new Privilege();
                    privilege.setDateCreated(OffsetDateTime.now());
                    privilege.setId(GenerateKey.generateEntityId());
                    privilege.setName(accessRights.name());
                    return privilege;
                })
                .collect(toSet());

        allPrivileges.removeAll(existingPrivileges);
        privilegeRepository.saveAll(allPrivileges);
        return allPrivileges.stream().collect(Collectors.toList());
    }
}
