package zw.co.esolutions.zpas.security.eventlisteners;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.repository.sequence.GenerationNumberRepository;
import zw.co.esolutions.zpas.services.impl.payments.cache.GenerationNumberSequenceCache;

import javax.persistence.IdClass;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.math.BigDecimal;

/**
 * Created by alfred on 08 July 2019
 */
@Slf4j
@Component
public class ZpasServletContextListener implements ServletContextListener {
    @Autowired
    private GenerationNumberRepository generationNumberRepository;

    @Autowired
    private GenerationNumberSequenceCache generationNumberSequenceCache;

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        log.info("Destroying context, persisting all the necessary state. {}", generationNumberSequenceCache);
//        generationNumberSequenceCache.getGenerationNumberCache().entrySet().forEach(stringBigDecimalEntry -> {
//            log.info("Persisting current generation numbers...");
//            final String key = stringBigDecimalEntry.getKey();
//            final BigDecimal value = stringBigDecimalEntry.getValue();
//            final String[] split = key.split("-");
//            if(split.length == 3) {
//                final String formattedDate = split[0];
//                final String customerCode = split[1];
//                final String userCode = split[2];
//                generationNumberRepository.findByCustomerCodeAndUserCodeAndSequenceDate(customerCode, userCode, formattedDate).ifPresent(generationNumber -> {
//                    generationNumber.setNumber(value.longValue());
//                    generationNumberRepository.save(generationNumber);
//                });
//            } else {
//                log.error("Malformed map key.");
//            }
//        });
        log.info("Context destroyed.");
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        log.info("Context initialised.");
    }


}
