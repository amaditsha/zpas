package zw.co.esolutions.zpas.security.ldap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.security.model.Privilege;
import zw.co.esolutions.zpas.security.repo.PrivilegeRepository;
import zw.co.esolutions.zpas.security.repo.UserRoleRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mabuza on 05 Mar 2019
 */

@Component("customLdapAuthoritiesPopulator")
@Slf4j
public class CustomLdapAuthoritiesPopulator implements LdapAuthoritiesPopulator {

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    PrivilegeRepository privilegeRepository;

    @Override
    public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations dirContextOperations, String username) {

        List<GrantedAuthority> authorities = new ArrayList<>();

        List<String> userPrivileges = userRoleRepository.findAllByProfiles_Username(username).stream()
                .map(userRoles -> {
                    final List<Privilege> privileges = privilegeRepository.findAllByUserRoles(userRoles);
                    return privileges;
                })
                .flatMap(Collection::stream)
                .map(Privilege::getName)
                .collect(Collectors.toList());
        for (String privilege: userPrivileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }

        return authorities;
    }
}
