package zw.co.esolutions.zpas.security.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.integration.camel.config.util.Konstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@Component
@Slf4j
@WebFilter("/*")
@Order(1)
public class SecurityFilter implements Filter {

    @Value("${otp.enabled}")
    String otpEnabled;

    private static final List<String> ALLOWED_PATHS = Arrays.asList(
            "/api/", "/css/", "/fonts/", "/images/", "/js/", "/login", "/logout");

    private static final List<String> OTP_PATHS = Arrays.asList("/otp/page", "/otp");

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.debug("In Security Filter");

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        final HttpSession httpSession = httpServletRequest.getSession();

        String URI = httpServletRequest.getRequestURI();

        boolean allowedPath = ALLOWED_PATHS.stream().anyMatch(path -> URI.contains(path));

        if (allowedPath) {

            log.debug("Continue..");

        } else {

            String authenticated = (String) httpSession.getAttribute(Konstants.AUTHENTICATED);

            log.debug("Is Authenticated : {}", authenticated);

            if (authenticated != null) {

                boolean validateOTP = Boolean.valueOf(otpEnabled);

                if (validateOTP) {

                    String otpAuthenticated = (String) httpSession.getAttribute(Konstants.OTP_AUTHENTICATED);

                    log.debug("Is OTP Authenticated : {}", otpAuthenticated);

                    boolean otpPath = OTP_PATHS.stream().anyMatch(path -> URI.contains(path));

                    log.debug("Is OTP Path : {}", otpPath);

                    if (otpAuthenticated == null) {

                        if (otpPath) {
                            log.info("User is OTP Authenticating");
                        } else {
                            log.info("Redirecting to OTP page");
                            String redirectURI = httpServletRequest.getContextPath() + "/otp/page";
                            log.info("Redirect URI : {}", redirectURI);
                            httpServletResponse.sendRedirect(redirectURI);
                            return;
                        }
                    } else {
                        log.debug("User 2Factor Authenticated");
                    }

                } else {
                    log.debug("OTP Validation disabled");
                }
            } else {
                log.info("User not authenticated");
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
