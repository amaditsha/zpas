package zw.co.esolutions.zpas.security.services.iface.userrole;

import zw.co.esolutions.zpas.dto.profile.UserRoleDTO;
import zw.co.esolutions.zpas.dto.profile.UserRolePrivilegeDTO;
import zw.co.esolutions.zpas.dto.profile.UserRolePrivilegeRequestDTO;
import zw.co.esolutions.zpas.security.model.UserRole;

import java.util.List;
import java.util.Optional;

public interface UserRoleService {
    List<UserRole> getAllUserRoles();
    UserRole createUserRole(UserRoleDTO userRoleDTO);
    UserRole addPrivilegeToUserRole(UserRolePrivilegeRequestDTO userRolePrivilegeRequestDTO);
    UserRole removePrivilegeFromUserRole(UserRolePrivilegeRequestDTO userRolePrivilegeRequestDTO);
    List<UserRolePrivilegeDTO> getUserRolePrivileges(String userRoleId);
    UserRole updateUserRole(UserRole userRole);
    UserRole deleteUserRole(UserRole userRole);
    Optional<UserRole> getUserRoleById(String id);
}
