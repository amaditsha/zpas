package zw.co.esolutions.zpas.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Created by alfred on 01 Apr 2019
 */
@Slf4j
@Component
public class JpaUserDetailsContextMapper implements UserDetailsContextMapper {
    @Autowired
    private UserDetailsService zpasUserDetailsService;

    @Override
    public UserDetails mapUserFromContext(DirContextOperations dirContextOperations, String username, Collection<? extends GrantedAuthority> collection) {
        log.info("username is: =============>{}", username);
        final UserPrincipal userPrincipal = (UserPrincipal) zpasUserDetailsService.loadUserByUsername(username);
        userPrincipal.setAuthorities(collection);
        return userPrincipal;
    }

    @Override
    public void mapUserToContext(UserDetails userDetails, DirContextAdapter dirContextAdapter) {

    }
}
