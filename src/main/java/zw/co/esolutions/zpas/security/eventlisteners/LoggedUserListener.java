package zw.co.esolutions.zpas.security.eventlisteners;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.Person;
import zw.co.esolutions.zpas.security.UserPrincipal;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.security.repo.ProfileRepository;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;

import javax.servlet.http.HttpSession;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by alfred on 19 Mar 2019
 */
@Slf4j
@Component
public class LoggedUserListener implements ApplicationListener<AuthenticationSuccessEvent> {
//public class LoggedUserListener implements AuthenticationSuccessHandler {

    @Autowired
    private HttpSession session;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private AccountsService accountsService;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        UserPrincipal userPrincipal = (UserPrincipal) event.getAuthentication().getPrincipal();
        String loggedInUsername = userPrincipal.getUsername();
        final Profile profile = profileRepository.findByUsername(loggedInUsername);

        log.info("Authentication Success.");
        if (profile != null) {
            String lastLoggedInAsId = profile.getLastLoggedInAsId();
            String lastLoggedInAsName = profile.getLastLoggedInAsName();
            session.setAttribute("username", loggedInUsername);
            session.setAttribute("profileId", profile.getId());
            profileService.successLoginHandler(loggedInUsername);
            setHighestUserRoleInSession(profile);
            final String personId = profile.getPersonId();
            final String financialInstitutionId = profile.getFinancialInstitutionId();
            if (StringUtils.isEmpty(personId)) {
                log.info("Person ID not set for current profile: {}", loggedInUsername);
                log.info("Financial Institution Id is: {}", profile.getFinancialInstitutionId());

                String bicFi = "";
                if(StringUtils.isNoneBlank(financialInstitutionId)){
                    Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(financialInstitutionId);
                    bicFi = financialInstitutionOptional.map(financialInstitution ->
                            financialInstitution.getOrganisationIdentification().stream()
                                    .map(identification -> identification.getBICFI()).findFirst()
                                    .orElse("")).orElse("");
                }
                log.info("BICFI: " + bicFi);
                session.setAttribute("clientName", profile.getFirstNames() + " " + profile.getLastName());
                session.setAttribute("clientId", "");
                session.setAttribute("loginAsId", StringUtils.isNoneBlank(lastLoggedInAsId)? lastLoggedInAsId : profile.getFinancialInstitutionId());
                session.setAttribute("loginAsName", StringUtils.isNoneBlank(lastLoggedInAsName)? lastLoggedInAsName : profile.getFirstNames() + " " + profile.getLastName());
                session.setAttribute("financialInstitutionId", profile.getFinancialInstitutionId());
                session.setAttribute("bicFi", bicFi);
            } else {
                final Optional<Person> personOptional = clientService.getPersonById(personId);
                personOptional.ifPresent(person -> {
                    final Optional<Organisation> organisationOptional = financialInstitutionService.getOrganisationByEmployeeId(personId);
                    final String fi = organisationOptional.map(Organisation::getId).orElse(profile.getFinancialInstitutionId());
                    String bicFi = "";
                    if(StringUtils.isNoneBlank(financialInstitutionId)){
                        Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(financialInstitutionId);
                        bicFi = financialInstitutionOptional.map(financialInstitution -> financialInstitution.getBIC()).orElse("");
                    }
                    log.info("BICFI: " + bicFi);
                    log.info("Financial Institution Id is: {}", fi);
                    final String personName = person.getName();
                    session.setAttribute("clientName", personName);
                    session.setAttribute("clientId", person.getId());
                    session.setAttribute("loginAsId", StringUtils.isNoneBlank(lastLoggedInAsId)? lastLoggedInAsId : person.getId());
                    session.setAttribute("loginAsName", StringUtils.isNoneBlank(lastLoggedInAsName)? lastLoggedInAsName : personName);
                    session.setAttribute("financialInstitutionId", fi);
                    session.setAttribute("bicFi", bicFi);

                    log.info("logInAsId: " + session.getAttribute("loginAsId"));
                    accountsService.accountBalanceRequest((String) session.getAttribute("loginAsId"));
                });
            }

            notificationService.onStartUp(session);
        } else {
            log.info("Profile not found for username: \"{}\"", loggedInUsername);
        }
    }

    private void setHighestUserRoleInSession(Profile profile) {
        final List<UserRole> userRoles = profile.getUserRoles().stream()
                .sorted(Comparator.comparingInt(userRole -> userRole.getRoleLevel().getWeight()))
                .collect(Collectors.toList());
        if (userRoles.size() > 0) {
            final UserRole highestUserRole = userRoles.get(0);
            session.setAttribute("highestUserRoleLevel", highestUserRole.getRoleLevel().name());
        }
    }
}