package zw.co.esolutions.zpas.security.services.impl.profile;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.dto.profile.PasswordDTO;
import zw.co.esolutions.zpas.dto.profile.ProfileDTO;
import zw.co.esolutions.zpas.dto.profile.UsernameCheckDTO;
import zw.co.esolutions.zpas.security.PasswordUtil;
import zw.co.esolutions.zpas.security.email.MailSendUtil;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.security.repo.ProfileRepository;
import zw.co.esolutions.zpas.security.repo.UserRoleRepository;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.LdapUtil;
import zw.co.esolutions.zpas.utilities.util.StringUtil;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by mabuza on 06 Mar 2019
 */
@Slf4j
@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {

    public static final String BASE_DN = "o=zpas";

    @Autowired
    private MailSendUtil mailSendUtil;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public Optional<Profile> createProfile(ProfileDTO profileDTO) {
        log.info("Creating profile -> " + profileDTO.getFirstNames());

        List<UserRole> userRoles = userRoleRepository.getRolesByIdsIn(profileDTO.getUserRoles());

        Profile profile = new Profile();
        profile.setFirstNames(StringUtil.capitaliseWord(profileDTO.getFirstNames()));
        profile.setLastName(StringUtil.capitaliseWord(profileDTO.getLastName()));
        profile.setEmail(profileDTO.getEmail());
        profile.setUsername(profileDTO.getEmail());
        profile.setMobileNumber(profileDTO.getMobileNumber());
        profile.setPhoneNumber(profileDTO.getPhoneNumber());
        profile.setUserRoles(userRoles);
        profile.setDateCreated(OffsetDateTime.now());
        profile.setStatus(EntityStatus.PENDING_APPROVAL);
        profile.setId(GenerateKey.generateEntityId());
        profile.setChangePassword(true);
        profile.setProfileType(profileDTO.getProfileType());
        profile.setEnabled(true);
        profile.setLocked(false);
        profile.setFinancialInstitutionId(profileDTO.getFinancialInstitutionId());
        profile.setPersonId(profileDTO.getPersonId());

        profileRepository.save(profile);
        log.info("Profile created successfully.");
        return Optional.ofNullable(profile);
    }

    @Override
    public Optional<Profile> updateProfile(Profile profile) {
        profile.setStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return Optional.ofNullable(profileRepository.save(profile));
    }

    @Override
    public Optional<Profile> approveProfile(String id) {
        Profile savedProfile = profileRepository.findProfileById(id).get();
        String password = PasswordUtil.getPassword(8);
        log.info("Profile to be approved -> {}", savedProfile.getUsername());

        if (savedProfile.getStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
            savedProfile.setStatus(EntityStatus.ACTIVE);
            profileRepository.save(savedProfile);
        } else if(savedProfile.getStatus().equals(EntityStatus.PENDING_APPROVAL)) {
            try {
                savedProfile.setUserPassword(password);
                LdapUtil.createLDAPEntry(savedProfile);
                savedProfile.setStatus(EntityStatus.ACTIVE);
                profileRepository.save(savedProfile);
                mailSendUtil.sendPassword(savedProfile);
                log.info("Temporary password -> {}", password);
                log.info("Email sent");
            } catch (Exception e) {
                e.printStackTrace();
                log.debug("User Already In Directory");
            }
        } else if (savedProfile.getStatus().equals(EntityStatus.ACTIVE)) {
            return Optional.ofNullable(savedProfile);
        }
        savedProfile.setStatus(EntityStatus.ACTIVE);
        profileRepository.save(savedProfile);
        return Optional.ofNullable(savedProfile);
    }

    @Override
    public Optional<Profile> deleteProfile(Profile profile) {
        return null;
    }

    @Override
    public Optional<Profile> rejectProfile(String profileId) {
        return profileRepository.findProfileById(profileId).map(profile -> {
            profile.setStatus(EntityStatus.DISAPPROVED);
            return Optional.of(profile);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<Profile> getProfileById(String id) {
        return profileRepository.findProfileById(id);
    }

    @Override
    public Optional<Profile> getProfileByPersonId(String personId) {
        return profileRepository.findByPersonId(personId);
    }

    @Override
    public Optional<Profile> failedLoginHandler(String username) {
        final Profile profile = profileRepository.findByUsername(username);
        if (profile == null) {
            return Optional.empty();
        } else {
            final int loginAttempts = profile.getLoginAttempts();
            if (loginAttempts > 2) {
                return lockProfile(profile.getId());
            }
            profile.setLoginAttempts(loginAttempts + 1);
            return Optional.ofNullable(profile);
        }
    }

    @Override
    public Optional<Profile> successLoginHandler(String username) {
        final Profile profile = profileRepository.findByUsername(username);
        if (profile == null) {
            return Optional.empty();
        } else {
            profile.setLoginAttempts(0);
            profile.setLocked(false);
            return Optional.ofNullable(profile);
        }
    }

    @Override
    public Optional<UsernameCheckDTO> checkUsernameAvailability(String username) {
        Profile profile = profileRepository.findByUsername(username);
        if(profile == null) {
            return Optional.of(UsernameCheckDTO.builder()
                    .available(true)
                    .extraInformation("Username available.")
                    .username(username)
                    .build());
        } else {
            return Optional.of(UsernameCheckDTO.builder()
                    .available(false)
                    .extraInformation(String.format("%s %s", "Username already taken by", profile.getFullName()))
                    .username(username)
                    .build());
        }
    }

    @Override
    public Optional<Profile> lockProfile(String profileId) {
        return profileRepository.findProfileById(profileId).map(profile -> {
            if (profile.isLocked()) {
                return Optional.ofNullable(profile);
            }
            profile.setLocked(true);
            return Optional.ofNullable(profile);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<Profile> unlockProfile(String profileId) {
        return profileRepository.findProfileById(profileId).map(profile -> {
            if (!profile.isLocked()) {
                return Optional.ofNullable(profile);
            }
            profile.setLocked(false);
            return Optional.ofNullable(profile);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<Profile> disableProfile(String profileId) {
        return profileRepository.findProfileById(profileId).map(profile -> {
            if (!profile.isEnabled()) {
                return Optional.ofNullable(profile);
            }
            profile.setEnabled(false);
            return Optional.ofNullable(profile);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<Profile> enableProfile(String profileId) {
        return profileRepository.findProfileById(profileId).map(profile -> {
            if (profile.isEnabled()) {
                return Optional.ofNullable(profile);
            }
            profile.setEnabled(true);
            return Optional.ofNullable(profile);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<String> changeProfilePassword(PasswordDTO passwordDTO) {
        log.info("The Password DTO is: {}", passwordDTO);
        if(passwordDTO.getUsername() == null || passwordDTO.getOldPassword() == null) {
            return Optional.ofNullable(SystemConstants.AUTH_STATUS_INVALID_CREDENTIALS);
        }
        String result = "";
        String authResult = checkPassword(passwordDTO.getUsername(), passwordDTO.getOldPassword());
        if (SystemConstants.AUTH_STATUS_AUTHENTICATED.equalsIgnoreCase(authResult)) {
            final Optional<Profile> optionalProfile = this.getProfileByUsername(passwordDTO.getUsername());
            log.info("Username is: {}", passwordDTO.getUsername());
            if (optionalProfile.isPresent()) {
                final Profile profile = optionalProfile.get();
                profileRepository.save(profile);
                result = LdapUtil.changeUserPassword(profile.getUsername(), passwordDTO.getOldPassword(),
                        passwordDTO.getNewPassword());
                if (result.equals(SystemConstants.CHANGE_PASSWORD_SUCCESS)) {
                    profile.setChangePassword(false);
                    profileRepository.save(profile);
                }
            } else {
                log.error("Profile not found.");
                result = SystemConstants.AUTH_STATUS_SYSTEM_ERROR;
            }
        } else {
            if (SystemConstants.AUTH_STATUS_INVALID_CREDENTIALS.equalsIgnoreCase(authResult)) {
                return Optional.ofNullable(SystemConstants.INVALID_OLD_PASSWORD);
            }
            return Optional.ofNullable(authResult);
        }
        return Optional.ofNullable(result);
    }

    public String checkPassword(String userName, String password) {
        log.info("\n\n\nUsername is " + userName + " password is " + password);
        return LdapUtil.validateUser(userName, password);
    }

    @Override
    public Optional<Profile> getProfileByUsername(String username) {
        return Optional.ofNullable(profileRepository.findByUsername(username));
    }

    @Override
    public List<Profile> getAllProfiles() {
        return profileRepository.findAll();
    }

}
