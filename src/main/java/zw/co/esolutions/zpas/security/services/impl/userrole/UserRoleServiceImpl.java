package zw.co.esolutions.zpas.security.services.impl.userrole;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.dto.profile.UserRoleDTO;
import zw.co.esolutions.zpas.dto.profile.UserRolePrivilegeDTO;
import zw.co.esolutions.zpas.dto.profile.UserRolePrivilegeRequestDTO;
import zw.co.esolutions.zpas.security.model.Privilege;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.security.repo.PrivilegeRepository;
import zw.co.esolutions.zpas.security.repo.UserRoleRepository;
import zw.co.esolutions.zpas.security.services.iface.userrole.UserRoleService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.RoleLevel;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by mabuza on 06 Mar 2019
 */
@Slf4j
@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Override
    public List<UserRole> getAllUserRoles() {
        return userRoleRepository.findAll();

    }

    @Override
    public UserRole createUserRole(UserRoleDTO userRoleDTO) {
        log.info("Creating user role -> " + userRoleDTO.getName());

        UserRole userRole = new UserRole();
        userRole.setName(userRoleDTO.getName());
        userRole.setEntityStatus(EntityStatus.ACTIVE.name());
        userRole.setId(GenerateKey.generateEntityId());
        userRole.setDateCreated(OffsetDateTime.now());

        try {
            RoleLevel roleLevel = RoleLevel.valueOf(userRoleDTO.getRoleLevel());
            userRole.setRoleLevel(roleLevel);
        } catch (RuntimeException re) {
            userRole.setRoleLevel(RoleLevel.SYSTEM);
        }

        userRoleRepository.save(userRole);
        log.info("user role created successfully.");
        return userRole;
    }

    @Override
    public UserRole addPrivilegeToUserRole(UserRolePrivilegeRequestDTO userRolePrivilegeRequestDTO) {
        return userRoleRepository.findById(userRolePrivilegeRequestDTO.getUserRoleId()).map(userRole -> {
            privilegeRepository.findById(userRolePrivilegeRequestDTO.getPrivilegeId()).ifPresent(privilege -> {
                userRole.getPrivileges().add(privilege);
            });
            return userRole;
        }).orElse(null);
    }

    @Override
    public UserRole removePrivilegeFromUserRole(UserRolePrivilegeRequestDTO userRolePrivilegeRequestDTO) {
        return userRoleRepository.findById(userRolePrivilegeRequestDTO.getUserRoleId()).map(userRole -> {
            privilegeRepository.findById(userRolePrivilegeRequestDTO.getPrivilegeId()).ifPresent(privilege -> {
                userRole.getPrivileges().remove(privilege);
            });
            return userRole;
        }).orElse(null);
    }

    @Override
    public List<UserRolePrivilegeDTO> getUserRolePrivileges(String userRoleId) {
        return userRoleRepository.findById(userRoleId).map(userRole -> {
            final Set<UserRolePrivilegeDTO> userRolePrivilegeDTOS = new HashSet<>();
            final List<String> privilegesEnabledForUserRoleIds = new ArrayList<>();
            if(privilegesEnabledForUserRoleIds.size() == 0) {
                privilegesEnabledForUserRoleIds.add("none");
            }

                //already enabled privileges
            userRole.getPrivileges().forEach(privilege -> {
                final String privilegeId = privilege.getId();
                privilegesEnabledForUserRoleIds.add(privilegeId);
                UserRolePrivilegeDTO userRolePrivilegeDTO = UserRolePrivilegeDTO.builder()
                        .privilegeEnabled(true)
                        .privilegeId(privilegeId)
                        .privilegeName(privilege.getName())
                        .roleId(userRole.getId())
                        .roleLevel(userRole.getRoleLevel())
                        .roleName(userRole.getName())
                        .build();
                userRolePrivilegeDTOS.add(userRolePrivilegeDTO);
            });

            if(privilegesEnabledForUserRoleIds.size() == 0) {
                privilegesEnabledForUserRoleIds.add("none");
            }

            //find all the other roles and add them to the set, these are disabled on the role
            final List<Privilege> disabledPrivilegesForUserRole = privilegeRepository.getPrivilegesByIdsNotIn(privilegesEnabledForUserRoleIds);

            disabledPrivilegesForUserRole.forEach(privilege -> {
                UserRolePrivilegeDTO userRolePrivilegeDTO = UserRolePrivilegeDTO.builder()
                        .privilegeEnabled(false)
                        .privilegeId(privilege.getId())
                        .privilegeName(privilege.getName())
                        .roleId(userRole.getId())
                        .roleLevel(userRole.getRoleLevel())
                        .roleName(userRole.getName())
                        .build();
                userRolePrivilegeDTOS.add(userRolePrivilegeDTO);
            });
            return userRolePrivilegeDTOS.stream().collect(Collectors.toList());
        }).orElse(new ArrayList<>());
    }

    @Override
    public UserRole updateUserRole(UserRole userRole) {
        return null;
    }

    @Override
    public UserRole deleteUserRole(UserRole userRole) {
        return null;
    }

    @Override
    public Optional<UserRole> getUserRoleById(String id) {
        return userRoleRepository.findById(id);
    }
}
