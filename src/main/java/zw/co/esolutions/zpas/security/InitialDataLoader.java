package zw.co.esolutions.zpas.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.enums.CurrencyCode;
import zw.co.esolutions.zpas.model.Currency;
import zw.co.esolutions.zpas.model.Operator;
import zw.co.esolutions.zpas.repository.CurrencyRepository;
import zw.co.esolutions.zpas.repository.OperatorRepository;
import zw.co.esolutions.zpas.security.model.Privilege;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.security.repo.PrivilegeRepository;
import zw.co.esolutions.zpas.security.repo.ProfileRepository;
import zw.co.esolutions.zpas.security.repo.UserRoleRepository;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;
import zw.co.esolutions.zpas.services.iface.currency.CurrencyService;
import zw.co.esolutions.zpas.utilities.enums.*;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.LdapUtil;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Created by mabuza on 21 Feb 2019
 */
@Slf4j
@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    boolean alreadySetup = false;

    @Autowired
    ProfileService profileService;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private CurrencyService currencyService;

   /* @Autowired
    private PasswordEncoder passwordEncoder;*/

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        Profile adminProfile = profileRepository.findByUsername("system");

        if (adminProfile != null) {
            alreadySetup = true;
            currencyService.refreshCurrencies();
            return;
        }

        Operator operator = new Operator();
        operator.setCode("ESOLUTIONS");
        operator.setContactPhone("04-480 344");
        operator.setEmailAddress("admin@esolutions.co.zw");
        operator.setName("ESOLUTIONS");
        operator.setEntityStatus(EntityStatus.ACTIVE);
        operator.setId("9039328937681");
        operator.setDateCreated(OffsetDateTime.now());

        operatorRepository.save(operator);

        List<Privilege> adminPrivileges = Stream.of(AccessRights.values())
                .map(accessRights -> {
                    Privilege privilege = new Privilege();
                    privilege.setDateCreated(OffsetDateTime.now());
                    privilege.setId(GenerateKey.generateEntityId());
                    privilege.setName(accessRights.name());
                    return privilege;
                })
                .collect(toList());
        privilegeRepository.saveAll(adminPrivileges);

        createRoleIfNotFound("SuperAdmin", adminPrivileges);

        UserRole adminRole = userRoleRepository.getUserRoleByName("SuperAdmin");
        Profile profile = new Profile();
        profile.setId("9394855454345");
        profile.setFirstNames("Super");
        profile.setLastName("Admin");
        profile.setEmail("insureaccess.esolutions@gmail.com");
        profile.setUserPassword("test");
        profile.setStatus(EntityStatus.PENDING_APPROVAL);
        profile.setOwnerId("9039328937681");
        profile.setProfileType(ProfileType.SYSTEM);
        profile.setOwnerType(OwnerType.OPERATOR);
        profile.setUsername("system");
        profile.setMobileNumber("0772881889");
        profile.setUserRoles(Collections.singletonList(adminRole));
        profile.setEnabled(true);
        profile.setLocked(false);
        profile.setDateCreated(OffsetDateTime.now());
        profileRepository.save(profile);


        alreadySetup = true;

        Profile profile1 = profileRepository.findByUsername("system");
        approveProfile(profile1.getId());
    }

    /*@Transactional
    Privilege createPrivilegeIfNotFound(String name) {

        Privilege privilege = privilegeRepository.getPrivilegeByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilege.setId(GenerateKey.generateEntityId());
            privilegeRepository.save(privilege);
        }
        return privilege;
    }*/

    @Transactional
    void createRoleIfNotFound(String name, List<Privilege> privileges) {

        UserRole role = userRoleRepository.getUserRoleByName(name);
        if (role == null) {
            role = new UserRole(name);
            role.setDateCreated(OffsetDateTime.now());
            role.setId("1687865689433");
            role.setRoleLevel(RoleLevel.SYSTEM);
            role.setEntityStatus(EntityStatus.ACTIVE.name());
            role.setPrivileges(privileges);

            userRoleRepository.save(role);
        }
    }

    @Transactional
    public Profile approveProfile(String id) {
        Profile savedProfile = profileRepository.findProfileById(id).get();
        String password = PasswordUtil.getPassword(8);
        log.info("Profile to be approved -> {}", savedProfile.getUsername());
        log.info("Temporary password -> {}", password);
        try {
            savedProfile.setUserPassword(password);
            LdapUtil.createLDAPEntry(savedProfile);
            log.info("Email sent");
        } catch (Exception e) {
            e.printStackTrace();
            log.debug("User Already In Directory");
        }
        savedProfile.setStatus(EntityStatus.ACTIVE);

        return savedProfile;
    }
}
