package zw.co.esolutions.zpas.security.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.dto.message.MessageRequestDTO;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.services.iface.message.MessageService;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by mabuza on 10 Mar 2019
 */
@Component
public class MailSendUtil {
    @Value("${MAIL.SEND.EMAIL}")
    private String originator;

    @Autowired
    private MessageService messageService;

    public void sendPassword(Profile profile) {

        // Create an email instance
        MessageRequestDTO messageRequestDTO = MessageRequestDTO.builder()
                .messageDate(OffsetDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                .subject("ZEEPAY Login Details")
                .originator(originator)
                .destination(profile.getEmail())
                .messageText("Hi " + profile.getFirstNames() + ",\n" +
                        "                        Your profile on ZPAS has been approved. You will be required to change your password on initial login.\n" +
                        "                        Your username is: " + profile.getUsername() + "\n" +
                        "                        and \n" +
                        "                        your password is: " + profile.getUserPassword() + "\n" +
                        "                        Please Make sure you enter a secure password. \n" +
                        "                        FOR ANY QUERIES. CONTACT OUR OFFICES ON (04) 480 344\n")
                .attachmentFilePaths(new ArrayList<>())
                .messageChannel(MessageChannel.EMAIL)
                .messageReference(RefGen.getReference("E"))
                .username(profile.getUsername())
                .build();

        // Send mail
        messageService.send(messageRequestDTO, Optional.ofNullable(profile.getPersonId()).orElse(""));
    }

    public void sendOTP(Profile profile, String otp) {
        final MessageRequestDTO messageRequestDTO = MessageRequestDTO.builder()
                .messageDate(OffsetDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                .subject("ZEEPAY OTP")
                .originator(originator)
                .destination(profile.getEmail())
                .messageText("Hi " + profile.getFirstNames() + ",\n" +
                        "                        Your OPT is " + otp + ". You are required to enter the OTP before access to the system.\n" +
                        "                        FOR ANY QUERIES. CONTACT OUR OFFICES ON (04) 480 344\n")
                .attachmentFilePaths(new ArrayList<>())
                .messageChannel(MessageChannel.EMAIL)
                .messageReference(RefGen.getReference("E"))
                .username(profile.getUsername())
                .build();
        messageService.send(messageRequestDTO, Optional.ofNullable(profile.getPersonId()).orElse(""));
    }
}
