package zw.co.esolutions.zpas.security.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.OwnerType;
import zw.co.esolutions.zpas.utilities.enums.ProfileType;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mabuza on 13 Feb 2019
 */
@Audited
@Data
@Entity
@NoArgsConstructor
public class Profile implements Serializable, Auditable {
    @Id
    @Column(length=30)
    private String id;
    @Column(length=50)
    @Field
    @Analyzer(definition = "customanalyzer")
    @NotNull
    private String lastName;
    @Column(length=50)
    @Field
    @Analyzer(definition = "customanalyzer")
    @NotNull
    private String firstNames;
    @Column(length=70)
    @Field
    @Analyzer(definition = "customanalyzer")
    @NotNull
    private String username;
    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;
    @Field
    @Analyzer(definition = "customanalyzer")
    @Column(length=30)
    private String ownerId;
    @Enumerated(EnumType.STRING)
    @Column(length=70)
    private OwnerType ownerType;

    @Enumerated(EnumType.STRING)
    private ProfileType profileType;

    @Enumerated(EnumType.STRING)
    @Column(length=70)
    private EntityStatus status;
    @Column(length=30)
    private String userPassword;
    @Column(length=70)
    @Field
    @Analyzer(definition = "customanalyzer")
    private String email;
    @Column(length=30)
    @Field
    @Analyzer(definition = "customanalyzer")
    private String phoneNumber;
    @Column(length=30)
    @Field
    @Analyzer(definition = "customanalyzer")
    @NotNull
    private String mobileNumber;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime lastLoginDate;
    @Column
    private int loginAttempts;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    private OffsetDateTime passwordExpiryDate;
    @Column
    private boolean changePassword;
    @Column
    private boolean enabled;

    private boolean locked;

    @Column
    private boolean loggedIn;
    @Column(length=30)
    private String ipAddress;
    @Column(length=30)
    private String participantBranchId;
    @Column(length=30)
    private String personId;
    @Column(length=30)
    private String financialInstitutionId;
    @Column(length=50)
    private String lastLoggedInAsId;
    @Column(length=50)
    private String lastLoggedInAsName;
    @ManyToMany
    @JoinTable(
            name = "profiles_user_roles",
            joinColumns = @JoinColumn(
                    name = "profile_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "user_role_id", referencedColumnName = "id"))
    private List<UserRole> userRoles;
    @Column(columnDefinition= "TIMESTAMP WITH TIME ZONE")
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @DateBridge(resolution = Resolution.MONTH)
    private OffsetDateTime dateCreated;
    @Version
    @Column
    private long version;

    public String getFullName() {
        return String.format("%s %s", firstNames, lastName);
    }

    public boolean canBeApproved() {
        return status == EntityStatus.PENDING_EDIT_APPROVAL || status == EntityStatus.PENDING_APPROVAL;
    }

    @Override
    public String toString() {
        StringBuilder userRolesSb = new StringBuilder("[");
        if(userRoles != null) {
            for(int i = 0; i < userRoles.size(); i++) {
                final UserRole userRole = userRoles.get(i);
                if(i < userRoles.size() - 1) {
                    userRolesSb.append(userRole.getName() + ", ");
                } else {
                    userRolesSb.append(userRole.getName());
                }
            }
        }
        userRolesSb.append("]");
        return "Profile{" +
                "id='" + id + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstNames='" + firstNames + '\'' +
                ", username='" + username + '\'' +
                ", ownerId='" + ownerId + '\'' +
                ", OwnerType=" + ownerType +
                ", status=" + status +
                ", userPassword='" + userPassword + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", lastLoginDate=" + lastLoginDate +
                ", loginAttempts=" + loginAttempts +
                ", passwordExpiryDate=" + passwordExpiryDate +
                ", changePassword=" + changePassword +
                ", locked=" + locked +
                ", enabled=" + enabled +
                ", loggedIn=" + loggedIn +
                ", ipAddress='" + ipAddress + '\'' +
                ", participantBranchId='" + participantBranchId + '\'' +
                ", personId='" + personId + '\'' +
                ", dateCreated=" + dateCreated +
                ", version=" + version +
                '}';
    }

    @Override
    public String getEntityName() {
        return "PROFILE";
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<String, String>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        attributesMap.put("dateCreated", getDateCreated().format(formatter) + "");
        attributesMap.put("lastUpdated", getLastUpdated() + "");
        attributesMap.put("entityStatus", getStatus() + "");
        return attributesMap;
    }

    @Override
    public String getAuditableAttributesString() {
        return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
    }

    @Override
    public String getInstanceName() {
        return null;
    }
}
