package zw.co.esolutions.zpas.security.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.security.model.Privilege;
import zw.co.esolutions.zpas.security.model.UserRole;

import java.util.List;

@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege, String> {
    @Query("SELECT p FROM Privilege p WHERE p.id in :ids ORDER BY p.dateCreated DESC")
    List<Privilege> getPrivilegesByIdsIn(@Param("ids") List<String> ids);

    @Query("SELECT p FROM Privilege p WHERE p.id not in :ids ORDER BY p.dateCreated DESC")
    List<Privilege> getPrivilegesByIdsNotIn(@Param("ids") List<String> ids);

    List<Privilege> findAllByUserRoles(UserRole userRole);
}
