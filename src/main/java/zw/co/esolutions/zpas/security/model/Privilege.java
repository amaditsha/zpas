package zw.co.esolutions.zpas.security.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Created by mabuza on 21 Feb 2019
 */
@Audited
@Entity
@Data
@NoArgsConstructor
public class Privilege implements Serializable {
    @Id
    @Column(length=30)
    private String id;

    private String name;
    private OffsetDateTime dateCreated;

    @ManyToMany(mappedBy = "privileges")
    private List<UserRole> userRoles;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Privilege privilege = (Privilege) o;
        return name.equals(privilege.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
