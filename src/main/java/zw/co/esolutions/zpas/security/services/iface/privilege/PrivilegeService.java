package zw.co.esolutions.zpas.security.services.iface.privilege;

import zw.co.esolutions.zpas.security.model.Privilege;

import java.util.List;

public interface PrivilegeService {
    List<Privilege> getAllPrivileges();
    List<Privilege> refreshPrivileges();
}
