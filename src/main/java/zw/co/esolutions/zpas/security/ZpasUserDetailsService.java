package zw.co.esolutions.zpas.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.repo.ProfileRepository;

/**
 * Created by mabuza on 13 Feb 2019
 */
@Slf4j
@Service
public class ZpasUserDetailsService implements UserDetailsService {
    @Autowired
    private ProfileRepository profileRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Profile profile = profileRepository.findByUsername(username);
        log.info("Profile is: {}", profile);
        if (profile == null)
            throw new UsernameNotFoundException("Profile 404");
        return new UserPrincipal(profile);
    }
}
