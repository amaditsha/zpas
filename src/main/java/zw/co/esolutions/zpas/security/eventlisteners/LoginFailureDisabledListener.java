package zw.co.esolutions.zpas.security.eventlisteners;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureDisabledEvent;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;

/**
 * Created by alfred on 19 Mar 2019
 */
@Slf4j
@Component
public class LoginFailureDisabledListener implements ApplicationListener<AuthenticationFailureDisabledEvent> {

    @Autowired
    private ProfileService profileService;

    @Override
    public void onApplicationEvent(AuthenticationFailureDisabledEvent event) {
        String username = (String) event.getAuthentication().getPrincipal();
        log.error("Authentication Failure is: {}", event.getException().getMessage());
        log.info("Username is: {}. Account is disabled.", username);
    }
}