package zw.co.esolutions.zpas.security.services.iface.profile;

import zw.co.esolutions.zpas.dto.profile.PasswordDTO;
import zw.co.esolutions.zpas.dto.profile.ProfileDTO;
import zw.co.esolutions.zpas.dto.profile.UsernameCheckDTO;
import zw.co.esolutions.zpas.security.model.Profile;

import java.util.List;
import java.util.Optional;

public interface ProfileService {
    Optional<Profile> createProfile(ProfileDTO profileDTO);
    Optional<Profile> updateProfile(Profile profile);
    Optional<Profile> approveProfile(String id);
    Optional<Profile> deleteProfile(Profile profile);
    Optional<Profile> rejectProfile(String profileId);
    Optional<Profile> getProfileById(String id);
    Optional<Profile> getProfileByPersonId(String personId);

    Optional<Profile> failedLoginHandler(String username);
    Optional<Profile> successLoginHandler(String username);
    Optional<UsernameCheckDTO> checkUsernameAvailability(String username);
    Optional<Profile> lockProfile(String profileId);
    Optional<Profile> unlockProfile(String profileId);
    Optional<Profile> disableProfile(String profileId);
    Optional<Profile> enableProfile(String profileId);

    Optional<String> changeProfilePassword(PasswordDTO passwordDTO);

    Optional<Profile> getProfileByUsername(String username);
    List<Profile> getAllProfiles();
}
