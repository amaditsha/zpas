package zw.co.esolutions.zpas.security.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import zw.co.esolutions.zpas.utilities.audit.Auditable;
import zw.co.esolutions.zpas.utilities.enums.RoleLevel;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by mabuza on 21 Feb 2019
 */
@Audited
@EqualsAndHashCode(exclude = {"profiles", "roleLevel", "privileges"})
@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserRole implements Serializable, Auditable {
    @Id
    @Column(length=30)
    private String id;
    private String entityStatus;
    private String name;

    @CreationTimestamp
    private OffsetDateTime dateCreated;

    @UpdateTimestamp
    protected OffsetDateTime lastUpdated;

    @JsonIgnore
    @ManyToMany(mappedBy = "userRoles")
    private List<Profile> profiles;

    @Enumerated(EnumType.STRING)
    private RoleLevel roleLevel;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "user_roles_privileges",
            joinColumns = @JoinColumn(
                    name = "user_role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "privilege_id", referencedColumnName = "id"))
    private List<Privilege> privileges;

    public UserRole(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRole userRole = (UserRole) o;
        return id.equals(userRole.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        StringBuilder privillegesSb = new StringBuilder("[");
        if(privileges != null) {
            for(int i = 0; i < privileges.size(); i++) {
                final Privilege privilege = privileges.get(i);
                if(i < privileges.size() - 1) {
                    privillegesSb.append(privilege.getName() + ", ");
                } else {
                    privillegesSb.append(privilege.getName());
                }
            }
        }
        privillegesSb.append("]");
        return "UserRole{" +
                "id='" + id + '\'' +
                ", entityStatus='" + entityStatus + '\'' +
                ", name='" + name + '\'' +
                ", dateCreated=" + dateCreated +
                ", roleLevel=" + roleLevel +
                '}';
    }

    @Override
    public Map<String, String> getAuditableAttributesMap() {
        Map<String, String> attributesMap = new HashMap<String, String>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        attributesMap.put("dateCreated", getDateCreated() + "");
        attributesMap.put("lastUpdated", getLastUpdated() + "");
        attributesMap.put("entityStatus", getEntityStatus() + "");
        attributesMap.put("role", getRoleLevel() + "");
        return attributesMap;
    }

    @Override
    public String getInstanceName() {
        return null;
    }

    @Override
    public String getEntityName() {
        return "USER ROLE";
    }
}