package zw.co.esolutions.zpas;

import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import zw.co.esolutions.zpas.security.eventlisteners.ZpasServletContextListener;

import javax.servlet.ServletContextListener;

@Configuration
@ImportResource({"classpath*:applicationContext.xml"})
public class SpringBeanXmlConfiguration {
    @Bean
    ServletListenerRegistrationBean<ServletContextListener> servletListener() {
        ServletListenerRegistrationBean<ServletContextListener> srb = new ServletListenerRegistrationBean<>();
        srb.setListener(new ZpasServletContextListener());
        return srb;
    }
}
