package zw.co.esolutions.zpas.services.impl.payments.converters;

import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.DebitOrderStatus;
import zw.co.esolutions.zpas.enums.PaymentTypeCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.services.impl.payments.PaymentEntry;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;
import zw.co.esolutions.zpas.utilities.util.RefGen;
import zw.co.esolutions.zpas.utilities.util.SortCodeUtil;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created by cosmas on 16/05/18.
 */
public class SFIUtils {

    private static final Logger log = LoggerFactory.getLogger(SFIUtils.class);
    public static final String FILES_ROOT = StorageProperties.BASE_LOCATION;
    public static final String DEBIT_ORDER_REQUEST = StorageProperties.DEBIT_ORDER_LOCATION_REQUEST;
    public static final String OUTGOING_DEBIT_ORDER_REQUEST = StorageProperties.OUTGOING_DEBIT_ORDER_REQUEST;
    public static final String DEBIT_ORDER_RESPONSE_FOLDER = StorageProperties.DEBIT_ORDER_LOCATION_RESPONSE;
    public static final String DEBIT_ORDER_RESPONSE_ARCHIVE_FOLDER = StorageProperties.DEBIT_ORDER_LOCATION_ARCHIVE;

    public static SFIHeaderRecord generateSFIHeaderRecord(DebitOrderBatch debitOrderBatch, SortCodeUtil sortCodeUtil) throws Exception {

        final Optional<String> receiverSortCodeID = sortCodeUtil.getSingleSortCode(debitOrderBatch.getReceiverID());

        SFIHeaderRecord header = new SFIHeaderRecord(
                debitOrderBatch.getHeaderID(),
                debitOrderBatch.getProcessingDate(),
                debitOrderBatch.getSenderID(),
                receiverSortCodeID.orElse(debitOrderBatch.getReceiverID()),
                debitOrderBatch.getFileID(),
                debitOrderBatch.getWorkCode(),
                debitOrderBatch.getVersion(),
                debitOrderBatch.getBatchResponseCode());

        return header;
    }

    public static SFIHeaderRecord generateSFIHeaderRecord(BulkPayment bulkPayment) throws Exception {
        SFIHeaderRecord header = new SFIHeaderRecord(
                bulkPayment.getHeaderID(),
                bulkPayment.getProcessingDate(),
                bulkPayment.getSenderID(),
                bulkPayment.getReceiverID(),
                bulkPayment.getFileID(),
                bulkPayment.getWorkCode(),
                bulkPayment.getSfiVersion(),
                bulkPayment.getBatchResponseCode());
        return header;
    }



    public static SFITrailerRecord generateSFITrailerRecord(DebitOrderBatch debitOrderBatch) throws Exception {
        SFITrailerRecord sfiTrailerRecord = new SFITrailerRecord(
                debitOrderBatch.getTrailerId(),
                debitOrderBatch.getTotalAmount().longValue(),
                debitOrderBatch.getTotalAmount().longValue(),
                debitOrderBatch.getTotalCount(),
                debitOrderBatch.getTotalCount(),
                debitOrderBatch.getCurrencyCode());
        return sfiTrailerRecord;
    }

    public static SFITrailerRecord generateSFITrailerRecord(BulkPayment bulkPayment) throws Exception {
        SFITrailerRecord sfiTrailerRecord = new SFITrailerRecord(
                bulkPayment.getTrailerId(),
                bulkPayment.getAmount().getAmount().doubleValue(),
                bulkPayment.getAmount().getAmount().doubleValue(),
                bulkPayment.getGroups().size(),
                bulkPayment.getGroups().size(),
                bulkPayment.getAmount().getCurrency().getCodeName());
        return sfiTrailerRecord;
    }

    public static DebitOrderBatch readDebitOrderBatchSFIFileContents(String fileName) throws Exception {
        log.info("fileName: " + fileName);
        final SFIFileContents sfiFileContents = BeanUtils.parseSFIFile(fileName);

        DebitOrderBatch debitOrderBatch = new DebitOrderBatch();
        readDebitOrderBatchInformation(debitOrderBatch, sfiFileContents);
        log.info("DEBIT ORDER BATCH=======================> {}", debitOrderBatch);

        List<DebitOrderBatchItem> debitOrderBatchItems = readDebitOrderBatchItems(debitOrderBatch, sfiFileContents);
        debitOrderBatch.setDebitOrderBatchItems(debitOrderBatchItems);
        return debitOrderBatch;
    }

    private static DebitOrderBatch readDebitOrderBatchInformation(final DebitOrderBatch debitOrderBatch, SFIFileContents sfiFileContents) throws Exception {
        final SFIHeaderRecord headerRecord = sfiFileContents.getHeaderRecord();
        final SFITrailerRecord trailerRecord = sfiFileContents.getTrailerRecord();
        log.info("trailer record exists: " + trailerRecord);
        debitOrderBatch.setHeaderID(headerRecord.getHeaderID());
        debitOrderBatch.setProcessingDate(headerRecord.getProcessingDate());
        debitOrderBatch.setSenderID(headerRecord.getSenderID());
        debitOrderBatch.setReceiverID(headerRecord.getReceiverID());
        debitOrderBatch.setFileID(headerRecord.getFileID());
        debitOrderBatch.setWorkCode(headerRecord.getWorkCode());
        debitOrderBatch.setVersion(headerRecord.getSFIVersion());
        debitOrderBatch.setBatchResponseCode(headerRecord.getSenderName());
        debitOrderBatch.setTrailerId(trailerRecord.getTrailerId());
        debitOrderBatch.setTotalAmount(new BigDecimal(trailerRecord.getTotalDebitValue()));
        debitOrderBatch.setCurrencyCode(trailerRecord.getCurrencyCode());

        return debitOrderBatch;
    }

    public void main1(String[] args) throws Exception {
        SFIHeaderRecord sfiHeaderRecord = new SFIHeaderRecord();
        sfiHeaderRecord.setFileID(GenerateKey.generateFileId());
        sfiHeaderRecord.setWorkCode(RefGen.getReference("WC"));
        sfiHeaderRecord.setVersion("008");
        sfiHeaderRecord.setHeaderID("UHL");
        sfiHeaderRecord.setProcessingDate(OffsetDateTime.now());
        sfiHeaderRecord.setReceiverID("ZB001");
        sfiHeaderRecord.setSenderID("ZB001");
        sfiHeaderRecord.setSenderName("00");

        SFITrailerRecord sfiTrailerRecord = new SFITrailerRecord();
        sfiTrailerRecord.setBatchId(RefGen.getReference("B"));
        sfiTrailerRecord.setCreditItemsCount(0);
        sfiTrailerRecord.setDebitItemsCount(1);
        sfiTrailerRecord.setTotalCreditValue(0.0D);
        sfiTrailerRecord.setTotalDebitValue(500.0D);
        sfiTrailerRecord.setTrailerId("UTL");
        sfiTrailerRecord.setCurrencyCode("ZWL");
        sfiTrailerRecord.setCheckSum("");
        sfiTrailerRecord.setCheckSumTotal("");

        List<SFIDetailRecord> sfiDetailRecords = new ArrayList<>();
        SFIDetailRecord sfiDetailRecord = new SFIDetailRecord();
        sfiDetailRecord.setAmount(0L);
        sfiDetailRecord.setDestinationAccount("1009873687");
        sfiDetailRecord.setDestinationAccountType("Cu");
        sfiDetailRecord.setDestinationName("Tarisai Kunaka");
        sfiDetailRecord.setDestinationSortCode("00192");
        sfiDetailRecord.setNarrative("Salary");
        sfiDetailRecord.setOriginAccount("100983732");
        sfiDetailRecord.setOriginAccountType("Cu");
        sfiDetailRecord.setOriginSortCode("0987121");
        sfiDetailRecord.setProcessingDate(OffsetDateTime.now());
        sfiDetailRecord.setRecordType("DEB");
        sfiDetailRecord.setReferenceID(RefGen.getReference("D"));
        sfiDetailRecord.setTransactionCode("");
        sfiDetailRecord.setUnpaidReason("");
        sfiDetailRecord.setAmountCurrencyCode("ZWL");
        sfiDetailRecord.setOriginCurrencyRate("8");
        sfiDetailRecord.setId("0019203J");
        sfiDetailRecord.setResponse("");

        sfiDetailRecords.add(sfiDetailRecord);
        SFIFileContents sfiFileContents = new SFIFileContents(sfiHeaderRecord, sfiDetailRecords, sfiTrailerRecord);
        sfiFileContents.setTrailerRecord(sfiTrailerRecord);
        sfiFileContents.setHeaderRecord(sfiHeaderRecord);
        sfiFileContents.setDetailRecords(sfiDetailRecords);
        sfiFileContents.setFileName("/opt/eSolutions/zpas/files/debit_orders.sfi");
        try {
            generateSFIFiles(sfiFileContents, sfiFileContents.getFileName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<DebitOrderBatchItem> readDebitOrderBatchItems(DebitOrderBatch debitOrderBatch, SFIFileContents sfiFileContents) throws Exception {
        List<DebitOrderBatchItem> debitOrderBatchItems = new ArrayList<>();
        for(SFIDetailRecord sdr: sfiFileContents.getDetailRecords()) {
            DebitOrderBatchItem debitOrderBatchItem = new DebitOrderBatchItem();
            debitOrderBatchItem.setRecordType(sdr.getRecordType());
            debitOrderBatchItem.setTransactionCode(sdr.getTransactionCode());
            debitOrderBatchItem.setOriginAccount(sdr.getOriginAccount());
            debitOrderBatchItem.setOriginSortCode(sdr.getOriginSortCode());
            debitOrderBatchItem.setOriginAccountType(sdr.getOriginAccountType());
            debitOrderBatchItem.setReference(RefGen.getReference("DOB"));
            debitOrderBatchItem.setReferenceID(sdr.getReferenceID());
            debitOrderBatchItem.setAmount(new BigDecimal(sdr.getAmount()));
            debitOrderBatchItem.setAmountCurrencyCode(sdr.getAmountCurrencyCode());
            debitOrderBatchItem.setNarrative(sdr.getNarrative());
            //TODO SFI-1 read destination sort code
//            sdr.setDestinationSortCode("");
            //TODO SFI-2 read destination account
//            sdr.setDestinationAccount("");
            //TODO SFI-3 read destination account type
//            sdr.setDestinationAccountType("");
            //TODO SFI-8 read destination name
//            sdr.setDestinationName("");

            debitOrderBatchItem.setId(GenerateKey.generateRef());
            debitOrderBatchItem.setLastUpdated(OffsetDateTime.now());
            debitOrderBatchItem.setDateCreated(OffsetDateTime.now());
            debitOrderBatchItem.setStatus(EntityStatus.DRAFT.name());
            debitOrderBatchItem.setItemStatus(DebitOrderStatus.DRAFT);

            debitOrderBatchItems.add(debitOrderBatchItem);
        }
        return debitOrderBatchItems;
    }

    public static SFIFileContents generateSFIContentsFromTransfers(BulkPayment bulkPayment) throws Exception {
        Collection<SFIDetailRecord> records = new ArrayList<>();

        SFIHeaderRecord sfiHeaderRecord = generateSFIHeaderRecord(bulkPayment);
        SFITrailerRecord sfiTrailerRecord = generateSFITrailerRecord(bulkPayment);
        log.info("TRAIL RECORD=======================> " + sfiTrailerRecord);
        for (IndividualPayment individualPayment : bulkPayment.getGroups()) {
            log.info("==========> " + individualPayment.getAmount());
            records.add(generateSFIDetailRecord(individualPayment));
        }

        SFIFileContents sfc = new SFIFileContents(sfiHeaderRecord, records, null);
        String fileName = bulkPayment.getFileID();
        generateSFIFiles(sfc, FILES_ROOT + fileName);
        //     sfc.resetTrailerRecord();
        return sfc;
    }
    public static SFIFileContents generateSFIContentsFromTransfers(DebitOrderBatch debitOrderBatch, List<DebitOrderBatchItem> debitOrderBatchItems, SortCodeUtil sortCodeUtil) throws Exception {
        Collection<SFIDetailRecord> records = new ArrayList<>();
        log.info("Generating the SFI contents from debit order batch ");

        SFIHeaderRecord sfiHeaderRecord = generateSFIHeaderRecord(debitOrderBatch, sortCodeUtil);
        SFITrailerRecord sfiTrailerRecord = generateSFITrailerRecord(debitOrderBatch);

        for (DebitOrderBatchItem debitOrderBatchItem : debitOrderBatchItems) {
            records.add(generateSFIDetailRecord(debitOrderBatchItem));
        }

        log.info("SFI Header Record \n{}\n", sfiHeaderRecord);
        log.info("SFI Trailer Record \n{}\n", sfiTrailerRecord);
        log.info("SFI Detail Record \n{}\n", records);

        SFIFileContents sfc = new SFIFileContents(sfiHeaderRecord, records, sfiTrailerRecord);
        String fileName = debitOrderBatch.getFileName();
        String path = OUTGOING_DEBIT_ORDER_REQUEST + fileName;
        log.info("The path order request is {}", path);
        generateSFIFiles(sfc, path);
        return sfc;
    }



    public static void generateSFIFiles(SFIFileContents contents, String fileName) throws Exception {
        Path filesRoot = Paths.get(FILES_ROOT);
        Path filesResponseFolder = Paths.get(DEBIT_ORDER_RESPONSE_FOLDER);
        Path filesRequest = Paths.get(OUTGOING_DEBIT_ORDER_REQUEST);
        Path filesArchive = Paths.get(DEBIT_ORDER_RESPONSE_ARCHIVE_FOLDER);

        if(!Files.exists(filesRoot)) {
            Files.createDirectories(filesRoot);
        }
        if(!Files.exists(filesResponseFolder)) {
            Files.createDirectories(filesResponseFolder);
        }
        if(!Files.exists(filesRequest)) {
            Files.createDirectories(filesRequest);
        }
        if(!Files.exists(filesArchive)) {
            Files.createDirectories(filesArchive);
        }

        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(fileName));
            writeSFIContentsToWriter(contents, out);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }

    }

    private static void writeSFIContentsToWriter(SFIFileContents contents,Writer writer)throws Exception{
        try {
            writeHeaderRecord(contents, writer);
            writeDetailRecords(contents, writer);
            writeTrailHeader(contents, writer);

        }




        catch (Exception e) {
            throw new Exception(e);
        }
        finally {
            try {
                if (writer != null)
                    writer.close();
            }
            catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        ftp();
    }

    private static void ftp() {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;

        try {
            client.connect("192.168.100.8");
            client.login("petros", "password");

            //
            // Create an InputStream of the file to be uploaded
            //
            String filename = "SFIHeaderRecord.java";
            fis = new FileInputStream(filename);

            //
            // Store file to server
            //
            client.storeFile(filename, fis);
            client.logout();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void writeHeaderRecord(SFIFileContents contents, Writer out) throws Exception{
        SFIHeaderRecord header = contents.getHeaderRecord();
        out.write(header.getSFIHeaderID());
        out.write(header.getSFIProcessingDate());
        out.write(header.getSFISenderID());
        out.write(header.getSFIReceiverID());
        out.write(header.getSFIFileID());
        out.write(header.getSFIWorkCode());
        out.write(header.getSFIVersion());
        out.write(header.getSFISenderName());
        out.write("\n");
    }

    private static void writeDetailRecords(SFIFileContents contents, Writer out) throws Exception{
        Collection<SFIDetailRecord> detailRecords = contents.getDetailRecords();
        for(SFIDetailRecord detailRecord : detailRecords){
            out.write(detailRecord.getSFIRecordType());                     //1
            out.write(detailRecord.getSFIDestinationSortCode());            //2
            out.write(detailRecord.getSFIDestinationAccount());             //3
            out.write(detailRecord.getSFIDestinationAccountType());         //4
            out.write(detailRecord.getSFIAmountCurrencyCode());                //5   *
            out.write(detailRecord.getSFIOriginCurrencyRate());                //6   *
            out.write(detailRecord.getSFITransactionCode());                //7
            out.write(detailRecord.getSFIOriginSortCode());                 //8
            out.write(detailRecord.getSFIOriginAccount());                  //9
            out.write(detailRecord.getSFIOriginAccountType());              //10
            out.write(detailRecord.getSFIAmountCurrencyCode());                //11   *
            out.write(detailRecord.getSFIOriginCurrencyRate());                //12   *
            out.write(detailRecord.getSFIReferenceID());                       //13
            out.write(detailRecord.getSFIDestinationName());                //14
            out.write(detailRecord.getSFIAmount());                         //15
            out.write(detailRecord.getSFIProcessingDate());                 //16
            //    out.write(detailRecord.getSFIUnpaidReason());
//            out.write(rightPad(detailRecord.getResponse(),9));
            out.write(detailRecord.getSFIUnpaidReason());                      //17
            out.write(detailRecord.getSFINarrative());                      //18
            out.write("\n");

        }
    }

    private static void writeTrailHeader(SFIFileContents contents, Writer out) throws Exception{
        // re-set the trailer record
//        contents.resetTrailerRecord();
        SFITrailerRecord trailer = contents.getTrailerRecord();
        // write the trailer record
        out.write(trailer.getSFITrailerId());
        out.write(trailer.getSFICurrencyCode());
        out.write(trailer.getSFITotalDebitValue());
        out.write(trailer.getSFITotalCreditValue());
        out.write(trailer.getSFIDebitItemsCount());
        out.write(trailer.getSFICreditItemsCount());
        out.write("\n");
    }

    private static SFIDetailRecord generateSFIDetailRecord(IndividualPayment individualPayment) throws Exception {
        SFIDetailRecord sdr = new SFIDetailRecord();
        sdr.setRecordType(individualPayment.getType().getCodeName());
        //TODO SFI-1 set destination sort code
        sdr.setDestinationSortCode("");
        //TODO SFI-2 set destination account
        sdr.setDestinationAccount("");
        //TODO SFI-3 set destination account type
        sdr.setDestinationAccountType("");
        sdr.setAmountCurrencyCode(individualPayment.getAmount().getCurrency().getCodeName());
        //TODO SFI-4 set sort code
        sdr.setTransactionCode("");
        //TODO SFI-5 set original sort code
        sdr.setOriginSortCode("");
        //TODO SFI-6 set origin account
        sdr.setOriginAccount("");
        //TODO SFI-7 set origin account
        sdr.setOriginAccountType("");
        sdr.setReferenceID(individualPayment.getId());
        //TODO SFI-8 set destination name
        sdr.setDestinationName("");
        sdr.setAmount(MoneyUtil.convertToCents(individualPayment.getAmount().getAmount().doubleValue()));
        sdr.setProcessingDate(individualPayment.getValueDate());
        sdr.setUnpaidReason("");
        sdr.setResponse("");
        return sdr;
    }

    private static SFIDetailRecord generateSFIDetailRecord(DebitOrderBatchItem debitOrderBatchItem) throws Exception {
        SFIDetailRecord sdr = new SFIDetailRecord();
        sdr.setRecordType(debitOrderBatchItem.getRecordType());
        sdr.setDestinationSortCode(debitOrderBatchItem.getDestinationSortCode());
        sdr.setDestinationAccount(debitOrderBatchItem.getDestinationAccount());
        sdr.setDestinationAccountType(debitOrderBatchItem.getDestinationAccountType());
        sdr.setAmountCurrencyCode(debitOrderBatchItem.getAmountCurrencyCode());
        sdr.setOriginSortCode(debitOrderBatchItem.getOriginSortCode());
        sdr.setOriginAccount(debitOrderBatchItem.getOriginAccount());
        sdr.setOriginAccountType(debitOrderBatchItem.getDestinationAccountType());
        sdr.setReferenceID(debitOrderBatchItem.getReferenceID());
        sdr.setDestinationName(debitOrderBatchItem.getDestinationName());
        sdr.setAmount(debitOrderBatchItem.getAmount().longValue());
        sdr.setProcessingDate(debitOrderBatchItem.getProcessingDate());
        sdr.setResponse(debitOrderBatchItem.getResponse());
        sdr.setUnpaidReason(debitOrderBatchItem.getUnpaidReason());
        sdr.setNarrative(debitOrderBatchItem.getNarrative());
        sdr.setTransactionCode(debitOrderBatchItem.getTransactionCode());
        sdr.setOriginCurrencyRate(debitOrderBatchItem.getOriginCurrencyRate());
        sdr.setResponse(debitOrderBatchItem.getResponse());
        return sdr;
    }


    public static String rightPad(String str, int num) {
        return String.format("%1$-" + num + "s", str);
    }
}
