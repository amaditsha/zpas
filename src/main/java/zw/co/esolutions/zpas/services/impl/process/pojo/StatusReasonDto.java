package zw.co.esolutions.zpas.services.impl.process.pojo;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public final class StatusReasonDto {
    public final String code;
    public final List<String> narratives;

    public StatusReasonDto(String code, List<String> narratives) {
        this.code = code;
        this.narratives = narratives;
    }
}
