package zw.co.esolutions.zpas.services.impl.party;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.party.VirtualIdAvailabilityCheckDTO;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.Person;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.services.iface.party.PartyService;

import java.util.Optional;

/**
 * Created by alfred on 08 Apr 2019
 */
@Slf4j
@Service
public class PartyServiceImpl implements PartyService {
    @Autowired
    private PartyRepository partyRepository;

    @Override
    public Optional<Party> getPartyById(String partyId) {
        return partyRepository.findById(partyId);
    }

    @Override
    public Optional<Party> getPartyByVirtualId(String virtualId) {
        return partyRepository.findPartyByVirtualId(virtualId);
    }

    @Override
    public Optional<VirtualIdAvailabilityCheckDTO> checkVirtualIdAvailability(String virtualId) {
        final String sanitisedVirtualId = Optional.ofNullable(virtualId).map(String::toLowerCase).orElse("");
        return partyRepository.findPartyByVirtualId(sanitisedVirtualId)
                .map(party -> {
                    return Optional.of(VirtualIdAvailabilityCheckDTO.builder()
                            .available(false)
                            .partyId(party.getId())
                            .virtualId(party.getVirtualId())
                            .partyName(party.getName())
                            .build());
                }).orElseGet(() -> {
                    return Optional.of(VirtualIdAvailabilityCheckDTO.builder()
                            .available(true)
                            .partyId("")
                            .virtualId(virtualId)
                            .partyName("")
                            .build());
                });
    }
}
