package zw.co.esolutions.zpas.services.impl.alerts;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Notification;
import zw.co.esolutions.zpas.repository.NotificationRepository;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import javax.servlet.http.HttpSession;
import java.util.List;

@Slf4j
@Service
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    private NotificationRepository notificationRepository;

    // The SimpMessagingTemplate is used to send Stomp over WebSocket messages.
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ProfileService profileService;

    /**
     * Send notification to users subscribed on channel "/user/queue/notify".
     * <p>
     * The message will be sent only to the user with the given username.
     *
     * @param notification The notification message.
     * @param username     The username for the user to send notification.
     */
    public void notify(Notification notification, String username) {
        messagingTemplate.convertAndSendToUser(
                username,
                "/queue/notify",
                notification
        );
        return;
    }

    @Override
    public void onStartUp(HttpSession session) {
        log.info("Load the notification to the queue");
        String username = (String) session.getAttribute("username");
        String clientId = (String) session.getAttribute("clientId");
        List<Notification> notifications = getUnReadNotificationsForClientId(clientId);
        log.info("The number of init notifications for client id {} or username {} is {}", clientId, username, notifications.size());
        notifications.stream().forEach(notification -> {
            notify(notification, username);
        });
    }

    @Override
    public Notification createNotification(Notification notification) {
        log.info("Notification | " + notification.getMessage());

        profileService.getProfileByPersonId(notification.getClientId()).ifPresent(profile -> {
            notify(notification, profile.getUsername());
            notification.setUsername(profile.getUsername());
        });
        notificationRepository.save(notification);
        return notification;
    }

    @Override
    public List<Notification> getNotifications(String clientId) {
        return notificationRepository.findAllByClientId(clientId);
    }

    @Override
    public List<Notification> getUnReadNotificationsForGroupId(String groupId, String clientId) {
        return notificationRepository.findAllByGroupIdAndReadAndClientId(groupId, false, clientId);
    }

    @Override
    public List<Notification> getUnReadNotificationsForClientId(String clientId) {
        return notificationRepository.findAllByReadAndClientId(false, clientId);
    }

    @Override
    public List<Notification> getUnReadNotificationsForUsername(String username) {
        return notificationRepository.findAllByReadAndUsername(false, username);
    }

    @Override
    public Notification markNotificationAsRead(String notificationId) {
        notificationRepository.updateNotification(notificationId, true);
        return notificationRepository.findById(notificationId).get();
    }

    @Override
    public Notification getNotificationByGroupIdAndEntityId(String groupId, String entityId, String clientId) {
        return notificationRepository.findByGroupIdAndEntityIdAndClientId(groupId, entityId, clientId);
    }
}
