package zw.co.esolutions.zpas.services.impl.payments.zimra.response;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alfred on 21 August 2019
 */
@XmlRootElement(name = "ValidatewithContractNumberResponse")
public class BPValidationResponseWithTaxCode {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "GetResponse")
    private GetResponse getResponse;

    public GetResponse getGetResponse() {
        return getResponse;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setGetResponse(GetResponse getResponse) {
        this.getResponse = getResponse;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}