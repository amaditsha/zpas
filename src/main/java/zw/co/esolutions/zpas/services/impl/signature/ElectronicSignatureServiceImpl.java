package zw.co.esolutions.zpas.services.impl.signature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.datatype.ActiveCurrencyAndAmount;
import zw.co.esolutions.zpas.dto.agreement.SignatureCreationDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.ElectronicSignatureRepository;
import zw.co.esolutions.zpas.repository.SignatureConditionRepository;
import zw.co.esolutions.zpas.services.iface.signature.ElectronicSignatureService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ElectronicSignatureServiceImpl implements ElectronicSignatureService {

    @Autowired
    private ElectronicSignatureRepository electronicSignatureRepository;

    @Autowired
    private SignatureConditionRepository signatureConditionRepository;

    @Override
    public ElectronicSignature createElectronicSignature(SignatureCreationDTO signatureCreationDTO) {
        ElectronicSignature electronicSignature = new ElectronicSignature();
        /**
         * Undertaking for which a signature is provided.
         * */
//        electronicSignature.setUndertaking(new Undertaking());
        /**
         * Parameters related to the signature provided.
         * */
        /*Optional<SignatureCondition> signatureConditionOptional = signatureConditionRepository.findById(signatureCreationDTO.getConditionsId());
        if(signatureConditionOptional.isPresent()) {
            electronicSignature.setConditions(signatureConditionOptional.get());
        } else {
            log.error("Failed to get the signature condition with signature condition id {}", signatureCreationDTO.getConditionsId());
        }*/
        electronicSignature.setId(GenerateKey.generateEntityId());
        electronicSignature.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        electronicSignature.setDateCreated(OffsetDateTime.now());
        electronicSignature.setEntityVersion(0L);

        /**
         * Document which is used as a proof of evidence.
         * */
        Document document = new Document();
        document.setId(GenerateKey.generateEntityId());
        document.setDateCreated(OffsetDateTime.now());
        document.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        document.setIssueDate(OffsetDateTime.now());
        document.setCopyDuplicate(CopyDuplicateCode.valueOf(signatureCreationDTO.getCopyDuplicateCode()));
//        Specifies where the document is stored.

        PostalAddress placeOfStorage = new PostalAddress();
        placeOfStorage.setAddressType(AddressTypeCode.valueOf(signatureCreationDTO.getAddressType()));
        placeOfStorage.setStreetName(signatureCreationDTO.getStreetName());
        placeOfStorage.setStreetBuildingIdentification(signatureCreationDTO.getStreetBuildingIdentification());
        placeOfStorage.setTownName(signatureCreationDTO.getTownName());
        placeOfStorage.setState(signatureCreationDTO.getState());
        placeOfStorage.setBuildingName(signatureCreationDTO.getBuildingName());
        placeOfStorage.setFloor(signatureCreationDTO.getFloor());
        placeOfStorage.setDistrictName(signatureCreationDTO.getDistrictName());
        placeOfStorage.setProvince(signatureCreationDTO.getProvince());

        Country country = new Country();
        country.setId(GenerateKey.generateEntityId());
        country.setDateCreated(OffsetDateTime.now());
        country.setLastUpdated(OffsetDateTime.now());
        country.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        country.setEntityVersion(0L);
        country.setName(signatureCreationDTO.getCountryName());

        placeOfStorage.setCountry(country);
        placeOfStorage.setId(GenerateKey.generateEntityId());
        placeOfStorage.setDateCreated(OffsetDateTime.now());
        placeOfStorage.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        placeOfStorage.setEntityVersion(0L);
//        placeOfStorage.setIdentification(new GenericIdentification());
//        placeOfStorage.setRelatedParty(new Party());
//        placeOfStorage.setBICAddress("");
//        placeOfStorage.setStoredDocument(Lists.newArrayList());
//        placeOfStorage.setRemittanceRelatedPayment(new PaymentObligation());
//        placeOfStorage.setMainContact(new ContactPoint());
//        placeOfStorage.setReturnAddress(new ContactPoint());
//        placeOfStorage.setRelatedPayment(new PaymentProcessing());
//        placeOfStorage.setTemporaryIndicator(false);



        document.setPlaceOfStorage(Arrays.asList(placeOfStorage));
//        document.setPaymentObligation(new PaymentObligation());
//        Specifies the type of the document, for example commercial invoice.
        document.setType(DocumentTypeCode.valueOf(signatureCreationDTO.getDocumentTypeCode()));
        document.setStatus(signatureCreationDTO.getDocumentStatus());
//        Role played by a party in the context of a document or in the context of the business linked to the document.
//        document.setPartyRole(Lists.newArrayList());
        document.setPurpose(signatureCreationDTO.getDocumentPurpose());
        document.setAmount(new ActiveCurrencyAndAmount());
//        document.setAgreement(Lists.newArrayList());

        Location location = new Location();
        location.setId(GenerateKey.generateEntityId());
        location.setDateCreated(OffsetDateTime.now());
        location.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        location.setEntityVersion(0L);
        /**
         * Information that locates and identifies a specific address.
         * */


        document.setPlaceOfIssue(location);
        document.setDocumentVersion(Integer.valueOf(signatureCreationDTO.getDocumentVersion()));
        document.setDataSetType(DataSetTypeCode.valueOf(signatureCreationDTO.getDataSetTypeCode()));
//        Indication whether the document must be signed or not.
        document.setSignedIndicator(getBooleanFormValue(signatureCreationDTO.getSignedIndicator()));

        document.setLanguage(LanguageCode.valueOf(signatureCreationDTO.getLanguageCode()));
//        document.setRelatedContract(new RegisteredContract());
        document.setEntityVersion(0L);

        electronicSignature.setRelatedDocument(document);

        ElectronicSignature savedElectronicSignature = electronicSignatureRepository.save(electronicSignature);

        return savedElectronicSignature;
    }

    private Boolean getBooleanFormValue(String booleanStringValue) {
        booleanStringValue = booleanStringValue == null ? "false" : booleanStringValue.equalsIgnoreCase("on")?
                "true": booleanStringValue.equalsIgnoreCase("true")? "true": "false";
        return Boolean.valueOf(booleanStringValue);
    }

    @Override
    public ElectronicSignature updateElectronicSignature(ElectronicSignature electronicSignature) {
        electronicSignature.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return electronicSignatureRepository.save(electronicSignature);
    }

    @Override
    public ElectronicSignature deleteElectronicSignature(String id) {
        Optional<ElectronicSignature> electronicSignatureOptional = electronicSignatureRepository.findById(id);
        ElectronicSignature electronicSignature = null;
        if (electronicSignatureOptional.isPresent()) {
            electronicSignature = electronicSignatureOptional.get();
            electronicSignature.setEntityStatus(EntityStatus.DELETED);
            electronicSignature = electronicSignatureRepository.save(electronicSignature);
        }
        return electronicSignature;
    }


    @Override
    public ElectronicSignature approveElectronicSignature(String id) {
        Optional<ElectronicSignature> optionalElectronicSignature = electronicSignatureRepository.findById(id);
        ElectronicSignature electronicSignature = null;
        if (optionalElectronicSignature.isPresent()) {
            electronicSignature = optionalElectronicSignature.get();
            if(electronicSignature.getEntityStatus() != EntityStatus.DELETED) {
                electronicSignature.setEntityStatus(EntityStatus.ACTIVE);
                electronicSignature = electronicSignatureRepository.save(electronicSignature);
            } else {
                log.info("The electronic signature status has already been deleted.");
            }
        }
        return electronicSignature;
    }

    @Override
    public ElectronicSignature rejectElectronicSignature(String id) {
        Optional<ElectronicSignature> optionalElectronicSignature = electronicSignatureRepository.findById(id);
        ElectronicSignature electronicSignature = null;
        if (optionalElectronicSignature.isPresent()) {
            electronicSignature = optionalElectronicSignature.get();
            if(electronicSignature.getEntityStatus() != EntityStatus.DELETED) {
                electronicSignature.setEntityStatus(EntityStatus.DISAPPROVED);
                electronicSignature = electronicSignatureRepository.save(electronicSignature);
            } else {
                log.info("The electronic signature status has already been deleted.");
            }
        }
        return electronicSignature;
    }

    @Override
    public Optional<ElectronicSignature> getElectronicSignatureById(String signatureId) {
        return electronicSignatureRepository.findById(signatureId);
    }

    @Override
    public List<ElectronicSignature> getElectronicSignatures() {
        return electronicSignatureRepository.findAll();
    }
}
