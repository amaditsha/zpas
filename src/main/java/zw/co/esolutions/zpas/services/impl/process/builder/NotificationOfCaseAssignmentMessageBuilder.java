package zw.co.esolutions.zpas.services.impl.process.builder;
import zw.co.esolutions.zpas.iso.msg.camt030_001_05.CaseAssignment5;
import zw.co.esolutions.zpas.iso.msg.camt030_001_05.ReportHeader5;
import zw.co.esolutions.zpas.iso.msg.camt030_001_05.CaseForwardingNotification3;
import zw.co.esolutions.zpas.iso.msg.camt030_001_05.Case5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt030_001_05.NotificationOfCaseAssignmentV05;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.services.impl.process.pojo.PaymentInfoDto;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationPartyRoleUtil;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentInfoUtil;

@Slf4j
@Component
public class NotificationOfCaseAssignmentMessageBuilder {
    @Autowired
    InvestigationPartyRoleUtil investigationPartyRoleUtil;

    @Autowired
    PaymentInfoUtil paymentInfoUtil;

    public NotificationOfCaseAssignmentV05 buildNotificationOfCaseAssignmentMsg(PaymentInvestigationCase paymentInvestigationCase){

        log.info("building camt030 message.....");

        NotificationOfCaseAssignmentV05 notificationOfCaseAssignmentV05 = new NotificationOfCaseAssignmentV05();

        PaymentInfoDto paymentInfoDto = paymentInfoUtil.getPaymentInfo(paymentInvestigationCase.getUnderlyingPayment());

        ReportHeader5 header = new ReportHeader5();
        header.getId();




        notificationOfCaseAssignmentV05.setHdr(new ReportHeader5());
        notificationOfCaseAssignmentV05.setCase(new Case5());
        notificationOfCaseAssignmentV05.setAssgnmt(new CaseAssignment5());
        notificationOfCaseAssignmentV05.setNtfctn(new CaseForwardingNotification3());

        return null;

    }
}
