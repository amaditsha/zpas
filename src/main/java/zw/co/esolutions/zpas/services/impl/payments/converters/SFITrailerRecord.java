package zw.co.esolutions.zpas.services.impl.payments.converters;



/**
 * 
 * @author Gibson Mukarakate
 *
 */
public class SFITrailerRecord {
	private String trailerId;			// XxX : UTL
	private double totalDebitValue;		// 9 (24)
	private double totalCreditValue;	// 9 (24)
	private int debitItemsCount;		// 9 (6)	(excludes Header & Trailer records)
	private int creditItemsCount;		// 9 (6)	(excludes Header & Trailer records)
	private String currencyCode;        // XXX e.g USD
	private String checkSum;			// 9(6)
	private String checkSumTotal;        // 9(12)

	private String batchId;

	public static final int TRAILERID_FIELD_LENGTH = 3;
	public static final int TOTALDEBITVALUE_FIELD_LENGTH = 24;
	public static final int TOTALCREDITVALUE_FIELD_LENGTH = 24;
	public static final int DEBITITEMSCOUNT_FIELD_LENGTH = 6;
	public static final int CREDITITEMSCOUNT_FIELD_LENGTH = 6;
	public static final int CURRENCY_CODE_LENGTH=3;
	public static final int CHECKSUM_LENGTH=6;
	public static final int CHECKSUM_TOTAL_LENGTH=12;
	public static final String TRAILERID_DEFAULT = "UTL";

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public SFITrailerRecord() {
	}
	
	public SFITrailerRecord(String trailerId, double totalDebitValue, double totalCreditValue, int debitItemsCount, int creditItemsCount,String currencyCode) throws Exception {
		super();
		this.setTrailerId(trailerId);
		this.setTotalDebitValue(totalDebitValue);
		this.setTotalCreditValue(totalCreditValue);
		this.setDebitItemsCount(debitItemsCount);
		this.setCreditItemsCount(creditItemsCount);
		this.setCurrencyCode(currencyCode);
	}

	/**
	 * @return the creditItemsCount
	 */
	public int getCreditItemsCount() {
		return creditItemsCount;
	}
	/**
	 * @return the debitItemsCount
	 */
	public int getDebitItemsCount() {
		return debitItemsCount;
	}
	/**
	 * @return the totalCreditValue
	 */
	public double getTotalCreditValue() {
		return totalCreditValue;
	}
	/**
	 * @return the totalDebitValue
	 */
	public double getTotalDebitValue() {
		return totalDebitValue;
	}
	/**
	 * @return the trailerId
	 */
	public String getTrailerId() {
		return trailerId;
	}
	/**
	 * @param creditItemsCount the creditItemsCount to set
	 */
	public void setCreditItemsCount(int creditItemsCount) {
		this.creditItemsCount = creditItemsCount;
	}
	/**
	 * @param debitItemsCount the debitItemsCount to set
	 */
	public void setDebitItemsCount(int debitItemsCount) {
		this.debitItemsCount = debitItemsCount;
	}
	/**
	 * @param totalCreditValue the totalCreditValue to set
	 */
	public void setTotalCreditValue(double totalCreditValue) {
		this.totalCreditValue = totalCreditValue;
	}
	/**
	 * @param totalDebitValue the totalDebitValue to set
	 */
	public void setTotalDebitValue(double totalDebitValue) {
		this.totalDebitValue = totalDebitValue;
	}
	/**
	 * @param trailerId the trailerId to set
	 */
	public void setTrailerId(String trailerId) throws Exception {
		if (!SFITrailerRecord.TRAILERID_DEFAULT.equalsIgnoreCase(trailerId))
			throw new Exception("Invalid trailer ID, it must be '" + SFITrailerRecord.TRAILERID_DEFAULT + "'.");
		trailerId = trailerId.trim().toUpperCase();
		this.trailerId = trailerId;
	}
	
	
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	/**
	 * @return the checkSum
	 */
	public String getCheckSum() {
		return checkSum;
	}

	/**
	 * @param checkSum the checkSum to set
	 */
	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}

	/**
	 * @return the checkSumTotal
	 */
	public String getCheckSumTotal() {
		return checkSumTotal;
	}

	/**
	 * @param checkSumTotal the checkSumTotal to set
	 */
	public void setCheckSumTotal(String checkSumTotal) {
		this.checkSumTotal = checkSumTotal;
	}

	public String getSFICreditItemsCount() throws Exception {
		return BeanUtils.leftPad(""+this.getCreditItemsCount(), CREDITITEMSCOUNT_FIELD_LENGTH, '0');
	}
	public String getSFIDebitItemsCount() throws Exception {
		return BeanUtils.leftPad(""+this.getDebitItemsCount(), DEBITITEMSCOUNT_FIELD_LENGTH, '0');
	}
	public String getSFITotalCreditValue() throws Exception {
		return BeanUtils.leftPad(Formats.intFormat.format(this.getTotalCreditValue()), TOTALCREDITVALUE_FIELD_LENGTH, '0');
	}
	public String getSFITotalDebitValue() throws Exception {
		return BeanUtils.leftPad(Formats.intFormat.format(this.getTotalDebitValue()), TOTALDEBITVALUE_FIELD_LENGTH, '0');
	}
	public String getSFITrailerId() throws Exception {
		return BeanUtils.rightPad(this.getTrailerId(), TRAILERID_FIELD_LENGTH, ' ');
	}
	public String getSFICurrencyCode() throws Exception {
		return BeanUtils.rightPad(this.getCurrencyCode(), CURRENCY_CODE_LENGTH, ' ');
	}


	@Override
	public String toString() {
		return "SFITrailerRecord{" +
				"trailerId='" + trailerId + '\'' +
				", totalDebitValue=" + totalDebitValue +
				", totalCreditValue=" + totalCreditValue +
				", debitItemsCount=" + debitItemsCount +
				", creditItemsCount=" + creditItemsCount +
				", currencyCode='" + currencyCode + '\'' +
				", checkSum='" + checkSum + '\'' +
				", checkSumTotal='" + checkSumTotal + '\'' +
				", batchId='" + batchId + '\'' +
				'}';
	}
}
