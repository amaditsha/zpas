package zw.co.esolutions.zpas.services.impl.payments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.services.iface.process.WithdrawBulkPaymentService;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

@Service
public class WithdrawBulkPaymentsServiceImpl implements WithdrawBulkPaymentService {

    @Autowired
    private WithdrawBulkPaymentsProcessor withdrawBulkPaymentsProcessor;

    @Override
    public ServiceResponse withdrawBulkPayment() {
        return null;
    }
}
