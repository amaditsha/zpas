package zw.co.esolutions.zpas.services.impl.payments;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import zw.co.esolutions.zpas.codeset.CountryCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.integration.camel.processor.builder.CreditorPaymentActivationRequestStatusReportMessageBuilder;
import zw.co.esolutions.zpas.iso.msg.pain013_001_07.AddressType2Code;
import zw.co.esolutions.zpas.iso.msg.pain013_001_07.AddressType3Choice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.dto.payments.ActivatePaymentRequestDTO;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.iso.msg.pain013_001_07.*;
import zw.co.esolutions.zpas.iso.msg.pain013_001_07.Document;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.CreditorPaymentActivationRequestStatusReportV07;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.party.PartyService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentObligationsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentActivationInvalidRequestException;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.GroupStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alfred on 26 Apr 2019
 */
@Slf4j
@Service
@Transactional
public class PaymentObligationsServiceImpl implements PaymentObligationsService {
    static JAXBContext jaxbContextForPain013;
    static JAXBContext jaxbContextForPain014;

    static {
        try {
            jaxbContextForPain013 = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.pain013_001_07");
            jaxbContextForPain014 = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.pain014_001_07");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private MessageProducer messageProducer;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private AccountsService accountsService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private PaymentObligationsService paymentObligationsService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private PaymentObligationRepository paymentObligationRepository;

    @Autowired
    private PaymentObligationPartyRoleRepository paymentObligationPartyRoleRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Autowired
    private PaymentPartyRoleRepository paymentPartyRoleRepository;

    @Autowired
    private PaymentIdentificationRepository paymentIdentificationRepository;

    @Autowired
    private CreditorPaymentActivationRequestStatusReportMessageBuilder creditorPaymentActivationRequestStatusReportMessageBuilder;

    @Override
    public List<PaymentObligation> getPaymentObligationsByClientId(String clientId) {
        log.info("Getting all Payment Obligations for while client {} participates", clientId);
        final List<PaymentObligationPartyRole> clientRoles = paymentObligationPartyRoleRepository.findAllByPlayer_Id(clientId).stream()
                .collect(Collectors.toList());
        final Set<PaymentObligation> paymentSet = paymentObligationRepository.findAllByPartyRoleIn(clientRoles).stream().collect(Collectors.toSet());
        return paymentSet.stream().sorted(Comparator.comparing(PaymentObligation::getPaymentDueDate).reversed()).collect(Collectors.toList());
    }

    @Override
    public List<PaymentObligation> getPaymentObligationsForIds(List<String> ids) {
        if(ids == null) return new ArrayList<>();
        if(ids.size() == 0) return new ArrayList<>();
        return paymentObligationRepository.findAllByIdIn(ids);
    }

    @Override
    public List<PaymentObligation> getPaymentObligationsRaisedByClient(String clientId) {
        log.info("Getting Payment Obligations for which client {} is a Creditor", clientId);
        final List<PaymentObligationPartyRole> clientRoles = paymentObligationPartyRoleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(p -> p instanceof UltimateCreditorRole)
                .collect(Collectors.toList());
        final Set<PaymentObligation> paymentSet = paymentObligationRepository.findAllByPartyRoleIn(clientRoles).stream().collect(Collectors.toSet());
        return paymentSet.stream().sorted(Comparator.comparing(PaymentObligation::getPaymentDueDate).reversed()).collect(Collectors.toList());
    }

    @Override
    public List<PaymentObligation> getPaymentObligationsTargetingClient(String clientId) {
        log.info("Getting Payment Obligations for which client {} is a Debtor", clientId);
        final List<PaymentObligationPartyRole> clientRoles = paymentObligationPartyRoleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(p -> p instanceof UltimateDebtorRole)
                .collect(Collectors.toList());
        final Set<PaymentObligation> paymentSet = paymentObligationRepository.findAllByPartyRoleIn(clientRoles).stream().collect(Collectors.toSet());
        return paymentSet.stream().sorted(Comparator.comparing(PaymentObligation::getPaymentDueDate).reversed()).collect(Collectors.toList());
    }

    @Override
    public List<PaymentObligation> saveObligations(List<PaymentObligation> paymentObligations) {
        final List<PaymentObligationPartyRole> paymentObligationPartyRoles = paymentObligations.stream().flatMap(paymentObligation -> paymentObligation.getPartyRole().stream())
                .collect(Collectors.toList());
        paymentObligations.stream().forEach(paymentObligation -> {
            paymentObligation.setPartyRole(new ArrayList<>());
        });
        final List<Payment> paymentOffsets = paymentObligations.stream().flatMap(paymentObligation -> paymentObligation.getPaymentOffset().stream())
                .collect(Collectors.toList());
        paymentOffsets.forEach(payment -> payment.setPaymentObligation(new ArrayList<>()));
        log.info("Saving payment offsets.");
        savePayments(paymentOffsets);
        log.info("Saved payment offsets");
        log.info("Saving payment obligations.");
        final List<PaymentObligation> savedPaymentObligations = paymentObligationRepository.saveAll(paymentObligations);
        log.info("Saved payment obligations.");

        paymentObligationPartyRoles.forEach(role -> {
            List<RolePlayer> rolePlayers = new ArrayList<>();
            role.getPlayer().forEach(rolePlayer -> {
                final Optional<Party> partyById = partyService.getPartyById(rolePlayer.getId());
                partyById.ifPresent(party -> rolePlayers.add(party));
            });
            role.setPlayer(rolePlayers);
        });

        paymentObligationPartyRoleRepository.saveAll(paymentObligationPartyRoles);
        return savedPaymentObligations;
    }

    private void savePayments(List<Payment> payments) {
        final List<PaymentPartyRole> individualPaymentPartyRoles = payments.stream()
                .flatMap(individualPayment -> individualPayment.getPartyRole().stream())
                .collect(Collectors.toList());
        final List<PaymentIdentification> individualPaymentIdentifications = payments.stream()
                .flatMap(individualPayment -> individualPayment.getPaymentRelatedIdentifications().stream())
                .collect(Collectors.toList());
        payments.forEach(individualPayment -> {
            individualPayment.setPartyRole(new ArrayList<>());
            individualPayment.setPaymentRelatedIdentifications(new ArrayList<>());
        });

        paymentRepository.saveAll(payments);
        paymentPartyRoleRepository.saveAll(individualPaymentPartyRoles);
        paymentIdentificationRepository.saveAll(individualPaymentIdentifications);
    }

    @Override
    public Optional<PaymentObligation> getPaymentObligationsById(String paymentObligationId) {
        return paymentObligationRepository.findById(paymentObligationId);
    }

    @Override
    public Optional<PaymentObligation> activateCreditorPaymentRequest(ActivatePaymentRequestDTO activatePaymentRequestDTO) throws PaymentActivationInvalidRequestException {

        final CreditorPaymentActivationRequestV07 creditorPaymentActivationRequestV07 = getCreditorPaymentActivationRequestV07(activatePaymentRequestDTO);

        try {
            messageProducer.sendMessageToSystemClient(buildXml(creditorPaymentActivationRequestV07));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private String buildXml(CreditorPaymentActivationRequestV07 creditorPaymentActivationRequestV07) throws JAXBException {
        log.info("Building PAIN.013 XML message..");
        Document document = new Document();
        document.setCdtrPmtActvtnReq(creditorPaymentActivationRequestV07);

        Marshaller jaxbMarshaller = jaxbContextForPain013.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        return xml;
    }

    @Override
    public Optional<PaymentObligation> approveCreditorPaymentRequest(String obligationId, String clientId) {
        return paymentObligationRepository.findById(obligationId).map(paymentObligation -> {
            final List<RolePlayer> clientRolePlayers = paymentObligation.getPartyRole().stream().filter(role -> role instanceof UltimateCreditorRole)
                    .map(role -> (UltimateCreditorRole) role)
                    .flatMap(ultimateCreditorRole -> ultimateCreditorRole.getPlayer().stream())
                    .filter(rolePlayer -> rolePlayer.getId().equals(clientId))
                    .collect(Collectors.toList());
            if(clientRolePlayers.size() > 0) {
                log.warn("Client is the creditor, they cannot approve their own request");
                Optional<PaymentObligation> emptyOptional = Optional.empty();
                return emptyOptional;
            }
            paymentObligation.setEntityStatus(EntityStatus.ACTIVE);
            paymentObligation.getPaymentOffset().stream().forEach(payment -> {

                final PaymentStatus paymentStatus = new PaymentStatus();
                paymentStatus.setStatus(PaymentStatusCode.Pending);
                paymentStatus.setPayment(payment);
                paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.Authorised);
                paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                paymentStatus.setTransactionStatus(TransactionStatusCode.None);
                paymentStatus.setId(GenerateKey.generateEntityId());
                paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                paymentStatus.setDateCreated(OffsetDateTime.now());
                paymentStatus.setLastUpdated(OffsetDateTime.now());
                paymentStatus.setStatusDateTime(OffsetDateTime.now());
                paymentStatus.setStatusDescription("Initialising Payment Activation Request");
                paymentStatus.setInstructionProcessingStatus(StatusCode.Pending);

                final PaymentStatus savedPaymentStatus = paymentStatusRepository.save(paymentStatus);

                payment.getPaymentStatus().add(savedPaymentStatus);
                paymentsService.approvePayment(payment.getId());
            });
            final GroupHeader78 groupHeader78 = buildGroupHeader78(paymentObligation.getAmount().getAmount().doubleValue(), clientId);
            try {
                buildAndSendCreditorPaymentActivationRequestStatusReportV07(groupHeader78, GroupStatus.APPROVED, paymentObligation);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            return Optional.of(paymentObligation);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<PaymentObligation> rejectCreditorPaymentRequest(String obligationId, String clientId) {
        return paymentObligationRepository.findById(obligationId).map(paymentObligation -> {
            final List<RolePlayer> clientRolePlayers = paymentObligation.getPartyRole().stream().filter(role -> role instanceof UltimateCreditorRole)
                    .map(role -> (UltimateCreditorRole) role)
                    .flatMap(ultimateCreditorRole -> ultimateCreditorRole.getPlayer().stream())
                    .filter(rolePlayer -> rolePlayer.getId().equals(clientId))
                    .collect(Collectors.toList());
            if(clientRolePlayers.size() > 0) {
                log.warn("Client is the creditor, they cannot reject their own request");
                Optional<PaymentObligation> emptyOptional = Optional.empty();
                return emptyOptional;
            }
            paymentObligation.getPaymentOffset().stream().forEach(payment -> {
                final PaymentStatus paymentStatus = new PaymentStatus();
                paymentStatus.setStatus(PaymentStatusCode.Rejected);
                paymentStatus.setPayment(payment);
                paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.Rejected);
                paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                paymentStatus.setTransactionStatus(TransactionStatusCode.Cancelled);
                paymentStatus.setId(GenerateKey.generateEntityId());
                paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                paymentStatus.setDateCreated(OffsetDateTime.now());
                paymentStatus.setLastUpdated(OffsetDateTime.now());
                paymentStatus.setStatusDateTime(OffsetDateTime.now());
                paymentStatus.setStatusDescription("Rejected Activation Request");
                paymentStatus.setInstructionProcessingStatus(StatusCode.Rejected);

                final PaymentStatus savedPaymentStatus = paymentStatusRepository.save(paymentStatus);

                payment.getPaymentStatus().add(savedPaymentStatus);
                paymentsService.rejectPayment(payment.getId());
            });
            final GroupHeader78 groupHeader78 = buildGroupHeader78(paymentObligation.getAmount().getAmount().doubleValue(), clientId);
            try {
                buildAndSendCreditorPaymentActivationRequestStatusReportV07(groupHeader78, GroupStatus.REJECTED, paymentObligation);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            return Optional.of(paymentObligation);
        }).orElse(Optional.empty());
    }

    private void buildAndSendCreditorPaymentActivationRequestStatusReportV07(GroupHeader78 grpHdr, GroupStatus message, PaymentObligation paymentObligation) throws JAXBException {
        final CreditorPaymentActivationRequestStatusReportV07 creditorPaymentActivationRequestStatusReportV07 =
                creditorPaymentActivationRequestStatusReportMessageBuilder
                        .buildCreditorPaymentActivationRequestStatusReportMessage(grpHdr, message, paymentObligation);
        messageProducer.sendMessageToSystemClient(buildXml(creditorPaymentActivationRequestStatusReportV07));
    }

    private String buildXml(CreditorPaymentActivationRequestStatusReportV07 creditorPaymentActivationRequestStatusReportV07) throws JAXBException {
        log.info("Building PAIN.014 XML message..");
        zw.co.esolutions.zpas.iso.msg.pain014_001_07.Document document = new zw.co.esolutions.zpas.iso.msg.pain014_001_07.Document();
        document.setCdtrPmtActvtnReqStsRpt(creditorPaymentActivationRequestStatusReportV07);

        Marshaller jaxbMarshaller = jaxbContextForPain014.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);

        return xml;
    }

    private CreditorPaymentActivationRequestV07 getCreditorPaymentActivationRequestV07(final ActivatePaymentRequestDTO activatePaymentRequestDTO) throws PaymentActivationInvalidRequestException {
        log.info("Payment request: {}", activatePaymentRequestDTO);
        final CreditorPaymentActivationRequestV07 creditorPaymentActivationRequestV07 = new CreditorPaymentActivationRequestV07();
        final GroupHeader78 groupHeader78 = buildGroupHeader78(activatePaymentRequestDTO.getAmount(), activatePaymentRequestDTO.getClientId());

        final PartyIdentification135 initiatingPartyIdentification135 = getPartyIdentification135(activatePaymentRequestDTO.getClientId());

        creditorPaymentActivationRequestV07.setGrpHdr(groupHeader78);

        final PaymentInstruction31 paymentInstruction31 = new PaymentInstruction31();
        paymentInstruction31.setPmtInfId(GenerateKey.generateEntityId());
        paymentInstruction31.setPmtMtd(PaymentMethod7Code.TRF);
        final PaymentTypeInformation26 paymentTypeInformation26 = new PaymentTypeInformation26();
        paymentTypeInformation26.setInstrPrty(Priority2Code.NORM);
        final LocalInstrument2Choice localInstrument2Choice = new LocalInstrument2Choice();
        localInstrument2Choice.setCd("CSH");
        localInstrument2Choice.setPrtry("CSH");

        paymentTypeInformation26.setLclInstrm(localInstrument2Choice);
        final CategoryPurpose1Choice categoryPurpose1Choice = new CategoryPurpose1Choice();
        categoryPurpose1Choice.setCd("CSH");
        categoryPurpose1Choice.setPrtry("CSH");

        paymentTypeInformation26.setCtgyPurp(categoryPurpose1Choice);

        paymentInstruction31.setPmtTpInf(paymentTypeInformation26);
        final DateAndDateTime2Choice reqExctnDate = new DateAndDateTime2Choice();
        reqExctnDate.setDt(XmlDateUtil.getXmlDate(OffsetDateTime.now()));
        reqExctnDate.setDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        paymentInstruction31.setReqdExctnDt(reqExctnDate);
        final DateAndDateTime2Choice expiryDate = new DateAndDateTime2Choice();
        expiryDate.setDt(XmlDateUtil.getXmlDate(getOffsetDateTimeFromHtmlInput(activatePaymentRequestDTO.getDueDate())));
        expiryDate.setDtTm(XmlDateUtil.getXmlDate(getOffsetDateTimeFromHtmlInput(activatePaymentRequestDTO.getDueDate())));

        paymentInstruction31.setXpryDt(expiryDate);
        final PaymentCondition1 paymentCondition1 = new PaymentCondition1();
        paymentCondition1.setAmtModAllwd(false);
        paymentCondition1.setEarlyPmtAllwd(false);
        paymentCondition1.setDelyPnlty("0");
        final AmountOrRate1Choice amountOrRate1Choice = new AmountOrRate1Choice();
        final ActiveCurrencyAndAmount activeCurrencyAndAmount = new ActiveCurrencyAndAmount();
        activeCurrencyAndAmount.setValue(new BigDecimal("0"));
        activeCurrencyAndAmount.setCcy("ZWL");

        amountOrRate1Choice.setAmt(activeCurrencyAndAmount);
        amountOrRate1Choice.setRate(new BigDecimal("0"));

        paymentCondition1.setImdtPmtRbt(amountOrRate1Choice);
        paymentCondition1.setGrntedPmtReqd(false);

        paymentInstruction31.setPmtCond(paymentCondition1);
        final Optional<Party> debtorPartyOptional = partyService.getPartyById(activatePaymentRequestDTO.getDebtorId());
        final PartyIdentification135 debtor = new PartyIdentification135();
        debtor.setNm(debtorPartyOptional.map(Party::getName).orElse(""));
        final PostalAddress24 postalAddress241 = new PostalAddress24();

        final Party38Choice party38Choice1 = new Party38Choice();
        final PersonIdentification13 personIdentification131 = new PersonIdentification13();
        final DateAndPlaceOfBirth1 dateAndPlaceOfBirth11 = new DateAndPlaceOfBirth1();

        personIdentification131.setDtAndPlcOfBirth(dateAndPlaceOfBirth11);
        final GenericPersonIdentification1 genericPersonIdentification11 = new GenericPersonIdentification1();

        personIdentification131.getOthr().add(genericPersonIdentification11);

        party38Choice1.setPrvtId(personIdentification131);

        debtorPartyOptional.ifPresent(party -> {
            if(party instanceof Organisation) {
                Organisation organisation = (Organisation) party;
                final OffsetDateTime registrationDate = organisation.getRegistrationDate();
                dateAndPlaceOfBirth11.setBirthDt(XmlDateUtil.getXmlDate(registrationDate));
                organisation.getPlaceOfRegistration().getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    configurePostalAddress(postalAddress241, postalAddress);
                    debtor.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(""));
                });
            } else if(party instanceof Person) {
                Person person = (Person) party;
                person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                    genericPersonIdentification11.setId(personIdentification.getIdentityCardNumber());
                });
                final OffsetDateTime birthDate = person.getBirthDate();
                dateAndPlaceOfBirth11.setBirthDt(XmlDateUtil.getXmlDate(birthDate));
                person.getPlaceOfBirth().getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    dateAndPlaceOfBirth11.setPrvcOfBirth(postalAddress.getProvince());
                    dateAndPlaceOfBirth11.setCityOfBirth(postalAddress.getTownName());
                    dateAndPlaceOfBirth11.setCtryOfBirth(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(""));
                    genericPersonIdentification11.setIssr(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(""));
                    configurePostalAddress(postalAddress241, postalAddress);
                });
                person.getResidence().stream().findFirst().ifPresent(location -> {
                    location.getAddress().stream().findFirst().ifPresent(postalAddress -> {
                        debtor.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(""));
                    });
                });
            }
        });

        debtor.setPstlAdr(postalAddress241);
        debtor.setId(party38Choice1);
        paymentInstruction31.setDbtr(debtor);
        final CashAccount38 debtorAccount = new CashAccount38();

        final AccountIdentification4Choice accountIdentification4Choice = new AccountIdentification4Choice();
        final GenericAccountIdentification1 genericAccountIdentification1 = new GenericAccountIdentification1();
        final String sourceAccountNumber = activatePaymentRequestDTO.getSourceAccountNumber();
        log.info("Source account number: {}", sourceAccountNumber);
        final Optional<CashAccount> sourceCashAccountOptional = accountsService.findAccountByIban(sourceAccountNumber);
        sourceCashAccountOptional.map(cashAccount -> {
            accountIdentification4Choice.setIBAN(cashAccount.getIdentification().getNumber());
            genericAccountIdentification1.setId(cashAccount.getIdentification().getNumber());
            debtorAccount.setCcy(cashAccount.getBaseCurrency().getCodeName());
            debtorAccount.setNm(cashAccount.getIdentification().getName());
            return cashAccount;
        }).orElseThrow(() -> new PaymentActivationInvalidRequestException("Source/Debtor Account " + sourceAccountNumber + " does not exist."));

        final CashAccount sourceCashAccount = sourceCashAccountOptional.get();
        final FinancialInstitution sourceFinancialInstitution = sourceCashAccount.getFinancialInstitution();

        final BranchAndFinancialInstitutionIdentification6 debtorAgent = new BranchAndFinancialInstitutionIdentification6();
        final FinancialInstitutionIdentification18 financialInstitutionIdentification18 = new FinancialInstitutionIdentification18();
        final GenericFinancialIdentification1 genericFinancialIdentification1 = new GenericFinancialIdentification1();
        Optional.ofNullable(sourceFinancialInstitution).ifPresent(financialInstitution -> {
                    genericAccountIdentification1.setIssr(financialInstitution.getName());
                    financialInstitutionIdentification18.setBICFI(financialInstitution.getId());
                    financialInstitutionIdentification18.setLEI(financialInstitution.getId());
                    financialInstitutionIdentification18.setNm(financialInstitution.getName());
                    genericFinancialIdentification1.setId(financialInstitution.getId());
                    genericFinancialIdentification1.setIssr("");
                });

        accountIdentification4Choice.setOthr(genericAccountIdentification1);

        debtorAccount.setId(accountIdentification4Choice);
        final CashAccountType2Choice cashAccountType2Choice = new CashAccountType2Choice();
        cashAccountType2Choice.setCd("CD");
        cashAccountType2Choice.setPrtry("CD");

        debtorAccount.setTp(cashAccountType2Choice);

        paymentInstruction31.setDbtrAcct(debtorAccount);
        financialInstitutionIdentification18.setPstlAdr(new PostalAddress24());

        financialInstitutionIdentification18.setOthr(genericFinancialIdentification1);

        debtorAgent.setFinInstnId(financialInstitutionIdentification18);

        paymentInstruction31.setDbtrAgt(debtorAgent);
        paymentInstruction31.setUltmtDbtr(debtor);
        paymentInstruction31.setChrgBr(ChargeBearerType1Code.DEBT);

        final CreditTransferTransaction35 creditTransferTransaction35 = new CreditTransferTransaction35();
        final PaymentIdentification6 paymentIdentification6 = new PaymentIdentification6();
        paymentIdentification6.setInstrId(GenerateKey.generateEntityId());
        paymentIdentification6.setEndToEndId(GenerateKey.generateEntityId());
        paymentIdentification6.setUETR(GenerateKey.generateEntityId());

        creditTransferTransaction35.setPmtId(paymentIdentification6);
        final PaymentTypeInformation26 paymentTypeInformation261 = new PaymentTypeInformation26();
        paymentTypeInformation261.setInstrPrty(Priority2Code.NORM);
        final LocalInstrument2Choice localInstrument2Choice1 = new LocalInstrument2Choice();
        localInstrument2Choice1.setCd("CD");
        localInstrument2Choice1.setPrtry("CD");

        paymentTypeInformation261.setLclInstrm(localInstrument2Choice1);
        final CategoryPurpose1Choice categoryPurpose1Choice1 = new CategoryPurpose1Choice();
        categoryPurpose1Choice1.setCd("CSH");
        categoryPurpose1Choice1.setPrtry("CSH");

        paymentTypeInformation261.setCtgyPurp(categoryPurpose1Choice1);

        creditTransferTransaction35.setPmtTpInf(paymentTypeInformation261);
        final PaymentCondition1 paymentCondition11 = new PaymentCondition1();
        creditTransferTransaction35.setPmtCond(paymentCondition11);
        final AmountType4Choice amountType4Choice = new AmountType4Choice();
        final ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount = new ActiveOrHistoricCurrencyAndAmount();
        activeOrHistoricCurrencyAndAmount.setValue(new BigDecimal(activatePaymentRequestDTO.getAmount()));
        activeOrHistoricCurrencyAndAmount.setCcy(debtorAccount.getCcy());

        amountType4Choice.setInstdAmt(activeOrHistoricCurrencyAndAmount);
        final EquivalentAmount2 equivalentAmount2 = new EquivalentAmount2();
        final ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount1 = new ActiveOrHistoricCurrencyAndAmount();
        activeOrHistoricCurrencyAndAmount1.setValue(new BigDecimal(activatePaymentRequestDTO.getAmount()));
        activeOrHistoricCurrencyAndAmount1.setCcy(debtorAccount.getCcy());

        equivalentAmount2.setAmt(activeOrHistoricCurrencyAndAmount1);
        equivalentAmount2.setCcyOfTrf(debtorAccount.getCcy());

        amountType4Choice.setEqvtAmt(equivalentAmount2);

        creditTransferTransaction35.setAmt(amountType4Choice);
        creditTransferTransaction35.setChrgBr(ChargeBearerType1Code.DEBT);
        creditTransferTransaction35.setUltmtDbtr(debtor);
        creditTransferTransaction35.setIntrmyAgt1(debtorAgent);
        creditTransferTransaction35.setIntrmyAgt2(debtorAgent);
        creditTransferTransaction35.setIntrmyAgt3(debtorAgent);
        final BranchAndFinancialInstitutionIdentification6 creditorAgent = new BranchAndFinancialInstitutionIdentification6();
        final FinancialInstitutionIdentification18 financialInstitutionIdentification181 = new FinancialInstitutionIdentification18();
        final GenericFinancialIdentification1 genericFinancialIdentification11 = new GenericFinancialIdentification1();

        financialInstitutionIdentification181.setOthr(genericFinancialIdentification11);

        creditorAgent.setFinInstnId(financialInstitutionIdentification181);
        creditTransferTransaction35.setCdtrAgt(creditorAgent);
        creditTransferTransaction35.setCdtr(initiatingPartyIdentification135);
        final CashAccount38 creditorAccount = new CashAccount38();
        final AccountIdentification4Choice accountIdentification4Choice1 = new AccountIdentification4Choice();
        final CashAccountType2Choice cashAccountType2Choice1 = new CashAccountType2Choice();

        final GenericAccountIdentification1 genericAccountIdentification11 = new GenericAccountIdentification1();
        final Optional<CashAccount> destinationAccountOptional =
                Optional.ofNullable(accountsService.findAccountById(activatePaymentRequestDTO.getDestinationAccountId()));
        destinationAccountOptional.map(cashAccount -> {
            accountIdentification4Choice1.setIBAN(cashAccount.getIdentification().getNumber());
            genericAccountIdentification11.setId(cashAccount.getIdentification().getNumber());
            creditorAccount.setCcy(cashAccount.getBaseCurrency().getCodeName());
            creditorAccount.setNm(cashAccount.getIdentification().getName());
            cashAccountType2Choice1.setCd(cashAccount.getCashAccountType().getCodeName());
            cashAccountType2Choice1.setPrtry(cashAccount.getCashAccountType().getCodeName());
            creditorAccount.setCcy(cashAccount.getBaseCurrency().getCodeName());
            creditorAccount.setNm(cashAccount.getIdentification().getName());
            return cashAccount;
        }).orElseThrow(() -> new PaymentActivationInvalidRequestException("Destination/Creditor Account not found."));

        final CashAccount destinationCashAccount = destinationAccountOptional.get();

        if(sourceCashAccount.getId().equals(destinationCashAccount.getId())) {
            throw new PaymentActivationInvalidRequestException("Source and destination account cannot be the same.");
        }
        final FinancialInstitution destinationCashAccountFinancialInstitution = destinationCashAccount.getFinancialInstitution();

        Optional.ofNullable(destinationCashAccountFinancialInstitution).ifPresent(financialInstitution -> {
                    financialInstitutionIdentification181.setBICFI(financialInstitution.getId());
                    financialInstitutionIdentification181.setLEI(financialInstitution.getId());
                    financialInstitutionIdentification181.setNm(financialInstitution.getName());
                    genericFinancialIdentification11.setId(financialInstitution.getId());
                    genericFinancialIdentification11.setIssr("");
                });

        accountIdentification4Choice1.setOthr(genericAccountIdentification11);

        creditorAccount.setId(accountIdentification4Choice1);

        creditorAccount.setTp(cashAccountType2Choice1);

        creditTransferTransaction35.setCdtrAcct(creditorAccount);
        creditTransferTransaction35.setUltmtCdtr(initiatingPartyIdentification135);
        final Purpose2Choice purpose2Choice = new Purpose2Choice();
        purpose2Choice.setCd("OTHR");
        purpose2Choice.setPrtry("OTHR");

        creditTransferTransaction35.setPurp(purpose2Choice);

        paymentInstruction31.getCdtTrfTx().add(creditTransferTransaction35);

        creditorPaymentActivationRequestV07.getPmtInf().add(paymentInstruction31);

        final List<PaymentObligation> paymentObligations = createPaymentObligation(creditorPaymentActivationRequestV07);
        paymentObligations.stream().findFirst().ifPresent(paymentObligation -> {
            groupHeader78.setMsgId(paymentObligation.getId());
        });

        return creditorPaymentActivationRequestV07;
    }

    private GroupHeader78 buildGroupHeader78(final double amount, final String clientId) {
        final GroupHeader78 groupHeader78 = new GroupHeader78();
        groupHeader78.setMsgId(GenerateKey.generateReferenceNumber());
        groupHeader78.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));
        groupHeader78.setNbOfTxs("1");
        groupHeader78.setCtrlSum(new BigDecimal(amount));

        final PartyIdentification135 initiatingPartyIdentification135 = getPartyIdentification135(clientId);

        groupHeader78.setInitgPty(initiatingPartyIdentification135);
        return groupHeader78;
    }

    private PartyIdentification135 getPartyIdentification135(String clientId) {
        final PartyIdentification135 initiatingPartyIdentification135 = new PartyIdentification135();
        final Optional<Party> initiatingPartyOptional = partyService.getPartyById(clientId);
        initiatingPartyIdentification135.setNm(initiatingPartyOptional.map(Party::getName).orElse(""));
        final PostalAddress24 postalAddress24 = new PostalAddress24();

        initiatingPartyIdentification135.setPstlAdr(postalAddress24);
        final Party38Choice party38Choice = new Party38Choice();
        final PersonIdentification13 personIdentification13 = new PersonIdentification13();
        final DateAndPlaceOfBirth1 dateAndPlaceOfBirth1 = new DateAndPlaceOfBirth1();

        final Contact4 contact4 = new Contact4();
        contact4.setPrefrdMtd(PreferredContactMethod1Code.MAIL);

        final GenericPersonIdentification1 genericPersonIdentification1 = new GenericPersonIdentification1();
        initiatingPartyOptional.ifPresent(party -> {
            if(party instanceof Organisation) {
                Organisation organisation = (Organisation) party;
                contact4.setNm(party.getName());
                final OffsetDateTime registrationDate = organisation.getRegistrationDate();
                dateAndPlaceOfBirth1.setBirthDt(XmlDateUtil.getXmlDate(registrationDate));
                organisation.getPlaceOfRegistration().getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    configurePostalAddress(postalAddress24, postalAddress);
                    initiatingPartyIdentification135.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(""));
                });

                organisation.getContactPoint().stream().filter(c -> c instanceof PhoneAddress)
                        .map(c -> (PhoneAddress) c)
                        .findFirst().ifPresent(phoneAddress -> {
                    contact4.setPhneNb(phoneAddress.getPhoneNumber());
                    contact4.setMobNb(phoneAddress.getMobileNumber());
                    contact4.setFaxNb(phoneAddress.getFaxNumber());
                });
                organisation.getContactPoint().stream().filter(c -> c instanceof ElectronicAddress)
                        .map(c -> (ElectronicAddress) c)
                        .findFirst().ifPresent(electronicAddress -> {
                    contact4.setEmailAdr(electronicAddress.getEmailAddress());
                });
            } else if(party instanceof Person) {
                Person person = (Person) party;
                person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                    genericPersonIdentification1.setId(personIdentification.getIdentityCardNumber());
                });
                final OffsetDateTime birthDate = person.getBirthDate();
                dateAndPlaceOfBirth1.setBirthDt(XmlDateUtil.getXmlDate(birthDate));
                person.getPlaceOfBirth().getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    dateAndPlaceOfBirth1.setPrvcOfBirth(postalAddress.getProvince());
                    dateAndPlaceOfBirth1.setCityOfBirth(postalAddress.getTownName());
                    dateAndPlaceOfBirth1.setCtryOfBirth(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(""));
                    genericPersonIdentification1.setIssr(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(""));
                    configurePostalAddress(postalAddress24, postalAddress);
                });
                person.getResidence().stream().findFirst().ifPresent(location -> {
                    location.getAddress().stream().findFirst().ifPresent(postalAddress -> {
                        initiatingPartyIdentification135.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(""));
                    });
                });
                person.getContactPoint().stream().filter(c -> c instanceof PhoneAddress)
                        .map(c -> (PhoneAddress) c)
                        .findFirst().ifPresent(phoneAddress -> {
                    contact4.setPhneNb(phoneAddress.getPhoneNumber());
                    contact4.setMobNb(phoneAddress.getMobileNumber());
                    contact4.setFaxNb(phoneAddress.getFaxNumber());
                    contact4.setEmailPurp("Notifications");
                    contact4.setJobTitl("");
                    contact4.setRspnsblty("");
                    contact4.setDept("");
                });
                person.getContactPoint().stream().filter(c -> c instanceof ElectronicAddress)
                        .map(c -> (ElectronicAddress) c)
                        .findFirst().ifPresent(electronicAddress -> {
                    contact4.setEmailAdr(electronicAddress.getEmailAddress());
                });
            }
        });

        final PersonIdentificationSchemeName1Choice personIdentificationSchemeName1Choice = new PersonIdentificationSchemeName1Choice();
        personIdentificationSchemeName1Choice.setCd("NT");
        personIdentificationSchemeName1Choice.setPrtry("National-ID");

        genericPersonIdentification1.setSchmeNm(personIdentificationSchemeName1Choice);

        personIdentification13.getOthr().add(genericPersonIdentification1);

        personIdentification13.setDtAndPlcOfBirth(dateAndPlaceOfBirth1);

        party38Choice.setPrvtId(personIdentification13);

        initiatingPartyIdentification135.setId(party38Choice);

        initiatingPartyIdentification135.setCtctDtls(contact4);
        return initiatingPartyIdentification135;
    }

    private OffsetDateTime getOffsetDateTimeFromHtmlInput(String htmlDateInputString) {
        try {
            if(!StringUtils.isEmpty(htmlDateInputString)) {
                final LocalDate localDate = LocalDate.parse(htmlDateInputString, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                return LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
            } else {
                return OffsetDateTime.now();
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
            return OffsetDateTime.now();
        }
    }

    private void configurePostalAddress(PostalAddress24 postalAddress24, PostalAddress postalAddress) {
        final AddressType3Choice addressType3Choice = new AddressType3Choice();
        try {
            addressType3Choice.setCd(AddressType2Code.valueOf(postalAddress.getAddressType().name()));
        } catch (RuntimeException re) {
            log.info("Failed to parse address type.");
        }

        postalAddress24.setAdrTp(addressType3Choice);
        postalAddress24.setDept(postalAddress.getDepartment());
        postalAddress24.setSubDept(postalAddress.getSubDepartment());
        postalAddress24.setStrtNm(postalAddress.getStreetName());
        postalAddress24.setBldgNb(postalAddress.getBuildingIdentification());
        postalAddress24.setBldgNm(postalAddress.getBuildingName());
        postalAddress24.setFlr(postalAddress.getFloor());
        postalAddress24.setPstBx(postalAddress.getPostOfficeBox());
        postalAddress24.setRoom(postalAddress.getLot());
        postalAddress24.setPstCd(postalAddress.getPostCodeIdentification());
        postalAddress24.setTwnNm(postalAddress.getTownName());
        postalAddress24.setTwnLctnNm(postalAddress.getTownName());
        postalAddress24.setDstrctNm(postalAddress.getDistrictName());
        postalAddress24.setCtrySubDvsn(postalAddress.getCountyIdentification());
        postalAddress24.setCtry(postalAddress.getCountyIdentification());
    }

    private List<PaymentObligation> createPaymentObligation(final CreditorPaymentActivationRequestV07 creditorPaymentActivationRequestV07) {
        final GroupHeader78 grpHdr = creditorPaymentActivationRequestV07.getGrpHdr();
        final List<PaymentInstruction31> paymentInstruction31s = creditorPaymentActivationRequestV07.getPmtInf();

        BigDecimal ctrlSum = grpHdr.getCtrlSum();
        XMLGregorianCalendar creDtTm;
        PartyIdentification135 initgPty;
        String msgId = grpHdr.getMsgId();
        String nbOfTxs = grpHdr.getNbOfTxs();

        try {
            Preconditions.checkNotNull(grpHdr);
            Preconditions.checkNotNull(paymentInstruction31s);

            ctrlSum = grpHdr.getCtrlSum();
            creDtTm = grpHdr.getCreDtTm();
            initgPty = grpHdr.getInitgPty();
            msgId = grpHdr.getMsgId();
            nbOfTxs = grpHdr.getNbOfTxs();

            Preconditions.checkNotNull(initgPty, "Initiating party is missing.");
            Preconditions.checkNotNull(msgId, "Message ID is missing.");

            List<PaymentObligation> paymentObligations = new ArrayList<>();

            for (PaymentInstruction31 paymentInstruction31 : paymentInstruction31s) {
                final List<CreditTransferTransaction35> cdtTrfTxs = paymentInstruction31.getCdtTrfTx();
                final ChargeBearerType1Code chrgBr = paymentInstruction31.getChrgBr();
                final PartyIdentification135 dbtr = paymentInstruction31.getDbtr();
                final CashAccount38 dbtrAcct = paymentInstruction31.getDbtrAcct();
                final BranchAndFinancialInstitutionIdentification6 dbtrAgt = paymentInstruction31.getDbtrAgt();
                final PaymentCondition1 pmtCond = paymentInstruction31.getPmtCond();
                final String pmtInfId = paymentInstruction31.getPmtInfId();
                final PaymentMethod7Code pmtMtd = paymentInstruction31.getPmtMtd();
                final PaymentTypeInformation26 pmtTpInf = paymentInstruction31.getPmtTpInf();
                final DateAndDateTime2Choice reqdExctnDt = paymentInstruction31.getReqdExctnDt();
                final PartyIdentification135 ultmtDbtr = paymentInstruction31.getUltmtDbtr();
                final DateAndDateTime2Choice xpryDt = paymentInstruction31.getXpryDt();

                Preconditions.checkNotNull(pmtInfId, "Payment info ID is missing");
                Preconditions.checkNotNull(cdtTrfTxs, "Credit Transfer transaction is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(dbtr, "Debtor is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(dbtrAcct, "Debtor Account is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(dbtrAgt, "Debtor Agent is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(pmtCond, "Payment Condition is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(pmtMtd, "Payment method is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(pmtTpInf, "Payment Type Information is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(reqdExctnDt, "Requested Execution date is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(ultmtDbtr, "Ultimate Debtor is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(xpryDt, "Expiry Date is missing for Payment info ID \"" + pmtInfId + "\"");

                PaymentObligation paymentObligation = new PaymentObligation();
                paymentObligation.setId(RefGen.getReference("P"));
                paymentObligation.setDateCreated(OffsetDateTime.now());
                paymentObligation.setLastUpdated(OffsetDateTime.now());
                paymentObligation.setEntityStatus(EntityStatus.ACTIVE);
                paymentObligation.setEntityVersion(0L);
                paymentObligation.setPaymentOffset(Lists.newArrayList());
                paymentObligation.setRemittanceDeliveryMethod(RemittanceLocationMethodCode.UniformResourceIdentifier);
                paymentObligation.setRemittanceLocation(Lists.newArrayList());
                paymentObligation.setAssociatedDocument(Lists.newArrayList());
                paymentObligation.setPercentage(new BigDecimal("0"));
                paymentObligation.setExpiryDate(XmlDateUtil.getOffsetDateTime(xpryDt.getDtTm()));
                paymentObligation.setApplicableLaw(new CountryCode());

                final List<PaymentObligationPartyRole> paymentObligationPartyRoles = new ArrayList<>();
                Optional<RolePlayer> debtorRolePlayerOptional = Optional.empty();
                Optional<RolePlayer> creditorRolePlayerOptional = Optional.empty();

                UltimateDebtorRole ultimateDebtorRole = new UltimateDebtorRole();
                ultimateDebtorRole.setId(GenerateKey.generateEntityId());
                ultimateDebtorRole.setEntityStatus(EntityStatus.ACTIVE);
                ultimateDebtorRole.setDateCreated(OffsetDateTime.now());
                ultimateDebtorRole.setLastUpdated(OffsetDateTime.now());
                ultimateDebtorRole.setPaymentObligation(Arrays.asList(paymentObligation));
                ultimateDebtorRole.setPlayer(new ArrayList<>());
                ultimateDebtorRole.setHasCustomParty(false);

                Preconditions.checkNotNull(ultmtDbtr.getId(), "Id for Ultimate Debtor is missing");
                if (ultmtDbtr.getId().getPrvtId() != null) {
                    log.info("Private ID present, fetching person.");
                    debtorRolePlayerOptional = setAndGetPersonRolePlayer(ultmtDbtr.getId().getPrvtId().getOthr(), ultimateDebtorRole);
                } else if (ultmtDbtr.getId().getOrgId() != null) {
                    log.info("Organisation ID present, fetching organisation.");
                    debtorRolePlayerOptional = setAndGetOrganisationRolePlayer(ultmtDbtr.getId().getOrgId().getOthr(), ultimateDebtorRole);
                } else {
                    throw new RuntimeException("Failed to get Ultimate Creditor from the message");
                }

                UltimateCreditorRole ultimateCreditorRole = new UltimateCreditorRole();
                ultimateCreditorRole.setId(GenerateKey.generateEntityId());
                ultimateCreditorRole.setEntityStatus(EntityStatus.ACTIVE);
                ultimateCreditorRole.setDateCreated(OffsetDateTime.now());
                ultimateCreditorRole.setLastUpdated(OffsetDateTime.now());
                ultimateCreditorRole.setPaymentObligation(Arrays.asList(paymentObligation));
                ultimateCreditorRole.setPlayer(new ArrayList());
                ultimateCreditorRole.setHasCustomParty(false);

                Preconditions.checkNotNull(initgPty.getId(), "Id for Ultimate Creditor is missing");
                if (initgPty.getId().getPrvtId() != null) {
                    log.info("Private ID for Ultimate Creditor present, fetching person.");
                    creditorRolePlayerOptional = setAndGetPersonRolePlayer(initgPty.getId().getPrvtId().getOthr(), ultimateCreditorRole);
                } else if (initgPty.getId().getOrgId() != null) {
                    log.info("Organisation ID for Ultimate Creditor present, fetching person.");
                    creditorRolePlayerOptional = setAndGetOrganisationRolePlayer(initgPty.getId().getOrgId().getOthr(), ultimateCreditorRole);
                } else {
                    throw new RuntimeException("Failed to get the Ultimate Creditor from the message");
                }

                paymentObligationPartyRoles.add(ultimateDebtorRole);
                paymentObligationPartyRoles.add(ultimateCreditorRole);
                paymentObligation.setPartyRole(paymentObligationPartyRoles);
                paymentObligation.setPaymentDueDate(XmlDateUtil.getOffsetDateTime(xpryDt.getDtTm()));

                final List<Payment> creditTransfers = new ArrayList<>();

                for (CreditTransferTransaction35 cdtTrfTx35 : cdtTrfTxs) {
                    final AmountType4Choice amt = cdtTrfTx35.getAmt();
                    final PartyIdentification135 cdtr = cdtTrfTx35.getCdtr();
                    final CashAccount38 cdtrAcct = cdtTrfTx35.getCdtrAcct();
                    final BranchAndFinancialInstitutionIdentification6 cdtrAgt = cdtTrfTx35.getCdtrAgt();
                    final Cheque11 chqInstr = cdtTrfTx35.getChqInstr();
                    final ChargeBearerType1Code cdtTrfTx35ChrgBr = cdtTrfTx35.getChrgBr();
                    final List<InstructionForCreditorAgent1> instrForCdtrAgt = cdtTrfTx35.getInstrForCdtrAgt();
                    final BranchAndFinancialInstitutionIdentification6 intrmyAgt1 = cdtTrfTx35.getIntrmyAgt1();
                    final BranchAndFinancialInstitutionIdentification6 intrmyAgt2 = cdtTrfTx35.getIntrmyAgt2();
                    final BranchAndFinancialInstitutionIdentification6 intrmyAgt3 = cdtTrfTx35.getIntrmyAgt3();
                    final List<Document12> nclsdFile = cdtTrfTx35.getNclsdFile();
                    final PaymentCondition1 cdtTrfTx35PmtCond = cdtTrfTx35.getPmtCond();
                    final PaymentIdentification6 pmtId = cdtTrfTx35.getPmtId();
                    final PaymentTypeInformation26 cdtTrfTx35PmtTpInf = cdtTrfTx35.getPmtTpInf();
                    final Purpose2Choice purp = cdtTrfTx35.getPurp();
                    final List<RegulatoryReporting3> rgltryRptg = cdtTrfTx35.getRgltryRptg();
                    final List<RemittanceLocation7> rltdRmtInf = cdtTrfTx35.getRltdRmtInf();
                    final RemittanceInformation16 rmtInf = cdtTrfTx35.getRmtInf();
                    final List<SupplementaryData1> splmtryData = cdtTrfTx35.getSplmtryData();
                    final TaxInformation8 tax = cdtTrfTx35.getTax();
                    final PartyIdentification135 ultmtCdtr = cdtTrfTx35.getUltmtCdtr();
                    final PartyIdentification135 cdtTrfTx35UltmtDbtr = cdtTrfTx35.getUltmtDbtr();

                    final CreditTransfer creditTransfer = new CreditTransfer();
                    creditTransfer.setId(GenerateKey.generateEntityId());
                    creditTransfer.setDateCreated(OffsetDateTime.now());
                    creditTransfer.setLastUpdated(OffsetDateTime.now());
                    creditTransfer.setEntityStatus(EntityStatus.PENDING_APPROVAL);
                    creditTransfer.setPaymentObligation(new ArrayList<>());

                    creditTransfer.setCreditMethod(Lists.newArrayList());
                    creditTransfer.setType(PaymentTypeCode.valueOfCodeName(cdtTrfTx35PmtTpInf.getCtgyPurp().getCd()));
                    creditTransfer.setPriority(PriorityCode.Normal);
                    creditTransfer.setValueDate(XmlDateUtil.getOffsetDateTime(creDtTm));
                    List<PaymentStatus> paymentStatuses = new ArrayList<>();
                    final PaymentStatus paymentStatus = new PaymentStatus();
                    paymentStatus.setStatus(PaymentStatusCode.Pending);
                    paymentStatus.setPayment(creditTransfer);
                    paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                    paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                    paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                    paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.None);
                    paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                    paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                    paymentStatus.setTransactionStatus(TransactionStatusCode.None);
                    paymentStatus.setId(GenerateKey.generateEntityId());
                    paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                    paymentStatus.setDateCreated(OffsetDateTime.now());
                    paymentStatus.setLastUpdated(OffsetDateTime.now());
                    paymentStatus.setStatusDateTime(OffsetDateTime.now());
                    paymentStatus.setStatusDescription("Initialising Payment Activation Request");
                    paymentStatus.setInstructionProcessingStatus(StatusCode.Pending);

                    paymentStatuses.add(paymentStatus);
                    creditTransfer.setPaymentStatus(paymentStatuses);
                    creditTransfer.setInstructionForCreditorAgent(InstructionCode.PhoneNextAgent);
                    creditTransfer.setInstructionForDebtorAgent(InstructionCode.PayTheBeneficiary);
                    creditTransfer.setPartyRole(new ArrayList<>());
                    creditTransfer.setPaymentRelatedIdentifications(new ArrayList<>());


                    final CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();
                    final BigDecimal instructedAmountBigDecimal = Optional.ofNullable(amt.getInstdAmt())
                            .map(ActiveOrHistoricCurrencyAndAmount::getValue)
                            .orElse(new BigDecimal(0));
                    final String instructedAmountCcy = Optional.ofNullable(amt.getInstdAmt())
                            .map(ActiveOrHistoricCurrencyAndAmount::getCcy)
                            .orElse("");

                    currencyAndAmount.setAmount(instructedAmountBigDecimal);
                    final zw.co.esolutions.zpas.codeset.CurrencyCode currencyCode = new zw.co.esolutions.zpas.codeset.CurrencyCode();
                    currencyCode.setCodeName(instructedAmountCcy);
                    currencyCode.setName(instructedAmountCcy);
                    currencyAndAmount.setCurrency(currencyCode);
                    creditTransfer.setInstructedAmount(currencyAndAmount);
                    creditTransfer.setAmount(currencyAndAmount);
                    creditTransfer.setCurrencyOfTransfer(currencyCode);
                    creditTransfer.setEquivalentAmount(instructedAmountBigDecimal);

                    creditTransfer.setStandardSettlementInstructions("");
                    creditTransfer.setPaymentInstrument(PaymentInstrumentCode.CustomerCreditTransfer);

                    accountsService.findAccountByIban(cdtrAcct.getId().getOthr().getId()).ifPresent(cashAccount -> {
                        creditTransfer.setAccount(cashAccount);
                    });
                    creditTransfer.setPurpose(ProprietaryPurposeCode.valueOfCodeName(purp.getCd()));
                    creditTransfer.getPaymentObligation().add(paymentObligation);

                    final String bicfi = cdtTrfTx35.getCdtrAgt().getFinInstnId().getBICFI();
                    final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionByAnyBIC(bicfi);
                    final Optional<CashAccount> cashAccountOptional = accountsService.findAccountByIban(dbtrAcct.getId().getOthr().getId());
                    addDebtorPaymentRoles(creditTransfer, cashAccountOptional.orElse(null));
                    addCreditorPaymentPartyRoles(financialInstitutionOptional, creditorRolePlayerOptional.orElse(null), creditTransfer);

                    creditTransfers.add(creditTransfer);
                }

                final CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();

                currencyAndAmount.setAmount(creditTransfers.stream()
                        .map(c -> c.getAmount().getAmount()).reduce(BigDecimal::add).orElse(new BigDecimal(0)));
                final zw.co.esolutions.zpas.codeset.CurrencyCode currencyCode = new zw.co.esolutions.zpas.codeset.CurrencyCode();
                currencyCode.setCodeName("");
                currencyCode.setName("");
                currencyAndAmount.setCurrency(currencyCode);
                paymentObligation.setPaymentOffset(creditTransfers);
                paymentObligation.setAmount(currencyAndAmount);
                paymentObligation.setMaximumAmount(currencyAndAmount);
                paymentObligations.add(paymentObligation);
            }

            final List<PaymentObligation> savedPaymentObligations = paymentObligationsService.saveObligations(paymentObligations);
            return savedPaymentObligations;
        } catch (RuntimeException re) {
            log.error("An error occurred. {}", re.getMessage());
            re.printStackTrace();
            return new ArrayList<>();
        }
    }

    private Optional<RolePlayer> setAndGetOrganisationRolePlayer(List<GenericOrganisationIdentification1> othr, PaymentObligationPartyRole paymentObligationPartyRole) {
        return Optional.ofNullable(othr).map(genericOrganisationIdentification1s -> {
            return genericOrganisationIdentification1s.stream().findFirst().map(genericOrganisationIdentification1 -> {
                final String organisationId = genericOrganisationIdentification1.getId();
                return partyService.getPartyById(organisationId).map(party -> {
                    paymentObligationPartyRole.getPlayer().add(party);
                    return Optional.ofNullable((RolePlayer) party);
                }).orElse(Optional.empty());
            }).orElse(Optional.empty());
        }).orElse(Optional.empty());
    }

    private Optional<RolePlayer> setAndGetPersonRolePlayer(List<GenericPersonIdentification1> othr, PaymentObligationPartyRole paymentObligationPartyRole) {
        return Optional.ofNullable(othr).map(genericPersonIdentification1s -> {
            return genericPersonIdentification1s.stream().findFirst().map(genericPersonIdentification1 -> {
                final String personId = genericPersonIdentification1.getId();
                return clientService.findPersonIdentificationByIdNumber(personId).map(personIdentification -> {
                    final Person person = personIdentification.getPerson();
                    paymentObligationPartyRole.getPlayer().add(person);
                    return Optional.ofNullable((RolePlayer) person);
                }).orElse(Optional.empty());
            }).orElse(Optional.empty());
        }).orElse(Optional.empty());
    }

    private void addDebtorPaymentRoles(final IndividualPayment payment, CashAccount cashAccount) {
        log.info("Payment source account is: {}", cashAccount);

        /**
         * Configure Bulk Payment Roles
         */
        //The affected Bulk Payments
        //The affected Bulk Payment accounts
        final List<CashAccount> cashAccounts = Arrays.asList(cashAccount);

        final List<AccountPartyRole> accountPartyRoles = cashAccount.getPartyRole();

        //account servicing roles
        final List<AccountPartyRole> accountServicerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountServicerRole);
        }).collect(Collectors.toList());

        //The Role Players servicing this payment
        final List<RolePlayer> accountServicerRolePlayers = accountServicerRoles.stream()
                .flatMap(accountServicerRole -> accountServicerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //account owning roles
        final List<AccountPartyRole> accountOwnerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountOwnerRole);
        }).collect(Collectors.toList());

        //The Role Players owning this payment
        final List<RolePlayer> accountOwnerRolePlayers = accountOwnerRoles.stream()
                .flatMap(accountOwnerRole -> accountOwnerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //Debtor Role
        DebtorRole debtorRole = new DebtorRole();
        debtorRole.setPayment(Arrays.asList(payment));
        debtorRole.setCashAccount(cashAccounts);
        debtorRole.setId(GenerateKey.generateEntityId());
        debtorRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorRole.setDateCreated(OffsetDateTime.now());
        debtorRole.setLastUpdated(OffsetDateTime.now());
        debtorRole.setEntityVersion(0L);
        debtorRole.setPlayer(accountOwnerRolePlayers);
        debtorRole.setEntry(null);
        debtorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        //Debtor Agent Role
        DebtorAgentRole debtorAgentRole = new DebtorAgentRole();
        debtorAgentRole.setPayment(Arrays.asList(payment));
        debtorAgentRole.setCashAccount(cashAccounts);
        debtorAgentRole.setId(GenerateKey.generateEntityId());
        debtorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorAgentRole.setDateCreated(OffsetDateTime.now());
        debtorAgentRole.setLastUpdated(OffsetDateTime.now());
        debtorAgentRole.setEntityVersion(0L);
        debtorAgentRole.setPlayer(accountServicerRolePlayers);
        debtorAgentRole.setEntry(null);
        debtorAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Initiating Party Role
        InitiatingPartyRole initiatingPartyRole = new InitiatingPartyRole();
        initiatingPartyRole.setPayment(Arrays.asList(payment));
        initiatingPartyRole.setCashAccount(cashAccounts);
        initiatingPartyRole.setId(GenerateKey.generateEntityId());
        initiatingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
        initiatingPartyRole.setDateCreated(OffsetDateTime.now());
        initiatingPartyRole.setLastUpdated(OffsetDateTime.now());
        initiatingPartyRole.setEntityVersion(0L);
        initiatingPartyRole.setPlayer(accountOwnerRolePlayers);
        initiatingPartyRole.setEntry(null);
        initiatingPartyRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Forwarding Party Role
        ForwardingAgentRole forwardingAgentRole = new ForwardingAgentRole();
        forwardingAgentRole.setPayment(Arrays.asList(payment));
        forwardingAgentRole.setCashAccount(cashAccounts);
        forwardingAgentRole.setId(GenerateKey.generateEntityId());
        forwardingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        forwardingAgentRole.setDateCreated(OffsetDateTime.now());
        forwardingAgentRole.setLastUpdated(OffsetDateTime.now());
        forwardingAgentRole.setEntityVersion(0L);
        forwardingAgentRole.setPlayer(accountServicerRolePlayers);
        forwardingAgentRole.setEntry(null);
        forwardingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Instructing Agent Role
        InstructingAgentRole instructingAgentRole = new InstructingAgentRole();
        instructingAgentRole.setPayment(Arrays.asList(payment));
        instructingAgentRole.setCashAccount(cashAccounts);
        instructingAgentRole.setId(GenerateKey.generateEntityId());
        instructingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        instructingAgentRole.setDateCreated(OffsetDateTime.now());
        instructingAgentRole.setLastUpdated(OffsetDateTime.now());
        instructingAgentRole.setEntityVersion(0L);
        instructingAgentRole.setPlayer(accountServicerRolePlayers);
        instructingAgentRole.setEntry(null);
        instructingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);


        List<PaymentPartyRole> paymentPartyRoles = Arrays.asList(
                debtorRole, debtorAgentRole, initiatingPartyRole, forwardingAgentRole, instructingAgentRole
        );
        payment.getPartyRole().addAll(paymentPartyRoles);
    }

    private void addCreditorPaymentPartyRoles(final Optional<FinancialInstitution> financialInstitutionOptional, final RolePlayer client, Payment payment) {

        //Creditor Role
        CreditorRole creditorRole = new CreditorRole();
        creditorRole.setPayment(Arrays.asList(payment));
        creditorRole.setHasCustomParty(false);
        creditorRole.setId(GenerateKey.generateEntityId());
        creditorRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorRole.setDateCreated(OffsetDateTime.now());
        creditorRole.setLastUpdated(OffsetDateTime.now());
        creditorRole.setEntityVersion(0L);
        List<RolePlayer> clientRolePlayers = new ArrayList<>();
        clientRolePlayers.add(client);
        creditorRole.setPlayer(clientRolePlayers);
        creditorRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Creditor Agent Role
        CreditorAgentRole creditorAgentRole = new CreditorAgentRole();
        creditorAgentRole.setPayment(Arrays.asList(payment));
        financialInstitutionOptional.ifPresent(financialInstitution -> {
            List<RolePlayer> rolePlayers = new ArrayList<>();
            rolePlayers.add(financialInstitution);
            creditorAgentRole.setPlayer(rolePlayers);
        });
        creditorAgentRole.setId(GenerateKey.generateEntityId());
        creditorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorAgentRole.setDateCreated(OffsetDateTime.now());
        creditorAgentRole.setLastUpdated(OffsetDateTime.now());
        creditorAgentRole.setEntityVersion(0L);
        creditorAgentRole.setEntry(null);
        creditorAgentRole.setPartyRoleCode(PartyRoleCode.FinalAgent);

        List<PaymentPartyRole> individualPaymentPartyRoles = new ArrayList<>();
        individualPaymentPartyRoles.addAll(Arrays.asList(creditorRole, creditorAgentRole));
        payment.getPartyRole().addAll(individualPaymentPartyRoles);
    }

}
