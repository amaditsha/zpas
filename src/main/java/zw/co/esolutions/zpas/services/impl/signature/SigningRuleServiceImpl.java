package zw.co.esolutions.zpas.services.impl.signature;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.agreement.SigningRuleDTO;
import zw.co.esolutions.zpas.model.SigningRule;
import zw.co.esolutions.zpas.repository.SigningRuleRepository;
import zw.co.esolutions.zpas.services.iface.signature.SigningRuleService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SigningRuleServiceImpl implements SigningRuleService {

    @Autowired
    private SigningRuleRepository signingRuleRepository;

    @Override
    public SigningRule createSigningRule(SigningRuleDTO signingRuleDTO) {
        log.info("Saving the signing rule ...");
        SigningRule signingRule = new SigningRule();
        signingRule.setActivityName(signingRuleDTO.getActivityName());
        signingRule.setEntityType(signingRuleDTO.getEntityType());
        signingRule.setMaxAmount(MoneyUtil.convertToCents(getDoubleFromString(signingRuleDTO.getMaxAmount())));
        signingRule.setMinAmount(MoneyUtil.convertToCents(getDoubleFromString(signingRuleDTO.getMinAmount())));
        signingRule.setNumberOfApprovers(Integer.valueOf(signingRuleDTO.getNumberOfApprovers()));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        LocalDate startDateLocalDate = LocalDate.parse(signingRuleDTO.getStartDate(), formatter);
        OffsetDateTime startDate = startDateLocalDate.atTime(OffsetTime.of(0, 0, 0, 0, OffsetDateTime.now().getOffset()));

        signingRule.setStartDate(startDate);

        LocalDate endDateLocalDate = LocalDate.parse(signingRuleDTO.getEndDate(), formatter);
        OffsetDateTime endDate = endDateLocalDate.atTime(OffsetTime.of(23, 59, 59, 0, OffsetDateTime.now().getOffset()));

        signingRule.setEndDate(endDate);

        log.info("Persisting the signing rule | {}", signingRule);


        return signingRuleRepository.save(signingRule);
    }

    private Double getDoubleFromString(String stringAmount) {
        try {
            return Double.valueOf(stringAmount);
        }catch (Exception e) {
            e.printStackTrace();
            return 0.0;
        }
    }

    @Override
    public SigningRule updateSigningRule(SigningRuleDTO signingRuleDTO) {

        log.info("Editing the signing rule ...");
        Optional<SigningRule> optionalSigningRule = signingRuleRepository.findById(signingRuleDTO.getId());
        return optionalSigningRule.map(signingRule -> {
            signingRule.setId(signingRuleDTO.getActivityName());
            signingRule.setActivityName(signingRuleDTO.getActivityName());
            signingRule.setEntityType(signingRuleDTO.getEntityType());
            signingRule.setMaxAmount(MoneyUtil.convertToCents(getDoubleFromString(signingRuleDTO.getMaxAmount())));
            signingRule.setMinAmount(MoneyUtil.convertToCents(getDoubleFromString(signingRuleDTO.getMinAmount())));
            signingRule.setNumberOfApprovers(Integer.valueOf(signingRuleDTO.getNumberOfApprovers()));

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            LocalDate startDateLocalDate = LocalDate.parse(signingRuleDTO.getStartDate(), formatter);
            OffsetDateTime startDate = startDateLocalDate.atTime(OffsetTime.of(0, 0, 0, 0, OffsetDateTime.now().getOffset()));

            signingRule.setStartDate(startDate);

            LocalDate endDateLocalDate = LocalDate.parse(signingRuleDTO.getEndDate(), formatter);
            OffsetDateTime endDate = endDateLocalDate.atTime(OffsetTime.of(23, 59, 59, 0, OffsetDateTime.now().getOffset()));

            signingRule.setEndDate(endDate);

            log.info("Persisting the signing rule | {}", signingRule);
            return signingRuleRepository.save(signingRule);
        }).orElseGet(() -> {
            log.error("Failed to get the signing rule to update");
            return new SigningRule();
        });
    }

    @Override
    public SigningRule deleteSigningRule(String id) {
        Optional<SigningRule> optionalSigningRule = signingRuleRepository.findById(id);
        SigningRule signingRule = null;
        if (optionalSigningRule.isPresent()) {
            signingRule = optionalSigningRule.get();
            signingRule.setEntityStatus(EntityStatus.DELETED);
            signingRule = signingRuleRepository.save(signingRule);
        }
        return signingRule;
    }

    @Override
    public SigningRule approveSigningRule(String id) {
        Optional<SigningRule> optionalSigningRule = signingRuleRepository.findById(id);
        SigningRule signingRule = null;
        if (optionalSigningRule.isPresent()) {
            signingRule = optionalSigningRule.get();
            if(signingRule.getEntityStatus() != EntityStatus.DELETED) {
                signingRule.setEntityStatus(EntityStatus.ACTIVE);
                signingRule = signingRuleRepository.save(signingRule);
            } else {
                log.info("The signing rule status has already been deleted.");
            }
        }
        return signingRule;
    }

    @Override
    public SigningRule rejectSigningRule(String id) {
        Optional<SigningRule> optionalSigningRule = signingRuleRepository.findById(id);
        SigningRule signingRule = null;
        if (optionalSigningRule.isPresent()) {
            signingRule = optionalSigningRule.get();
            if(signingRule.getEntityStatus() != EntityStatus.DELETED) {
            signingRule.setEntityStatus(EntityStatus.DISAPPROVED);
            signingRule = signingRuleRepository.save(signingRule);
            } else {
                log.info("The signing rule status has already been deleted.");
            }
        }
        return signingRule;
    }

    @Override
    public Optional<SigningRule> getSigningRuleById(String signatureId) {
        return signingRuleRepository.findById(signatureId);
    }

    @Override
    public List<SigningRule> getSigningRules() {
        return signingRuleRepository.findAll();
    }
}
