package zw.co.esolutions.zpas.services.iface.merchant;

import zw.co.esolutions.zpas.dto.merchant.PartyMerchantAccountDTO;
import zw.co.esolutions.zpas.model.PartyMerchantAccount;

import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
public interface PartyMerchantAccountService {
    PartyMerchantAccount createMerchant(PartyMerchantAccountDTO merchant);
    PartyMerchantAccount updatePartyMerchantAccount(PartyMerchantAccountDTO merchant);
    PartyMerchantAccount deletePartyMerchantAccount(String id);
    PartyMerchantAccount approvePartyMerchantAccount(String id);
    PartyMerchantAccount rejectPartyMerchantAccount(String id);
    Optional<PartyMerchantAccount> getPartyMerchantAccount(String id);
    List<PartyMerchantAccount> getPartyMerchantAccounts();
    List<PartyMerchantAccount> getPartyMerchantAccountsByClientId(String clientId);
}
