package zw.co.esolutions.zpas.services.impl.process.builder;
import zw.co.esolutions.zpas.iso.msg.camt060_001_04.AccountSchemeName1Choice;
import zw.co.esolutions.zpas.iso.msg.camt060_001_04.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.AccountOwnerRole;
import zw.co.esolutions.zpas.model.AccountServicerRole;
import zw.co.esolutions.zpas.model.CashAccount;
import zw.co.esolutions.zpas.services.impl.process.util.AccountPartyRoleUtil;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class AccountReportingRequestMessageBuilder {

    @Autowired
    AccountPartyRoleUtil accountPartyRoleUtil;

    public AccountReportingRequestV04 buildAccountReportingRequestMsg(List<CashAccount> cashAccounts, String reqdMsgNameId){
        log.info("Building camt.060 message..");

        AccountReportingRequestV04 accountReportingRequestV04 = new AccountReportingRequestV04();

        CashAccount cashAccount = new CashAccount();
        Optional<CashAccount> accountOptional = cashAccounts.stream().findFirst();
        if(accountOptional.isPresent()){
            cashAccount = accountOptional.get();
        }


        /**
         * Setting the group header
         */
        GroupHeader76 groupHeader76 = new GroupHeader76();

        Party35Choice msgSender = new Party35Choice();
        msgSender.setPty(accountPartyRoleUtil.getPartyIdentification125(cashAccount, AccountOwnerRole.class));
        //msgSender.setAgt(accountPartyRoleUtil.getBranchAndFinancialInstitutionIdentification6(cashAccount, AccountServicerRole.class));

        groupHeader76.setMsgId(RefGen.getReference("M"));
        groupHeader76.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));
        groupHeader76.setMsgSndr(msgSender);

        /**
         * Setting the reporting request
         */
        List<ReportingRequest4> reportingRequest4List = new ArrayList<>();

        for (CashAccount account : cashAccounts){
            ReportingRequest4 reportingRequest4 = new ReportingRequest4();

            Party35Choice accOwner= new Party35Choice();
            accOwner.setPty(accountPartyRoleUtil.getPartyIdentification125(account, AccountOwnerRole.class));
            //accOwner.setAgt(accountPartyRoleUtil.getBranchAndFinancialInstitutionIdentification5(account, AccountServicerRole.class));

            reportingRequest4.setId(RefGen.getReference("RQ"));
            reportingRequest4.setReqdMsgNmId(reqdMsgNameId);
            reportingRequest4.setAcct(this.getCashAccount(account));
            reportingRequest4.setAcctOwnr(accOwner);
            reportingRequest4.setAcctSvcr(accountPartyRoleUtil.getBranchAndFinancialInstitutionIdentification5(account, AccountServicerRole.class));

            /**
             * Uncomment this for statement. Put reporting period section on Account.
             */
            //reportingRequest4.setRptgPrd(this.getReportingPeriod(account));

            //reportingRequest5.setRptgSeq(new SequenceRange1Choice());
            //reportingRequest5.setReqdTxTp(new TransactionType2());
            reportingRequest4.getReqdBalTp().addAll(this.buildRequestedBalanceType(account));

            reportingRequest4List.add(reportingRequest4);
        }

        accountReportingRequestV04.setGrpHdr(groupHeader76);
        accountReportingRequestV04.getRptgReq().addAll(reportingRequest4List);

        return accountReportingRequestV04;

    }

    private List<BalanceType13> buildRequestedBalanceType(CashAccount cashAccount){
        List<BalanceType13> balanceType13List = new ArrayList<>();

        BalanceType13 balanceType13 = new BalanceType13();

        BalanceType10Choice codeOrPrtry = new BalanceType10Choice();
        codeOrPrtry.setCd("ITAV");
        //codeOrPrtry.setPrtry("");

        /*BalanceSubType1Choice subType = new BalanceSubType1Choice();
        subType.setCd("");
        subType.setPrtry("");*/

        balanceType13.setCdOrPrtry(codeOrPrtry);
        //balanceType13.setSubTp(subType);

        balanceType13List.add(balanceType13);

        return balanceType13List;
    }

    private CashAccount24 getCashAccount(CashAccount cashAccount) {
        CashAccount24 cashAccount24 = new CashAccount24();

        String accountIBAN = cashAccount.getIdentification().getIBAN();
        String accountNumber = cashAccount.getIdentification().getNumber();
        String accountCcy = cashAccount.getBaseCurrency().getCodeName();
        String accountName = cashAccount.getIdentification().getName();

        GenericAccountIdentification1 other = new GenericAccountIdentification1();
        other.setId(accountNumber);
        //other.setSchmeNm(new AccountSchemeName1Choice());
        //other.setIssr("");

        AccountIdentification4Choice accOwnerAccountIdentification4Choice = new AccountIdentification4Choice();
        //accOwnerAccountIdentification4Choice.setIBAN(accountIBAN);
        accOwnerAccountIdentification4Choice.setOthr(other);

        CashAccountType2Choice type = new CashAccountType2Choice();
        type.setCd(cashAccount.getCashAccountType().getCodeName());
        //type.setPrtry("");

        cashAccount24.setId(accOwnerAccountIdentification4Choice);
        cashAccount24.setTp(type);
        cashAccount24.setCcy(accountCcy);
        cashAccount24.setNm(accountName);
        //cashAccount38.setPrxy(new ProxyAccountIdentification1());

        return cashAccount24;
    }

    private ReportingPeriod2 getReportingPeriod(CashAccount cashAccount){
        ReportingPeriod2 reportingPeriod2 = new ReportingPeriod2();

        DatePeriodDetails1 frmToDate = new DatePeriodDetails1();
        /*frmToDate.setFrDt(XmlDateUtil.getXmlDateNoTime(cashAccount.getReportedPeriod().getFromDateTime()));
        frmToDate.setToDt(XmlDateUtil.getXmlDateNoTime(cashAccount.getReportedPeriod().getToDateTime()));*/

        TimePeriodDetails1 frmToTime = new TimePeriodDetails1();
        /*frmToTime.setFrTm(XmlDateUtil.getXmlTimeNoDate(cashAccount.getReportedPeriod().getFromDateTime()));
        frmToTime.setToTm(XmlDateUtil.getXmlTimeNoDate(cashAccount.getReportedPeriod().getToDateTime()));*/

        reportingPeriod2.setFrToDt(frmToDate);
        reportingPeriod2.setFrToTm(frmToTime);
        //reportingPeriod2.setTp(new QueryType3Code());

        return reportingPeriod2;
    }

    /*private TransactionType2 getTransactionType(CashAccount cashAccount){
        TransactionType2 transactionType2 = new TransactionType2();

        transactionType2.setSts(new EntryStatus1Choice());
        transactionType2.setCdtDbtInd(new CreditDebitCode());

    }*/
}
