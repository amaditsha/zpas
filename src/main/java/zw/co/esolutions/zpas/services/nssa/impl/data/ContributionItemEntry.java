package zw.co.esolutions.zpas.services.nssa.impl.data;

import com.opencsv.bean.CsvBindByName;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleItemDTO;
import zw.co.esolutions.zpas.services.nssa.impl.exceptions.NssaPaymentInvalidRequestException;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Create by alfred on 19 Jul 2019
 */
@Data
@Builder
@Slf4j
public class ContributionItemEntry {
    @CsvBindByName(column = "FullName", required = true)
    private String fullName;

    @CsvBindByName(column = "NationalId")
    private String nationalId;

    @CsvBindByName(column = "SSN")
    private String ssn;

    @CsvBindByName(column = "POBS")
    protected double pobs;

    public ContributionItemEntry() {
    }

    public ContributionItemEntry(String fullName, String nationalId, String ssn, Double pobs) {
        this.nationalId = nationalId;
        this.pobs = pobs;
        this.fullName = fullName;
        this.ssn = ssn;
    }

    public ContributionScheduleItemDTO getContributionScheduleItem() throws NssaPaymentInvalidRequestException {
        CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();
        currencyAndAmount.setAmount(new BigDecimal(pobs));
        ContributionScheduleItemDTO contributionScheduleItem = new ContributionScheduleItemDTO();
        contributionScheduleItem.setId(UUID.randomUUID().toString());
        contributionScheduleItem.setDateCreated(OffsetDateTime.now());
        contributionScheduleItem.setLastUpdated(OffsetDateTime.now());
        contributionScheduleItem.setEntityStatus(EntityStatus.DRAFT);
        contributionScheduleItem.setName(fullName);
        contributionScheduleItem.setNationalId(nationalId);
        contributionScheduleItem.setSsn(ssn);
        contributionScheduleItem.setAmount(currencyAndAmount);
        contributionScheduleItem.setPobs(pobs);

        return contributionScheduleItem;
    }
}
