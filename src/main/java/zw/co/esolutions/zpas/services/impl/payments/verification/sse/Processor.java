package zw.co.esolutions.zpas.services.impl.payments.verification.sse;

import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.domain.PaymentStatusInfo;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

@Component
public class Processor extends AbstractProcessor<PaymentStatusInfo> {

    @Override
    public PaymentStatusInfo processOne() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return PaymentStatusInfo.builder()
                .status("SUCCESS")
                .requestId(GenerateKey.generateEntityId())
                .info("Dummy info")
                .build();
    }
}
