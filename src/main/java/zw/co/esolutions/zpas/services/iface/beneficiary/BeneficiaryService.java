package zw.co.esolutions.zpas.services.iface.beneficiary;

import zw.co.esolutions.zpas.model.Beneficiary;

import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
public interface BeneficiaryService {
    Beneficiary createBeneficiary(Beneficiary beneficiary);
    Beneficiary updateBeneficiary(Beneficiary beneficiary);
    Beneficiary deleteBeneficiary(String id);
    Beneficiary approveBeneficiary(String id);
    Beneficiary rejectBeneficiary(String id);
    Optional<Beneficiary> getBeneficiary(String id);
    List<Beneficiary> getBeneficiaries();
}
