package zw.co.esolutions.zpas.services.iface.payments;

import zw.co.esolutions.zpas.iso.msg.pain001_001_08.Document;
import zw.co.esolutions.zpas.model.Payment;

import java.util.Optional;

/**
 * Created by alfred on 05 Mar 2019
 */
public interface PaymentsAcquiringService {
    Payment buildPaymentFromDocument(Document document);
    Payment buildPaymentFromFile(String fileName);
    Optional<Payment> buildPaymentFromXml(String xml);
}
