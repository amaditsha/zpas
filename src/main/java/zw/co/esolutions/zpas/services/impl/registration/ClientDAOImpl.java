package zw.co.esolutions.zpas.services.impl.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PersonIdentification;
import zw.co.esolutions.zpas.model.PersonName;
import zw.co.esolutions.zpas.services.iface.registration.ClientDAO;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;


@Repository
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
public class ClientDAOImpl implements ClientDAO {
    PersonIdentification personIdentification;
    PersonName personName;

    @Autowired
    EntityManager entityManager;


    @Override
    public PersonIdentification getActivePerson(String identityCardNumber) {
        try{
           personIdentification = entityManager
                    .createQuery("from PersonIdentification pi where pi.identityCardNumber = :identityCardNumber", PersonIdentification.class)
                    .setParameter("identityCardNumber", identityCardNumber)
                    .getSingleResult();
        }catch (NoResultException e){
            personIdentification = null;
        }
        return personIdentification;
    }
}
