package zw.co.esolutions.zpas.services.impl.signature;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.RuleItem;
import zw.co.esolutions.zpas.repository.RuleItemRepository;
import zw.co.esolutions.zpas.services.iface.signature.RuleItemService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class RuleItemServiceImpl implements RuleItemService {

    @Autowired
    private RuleItemRepository ruleItemRepository;

    @Override
    public List<RuleItem> createRuleItems(List<RuleItem> ruleItems) {
        ruleItems.forEach(ruleItem -> {
            ruleItemRepository.save(ruleItem);
        });
        return ruleItems;
    }

    @Override
    public RuleItem createRuleItem(RuleItem ruleItem) {
        return ruleItemRepository.save(ruleItem);
    }

    @Override
    public RuleItem updateRuleItem(RuleItem ruleItem) {
        return ruleItemRepository.save(ruleItem);
    }

    @Override
    public RuleItem deleteRuleItem(String id) {
        Optional<RuleItem> ruleItemOptional = ruleItemRepository.findById(id);
        RuleItem ruleItem = null;
        if (ruleItemOptional.isPresent()) {
            ruleItem = ruleItemOptional.get();
            ruleItem.setEntityStatus(EntityStatus.DELETED);
            ruleItem = ruleItemRepository.save(ruleItem);
        }
        return ruleItem;
    }

    @Override
    public Optional<RuleItem> getRuleItemById(String signatureId) {
        return ruleItemRepository.findById(signatureId);
    }

    @Override
    public List<RuleItem> getRuleItems() {
        return ruleItemRepository.findAll();
    }
}
