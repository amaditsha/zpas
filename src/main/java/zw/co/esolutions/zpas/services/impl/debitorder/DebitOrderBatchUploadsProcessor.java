package zw.co.esolutions.zpas.services.impl.debitorder;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.debitorder.DebitOrderBatchDTO;
import zw.co.esolutions.zpas.dto.documents.DocumentDTO;
import zw.co.esolutions.zpas.enums.DebitOrderStatus;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.documents.DocumentsService;
import zw.co.esolutions.zpas.services.impl.payments.converters.SFIUtils;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.storage.StorageService;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.RefGen;
import zw.co.esolutions.zpas.utilities.util.SortCodeUtil;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Slf4j
@Component
@Transactional
public class DebitOrderBatchUploadsProcessor {

    private static final String OUTPUT_FOLDER = StorageProperties.BASE_LOCATION;

    private final AccountsService accountService;
    private final DocumentsService documentsService;
    private final StorageService storageService;
    private final DebitOrderBatchRepository debitOrderBatchRepository;

    @Autowired
    private SortCodeUtil sortCodeUtil;


    @Autowired
    public DebitOrderBatchUploadsProcessor(StorageService storageService,
                                           DebitOrderBatchRepository debitOrderBatchRepository,
                                           AccountsService accountService,
                                           DocumentsService documentsService) {
        this.storageService = storageService;
        this.debitOrderBatchRepository = debitOrderBatchRepository;
        this.accountService = accountService;
        this.documentsService = documentsService;
    }

    private Optional<DebitOrderBatch> saveDebitOrders(DebitOrderBatch debitOrderBatch) {
        log.info("Saving the direct orders ...");
        DebitOrderBatch savedDebitOrderBatch = debitOrderBatchRepository.save(debitOrderBatch);
        return Optional.ofNullable(savedDebitOrderBatch);
    }

    public Optional<DebitOrderBatch> uploadDebitOrderBatch(DebitOrderBatchDTO debitOrderBatchDTO) throws PaymentRequestInvalidArgumentException {
        log.info("Debit Order Upload DTO is: {}", debitOrderBatchDTO);
        final String fileName = debitOrderBatchDTO.getFileName();
        if (StringUtils.isEmpty(fileName)) {
            log.error("Invalid File ID specified. Failed to load uploaded debit order batch file. Upload and try again.");
            throw new PaymentRequestInvalidArgumentException("Invalid file");
        }

        final String fileExtension = getFileExtension(fileName);



        switch (fileExtension) {
            case ".csv": {
                final DebitOrderBatch debitOrderBatch = readBatchItemsFromCSVFile(debitOrderBatchDTO);
                saveDocument(debitOrderBatchDTO);
                return saveDebitOrders(debitOrderBatch);
            }
            case ".sfi": {
                try {
                    final DebitOrderBatch debitOrderBatch = readBatchItemsFromSFIFile(debitOrderBatchDTO);
                    saveDocument(debitOrderBatchDTO);
                    return saveDebitOrders(debitOrderBatch);
                } catch (Exception e) {
                    throw new PaymentRequestInvalidArgumentException("Something went wrong while reading SFI file.");
                }
            }
            case ".zip": {
                final Path resolve = Paths.get(OUTPUT_FOLDER).resolve(fileName);
                final String unzippedFile = unZipIt(resolve.toUri().getPath(), OUTPUT_FOLDER);
                uploadDebitOrderBatch(debitOrderBatchDTO.toBuilder().fileName(unzippedFile).build());
            }
            case ".xls":
            case ".xlsx": {
                final DebitOrderBatch debitOrderBatch = readBatchItemsFromExcelFile(debitOrderBatchDTO);
                saveDocument(debitOrderBatchDTO);
                return saveDebitOrders(debitOrderBatch);
            }
          /*  case ".xml": {
                final BulkPayment bulkPayment = (BulkPayment) paymentsAcquiringService.buildPaymentFromFile(fileName);
                log.info("Bulk Payment is: {}", bulkPayment);
                saveDocument(bulkPayment, bulkPayment.getAmount());
                return savePayments(bulkPayment, bulkPayment.getGroups()).map(b -> Optional.ofNullable((Payment) bulkPayment))
                        .orElse(Optional.empty());
            }*/
            default:
                throw new PaymentRequestInvalidArgumentException("Extension " + fileExtension + " not supported yet");
        }
    }

    private DebitOrderBatch readBatchItemsFromCSVFile(DebitOrderBatchDTO debitOrderBatchDTO) throws PaymentRequestInvalidArgumentException {
        log.info("Processing CSV batch file");
        final Optional<CashAccount> targetAccountOptional = accountService.findAccountByNumber(debitOrderBatchDTO.getTargetAccount());
        if (!targetAccountOptional.isPresent()) {
            throw new PaymentRequestInvalidArgumentException("Target account not found");
        }
        //additional batch info

        final DebitOrderBatch debitOrderBatch = new DebitOrderBatch();
        debitOrderBatch.setId(GenerateKey.generateEntityId());
        debitOrderBatch.setStatus(EntityStatus.DRAFT.name());
        debitOrderBatch.setBatchNumber(RefGen.getReference("BN"));
        debitOrderBatch.setDescription(debitOrderBatchDTO.getDescription());
        debitOrderBatch.setDateCreated(OffsetDateTime.now());

        log.info("Reading uploaded batch file...");
        BigDecimal totalAmount = new BigDecimal(0);

        Resource file = storageService.loadAsResource(debitOrderBatchDTO.getFileName());
        List<DebitOrderBatchItem> debitOrderBatchItems = new ArrayList<>();
        List<DebitOrderBatchEntry> debitOrderBatchEntries = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader(Paths.get(file.getURI()))) {
            CsvToBean<DebitOrderBatchEntry> csvDebitOrderBatchEntryCsvToBean = new CsvToBeanBuilder(reader)
                    .withType(DebitOrderBatchEntry.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<DebitOrderBatchEntry> csvDebitOrderBatchEntryIterator = csvDebitOrderBatchEntryCsvToBean.iterator();
            while (csvDebitOrderBatchEntryIterator.hasNext()) {
                debitOrderBatchEntries.add(csvDebitOrderBatchEntryIterator.next());
            }
        } catch (IOException | RuntimeException re) {
            throw new PaymentRequestInvalidArgumentException("Error Reading CSV File: " + re.getMessage());
        }

        /**
         * senderId is the bicFi of the destination fi
         */
        String receiverId = "";
        String clientName = "";
        if (targetAccountOptional.isPresent()) {
            CashAccount cashAccount = targetAccountOptional.get();
            clientName = cashAccount.getIdentification().getName();
            Optional<String> optionalSenderId = cashAccount.getPartyRole().stream().filter(role -> role instanceof AccountServicerRole).map(role -> {
                Optional<RolePlayer> optionalFI = role.getPlayer().stream().findFirst();
                String bicfi = "";
                if (optionalFI.isPresent()) {
                    FinancialInstitution financialInstitution = (FinancialInstitution) optionalFI.get();
                    bicfi = financialInstitution.getBIC();
                }
                return bicfi;
            }).findFirst();
            if (optionalSenderId.isPresent()) {
                receiverId = optionalSenderId.get();
            }
        }
        String destinationSortCode = sortCodeUtil.getSingleSortCode(receiverId).orElse("");
        log.info("Destination sort bic : {} & sort code : {}", receiverId, destinationSortCode);
        try {
            for (DebitOrderBatchEntry debitOrderBatchEntry : debitOrderBatchEntries) {
                DebitOrderBatchItem debitOrderBatchItem = debitOrderBatchEntry.getDebitOrderBatchItem();
                final BigDecimal debitOrderBatchItemAmount = debitOrderBatchItem.getAmount();
                totalAmount = totalAmount.add(debitOrderBatchItemAmount);
                debitOrderBatchItem.setDebitOrderBatch(debitOrderBatch);

                String originSortCode = sortCodeUtil.getSingleSortCode(debitOrderBatchItem.getSourceBICFI()).orElse("");

                log.info("Destination sort bic : {} & sort code : {}", debitOrderBatchItem.getSourceBICFI(), originSortCode);
                debitOrderBatchItem.setOriginSortCode(originSortCode);

                debitOrderBatchItem.setDestinationSortCode(destinationSortCode);
                debitOrderBatchItem.setAmountCurrencyCode(debitOrderBatchDTO.getCurrencyCode());
                debitOrderBatchItem.setDestinationAccount(targetAccountOptional.get().getIdentification().getIBAN());
                debitOrderBatchItem.setDestinationName(clientName);
                debitOrderBatchItem.setDestinationAccountType(targetAccountOptional.get().getCashAccountType().name());

                debitOrderBatchItems.add(debitOrderBatchItem);
            }
        } catch (RuntimeException re) {
            log.error(re.getMessage());
            throw new PaymentRequestInvalidArgumentException("An error occurred. " + re.getMessage());
        }

        //final configuration on batch and bulk
        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        instructedCurrencyAndAmount.setAmount(totalAmount);
        log.info("Debit Order Batch Amount is: {}", instructedCurrencyAndAmount);

        debitOrderBatch.setTotalAmount(totalAmount);
        debitOrderBatch.setTotalCount(debitOrderBatchItems.size());
        debitOrderBatch.setCurrencyCode(debitOrderBatchDTO.getCurrencyCode());
        debitOrderBatch.setTargetAccount(debitOrderBatchDTO.getTargetAccount());
        debitOrderBatch.setProcessingDate(debitOrderBatchDTO.getProcessingDate());
        debitOrderBatch.setSenderID(clientName);
        debitOrderBatch.setReceiverID(receiverId);
        debitOrderBatch.setClientId(debitOrderBatchDTO.getClientId());
        debitOrderBatch.setClientName(Optional.ofNullable(clientName).orElse(receiverId));
        debitOrderBatch.setFileID(GenerateKey.generateFileId());
        debitOrderBatch.setWorkCode(RefGen.getReference("WC"));
        debitOrderBatch.setVersion("008");
        debitOrderBatch.setHeaderID("UHL");
        debitOrderBatch.setTrailerId("UTL");
        debitOrderBatch.setBatchResponseCode("00");
        debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.DRAFT);

        debitOrderBatch.setFileName(getFileName("DEBIT_ORDERS", OffsetDateTime.now()));

        debitOrderBatch.setDebitOrderBatchItems(debitOrderBatchItems);

        return debitOrderBatch;
    }

    private DebitOrderBatch readBatchItemsFromSFIFile(DebitOrderBatchDTO debitOrderBatchDTO) throws Exception {
        log.info("Processing SFI batch file");
        final Optional<CashAccount> targetAccountOptional = accountService.findAccountByNumber(debitOrderBatchDTO.getTargetAccount());
        if (!targetAccountOptional.isPresent() ) {
            throw new PaymentRequestInvalidArgumentException("Target account not found");
        }

        String receiverId = "";
        String clientName = "";
        if(targetAccountOptional.isPresent()){
            CashAccount cashAccount = targetAccountOptional.get();
            clientName = cashAccount.getIdentification().getName();
            Optional<String> optionalDestinationSortCode = cashAccount.getPartyRole().stream().filter(role -> role instanceof AccountServicerRole).map(role -> {
                Optional<RolePlayer> optionalFI = role.getPlayer().stream().findFirst();
                String bicfi = "";
                if(optionalFI.isPresent()){
                    FinancialInstitution financialInstitution = (FinancialInstitution) optionalFI.get();
                    bicfi = financialInstitution.getBIC();
                }
                return bicfi;
            }).findFirst();
            if(optionalDestinationSortCode.isPresent()){
                receiverId = optionalDestinationSortCode.get();
            }
        }

        String destinationSortCode = sortCodeUtil.getSingleSortCode(receiverId).orElse("");
        log.info("Destination sort bic : {} & sort code : {}", receiverId, destinationSortCode);

        final DebitOrderBatch debitOrderBatch = SFIUtils.readDebitOrderBatchSFIFileContents(SFIUtils.FILES_ROOT + "/" + debitOrderBatchDTO.getFileName());
        debitOrderBatch.setId(GenerateKey.generateEntityId());
        debitOrderBatch.setStatus(EntityStatus.DRAFT.name());
        debitOrderBatch.setBatchNumber(RefGen.getReference("BN"));
        debitOrderBatch.setDescription(debitOrderBatchDTO.getDescription());
        debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.DRAFT);
        debitOrderBatch.setClientId(debitOrderBatchDTO.getClientId());
        debitOrderBatch.setClientName(Optional.ofNullable(clientName).orElse(receiverId));
        debitOrderBatch.setTargetAccount(debitOrderBatchDTO.getTargetAccount());
        debitOrderBatch.setTotalCount(debitOrderBatch.getDebitOrderBatchItems().size());
        debitOrderBatch.setFileID(GenerateKey.generateFileId());
        debitOrderBatch.setWorkCode(RefGen.getReference("WC"));
        debitOrderBatch.setVersion("008");
        debitOrderBatch.setHeaderID("UHL");
        debitOrderBatch.setTrailerId("UTL");
        debitOrderBatch.setReceiverID(receiverId);
        debitOrderBatch.setSenderID(clientName);
        debitOrderBatch.setBatchResponseCode("00");

        for(DebitOrderBatchItem debitOrderBatchItem : debitOrderBatch.getDebitOrderBatchItems()){
            if(targetAccountOptional.isPresent()){
                String originSortCode = sortCodeUtil.getSingleSortCode(debitOrderBatchItem.getSourceBICFI()).orElse("");
                log.info("Destination sort bic : {} & sort code : {}", debitOrderBatchItem.getSourceBICFI(), originSortCode);

                debitOrderBatchItem.setOriginSortCode(originSortCode);
                debitOrderBatchItem.setDestinationSortCode(destinationSortCode);
                debitOrderBatchItem.setAmountCurrencyCode(debitOrderBatchDTO.getCurrencyCode());
                debitOrderBatchItem.setDestinationAccount(targetAccountOptional.get().getIdentification().getIBAN());
                debitOrderBatchItem.setDestinationName(targetAccountOptional.get().getIdentification().getName());
                debitOrderBatchItem.setDestinationAccountType(targetAccountOptional.get().getCashAccountType().name());
            }
        }

        return debitOrderBatch;
    }

    //Generating Unique File Name
    public String getFileName(String baseFileName,OffsetDateTime date) {
        String timeStamp = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss").format(date);
        return baseFileName + "_" + timeStamp;
    }

    private DebitOrderBatch readBatchItemsFromExcelFile(DebitOrderBatchDTO debitOrderBatchDTO) throws PaymentRequestInvalidArgumentException {
        log.info("Processing Excel batch file");
        final Optional<CashAccount> targetAccountOptional = accountService.findAccountByNumber(debitOrderBatchDTO.getTargetAccount());
        if (!targetAccountOptional.isPresent()) {
            throw new PaymentRequestInvalidArgumentException("Target account not found..");
        }

        /**
         * senderId is the bicFi of the destination fi
         */
        String senderId = "";
        if (targetAccountOptional.isPresent()) {
            CashAccount cashAccount = targetAccountOptional.get();
            Optional<String> optionalSenderId = cashAccount.getPartyRole().stream().filter(role -> role instanceof AccountServicerRole).map(role -> {
                Optional<RolePlayer> optionalFI = role.getPlayer().stream().findFirst();
                String bicfi = "";
                if (optionalFI.isPresent()) {
                    FinancialInstitution financialInstitution = (FinancialInstitution) optionalFI.get();
                    bicfi = financialInstitution.getBIC();
                }
                return bicfi;
            }).findFirst();
            if (optionalSenderId.isPresent()) {
                senderId = optionalSenderId.get();
            }
        }

        log.info("senderId: " + senderId);
        //additional batch info



        //additional debit order batch info

        final DebitOrderBatch debitOrderBatch = new DebitOrderBatch();
        debitOrderBatch.setId(GenerateKey.generateEntityId());
        debitOrderBatch.setStatus(EntityStatus.DRAFT.name());
        debitOrderBatch.setBatchNumber(RefGen.getReference("BN"));
        debitOrderBatch.setDescription(debitOrderBatchDTO.getDescription());

        log.info("Reading uploaded xls batch file...");
        BigDecimal totalAmount = new BigDecimal(0);

        List<DebitOrderBatchItem> debitOrderBatchItems = new ArrayList<>();
        final Map<Integer, List<String>> debitOrderBatchData = readExcelFile(debitOrderBatchDTO.getFileName());
        List<DebitOrderBatchEntry> debitOrderBatchEntries = new ArrayList<>();

        try {

            for (List<String> dataList : debitOrderBatchData.values()) {
                final DebitOrderBatchEntry debitOrderBatchEntry = DebitOrderBatchEntry.builder()
                        .sourceAccount(dataList.get(0))
                        .sourceBIC(dataList.get(1))
                        .amount(dataList.get(2))
                        .narrative(dataList.get(3))
                        .sourceName(dataList.get(4))
                        .build();

                debitOrderBatchEntries.add(debitOrderBatchEntry);
            }
            log.info("Fetching source accounts..");
            List<String> accountIbans = debitOrderBatchEntries.stream().map(DebitOrderBatchEntry::getSourceAccount).collect(Collectors.toList());
            log.info("Account Ibans are: {}", accountIbans);
            log.info("Accounts are: {}", accountIbans);
            final Map<String, CashAccount> accountsMap = accountService.findAccountsByIbansIn(accountIbans);
            log.info("Account map: {}", accountsMap);

        } catch (RuntimeException re) {
            log.error(re.getMessage());
            return null;
        }
        log.info("senderId: " + senderId);

        try {
            for (DebitOrderBatchEntry debitOrderBatchEntry : debitOrderBatchEntries) {
                DebitOrderBatchItem debitOrderBatchItem = debitOrderBatchEntry.getDebitOrderBatchItem();
                final BigDecimal debitOrderBatchItemAmount = debitOrderBatchItem.getAmount();
                totalAmount = totalAmount.add(debitOrderBatchItemAmount);
                debitOrderBatchItem.setDebitOrderBatch(debitOrderBatch);

                debitOrderBatchItem.setDestinationSortCode(senderId);
                debitOrderBatchItem.setDestinationAccount(targetAccountOptional.get().getIdentification().getIBAN());
                debitOrderBatchItem.setDestinationName(targetAccountOptional.get().getIdentification().getName());
                debitOrderBatchItem.setDestinationAccountType(targetAccountOptional.get().getCashAccountType().name());

                debitOrderBatchItems.add(debitOrderBatchItem);
            }
        } catch (RuntimeException re) {
            log.error(re.getMessage());
            throw new PaymentRequestInvalidArgumentException("An error occurred.. " + re.getMessage());
        }

        //final configuration on batch and bulk
        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        instructedCurrencyAndAmount.setAmount(totalAmount);
        log.info("Debit Order Batch Amount is: {}", instructedCurrencyAndAmount);

        debitOrderBatch.setTotalAmount(totalAmount);
        debitOrderBatch.setTotalCount(debitOrderBatchItems.size());
        debitOrderBatch.setTargetAccount(debitOrderBatchDTO.getTargetAccount());
        debitOrderBatch.setProcessingDate(debitOrderBatchDTO.getProcessingDate());
        debitOrderBatch.setSenderID(senderId);
        debitOrderBatch.setReceiverID(senderId);
        debitOrderBatch.setClientId(debitOrderBatchDTO.getClientId());
        debitOrderBatch.setClientName(debitOrderBatchDTO.getClientName());
        debitOrderBatch.setFileID(GenerateKey.generateFileId());
        debitOrderBatch.setWorkCode(RefGen.getReference("WC"));
        debitOrderBatch.setVersion("003");
        debitOrderBatch.setHeaderID("UHL");
        debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.DRAFT);

        debitOrderBatch.setDebitOrderBatchItems(debitOrderBatchItems);

        return debitOrderBatch;
    }

    private void saveDocument(DebitOrderBatchDTO debitOrderBatchDTO) {
        final String fileName = debitOrderBatchDTO.getFileName();

        documentsService.createDocument(DocumentDTO.builder()
                .clientId(debitOrderBatchDTO.getClientId())
                .copyDuplicate("")
                .dataSetType("")
                .documentName(fileName)
                .documentPath(Paths.get(StorageProperties.DEBIT_ORDER_BASE_LOCATION).resolve(fileName).toString())
                .issueDate(LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                .purpose(debitOrderBatchDTO.getDescription())
                .type(debitOrderBatchDTO.getFileID())
                .build());
    }

    private static String getFileExtension(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            return "";
        }
        if (!fileName.contains(".")) {
            return "";
        }
        final String fileExtension = fileName.substring(fileName.lastIndexOf('.'));
        return fileExtension;
    }

    public static void main(String[] args) {
        String fileName = "DebitOrderTest.xls";
        final Map<Integer, List<String>> data = readExcelFile(fileName);

        log.info("Read data from Excel file {}.", fileName);
        data.values().forEach(strings -> {
            log.info("Data: {}", strings);
        });
    }

    private static Map<Integer, List<String>> readExcelFile(String fileName) {
        switch (getFileExtension(fileName)) {
            case ".xls":
                return readXLSExcelFile(fileName);
            case ".xlsx":
                readXLSXExcelFile(fileName);
            default:
                throw new RuntimeException("Excel Processor could not read unknown file extension: " + getFileExtension(fileName));
        }
    }

    private static Map<Integer, List<String>> readXLSExcelFile(String fileName) {
        Path excelFile = Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName);

//        HSSFWorkbook, HSSFSheet, HSSFRow, and HSSFCell
        Map<Integer, List<String>> data = new HashMap<>();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(Files.newInputStream(excelFile));
            HSSFSheet sheet = workbook.getSheetAt(0);

            int i = 0;
            final Iterator<Row> hssRows = sheet.iterator();
            while (hssRows.hasNext()) {
                final HSSFRow row = (HSSFRow) hssRows.next();
                data.put(i, new ArrayList<>());
                final Iterator<Cell> cells = row.iterator();
                int columnCount = 0;
                while (cells.hasNext()) {
                    final HSSFCell cell = (HSSFCell) cells.next();
                    switch (cell.getCellType()) {
                        case STRING: {
                            data.get(i).add(cell.getRichStringCellValue().getString());
                            break;
                        }
                        case NUMERIC: {
                            if (columnCount == 0) {
                                DataFormatter formatter = new DataFormatter();
                                String accountNumber = formatter.formatCellValue(cell);
                                data.get(i).add(accountNumber);
                            } else {
                                if (DateUtil.isCellDateFormatted(cell)) {
                                    data.get(i).add(cell.getDateCellValue() + "");
                                } else {
                                    data.get(i).add(cell.getNumericCellValue() + "");
                                }
                            }
                            break;
                        }
                        case BOOLEAN: {
                            data.get(i).add(cell.getBooleanCellValue() + "");
                            break;
                        }
                        case FORMULA: {
                            data.get(i).add(cell.getCellFormula() + "");
                            break;
                        }
                        default:
                            data.get(new Integer(i)).add(" ");
                    }
                    columnCount++;
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static Map<Integer, List<String>> readXLSXExcelFile(String fileName) {
        Path excelFile = Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName);

        Map<Integer, List<String>> data = new HashMap<>();
        try {
            Workbook workbook = new XSSFWorkbook(Files.newInputStream(excelFile));
            Sheet sheet = workbook.getSheetAt(0);

            int i = 0;
            for (Row row : sheet) {
                data.put(i, new ArrayList<>());
                for (Cell cell : row) {
                    switch (cell.getCellType()) {
                        case STRING: {
                            data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
                            break;
                        }
                        case NUMERIC: {
                            if (DateUtil.isCellDateFormatted(cell)) {
                                data.get(i).add(cell.getDateCellValue() + "");
                            } else {
                                data.get(i).add(cell.getNumericCellValue() + "");
                            }
                            break;
                        }
                        case BOOLEAN: {
                            data.get(i).add(cell.getBooleanCellValue() + "");
                            break;
                        }
                        case FORMULA: {
                            data.get(i).add(cell.getCellFormula() + "");
                            break;
                        }
                        default:
                            data.get(new Integer(i)).add(" ");
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static String unZipIt(String zipFile, String outputFolder) {

        byte[] buffer = new byte[1024];

        try {

            //create output directory is not exists
            File folder = new File(OUTPUT_FOLDER);
            if (!folder.exists()) {
                folder.mkdir();
            }

            //get the zip file content
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));

            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();
            List<String> fileNames = new ArrayList<>();

            while (ze != null) {
                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);

                log.info("file unzip : " + newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
                fileNames.add(newFile.getName());
            }

            zis.closeEntry();
            zis.close();

            return fileNames.get(0);

        } catch (IOException ex) {
            ex.printStackTrace();
            return "";
        }
    }

}
