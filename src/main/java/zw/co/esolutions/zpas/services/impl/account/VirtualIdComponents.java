package zw.co.esolutions.zpas.services.impl.account;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 23 Apr 2019
 */
@Data
@Builder(toBuilder = true)
public class VirtualIdComponents {
    private String virtualId;
    private String virtualCode;
    private Integer virtualNumber;
    private String virtualSuffix;
    private boolean fetchMaster;
}
