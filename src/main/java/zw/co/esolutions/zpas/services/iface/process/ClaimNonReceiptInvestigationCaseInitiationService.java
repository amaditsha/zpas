package zw.co.esolutions.zpas.services.iface.process;

import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

public interface ClaimNonReceiptInvestigationCaseInitiationService {
    ServiceResponse initiateClaimNonReceiptRequest(PaymentInvestigationCase paymentInvestigationCase);
}
