package zw.co.esolutions.zpas.services.impl.tax;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.tax.TaxAuthorityDTO;
import zw.co.esolutions.zpas.dto.tax.TaxAuthorityPaymentOfficeDTO;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.TaxAuthority;
import zw.co.esolutions.zpas.model.TaxAuthorityPaymentOffice;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.repository.TaxAuthorityPaymentOfficeRepository;
import zw.co.esolutions.zpas.repository.TaxAuthorityRepository;
import zw.co.esolutions.zpas.services.iface.tax.TaxAuthorityService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Slf4j
@Service
@Transactional
public class TaxAuthorityServiceImpl implements TaxAuthorityService {

    @Autowired
    TaxAuthorityRepository taxAuthorityRepository;

    @Autowired
    private TaxAuthorityPaymentOfficeRepository taxAuthorityPaymentOfficeRepository;

    @Autowired
    PartyRepository partyRepository;

    @Override
    public TaxAuthority createTaxAuthority(TaxAuthorityDTO taxAuthorityDTO) {
        TaxAuthority taxAuthority = new TaxAuthority();
        taxAuthority.setId(GenerateKey.generateEntityId());
        taxAuthority.setDateCreated(OffsetDateTime.now());
        taxAuthority.setName(taxAuthorityDTO.getName());
        taxAuthority.setCode(taxAuthorityDTO.getCode());
        taxAuthority.setAccountNumber(taxAuthorityDTO.getAccountNumber());
        taxAuthority.setBank(taxAuthorityDTO.getBank());
        Optional<Party> optionalParty = partyRepository.findById(taxAuthorityDTO.getLinkedPartyTaxAuthority());

        optionalParty.ifPresent(party -> {
            log.info("The party to register the tax authority is found.");
            taxAuthority.setLinkedPartyTaxAuthority(party);
        });
        taxAuthority.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        return taxAuthorityRepository.save(taxAuthority);
    }

    @Override
    public TaxAuthority updateTaxAuthority(TaxAuthorityDTO taxAuthorityDTO) {
        return taxAuthorityRepository.findById(taxAuthorityDTO.getId()).map(taxAuthority -> {
            taxAuthority.setAccountNumber(taxAuthorityDTO.getAccountNumber());
//            taxAuthority.setBank(taxAuthorityDTO.getBank());
            taxAuthority.setName(taxAuthorityDTO.getName());
            taxAuthority.setCode(taxAuthorityDTO.getCode());
            taxAuthority.setLastUpdated(OffsetDateTime.now());
            taxAuthority.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
            return taxAuthorityRepository.save(taxAuthority);
        }).orElseGet(() -> {
            return null;
        });
    }

    @Override
    public TaxAuthority deleteTaxAuthority(String id) {
        Optional<TaxAuthority> taxAuthorityOptional = taxAuthorityRepository.findById(id);
        TaxAuthority taxAuthority = null;

        if (taxAuthorityOptional.isPresent()) {
            taxAuthority = taxAuthorityOptional.get();
            taxAuthority.setEntityStatus(EntityStatus.DELETED);
            taxAuthority = taxAuthorityRepository.save(taxAuthority);
        } else {
            log.info("TaxAuthority has already been deleted");
        }
        return taxAuthority;
    }

    @Override
    public TaxAuthority approveTaxAuthority(String id) {
        Optional<TaxAuthority> optionalTaxAuthority = taxAuthorityRepository.findById(id);
        TaxAuthority taxAuthority = null;
        if (optionalTaxAuthority.isPresent()) {
            taxAuthority = optionalTaxAuthority.get();
            if (taxAuthority.getEntityStatus() != EntityStatus.DELETED) {
                taxAuthority.setEntityStatus(EntityStatus.ACTIVE);
                taxAuthority = taxAuthorityRepository.save(taxAuthority);
            } else {
                log.info("TaxAuthority has already been approved");
            }
        }
        return taxAuthority;
    }

    @Override
    public TaxAuthority rejectTaxAuthority(String id) {
        Optional<TaxAuthority> optionalTaxAuthority = taxAuthorityRepository.findById(id);
        TaxAuthority taxAuthority = null;
        if (optionalTaxAuthority.isPresent()) {
            taxAuthority = optionalTaxAuthority.get();
            if (taxAuthority.getEntityStatus() != EntityStatus.DELETED) {
                taxAuthority.setEntityStatus(EntityStatus.DISAPPROVED);
                taxAuthority = taxAuthorityRepository.save(taxAuthority);
            } else {
                log.info("TaxAuthority " + id + "has already been rejected");
            }
        }
        return taxAuthority;
    }

    @Override
    public Optional<TaxAuthority> getTaxAuthority(String id) {
        taxAuthorityRepository.findById(id).ifPresent(taxAuthority -> {

        });
        return taxAuthorityRepository.findById(id);
    }

    @Override
    public List<TaxAuthority> getTaxAuthorities() {
        return taxAuthorityRepository.findAll();
    }

    @Override
    public Optional<TaxAuthorityPaymentOffice> createTaxAuthorityPaymentOffice(TaxAuthorityPaymentOfficeDTO taxAuthorityPaymentOfficeDTO) {
        TaxAuthorityPaymentOffice taxAuthorityPaymentOffice = new TaxAuthorityPaymentOffice();
        taxAuthorityPaymentOffice.setId(GenerateKey.generateEntityId());
        taxAuthorityPaymentOffice.setDateCreated(OffsetDateTime.now());
        taxAuthorityPaymentOffice.setLastUpdated(OffsetDateTime.now());
        taxAuthorityPaymentOffice.setEntityStatus(EntityStatus.ACTIVE);
        taxAuthorityPaymentOffice.setName(taxAuthorityPaymentOfficeDTO.getName());
        return getTaxAuthority(taxAuthorityPaymentOfficeDTO.getTaxAuthorityId()).map(taxAuthority -> {
            taxAuthorityPaymentOffice.setTaxAuthority(taxAuthority);
            return Optional.ofNullable(taxAuthorityPaymentOfficeRepository.save(taxAuthorityPaymentOffice));
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<TaxAuthorityPaymentOffice> deleteTaxAuthorityPaymentOffice(String taxAuthorityPaymentOfficeId) {
        final Optional<TaxAuthorityPaymentOffice> taxAuthorityPaymentOfficeOptional
                = taxAuthorityPaymentOfficeRepository.findById(taxAuthorityPaymentOfficeId);
        return taxAuthorityPaymentOfficeOptional.map(taxAuthorityPaymentOffice -> {
            taxAuthorityPaymentOffice.setEntityStatus(EntityStatus.DELETED);
            return Optional.ofNullable(taxAuthorityPaymentOffice);
        }).orElse(taxAuthorityPaymentOfficeOptional);
    }

    @Override
    public Optional<TaxAuthorityPaymentOffice> approveTaxAuthorityPaymentOffice(String taxAuthorityPaymentOfficeId) {
        return taxAuthorityPaymentOfficeRepository.findById(taxAuthorityPaymentOfficeId)
                .map(taxAuthorityPaymentOffice -> {
                    taxAuthorityPaymentOffice.setEntityStatus(EntityStatus.ACTIVE);
                    return Optional.ofNullable(taxAuthorityPaymentOffice);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<TaxAuthorityPaymentOffice> rejectTaxAuthorityPaymentOffice(String taxAuthorityPaymentOfficeId) {
        return taxAuthorityPaymentOfficeRepository.findById(taxAuthorityPaymentOfficeId)
                .map(taxAuthorityPaymentOffice -> {
                    taxAuthorityPaymentOffice.setEntityStatus(EntityStatus.DISAPPROVED);
                    return Optional.ofNullable(taxAuthorityPaymentOffice);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOffice(String taxAuthorityPaymentOfficeId) {
        return taxAuthorityPaymentOfficeRepository.findById(taxAuthorityPaymentOfficeId);
    }

    @Override
    public Optional<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOfficeByCode(String taxAuthorityPaymentOfficeCode) {
        return taxAuthorityPaymentOfficeRepository.findByCode(taxAuthorityPaymentOfficeCode);
    }

    @Override
    public List<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOffices() {
        return taxAuthorityPaymentOfficeRepository.findAll();
    }

    @Override
    public List<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOfficesByTaxAuthority(String taxAuthorityId) {
        return taxAuthorityPaymentOfficeRepository.findAllByTaxAuthorityId(taxAuthorityId);
    }
}
