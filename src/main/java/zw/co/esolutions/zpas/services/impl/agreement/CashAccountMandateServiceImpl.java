package zw.co.esolutions.zpas.services.impl.agreement;

import zw.co.esolutions.zpas.dto.account.AccountPartyInfo;
import zw.co.esolutions.zpas.dto.agreement.ApprovalResponseDTO;
import zw.co.esolutions.zpas.dto.agreement.MandateRegistrationDTO;
import zw.co.esolutions.zpas.dto.agreement.SignatureConditionDTO;
import zw.co.esolutions.zpas.model.CashAccountMandate;
import zw.co.esolutions.zpas.enums.TransactionChannelCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.codeset.ExternalCode;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountMandateService;
import zw.co.esolutions.zpas.services.iface.agreement.SignatureConditionService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.lang.System;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CashAccountMandateServiceImpl implements CashAccountMandateService {
    //    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    @Autowired
    private CashAccountMandateRepository cashAccountMandateRepository;
    @Autowired
    private SignatureConditionService signatureConditionService;
    @Autowired
    private SignatureConditionRepository signatureConditionRepository;
    @Autowired
    private CashAccountRepository cashAccountRepository;
    @Autowired
    private CashAccountContractRepository cashAccountContractRepository;

    @Autowired
    private DateTimePeriodRepository dateTimePeriodRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private SigningDetailRepository signingDetailRepository;

    @Autowired
    private MandatePartyRoleRepository mandatePartyRoleRepository;

    @Autowired
    private RolePlayerRepository rolePlayerRepository;


    @Override
    public CashAccountMandate createCashAccountMandate(MandateRegistrationDTO mandateRegistrationDTO) {
        /**
         * Create the cash account mandate
         * */
        CashAccountMandate cashAccountMandate = new CashAccountMandate();
//        cashAccountMandate.setServices(Lists.newArrayList());

        /**
         * Create the cash account contract
         * */

        CashAccountContract cashAccountContract = new CashAccountContract();
        Optional<CashAccount> optionalCashAccount = cashAccountRepository.findById(mandateRegistrationDTO.getCashAccountId());
        cashAccountContract.setCashAccount(Arrays.asList(optionalCashAccount.get()));
//        cashAccountContract.setTransferCashAccount(Lists.newArrayList());
//        cashAccountContract.setServices(Lists.newArrayList());
//        cashAccountContract.setBalanceTransfer(Lists.newArrayList());
//        LocalDate targetDateLocalDate = LocalDate.parse(mandateRegistrationDTO.getTargetClosingDate(), formatter);
//        OffsetDateTime targeClosingDate = targetDateLocalDate.atTime(OffsetTime.of(0, 0, 0, 0, OffsetDateTime.now().getOffset()));
//
//
//        cashAccountContract.setTargetClosingDate(targeClosingDate);
//        cashAccountContract.setUrgencyFlag(getBooleanFormValue(mandateRegistrationDTO.getUrgencyFlag()));
//        cashAccountContract.setRemovalIndicator(getBooleanFormValue(mandateRegistrationDTO.getRemovalIndicator()));
//
//        LocalDate targetGoLiveDateLocalDate = LocalDate.parse(mandateRegistrationDTO.getTargetGoLiveDate(), formatter);
//        OffsetDateTime targetGoLiveDate = targetGoLiveDateLocalDate.atTime(OffsetTime.of(0, 0, 0, 0, OffsetDateTime.now().getOffset()));
//
//        cashAccountContract.setTargetGoLiveDate(targetGoLiveDate);
//        cashAccountContract.setAccount(Lists.newArrayList());
//        cashAccountContract.setAccountService(new AccountService());

        DateTimeFormatter requestDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate requestDateLocalDate = LocalDate.parse(mandateRegistrationDTO.getRequestDate(), requestDateFormatter);
        OffsetDateTime requestDate = requestDateLocalDate.atTime(OffsetTime.of(0, 0, 0, 0, OffsetDateTime.now().getOffset()));

        cashAccountContract.setRequestDate(requestDate);
//        cashAccountContract.setAccountAuthorisation(Lists.newArrayList());
        cashAccountContract.setTransactionChannel(TransactionChannelCode.valueOf(mandateRegistrationDTO.getTransactionChannelCode()));
//        cashAccountContract.setMasterAgreement(Lists.newArrayList());
//        cashAccountContract.setDateSigned(null);
        cashAccountContract.setDescription(mandateRegistrationDTO.getAccountContractDescription());
        cashAccountContract.setVersion(mandateRegistrationDTO.getAccountContractVersion());

        /**
         * Create validity period for account contract
         * */

        DateTimeFormatter dateTime_localFomatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        DateTimePeriod validityPeriod = new DateTimePeriod();

        LocalDateTime accountContractFromDateLocalDate = LocalDateTime.parse(mandateRegistrationDTO.getFromDateTime().replace("T", " "), dateTime_localFomatter);
        OffsetDateTime accountContractFromDate = accountContractFromDateLocalDate.atOffset(OffsetDateTime.now().getOffset());

        validityPeriod.setFromDateTime(accountContractFromDate);

        LocalDateTime accountContractToDateLocalDate = LocalDateTime.parse(mandateRegistrationDTO.getToDateTime().replace("T", " "), dateTime_localFomatter);
        OffsetDateTime accountContractToDate = accountContractToDateLocalDate.atOffset(OffsetDateTime.now().getOffset());

        validityPeriod.setToDateTime(accountContractToDate);

        cashAccountContract.setValidityPeriod(Arrays.asList(validityPeriod));
//        cashAccountContract.setDocument(null);
        cashAccountContract.setEntityVersion(0L);

        cashAccountMandate.setCashAccountContract(Arrays.asList(cashAccountContract));


        /**
         * Create the signature conditions
         **/

        SignatureConditionDTO signatureConditionDTO = new SignatureConditionDTO();
        signatureConditionDTO.setRequiredSignatureNumber(mandateRegistrationDTO.getRequiredSignatureNumber());
        signatureConditionDTO.setSignatoryRightIndicator(mandateRegistrationDTO.getSignatoryRightIndicator());
        signatureConditionDTO.setSignatureOrderIndicator(mandateRegistrationDTO.getSignatureOrderIndicator());
        signatureConditionDTO.setSignatureOrder(mandateRegistrationDTO.getSignatureOrder());
        signatureConditionDTO.setDescription(mandateRegistrationDTO.getSignatureDescription());


        SignatureCondition condition = signatureConditionService.createSignatureCondition(signatureConditionDTO);
        log.info("Signature Conditions: " + condition);
        //cashAccountMandate.setSignatureConditions(signatureConditions);
        cashAccountMandate.setMandateIdentification(mandateRegistrationDTO.getMandateIdentification());
//        cashAccountMandate.setOriginalMandate(null);
//        cashAccountMandate.setAmendment(null);
//        Specifies each role linked to a mandate and played by a party in that context.


//        cashAccountMandate.setMandatePartyRole(mandatePartyRoles);

        /**
         * Create the mandate status
         * */
//        Specifies whether a mandate is accepted or rejected.
        MandateStatus mandateStatus = new MandateStatus();
        mandateStatus.setAccepted(mandateRegistrationDTO.getAccepted());
        ExternalCode externalCode = new ExternalCode();
        externalCode.setCodeName("");
        externalCode.setName("");

        mandateStatus.setAccepted(mandateRegistrationDTO.getAccepted());
        mandateStatus.setRejectReason(externalCode);
        mandateStatus.setMandate(cashAccountMandate);
        mandateStatus.setEntityVersion(0L);
//        mandateStatus.setStatusReason(Lists.newArrayList());
        mandateStatus.setStatusDateTime(OffsetDateTime.now());
        mandateStatus.setValidityTime(null);

        mandateStatus.setStatusDescription(mandateRegistrationDTO.getStatusDescription());
//        mandateStatus.setInstructionProcessingStatus(StatusCode.valueOf(mandateRegistrationDTO.getInstructionProcessingStatus()));
//        mandateStatus.setSettlementStatus(SecuritiesSettlementStatusCode.valueOf(mandateRegistrationDTO.getSettlementStatus()));
//        mandateStatus.setCancellationProcessingStatus(CancellationProcessingStatusCode.valueOf(mandateRegistrationDTO.getCancellationProcessingStatus()));
//        mandateStatus.setTransactionProcessingStatus(InstructionProcessingStatusCode.valueOf(mandateRegistrationDTO.getTransactionProcessingStatus()));
//        mandateStatus.setModificationProcessingStatus(ModificationProcessingStatusCode.valueOf(mandateRegistrationDTO.getModificationProcessingStatus()));

        mandateStatus.setPartyRole(null);


        cashAccountMandate.setMandateStatus(Arrays.asList(mandateStatus));

//        cashAccountMandate.setAccountContract(new AccountContract());
        cashAccountMandate.setTrackingDays(mandateRegistrationDTO.getTrackingDays());
        cashAccountMandate.setTrackingIndicator(getBooleanFormValue(mandateRegistrationDTO.getTrackingIndicator()));
        cashAccountMandate.setRate(mandateRegistrationDTO.getRate());


        /**
         * Create the master agreement
         * */
        cashAccountMandate.setMasterAgreement(null);


        /**
         * Create the Agreement
         * */
        cashAccountMandate.setId(GenerateKey.generateEntityId());
//        Date on which the agreement was signed by all parties.
        cashAccountMandate.setDateSigned(null);
//        Full name of an agreement, annexes and amendments in place between the principals.
        cashAccountMandate.setDescription(mandateRegistrationDTO.getDescription());
        cashAccountMandate.setVersion(mandateRegistrationDTO.getVersion());
//        Period during which the agreement is valid
        /**
         * Create validity period for mandate
         * */
        DateTimePeriod dateTimePeriod = new DateTimePeriod();

        LocalDateTime fromDateLocalDate = LocalDateTime.parse(mandateRegistrationDTO.getFromDateTime().replace("T", " "), dateTime_localFomatter);
        OffsetDateTime fromDate = fromDateLocalDate.atOffset(OffsetDateTime.now().getOffset());

        dateTimePeriod.setFromDateTime(fromDate);

        LocalDateTime toDateLocalDate = LocalDateTime.parse(mandateRegistrationDTO.getFromDateTime().replace("T", " "), dateTime_localFomatter);
        OffsetDateTime toDate = toDateLocalDate.atOffset(OffsetDateTime.now().getOffset());

        dateTimePeriod.setToDateTime(toDate);

        cashAccountMandate.setValidityPeriod(Arrays.asList(dateTimePeriod));
//        cashAccountMandate.setDocument(new Document());
        cashAccountMandate.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        cashAccountMandate.setDateCreated(OffsetDateTime.now());
        cashAccountMandate.setLastUpdated(OffsetDateTime.now());
        cashAccountMandate.setEntityVersion(0L);

        /**
         * Save cash account mandate
         * */
        CashAccountMandate savedAccountMandate = cashAccountMandateRepository.save(cashAccountMandate);

        /**
         * Save all owning entities that need a saved cash account mandate
         */

        validityPeriod.setRelatedAgreement(savedAccountMandate);
        validityPeriod = dateTimePeriodRepository.save(validityPeriod);

        cashAccountContract.setCashAccountMandate(savedAccountMandate);
        cashAccountContract = cashAccountContractRepository.save(cashAccountContract);


        /**
         * Saving the signature conditions
         */
        condition.setMandate(savedAccountMandate);
        signatureConditionRepository.save(condition);

        ArrayList<Mandate> mandates = new ArrayList<>();
        mandates.add(savedAccountMandate);
        ArrayList<MandatePartyRole> mandatePartyRoles = new ArrayList<>();

        /**
         * Setting the mandate holder
         */

        List<RolePlayer> holderPlayers = new ArrayList<>();
        Map<String, String> mandateHolderRoleMap = mandateRegistrationDTO.getMandateHolderRolesMap();

        if (mandateRegistrationDTO.getMandateHolderIds() != null) {
            mandateRegistrationDTO.getMandateHolderIds().forEach(id -> {
                clientService.getPersonById(id).ifPresent(holderPlayers::add);
                //mandatePartyRoles.add(mandateHolder);
            });
        }


        holderPlayers.forEach(rolePlayer -> {
            MandateHolder mandateHolder = new MandateHolder();
            mandateHolder.setEntityVersion(0L);
            mandateHolder.setPartyRoleCode(PartyRoleCode.Custodian);
            mandateHolder.setMandate(mandates);
            mandateHolder.setHolderType(MandateHolderType.valueOf(mandateHolderRoleMap.get(rolePlayer.getId())));
            mandateHolder.setPlayer(Arrays.asList(rolePlayer));
            log.info("Mandate Holder Player: " + mandateHolder.getPlayer().size());
            mandatePartyRoles.add(mandateHolder);
        });
        /**
         * Setting the mandate issuer role
         */

        MandateIssuer mandateIssuer = new MandateIssuer();
        List<RolePlayer> issuerPlayers = new ArrayList<>();

        optionalCashAccount.ifPresent(cashAccount -> {
            cashAccount.getPartyRole().forEach(accountPartyRole -> {
                AccountOwnerRole accountOwnerRole;
                if (accountPartyRole instanceof AccountOwnerRole) {
                    accountOwnerRole = (AccountOwnerRole) accountPartyRole;
                    issuerPlayers.add(accountOwnerRole.getPlayer().get(0));  /*Hopefully we have one owner*/
                }
            });
        });
        mandateIssuer.setPlayer(issuerPlayers);
        mandateIssuer.setMandate(mandates);


        mandatePartyRoles.add(mandateIssuer);

        mandatePartyRoleRepository.saveAll(mandatePartyRoles);

        return savedAccountMandate;
    }

    private Boolean getBooleanFormValue(String booleanStringValue) {
        booleanStringValue = booleanStringValue == null ? "false" : booleanStringValue.equalsIgnoreCase("on") ?
                "true" : booleanStringValue.equalsIgnoreCase("true") ? "true" : "false";
        return Boolean.valueOf(booleanStringValue);
    }

    @Override
    public CashAccountMandate updateCashAccountMandate(CashAccountMandate cashAccountMandate) {
        cashAccountMandate.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return cashAccountMandateRepository.save(cashAccountMandate);
    }

    @Override
    public CashAccountMandate deactivateCashAccountMandate(CashAccountMandate cashAccountMandate) {
        cashAccountMandate.setEntityStatus(EntityStatus.INACTIVE);
        return cashAccountMandateRepository.save(cashAccountMandate);
    }

    @Override
    public CashAccountMandate deleteCashAccountMandate(String id, String statusDescription) {
        Optional<CashAccountMandate> cashAccountMandateOptional = cashAccountMandateRepository.findById(id);
        CashAccountMandate cashAccountMandate = null;
        if (cashAccountMandateOptional.isPresent()) {
            cashAccountMandate = cashAccountMandateOptional.get();
            cashAccountMandate.setEntityStatus(EntityStatus.DELETED);


            cashAccountMandate.getMandateStatus().stream().findFirst().ifPresent(mandateStatus -> {
                mandateStatus.setAccepted(false);
                mandateStatus.setStatusDescription(statusDescription);
            });
            cashAccountMandate = cashAccountMandateRepository.save(cashAccountMandate);
        }
        return cashAccountMandate;
    }

    public static void main(String[] args) {
        List<String> mandateHolderRolesTemp = Arrays.asList("M4NE1553762229393857|INITIATE", "QWIU1553863387889245|BOTH", "YDLV1553791282535329|AUTHORISE");
        Map<String, String> mandateHolderRoles = new HashMap<>();
        mandateHolderRolesTemp.forEach(roleTemp -> {
            List<String> role = Arrays.asList(roleTemp.split("\\|"));
            mandateHolderRoles.put(role.get(0), role.get(1));
        });

        System.out.println(mandateHolderRoles);
    }


    @Override
    public CashAccountMandate approveCashAccountMandate(String id) {
        Optional<CashAccountMandate> optionalCashAccountMandate = cashAccountMandateRepository.findById(id);
        CashAccountMandate cashAccountMandate = null;
        if (optionalCashAccountMandate.isPresent()) {
            cashAccountMandate = optionalCashAccountMandate.get();
            if (cashAccountMandate.getEntityStatus() != EntityStatus.DELETED) {
                cashAccountMandate.setEntityStatus(EntityStatus.ACTIVE);
                cashAccountMandate.getMandateStatus().stream().findFirst().ifPresent(mandateStatus -> {
                    mandateStatus.setAccepted(true);
                    mandateStatus.setStatusDescription("");
                });
                cashAccountMandate = cashAccountMandateRepository.save(cashAccountMandate);
            } else {
                log.info("The Signature Condition status has already been deleted.");
            }
        }
        return cashAccountMandate;
    }

    @Override
    public CashAccountMandate rejectCashAccountMandate(String id, String statusDescription) {
        Optional<CashAccountMandate> optionalCashAccountMandate = cashAccountMandateRepository.findById(id);
        CashAccountMandate cashAccountMandate = null;
        if (optionalCashAccountMandate.isPresent()) {
            cashAccountMandate = optionalCashAccountMandate.get();
            if (cashAccountMandate.getEntityStatus() != EntityStatus.DELETED) {
                cashAccountMandate.setEntityStatus(EntityStatus.DISAPPROVED);

                cashAccountMandate.getMandateStatus().stream().findFirst().ifPresent(mandateStatus -> {
                    mandateStatus.setAccepted(false);
                    mandateStatus.setStatusDescription(statusDescription);
                });
                cashAccountMandate = cashAccountMandateRepository.save(cashAccountMandate);
            } else {
                log.info("The Signature Condition status has already been deleted.");
            }
        }
        return cashAccountMandate;
    }

    private Boolean canMandateAuthorise(MandateHolderType holderType) {
        if (holderType.equals(MandateHolderType.AUTHORISE) || holderType.equals(MandateHolderType.BOTH)) {
            return true;
        }
        return false;
    }

    @Override
    public List<MandateHolder> getMandateHolders(String cashAccountId) {
        List<CashAccountContract> cashAccountContracts = cashAccountContractRepository.findAllByCashAccount_Id(cashAccountId);

        List<MandateHolder> mandateHolders = new ArrayList<>();
        log.info("The cash account contracts for cash id {} are {}", cashAccountId, cashAccountContracts.size());
        final List<CashAccountMandate> cashAccountMandates = cashAccountContracts.stream()
                .filter(cashAccountContract -> getIsValidStatus(cashAccountContract.getCashAccountMandate().getEntityStatus()))
                .map(CashAccountContract::getCashAccountMandate)
                .collect(Collectors.toList());

        cashAccountMandates.forEach(cashAccountMandate -> {
            List<MandatePartyRole> mandatePartyRole = cashAccountMandate.getMandatePartyRole();
            mandatePartyRole.forEach(mandatePartyRole1 -> {
                if (mandatePartyRole1 instanceof MandateHolder && canMandateAuthorise(((MandateHolder) mandatePartyRole1).getHolderType())) {
                    MandateHolder mandateHolder = (MandateHolder) mandatePartyRole1;
                    mandateHolders.add(mandateHolder);
                }
            });
        });
        return mandateHolders;
    }

    @Override
    public List<MandateHolder> getAllMandateHolders(String cashAccountId) {
        List<CashAccountContract> cashAccountContracts = cashAccountContractRepository.findAllByCashAccount_Id(cashAccountId);

        List<MandateHolder> mandateHolders = new ArrayList<>();
        log.info("The cash account contracts for cash id {} are {}", cashAccountId, cashAccountContracts.size());
        final List<CashAccountMandate> cashAccountMandates = cashAccountContracts.stream()
                .filter(cashAccountContract -> getIsValidStatus(cashAccountContract.getCashAccountMandate().getEntityStatus()))
                .map(CashAccountContract::getCashAccountMandate)
                .collect(Collectors.toList());

        cashAccountMandates.forEach(cashAccountMandate -> {
            List<MandatePartyRole> mandatePartyRole = cashAccountMandate.getMandatePartyRole();
            mandatePartyRole.forEach(mandatePartyRole1 -> {
                if (mandatePartyRole1 instanceof MandateHolder) {
                    MandateHolder mandateHolder = (MandateHolder) mandatePartyRole1;
                    mandateHolders.add(mandateHolder);
                }
            });
        });
        return mandateHolders;
    }

    @Override
    public List<MandatePartyRole> getInitiators(String cashAccountId) {
        List<CashAccountContract> cashAccountContracts = cashAccountContractRepository.findAllByCashAccount_Id(cashAccountId);

        List<MandatePartyRole> mandateInitiator = new ArrayList<>();
        log.info("The cash account contracts for cash id {} are {}", cashAccountId, cashAccountContracts.size());
        final List<CashAccountMandate> cashAccountMandates = cashAccountContracts.stream()
                .filter(cashAccountContract -> getIsValidStatus(cashAccountContract.getCashAccountMandate().getEntityStatus()))
                .map(CashAccountContract::getCashAccountMandate)
                .filter(cashAccountMandate -> {
                    log.warn("Checking the validity period of cash account mandate ...");
                    return cashAccountMandate.getValidityPeriod().stream()
                            .anyMatch(dateTimePeriod -> dateTimePeriod.getToDateTime().isAfter(OffsetDateTime.now()) && dateTimePeriod.getFromDateTime().isBefore(OffsetDateTime.now()));
                })
                .collect(Collectors.toList());

        cashAccountMandates.forEach(cashAccountMandate -> {
            List<MandatePartyRole> mandatePartyRole = cashAccountMandate.getMandatePartyRole();
            mandatePartyRole.forEach(mandatePartyRole1 -> {

                if (mandatePartyRole1 instanceof MandateHolder && canMandateInitiator(((MandateHolder) mandatePartyRole1).getHolderType())) {
                    mandateInitiator.add(mandatePartyRole1);
                } else if (mandatePartyRole1 instanceof MandateIssuer) {
                    mandateInitiator.add(mandatePartyRole1);
                }
            });
        });
        return mandateInitiator;
    }

    @Override
    public ApprovalResponseDTO getPaymentInitiators(String cashAccountId, String personId) {
        List<CashAccountContract> cashAccountContracts = cashAccountContractRepository.findAllByCashAccount_Id(cashAccountId);

        List<MandatePartyRole> mandateInitiator = new ArrayList<>();

        log.info("The cash account contracts for cash id {} are {}", cashAccountId, cashAccountContracts.size());
        Optional<CashAccountMandate> optionalCashAccountMandate = cashAccountContracts.stream()
                .filter(cashAccountContract -> getIsValidStatus(cashAccountContract.getCashAccountMandate().getEntityStatus()))
                .map(CashAccountContract::getCashAccountMandate)
                .filter(cashAccountMandate -> {
                    log.warn("Checking the validity period of cash account mandate ...");
                    return cashAccountMandate.getValidityPeriod().stream()
                            .anyMatch(dateTimePeriod -> {
                                if (dateTimePeriod.getToDateTime().isAfter(OffsetDateTime.now()) && dateTimePeriod.getFromDateTime().isBefore(OffsetDateTime.now())) {
                                    return true;
                                } else {
                                    deactivateCashAccountMandate(cashAccountMandate);
                                    return false;
                                }
                            });
                })
                .findFirst();
        return optionalCashAccountMandate.map(cashAccountMandate -> {
            List<MandatePartyRole> mandatePartyRole = cashAccountMandate.getMandatePartyRole();
            mandatePartyRole.forEach(mandatePartyRole1 -> {

                if (mandatePartyRole1 instanceof MandateHolder && canMandateInitiator(((MandateHolder) mandatePartyRole1).getHolderType())) {
                    mandateInitiator.add(mandatePartyRole1);
                } else if (mandatePartyRole1 instanceof MandateIssuer) {
                    mandateInitiator.add(mandatePartyRole1);
                }
            });
            List<String> initiatorsIds = mandateInitiator.stream().map(Role::getPlayer)
                    .flatMap(Collection::stream)
                    .map(RolePlayer::getId)
                    .collect(Collectors.toList());

            if (initiatorsIds.contains(personId)) {
                log.info("The person is allowed to initiate payments on this account.");
                return ApprovalResponseDTO.builder()
                        .done(true)
                        .narrative("You are authorised to initiate payment on this account.")
                        .build();
            }
            return ApprovalResponseDTO.builder()
                    .done(false)
                    .narrative("You are not authorised to initiate payment on this account")
                    .build();
        }).orElseGet(() -> {
            log.info("No account mandate found for the person with id {} and cash account id {}", personId, cashAccountId);
            Optional<CashAccount> optionalCashAccount = cashAccountRepository.findById(cashAccountId);
            log.info("Checking if the person initiating is the account owner ...");
            List<String> ownerIds = new ArrayList<>();
            optionalCashAccount.ifPresent(cashAccount -> {
                cashAccount.getPartyRole().forEach(accountPartyRole -> {
                    AccountOwnerRole accountOwnerRole;
                    if (accountPartyRole instanceof AccountOwnerRole) {
                        accountOwnerRole = (AccountOwnerRole) accountPartyRole;
                        ownerIds.add(accountOwnerRole.getPlayer().get(0).getId());  /*Hopefully we have one owner*/
                    }
                });
            });
            if (ownerIds.contains(personId)) {
                log.info("The person is the owner of the account. Can initiate on this account.");
                return ApprovalResponseDTO.builder()
                        .done(true)
                        .narrative("The person is the owner of the account.")
                        .build();
            } else {
                log.warn("The person approving is not the owner of the account hence cannot initiate on this account.");
            }
            return ApprovalResponseDTO.builder()
                    .done(false)
                    .narrative("No valid mandate found for the provided cash account. Please consult administration")
                    .build();
        });
    }

    @Override
    public AccountPartyInfo getAccountPartyInfo(String cashAccountId) {
        Optional<CashAccount> optionalCashAccount = cashAccountRepository.findById(cashAccountId);
        AccountPartyInfo accountPartyInfo = new AccountPartyInfo();
        log.info("Checking the account party information ...");
        optionalCashAccount.ifPresent(cashAccount -> {
            cashAccount.getPartyRole().forEach(accountPartyRole -> {
                AccountOwnerRole accountOwnerRole;
                if (accountPartyRole instanceof AccountOwnerRole) {
                    accountOwnerRole = (AccountOwnerRole) accountPartyRole;
                    RolePlayer rolePlayer = accountOwnerRole.getPlayer().get(0); /*Hopefully we have one owner*/

                    if (rolePlayer instanceof Organisation) {
                        log.info("Account owner is the organisation.");
                        accountPartyInfo.setPartyType("Organisation");
                        Optional<EmployingPartyRole> employingPartyRoleOptional = rolePlayer.getRole().stream()
                                .filter(role -> role instanceof EmployingPartyRole).map(role -> (EmployingPartyRole) role).findFirst();
                        if (employingPartyRoleOptional.isPresent()) {
                            log.info("The organisation employer role is found with id {}", employingPartyRoleOptional.get().getId());
                            accountPartyInfo.setPartyRoleId(employingPartyRoleOptional.get().getId());
                        } else {
                            log.info("The organisation employer role is not found.");
                        }

                    } else if (rolePlayer instanceof Person) {
                        accountPartyInfo.setPartyType("Person");
                        log.info("The Account owner is the person");
                    }
                }
            });
        });
        return accountPartyInfo;
    }

    private Boolean canMandateInitiator(MandateHolderType holderType) {
        if (holderType.equals(MandateHolderType.INITIATE) || holderType.equals(MandateHolderType.BOTH)) {
            return true;
        }
        return false;
    }

    @Override
    public List<MandatePartyRole> getAuthoriseMandateParties(String cashAccountId) {
        List<CashAccountContract> cashAccountContracts = cashAccountContractRepository.findAllByCashAccount_Id(cashAccountId);

        List<MandatePartyRole> mandatePartyRoles = new ArrayList<>();
        log.info("The cash account contracts for cash id {} are {}", cashAccountId, cashAccountContracts.size());
        final List<CashAccountMandate> cashAccountMandates = cashAccountContracts.stream()
                .filter(cashAccountContract -> getIsValidStatus(cashAccountContract.getCashAccountMandate().getEntityStatus()))
                .map(CashAccountContract::getCashAccountMandate)
                .collect(Collectors.toList());

        cashAccountMandates.forEach(cashAccountMandate -> {
            List<MandatePartyRole> mandatePartyRole = cashAccountMandate.getMandatePartyRole();
            mandatePartyRole.forEach(mandatePartyRole1 -> {
                if (mandatePartyRole1 instanceof MandateHolder && canMandateAuthorise(((MandateHolder) mandatePartyRole1).getHolderType())) {
                    mandatePartyRoles.add(mandatePartyRole1);
                } else if (mandatePartyRole1 instanceof MandateIssuer) {
                    mandatePartyRoles.add(mandatePartyRole1);
                }
            });
        });
        return mandatePartyRoles;
    }

    @Override
    public List<RolePlayer> getSignatureForPayment(String paymentId) {
        List<SigningDetail> signingDetails = signingDetailRepository.findByPaymentIdAndDone(paymentId, true);
        if(signingDetails.isEmpty()) {

        }
        List<String> partyIds = signingDetails.stream()
                .map(signingDetail -> signingDetail.getPartyId()).collect(Collectors.toList());
        List<RolePlayer> rolePlayers = rolePlayerRepository.findByIdIn(partyIds);
        return rolePlayers;
    }

    @Override
    public List<RolePlayer> getAllSignatureForSignatureCondition(String paymentId) {
        Optional<SigningDetail> optionalSigningDetail = signingDetailRepository.findFirstByPaymentId(paymentId);
        return optionalSigningDetail.map(signingDetail -> {
            Optional<CashAccountMandate> optionalCashAccountMandate = cashAccountMandateRepository
                    .findFirstBySignatureConditions_Id(signingDetail.getSignatureConditionId());
            if (optionalCashAccountMandate.isPresent()) {
                CashAccountMandate cashAccountMandate = optionalCashAccountMandate.get();
                return cashAccountMandate.getMandatePartyRole().stream().filter(mandatePartyRole1 -> mandatePartyRole1 instanceof MandateHolder && canMandateAuthorise(((MandateHolder) mandatePartyRole1).getHolderType()))
                        .map(mandatePartyRole1 -> (MandateHolder) mandatePartyRole1)
                        .map(mandateHolder -> mandateHolder.getPlayer())
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
            } else {
                return new ArrayList<RolePlayer>();
            }
        }).orElseGet(() -> new ArrayList<RolePlayer>());
    }


    @Override
    public List<CashAccountMandate> getCashAccountMandateByCashAccountId(String cashAccountId) {
        List<CashAccountContract> cashAccountContracts = cashAccountContractRepository.findAllByCashAccount_Id(cashAccountId);

        log.info("The cash account contracts for cash id {} are {}", cashAccountId, cashAccountContracts.size());
        List<CashAccountMandate> accountMandateList = cashAccountContracts.stream()
                .map(CashAccountContract::getCashAccountMandate)
                .collect(Collectors.toList());
        return accountMandateList;
    }

    @Override
    public Optional<CashAccountMandate> checkIfCashAccountHaveValidMandate(String cashAccountId) {
        List<CashAccountContract> cashAccountContracts = cashAccountContractRepository.findAllByCashAccount_Id(cashAccountId);

        log.info("The cash account contracts for cash id {} are {}", cashAccountId, cashAccountContracts.size());
        return cashAccountContracts.stream()
                .filter(cashAccountContract -> getIsValidStatus(cashAccountContract.getCashAccountMandate().getEntityStatus()))
                .map(CashAccountContract::getCashAccountMandate)
                .findFirst();
    }

    private Boolean getIsValidStatus(EntityStatus status) {
        if (status.equals(EntityStatus.DELETED) || status.equals(EntityStatus.DISAPPROVED) || status.equals(EntityStatus.INACTIVE)) {
            return false;
        }
        return true;
    }

    @Override
    public Optional<CashAccountMandate> getCashAccountMandateById(String cashAccountMandateId) {
        return cashAccountMandateRepository.findById(cashAccountMandateId);
    }

    @Override
    public List<CashAccountMandate> getCashAccountMandates() {
        return cashAccountMandateRepository.findAll();
    }
}
