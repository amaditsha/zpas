package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.StatusReason;
import zw.co.esolutions.zpas.repository.StatusReasonRepository;
import zw.co.esolutions.zpas.repository.StatusReasonRepository;

@Slf4j
@Service
public class StatusReasonUtil {
    
    @Autowired
    private StatusReasonRepository investigationCaseStatusRepository;

    @Autowired
    StatusReasonRepository statusReasonRepository;
    
    public void saveOrUpdate(StatusReason statusReason) {
        log.info("Saving the reason with id {}", statusReason.getId());
        
        investigationCaseStatusRepository.save(statusReason);
    }
}
