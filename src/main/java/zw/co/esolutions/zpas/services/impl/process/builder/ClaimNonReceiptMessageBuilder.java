package zw.co.esolutions.zpas.services.impl.process.builder;
import javax.xml.datatype.XMLGregorianCalendar;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt027_001_06.*;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
@Component
public class ClaimNonReceiptMessageBuilder {


    public ClaimNonReceiptV06 buildClaimNonReceiptRequestMsg(PaymentInvestigationCase paymentInvestigationCase) {

        final CaseAssignment4  caseAssignment4 = new CaseAssignment4();
        caseAssignment4.setId(paymentInvestigationCase.getAssignmentIdentification());

        Party35Choice assignerChoice = new Party35Choice();
        assignerChoice.setAgt(this.getBranchAndFinancialInstitutionIdentification5(paymentInvestigationCase, Assigner.class));

        caseAssignment4.setAssgnr(assignerChoice);


        Party35Choice assigneeChoice = new Party35Choice();
        assigneeChoice.setAgt(this.getBranchAndFinancialInstitutionIdentification5(paymentInvestigationCase, Assignee.class));

        caseAssignment4.setAssgne(assigneeChoice);
        caseAssignment4.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        final Party35Choice cretrChoice = new Party35Choice();
        cretrChoice.setPty(this.getPartyIdentification125(paymentInvestigationCase, CaseCreator.class));

        Case4 case5 = new Case4();
        case5.setId(paymentInvestigationCase.getIdentification());
        case5.setCretr(cretrChoice);
        case5.setReopCaseIndctn(false);

        final Optional<Payment> paymentOptional = paymentInvestigationCase.getUnderlyingPayment().stream().findFirst();

        final UnderlyingPaymentInstruction4 underlyingPaymentInstruction4 = new UnderlyingPaymentInstruction4();
        if(paymentOptional.isPresent()) {


            ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount = new ActiveOrHistoricCurrencyAndAmount();
            activeOrHistoricCurrencyAndAmount.setValue(paymentOptional.get().getAmount().getAmount());
            activeOrHistoricCurrencyAndAmount.setCcy(paymentOptional.get().getAmount().getCurrency().getCodeName());

            DateAndDateTime2Choice dateAndDateTime2Choice = new DateAndDateTime2Choice();
            dateAndDateTime2Choice.setDt(XmlDateUtil.getXmlDateNoTime(OffsetDateTime.now()));
//            dateAndDateTime2Choice.setDtTm(XmlDateUtil.getXmlDate(paymentInvestigationCase.getUnderlyingInstruction().getRequestedExecutionDate()));

            UnderlyingGroupInformation1 underlyingGroupInformation1 = new UnderlyingGroupInformation1();
            underlyingGroupInformation1.setOrgnlMsgId(this.getOriginalMessageIdentification(paymentInvestigationCase.getUnderlyingPayment().get(0)));
            underlyingGroupInformation1.setOrgnlMsgNmId("pain.001.001.08");
//            underlyingGroupInformation1.setOrgnlCreDtTm(new XMLGregorianCalendar());
//            underlyingGroupInformation1.setOrgnlMsgDlvryChanl("");

            String messageID = this.getOriginalMessageIdentification(paymentInvestigationCase.getUnderlyingPayment().get(0));

            underlyingPaymentInstruction4.setOrgnlGrpInf(underlyingGroupInformation1);
            underlyingPaymentInstruction4.setOrgnlPmtInfId(messageID);
            underlyingPaymentInstruction4.setOrgnlInstrId(messageID);
            underlyingPaymentInstruction4.setOrgnlEndToEndId(paymentOptional.get().getPaymentRelatedIdentifications().stream().findFirst()
                    .map(PaymentIdentification::getEndToEndIdentification)
                    .orElse(""));
            underlyingPaymentInstruction4.setOrgnlInstdAmt(activeOrHistoricCurrencyAndAmount);
            underlyingPaymentInstruction4.setReqdExctnDt(dateAndDateTime2Choice);
            underlyingPaymentInstruction4.setReqdColltnDt(XmlDateUtil.getXmlDate(OffsetDateTime.now()));
//            underlyingPaymentInstruction4.setOrgnlTxRef(new OriginalTransactionReference28());
        }

        final UnderlyingTransaction4Choice underlyingTransaction5Choice = new UnderlyingTransaction4Choice();
        underlyingTransaction5Choice.setInitn(underlyingPaymentInstruction4);

        ClaimNonReceiptV06 claimNonReceiptV07 = new ClaimNonReceiptV06();
        claimNonReceiptV07.setAssgnmt(caseAssignment4);
        claimNonReceiptV07.setCase(case5);
        claimNonReceiptV07.setUndrlyg(underlyingTransaction5Choice);
//        claimNonReceiptV07.setCoverDtls(new MissingCover4());
//        claimNonReceiptV07.setInstrForAssgne(new InstructionForAssignee1());

        return claimNonReceiptV07;
    }

    public String getOriginalMessageIdentification(Payment payment) {
        if (payment instanceof BulkPayment) {
            return payment.getPaymentRelatedIdentifications().stream().findFirst()
                    .get()
                    .getEndToEndIdentification();
        } else {
            IndividualPayment individualPayment = (IndividualPayment) payment;

            if (individualPayment.getBulkPayment() != null) {
                return individualPayment.getBulkPayment()
                        .getPaymentRelatedIdentifications().stream().findFirst()
                        .get()
                        .getEndToEndIdentification();
            } else {
                return individualPayment.getPaymentRelatedIdentifications().stream().findFirst()
                        .get()
                        .getEndToEndIdentification();
            }
        }
    }

    public Party35Choice getParty35Choice(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        Party35Choice party35Choice = new Party35Choice();
        party35Choice.setPty(getPartyIdentification125(investigationCase, investigationPartyRoleSubclass));
        party35Choice.setAgt(getBranchAndFinancialInstitutionIdentification5(investigationCase, investigationPartyRoleSubclass));
        return party35Choice;

    }


    public PartyIdentification125 getPartyIdentification125(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        PartyIdentification125  partyIdentification125  = new PartyIdentification125 ();

        Optional<Party> partyOptional = this.getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            partyIdentification125.setNm(partyOptional.get().getName());

            partyIdentification125.setId(this.getParty34Choice(investigationCase, investigationPartyRoleSubclass));
        }
        return partyIdentification125 ;
    }


    public Optional<InvestigationPartyRole> getInvestigationPartyRole(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        final Optional<InvestigationPartyRole> investigationPartyRoleOptional = investigationCase.getInvestigationPartyRole().stream()
                .filter(investigationPartyRole -> investigationPartyRoleSubclass.isInstance(investigationPartyRole))
                .findFirst();

        return investigationPartyRoleOptional;
    }


    public Optional<Party> getParty(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {

        final Optional<InvestigationPartyRole> investigationPartyRoleOptional = this.getInvestigationPartyRole(investigationCase, investigationPartyRoleSubclass);

        //if party role present, extract the party
        if (investigationPartyRoleOptional.isPresent()) {

            InvestigationPartyRole investigationPartyRole = investigationPartyRoleOptional.get();

            log.info("Party is present, building the info in the msg..");

            if (investigationPartyRole.isHasCustomParty()) {
                log.info("Getting custom party");
                CustomParty customParty = investigationPartyRole.getCustomParty();

                Party party = new Person();

                PersonIdentification personIdentification = new PersonIdentification();
                personIdentification.setIdentityCardNumber(customParty.getNationalId());
                personIdentification.setPassportNumber(customParty.getPassportNumber());

                PersonName personName = new PersonName();
                personName.setName(customParty.getName());

                personIdentification.setPersonName(Arrays.asList(personName));

                party.setIdentification(Arrays.asList(personIdentification));

                return Optional.of(party);

            } else {
                log.info("Getting registered party");
                log.info("The investigation party role is: {}", investigationPartyRole);
                if(investigationPartyRole.getPlayer() == null) {
                    log.error("The player is null");
                    return Optional.empty();
                }
                Optional<RolePlayer> rolePlayerOptional = investigationPartyRole.getPlayer().stream()
                        .findFirst();

                //if role player present, return the party
                if (rolePlayerOptional.isPresent()) {

                    Party party = (Party) rolePlayerOptional.get();

                    return Optional.of(party);

                }
            }
        }

        return Optional.empty();
    }

    public BranchAndFinancialInstitutionIdentification5 getBranchAndFinancialInstitutionIdentification5(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {

        log.info("Branch & FI identification 5");
        BranchAndFinancialInstitutionIdentification5 branchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();

        FinancialInstitutionIdentification8 financialInstitutionIdentification8 = new FinancialInstitutionIdentification8();

        Optional<Party> partyOptional = this.getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            log.info("Party is present in the optional and class is {}", investigationPartyRoleSubclass);
            if (partyOptional.get() instanceof Organisation) {
                log.info("The party is organisation. Setting the appropriate details.");
                Organisation organisation = (Organisation) partyOptional.get();

                financialInstitutionIdentification8.setBICFI(organisation.getOrganisationIdentification().stream()
                        .map(OrganisationIdentification::getBICFI).findFirst().orElse(""));

                financialInstitutionIdentification8.setNm(organisation.getOrganisationIdentification().stream()
                        .map(OrganisationIdentification::getOrganisationName)
                        .flatMap(organisationNames -> organisationNames.stream())
                        .map(OrganisationName::getShortName)
                        .findFirst().orElse(""));

                branchAndFinancialInstitutionIdentification5.setFinInstnId(financialInstitutionIdentification8);
            } else {
                log.info("The party is not organisation | {}", partyOptional.get().getClass().getName());
            }
        } else {
            log.info("No party is present. Returning empty branch.");
        }
        return branchAndFinancialInstitutionIdentification5;
    }

    /*public String getPartyName(Party party) {
        List<PartyIdentificationInformation> identification = party.getIdentification();
        log.info("Party identification is: {}", identification);
        final Stream<PartyName> partyNameStream = identification.stream()
                .map(partyIdentificationInformation -> Optional.ofNullable(partyIdentificationInformation.getPartyName()).orElse(new ArrayList<>()))
                .flatMap(partyNames -> partyNames.stream());

        if (party instanceof Organisation) {
            return partyNameStream.map(partyName -> {
                OrganisationName organisationName = (OrganisationName) partyName;
                return organisationName.getLegalName() != null ? organisationName.getLegalName() : organisationName.getName();
            }).findFirst().orElse("");
        } else {
            return partyNameStream.map(partyName -> {
                PersonName personName = (PersonName) partyName;
                return personName.getName() == null ?
                        personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName() : personName.getName();
            }).findFirst().orElse("");
        }
    }*/

    public Party34Choice getParty34Choice(InvestigationCase investigationCase,  Class<?> investigationPartyRoleSubclass) {
        Party34Choice  party38Choice  = new Party34Choice();

        Optional<Party> partyOptional = getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            Party party = partyOptional.get();

            if (party instanceof Organisation) {
                Organisation debtorOrganisation = (Organisation) party;

                OrganisationIdentification8 organisationIdentification8 = new OrganisationIdentification8();
                organisationIdentification8.setAnyBIC(debtorOrganisation.getOrganisationIdentification().stream()
                        .map(OrganisationIdentification::getAnyBIC)
                        .findFirst().orElse(""));
                party38Choice.setOrgId(organisationIdentification8);
            } else {
                Person debtorPerson = (Person) party;

                PersonIdentification13 debtorPersonIdentification13 = new PersonIdentification13();
                GenericPersonIdentification1 debtorGenericPersonIdentification1 = new GenericPersonIdentification1();
                debtorGenericPersonIdentification1.setId(debtorPerson.getPersonIdentification().stream()
                        .map(PersonIdentification::getIdentityCardNumber)
                        .findFirst().orElse(""));
//                debtorGenericPersonIdentification1.setSchmeNm(new PersonIdentificationSchemeName1Choice());
//                debtorGenericPersonIdentification1.setIssr("");

                debtorPersonIdentification13.getOthr().add(debtorGenericPersonIdentification1);

                party38Choice.setPrvtId(debtorPersonIdentification13);
            }
        }
        return party38Choice ;
    }

}


