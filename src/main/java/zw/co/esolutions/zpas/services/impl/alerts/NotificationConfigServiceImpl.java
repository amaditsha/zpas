package zw.co.esolutions.zpas.services.impl.alerts;

import zw.co.esolutions.zpas.dto.message.MessageRequestDTO;
import zw.co.esolutions.zpas.dto.message.MessageResponseDTO;
import zw.co.esolutions.zpas.dto.message.NotificationRequestDTO;
import zw.co.esolutions.zpas.model.Notification;

import java.time.OffsetDateTime;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.model.NotificationConfig;
import zw.co.esolutions.zpas.repository.NotificationConfigRepository;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.repo.ProfileRepository;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import zw.co.esolutions.zpas.services.iface.message.MessageService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NotificationConfigServiceImpl implements NotificationConfigService {

    @Autowired
    NotificationConfigRepository notificationConfigRepository;
    @Autowired
    PartyRepository partyRepository;
    @Autowired
    MessageService messageService;
    @Autowired
    NotificationService notificationService;
    @Autowired
    ProfileRepository profileRepository;


    @Override
    public NotificationConfig create(NotificationConfig config) {
        return notificationConfigRepository.save(config);
    }

    @Override
    public NotificationConfig update(NotificationConfig config) {
        config.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return notificationConfigRepository.save(config);
    }

    @Override
    public Optional<NotificationConfig> findById(String id) {
        return notificationConfigRepository.findById(id);
    }

    @Override
    public void delete(String id) {
        notificationConfigRepository.deleteById(id);
    }

    @Override
    public List<NotificationConfig> findAllByPartyId(String partyId) {
        List<NotificationConfig> notificationConfigs = notificationConfigRepository.findAllByParty_id(partyId);
        List<NotificationType> notificationTypes = Arrays.asList(NotificationType.values());
        if (notificationConfigs.isEmpty()) {
            List<NotificationConfig> tempConfigs = new ArrayList<>();
            notificationTypes.forEach(notificationType -> {
                NotificationConfig notificationConfig = new NotificationConfig();
                notificationConfig.setNotificationType(notificationType);
                notificationConfig.setEmail(true);
                notificationConfig.setSms(true);
                notificationConfig.setBell(true);
                notificationConfig.setId(GenerateKey.generateEntityId());
                notificationConfig.setDateCreated(OffsetDateTime.now());
                notificationConfig.setEntityStatus(EntityStatus.PENDING_APPROVAL);
                notificationConfig.setParty(partyRepository.findById(partyId).get());

                tempConfigs.add(notificationConfig);
            });
            notificationConfigRepository.saveAll(tempConfigs);
            log.info("Created {} notification configs for party with id {}", tempConfigs.size(), partyId);
            return tempConfigs;
        } else if(notificationConfigs.size() != notificationTypes.size()) {
            log.info("The notification configs size are not equal to notification types size");
            List<NotificationType> tempNotificationTypes = notificationConfigs.stream()
                    .map(NotificationConfig::getNotificationType).collect(Collectors.toList());

            notificationTypes.forEach(notificationType -> {
                Optional<NotificationType> notificationTypeOptional = tempNotificationTypes.stream().filter(tempNot -> tempNot.equals(notificationType)).findFirst();
                if(!notificationTypeOptional.isPresent()) {
                    NotificationConfig notificationConfig = new NotificationConfig();
                    notificationConfig.setNotificationType(notificationType);
                    notificationConfig.setEmail(true);
                    notificationConfig.setSms(true);
                    notificationConfig.setBell(true);
                    notificationConfig.setId(GenerateKey.generateEntityId());
                    notificationConfig.setDateCreated(OffsetDateTime.now());
                    notificationConfig.setEntityStatus(EntityStatus.PENDING_APPROVAL);
                    notificationConfig.setParty(partyRepository.findById(partyId).get());

                    notificationConfigRepository.save(notificationConfig);
                }
            });
        }
        log.info("Found the notification configs of size {}", notificationConfigs.size());
        return notificationConfigs;
    }

    @Override
    public NotificationConfig findByPartyIdAndNotificationType(String partyId, NotificationType notificationType) {
        return notificationConfigRepository.findFirstByNotificationTypeAndParty_id(notificationType, partyId);
    }

    @Override
    public MessageResponseDTO processAndSendNotifications(NotificationRequestDTO notificationRequestDTO) {
        log.info("Processing the notification type by configuration detail | {}", notificationRequestDTO);
        MessageRequestDTO.MessageRequestDTOBuilder messageRequestDTOBuilder = MessageRequestDTO.builder();
        messageRequestDTOBuilder.recipientName(notificationRequestDTO.getRecipientName());
        messageRequestDTOBuilder.originator(notificationRequestDTO.getOriginator());
        messageRequestDTOBuilder.destination(notificationRequestDTO.getDestination());
        messageRequestDTOBuilder.messageText(notificationRequestDTO.getMessageText());
        messageRequestDTOBuilder.messageReference(notificationRequestDTO.getMessageReference());
        messageRequestDTOBuilder.messageDate(notificationRequestDTO.getMessageDate());
        messageRequestDTOBuilder.attachmentFilePaths(notificationRequestDTO.getAttachmentFilePaths());
        messageRequestDTOBuilder.subject(notificationRequestDTO.getSubject());
        messageRequestDTOBuilder.username(notificationRequestDTO.getUsername());
        messageRequestDTOBuilder.password(notificationRequestDTO.getPassword());
        messageRequestDTOBuilder.messageChannel(notificationRequestDTO.getMessageChannel());

        Optional<Profile> profileOptional = profileRepository.findByPersonId(notificationRequestDTO.getClientId());
        return profileOptional.map(profile -> {
            log.info("Found profile. Sending the email notification for message : {}", notificationRequestDTO.getMessageText());
            String email = profile.getEmail();
            String mobileNumber = profile.getMobileNumber();
            if (notificationRequestDTO.getNotificationType() != null) {
                NotificationConfig notificationConfig = this.findByPartyIdAndNotificationType(notificationRequestDTO.getClientId(), notificationRequestDTO.getNotificationType());

                if (notificationConfig != null) {
                    MessageResponseDTO messageResponseDTO = new MessageResponseDTO();
                    if (notificationConfig.getBell()) {
                        log.info("Sending the bell notification for customer with id {} and payload {}", notificationRequestDTO.getClientId(), notificationRequestDTO.getMessageText());
                        sendSystemNotification(notificationRequestDTO.getClientId(), notificationRequestDTO.getMessageText(), notificationRequestDTO.getNotificationType());
                    }

                    if (notificationConfig.getSms()) {
                        log.info("Sending the sms notification for customer with id {} and payload {}", notificationRequestDTO.getClientId(), notificationRequestDTO.getMessageText());
                        messageRequestDTOBuilder.destination(mobileNumber);
                        messageRequestDTOBuilder.messageChannel(MessageChannel.SMS);
                        messageResponseDTO = messageService.send(messageRequestDTOBuilder.build(), notificationRequestDTO.getClientId());
                    }

                    if (notificationConfig.getEmail()) {
                        log.info("Sending the email notification for customer with id {} and payload {}", notificationRequestDTO.getClientId(), notificationRequestDTO.getMessageText());
                        messageRequestDTOBuilder.destination(email);
                        messageRequestDTOBuilder.messageChannel(MessageChannel.EMAIL);
                        messageResponseDTO = messageService.send(messageRequestDTOBuilder.build(), notificationRequestDTO.getClientId());
                    }
                    return messageResponseDTO;
                }
            }
            return messageService.send(messageRequestDTOBuilder.build(), notificationRequestDTO.getClientId());
        }).orElseGet(() -> {
            log.info("Profile for the client {} was not found. Forwarding the request to send.");
            return messageService.send(messageRequestDTOBuilder.build(), notificationRequestDTO.getClientId());
        });
    }


    private void sendSystemNotification(String clientId, String message, NotificationType notificationType) {
        Notification notification = new Notification();
        log.info("The system notification received with msg {} for client id {}", message, clientId);
        notification.setId(GenerateKey.generateEntityId());
        notification.setEntityStatus(EntityStatus.NONE);
        notification.setMessage(message);
        notification.setRead(false);
        notification.setNotificationType(notificationType);
        notification.setEntityId("");
        notification.setClientId(clientId);
        notification.setGroupId("");
        notificationService.createNotification(notification);
        log.info("The system notification sent.");
    }

}
