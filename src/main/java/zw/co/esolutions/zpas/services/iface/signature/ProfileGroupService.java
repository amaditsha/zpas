package zw.co.esolutions.zpas.services.iface.signature;

import zw.co.esolutions.zpas.model.ProfileGroup;

import java.util.List;
import java.util.Optional;

public interface ProfileGroupService {
    ProfileGroup createProfileGroup(ProfileGroup signingRule);
    ProfileGroup updateProfileGroup(ProfileGroup signingRule);
    ProfileGroup deleteProfileGroup(String id);
    ProfileGroup approveProfileGroup(String id);
    ProfileGroup rejectProfileGroup(String id);
    Optional<ProfileGroup> getProfileGroupById(String signatureId);
    List<ProfileGroup> getProfileGroups();
}
