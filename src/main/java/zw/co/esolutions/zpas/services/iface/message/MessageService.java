package zw.co.esolutions.zpas.services.iface.message;

import zw.co.esolutions.zpas.dto.message.BulkMessageRequestDTO;
import zw.co.esolutions.zpas.dto.message.BulkMessageResponseDTO;
import zw.co.esolutions.zpas.dto.message.MessageRequestDTO;
import zw.co.esolutions.zpas.dto.message.MessageResponseDTO;

public interface MessageService {
    MessageResponseDTO send(MessageRequestDTO messageRequestDTO, String customerId);
    BulkMessageResponseDTO sendBulk(BulkMessageRequestDTO messageRequestDTO);
    BulkMessageResponseDTO sendBulkByPolling(BulkMessageRequestDTO messageRequestDTO);
}
