package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.enums.CaseStatusCode;
import zw.co.esolutions.zpas.enums.InstructionProcessingStatusCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.InvestigationCaseStatusRepository;
import zw.co.esolutions.zpas.repository.StatusReasonRepository;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

@Slf4j
@Service
public class InvestigationCaseStatusUtil {

    @Autowired
    private InvestigationCaseStatusRepository investigationCaseStatusRepository;

    @Autowired
    StatusReasonRepository statusReasonRepository;

    public void saveOrUpdate(InvestigationCaseStatus investigationCaseStatus) {
        log.info("Saving the investigation with id {}", investigationCaseStatus.getId());

        investigationCaseStatusRepository.save(investigationCaseStatus);
    }

    public void saveCaseStatus(InvestigationCase investigationCase, CaseStatusCode caseStatusCode, String desc) {
        /**
         * Save the investigation status
         * */
        InvestigationCaseStatus investigationCaseStatus = new InvestigationCaseStatus();
        investigationCaseStatus.setCaseStatus(caseStatusCode);
        investigationCaseStatus.setInvestigationCase(investigationCase);
        investigationCaseStatus.setId(UUID.randomUUID().toString());
        investigationCaseStatus.setEntityStatus(EntityStatus.ACTIVE);
        investigationCaseStatus.setStatusDateTime(OffsetDateTime.now());
//        investigationCaseStatus.setValidityTime(new DateTimePeriod());
        investigationCaseStatus.setStatusDescription(desc);
        investigationCaseStatus.setTransactionProcessingStatus(InstructionProcessingStatusCode.None);
        //investigationCaseStatus.setPartyRole(new InvestigationPartyRole());

        investigationCaseStatus = investigationCaseStatusRepository.save(investigationCaseStatus);

        StatusReason statusReason = new StatusReason();
        statusReason.setId(UUID.randomUUID().toString());
        statusReason.setEntityStatus(EntityStatus.ACTIVE);
        statusReason.setStatus(investigationCaseStatus);
        statusReason.setReason(desc);
//        statusReason.setAdditionalInfo(new ArrayList<>());

        statusReasonRepository.save(statusReason);
    }
}
