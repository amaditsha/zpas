package zw.co.esolutions.zpas.services.impl.payments.zimra.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by alfred on 21 August 2019
 */
public class AssessmentValidationGetRequest {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "year")
    private String year;

    @XmlElement(name = "assNo")
    private String assNo;

    @XmlElement(name = "office")
    private String office;

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAssNo() {
        return assNo;
    }

    public void setAssNo(String assNo) {
        this.assNo = assNo;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }
}
