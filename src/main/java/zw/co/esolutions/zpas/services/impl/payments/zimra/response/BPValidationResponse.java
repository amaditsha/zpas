package zw.co.esolutions.zpas.services.impl.payments.zimra.response;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alfred on 21 August 2019
 */
@XmlRootElement(name = "ValidatewithBPNumberResponse")
public class BPValidationResponse {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "GetCompanyDetailsResponse")
    private GetCompanyDetailsResponse getCompanyDetailsResponse;


    // Getter Methods

    public GetCompanyDetailsResponse getGetCompanyDetailsResponse() {
        return getCompanyDetailsResponse;
    }

    public String getXmlns() {
        return xmlns;
    }

    // Setter Methods

    public void setGetCompanyDetailsResponse(GetCompanyDetailsResponse getCompanyDetailsResponse) {
        this.getCompanyDetailsResponse = getCompanyDetailsResponse;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}
