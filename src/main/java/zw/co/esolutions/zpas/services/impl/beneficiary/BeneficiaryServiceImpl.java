package zw.co.esolutions.zpas.services.impl.beneficiary;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Beneficiary;
import zw.co.esolutions.zpas.repository.BeneficiaryRepository;
import zw.co.esolutions.zpas.services.iface.beneficiary.BeneficiaryService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Slf4j
@Service
public class BeneficiaryServiceImpl implements BeneficiaryService {
    @Autowired
    BeneficiaryRepository beneficiaryRepository;
    
    @Override
    public Beneficiary createBeneficiary(Beneficiary beneficiary) {
        beneficiary.setId(GenerateKey.generateEntityId());
        beneficiary.setDateCreated(OffsetDateTime.now());
        beneficiary.setLastUpdated(OffsetDateTime.now());
        beneficiary.setEntityVersion(0L);
        beneficiary.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        return beneficiaryRepository.save(beneficiary);
    }

    @Override
    public Beneficiary updateBeneficiary(Beneficiary beneficiary) {
        beneficiary.setLastUpdated(OffsetDateTime.now());
        beneficiary.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return beneficiaryRepository.save(beneficiary);
    }

    @Override
    public Beneficiary deleteBeneficiary(String id) {
        Optional<Beneficiary> beneficiaryOptional = beneficiaryRepository.findById(id);
        Beneficiary beneficiary = null;

        if(beneficiaryOptional.isPresent()){
            beneficiary = beneficiaryOptional.get();
            beneficiary.setLastUpdated(OffsetDateTime.now());
            beneficiary.setEntityStatus(EntityStatus.DELETED);
            beneficiary = beneficiaryRepository.save(beneficiary);
        }else{
            log.info("Beneficiary has already been deleted");
        }
        return beneficiary;
    }

    @Override
    public Beneficiary approveBeneficiary(String id) {
        Optional<Beneficiary> optionalBeneficiary = beneficiaryRepository.findById(id);
        Beneficiary beneficiary = null;
        if (optionalBeneficiary.isPresent()){
            beneficiary = optionalBeneficiary.get();
            if(beneficiary.getEntityStatus()!= EntityStatus.DELETED){
                beneficiary.setEntityStatus(EntityStatus.ACTIVE);
                beneficiary = beneficiaryRepository.save(beneficiary);
            }else{
                log.info("Beneficiary has already been approved");
            }
        }
        return beneficiary;
    }

    @Override
    public Beneficiary rejectBeneficiary(String id) {
        Optional<Beneficiary> optionalBeneficiary = beneficiaryRepository.findById(id);
        Beneficiary beneficiary = null;
        if (optionalBeneficiary.isPresent()) {
            beneficiary = optionalBeneficiary.get();
            if(beneficiary.getEntityStatus() != EntityStatus.DELETED) {
                beneficiary.setEntityStatus(EntityStatus.DISAPPROVED);
                beneficiary = beneficiaryRepository.save(beneficiary);
            } else {
                log.info("Beneficiary "+ id + "has already been rejected");
            }
        }
        return beneficiary;
    }

    @Override
    public Optional<Beneficiary> getBeneficiary(String id) {
        beneficiaryRepository.findById(id).ifPresent(beneficiary -> {

        });
        return beneficiaryRepository.findById(id);
    }

    @Override
    public List<Beneficiary> getBeneficiaries() {
        return beneficiaryRepository.findAll();
    }
}
