package zw.co.esolutions.zpas.services.iface.process;

import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

public interface PaymentInitiationService {

    ServiceResponse initiateCustomerCreditTransfer(Payment payment);
    ServiceResponse initiateCustomerDirectDebit(Payment payment);

}
