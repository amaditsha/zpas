package zw.co.esolutions.zpas.services.impl.process;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.iso.msg.camt060_001_04.AccountReportingRequestV04;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.iso.msg.camt060_001_04.Document;
import zw.co.esolutions.zpas.repository.MessageRepository;
import zw.co.esolutions.zpas.services.iface.process.AccountInfoInitiationService;
import zw.co.esolutions.zpas.services.impl.process.builder.AccountReportingRequestMessageBuilder;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.*;

@Slf4j
@Service
public class AccountInfoInitiationServiceImpl implements AccountInfoInitiationService {

    @Autowired
    AccountReportingRequestMessageBuilder accountReportingRequestMessageBuilder;

    @Autowired MessageProducer messageProducer;
    @Autowired MessageRepository messageRepository;

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt060_001_04");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ServiceResponse initiateAccountInfoRequest(List<CashAccount> cashAccounts, String reqdMsgNameId) {
        try {
            log.info("Handling Account Info Initiation request");

            Map<String, List<CashAccount>> accountsByBICFI = new HashMap<>();

            /**
             * Group accounts by their FIs
             */
            for (CashAccount cashAccount: cashAccounts) {
                final FinancialInstitution financialInstitution = (FinancialInstitution) cashAccount.getPartyRole().stream()
                        .filter(accountPartyRole -> accountPartyRole instanceof AccountServicerRole)
                        .flatMap(accountPartyRole -> accountPartyRole.getPlayer().stream())
                        .findFirst()
                        .get();

                final String bicfi = financialInstitution.getOrganisationIdentification().get(0).getBICFI();

                accountsByBICFI.compute(bicfi, (key, cashAccounts2) -> {
                    log.info("bicfi: " + bicfi + " cashAccount: " + cashAccount);
                    if(cashAccounts2 == null){
                        cashAccounts2 = new ArrayList<>();
                    }
                    cashAccounts2.add(cashAccount);
                    accountsByBICFI.put(key, cashAccounts2);

                    return cashAccounts2;
                });

            }

            for (String bicfi: accountsByBICFI.keySet()) {
                //build the message
                AccountReportingRequestV04 accountReportingRequestV04 = accountReportingRequestMessageBuilder
                        .buildAccountReportingRequestMsg(accountsByBICFI.get(bicfi), reqdMsgNameId);

                //send the message to Hub
                messageProducer.sendMessageToHub(buildXml(accountReportingRequestV04), bicfi);
            }

            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_SUCCESS)
                    .narrative("Account Info Request initiated successfully")
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .narrative("Account Info Request initiation failed : " + e.getMessage())
                    .build();
        }
    }

    private String buildXml(AccountReportingRequestV04 accountReportingRequestV04) throws JAXBException {
        log.info("Building camt.060 XML message..");

        Document document = new Document();
        document.setAcctRptgReq(accountReportingRequestV04);

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, xmlEncoding);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);

        return xml;
    }

    /**
     * when you receive msg(camt052 or camt053), store msg, create notification and publish an event.
     * uuid, original msg id, payload, dateCreated
     * store message repository
     * creating a notification is actually publishing the event
     */
}
