package zw.co.esolutions.zpas.services.iface.process;

import zw.co.esolutions.zpas.services.impl.process.pojo.DebitAuthorisationDTO;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

public interface DebitAuthorisationResponseInitiationService {
    ServiceResponse initiateDebitAuthorisationResponse(DebitAuthorisationDTO debitAuthorisationDTO);
}
