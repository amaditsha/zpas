package zw.co.esolutions.zpas.services.iface.alerts;

import zw.co.esolutions.zpas.dto.message.MessageResponseDTO;
import zw.co.esolutions.zpas.dto.message.NotificationRequestDTO;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.model.NotificationConfig;

import java.util.List;
import java.util.Optional;

public interface NotificationConfigService {
    NotificationConfig create(NotificationConfig config);
    NotificationConfig update(NotificationConfig config);
    Optional<NotificationConfig> findById(String id);
    void delete(String id);
    List<NotificationConfig> findAllByPartyId(String partyId);
    NotificationConfig findByPartyIdAndNotificationType(String partyId, NotificationType notificationType);
    MessageResponseDTO processAndSendNotifications(NotificationRequestDTO notificationRequestDTO);
}
