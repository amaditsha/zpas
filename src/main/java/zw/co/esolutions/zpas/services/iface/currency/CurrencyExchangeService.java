package zw.co.esolutions.zpas.services.iface.currency;

import zw.co.esolutions.zpas.model.CurrencyExchange;

import java.util.Optional;

/**
 * Created by petros on 14 Feb 2019
 */
public interface CurrencyExchangeService {
    CurrencyExchange createCurrencyExchange(CurrencyExchange currencyExchange);

    CurrencyExchange deleteCurrencyExchange(String id);

    Optional<CurrencyExchange> findCurrencyExchangeById(String id);

    CurrencyExchange updateCurrencyExchange(CurrencyExchange currencyExchange);
}

