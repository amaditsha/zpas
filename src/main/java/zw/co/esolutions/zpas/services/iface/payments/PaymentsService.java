package zw.co.esolutions.zpas.services.iface.payments;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import zw.co.esolutions.zpas.dto.payments.*;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.model.IndividualPayment;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 28 Feb 2019
 */
public interface PaymentsService {
    Optional<Payment> getPaymentById(String id);
    Optional<Payment> getPaymentByEndToEndId(String id);
    Optional<PaymentsStatusDTO> getPaymentsStatusesByClientId(String clientId);
    Optional<IndividualPayment> makePayment(MakePaymentDTO makePaymentDTO) throws PaymentRequestInvalidArgumentException;
    List<Payment> getPaymentsByClientId(String clientId);
    Page<IndividualPayment> getPaginatedIndividualPaymentsByBulkPaymentId(String bulkPaymentId, Pageable pageable);
    List<Payment> getLatestPaymentsByClientId(String clientId);
    List<Payment> searchPayments(SearchDTO searchDTO);
    List<CustomPartyDTO> getPaymentBeneficiariesInfoByPaymentIds(List<String> paymentIds);
    Optional<Payment> submitPayment(String paymentId);
    void sendIdentificationRequest(String paymentId) throws PaymentRequestInvalidArgumentException;
    Optional<Payment> approvePayment(String paymentId);
    void updateBulkPaymentStatuses(String bulkId);
    Optional<Payment> rejectPayment(String paymentId);
    Optional<Payment> deletePayment(String paymentId);

    Optional<Payment> resolvePayment(ResolvePaymentDTO resolvePaymentDTO);
    List<Payment> getPaymentsByStatusCode(PaymentStatusCode paymentStatusCode, String bulkPaymentId);
    List<Payment> getPaymentsByEntityStatus(EntityStatus entityStatus, String clientId);
}
