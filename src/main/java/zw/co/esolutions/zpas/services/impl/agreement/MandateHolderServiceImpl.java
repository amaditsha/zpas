package zw.co.esolutions.zpas.services.impl.agreement;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.agreement.MandateHolderInfo;
import zw.co.esolutions.zpas.enums.MandateHolderType;
import zw.co.esolutions.zpas.enums.PartyRoleCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.MandateHolderRepository;
import zw.co.esolutions.zpas.repository.RolePlayerRepository;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountMandateService;
import zw.co.esolutions.zpas.services.iface.agreement.MandateHolderService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MandateHolderServiceImpl implements MandateHolderService {

    @Autowired
    private MandateHolderRepository mandateHolderRepository;

    @Autowired
    private RolePlayerRepository rolePlayerRepository;

    @Autowired
    private CashAccountMandateService cashAccountMandateService;


    @Override
    public List<RolePlayer> getMandateHolderRoles(String partyId) {
        List<MandateHolder> mandateHolders = mandateHolderRepository.findAllByPlayer_Id(partyId);
        log.info("The mandate holders which are found for mandate holder {} is {}", partyId, mandateHolders.size());

        List<RolePlayer> rolePlayers = mandateHolders.stream()
                .map(MandatePartyRole::getMandate)
                .flatMap(Collection::stream)
                .filter(mandate -> mandate instanceof CashAccountMandate)
                .map(mandate -> (CashAccountMandate) mandate)
                .filter(cashAccountMandate -> getIsValidStatus(cashAccountMandate.getEntityStatus()))
                .map(Mandate::getMandatePartyRole)
                .flatMap(Collection::stream)
                .filter(mandatePartyRole -> mandatePartyRole instanceof MandateIssuer)
                .map(Role::getPlayer)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        log.info("The mandate issuers size for the the party with id {} are {}", partyId, rolePlayers.size());

        return rolePlayers;
    }

    private Boolean getIsValidStatus(EntityStatus status) {
        if (status.equals(EntityStatus.DELETED) || status.equals(EntityStatus.DISAPPROVED) || status.equals(EntityStatus.INACTIVE)) {
            return false;
        }
        return true;
    }

    @Override
    public MandateHolder saveMandateHolder(MandateHolderInfo mandateHolderInfo) {
        log.info("Saving the mandate holder | {}", mandateHolderInfo);

        return rolePlayerRepository.findById(mandateHolderInfo.getRolePlayerId()).map(rolePlayer -> {
            MandateHolder mandateHolder = new MandateHolder();
            mandateHolder.setPartyRoleCode(PartyRoleCode.Custodian);
            mandateHolder.setHolderType(MandateHolderType.valueOf(mandateHolderInfo.getMandateHolderType()));
            mandateHolder.setPlayer(Arrays.asList(rolePlayer));

            Optional<CashAccountMandate> accountMandateOptional = cashAccountMandateService
                    .getCashAccountMandateById(mandateHolderInfo.getCashAccountMandateId());
            List<Mandate> mandates = Arrays.asList(accountMandateOptional.orElse(null));
            mandateHolder.setMandate(mandates);
            log.info("Creating a new mandate holder ...");
            return mandateHolderRepository.save(mandateHolder);

        }).orElseGet(() -> {
            log.info("Failed to get the role player");
            return null;
        });
    }

    @Override
    public void deleteMandateHolder(String id) {
        log.info("Deleting the mandate holder given by id {}", id);
        mandateHolderRepository.deleteById(id);
    }

}
