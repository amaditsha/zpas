package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt056_001_07.*;
import zw.co.esolutions.zpas.model.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@Slf4j
public class InvestigationParty_056RoleUtil {

    public PartyIdentification125 getPartyIdentification125(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        PartyIdentification125  partyIdentification125  = new PartyIdentification125 ();

        Optional<Party> partyOptional = this.getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            partyIdentification125.setNm(this.getPartyName(partyOptional.get()));

            partyIdentification125.setId(this.getParty34Choice(investigationCase, investigationPartyRoleSubclass));
        }
        return partyIdentification125 ;
    }

    public BranchAndFinancialInstitutionIdentification5 getBranchAndFinancialInstitutionIdentification5 (InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {

        BranchAndFinancialInstitutionIdentification5  branchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5 ();

        FinancialInstitutionIdentification8 financialInstitutionIdentification8  = new FinancialInstitutionIdentification8 ();

        Optional<Party> partyOptional = this.getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            Organisation organisation = (Organisation) partyOptional.get();

            financialInstitutionIdentification8 .setBICFI(organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getBICFI).findFirst().orElse(""));

            financialInstitutionIdentification8 .setNm(organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getOrganisationName)
                    .flatMap(organisationNames -> organisationNames.stream())
                    .map(OrganisationName::getShortName)
                    .findFirst().orElse(""));

            branchAndFinancialInstitutionIdentification5.setFinInstnId(financialInstitutionIdentification8 );
        }
        return branchAndFinancialInstitutionIdentification5;
    }

    public Optional<InvestigationPartyRole> getInvestigationPartyRole(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        final Optional<InvestigationPartyRole> investigationPartyRoleOptional = investigationCase.getInvestigationPartyRole().stream()
                .filter(investigationPartyRole -> investigationPartyRoleSubclass.isInstance(investigationPartyRole))
                .findFirst();

        return investigationPartyRoleOptional;
    }

    public Optional<Party> getParty(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {

        final Optional<InvestigationPartyRole> investigationPartyRoleOptional = this.getInvestigationPartyRole(investigationCase, investigationPartyRoleSubclass);

        //if party role present, extract the party
        if (investigationPartyRoleOptional.isPresent()) {

            InvestigationPartyRole investigationPartyRole = investigationPartyRoleOptional.get();

            log.info("Party is present, building the info in the msg..");

            if (investigationPartyRole.isHasCustomParty()) {
                log.info("Getting custom party");
                CustomParty customParty = investigationPartyRole.getCustomParty();

                Party party = new Person();

                PersonIdentification personIdentification = new PersonIdentification();
                personIdentification.setIdentityCardNumber(customParty.getNationalId());
                personIdentification.setPassportNumber(customParty.getPassportNumber());

                PersonName personName = new PersonName();
                personName.setName(customParty.getName());

                personIdentification.setPersonName(Arrays.asList(personName));

                party.setIdentification(Arrays.asList(personIdentification));

                return Optional.of(party);

            } else {
                log.info("Getting registered party");
                Optional<RolePlayer> rolePlayerOptional = investigationPartyRole.getPlayer().stream()
                        .findFirst();

                //if role player present, return the party
                if (rolePlayerOptional.isPresent()) {

                    Party party = (Party) rolePlayerOptional.get();

                    return Optional.of(party);

                }
            }
        }

        return Optional.empty();
    }

    public Party35Choice getParty35Choice(InvestigationCase investigationCase,  Class<?> investigationPartyRoleSubclass) {
        Party35Choice  party35Choice  = new Party35Choice ();

        party35Choice.setPty(this.getPartyIdentification125(investigationCase, investigationPartyRoleSubclass));
        party35Choice.setAgt(this.getBranchAndFinancialInstitutionIdentification5(investigationCase, investigationPartyRoleSubclass));

        return party35Choice ;
    }
    public Party34Choice getParty34Choice(InvestigationCase investigationCase,  Class<?> investigationPartyRoleSubclass) {
        Party34Choice  party34Choice  = new Party34Choice ();

        Optional<Party> partyOptional = getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            Party party = partyOptional.get();

            if (party instanceof Organisation) {
                Organisation debtorOrganisation = (Organisation) party;

                OrganisationIdentification8 organisationIdentification8 = new OrganisationIdentification8();
                organisationIdentification8.setAnyBIC(debtorOrganisation.getOrganisationIdentification().stream()
                        .map(OrganisationIdentification::getAnyBIC)
                        .findFirst().orElse(""));
                party34Choice.setOrgId(organisationIdentification8);
            } else {
                Person debtorPerson = (Person) party;

                PersonIdentification13 debtorPersonIdentification13 = new PersonIdentification13();
                GenericPersonIdentification1 debtorGenericPersonIdentification1 = new GenericPersonIdentification1();
                debtorGenericPersonIdentification1.setId(debtorPerson.getPersonIdentification().stream()
                        .map(PersonIdentification::getIdentityCardNumber)
                        .findFirst().orElse(""));
                debtorGenericPersonIdentification1.setSchmeNm(new PersonIdentificationSchemeName1Choice());
                debtorGenericPersonIdentification1.setIssr("");

                debtorPersonIdentification13.getOthr().add(debtorGenericPersonIdentification1);

                party34Choice.setPrvtId(debtorPersonIdentification13);
            }
        }
        return party34Choice ;
    }

    public String getPartyName(Party party) {
        final Stream<PartyName> partyNameStream = party.getIdentification().stream()
                .map(PartyIdentificationInformation::getPartyName)
                .flatMap(partyNames -> partyNames.stream());

        if (party instanceof Organisation) {
            return partyNameStream.map(partyName -> {
                OrganisationName organisationName = (OrganisationName) partyName;
                return organisationName.getLegalName() != null ? organisationName.getLegalName() : organisationName.getName();
            }).findFirst().orElse("");
        } else {
            return partyNameStream.map(partyName -> {
                PersonName personName = (PersonName) partyName;
                return personName.getName() == null ?
                        personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName() : personName.getName();
            }).findFirst().orElse("");
        }
    }

    public Integer getNumberOfTxns(InvestigationCase investigationCase) {
        Integer size = 0;
        PaymentInvestigationCase paymentInvestigationCase = (PaymentInvestigationCase) investigationCase;
        try {
            size = paymentInvestigationCase.getUnderlyingPayment().size();
        }catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        return  size;
    }

    public BigDecimal getCtrlSum(InvestigationCase investigationCase) {
        PaymentInvestigationCase paymentInvestigationCase = (PaymentInvestigationCase) investigationCase;
        final BigDecimal controlSum = paymentInvestigationCase.getUnderlyingPayment().stream()
                .map(individualPayment -> individualPayment.getAmount().getAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return  controlSum;
    }

}
