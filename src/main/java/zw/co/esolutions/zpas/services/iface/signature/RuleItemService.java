package zw.co.esolutions.zpas.services.iface.signature;

import zw.co.esolutions.zpas.model.RuleItem;

import java.util.List;
import java.util.Optional;

public interface RuleItemService {
    List<RuleItem> createRuleItems(List<RuleItem> ruleItems);
    RuleItem createRuleItem(RuleItem signingRule);
    RuleItem updateRuleItem(RuleItem signingRule);
    RuleItem deleteRuleItem(String id);
    Optional<RuleItem> getRuleItemById(String signatureId);
    List<RuleItem> getRuleItems();
}
