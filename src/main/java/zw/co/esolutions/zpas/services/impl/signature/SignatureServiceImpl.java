package zw.co.esolutions.zpas.services.impl.signature;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Signature;
import zw.co.esolutions.zpas.repository.SignatureRepository;
import zw.co.esolutions.zpas.services.iface.signature.SignatureService;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SignatureServiceImpl implements SignatureService {

    @Autowired
    private SignatureRepository signatureRepository;


    @Override
    public Optional<Signature> getSignatureById(String signatureId) {
        return this.signatureRepository.findById(signatureId);
    }

    @Override
    public List<Signature> getSignatures() {
        return signatureRepository.findAll();
    }

}