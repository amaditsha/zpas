package zw.co.esolutions.zpas.services.impl.payments.cache;

import com.google.common.base.Preconditions;
import com.hazelcast.core.IMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.model.BulkPayment;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.sequence.GenerationNumber;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.repository.sequence.GenerationNumberRepository;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.ScheduledService;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.domain.PaymentStatusInfo;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by alfred on 05 July 2019.
 */
@Slf4j
@Component
public class GenerationNumberCacheProcessor {
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");

    @Value("${GENERATION.SEQUENCE.PERSIST.AFTER}")
    private Integer generationSequencePersistAfter;

    @Autowired
    private GenerationNumberSequenceCache generationNumberSequenceCache;

    @Autowired
    private GenerationNumberRepository generationNumberRepository;

    @Inject
    public GenerationNumberCacheProcessor(GenerationNumberSequenceCache generationNumberSequenceCache, GenerationNumberRepository generationNumberRepository) {
        this.generationNumberSequenceCache = generationNumberSequenceCache;
        this.generationNumberRepository = generationNumberRepository;
    }

    /**
     * The key of the map is a String formatted as CustomerCode-UserCode
     *
     * @param customerCode
     * @param userCode
     * @return BigDecimal
     */
    public BigDecimal getGenerationNumber(String customerCode, String userCode, OffsetDateTime sequenceDate) {
        final String mapKey = getMapKey(customerCode, userCode, sequenceDate);
        final BigDecimal currentGenerationNumber = getGenerationNumberMap()
                .getOrDefault(mapKey, cacheGenerationNumber(customerCode, userCode, sequenceDate));
        incrementGenerationNumber(customerCode, userCode, sequenceDate);
        return currentGenerationNumber;
    }

    private void incrementGenerationNumber(String customerCode, String userCode, OffsetDateTime sequenceDate) {
        final String mapKey = getMapKey(customerCode, userCode, sequenceDate);
        final BigDecimal bigDecimal = generationNumberSequenceCache.getGenerationNumberCache()
                .computeIfPresent(mapKey, (key, value) -> value.add(new BigDecimal(1)));
        if(bigDecimal.longValue() % generationSequencePersistAfter == 0) {
            log.info("Saving Generation number snapshot.");
            persistGenerationNumber(customerCode, userCode, sequenceDate);
        }
    }

    public BigDecimal persistGenerationNumber(String customerCode, String userCode, OffsetDateTime sequenceDate) {
        final String mapKey = getMapKey(customerCode, userCode, sequenceDate);
        final BigDecimal bigDecimal = generationNumberSequenceCache.getGenerationNumberCache().getOrDefault(mapKey, new BigDecimal(1));
        generationNumberRepository.findByCustomerCodeAndUserCodeAndSequenceDate(customerCode, userCode, sequenceDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                .ifPresent(generationNumber -> {
                    generationNumber.setNumber(bigDecimal.longValue());
                    generationNumberRepository.save(generationNumber);
                });
        return bigDecimal;
    }

    private String getMapKey(String customerCode, String userCode, OffsetDateTime sequenceDate) {
        return String.format("%s-%s-%s", FORMATTER.format(sequenceDate), customerCode, userCode);
    }

    public IMap<String, BigDecimal> getGenerationNumberMap() {
        return generationNumberSequenceCache.getGenerationNumberCache();
    }

    private BigDecimal cacheGenerationNumber(String customerCode, String userCode, OffsetDateTime sequenceDate) {
        final String mapKey = getMapKey(customerCode, userCode, sequenceDate);
        final String formattedSequenceDate = sequenceDate.format(FORMATTER);
        BigDecimal generationNumber = getGenerationNumberMap().computeIfAbsent(mapKey, key -> {
            log.info("Initialising generation number cache -> {}", key);
            final Optional<GenerationNumber> generationNumberOptional = generationNumberRepository.findByCustomerCodeAndUserCodeAndSequenceDate(customerCode, userCode, formattedSequenceDate);
            if(generationNumberOptional.isPresent()) {
                return new BigDecimal(generationNumberOptional.get().getNumber());
            } else {
                GenerationNumber generationNumberObject = new GenerationNumber();
                generationNumberObject.setCustomerCode(customerCode);
                generationNumberObject.setUserCode(userCode);
                generationNumberObject.setDateCreated(OffsetDateTime.now());
                generationNumberObject.setLastUpdated(OffsetDateTime.now());
                generationNumberObject.setEntityStatus(EntityStatus.ACTIVE);
                generationNumberObject.setId(UUID.randomUUID().toString());
                generationNumberObject.setNumber(1L);
                generationNumberObject.setSequenceDate(formattedSequenceDate);
                generationNumberRepository.save(generationNumberObject);
                return new BigDecimal(1);
            }
        });

        Preconditions.checkNotNull(generationNumber, "Failed to initialize generationNumber cache");
        return generationNumber;
    }

}
