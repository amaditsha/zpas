package zw.co.esolutions.zpas.services.iface.process;

import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

public interface WithdrawBulkPaymentService {
    ServiceResponse withdrawBulkPayment();
}
