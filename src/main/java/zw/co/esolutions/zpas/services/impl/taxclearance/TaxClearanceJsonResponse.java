package zw.co.esolutions.zpas.services.impl.taxclearance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by alfred on 23 May 2019
 */
@Data
@Builder(toBuilder = true)
public class TaxClearanceJsonResponse implements Serializable {
    private String bpn;
    private String startDate;
    private boolean exists;
    private boolean verificationCompleted;
    private String expiryDate;
    private String companyName;
    private TaxClearanceJsonRequest request;

    public TaxClearanceJsonResponse(
            @JsonProperty("bpn") String bpn,
            @JsonProperty("startDate") String startDate,
            @JsonProperty("exists") boolean exists,
            @JsonProperty("verificationCompleted") boolean verificationCompleted,
            @JsonProperty("expiryDate") String expiryDate,
            @JsonProperty("companyName") String companyName,
            @JsonProperty("request") TaxClearanceJsonRequest request) {
        this.bpn = bpn;
        this.startDate = startDate;
        this.exists = exists;
        this.verificationCompleted = verificationCompleted;
        this.expiryDate = expiryDate;
        this.companyName = companyName;
        this.request = request;
    }
}
