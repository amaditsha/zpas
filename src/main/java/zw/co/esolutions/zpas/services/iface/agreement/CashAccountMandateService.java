package zw.co.esolutions.zpas.services.iface.agreement;

import zw.co.esolutions.zpas.dto.account.AccountPartyInfo;
import zw.co.esolutions.zpas.dto.agreement.ApprovalResponseDTO;
import zw.co.esolutions.zpas.dto.agreement.MandateRegistrationDTO;
import zw.co.esolutions.zpas.model.CashAccountMandate;
import zw.co.esolutions.zpas.model.MandateHolder;
import zw.co.esolutions.zpas.model.MandatePartyRole;
import zw.co.esolutions.zpas.model.RolePlayer;

import java.util.List;
import java.util.Optional;

public interface CashAccountMandateService {
    CashAccountMandate createCashAccountMandate(MandateRegistrationDTO mandateRegistrationDTO);
    CashAccountMandate updateCashAccountMandate(CashAccountMandate cashAccountMandate);
    CashAccountMandate deactivateCashAccountMandate(CashAccountMandate cashAccountMandate);
    CashAccountMandate deleteCashAccountMandate(String id, String statusDescription);
    CashAccountMandate approveCashAccountMandate(String id);
    CashAccountMandate rejectCashAccountMandate(String id, String statusDescription);
    Optional<CashAccountMandate> getCashAccountMandateById(String cashAccountMandateId);
    Optional<CashAccountMandate> checkIfCashAccountHaveValidMandate(String cashAccountId);
    List<CashAccountMandate> getCashAccountMandates();
    List<MandateHolder> getMandateHolders(String cashAccountId);
    List<MandateHolder> getAllMandateHolders(String cashAccountId);
    List<MandatePartyRole> getInitiators(String cashAccountId);
    ApprovalResponseDTO getPaymentInitiators(String cashAccountId, String personId);
    List<MandatePartyRole> getAuthoriseMandateParties(String cashAccountId);
    List<CashAccountMandate> getCashAccountMandateByCashAccountId(String cashAccountId);
    List<RolePlayer> getSignatureForPayment(String paymentId);
    List<RolePlayer> getAllSignatureForSignatureCondition(String paymentId);
    AccountPartyInfo getAccountPartyInfo(String cashAccountId);
}
