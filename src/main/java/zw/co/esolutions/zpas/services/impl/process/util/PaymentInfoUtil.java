package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.model.BulkPayment;
import zw.co.esolutions.zpas.model.IndividualPayment;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.PaymentDetails;
import zw.co.esolutions.zpas.repository.IndividualPaymentRepository;
import zw.co.esolutions.zpas.repository.PaymentDetailsRepository;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.services.impl.process.pojo.PaymentInfoDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class PaymentInfoUtil {

    @Autowired
    IndividualPaymentRepository individualPaymentRepository;

    @Autowired
    private PaymentDetailsRepository paymentDetailsRepository;

    public PaymentInfoDto getPaymentInfo(Payment payment) {

        List<IndividualPayment> individualPayments;

        if (payment instanceof BulkPayment) {
            log.info("Payment is of type BulkPayment");
            individualPayments = ((BulkPayment) payment).getGroups();
            if (individualPayments.isEmpty()) {
                log.info("Failed to get groups, try to fetch from db..");
                individualPayments = individualPaymentRepository.findAllByBulkPayment((BulkPayment) payment);
            }
        } else {
            log.info("Payment is of type IndividualPayment");
            individualPayments = Arrays.asList((IndividualPayment) payment);
        }

        int nrOfPayments = individualPayments.size();

        final BigDecimal controlSum = individualPayments.stream()
                .map(individualPayment -> individualPayment.getAmount().getAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        log.info("Total no. of payments is: {}", nrOfPayments);
        log.info("Control Sum is: {}", controlSum);

        return new PaymentInfoDto(individualPayments, nrOfPayments, controlSum);
    }

    public List<PaymentDetails> getPaymentDetailsList(Payment payment) {

        List<PaymentDetails> paymentDetailsList;

        if (payment instanceof BulkPayment) {
            log.info("Payment is of type BulkPayment");
            paymentDetailsList = paymentDetailsRepository.findAllByBulkPaymentId(payment.getId());
        } else {
            log.info("Payment is of type IndividualPayment");
            paymentDetailsList = new ArrayList<>(Arrays.asList(payment.getPaymentDetails()));
        }

        int nrOfPayments = paymentDetailsList.size();

        log.info("Total no. of payments is: {}", nrOfPayments);

        return paymentDetailsList;
    }

    public PaymentInfoDto getPaymentInfo(List<Payment> payments) {

        final List<PaymentInfoDto> paymentInfoDtos = payments.stream()
                .map(payment -> getPaymentInfo(payment))
                .collect(Collectors.toList());

        List<IndividualPayment> allIndividualPayments = new ArrayList<>();
        Integer totalNrOfPayments = 0;
        BigDecimal totalControlSum = BigDecimal.ZERO;

        for (PaymentInfoDto paymentInfoDto : paymentInfoDtos) {
            allIndividualPayments.addAll(paymentInfoDto.individualPayments);
            totalNrOfPayments += paymentInfoDto.nrOfPayments;
            totalControlSum = totalControlSum.add(paymentInfoDto.controlSum);
        }

        return new PaymentInfoDto(allIndividualPayments, totalNrOfPayments, totalControlSum);
    }
}
