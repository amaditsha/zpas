package zw.co.esolutions.zpas.services.impl.debitorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.debitorder.DebitOrderBatchDTO;
import zw.co.esolutions.zpas.dto.debitorder.DebitOrderBatchUploadDTO;
import zw.co.esolutions.zpas.model.DebitOrderBatch;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;

import java.util.Optional;

@Service
public class DebitOrderBatchUploadServiceImpl {

    @Autowired
    private DebitOrderBatchUploadsProcessor debitOrderBatchUploadsProcessor;


    public Optional<DebitOrderBatch> uploadDebitOrderBatch(DebitOrderBatchDTO debitOrderBatchDTO) throws PaymentRequestInvalidArgumentException {
        return debitOrderBatchUploadsProcessor.uploadDebitOrderBatch(debitOrderBatchDTO);
    }

}
