package zw.co.esolutions.zpas.services.iface.signature;

import zw.co.esolutions.zpas.dto.agreement.SignatureCreationDTO;
import zw.co.esolutions.zpas.model.ElectronicSignature;

import java.util.List;
import java.util.Optional;

public interface ElectronicSignatureService {
    ElectronicSignature createElectronicSignature(SignatureCreationDTO signatureCreationDTO);
    ElectronicSignature updateElectronicSignature(ElectronicSignature electronicSignature);
    ElectronicSignature deleteElectronicSignature(String id);
    ElectronicSignature approveElectronicSignature(String id);
    ElectronicSignature rejectElectronicSignature(String id);
    Optional<ElectronicSignature> getElectronicSignatureById(String signatureId);
    List<ElectronicSignature> getElectronicSignatures();
}
