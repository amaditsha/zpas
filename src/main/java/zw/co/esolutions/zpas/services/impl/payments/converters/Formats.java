package zw.co.esolutions.zpas.services.impl.payments.converters;
/*
 * Created on Jul 4, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences &gt;Java&gt;Code Generation&gt;Code and Comments
 */


/**
 * @author JimmyRitz
 */

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Formats {
	
    public static DateTimeFormatter rfc3339DateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static DateTimeFormatter shortUploadDateFormat = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
    public static DateTimeFormatter yearMonthAndDayDateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");
	public static DateTimeFormatter shortDateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	public static DateTimeFormatter longDateTimeFormat = DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy hh:mm a");
	public static DateTimeFormatter shortDateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm");
	public static DateTimeFormatter longDateFormat = DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy");
	public static DateTimeFormatter fullMonthDateFormat = DateTimeFormatter.ofPattern("dd MMMM yyyy");
	public static DateTimeFormatter yearOnlyDateFormat = DateTimeFormatter.ofPattern("yyyy");
	public static DateTimeFormatter shortPlainDateFormat = DateTimeFormatter.ofPattern("ddMMyyyy");
	public static DateTimeFormatter short2DigitYearPlainDateFormat = DateTimeFormatter.ofPattern("ddMMyy");
	public static DateTimeFormatter yearAndMonthDateFormat = DateTimeFormatter.ofPattern("yyyy MMMM");
	public static DateTimeFormatter SFIDateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");
	public static DateTimeFormatter fileTimePrefix = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
	
	public static DecimalFormat twoDigitIntFormat = new DecimalFormat("00");
	public static DecimalFormat threeDigitIntFormat = new DecimalFormat("000");
	public static DecimalFormat fourDigitIntFormat = new DecimalFormat("0000");
	public static DecimalFormat fiveDigitIntFormat = new DecimalFormat("00000");
	public static DecimalFormat sixDigitIntFormat = new DecimalFormat("000000");
	public static DecimalFormat sevenDigitIntFormat = new DecimalFormat("0000000");
	public static DecimalFormat eightDigitIntFormat = new DecimalFormat("00000000");
	public static DecimalFormat nineDigitIntFormat = new DecimalFormat("000000000");
	
	public static DecimalFormat intFormat = new DecimalFormat("0");
	public static DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
	public static DecimalFormat moneyFormatNoCommas = new DecimalFormat("0.00");
	public static DecimalFormat threeDecimalFormat = new DecimalFormat("0.000");
	public static DecimalFormat sixDecimalFormat = new DecimalFormat("0.000000");


	public static String toProperCase(String myString) {
		boolean nextIsUpper = true;
		char m = ' ';
		myString.toLowerCase();
		char[] chars = myString.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (nextIsUpper) chars[i] = String.valueOf(chars[i]).toUpperCase().charAt(0);
			if (chars[i] == m) nextIsUpper = true;
			else if (chars[i] != m) nextIsUpper = false;
		}
		return String.valueOf(chars);
	}

	//Date formats
	public OffsetDateTime getLongDateFormat(String date) {
		OffsetDateTime formattedDate = null;
		try {
			formattedDate = LocalDateTime.parse(date, Formats.fullMonthDateFormat).atOffset(ZoneOffset.UTC);
		} catch (Exception e) {
			System.out.println("Could  not format date as required: Date..." + date);
		}
		return formattedDate;

	}

	public OffsetDateTime getYearOnlyDateFormat(String date) {
		OffsetDateTime formattedDate = null;
		try {
			formattedDate = LocalDateTime.parse(date, Formats.fullMonthDateFormat).atOffset(ZoneOffset.UTC);
		} catch (Exception e) {
			System.out.println("Could  not format date as required: Date..." + date);
		}
		return formattedDate;

	}

	public OffsetDateTime getFullMonthDateFormat(String date) {
		OffsetDateTime formattedDate = null;
		try {
			formattedDate = LocalDateTime.parse(date, Formats.fullMonthDateFormat).atOffset(ZoneOffset.UTC);
		} catch (Exception e) {
			System.out.println("Could  not format date as required: Date..." + date);
		}
		return formattedDate;

	}

	public OffsetDateTime getYearAndMonthDateFormat(String date) {
		OffsetDateTime formattedDate = null;
		java.util.Date dateFromString = null;
		try {
			formattedDate = LocalDateTime.parse(date, Formats.fullMonthDateFormat).atOffset(ZoneOffset.UTC);
		} catch (Exception e) {
			System.out.println("Could  not format date as required: Date..." + date);
		}
		return formattedDate;

	}

	public OffsetDateTime getLongDateTimeFormat(String date) {
		OffsetDateTime formattedDate = null;
		java.util.Date dateFromString = null;
		try {
			formattedDate = LocalDateTime.parse(date, Formats.fullMonthDateFormat).atOffset(ZoneOffset.UTC);
		} catch (Exception e) {
			System.out.println("Could  not format date as required: Date..." + date);
		}
		return formattedDate;

	}

	public OffsetDateTime getDateInShortDateFormat(String date) {
		OffsetDateTime formattedDate = null;
		java.util.Date dateFromString = null;
		try {
			formattedDate = LocalDateTime.parse(date, Formats.fullMonthDateFormat).atOffset(ZoneOffset.UTC);
		} catch (Exception e) {
			System.out.println("Could  not format date as required: Date..." + date);
		}
		return formattedDate;

	}

	public static int getIntFromString(String intString) {
		int number = 0;
		try {
			number = Integer.parseInt(intString);
		} catch (Exception e) {
			System.out.println("Could not convert string into int......String........." + intString);
		}
		return number;
	}

	public static double getDoubleFromString(String doubleString) {
		double val = 0;
		try {
			val = Double.parseDouble(doubleString);
		} catch (Exception e) {
			System.out.println("Could not convert string into double......String........." + doubleString);
		}

		return val;
	}

	/**
	 * @param myString
	 * @return
	 */
	public static String toNationalIdFormat(String myString) {
		char s = ' ';
		char h = '-';
		myString = myString.toUpperCase();
		myString = stripSpacesAndChar(myString);
		int i, len = myString.length();
		char c;
		StringBuffer dest = new StringBuffer(len + 3);
		for (i = 0; i < len; i++) {
			c = myString.charAt(i);
			if (i == 2) {
				dest.append(h);
			}
			if (i == (len - 3)) {
				dest.append(s);
				dest.append(c);
				continue;
			}
			dest.append(c);
		}
		return (dest.toString()).trim();
	}

	/**
	 * @param myString
	 * @return
	 */
	public static String stripSpacesAndChar(String myString) {
		Pattern p = Pattern.compile("[^A-Za-z0-9]");
		Matcher m = p.matcher(myString);
		StringBuffer sb = new StringBuffer();
		boolean result = m.find();

		while (result) {
			m.appendReplacement(sb, "");
			result = m.find();
		}

		// Add the last segment of input to the new String
		m.appendTail(sb);

		return sb.toString();
	}

    //Decimal and Money formats:: To be implemeted later.Busy doing something else
	}
