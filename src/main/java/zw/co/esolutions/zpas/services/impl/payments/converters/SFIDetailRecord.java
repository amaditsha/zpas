package zw.co.esolutions.zpas.services.impl.payments.converters;


import java.time.OffsetDateTime;


/**
 * 
 * @author Gibson Mukarakate
 *
 */
public class SFIDetailRecord {
	private String recordType;				// XxX: {PAY, DBB}
	private String destinationSortCode;		// 9 (7)
	private String destinationAccount;		// 9 (20)
	private String destinationAccountType;	// 99 : 01 (current), 02
	private String transactionCode;			// 99 : 
	private String originSortCode;			// 9 (7)
	private String originAccount;			// 9 (20)
	private String originAccountType;		// 99 : 01 (current), 02
	private String referenceID;				// X (15)
	private String destinationName;			// X (30)
	private long amount;					// 9 (24)
	private OffsetDateTime processingDate;	// 9 (8)
	private String unpaidReason;			// 99 = 00
	private String narrative;				// X (30)
	private String amountCurrencyCode;      // XXX USD
	private String originCurrencyRate ;      // 9(24)
	private String id;
	private String response;

	public static final int RECORDTYPE_FIELD_LENGTH = 3;
	public static final int DESTINATIONSORTCODE_FIELD_LENGTH = 7;
	public static final int DESTINATIONACCOUNT_FIELD_LENGTH = 20;
	public static final int DESTINATIONACCOUNTTYPE_FIELD_LENGTH = 2;
	public static final int TRANSACTIONCODE_FIELD_LENGTH = 2;
	public static final int ORIGINSORTCODE_FIELD_LENGTH = 7;
	public static final int ORIGINACCOUNT_FIELD_LENGTH = 20;
	public static final int ORIGINACCOUNTTYPE_FIELD_LENGTH = 2;
	public static final int REFERENCEID_FIELD_LENGTH = 15;
	public static final int DESTINATIONNAME_FIELD_LENGTH = 30;
	public static final int AMOUNT_FIELD_LENGTH = 24;
	public static final int PROCESSINGDATE_FIELD_LENGTH = 8;
	public static final int UNPAIDREASON_FIELD_LENGTH = 2;
	public static final int NARRATIVE_FIELD_LENGTH = 30;
	public static final int AMOUNT_CURRENCY_CODE = 3;
	public static final int DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH =3;
	public static final int ORIGIN_CURRENCY_RATE=24;

	public static final String RECORDTYPE_VALUE_PAY = "PAY";
	public static final String RECORDTYPE_VALUE_DBB = "DEB";

	public SFIDetailRecord() {
	}	

	public SFIDetailRecord(String recordType, String destinationSortCode, String destinationAccount, String destinationAccountType, String transactionCode, String originSortCode, String originAccount, String originAccountType, String referenceID, String destinationName, long amount, OffsetDateTime processingDate, String unpaidReason, String narrative,String amountCurrencyCode) throws Exception {
		super();
		this.setRecordType(recordType);
		this.setDestinationSortCode(destinationSortCode);
		this.setDestinationAccount(destinationAccount);
		this.setDestinationAccountType(destinationAccountType);
		this.setTransactionCode(transactionCode);
		this.setOriginSortCode(originSortCode);
		this.setOriginAccount(originAccount);
		this.setOriginAccountType(originAccountType);
		this.setReferenceID(referenceID);
		this.setDestinationName(destinationName);
		this.setAmount(amount);
		this.setProcessingDate(processingDate);
		this.setUnpaidReason(unpaidReason);
		this.setNarrative(narrative);
		this.setAmountCurrencyCode(amountCurrencyCode);
	}

	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @return the destinationAccount
	 */
	public String getDestinationAccount() {
		return destinationAccount;
	}
	/**
	 * @return the destinationAccountType
	 */
	public String getDestinationAccountType() {
		return destinationAccountType;
	}
	/**
	 * @return the destinationName
	 */
	public String getDestinationName() {
		return destinationName;
	}
	/**
	 * @return the destinationSortCode
	 */
	public String getDestinationSortCode() {
		return destinationSortCode;
	}
	/**
	 * @return the narrative
	 */
	public String getNarrative() {
		return narrative;
	}
	/**
	 * @return the originAccount
	 */
	public String getOriginAccount() {
		return originAccount;
	}
	/**
	 * @return the originAccountType
	 */
	public String getOriginAccountType() {
		return originAccountType;
	}
	/**
	 * @return the originSortCode
	 */
	public String getOriginSortCode() {
		return originSortCode;
	}
	/**
	 * @return the processingDate
	 */
	public OffsetDateTime getProcessingDate() {
		return processingDate;
	}
	/**
	 * @return the recordType
	 */
	public String getRecordType() {
		return recordType;
	}
	/**
	 * @return the referenceID
	 */
	public String getReferenceID() {
		return referenceID;
	}
	/**
	 * @return the transactionCode
	 */
	public String getTransactionCode() {
		return transactionCode;
	}
	/**
	 * @return the unpaidReason
	 */
	public String getUnpaidReason() {
		return unpaidReason;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @param destinationAccount the destinationAccount to set
	 */
	public void setDestinationAccount(String destinationAccount) throws Exception {
		if (destinationAccount != null)
			destinationAccount = ""+Long.parseLong(destinationAccount);
		this.destinationAccount = destinationAccount;
	}
	/**
	 * @param destinationAccountType the destinationAccountType to set
	 */
	public void setDestinationAccountType(String destinationAccountType) {
		this.destinationAccountType = destinationAccountType;
	}
	/**
	 * @param destinationName the destinationName to set
	 */
	public void setDestinationName(String destinationName) {
		if (destinationName != null)
			destinationName = destinationName.toUpperCase().trim();
		this.destinationName = destinationName;
	}
	/**
	 * @param destinationSortCode the destinationSortCode to set
	 */
	public void setDestinationSortCode(String destinationSortCode) {
		this.destinationSortCode = destinationSortCode;
	}
	/**
	 * @param narrative the narrative to set
	 */
	public void setNarrative(String narrative) {
		if (narrative != null)
			narrative = narrative.toUpperCase().trim();
		this.narrative = narrative;
	}
	/**
	 * @param originAccount the originAccount to set
	 */
	public void setOriginAccount(String originAccount) {
		if (originAccount != null && originAccount.trim().length() > 0)
			originAccount = ""+Long.parseLong(originAccount);
		this.originAccount = originAccount;
	}
	/**
	 * @param originAccountType the originAccountType to set
	 */
	public void setOriginAccountType(String originAccountType) {
		this.originAccountType = originAccountType;
	}
	/**
	 * @param originSortCode the originSortCode to set
	 */
	public void setOriginSortCode(String originSortCode) {
		this.originSortCode = originSortCode;
	}
	/**
	 * @param processingDate the processingDate to set
	 */
	public void setProcessingDate(OffsetDateTime processingDate) {
		this.processingDate = processingDate;
	}
	/**
	 * @param recordType the recordType to set
	 */
	public void setRecordType(String recordType) throws Exception {
		if (!RECORDTYPE_VALUE_PAY.equalsIgnoreCase(recordType) && !RECORDTYPE_VALUE_DBB.equalsIgnoreCase(recordType))
			throw new Exception("Invalid record type for record '" + recordType + "'.");
		recordType = recordType.toUpperCase().trim();
		this.recordType = recordType;
	}
	/**
	 * @param referenceID the referenceID to set
	 */
	public void setReferenceID(String referenceID) {
		if (referenceID != null)
			referenceID = referenceID.toUpperCase().trim();
		this.referenceID = referenceID;
	}
	/**
	 * @param transactionCode the transactionCode to set
	 */
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	/**
	 * @param unpaidReason the unpaidReason to set
	 */
	public void setUnpaidReason(String unpaidReason) {
		this.unpaidReason = unpaidReason;
	}
	
	/**
	 * @return the amountCurrencyCode
	 */
	public String getAmountCurrencyCode() {
		return amountCurrencyCode;
	}

	/**
	 * @param amountCurrencyCode the amountCurrencyCode to set
	 */
	public void setAmountCurrencyCode(String amountCurrencyCode) {
		this.amountCurrencyCode = amountCurrencyCode;
	}
	

	/**
	 * @return the originCurrencyRate
	 */
	public String getOriginCurrencyRate() {
        return originCurrencyRate;
	}
	/**
	 * @param originCurrencyRate the originCurrencyRate to set
	 */
	public void setOriginCurrencyRate(String originCurrencyRate) {
		this.originCurrencyRate = originCurrencyRate;
	}

    public String getSFIAmountCurrencyCode() throws Exception {
        return BeanUtils.leftPad(this.getAmountCurrencyCode(), AMOUNT_CURRENCY_CODE, '0');
    }
    public String getSFIOriginCurrencyRate() throws Exception {
        return BeanUtils.leftPad(this.getOriginCurrencyRate(), ORIGIN_CURRENCY_RATE, '0');
    }

	public String getSFIAmount() throws Exception {
		return BeanUtils.leftPad(Formats.intFormat.format(this.getAmount()), AMOUNT_FIELD_LENGTH, '0');
	}
	public String getSFIDestinationAccount() throws Exception {
		return BeanUtils.leftPad(this.getDestinationAccount(), DESTINATIONACCOUNT_FIELD_LENGTH, '0');
	}
	public String getSFIDestinationAccountType() throws Exception {
		return BeanUtils.leftPad(this.getDestinationAccountType(), DESTINATIONACCOUNTTYPE_FIELD_LENGTH, '0');
	}
	public String getSFIDestinationName() throws Exception {
		return BeanUtils.rightPad(this.getDestinationName(), DESTINATIONNAME_FIELD_LENGTH, ' ');
	}
	public String getSFIDestinationSortCode() throws Exception {
		return BeanUtils.leftPad(this.getDestinationSortCode(), DESTINATIONSORTCODE_FIELD_LENGTH, '0');
	}
	public String getFiveDigitDestinationSortCode() throws Exception {
		return BeanUtils.getFiveDigitBankSortCode(getSFIDestinationSortCode());
	}
	public String getSFINarrative() throws Exception {
		return BeanUtils.rightPad(this.getNarrative(), NARRATIVE_FIELD_LENGTH, ' ');
	}
	public String getSFIOriginAccount() throws Exception {
		return BeanUtils.leftPad(this.getOriginAccount(), ORIGINACCOUNT_FIELD_LENGTH, '0');
	}
	public String getSFIOriginAccountType() throws Exception {
		return BeanUtils.leftPad(this.getOriginAccountType(), ORIGINACCOUNTTYPE_FIELD_LENGTH, '0');
	}
	public String getSFIOriginSortCode() throws Exception {
		return BeanUtils.leftPad(this.getOriginSortCode(), ORIGINSORTCODE_FIELD_LENGTH, '0');
	}
	public String getFiveDigitOriginSortCode() throws Exception {
		return BeanUtils.getFiveDigitBankSortCode(getSFIOriginSortCode());
	}
	public String getSFIProcessingDate() throws Exception {
		String tmp = (this.getProcessingDate()==null)? "": Formats.SFIDateFormat.format(this.getProcessingDate());
		return BeanUtils.leftPad(tmp, PROCESSINGDATE_FIELD_LENGTH, ' ');
	}
	public String getSFIRecordType() throws Exception {
		return BeanUtils.rightPad(this.getRecordType(), RECORDTYPE_FIELD_LENGTH, ' ');
	}
	public String getSFIReferenceID() throws Exception {
		return BeanUtils.rightPad(this.getReferenceID(), REFERENCEID_FIELD_LENGTH, ' ');
	}
	public String getSFITransactionCode() throws Exception {
		return BeanUtils.leftPad(this.getTransactionCode(), TRANSACTIONCODE_FIELD_LENGTH, '0');
	}
	public String getSFIUnpaidReason() throws Exception {
		return BeanUtils.leftPad(this.getUnpaidReason(), UNPAIDREASON_FIELD_LENGTH, '0');
	}
	
	public boolean isDebit() {
		if (SFIDetailRecord.RECORDTYPE_VALUE_PAY.equalsIgnoreCase(this.getRecordType()))
			return false;
		else
			return true;
	}

	public String getId() {
        return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

	@Override
	public String toString() {
		return "SFIDetailRecord{" +
				"recordType='" + recordType + '\'' +
				", destinationSortCode='" + destinationSortCode + '\'' +
				", destinationAccount='" + destinationAccount + '\'' +
				", destinationAccountType='" + destinationAccountType + '\'' +
				", transactionCode='" + transactionCode + '\'' +
				", originSortCode='" + originSortCode + '\'' +
				", originAccount='" + originAccount + '\'' +
				", originAccountType='" + originAccountType + '\'' +
				", referenceID='" + referenceID + '\'' +
				", destinationName='" + destinationName + '\'' +
				", amount=" + amount +
				", processingDate=" + processingDate +
				", unpaidReason='" + unpaidReason + '\'' +
				", narrative='" + narrative + '\'' +
				", amountCurrencyCode='" + amountCurrencyCode + '\'' +
				", originCurrencyRate='" + originCurrencyRate + '\'' +
				", id='" + id + '\'' +
				", response='" + response + '\'' +
				'}';
	}
}
