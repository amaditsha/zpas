package zw.co.esolutions.zpas.services.nssa.impl;
import zw.co.esolutions.zpas.model.Payment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.documents.DocumentDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PaymentResponseDTO;
import zw.co.esolutions.zpas.dto.nssa.*;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.model.CashAccount;
import zw.co.esolutions.zpas.model.PaymentStatus;
import zw.co.esolutions.zpas.model.payments.ExtraDetail;
import zw.co.esolutions.zpas.repository.OrganisationRepository;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.repository.payments.ExtraDetailRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.services.iface.documents.DocumentsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.registration.NonFinancialInstitutionService;
import zw.co.esolutions.zpas.services.impl.payments.converters.BeanUtils;
import zw.co.esolutions.zpas.services.impl.payments.converters.SFIDetailRecord;
import zw.co.esolutions.zpas.services.impl.payments.converters.SFIFileContents;
import zw.co.esolutions.zpas.services.impl.payments.converters.SFIUtils;
import zw.co.esolutions.zpas.services.nssa.iface.ContributionsService;
import zw.co.esolutions.zpas.services.nssa.iface.NssaClientsService;
import zw.co.esolutions.zpas.services.nssa.impl.data.ContributionItemEntry;
import zw.co.esolutions.zpas.services.nssa.impl.exceptions.NssaPaymentInvalidRequestException;
import zw.co.esolutions.zpas.services.nssa.impl.json.NssaP4RequestDTO;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.storage.StorageService;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static zw.co.esolutions.zpas.utilities.util.PaymentsUtil.*;

/**
 * Create by alfred on 19 Jul 2019
 */
@Slf4j
@Service
@Transactional
public class ContributionsServiceImpl implements ContributionsService {
    private static final String OUTPUT_FOLDER = StorageProperties.BASE_LOCATION;

    @Value("${NSSA.ACCOUNT}")
    private String accountNumber;

    @Value("${NSSA.CONTRIBUTIONS.SYSTEM.BASE.URL}")
    private String nssaContributionsSystemBaseUrl;

    @Autowired
    private NssaClientsService nssaClientsService;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private NonFinancialInstitutionService nonFinancialInstitutionService;

    @Autowired
    private DocumentsService documentsService;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private StorageService storageService;

    @Autowired
    private ExtraDetailRepository extraDetailRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public Optional<ContributionScheduleDTO> createContributionSchedule(MakeNssaPaymentDTO makeNssaPaymentDTO) throws NssaPaymentInvalidRequestException {
        log.info("NSSA Payment Upload DTO is: {}", makeNssaPaymentDTO);
        final String fileName = makeNssaPaymentDTO.getFileName();
        if (StringUtils.isEmpty(fileName)) {
            log.error("Invalid File ID specified. Failed to load uploaded bulk payment file. Upload and try again.");
            throw new NssaPaymentInvalidRequestException("Invalid file");
        }

        final String fileExtension = getFileExtension(fileName);
        log.info("Extension for the uploaded file is: {}", fileExtension);

        switch (fileExtension) {
            case ".csv": {
                final ContributionScheduleDTO contributionSchedule = readContributionScheduleFromCSVFile(makeNssaPaymentDTO);
                saveDocument(makeNssaPaymentDTO, contributionSchedule.getAmount());
                return saveContributionSchedule(contributionSchedule);
            }
            case ".sfi": {
                final ContributionScheduleDTO contributionSchedule = readContributionScheduleFromSFIFile(makeNssaPaymentDTO);
                saveDocument(makeNssaPaymentDTO, contributionSchedule.getAmount());
                return saveContributionSchedule(contributionSchedule);
            }
            case ".zip": {
                final Path resolve = Paths.get(OUTPUT_FOLDER).resolve(fileName);
                final String unzippedFile = unZipIt(resolve.toUri().getPath(), OUTPUT_FOLDER);
                createContributionSchedule(makeNssaPaymentDTO.toBuilder().fileName(unzippedFile).build());
            }
            case ".xls":
            case ".xlsx": {
                final ContributionScheduleDTO contributionSchedule = readContributionScheduleFromExcelFile(makeNssaPaymentDTO);
                saveDocument(makeNssaPaymentDTO, contributionSchedule.getAmount());
                return saveContributionSchedule(contributionSchedule);
            }
            default:
                throw new NssaPaymentInvalidRequestException("Extension " + fileExtension + " not supported yet");
        }
    }

    @Override
    public Optional<ContributionScheduleDTO> getContributionScheduleById(String contributionScheduleId) {
        RestTemplate restTemplate = new RestTemplate();
        String getContributionScheduleUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/view/" + contributionScheduleId;

        ContributionScheduleDTO contributionScheduleDTO = restTemplate.getForObject(getContributionScheduleUrl, ContributionScheduleDTO.class);

        return Optional.ofNullable(contributionScheduleDTO);
    }

    @Override
    public List<ContributionScheduleDTO> getContributionSchedulesByClientId(String clientId) {
        return organisationRepository.findById(clientId).map(organisation -> {
            String getContributionSchedulesUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/history/all/" + organisation.getSsr();

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<List<ContributionScheduleDTO>> response = restTemplate.exchange(
                    getContributionSchedulesUrl,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<ContributionScheduleDTO>>() {
                    });
            List<ContributionScheduleDTO> contributionScheduleDTOS = response.getBody();
            return contributionScheduleDTOS;
        }).orElse(new ArrayList<>());
    }

    @Override
    public Optional<ContributionScheduleDTO> getContributionScheduleByMonth(String clientId, String month) {
        return organisationRepository.findById(clientId).map(organisation -> {
            RestTemplate restTemplate = new RestTemplate();
            String getContributionScheduleUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/find/by-month/" + organisation.getSsr() + "/" + month;

            ContributionScheduleDTO contributionScheduleDTO = restTemplate.getForObject(getContributionScheduleUrl, ContributionScheduleDTO.class);

            return Optional.ofNullable(contributionScheduleDTO);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<ContributionScheduleDTO> submitContributionSchedule(String contributionScheduleId) {
        return getContributionScheduleById(contributionScheduleId).map(contributionScheduleDTO -> {
            RestTemplate restTemplate = new RestTemplate();
            String submitContributionScheduleUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/submit/" + contributionScheduleId;

            log.info("Tried to reach nssa service at url: {}", submitContributionScheduleUrl);

            ResponseEntity<ContributionScheduleDTO> response = restTemplate.exchange(
                    submitContributionScheduleUrl,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ContributionScheduleDTO>() {
                    });
            ContributionScheduleDTO scheduleDTO = response.getBody();

            return Optional.ofNullable(scheduleDTO);

        }).orElse(Optional.empty());
    }

    @Override
    public Optional<ContributionScheduleDTO> approveContributionSchedule(String contributionScheduleId) {
        RestTemplate restTemplate = new RestTemplate();
        String approveContributionScheduleUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/approve/" + contributionScheduleId;

        log.info("Trying to reach NSSA Contributions service at url: {}", approveContributionScheduleUrl);

        ContributionScheduleDTO contributionScheduleDTO = restTemplate.getForObject(approveContributionScheduleUrl, ContributionScheduleDTO.class);

        log.info("Received approval response for contribution: {}", contributionScheduleDTO.getMonth());

        return Optional.ofNullable(contributionScheduleDTO);
    }

    @Override
    public Optional<ContributionScheduleDTO> updateContributionSchedule(ContributionScheduleDTO contributionSchedule) {

        RestTemplate restTemplate = new RestTemplate();
        String addContributionScheduleUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/update";

        ResponseEntity<ContributionScheduleDTO> response = restTemplate.exchange(
                addContributionScheduleUrl,
                HttpMethod.POST,
                new HttpEntity<>(contributionSchedule),
                new ParameterizedTypeReference<ContributionScheduleDTO>() {
                });
        ContributionScheduleDTO scheduleDTO = response.getBody();
        return Optional.ofNullable(scheduleDTO);
    }

    @Override
    public Optional<ContributionScheduleDTO> rejectContributionSchedule(String contributionScheduleId) {
        RestTemplate restTemplate = new RestTemplate();
        String approveContributionScheduleUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/reject/" + contributionScheduleId;

        ContributionScheduleDTO contributionScheduleDTO = restTemplate.getForObject(approveContributionScheduleUrl, ContributionScheduleDTO.class);

        return Optional.ofNullable(contributionScheduleDTO);
    }

    @Override
    public Optional<ContributionScheduleDTO> getContributionScheduleByPaymentId(String paymentId) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String getContributionScheduleByReferenceUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/get/by-payment/" + paymentId;

            log.info("Trying to get contribution by reference: {}", getContributionScheduleByReferenceUrl);

            ContributionScheduleDTO contributionScheduleDTO = restTemplate.getForObject(getContributionScheduleByReferenceUrl, ContributionScheduleDTO.class);

            return Optional.ofNullable(contributionScheduleDTO);
        } catch (RuntimeException re) {
            log.error("Could not access NSSA system.");
            return Optional.empty();
        }
    }

    @Override
    public Optional<ContributionScheduleDTO> handleContributionSchedulePaymentStatusReportReceived(String paymentEndToEndId) {
        return paymentsService.getPaymentByEndToEndId(paymentEndToEndId).map(payment -> {
            return getContributionScheduleByPaymentId(payment.getId()).map(contributionSchedule -> {
                addExtraDetailOnPayment(contributionSchedule, payment);
                final PaymentResponseDTO paymentResponseDTO = PaymentResponseDTO.builder()
                        .paymentId(payment.getId())
                        .endToEndId(payment.getEndToEndId())
                        .paymentStatus(payment.getLatestPaymentStatus().map(PaymentStatus::getStatus).orElse(PaymentStatusCode.None))
                        .responseStatus("SUCCESSFUL")
                        .build();

                RestTemplate restTemplate = new RestTemplate();
                String processPaymentResponseUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/process/response";

                ResponseEntity<ContributionScheduleDTO> response = restTemplate.exchange(
                        processPaymentResponseUrl,
                        HttpMethod.POST,
                        new HttpEntity<>(paymentResponseDTO),
                        new ParameterizedTypeReference<ContributionScheduleDTO>() {
                        });
                ContributionScheduleDTO scheduleDTO = response.getBody();
                return Optional.ofNullable(scheduleDTO);
            }).orElseGet(() -> {
                log.info("There was no Contribution schedule found related to payment reference \"{}\"", paymentEndToEndId);
                return Optional.empty();
            });
        }).orElseGet(() -> {
            log.error("Failed to get payment for end to end ID \"{}\"", paymentEndToEndId);
            return Optional.empty();
        });
    }

    private void addExtraDetailOnPayment(ContributionScheduleDTO contributionSchedule, Payment payment) {
        ExtraDetail extraDetail = new ExtraDetail();
        extraDetail.setId(UUID.randomUUID().toString());
        extraDetail.setDateCreated(OffsetDateTime.now());
        extraDetail.setLastUpdated(OffsetDateTime.now());
        extraDetail.setEntityStatus(EntityStatus.ACTIVE);
        extraDetail.setPayment(payment);

        final PaymentStatusCode paymentStatusCode = payment.getLatestPaymentStatus().map(PaymentStatus::getStatus).orElse(PaymentStatusCode.None);

        StringBuilder message = new StringBuilder("Payment for ");
        message.append(contributionSchedule.getMonth().format(DateTimeFormatter.ofPattern("dd MMMM")));
        message.append(" contribution ");
        switch (paymentStatusCode) {
            case None:
            case Received:
            case AcceptedTechnicalValidation:
            case PartiallyAccepted:
            case AcceptedCustomerProfile:
            case Pending:
            case AcceptedSettlementInProcess:
            case PartiallyAcceptedCancellationRequest:
            case PendingCancellationRequest:
                message.append(" has been accepted for further processing.");
                break;
            case Rejected:
                message.append(" has been rejected.");
                break;
            case AcceptedSettlementCompleted:
            case Accepted:
            case AcceptedWithChange:
                message.append(" has been completed successfully. We are now processing the P4 schedule.");
                break;
            default:
                return;
        }

        extraDetail.setDetail(message.toString());
        extraDetailRepository.save(extraDetail);
        payment.addExtraDetail(extraDetail);
        paymentRepository.save(payment);
    }

    private String getNssaP4RequestJsonString(NssaP4RequestDTO nssaP4RequestDTO) {
        ObjectMapper mapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(writer, nssaP4RequestDTO);//Prettified JSON
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        final String nssaP4RequestMessage = writer.toString();
        return nssaP4RequestMessage;
    }

    @Override
    public Optional<ContributionScheduleDTO> deleteContributionSchedule(String contributionScheduleId) {
        RestTemplate restTemplate = new RestTemplate();
        String deleteContributionScheduleUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/delete/" + contributionScheduleId;

        ContributionScheduleDTO contributionScheduleDTO = restTemplate.getForObject(deleteContributionScheduleUrl, ContributionScheduleDTO.class);

        return Optional.ofNullable(contributionScheduleDTO);
    }

    @Override
    public Optional<ContributionScheduleDTO> addItemToContributionSchedule(ContributionScheduleItemDTO contributionScheduleItem) {
        RestTemplate restTemplate = new RestTemplate();
        String addContributionScheduleItemUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/items";

        ResponseEntity<ContributionScheduleDTO> response = restTemplate.exchange(
                addContributionScheduleItemUrl,
                HttpMethod.POST,
                new HttpEntity<>(contributionScheduleItem),
                new ParameterizedTypeReference<ContributionScheduleDTO>() {
                });
        ContributionScheduleDTO scheduleDTO = response.getBody();
        return Optional.ofNullable(scheduleDTO);
    }

    @Override
    public Optional<ContributionScheduleDTO> removeItemFromContributionSchedule(String contributionScheduleItemId) {
        RestTemplate restTemplate = new RestTemplate();
        String removeContributionScheduleItemUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/items/remove/" + contributionScheduleItemId;

        ContributionScheduleDTO contributionScheduleDTO = restTemplate.getForObject(removeContributionScheduleItemUrl, ContributionScheduleDTO.class);

        return Optional.ofNullable(contributionScheduleDTO);
    }

    @Override
    public Optional<ContributionScheduleItemDTO> getContributionScheduleItem(String contributionScheduleItemId) {
        RestTemplate restTemplate = new RestTemplate();
        String getContributionScheduleItemUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/view/item/" + contributionScheduleItemId;

        log.info("About to reach URL: {}", getContributionScheduleItemUrl);

        ContributionScheduleItemDTO contributionScheduleItemDTO = restTemplate.getForObject(getContributionScheduleItemUrl, ContributionScheduleItemDTO.class);

        return Optional.ofNullable(contributionScheduleItemDTO);
    }

    @Override
    public List<ContributionScheduleItemDTO> getContributionScheduleItemsByEmployeeId(String employeeId) {
        RestTemplate restTemplate = new RestTemplate();
        String getContributionScheduleItemsByEmployeeUrl = nssaContributionsSystemBaseUrl +
                "/nssa/payments/items/by/employee/" + employeeId;

        ResponseEntity<List<ContributionScheduleItemDTO>> response = restTemplate.exchange(
                getContributionScheduleItemsByEmployeeUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ContributionScheduleItemDTO>>() {
                });
        List<ContributionScheduleItemDTO> contributionScheduleItemDTOS = response.getBody();
        return contributionScheduleItemDTOS;
    }

    private void saveDocument(MakeNssaPaymentDTO makeNssaPaymentDTO, CurrencyAndAmount paymentAmount) {
        log.info("Saving document for the PaymentUploadDTO: {}", makeNssaPaymentDTO);
        final String fileName = makeNssaPaymentDTO.getFileName();

        documentsService.createDocument(DocumentDTO.builder()
                .clientId(makeNssaPaymentDTO.getClientId())
                .copyDuplicate("")
                .dataSetType("")
                .documentName(fileName)
                .documentPath(Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName).toString())
                .issueDate(LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                .purpose(ProprietaryPurposeCode.Pension.getCodeName())
                .type("NSSA Payment")
                .remittedAmount(paymentAmount)
                .build());
    }

    private Optional<ContributionScheduleDTO> saveContributionSchedule(ContributionScheduleDTO contributionSchedule) {

        //persist the contribution schedule and its entries
        log.info("Contribution Schedule is {}", contributionSchedule.getId());
        List<ContributionScheduleItemDTO> contributionScheduleItems = contributionSchedule.getContributionScheduleItems();
        log.info("{} Items in contribution schedule {}", contributionScheduleItems.size(), contributionSchedule.getId());

        final Optional<CashAccount> destinationAccountOptional = accountService.findAccountByNumber(accountNumber);
        if (!destinationAccountOptional.isPresent()) {
            return Optional.empty();
        }
//        final CashAccount destinationAccount = destinationAccountOptional.get();

        final AccountDTO sourceAccountDTO = contributionSchedule.getAccount();
        final Optional<CashAccount> sourceAccountOptional = accountService.findAccountByNumber(sourceAccountDTO.getNumber());
        if (!sourceAccountOptional.isPresent()) {
            return Optional.empty();
        }
//        final CashAccount sourceAccount = sourceAccountOptional.get();

        final String monthString = contributionSchedule.getMonth().format(DateTimeFormatter.ofPattern("MMMM yyyy"));
        log.info("Making the payment for contribution schedule for {} made for month {}", contributionSchedule.getEmployer().getName(), monthString);

//        MakePaymentDTO makePaymentDTO = MakePaymentDTO.builder()
//                .amount(contributionSchedule.getAmount().getAmount().doubleValue())
//                .address("")
//                .beneficiaryName("NSSA")
//                .clientId(contributionSchedule.getEmployer().getId())
//                .clientName(contributionSchedule.getEmployer().getName())
//                .clientReference(contributionSchedule.getClientReference())
//                .destinationAccountNumber(destinationAccount.getAccountNumber())
//                .destinationFinancialInstitutionId(destinationAccount.getFinancialInstitution().getId())
//                .makePaymentRequestType(MakePaymentRequestType.COMPANY.name())
//                .beneficiaryReference("NSSA Payment for " + monthString)
//                .paymentInstrument(PaymentInstrumentCode.CustomerCreditTransfer.name())
//                .paymentType(PaymentTypeCode.PensionPayment.name())
//                .priority(PriorityCode.Normal.name())
//                .purpose(ProprietaryPurposeCode.Pension.getCodeName())
//                .sourceAccountId(sourceAccount.getId())
//                .sourceFinancialInstitutionId(sourceAccount.getFinancialInstitution().getId())
//                .valueDate(OffsetDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
//                .build();

//        try {
//            final Optional<IndividualPayment> individualPaymentOptional = paymentsService.makePayment(makePaymentDTO);
//            if (individualPaymentOptional.isPresent()) {
//                final IndividualPayment contributionScheduleIndividualPayment = individualPaymentOptional.get();
//                final String id = contributionScheduleIndividualPayment.getId();
//                final String endToEndId = contributionScheduleIndividualPayment.getEndToEndId();
//                log.info("End To End ID is: {}", endToEndId);
//                log.info("Payment ID is: {}", id);
//                contributionSchedule.setPaymentId(id);
//                contributionSchedule.setPaymentReference(endToEndId);
//
//            } else {
//                return Optional.empty();
//            }
//        } catch (PaymentRequestInvalidArgumentException e) {
//            e.printStackTrace();
//            return Optional.empty();
//        }

        RestTemplate restTemplate = new RestTemplate();
        String addContributionScheduleUrl = nssaContributionsSystemBaseUrl + "/nssa/payments/create";
        log.info("About to create contribution schedule at URL: {}", addContributionScheduleUrl);
        log.info("Sending contribution schedule: \n{}\n", contributionSchedule);

        ResponseEntity<ContributionScheduleDTO> response = restTemplate.exchange(
                addContributionScheduleUrl,
                HttpMethod.POST,
                new HttpEntity<>(contributionSchedule),
                new ParameterizedTypeReference<ContributionScheduleDTO>() {
                });
        ContributionScheduleDTO scheduleDTO = response.getBody();
        return Optional.ofNullable(scheduleDTO);
    }

    private ContributionScheduleDTO readContributionScheduleFromSFIFile(MakeNssaPaymentDTO makeNssaPaymentDTO) throws NssaPaymentInvalidRequestException {
        final CashAccount cashAccount = accountService.findAccountById(makeNssaPaymentDTO.getSourceAccountId());
        if (cashAccount == null) {
            throw new NssaPaymentInvalidRequestException("Source account not found");
        }
        final ContributionScheduleDTO contributionSchedule = readSFIFileContents(SFIUtils.FILES_ROOT + makeNssaPaymentDTO.getFileName(), cashAccount, makeNssaPaymentDTO);

        final CurrencyCode baseCurrency = cashAccount.getBaseCurrency();

        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        final Optional<BigDecimal> totalAmountOptional = contributionSchedule.getContributionScheduleItems().stream().map(p -> p.getAmount().getAmount()).reduce((bigDecimal, bigDecimal2) -> bigDecimal.add(bigDecimal2));
        instructedCurrencyAndAmount.setAmount(new BigDecimal(makeNssaPaymentDTO.getApwcs()).add(totalAmountOptional.orElse(new BigDecimal(0))));
        instructedCurrencyAndAmount.setCurrency(baseCurrency);
        contributionSchedule.setAmount(instructedCurrencyAndAmount);
        contributionSchedule.setMonth(makeNssaPaymentDTO.getMonthOffsetDateTime());
        return contributionSchedule;
    }

    private ContributionScheduleDTO readSFIFileContents(String fileName, CashAccount cashAccount, MakeNssaPaymentDTO makeNssaPaymentDTO) throws NssaPaymentInvalidRequestException {

        final CashAccount sourceCashAccount = accountService.findAccountById(makeNssaPaymentDTO.getSourceAccountId());
        if (sourceCashAccount == null) {
            throw new NssaPaymentInvalidRequestException("Source account not found");
        }

        final SFIFileContents sfiFileContents;
        try {
            sfiFileContents = BeanUtils.parseSFIFile(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NssaPaymentInvalidRequestException("Failed to read SFI File. Error is: " + e.getMessage());
        }

        ContributionScheduleDTO contributionSchedule = new ContributionScheduleDTO();

        configureContributionScheduleInformation(contributionSchedule, makeNssaPaymentDTO);


        final AccountDTO sourceAccountDTO = AccountDTO.builder()
                .baseCurrency(sourceCashAccount.getBaseCurrency())
                .name(sourceCashAccount.getAccountName())
                .number(sourceCashAccount.getAccountNumber())
                .accountOwner(contributionSchedule.getEmployer())
                .build();
        contributionSchedule.setAccount(sourceAccountDTO);

        List<ContributionScheduleItemDTO> contributionScheduleItems = readIndividualPayments(contributionSchedule, sfiFileContents, makeNssaPaymentDTO, cashAccount);
        contributionSchedule.setContributionScheduleItems(contributionScheduleItems);

        contributionSchedule.setPobs(contributionScheduleItems.stream().mapToDouble(ContributionScheduleItemDTO::getPobs).sum());

        return contributionSchedule;
    }

    private List<ContributionScheduleItemDTO> readIndividualPayments(ContributionScheduleDTO contributionSchedule, SFIFileContents sfiFileContents, MakeNssaPaymentDTO makeNssaPaymentDTO, CashAccount cashAccount) throws NssaPaymentInvalidRequestException {
        List<ContributionScheduleItemDTO> contributionScheduleItems = new ArrayList<>();
        BigDecimal totalAmount = new BigDecimal(0);

        final Map<String, EmployeeDTO> nationalIdEmployeeMap = nssaClientsService.getEmployeesByEmployerId(makeNssaPaymentDTO.getClientId())
                .stream().collect(Collectors.toMap(EmployeeDTO::getSsn, Function.identity()));

        List<ContributionItemEntry> contributionItemEntries = new ArrayList<>();
        for (SFIDetailRecord sdr : sfiFileContents.getDetailRecords()) {
            final ContributionItemEntry contributionItemEntry;
            try {
                log.info("The sdr is: {}", sdr);
                contributionItemEntry = ContributionItemEntry.builder()
                        .fullName(sdr.getDestinationName())
                        .nationalId("")
                        .ssn(sdr.getReferenceID())
                        .pobs(Double.valueOf(sdr.getAmount()))
                        .build();
                contributionItemEntries.add(contributionItemEntry);
            } catch (Exception e) {
                e.printStackTrace();
                throw new NssaPaymentInvalidRequestException("Invalid payment information: Error is: " + e.getMessage());
            }
        }

        try {
            for (ContributionItemEntry contributionItemEntry : contributionItemEntries) {
                ContributionScheduleItemDTO contributionScheduleItem = contributionItemEntry.getContributionScheduleItem();
                final BigDecimal individualPaymentAmount = contributionScheduleItem.getAmount().getAmount();
                totalAmount = totalAmount.add(individualPaymentAmount);
//                contributionScheduleItem.setContributionSchedule(contributionSchedule);

                final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
                instructedCurrencyAndAmount.setAmount(individualPaymentAmount);
                instructedCurrencyAndAmount.setCurrency(cashAccount.getBaseCurrency());
                contributionScheduleItem.setAmount(instructedCurrencyAndAmount);

                EmployeeDTO employee = nationalIdEmployeeMap.get(contributionItemEntry.getSsn());
                if (employee == null) {
                    throw new NssaPaymentInvalidRequestException("EmployeeDTO " + contributionItemEntry.getFullName() + " not found.");
                }
                contributionScheduleItem.setEmployee(employee);

                contributionScheduleItems.add(contributionScheduleItem);
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
            log.error("An error occurred while parsing sfi file: {}", re.getMessage());
            throw new NssaPaymentInvalidRequestException("An error occurred. " + re.getMessage());
        }
        return contributionScheduleItems;
    }

    private ContributionScheduleDTO readContributionScheduleFromExcelFile(MakeNssaPaymentDTO makeNssaPaymentDTO) throws NssaPaymentInvalidRequestException {
        log.info("Processing CSV contribution schedule file");

        final CashAccount sourceCashAccount = accountService.findAccountById(makeNssaPaymentDTO.getSourceAccountId());
        if (sourceCashAccount == null) {
            throw new NssaPaymentInvalidRequestException("Source account not found");
        }

        final Map<String, EmployeeDTO> nationalIdEmployeeMap = nssaClientsService.getEmployeesByEmployerId(makeNssaPaymentDTO.getClientId())
                .stream().collect(Collectors.toMap(EmployeeDTO::getSsn, Function.identity()));

        final ContributionScheduleDTO contributionSchedule = new ContributionScheduleDTO();

        log.info("Reading uploaded schedule items file...");
        BigDecimal totalAmount = new BigDecimal(0);

        List<ContributionScheduleItemDTO> contributionScheduleItems = new ArrayList<>();
        List<ContributionItemEntry> contributionItemEntries = new ArrayList<>();
        final Map<Integer, List<String>> contributionItemsData = readExcelFile(makeNssaPaymentDTO.getFileName());
        try {

            for (List<String> dataList : contributionItemsData.values()) {
                final ContributionItemEntry contributionItemEntry = ContributionItemEntry.builder()
                        .fullName(dataList.get(0))
                        .nationalId(dataList.get(1))
                        .ssn(dataList.get(2))
                        .pobs(Double.valueOf(dataList.get(3)))
                        .build();
                contributionItemEntries.add(contributionItemEntry);

            }
        } catch (RuntimeException re) {
            re.printStackTrace();
            log.error("An error occurred while parsing the exel file: {}", re.getMessage());
            return null;
        }

        try {
            for (ContributionItemEntry contributionItemEntry : contributionItemEntries) {
                ContributionScheduleItemDTO contributionScheduleItem = contributionItemEntry.getContributionScheduleItem();
                final BigDecimal individualPaymentAmount = contributionScheduleItem.getAmount().getAmount();
                totalAmount = totalAmount.add(individualPaymentAmount);
//                contributionScheduleItem.setContributionSchedule(contributionSchedule);

                final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
                instructedCurrencyAndAmount.setAmount(individualPaymentAmount);
                instructedCurrencyAndAmount.setCurrency(sourceCashAccount.getBaseCurrency());
                contributionScheduleItem.setAmount(instructedCurrencyAndAmount);

                EmployeeDTO employee = nationalIdEmployeeMap.get(contributionItemEntry.getSsn());
                if (employee == null) {
                    throw new NssaPaymentInvalidRequestException("EmployeeDTO " + contributionItemEntry.getFullName() + " not found.");
                }
                contributionScheduleItem.setEmployee(employee);

                contributionScheduleItems.add(contributionScheduleItem);
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
            log.error("An error occurred while reading excel file: {}", re.getMessage());
            throw new NssaPaymentInvalidRequestException("An error occurred. " + re.getMessage());
        }

        final CurrencyCode baseCurrency = sourceCashAccount.getBaseCurrency();

        final CurrencyAndAmount totalCurrencyAmount = new CurrencyAndAmount();
        final Optional<BigDecimal> totalAmountOptional = contributionScheduleItems.stream().map(p -> p.getAmount().getAmount()).reduce((bigDecimal, bigDecimal2) -> bigDecimal.add(bigDecimal2));
        totalCurrencyAmount.setAmount(new BigDecimal(makeNssaPaymentDTO.getApwcs()).add(totalAmountOptional.orElse(new BigDecimal(0))));
        totalCurrencyAmount.setCurrency(baseCurrency);
        contributionSchedule.setAmount(totalCurrencyAmount);

        configureContributionScheduleInformation(contributionSchedule, makeNssaPaymentDTO);

        final AccountDTO accountDTO = AccountDTO.builder()
                .baseCurrency(sourceCashAccount.getBaseCurrency())
                .name(sourceCashAccount.getAccountName())
                .number(sourceCashAccount.getAccountNumber())
                .accountOwner(contributionSchedule.getEmployer())
                .build();
        contributionSchedule.setAccount(accountDTO);

        contributionSchedule.setPobs(contributionScheduleItems.stream().mapToDouble(ContributionScheduleItemDTO::getPobs).sum());

        contributionSchedule.setContributionScheduleItems(contributionScheduleItems);
        return contributionSchedule;
    }

    private ContributionScheduleDTO readContributionScheduleFromCSVFile(MakeNssaPaymentDTO makeNssaPaymentDTO) throws NssaPaymentInvalidRequestException {
        log.info("Processing CSV contribution schedule file");

        final CashAccount sourceCashAccount = accountService.findAccountById(makeNssaPaymentDTO.getSourceAccountId());
        if (sourceCashAccount == null) {
            throw new NssaPaymentInvalidRequestException("Source account not found");
        }

        final List<EmployeeDTO> employeesByEmployerId = nssaClientsService.getEmployeesByEmployerId(makeNssaPaymentDTO.getClientId());

        final Map<String, EmployeeDTO> ssnEmployeeMap = employeesByEmployerId
                .stream().collect(Collectors.toMap(EmployeeDTO::getSsn, Function.identity(), (k1, k2) -> k1));

        System.out.printf("%20s\t%50s\n", "KEY", "VALUE");
        ssnEmployeeMap.entrySet().forEach(stringEmployeeEntry -> {
            System.out.printf("%20s\t%50s\n", stringEmployeeEntry.getKey(), stringEmployeeEntry.getValue());
        });

        final ContributionScheduleDTO contributionSchedule = new ContributionScheduleDTO();

        log.info("Reading uploaded schedule items file...");
        BigDecimal totalAmount = new BigDecimal(0);

        Resource file = storageService.loadAsResource(makeNssaPaymentDTO.getFileName());
        List<ContributionScheduleItemDTO> contributionScheduleItems = new ArrayList<>();
        List<ContributionItemEntry> contributionItemEntries = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader(Paths.get(file.getURI()))) {
            CsvToBean<ContributionItemEntry> csvPaymentEntryCsvToBean = new CsvToBeanBuilder(reader)
                    .withType(ContributionItemEntry.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<ContributionItemEntry> csvContributionItemEntryIterator = csvPaymentEntryCsvToBean.iterator();
            while (csvContributionItemEntryIterator.hasNext()) {
                contributionItemEntries.add(csvContributionItemEntryIterator.next());
            }
        } catch (IOException | RuntimeException re) {
            throw new NssaPaymentInvalidRequestException("Error Reading CSV File: " + re.getMessage());
        }

        try {
            for (ContributionItemEntry contributionItemEntry : contributionItemEntries) {
                ContributionScheduleItemDTO contributionScheduleItem = contributionItemEntry.getContributionScheduleItem();
                final BigDecimal individualPaymentAmount = contributionScheduleItem.getAmount().getAmount();
                totalAmount = totalAmount.add(individualPaymentAmount);
//                contributionScheduleItem.setContributionSchedule(contributionSchedule);

                final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
                instructedCurrencyAndAmount.setAmount(individualPaymentAmount);
                instructedCurrencyAndAmount.setCurrency(sourceCashAccount.getBaseCurrency());
                contributionScheduleItem.setAmount(instructedCurrencyAndAmount);

                EmployeeDTO employee = ssnEmployeeMap.get(contributionItemEntry.getSsn());
                if (employee == null) {
                    throw new NssaPaymentInvalidRequestException("EmployeeDTO " + contributionItemEntry.getFullName() + " not found.");
                }
                contributionScheduleItem.setEmployee(employee);

                contributionScheduleItems.add(contributionScheduleItem);
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
            log.error("An error occurred while reading csv file: {}", re.getMessage());
            throw new NssaPaymentInvalidRequestException("An error occurred. " + re.getMessage());
        }

        final CurrencyCode baseCurrency = sourceCashAccount.getBaseCurrency();

        final CurrencyAndAmount totalCurrencyAmount = new CurrencyAndAmount();
        final Optional<BigDecimal> totalAmountOptional = contributionScheduleItems.stream().map(p -> p.getAmount().getAmount()).reduce((bigDecimal, bigDecimal2) -> bigDecimal.add(bigDecimal2));
        totalCurrencyAmount.setAmount(new BigDecimal(makeNssaPaymentDTO.getApwcs()).add(totalAmountOptional.orElse(new BigDecimal(0))));
        totalCurrencyAmount.setCurrency(baseCurrency);
        contributionSchedule.setAmount(totalCurrencyAmount);

        configureContributionScheduleInformation(contributionSchedule, makeNssaPaymentDTO);

        final AccountDTO sourceAccountDTO = AccountDTO.builder()
                .baseCurrency(sourceCashAccount.getBaseCurrency())
                .name(sourceCashAccount.getAccountName())
                .number(sourceCashAccount.getAccountNumber())
                .accountOwner(contributionSchedule.getEmployer())
                .build();
        contributionSchedule.setAccount(sourceAccountDTO);

        contributionSchedule.setPobs(contributionScheduleItems.stream().mapToDouble(ContributionScheduleItemDTO::getPobs).sum());

        contributionSchedule.setContributionScheduleItems(contributionScheduleItems);
        log.info("File parser got {} items.", contributionScheduleItems.size());
        return contributionSchedule;
    }

    private void configureContributionScheduleInformation(ContributionScheduleDTO contributionSchedule, MakeNssaPaymentDTO makeNssaPaymentDTO) {
        contributionSchedule.setDateCreated(OffsetDateTime.now());
        contributionSchedule.setLastUpdated(OffsetDateTime.now());
        contributionSchedule.setEntityStatus(EntityStatus.DRAFT);
        contributionSchedule.setMonth(makeNssaPaymentDTO.getMonthOffsetDateTime());
        contributionSchedule.setId(UUID.randomUUID().toString());
        contributionSchedule.setClientReference(makeNssaPaymentDTO.getClientReference());
        contributionSchedule.setApwcs(makeNssaPaymentDTO.getApwcs());

        nonFinancialInstitutionService.getNonFinancialInstitutionById(makeNssaPaymentDTO.getClientId())
                .ifPresent(nonFinancialInstitution -> {
                    final EmployerDTO employerDTO = EmployerDTO.builder()
                            .anyBIC(nonFinancialInstitution.getAnyBIC())
                            .bic(nonFinancialInstitution.getBIC())
                            .emailAddress(nonFinancialInstitution.getEmailAddress())
                            .name(nonFinancialInstitution.getName())
                            .physicalAddress(nonFinancialInstitution.getPhysicalAddress())
                            .ssr(nonFinancialInstitution.getSsr())
                            .build();
                    contributionSchedule.setEmployer(employerDTO);
                });
    }
}
