package zw.co.esolutions.zpas.services.impl.payments.verification.sse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.services.impl.payments.cache.PaymentsCacheProcessor;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.domain.PaymentStatusInfo;

@Slf4j
@Component
public class ScheduledService {
    @Autowired
    private PaymentsCacheProcessor cacheProcessor;
//
//    @Autowired
//    private Acmt024ResponseSimulator acmt024ResponseSimulator;

    private static long counter = 0;

    private final SseEmitter emitter = new SseEmitter();
    private String verificationRequestId;

    public SseEmitter getInfiniteMessages(String verificationRequestId) {
        this.verificationRequestId = verificationRequestId;
        return emitter;
    }

    @Scheduled(fixedDelay = 2000)
    void timerHandler() {
        if (!StringUtils.isEmpty(verificationRequestId)) {
            PaymentStatusInfo paymentStatusInfo = cacheProcessor.getPaymentStatusInfo(verificationRequestId);
//            if(counter % 5 == 0) {
//                log.info("Simulating Hub response for ACMT024.");
//                acmt024ResponseSimulator.simulateIdentificationVerificationReport(verificationRequestId);
//            }
            try {
                emitter.send(paymentStatusInfo, MediaType.APPLICATION_JSON);
            } catch (Exception e) {
                emitter.completeWithError(e);
            }
        } else {
//            log.error("Request ID not set");
        }
        counter++;
        if(counter == Long.MAX_VALUE) {
            counter = 0;
        }
    }

    public static String getVerificationStatus(Payment payment) {
        switch (payment.getEntityStatus()) {
            case PENDING_VERIFICATION:
                return "IN_PROGRESS";
            case FAILED:
                return "FAILED";
            case PENDING_APPROVAL:
                return "SUCCESS";
            default:
                return "INDETERMINATE";
        }
    }

    public static String getVerificationInfo(Payment payment) {
        switch (payment.getEntityStatus()) {
            case PENDING_VERIFICATION:
                return "Verification in progress.";
            case FAILED:
                return "Verification completed. Information incorrect.";
            case PENDING_APPROVAL:
                return "Verification completed. Information correct.";
            default:
                return "Could not determine verification status";
        }
    }
}
