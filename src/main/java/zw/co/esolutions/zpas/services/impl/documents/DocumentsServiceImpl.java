package zw.co.esolutions.zpas.services.impl.documents;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.datatype.ActiveCurrencyAndAmount;
import zw.co.esolutions.zpas.dto.documents.DocumentDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.DocumentPartyRoleRepository;
import zw.co.esolutions.zpas.repository.DocumentRepository;
import zw.co.esolutions.zpas.services.iface.documents.DocumentsService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by alfred on 11 Mar 2019
 */
@Slf4j
@Service
public class DocumentsServiceImpl implements DocumentsService {
    @Autowired
    private ClientService clientService;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private DocumentPartyRoleRepository documentPartyRoleRepository;

    @Override
    public Optional<Document> createDocument(DocumentDTO documentDTO) {
        final Document document = getDocumentFromDTO(documentDTO);
        if(document != null) {
            final Document savedDocument = documentRepository.save(document);
            return Optional.ofNullable(savedDocument);
        }
        return Optional.empty();
    }

    @Override
    public List<Document> getDocumentsByClientId(String clientId) {
        return documentPartyRoleRepository.findAllByPlayer_Id(clientId).stream().flatMap(role -> {
                    return documentRepository.findAllByPartyRole_Id(role.getId()).stream();
                }).collect(Collectors.toList());
    }

    @Override
    public Optional<Document> getDocumentById(String documentId) {
        return documentRepository.findById(documentId);
    }

    private Document getDocumentFromDTO(DocumentDTO documentDTO) {
        final Optional<Person> clientOptional = clientService.getPersonById(documentDTO.getClientId());

        return clientOptional.map(person -> {
            LocalDate issueLocalDate = LocalDate.parse(documentDTO.getIssueDate(), DateTimeFormatter.BASIC_ISO_DATE);

            Document document = new Document();
            document.setId(GenerateKey.generateEntityId());
            document.setDateCreated(OffsetDateTime.now());
            document.setLastUpdated(OffsetDateTime.now());
            document.setEntityStatus(EntityStatus.ACTIVE);

            final Scheme scheme = new Scheme();
            scheme.setId(GenerateKey.generateEntityId());
            scheme.setEntityStatus(EntityStatus.ACTIVE);
            scheme.setDateCreated(OffsetDateTime.now());
            scheme.setLastUpdated(OffsetDateTime.now());
            scheme.setNameShort("Default");
            scheme.setCode("Default");
            scheme.setIdentification(new ArrayList<>());
            scheme.setVersion("0");
            scheme.setNameLong("Default");
            scheme.setDescription("Default Scheme");
            scheme.setDomainValueCode("Default");
            scheme.setDomainValueName("Default");

            final DocumentPartyRole documentPartyRole = new DocumentPartyRole();
            documentPartyRole.setDocument(Arrays.asList(document));
            documentPartyRole.setId(GenerateKey.generateEntityId());
            documentPartyRole.setEntityStatus(EntityStatus.ACTIVE);
            documentPartyRole.setDateCreated(OffsetDateTime.now());
            documentPartyRole.setLastUpdated(OffsetDateTime.now());
            documentPartyRole.setEntityVersion(0L);
            documentPartyRole.setPlayer(Arrays.asList(person));
            documentPartyRole.setContactPersonRole(person.getContactPersonRole());
            documentPartyRole.setPartyRoleCode(PartyRoleCode.Custodian);

            final DocumentPartyRole savedDocumentPartyRole = documentPartyRoleRepository.save(documentPartyRole);

            final GenericIdentification genericIdentification = new GenericIdentification();
            genericIdentification.setId(GenerateKey.generateEntityId());
            genericIdentification.setEntityStatus(EntityStatus.ACTIVE);
            genericIdentification.setDateCreated(OffsetDateTime.now());
            genericIdentification.setLastUpdated(OffsetDateTime.now());
            genericIdentification.setIdentification(documentDTO.getDocumentName());
            genericIdentification.setIssueDate(issueLocalDate.atTime(OffsetTime.now()));
            genericIdentification.setExpiryDate(OffsetDateTime.now().plusYears(100));
//            genericIdentification.setScheme(scheme);
            genericIdentification.setIdentificationForContactPoint(person.getContactPoint());
            genericIdentification.setIdentifiedLocation(new Location());
            if(person.getPersonIdentification().size() > 0) {
                genericIdentification.setRelatedPartyIdentification(person.getPersonIdentification().get(0));
            }
            genericIdentification.setIdentificationForBankTransaction(Lists.newArrayList());
            genericIdentification.setIdentifiedDocument(document);
            genericIdentification.setRelatedStatusReason(new StatusReason());
            genericIdentification.setEntityVersion(0L);

            CopyDuplicateCode copyDuplicateCode = CopyDuplicateCode.None;
            DataSetTypeCode dataSetTypeCode = DataSetTypeCode.None;
            DocumentTypeCode documentTypeCode = DocumentTypeCode.None;
            LanguageCode languageCode = LanguageCode.None;
            try {
                copyDuplicateCode = CopyDuplicateCode.valueOf(documentDTO.getCopyDuplicate());
                dataSetTypeCode = DataSetTypeCode.valueOf(documentDTO.getDataSetType());
                documentTypeCode = DocumentTypeCode.valueOf(documentDTO.getType());
                languageCode = LanguageCode.valueOfCodeName(documentDTO.getLanguageCode());
            } catch (RuntimeException re) {

            }

            document.setIssueDate(issueLocalDate.atTime(OffsetTime.now()));
            document.setCopyDuplicate(copyDuplicateCode);
            document.setPlaceOfStorage(Lists.newArrayList());
            document.setType(documentTypeCode);
            document.setStatus(EntityStatus.ACTIVE.name());
            document.setPartyRole(Arrays.asList(savedDocumentPartyRole));
            document.setPurpose(documentDTO.getPurpose());
            document.setAmount(new ActiveCurrencyAndAmount());
            document.setPlaceOfIssue(person.getDomicile());
            document.setDocumentVersion(0L);
            document.setDataSetType(dataSetTypeCode);
            document.setSignedIndicator(false);
            document.setRemittedAmount(documentDTO.getRemittedAmount());
            document.setLanguage(languageCode);
            document.setDocumentIdentification(genericIdentification);
            document.setEntityVersion(0L);
            return document;
        }).orElseGet(() -> {
            return null;
        });
    }
}
