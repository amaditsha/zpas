package zw.co.esolutions.zpas.services.impl.payments;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.payments.MakePaymentDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.iso.msg.pain001_001_08.Document;
import zw.co.esolutions.zpas.iso.msg.pain001_001_08.*;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.CustomPartyRepository;
import zw.co.esolutions.zpas.repository.SchemeRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsAcquiringService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.xml.bind.*;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.StringReader;
import java.lang.System;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alfred on 05 Mar 2019
 */
@Slf4j
@Service
public class PaymentsAcquiringServiceImpl implements PaymentsAcquiringService {
    private static final String FILES_FOLDER = StorageProperties.BASE_LOCATION;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private CustomPartyRepository customPartyRepository;

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;
    static JAXBContext jaxbContextForPain;

    static {
        try {
            jaxbContext = JAXBContext.newInstance(Document.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            jaxbContextForPain = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.pain001_001_08");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public Payment buildPaymentFromDocument(Document document) {
        return buildMessage(document);
    }

    @Override
    public Payment buildPaymentFromFile(String fileName) {
        return buildMessage(readDocumentFromFile(fileName));
    }

    @Override
    public Optional<Payment> buildPaymentFromXml(String xml) {
        return Optional.ofNullable(buildMessage(readDocumentFromXml(xml)));
    }

    public Document readDocumentFromFile(String fileName) {
        Path xmlFilePath = Paths.get(FILES_FOLDER).resolve(fileName);

        Unmarshaller unmarshaller = null;
        try {
            unmarshaller = jaxbContext.createUnmarshaller();
            Source source = new StreamSource(Files.newInputStream(xmlFilePath));
            final JAXBElement<Document> documentJAXBElement = unmarshaller.unmarshal(source, Document.class);
            return documentJAXBElement.getValue();
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Document readDocumentFromXml(String xml) {

        log.info("Parsing the ISO20022 message..");

        Unmarshaller jaxbUnmarshaller;
        try {
            jaxbUnmarshaller = jaxbContextForPain.createUnmarshaller();

            StringReader reader = new StringReader(xml);
            Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
            return document;
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Payment buildMessage(Document document) {
        if (document == null) {
            return null;
        }
        final CustomerCreditTransferInitiationV08 customerCreditTransferInitiationV08 = document.getCstmrCdtTrfInitn();
        final GroupHeader48 groupHeader48 = customerCreditTransferInitiationV08.getGrpHdr();
        final List<SupplementaryData1> supplementaryData1s = customerCreditTransferInitiationV08.getSplmtryData();
        final List<PaymentInstruction22> paymentInstruction22s = customerCreditTransferInitiationV08.getPmtInf();

        final List<IndividualPayment> individualPayments = new ArrayList<>();

        BulkPayment bulkPayment = new BulkPayment();
        bulkPayment.setGroups(individualPayments);
        bulkPayment.setHeaderID(groupHeader48.getMsgId());
        bulkPayment.setProcessingDate(XmlDateUtil.getOffsetDateTime(groupHeader48.getCreDtTm()));

        final List<PaymentObligation> paymentObligations = new ArrayList<>();
        bulkPayment.setPaymentObligation(paymentObligations);
        BigDecimal totalAmount = new BigDecimal("0");

        for (PaymentInstruction22 paymentInstruction22 : paymentInstruction22s) {
            final String pmtInfId = paymentInstruction22.getPmtInfId();
            final PaymentMethod3Code paymentMethod3Code = paymentInstruction22.getPmtMtd();
            final BigDecimal ctrlSum = paymentInstruction22.getCtrlSum();
            final DateAndDateTimeChoice reqdExctnDt = paymentInstruction22.getReqdExctnDt();
            final PartyIdentification43 debtor = paymentInstruction22.getDbtr();
            final CashAccount24 debtorAccount = paymentInstruction22.getDbtrAcct();
            final BranchAndFinancialInstitutionIdentification5 debtorAgent = paymentInstruction22.getDbtrAgt();
            final CashAccount24 debtorAgentAccount = paymentInstruction22.getDbtrAgtAcct();
            final CashAccount24 chrgsAcct = paymentInstruction22.getChrgsAcct();

            final Optional<CashAccount> cashAccountOptional = accountService.findAccountByIban(debtorAccount.getId().getIBAN());
            if (!cashAccountOptional.isPresent()) {
                return null;
            }
            final CashAccount cashAccount = cashAccountOptional.get();

            final List<CreditTransferTransaction26> transferTransaction26s = paymentInstruction22.getCdtTrfTxInf();
            transferTransaction26s.forEach(creditTransferTransaction26 -> {

                IndividualPayment individualPayment = new CreditTransfer();
                individualPayment.setPaymentInstrument(PaymentInstrumentCode.CustomerCreditTransfer);
                individualPayment.setAccount(cashAccount);

                individualPayment.setBulkPayment(bulkPayment);
                individualPayment.setPaymentObligation(new ArrayList<>());

                String ccy = "";
                BigDecimal amountValue = new BigDecimal(0);
                final Optional<EquivalentAmount2> equivalentAmount2Optional = Optional.ofNullable(creditTransferTransaction26.getAmt().getEqvtAmt());
                if (equivalentAmount2Optional.isPresent()) {
                    final EquivalentAmount2 equivalentAmount2 = equivalentAmount2Optional.get();
                    ccy = equivalentAmount2.getAmt().getCcy();
                    amountValue = equivalentAmount2.getAmt().getValue();
                }

                final Optional<ActiveOrHistoricCurrencyAndAmount> instdAmtOptional = Optional.ofNullable(creditTransferTransaction26.getAmt().getInstdAmt());
                if (instdAmtOptional.isPresent()) {
                    final ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount = instdAmtOptional.get();
                    ccy = activeOrHistoricCurrencyAndAmount.getCcy();
                    amountValue = activeOrHistoricCurrencyAndAmount.getValue();
                }

                final CurrencyCode currencyOfTransfer = new CurrencyCode();
                currencyOfTransfer.setCodeName(ccy);
                currencyOfTransfer.setName(ccy);

                final CurrencyAndAmount amount = new CurrencyAndAmount();
                amount.setAmount(amountValue);
                final CurrencyCode amountCurrencyCode = new CurrencyCode();
                amountCurrencyCode.setCodeName(ccy);
                amount.setCurrency(amountCurrencyCode);
                individualPayment.setAmount(amount);

                individualPayment.setCurrencyOfTransfer(currencyOfTransfer);
                final List<CreditInstrument> creditMethod = new ArrayList<>();
                CreditInstrument creditInstrument = new CreditInstrument();
                creditInstrument.setId(GenerateKey.generateEntityId());
                creditInstrument.setEntityStatus(EntityStatus.ACTIVE);
                creditInstrument.setDateCreated(OffsetDateTime.now());
                creditInstrument.setLastUpdated(OffsetDateTime.now());
                creditInstrument.setEntityVersion(0L);
                creditInstrument.setRelatedPayment(Arrays.asList(individualPayment));
                creditInstrument.setMethod(PaymentMethodCode.valueOfCodeName(paymentMethod3Code.value()));
                creditInstrument.setCreditInstrumentIdentification("");
                creditInstrument.setNetAmount(amount);
                creditInstrument.setDeadline(OffsetDateTime.now());

                individualPayment.setCreditMethod(creditMethod);
                individualPayment.setType(PaymentTypeCode.None);
                individualPayment.setInstructedAmount(amount);
                individualPayment.setPriority(PriorityCode.Normal);

                individualPayment.setValueDate(XmlDateUtil.getOffsetDateTime(reqdExctnDt.getDtTm()));
                individualPayment.setPaymentStatus(new ArrayList<>());
                individualPayment.setPoolingAdjustmentDate(OffsetDateTime.now());
                individualPayment.setEquivalentAmount(amount.getAmount());
                individualPayment.setCurrencyExchange(new ArrayList<>());
                individualPayment.setPaymentRelatedIdentifications(new ArrayList<>());

                individualPayment.setStandardSettlementInstructions("");

                List<PaymentIdentification> paymentIdentifications = new ArrayList<>();
                PaymentIdentification paymentIdentification = new PaymentIdentification();
                paymentIdentification.setExecutionIdentification(paymentInstruction22.getPmtInfId());
                paymentIdentification.setEndToEndIdentification(creditTransferTransaction26.getPmtId().getEndToEndId());
                paymentIdentification.setInstructionIdentification(creditTransferTransaction26.getPmtId().getInstrId());
                paymentIdentification.setTransactionIdentification(creditTransferTransaction26.getPmtId().getEndToEndId());
                paymentIdentification.setClearingSystemReference(creditTransferTransaction26.getPmtId().getEndToEndId());
                paymentIdentification.setCreditorReference(creditTransferTransaction26.getPmtId().getEndToEndId());
                paymentIdentification.setPayment(individualPayment);
                paymentIdentification.setId(GenerateKey.generateEntityId());
                paymentIdentification.setDateCreated(OffsetDateTime.now());
                paymentIdentification.setLastUpdated(OffsetDateTime.now());
                paymentIdentification.setEntityStatus(EntityStatus.ACTIVE);
                paymentIdentification.setEntityVersion(0L);
                paymentIdentification.setCounterpartyReference(GenerateKey.generateRef());
                paymentIdentification.setIdentification(creditTransferTransaction26.getPmtId().getEndToEndId());
                paymentIdentification.setCommonIdentification(creditTransferTransaction26.getPmtId().getEndToEndId());
                paymentIdentification.setMatchingReference(creditTransferTransaction26.getPmtId().getEndToEndId());
                paymentIdentification.setUniqueTradeIdentifier(creditTransferTransaction26.getPmtId().getEndToEndId());
                paymentIdentifications.add(paymentIdentification);
                individualPayment.setPaymentRelatedIdentifications(paymentIdentifications);

                individualPayment.setId(UUID.randomUUID().toString());
                individualPayment.setDateCreated(OffsetDateTime.now());
                individualPayment.setLastUpdated(OffsetDateTime.now());
                individualPayment.setEntityStatus(EntityStatus.DRAFT);
                individualPayment.setEntityVersion(0L);
                addIndividualPaymentDebtorPartyRoles(individualPayment);

                final MakePaymentDTO makePaymentDTO = MakePaymentDTO.builder()
                        .beneficiaryName(Optional.ofNullable(creditTransferTransaction26.getCdtr()).map(p -> p.getNm()).orElse(""))
                        .nationalId("")
                        .passportNumber("")
                        .destinationAccountNumber(Optional.ofNullable(creditTransferTransaction26.getCdtrAcct()).map(cashAccount24 -> cashAccount.getId()).orElse(""))
                        .destinationFinancialInstitutionId(Optional.ofNullable(creditTransferTransaction26.getCdtrAgt()).map(bfii -> {
                            final FinancialInstitutionIdentification8 finInstnId = bfii.getFinInstnId();
                            if (finInstnId != null) {
                                return finInstnId.getBICFI();
                            } else {
                                return "";
                            }
                        }).orElse(""))
                        .build();
                addIndividualPaymentCreditorPartyRoles(individualPayment, makePaymentDTO);

                individualPayments.add(individualPayment);
            });
        }

        for (IndividualPayment individualPayment : individualPayments) {
            final BigDecimal individualPaymentAmount = individualPayment.getAmount().getAmount();
            totalAmount = totalAmount.add(individualPaymentAmount);
        }

        final PaymentTypeCode paymentTypeCode = PaymentTypeCode.None;
        bulkPayment.setType(paymentTypeCode);

        //final configuration on batch and bulk
        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        instructedCurrencyAndAmount.setAmount(totalAmount);
        CurrencyCode instructedCurrencyAndAmountCurrencyCode = new CurrencyCode();
        instructedCurrencyAndAmountCurrencyCode.setCodeName("");
        instructedCurrencyAndAmountCurrencyCode.setName("");
        instructedCurrencyAndAmount.setCurrency(instructedCurrencyAndAmountCurrencyCode);
        log.info("Bulk Amount is: {}", instructedCurrencyAndAmount);

        bulkPayment.setInstructedAmount(instructedCurrencyAndAmount);
        bulkPayment.setEquivalentAmount(instructedCurrencyAndAmount.getAmount());
        bulkPayment.setAmount(instructedCurrencyAndAmount);

        bulkPayment.setPriority(PriorityCode.Normal);
        bulkPayment.setValueDate(OffsetDateTime.now());

//        final CashAccount account = new CashAccount();
//        bulkPayment.setAccount(account);

        final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionByAnyBIC(groupHeader48.getInitgPty().getId().getOrgId().getOthr().get(0).getId());

        financialInstitutionOptional.ifPresent(financialInstitution -> {

            InitiatingPartyRole initiatingPartyRole = new InitiatingPartyRole();
            initiatingPartyRole.setPayment(Arrays.asList(bulkPayment));
//            initiatingPartyRole.setCashAccount(Arrays.asList(bulkPayment.getAccount()));
            initiatingPartyRole.setId(GenerateKey.generateEntityId());
            initiatingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
            initiatingPartyRole.setDateCreated(OffsetDateTime.now());
            initiatingPartyRole.setLastUpdated(OffsetDateTime.now());
            initiatingPartyRole.setEntityVersion(0L);
            initiatingPartyRole.setPlayer(Arrays.asList(financialInstitution));
            initiatingPartyRole.setHasCustomParty(false);
            initiatingPartyRole.setPartyRoleCode(PartyRoleCode.FirstAgent);
            bulkPayment.setPartyRole(Arrays.asList(initiatingPartyRole));
        });

        bulkPayment.setPoolingAdjustmentDate(OffsetDateTime.now());

        final List<PaymentIdentification> paymentRelatedIdentifications = new ArrayList<>();
        bulkPayment.setPaymentRelatedIdentifications(paymentRelatedIdentifications);

        bulkPayment.setEquivalentAmount(totalAmount);

        bulkPayment.setOriginalPayment(bulkPayment);

        bulkPayment.setReturnPayment(new ArrayList<>());

        bulkPayment.setInvoiceReconciliation(new ArrayList<>());

        bulkPayment.setPaymentInstrument(PaymentInstrumentCode.CustomerCreditTransfer);

        bulkPayment.setId(UUID.randomUUID().toString());
        bulkPayment.setDateCreated(OffsetDateTime.now());
        bulkPayment.setLastUpdated(OffsetDateTime.now());
        bulkPayment.setEntityStatus(EntityStatus.ACTIVE);
        bulkPayment.setEntityVersion(0L);

        return bulkPayment;
    }

    private void addIndividualPaymentCreditorPartyRoles(final IndividualPayment individualPayment, final MakePaymentDTO makePaymentDTO) {
        final String destinationFinancialInstitutionId = makePaymentDTO.getDestinationFinancialInstitutionId();
        final Optional<FinancialInstitution> destinationFinancialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(destinationFinancialInstitutionId);

        /**
         * Configure Individual Payment Roles
         */
        //The Payment
        final List<Payment> individualPayments = Arrays.asList(individualPayment);
        //The affected Bulk Payment accounts
        final List<CashAccount> cashAccounts = Arrays.asList(individualPayment.getAccount());

        final List<AccountPartyRole> accountPartyRoles = individualPayment.getAccount().getPartyRole();

        //account servicing roles
        final List<AccountPartyRole> accountServicerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountServicerRole);
        }).collect(Collectors.toList());

        //The Role Players servicing this payment
        final List<RolePlayer> accountServicerRolePlayers = accountServicerRoles.stream()
                .flatMap(accountServicerRole -> accountServicerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //account owning roles
        final List<AccountPartyRole> accountOwnerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountOwnerRole);
        }).collect(Collectors.toList());

        //The Role Players owning this payment
        final List<RolePlayer> accountOwnerRolePlayers = accountOwnerRoles.stream()
                .flatMap(accountOwnerRole -> accountOwnerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //Debtor Role
        DebtorRole debtorRole = new DebtorRole();
        debtorRole.setPayment(individualPayments);
        debtorRole.setCashAccount(cashAccounts);
        debtorRole.setId(GenerateKey.generateEntityId());
        debtorRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorRole.setDateCreated(OffsetDateTime.now());
        debtorRole.setLastUpdated(OffsetDateTime.now());
        debtorRole.setEntityVersion(0L);
        debtorRole.setPlayer(accountOwnerRolePlayers);
        debtorRole.setEntry(null);
        debtorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        //Debtor Agent Role
        DebtorAgentRole debtorAgentRole = new DebtorAgentRole();
        debtorAgentRole.setPayment(individualPayments);
        debtorAgentRole.setCashAccount(cashAccounts);
        debtorAgentRole.setId(GenerateKey.generateEntityId());
        debtorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorAgentRole.setDateCreated(OffsetDateTime.now());
        debtorAgentRole.setLastUpdated(OffsetDateTime.now());
        debtorAgentRole.setEntityVersion(0L);
        debtorAgentRole.setPlayer(accountServicerRolePlayers);
        debtorAgentRole.setEntry(null);
        debtorAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Initiating Party Role
        InitiatingPartyRole initiatingPartyRole = new InitiatingPartyRole();
        initiatingPartyRole.setPayment(individualPayments);
        initiatingPartyRole.setCashAccount(cashAccounts);
        initiatingPartyRole.setId(GenerateKey.generateEntityId());
        initiatingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
        initiatingPartyRole.setDateCreated(OffsetDateTime.now());
        initiatingPartyRole.setLastUpdated(OffsetDateTime.now());
        initiatingPartyRole.setEntityVersion(0L);
        initiatingPartyRole.setPlayer(accountOwnerRolePlayers);
        initiatingPartyRole.setEntry(null);
        initiatingPartyRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Forwarding Party Role
        ForwardingAgentRole forwardingAgentRole = new ForwardingAgentRole();
        forwardingAgentRole.setPayment(individualPayments);
        forwardingAgentRole.setCashAccount(cashAccounts);
        forwardingAgentRole.setId(GenerateKey.generateEntityId());
        forwardingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        forwardingAgentRole.setDateCreated(OffsetDateTime.now());
        forwardingAgentRole.setLastUpdated(OffsetDateTime.now());
        forwardingAgentRole.setEntityVersion(0L);
        forwardingAgentRole.setPlayer(accountServicerRolePlayers);
        forwardingAgentRole.setEntry(null);
        forwardingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Instructing Agent Role
        InstructingAgentRole instructingAgentRole = new InstructingAgentRole();
        instructingAgentRole.setPayment(individualPayments);
        instructingAgentRole.setCashAccount(cashAccounts);
        instructingAgentRole.setId(GenerateKey.generateEntityId());
        instructingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        instructingAgentRole.setDateCreated(OffsetDateTime.now());
        instructingAgentRole.setLastUpdated(OffsetDateTime.now());
        instructingAgentRole.setEntityVersion(0L);
        instructingAgentRole.setPlayer(accountServicerRolePlayers);
        instructingAgentRole.setEntry(null);
        instructingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);


        /**
         * Configure Payment Parties for individual payments in the bulk
         */
        CustomParty customParty = new CustomParty();
        customParty.setId(GenerateKey.generateEntityId());
        customParty.setDateCreated(OffsetDateTime.now());
        customParty.setLastUpdated(OffsetDateTime.now());
        customParty.setEntityStatus(EntityStatus.ACTIVE);
        customParty.setEntityVersion(0L);
        customParty.setName(makePaymentDTO.getBeneficiaryName());
        customParty.setNationalId(makePaymentDTO.getNationalId());
        customParty.setPassportNumber(makePaymentDTO.getPassportNumber());
        customParty.setAccountNumber(makePaymentDTO.getDestinationAccountNumber());
        customParty.setFinancialInstitutionIdentification(makePaymentDTO.getDestinationFinancialInstitutionId());

        final CustomParty savedCustomParty = customPartyRepository.save(customParty);

        //Scheme
//        final Scheme scheme = new Scheme();
//        scheme.setId(GenerateKey.generateEntityId());
//        scheme.setEntityStatus(EntityStatus.ACTIVE);
//        scheme.setDateCreated(OffsetDateTime.now());
//        scheme.setLastUpdated(OffsetDateTime.now());
//        scheme.setNameShort("Default");
//        scheme.setCode("");
//        scheme.setIdentification(new ArrayList<>());
//        scheme.setVersion("0");
//        scheme.setNameLong("Default Scheme");
//        scheme.setDescription("This is the default scheme for the creditor role.");
//        scheme.setDomainValueCode("Default");
//        scheme.setDomainValueName("Default");
//        schemeRepository.save(scheme);

        //Creditor Role
        CreditorRole creditorRole = new CreditorRole();
//        scheme.setCreditorRole(creditorRole);
//        creditorRole.setScheme(scheme);
        creditorRole.setPayment(individualPayments);
        creditorRole.setCustomParty(savedCustomParty);
        creditorRole.setHasCustomParty(true);
        creditorRole.setId(GenerateKey.generateEntityId());
        creditorRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorRole.setDateCreated(OffsetDateTime.now());
        creditorRole.setLastUpdated(OffsetDateTime.now());
        creditorRole.setEntityVersion(0L);
        creditorRole.setEntry(null);
        creditorRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Creditor Agent Role
        CreditorAgentRole creditorAgentRole = new CreditorAgentRole();
        creditorAgentRole.setPayment(individualPayments);
        destinationFinancialInstitutionOptional.ifPresent(financialInstitution -> {
            creditorAgentRole.setPlayer(Arrays.asList(financialInstitution));
        });
        creditorAgentRole.setId(GenerateKey.generateEntityId());
        creditorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorAgentRole.setDateCreated(OffsetDateTime.now());
        creditorAgentRole.setLastUpdated(OffsetDateTime.now());
        creditorAgentRole.setEntityVersion(0L);
        creditorAgentRole.setEntry(null);
        creditorAgentRole.setPartyRoleCode(PartyRoleCode.FinalAgent);

        List<PaymentPartyRole> individualPaymentPartyRoles = Arrays.asList(
                creditorRole, creditorAgentRole, debtorRole, debtorAgentRole,
                initiatingPartyRole, forwardingAgentRole, instructingAgentRole
        );

        individualPayment.setPartyRole(individualPaymentPartyRoles);
    }

    private void addIndividualPaymentDebtorPartyRoles(final IndividualPayment individualPayment) {
        /**
         * Configure Bulk Payment Roles
         */
        //The affected Bulk Payments
        final List<Payment> bulkPayments = Arrays.asList(individualPayment);
        //The affected Bulk Payment accounts
        final List<CashAccount> cashAccounts = Arrays.asList(individualPayment.getAccount());

        final List<AccountPartyRole> accountPartyRoles = individualPayment.getAccount().getPartyRole();

        //account servicing roles
        final List<AccountPartyRole> accountServicerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountServicerRole);
        }).collect(Collectors.toList());

        //The Role Players servicing this payment
        final List<RolePlayer> accountServicerRolePlayers = accountServicerRoles.stream()
                .flatMap(accountServicerRole -> accountServicerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //account owning roles
        final List<AccountPartyRole> accountOwnerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountOwnerRole);
        }).collect(Collectors.toList());

        //The Role Players owning this payment
        final List<RolePlayer> accountOwnerRolePlayers = accountOwnerRoles.stream()
                .flatMap(accountOwnerRole -> accountOwnerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //Debtor Role
        DebtorRole debtorRole = new DebtorRole();
        debtorRole.setPayment(bulkPayments);
        debtorRole.setCashAccount(cashAccounts);
        debtorRole.setId(GenerateKey.generateEntityId());
        debtorRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorRole.setDateCreated(OffsetDateTime.now());
        debtorRole.setLastUpdated(OffsetDateTime.now());
        debtorRole.setEntityVersion(0L);
        debtorRole.setPlayer(accountOwnerRolePlayers);
//        debtorRole.setContactPersonRole(accountOwnerRoles.stream().map(aor -> aor.getContactPersonRole()).findFirst().orElse(null));
        debtorRole.setEntry(null);
        debtorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        //Debtor Agent Role
        DebtorAgentRole debtorAgentRole = new DebtorAgentRole();
        debtorAgentRole.setPayment(bulkPayments);
        debtorAgentRole.setCashAccount(cashAccounts);
        debtorAgentRole.setId(GenerateKey.generateEntityId());
        debtorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorAgentRole.setDateCreated(OffsetDateTime.now());
        debtorAgentRole.setLastUpdated(OffsetDateTime.now());
        debtorAgentRole.setEntityVersion(0L);
        debtorAgentRole.setPlayer(accountServicerRolePlayers);
//        debtorAgentRole.setContactPersonRole(accountServicerRoles.stream().map(aor -> aor.getContactPersonRole()).findFirst().orElse(null));
        debtorAgentRole.setEntry(null);
        debtorAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Initiating Party Role
        InitiatingPartyRole initiatingPartyRole = new InitiatingPartyRole();
        initiatingPartyRole.setPayment(bulkPayments);
        initiatingPartyRole.setCashAccount(cashAccounts);
        initiatingPartyRole.setId(GenerateKey.generateEntityId());
        initiatingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
        initiatingPartyRole.setDateCreated(OffsetDateTime.now());
        initiatingPartyRole.setLastUpdated(OffsetDateTime.now());
        initiatingPartyRole.setEntityVersion(0L);
        initiatingPartyRole.setPlayer(accountOwnerRolePlayers);
        initiatingPartyRole.setEntry(null);
        initiatingPartyRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Forwarding Party Role
        ForwardingAgentRole forwardingAgentRole = new ForwardingAgentRole();
        forwardingAgentRole.setPayment(bulkPayments);
        forwardingAgentRole.setCashAccount(cashAccounts);
        forwardingAgentRole.setId(GenerateKey.generateEntityId());
        forwardingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        forwardingAgentRole.setDateCreated(OffsetDateTime.now());
        forwardingAgentRole.setLastUpdated(OffsetDateTime.now());
        forwardingAgentRole.setEntityVersion(0L);
        forwardingAgentRole.setPlayer(accountServicerRolePlayers);
//        forwardingAgentRole.setContactPersonRole(accountServicerRoles.stream().map(aor -> aor.getContactPersonRole()).findFirst().orElse(null));
        forwardingAgentRole.setEntry(null);
        forwardingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Instructing Agent Role
        InstructingAgentRole instructingAgentRole = new InstructingAgentRole();
        instructingAgentRole.setPayment(bulkPayments);
        instructingAgentRole.setCashAccount(cashAccounts);
        instructingAgentRole.setId(GenerateKey.generateEntityId());
        instructingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        instructingAgentRole.setDateCreated(OffsetDateTime.now());
        instructingAgentRole.setLastUpdated(OffsetDateTime.now());
        instructingAgentRole.setEntityVersion(0L);
        instructingAgentRole.setPlayer(accountServicerRolePlayers);
//        instructingAgentRole.setContactPersonRole(accountServicerRoles.stream().map(aor -> aor.getContactPersonRole()).findFirst().orElse(null));
        instructingAgentRole.setEntry(null);
        instructingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);


        List<PaymentPartyRole> bulkPaymentPartyRoles = Arrays.asList(
                debtorRole, debtorAgentRole, initiatingPartyRole, forwardingAgentRole, instructingAgentRole
        );
        individualPayment.setPartyRole(bulkPaymentPartyRoles);
    }

    public static void main(String[] args) {
        for (String w : "AlfredSamanga".split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            System.out.print(w + " ");
        }
    }
}
