package zw.co.esolutions.zpas.services.impl.enversaudit;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.model.CashAccount;
import zw.co.esolutions.zpas.model.enversaudit.AccountAuditResult;
import zw.co.esolutions.zpas.model.enversaudit.AuditedRevisionEntity;
import zw.co.esolutions.zpas.services.iface.enversaudit.AuditAccountService;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class AuditAccountServiceImpl implements AuditAccountService {
    @Autowired
    EntityManager entityManager;

    @Override
    public List<AccountAuditResult> auditAccount(String accountId) {
        List<Object[]> results = (List<Object[]>) AuditReaderFactory
                .get( entityManager )
                .createQuery()
                .forRevisionsOfEntity( CashAccount.class, false, true )
                .add( AuditEntity.property( "id" ).eq( accountId ) )
                .getResultList();

        log.info("The account revision size is {}", results.size());

        return results.stream()
                .map(objectArray ->
                        new AccountAuditResult((CashAccount) objectArray[0], (AuditedRevisionEntity) objectArray[1]))
                .collect(Collectors.toList());
    }
}
