package zw.co.esolutions.zpas.services.nssa.iface;

import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleDTO;
import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleItemDTO;
import zw.co.esolutions.zpas.dto.nssa.MakeNssaPaymentDTO;
import zw.co.esolutions.zpas.services.nssa.impl.exceptions.NssaPaymentInvalidRequestException;

import java.util.List;
import java.util.Optional;

/**
 * Create by alfred on 19 Jul 2019
 */
public interface ContributionsService {
    Optional<ContributionScheduleDTO> createContributionSchedule(MakeNssaPaymentDTO makeNssaPaymentDTO) throws NssaPaymentInvalidRequestException;
    Optional<ContributionScheduleDTO> getContributionScheduleById(String contributionScheduleId);
    List<ContributionScheduleDTO> getContributionSchedulesByClientId(String clientId);
    Optional<ContributionScheduleDTO> getContributionScheduleByMonth(String clientId, String month);
    Optional<ContributionScheduleDTO> getContributionScheduleByPaymentId(String paymentId);
    Optional<ContributionScheduleDTO> approveContributionSchedule(String contributionScheduleId);
    Optional<ContributionScheduleDTO> updateContributionSchedule(ContributionScheduleDTO contributionScheduleDTO);
    Optional<ContributionScheduleDTO> submitContributionSchedule(String contributionScheduleId);
    Optional<ContributionScheduleDTO> rejectContributionSchedule(String contributionScheduleId);
    Optional<ContributionScheduleDTO> handleContributionSchedulePaymentStatusReportReceived(String paymentEndToEndId);
    Optional<ContributionScheduleDTO> deleteContributionSchedule(String contributionScheduleId);
    Optional<ContributionScheduleDTO> addItemToContributionSchedule(ContributionScheduleItemDTO contributionScheduleItem);
    Optional<ContributionScheduleDTO> removeItemFromContributionSchedule(String contributionScheduleItemId);
    Optional<ContributionScheduleItemDTO> getContributionScheduleItem(String contributionScheduleItemId);
    List<ContributionScheduleItemDTO> getContributionScheduleItemsByEmployeeId(String employeeId);
}
