package zw.co.esolutions.zpas.services.impl.registration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.registration.NonFinancialInstitutionRegistrationDTO;
import zw.co.esolutions.zpas.enums.AddressTypeCode;
import zw.co.esolutions.zpas.enums.PartyTypeCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.country.CountryService;
import zw.co.esolutions.zpas.services.iface.registration.NonFinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class NonFinancialInstitutionServiceImpl implements NonFinancialInstitutionService {
    @Autowired
    NonFinancialInstitutionRepository nonFinancialInstitutionRepository;

    @Autowired
    private PostalAddressRepository postalAddressRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private OrganisationNameRepository organisationNameRepository;

    @Autowired
    private AccountPartyRoleRepository accountPartyRoleRepository;

    @Autowired
    private EmployingPartyRoleRepository employingPartyRoleRepository;

    @Autowired
    private OrganisationIdentificationRepository organisationIdentificationRepository;

    @Autowired
    private AccountsService accountService;

    private Boolean getEnableDebitOrder(String enableDebitOrderStr) {
        if(enableDebitOrderStr == null) {
            return false;
        }
        return enableDebitOrderStr.equalsIgnoreCase("on") || enableDebitOrderStr.equalsIgnoreCase("true");
    }


    @Override
    public NonFinancialInstitution registerNonFinancialInstitution(NonFinancialInstitutionRegistrationDTO dto) {
        final PostalAddress placeOfRegistrationPostalAddress = new PostalAddress();
        AddressTypeCode addressType = AddressTypeCode.None;
        LocalDate registrationLocalDate;
        try {
            registrationLocalDate = LocalDate.parse(dto.getRegistrationDay(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            addressType = AddressTypeCode.valueOf(dto.getPlcOfRegAddressType());
        } catch (RuntimeException re) {
            registrationLocalDate = LocalDate.now();
            re.printStackTrace();
        }
        placeOfRegistrationPostalAddress.setId(GenerateKey.generateEntityId());
        placeOfRegistrationPostalAddress.setDateCreated(OffsetDateTime.now());
        placeOfRegistrationPostalAddress.setLastUpdated(OffsetDateTime.now());
        placeOfRegistrationPostalAddress.setEntityStatus(EntityStatus.ACTIVE);
        placeOfRegistrationPostalAddress.setEntityVersion(0L);
        placeOfRegistrationPostalAddress.setAddressType(addressType);
        placeOfRegistrationPostalAddress.setStreetName(dto.getStreetName());
        placeOfRegistrationPostalAddress.setStreetBuildingIdentification(dto.getStreetBuildingIdentification());
        placeOfRegistrationPostalAddress.setPostCodeIdentification(dto.getPostCodeIdentification());
        placeOfRegistrationPostalAddress.setTownName(dto.getTownName());
        placeOfRegistrationPostalAddress.setState(dto.getState());
        placeOfRegistrationPostalAddress.setBuildingName(dto.getBuildingName());
        placeOfRegistrationPostalAddress.setFloor(dto.getFloor());
        placeOfRegistrationPostalAddress.setDistrictName(dto.getDistrictName());

        countryService.findCountryById(dto.getPlcOfRegCountryId()).ifPresent(country -> {
            placeOfRegistrationPostalAddress.setCountyIdentification(dto.getPlcOfRegCountryId());
            placeOfRegistrationPostalAddress.setCountry(country);
        });
        placeOfRegistrationPostalAddress.setPostOfficeBox(dto.getPostOfficeBox());
        placeOfRegistrationPostalAddress.setProvince(dto.getProvince());
        placeOfRegistrationPostalAddress.setDepartment(dto.getDepartment());
        placeOfRegistrationPostalAddress.setSubDepartment(dto.getSubDepartment());
        placeOfRegistrationPostalAddress.setSuiteIdentification(dto.getSuiteIdentification());
        placeOfRegistrationPostalAddress.setBuildingIdentification(dto.getBuildingIdentification());
        placeOfRegistrationPostalAddress.setMailDeliverySubLocation(dto.getMailDeliverySubLocation());
        placeOfRegistrationPostalAddress.setBlock(dto.getBlock());
        placeOfRegistrationPostalAddress.setDistrictSubDivisionIdentification(dto.getDistrictSubDivisionIdentification());
        placeOfRegistrationPostalAddress.setLot(dto.getLot());

        NonFinancialInstitution  nonFinancialInstitution = new NonFinancialInstitution();
        nonFinancialInstitution.setVirtualId(dto.getVirtualId());
        nonFinancialInstitution.setPurpose(dto.getPurpose());
        nonFinancialInstitution.setSsr(dto.getSsr());
        nonFinancialInstitution.setNssaBusinessPartnerNumber(dto.getNssaBusinessPartnerNumber());

        nonFinancialInstitution.setEnableDebitOrder(getEnableDebitOrder(dto.getEnableDebitOrder()));
        nonFinancialInstitution.setRegistrationDate(registrationLocalDate.atTime(OffsetTime.now()));

        List<OrganisationIdentification> organisationIdentificationList = new ArrayList<>();

        OrganisationIdentification organisationIdentification = new OrganisationIdentification();
        organisationIdentification.setBICFI(dto.getBicFIIdentifier());
        organisationIdentification.setOtherBIC(dto.getOtherBIC());
        organisationIdentification.setId(GenerateKey.generateEntityId());
        organisationIdentification.setDateCreated(OffsetDateTime.now());
        organisationIdentification.setLastUpdated(OffsetDateTime.now());
        organisationIdentification.setTaxIdentificationNumber(dto.getTaxIdentificationNumber());
        organisationIdentification.setEntityStatus(EntityStatus.ACTIVE);
        organisationIdentification.setEntityVersion(0L);
        organisationIdentification.setPartyType(PartyTypeCode.Issuer);

        organisationIdentification.setAnyBIC(dto.getAnyBICIdentifier());
        organisationIdentification.setBICNonFI(dto.getBICNonFI());

        List<OrganisationName> organisationNames = new ArrayList<>();
        OrganisationName organisationName = new OrganisationName();
        if (nonFinancialInstitution.getOrganisationIdentification() != null && nonFinancialInstitution.getOrganisationIdentification().size() > 0) {
            organisationName.setOrganisation(nonFinancialInstitution.getOrganisationIdentification().get(0));
        }

        organisationName.setId(GenerateKey.generateEntityId());
        organisationName.setDateCreated(OffsetDateTime.now());
        organisationName.setLastUpdated(OffsetDateTime.now());
        organisationName.setEntityStatus(EntityStatus.ACTIVE);
        organisationName.setEntityVersion(0L);

        organisationName.setLegalName(dto.getLegalName());
        organisationName.setName(dto.getLegalName());
        organisationName.setTradingName(dto.getTradingName());
        organisationName.setShortName(dto.getShortName());
        organisationName.setOrganisation(organisationIdentification);
        organisationNames.add(organisationName);
        organisationIdentification.setOrganisationName(organisationNames);

        organisationIdentification.setOrganisation(nonFinancialInstitution);
        organisationIdentificationList.add(organisationIdentification);
        nonFinancialInstitution.setOrganisationIdentification(organisationIdentificationList);

        //select actual from already existing
        final Organisation parentOrganisation = null;
        nonFinancialInstitution.setParentOrganisation(parentOrganisation);

        List<Organisation> branches = new ArrayList();
        nonFinancialInstitution.setBranch(branches);

        List<Location> placesOfOperation = new ArrayList<>();
        Location placeOfOperation = new Location();
        placeOfOperation.setId(GenerateKey.generateEntityId());
        placeOfOperation.setDateCreated(OffsetDateTime.now());
        placeOfOperation.setLastUpdated(OffsetDateTime.now());
        placeOfOperation.setEntityStatus(EntityStatus.DRAFT);
        placeOfOperation.setEntityVersion(0L);
        placeOfOperation.setOperatingOrganisation(nonFinancialInstitution);
        PostalAddress placeOfOperationPostalAddress = readPostalAddressForPlaceOfOperation(dto);
        placeOfOperationPostalAddress.setLocation(placeOfOperation);
        placeOfOperationPostalAddress.setRelatedParty(nonFinancialInstitution);
        placeOfOperation.setAddress(Arrays.asList(placeOfOperationPostalAddress));
        placesOfOperation.add(placeOfOperation);
        nonFinancialInstitution.setPlaceOfOperation(placesOfOperation);

        final Location placeOfRegistration = new Location();
        placeOfRegistration.setId(GenerateKey.generateEntityId());
        placeOfRegistration.setDateCreated(OffsetDateTime.now());
        placeOfRegistration.setLastUpdated(OffsetDateTime.now());
        placeOfRegistration.setEntityStatus(EntityStatus.DRAFT);
        placeOfRegistration.setEntityVersion(0L);
        placeOfRegistrationPostalAddress.setRelatedParty(nonFinancialInstitution);
        placeOfRegistrationPostalAddress.setLocation(placeOfRegistration);
        placeOfRegistration.setOperatingOrganisation(nonFinancialInstitution);

        placeOfRegistration.setAddress(Arrays.asList(placeOfRegistrationPostalAddress));
        placeOfRegistration.setRegisteredOrganisation(nonFinancialInstitution);

        nonFinancialInstitution.setPlaceOfRegistration(placeOfRegistration);

        nonFinancialInstitution.setDescription(dto.getDescription());

        nonFinancialInstitution.setEstablishmentDate(OffsetDateTime.now());

        List<ContactPoint> contactPoints = readContactPointsJsonWithObjectMapper(dto.getPhonesJson(), dto.getAddressesJson());
        contactPoints.forEach(contactPoint -> {
            log.info("Contact point is: {}", contactPoint);
            contactPoint.setRelatedParty(nonFinancialInstitution);
        });
        nonFinancialInstitution.setContactPoint(contactPoints);

        List<PartyIdentificationInformation> identifications = new ArrayList<>();
        nonFinancialInstitution.setIdentification(identifications);

        List<Location> residence = new ArrayList<>();
        nonFinancialInstitution.setResidence(residence);

        nonFinancialInstitution.setId(GenerateKey.generateEntityId());
        nonFinancialInstitution.setDateCreated(OffsetDateTime.now());
        nonFinancialInstitution.setLastUpdated(OffsetDateTime.now());
        nonFinancialInstitution.setEntityStatus(EntityStatus.DRAFT);

        List<Role> roles = new ArrayList<>();
        nonFinancialInstitution.setRole(roles);

        final DateTimePeriod dateTimePeriod = new DateTimePeriod();
        dateTimePeriod.setId(GenerateKey.generateEntityId());
        dateTimePeriod.setId(GenerateKey.generateEntityId());
        dateTimePeriod.setDateCreated(OffsetDateTime.now());
        dateTimePeriod.setLastUpdated(OffsetDateTime.now());
        dateTimePeriod.setEntityStatus(EntityStatus.DRAFT);

        final LocalDate fromStartDate = LocalDate.parse(dto.getValidityPeriodStartDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dateTimePeriod.setFromDateTime(fromStartDate.atTime(OffsetTime.now()));

        final LocalDate toEndDate = LocalDate.parse(dto.getValidityPeriodEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dateTimePeriod.setToDateTime(toEndDate.atTime(OffsetTime.now()));

        dateTimePeriod.setNumberOfDays(Period.between(fromStartDate, toEndDate).getDays());

        dateTimePeriod.setEntityVersion(0L);

        nonFinancialInstitution.setValidityPeriod(dateTimePeriod);
        log.info("Creating non financial institution.");
        return nonFinancialInstitutionRepository.save(nonFinancialInstitution);
    }

    @Override
    public Optional<NonFinancialInstitution> getNonFinancialInstitutionById(String nonFinancialInstitutionId) {
        return findFinancialInstitutionByIdHelper(nonFinancialInstitutionId);
    }

    @Override
    public Optional<NonFinancialInstitution> approveNonFinancialInstitution(String nonFinancialInstitutionId) {
        return nonFinancialInstitutionRepository.findById(nonFinancialInstitutionId).map(nonFinancialInstitution -> {
            if (nonFinancialInstitution.getEntityStatus() == EntityStatus.DRAFT) {
                log.error("Financial Institution has not been submitted, can't be approved.");
                return Optional.ofNullable(nonFinancialInstitution);
            }
            nonFinancialInstitution.setEntityStatus(EntityStatus.ACTIVE);
            final NonFinancialInstitution saved = nonFinancialInstitutionRepository.save(nonFinancialInstitution);
            return Optional.ofNullable(saved);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<NonFinancialInstitution> rejectNonFinancialInstitution(String nonFinancialInstitutionId) {
        return nonFinancialInstitutionRepository.findById(nonFinancialInstitutionId).map(nonFinancialInstitution -> {
            if (nonFinancialInstitution.getEntityStatus() == EntityStatus.DRAFT) {
                log.error("Financial Institution has not been submitted, can't be rejected.");
                return Optional.ofNullable(nonFinancialInstitution);
            }
            nonFinancialInstitution.setEntityStatus(EntityStatus.DISAPPROVED);
            final NonFinancialInstitution saved = nonFinancialInstitutionRepository.save(nonFinancialInstitution);
            return Optional.ofNullable(saved);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<NonFinancialInstitution> submitNonFinancialInstitution(String nonFinancialInstitutionId) {
        return nonFinancialInstitutionRepository.findById(nonFinancialInstitutionId).map(nonFinancialInstitution -> {
            nonFinancialInstitution.setEntityStatus(EntityStatus.PENDING_APPROVAL);
            final NonFinancialInstitution saved = nonFinancialInstitutionRepository.save(nonFinancialInstitution);
            return Optional.ofNullable(saved);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<NonFinancialInstitution> deleteNonFinancialInstitution(String nonFinancialInstitutionId) {
        return nonFinancialInstitutionRepository.findById(nonFinancialInstitutionId).map(nonFinancialInstitution -> {
            if (nonFinancialInstitution.getEntityStatus() != EntityStatus.DRAFT) {
                log.error("Financial Institution cannot be deleted. It's not in draft status");
            }
            nonFinancialInstitution.setEntityStatus(EntityStatus.DELETED);
            return Optional.ofNullable(nonFinancialInstitution);
        }).orElse(Optional.empty());    }

    @Override
    public Optional<NonFinancialInstitution> getNonFinancialInstitutionByAnyBIC(String nonFinancialInstitutionId) {
        return findFinancialInstitutionByIdHelper(nonFinancialInstitutionId);
    }

    @Override
    public List<NonFinancialInstitution> getNonFinancialInstitutions() {
        return nonFinancialInstitutionRepository.findAll();
    }

    @Override
    public NonFinancialInstitution updateNonFinancialInstitution(NonFinancialInstitutionRegistrationDTO dto) {
        OrganisationIdentification organisationIdentification = new OrganisationIdentification();
        organisationIdentification.setBICFI(dto.getBicFIIdentifier());
        organisationIdentification.setDateCreated(OffsetDateTime.now());
        organisationIdentification.setLastUpdated(OffsetDateTime.now());
        organisationIdentification.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        organisationIdentification.setEntityVersion(1L);

        organisationIdentification.setAnyBIC(dto.getAnyBICIdentifier());

        List<OrganisationName> organisationNames = new ArrayList<>();
        OrganisationName organisationName = new OrganisationName();
        organisationName.setLegalName(dto.getLegalName());
        organisationName.setTradingName(dto.getTradingName());
        organisationName.setShortName(dto.getShortName());
        organisationNames.add(organisationName);
        organisationIdentification.setOrganisationName(organisationNames);

        PostalAddress postalAddress = new PostalAddress();

        AddressTypeCode addressType = AddressTypeCode.valueOf(dto.getPlcOfRegAddressType());
        postalAddress.setAddressType(addressType);
        postalAddress.setStreetName(dto.getStreetName());
        postalAddress.setStreetBuildingIdentification(dto.getStreetBuildingIdentification());
        postalAddress.setPostCodeIdentification(dto.getPostCodeIdentification());
        postalAddress.setTownName(dto.getTownName());
        postalAddress.setState(dto.getState());
        postalAddress.setBuildingName(dto.getBuildingName());
        postalAddress.setFloor(dto.getFloor());
        postalAddress.setDistrictName(dto.getDistrictName());

        postalAddress.setPostOfficeBox(dto.getPostOfficeBox());
        postalAddress.setProvince(dto.getProvince());
        postalAddress.setDepartment(dto.getDepartment());
        postalAddress.setSubDepartment(dto.getSubDepartment());
        postalAddress.setSuiteIdentification(dto.getSuiteIdentification());
        postalAddress.setBuildingIdentification(dto.getBuildingIdentification());
        postalAddress.setMailDeliverySubLocation(dto.getMailDeliverySubLocation());
        postalAddress.setBlock(dto.getBlock());
        postalAddress.setDistrictSubDivisionIdentification(dto.getDistrictSubDivisionIdentification());
        postalAddress.setLot(dto.getLot());

        NonFinancialInstitution nonFinancialInstitution = new NonFinancialInstitution();
        nonFinancialInstitution.setPurpose(dto.getPurpose());

        LocalDate registrationLocalDate = LocalDate.parse(dto.getRegistrationDay(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        nonFinancialInstitution.setRegistrationDate(registrationLocalDate.atTime(OffsetTime.now()));

        List<OrganisationIdentification> organisationIdentificationList = new ArrayList<>();
        organisationIdentification.setOrganisation(nonFinancialInstitution);
        organisationIdentificationList.add(organisationIdentification);
        nonFinancialInstitution.setOrganisationIdentification(organisationIdentificationList);

        //select actual from already existing
        final Organisation parentOrganisation = null;
        nonFinancialInstitution.setParentOrganisation(parentOrganisation);

        List<Organisation> branches = new ArrayList();
        nonFinancialInstitution.setBranch(branches);

        List<Location> placesOfOperation = new ArrayList<>();
        nonFinancialInstitution.setPlaceOfOperation(placesOfOperation);

        final Location placeOfRegistration = new Location();
        placeOfRegistration.setDateCreated(OffsetDateTime.now());
        placeOfRegistration.setLastUpdated(OffsetDateTime.now());
        placeOfRegistration.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        placeOfRegistration.setEntityVersion(1L);
//        placeOfRegistration.setDomiciledParty(new Party());
        placeOfRegistration.setRegisteredOrganisation(nonFinancialInstitution);
        placeOfRegistration.setAddress(Arrays.asList(postalAddress));

        nonFinancialInstitution.setPlaceOfRegistration(placeOfRegistration);

        nonFinancialInstitution.setDescription(dto.getDescription());

        nonFinancialInstitution.setEstablishmentDate(OffsetDateTime.now());

        List<ContactPoint> contactPoints = readContactPointsJsonWithObjectMapper(dto.getPhonesJson(), dto.getAddressesJson());
        contactPoints.forEach(contactPoint -> {
            log.info("Contact point is: {}", contactPoint);
            contactPoint.setRelatedParty(nonFinancialInstitution);
        });
        nonFinancialInstitution.setContactPoint(contactPoints);

        List<PartyIdentificationInformation> identifications = new ArrayList<>();
        nonFinancialInstitution.setIdentification(identifications);

//        final Tax taxConditions = new Tax();
//        taxConditions.setId(GenerateKey.generateEntityId());
//        taxConditions.setDateCreated(OffsetDateTime.now());
//        taxConditions.setLastUpdated(OffsetDateTime.now());
//        taxConditions.setEntityStatus(EntityStatus.DRAFT);
//        final CurrencyAndAmount amount = new CurrencyAndAmount();
//        amount.setAmount(new BigDecimal(2));
//        taxConditions.setAmount(new CurrencyAndAmount());
//        taxConditions.setRate(new PercentageRate());
//
//        financialInstitution.setTaxationConditions(taxConditions);

//        final Location domicile = new Location();
//        financialInstitution.setDomicile(domicile);

        List<Location> residence = new ArrayList<>();
        nonFinancialInstitution.setResidence(residence);
//
//        final PowerOfAttorney powerOfAttorney = new PowerOfAttorney();
//        powerOfAttorney.setId(GenerateKey.generateEntityId());
//        powerOfAttorney.setEntityStatus(EntityStatus.DRAFT);
//        powerOfAttorney.setDateCreated(OffsetDateTime.now());
//        powerOfAttorney.setLastUpdated(OffsetDateTime.now());
//        powerOfAttorney.setEntityVersion(0L);
//
//        financialInstitution.setPowerOfAttorney(powerOfAttorney);

        //  financialInstitution.setId(GenerateKey.generateEntityId());
        // financialInstitution.setDateCreated(OffsetDateTime.now());
        nonFinancialInstitution.setLastUpdated(OffsetDateTime.now());
        nonFinancialInstitution.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
//        financialInstitution.setEntityVersion(0L);

        List<Role> roles = new ArrayList<>();
        nonFinancialInstitution.setRole(roles);

        final DateTimePeriod dateTimePeriod = new DateTimePeriod();
       /* dateTimePeriod.setId(GenerateKey.generateEntityId());
        dateTimePeriod.setId(GenerateKey.generateEntityId());*/
        dateTimePeriod.setDateCreated(OffsetDateTime.now());
        dateTimePeriod.setLastUpdated(OffsetDateTime.now());
        dateTimePeriod.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);

        final LocalDate fromStartDate = LocalDate.parse(dto.getValidityPeriodStartDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dateTimePeriod.setFromDateTime(fromStartDate.atTime(OffsetTime.now()));

        final LocalDate toEndDate = LocalDate.parse(dto.getValidityPeriodEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dateTimePeriod.setToDateTime(toEndDate.atTime(OffsetTime.now()));

        dateTimePeriod.setNumberOfDays(Period.between(fromStartDate, toEndDate).getDays());

        dateTimePeriod.setEntityVersion(1L);

        nonFinancialInstitution.setValidityPeriod(dateTimePeriod);
        return nonFinancialInstitutionRepository.save(nonFinancialInstitution);
    }

    @Override
    public List<NonFinancialInstitution> getNonFinancialInstitutionsByClientId(String clientId) {
        log.info("ClientId: " + clientId);
        final List<Account> accounts = accountService.getAccountsByClientId(clientId);
        log.info("Accounts are: {}", accounts);
        final List<NonFinancialInstitution> nonFinancialInstitutions = new ArrayList<>();
        accounts.forEach(account -> {
            accountPartyRoleRepository.findAllByAccount_Id(account.getId()).stream()
                    .filter(role -> role instanceof AccountServicerRole).forEach(role -> {
                final List<NonFinancialInstitution> nonFis = nonFinancialInstitutionRepository.findAllByRole_Id(role.getId());
                log.info("NonFinancial Institutions: {}", nonFis);
                nonFinancialInstitutions.addAll(nonFis);
            });
        });
        return nonFinancialInstitutions;
    }

    @Override
    public Optional<Organisation> getOrganisationByEmployeeId(String employeeId) {
        log.info("EmployeeDTO ID: " + employeeId);
        final List<EmployingPartyRole> employingPartyRoles = employingPartyRoleRepository.findAllByEmployee_Id(employeeId);
        return employingPartyRoles.stream().findFirst().map(employingPartyRole -> {
            final List<RolePlayer> employers = employingPartyRole.getPlayer();
            return employers.stream().findFirst().map(rolePlayer -> {
                Organisation organisation = (Organisation) rolePlayer;
                return Optional.ofNullable(organisation);
            }).orElse(Optional.empty());
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<NonFinancialInstitution> getNonFinancialInstitutionByAccountId(String accountId) {
        final CashAccount account = accountService.findAccountById(accountId);
        log.info("Account is: {}", account);
        final List<NonFinancialInstitution> nonFinancialInstitutions = new ArrayList<>();
        accountPartyRoleRepository.findAllByAccount_Id(account.getId()).stream()
                .filter(role -> role instanceof AccountServicerRole).forEach(role -> {
            final List<NonFinancialInstitution> nonFis = nonFinancialInstitutionRepository.findAllByRole_Id(role.getId());
            nonFinancialInstitutions.addAll(nonFis);
        });
        if (nonFinancialInstitutions.size() > 0) {
            final NonFinancialInstitution nonFinancialInstitution = nonFinancialInstitutions.get(0);
            return getNonFinancialInstitutionById(nonFinancialInstitution.getId());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public NonFinancialInstitution updateNonFinancialInstitutionGeneralInformation(NonFinancialInstitution nonFinancialInstitution) {
        nonFinancialInstitution.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        nonFinancialInstitution.setLastUpdated(OffsetDateTime.now());
        return nonFinancialInstitutionRepository.save(nonFinancialInstitution);
    }

    @Override
    public OrganisationIdentification updateNonFinancialInstitutionOrganisatinIdentification(OrganisationIdentification organisationIdentification) {
        organisationIdentification.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        organisationIdentification.setLastUpdated(OffsetDateTime.now());
        return organisationIdentificationRepository.save(organisationIdentification);
    }

    private List<ContactPoint> readContactPointsJsonWithObjectMapper(String phonesJson, String electronicAddressesJson) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        try {
            List<ContactPoint> contactPoints = new ArrayList<>();
            List<PhoneAddress> phoneAddresses = mapper.readValue(phonesJson, mapper.getTypeFactory()
                    .constructCollectionType(List.class, PhoneAddress.class));
            List<ElectronicAddress> electronicAddresses = mapper.readValue(electronicAddressesJson, mapper.getTypeFactory()
                    .constructCollectionType(List.class, ElectronicAddress.class));
            phoneAddresses.forEach(phoneAddress -> {
                phoneAddress.setId(GenerateKey.generateEntityId());
                phoneAddress.setDateCreated(OffsetDateTime.now());
                phoneAddress.setLastUpdated(OffsetDateTime.now());
                phoneAddress.setEntityStatus(EntityStatus.ACTIVE);
                phoneAddress.setEntityVersion(0L);
                contactPoints.add(phoneAddress);
            });
            electronicAddresses.forEach(electronicAddress -> {
                electronicAddress.setId(GenerateKey.generateEntityId());
                electronicAddress.setDateCreated(OffsetDateTime.now());
                electronicAddress.setLastUpdated(OffsetDateTime.now());
                electronicAddress.setEntityStatus(EntityStatus.ACTIVE);
                electronicAddress.setEntityVersion(0L);
                contactPoints.add(electronicAddress);
            });
            log.info(contactPoints + "");
            return contactPoints;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }    }

    private PostalAddress readPostalAddressForPlaceOfOperation(NonFinancialInstitutionRegistrationDTO firDTO) {
        final Optional<Country> countryOptional = countryService.findCountryById(firDTO.getPlaceOfOperationCountryId());

        final PostalAddress postalAddress = new PostalAddress();
        postalAddress.setId(GenerateKey.generateEntityId());
        postalAddress.setDateCreated(OffsetDateTime.now());
        postalAddress.setLastUpdated(OffsetDateTime.now());
        postalAddress.setEntityStatus(EntityStatus.ACTIVE);

        AddressTypeCode addressTypeCode = AddressTypeCode.Business;
        try {
            addressTypeCode = AddressTypeCode.valueOf(firDTO.getPlaceOfOperationAddressType());
        } catch (RuntimeException re) {

        }
        postalAddress.setAddressType(addressTypeCode);
        postalAddress.setStreetName(firDTO.getPlaceOfOperationStreetName());
        postalAddress.setStreetBuildingIdentification(firDTO.getPlaceOfOperationStreetBuildingIdentification());
        postalAddress.setPostCodeIdentification(firDTO.getPlaceOfOperationPostCodeIdentification());
        postalAddress.setTownName(firDTO.getPlaceOfOperationTownName());
        postalAddress.setState(firDTO.getPlaceOfOperationState());
        postalAddress.setBuildingName(firDTO.getPlaceOfOperationBuildingName());
        postalAddress.setFloor(firDTO.getPlaceOfOperationFloor());
        postalAddress.setDistrictName(firDTO.getPlaceOfOperationDistrictName());
        postalAddress.setRegionIdentification("");
        postalAddress.setPostOfficeBox(firDTO.getPlaceOfOperationPostOfficeBox());
        postalAddress.setProvince(firDTO.getPlaceOfOperationProvince());
        postalAddress.setDepartment(firDTO.getPlaceOfOperationDepartment());
        postalAddress.setSubDepartment(firDTO.getPlaceOfOperationSubDepartment());
        postalAddress.setSuiteIdentification(firDTO.getPlaceOfOperationSuiteIdentification());
        postalAddress.setBuildingIdentification(firDTO.getPlaceOfOperationBuildingIdentification());
        postalAddress.setMailDeliverySubLocation(firDTO.getPlaceOfOperationMailDeliverySubLocation());
        postalAddress.setBlock(firDTO.getPlaceOfOperationBlock());
        postalAddress.setLot(firDTO.getPlaceOfOperationLot());

        countryOptional.ifPresent(country -> {
            postalAddress.setCountry(country);
            postalAddress.setCountyIdentification(country.getId());
        });

        postalAddress.setDistrictSubDivisionIdentification(firDTO.getPlaceOfOperationDistrictSubDivisionIdentification());
        return postalAddress;
    }

    private Optional<NonFinancialInstitution> findFinancialInstitutionByIdHelper(String nonFinancialInstitutionId) {
        return nonFinancialInstitutionRepository.findById(nonFinancialInstitutionId).map(financialInstitution -> {
            final List<OrganisationIdentification> organisationIdentifications = organisationIdentificationRepository
                    .findAllByOrganisationId(nonFinancialInstitutionId);
            log.info("Organisation Identifications: {}", organisationIdentifications);
            organisationIdentifications.forEach(organisationIdentification -> {
                final List<OrganisationName> organisationNames = organisationNameRepository.findAllByOrganisationId(organisationIdentification.getId());
                log.info("Organisation Names: {}", organisationNames);
                organisationIdentification.setOrganisationName(organisationNames);
            });

            final Location placeOfRegistration = financialInstitution.getPlaceOfRegistration();
            if (placeOfRegistration != null) {
                log.info("Place of Registration Location Id: {}", placeOfRegistration.getId());
                final List<PostalAddress> placeOfRegistrationAddresses = postalAddressRepository.findAllByLocationId(placeOfRegistration.getId());
                log.info("Postal Addresses for Place of Registration: {}", placeOfRegistrationAddresses);
                placeOfRegistration.setAddress(placeOfRegistrationAddresses);
                financialInstitution.setPlaceOfRegistration(placeOfRegistration);
            }

            final List<Location> placesOfOperation = financialInstitution.getPlaceOfOperation();
            if(placesOfOperation != null) {
                log.info("Size of place of operation: {}", placesOfOperation.size());
                placesOfOperation.forEach(location -> {
                    log.info("Place of Operation Location Id: {}", location.getId());
                    final List<PostalAddress> allByLocationId = postalAddressRepository.findAllByLocationId(location.getId());
                    log.info("Postal Addresses for place of operation: {}", allByLocationId);
                    location.setAddress(allByLocationId);
                });
            }

            financialInstitution.setOrganisationIdentification(organisationIdentifications);
            financialInstitution.getContactPoint().removeIf(contactPoint -> contactPoint instanceof PostalAddress);
            sanitiseFinancialInstitution(financialInstitution);
            return Optional.of(financialInstitution);
        }).orElseGet(() -> {
            return Optional.empty();
        });
    }

    private void sanitiseFinancialInstitution(NonFinancialInstitution nonFinancialInstitution) {
        if (nonFinancialInstitution.getPurpose() == null) {
            nonFinancialInstitution.setPurpose("");
        }
        if (nonFinancialInstitution.getRegistrationDate() == null) {
            nonFinancialInstitution.setRegistrationDate(OffsetDateTime.now());
        }
        if (nonFinancialInstitution.getOrganisationIdentification() == null) {
            nonFinancialInstitution.setOrganisationIdentification(new ArrayList());
        }
        if (nonFinancialInstitution.getPlaceOfOperation() == null) {
            nonFinancialInstitution.setPlaceOfOperation(new ArrayList());
        }
        if (nonFinancialInstitution.getPlaceOfRegistration() == null) {
            nonFinancialInstitution.setPlaceOfRegistration(new Location());
        }
        if (nonFinancialInstitution.getDescription() == null) {
            nonFinancialInstitution.setDescription("");
        }
        if (nonFinancialInstitution.getEstablishmentDate() == null) {
            nonFinancialInstitution.setEstablishmentDate(OffsetDateTime.now());
        }
        if (nonFinancialInstitution.getContactPoint() == null) {
            nonFinancialInstitution.setContactPoint(new ArrayList());
        }
        if (nonFinancialInstitution.getIdentification() == null) {
            nonFinancialInstitution.setIdentification(new ArrayList());
        }
        if (nonFinancialInstitution.getTaxationConditions() == null) {
            nonFinancialInstitution.setTaxationConditions(new Tax());
        }
        if (nonFinancialInstitution.getResidence() == null) {
            nonFinancialInstitution.setResidence(new ArrayList());
        }
        if (nonFinancialInstitution.getValidityPeriod() == null) {
            nonFinancialInstitution.setValidityPeriod(new DateTimePeriod());
        }
    }

}
