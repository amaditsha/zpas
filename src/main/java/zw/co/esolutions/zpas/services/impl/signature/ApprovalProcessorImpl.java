package zw.co.esolutions.zpas.services.impl.signature;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.agreement.ApprovalRequestDTO;
import zw.co.esolutions.zpas.dto.agreement.ApprovalResponseDTO;
import zw.co.esolutions.zpas.enums.MandateHolderType;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountMandateService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.signature.ApprovalProcessorService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityType;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.lang.System;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ApprovalProcessorImpl implements ApprovalProcessorService {

    @Autowired
    private SigningDetailRepository signingDetailRepository;

    @Autowired
    private CashAccountMandateRepository cashAccountMandateRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private CashAccountContractRepository cashAccountContractRepository;

    @Autowired
    private RolePlayerRepository rolePlayerRepository;
    @Autowired
    private CashAccountRepository cashAccountRepository;
    @Autowired
    private CashAccountMandateService cashAccountMandateService;
    @Autowired
    private ClientService clientService;

    public static void main(String[] args) {
        List<String> ns = Arrays.asList("a", "b", "c", "d", "e");
        System.out.println("The index of c is " + ns.indexOf("c"));
    }

    private Boolean canMandateAuthorise(MandateHolderType holderType) {
        if (holderType.equals(MandateHolderType.AUTHORISE) || holderType.equals(MandateHolderType.BOTH)) {
            return true;
        }
        return false;
    }

    @Override
    public ApprovalResponseDTO approveReq(ApprovalRequestDTO requestDTO) {
        log.info("Processing the account approval request {}", requestDTO);
        final String personId = requestDTO.getPersonId();
        final String paymentId = requestDTO.getPaymentId();
        final String cashAccountId = requestDTO.getCashAccountId();

        List<CashAccountContract> cashAccountContracts = cashAccountContractRepository.findAllByCashAccount_Id(cashAccountId);

        log.info("The cash account contracts for cash id {} are {}", cashAccountId, cashAccountContracts.size());
        Optional<CashAccountMandate> optionalCashAccountMandate = cashAccountContracts.stream()
                .filter(cashAccountContract -> getIsValidStatus(cashAccountContract.getCashAccountMandate().getEntityStatus()))
                .map(CashAccountContract::getCashAccountMandate)
                .filter(cashAccountMandate -> {
                    log.warn("Checking the validity period of cash account mandate ...");
                        return cashAccountMandate.getValidityPeriod().stream()
                                .anyMatch(dateTimePeriod -> {
                                    if(dateTimePeriod.getToDateTime().isAfter(OffsetDateTime.now()) && dateTimePeriod.getFromDateTime().isBefore(OffsetDateTime.now())) {
                                        return true;
                                    } else {
                                        cashAccountMandateService.deactivateCashAccountMandate(cashAccountMandate);
                                        return false;
                                    }
                                });
                })
                .findFirst();
        return optionalCashAccountMandate.map(cashAccountMandate -> {

            List<MandatePartyRole> mandatePartyRoles = cashAccountMandate.getMandatePartyRole();
            log.info("The mandate party roles size for the mandate is {}", mandatePartyRoles.size());
            List<MandateHolder> mandateHolders = new ArrayList<>();
            Map<String, MandateHolder> mandateHolderPartyIdsMap = new HashMap<>();
            List<MandateIssuer> mandateIssuers = new ArrayList<>();
            mandatePartyRoles.forEach(mandatePartyRole -> {
                if (mandatePartyRole instanceof MandateHolder && canMandateAuthorise(((MandateHolder) mandatePartyRole).getHolderType())) {
                    mandateHolders.add((MandateHolder) mandatePartyRole);
                    new ArrayList<>(mandatePartyRole.getPlayer()).forEach(rolePlayer -> {
                        mandateHolderPartyIdsMap.put(rolePlayer.getId(), (MandateHolder) mandatePartyRole);
                    });
                } else if (mandatePartyRole instanceof MandateIssuer) {
                    mandateIssuers.add((MandateIssuer) mandatePartyRole);
                }
            });

            log.info("Authenticate the person id {} with mandate party roles of size {}", personId, mandatePartyRoles.size());
            Optional<SignatureCondition> signatureConditionOptional = cashAccountMandate.getSignatureConditions().stream().filter(signatureCondition -> getIsValidStatus(signatureCondition.getEntityStatus()))
                    .findFirst();
            return signatureConditionOptional.map(signatureCondition -> {
                log.info("The signature condition is found for cash account mandate.");

//                Number of account owners or related parties required to authorise transactions on the account.
                List<SigningDetail> signingDetails = signingDetailRepository.findAllBySignatureConditionIdAndPaymentId(signatureCondition.getId(), paymentId);
                if (signingDetails == null || signingDetails.isEmpty()) {
                    log.info("No signing detail for the condition provided. Creating {} signing detail", signatureCondition.getRequiredSignatureNumber());
                    signingDetails = new ArrayList<>();
                    int noOfRequiredSignature = signatureCondition.getRequiredSignatureNumber().intValue();
                    for (int i = 0; i < noOfRequiredSignature; i++) {
                        SigningDetail signingDetail = new SigningDetail();
                        signingDetail.setId(GenerateKey.generateEntityId());
                        signingDetail.setEntityType(EntityType.NONE.name());
                        signingDetail.setPaymentId(paymentId);
                        signingDetail.setDone(false);
                        signingDetail.setHolder(false);
                        signingDetail.setSignatureConditionId(signatureCondition.getId());
                        signingDetail.setOrderingNo(0);
                        signingDetail.setDateCreated(OffsetDateTime.now());
                        signingDetail.setEntityStatus(EntityStatus.ACTIVE);

                        signingDetails.add(signingDetail);
                    }
                    signingDetailRepository.saveAll(signingDetails);


                    if (noOfRequiredSignature == 0) {
                        return getErrorApprovalResponseDTO(personId, signatureCondition.getId(), false, "Signature requirements for the required number of approver is 0." +
                                " Please consult administration.", paymentId);
                    }
                }

                if (signatureCondition.getSignatoryRightIndicator()) {
                    log.info("Indicates whether the signature of the account owner is required to authorise transactions on the account.");
                    MandateIssuer mandateIssuer = mandateIssuers.stream().findFirst().get();


                    Optional<RolePlayer> rolePlayerOptional = rolePlayerRepository.findById(mandateIssuer.getPlayer().stream().findFirst().get().getId());
                    if(rolePlayerOptional.get() instanceof Person) {
                        Person issuer = (Person) rolePlayerOptional.get();
                        if (issuer.getId().equals(personId)) {
                            return processSigningInfo(personId, paymentId, signatureCondition);
                            //...
                        } else {
                            log.info("The provided ID is not for the issuer.");
                        }
                    } else if(rolePlayerOptional.get() instanceof Organisation) {
                        log.info("The role player is any organisation.");

                        Organisation organisation = (Organisation) rolePlayerOptional.get();

                        List<Person> employees = clientService.getPersonsByEmployerId(organisation.getId());
                        Optional<Person> employeePersonOptional = employees.stream().filter(person -> person.getId().equals(personId))
                                .findFirst();
                        if(employeePersonOptional.isPresent()) {
                            log.info("The employee for the organisation is found.");
                            return processSigningInfo(personId, paymentId, signatureCondition);
                        } else {
                            log.warn("The organisation employee is not found.");
                        }


                    } else {
                        log.error("The role player is not defined.");
                    }
                }

//                Check if the order is followed
                if (signatureCondition.getSignatureOrderIndicator()) {
                    log.info("Indicates the order in which the mandate holders are allowed to sign. ");
                    List<String> signatoryPersonsIds = Arrays.asList(signatureCondition.getSignatureOrder().split(","));
                    Integer totalNumberOfSignatory = signatoryPersonsIds.size();
                    List<SigningDetail> signedPersonsDetails = signingDetailRepository
                            .findByPaymentIdAndSignatureConditionIdAndDoneAndHolder(paymentId, signatureCondition.getId(), true, false);
                    Integer numberOfPersonsSigns = signedPersonsDetails.size();
                    Integer remainingToSign = totalNumberOfSignatory - numberOfPersonsSigns;

                    log.info("The size of signatory persons id is {} | number of persons already signed {} | remaining number of signatures {}", totalNumberOfSignatory, numberOfPersonsSigns, remainingToSign);
                    Optional<SigningDetail> latestSigningDetailOptional = signingDetailRepository
                            .findTopByPaymentIdAndSignatureConditionIdAndDoneAndHolderOrderByDateCreatedDesc(paymentId, signatureCondition.getId(), true, false);
                    int latestIndexOfSigningDetail = latestSigningDetailOptional.map(signingDetail -> signatoryPersonsIds.indexOf(signingDetail.getPartyId())).orElse(0);


                    log.info("Latest index of the signing index | {}", latestIndexOfSigningDetail);
                    if (totalNumberOfSignatory - signatoryPersonsIds.indexOf(personId) >= remainingToSign) {
                        log.info("Passed test section 1 ...");
                        if (signatoryPersonsIds.indexOf(personId) >= latestIndexOfSigningDetail) {
                            log.info("Passed test section 2. Proceed to sign the person");
                            Optional<SigningDetail> optionalSigningDetail = signingDetailRepository.findByPartyIdAndSignatureConditionIdAndPaymentId(personId, signatureCondition.getId(), paymentId);

                            return signingThePerson(personId, paymentId, signatureCondition, optionalSigningDetail, false);
                        } else {
                            log.info("Failed to pass test section 2. Index of person {} is not greater than latest stored index {}", signatoryPersonsIds.indexOf(personId), latestIndexOfSigningDetail);
                            return getErrorApprovalResponseDTO(personId, signatureCondition.getId(), true, "You are not qualified to sign on this stage.", paymentId);
                        }
                    } else {
                        log.info("Failed to pass test for signing ");
                        return getErrorApprovalResponseDTO(personId, signatureCondition.getId(), true, "You are not qualified to sign on this stage.", paymentId);
                    }
                }


                log.info("Check the method for mandate holder ...");

                if (mandateHolderPartyIdsMap.keySet().contains(personId)) {
                    log.info("The person with id {} is mandate for this account ", personId);
//                    String mandateHolderId = mandateHolderPartyIdsMap.get(personId).getId();

                    Optional<SigningDetail> optionalSigningDetail = signingDetailRepository.findByPartyIdAndSignatureConditionIdAndPaymentId(personId, signatureCondition.getId(), paymentId);

                    return optionalSigningDetail.map(signingDetail -> {
                        if (signingDetail.getDone()) {
                            log.info("The person with id {} already approved", personId);
                            return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
                        } else {
                            log.info("the person with id {} is approving", personId);
                            signingDetail.setDone(true);
                            signingDetail.setDateApproved(OffsetDateTime.now());
                            signingDetailRepository.save(signingDetail);
                            return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
                        }

                    }).orElseGet(() -> {
                        log.info("No signing detail created for person id {}", personId);
                        Optional<SigningDetail> signingDetailOptional = signingDetailRepository.findAllBySignatureConditionIdAndPaymentId(signatureCondition.getId(), paymentId)
                                .stream().filter(signingDetail -> !signingDetail.getDone())
                                .filter(signingDetail -> !checkIsPartyAppointed(signingDetail))
                                .findFirst();

                        return signingDetailOptional.map(signingDetail -> {
                            log.info("the person with id {} is approving", personId);
                            signingDetail.setPartyId(personId);
                            signingDetail.setDateApproved(OffsetDateTime.now());
                            signingDetail.setDone(true);

                            signingDetailRepository.save(signingDetail);
                            //respond to the caller
                            return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
                        }).orElseGet(() -> {
                            return getErrorApprovalResponseDTO(personId, signatureCondition.getId(), true, "Your signature is already stamped. ", paymentId);
                        });

                    });
                } else {
                    return getErrorApprovalResponseDTO(personId, signatureCondition.getId(), false, "The person is not a mandate holder for this account." +
                            " Please consult administration.", paymentId);
                }


            }).orElseGet(() -> {
                return ApprovalResponseDTO.builder()
                        .entityId(null)
                        .remainingRoleOrderings(null)
                        .done(false)
                        .narrative("No signature condition specified. Please create one for this account.")
                        .build();
            });
        }).orElseGet(() -> {
            log.info("No account mandate found for the person with id {} and cash account id {}", personId, cashAccountId);
            Optional<CashAccount> optionalCashAccount = cashAccountRepository.findById(cashAccountId);
            log.info("Checking if the person approving is the account owner ...");
            List<String> ownerIds = new ArrayList<>();
            optionalCashAccount.ifPresent(cashAccount -> {
                cashAccount.getPartyRole().forEach(accountPartyRole -> {
                    AccountOwnerRole accountOwnerRole;
                    if(accountPartyRole instanceof AccountOwnerRole){
                        accountOwnerRole = (AccountOwnerRole) accountPartyRole;
                        ownerIds.add(accountOwnerRole.getPlayer().get(0).getId());  /*Hopefully we have one owner*/
                    }
                });
            });
            if(ownerIds.contains(personId)) {
                log.info("The person is the owner of the account. Can approve on this account.");
                return ApprovalResponseDTO.builder()
                        .entityId(null)
                        .remainingRoleOrderings(null)
                        .done(true)
                        .narrative("The person is the owner of the account.")
                        .build();
            } else {
                log.warn("The person approving is not the owner of the account hence cannot approve on this account.");
            }
            return ApprovalResponseDTO.builder()
                    .entityId(null)
                    .remainingRoleOrderings(null)
                    .done(false)
                    .narrative("No valid mandate found for the provided cash account. Please consult administration")
                    .build();
        });
    }

    private ApprovalResponseDTO processSigningInfo(String personId, String paymentId, SignatureCondition signatureCondition) {
        log.info("The issuer id found and is required by signature condition requirements");
        Optional<SigningDetail> optionalSigningDetail = signingDetailRepository.findByPartyIdAndSignatureConditionIdAndPaymentId(personId, signatureCondition.getId(), paymentId);

        return optionalSigningDetail.map(signingDetail -> {
            log.info("The account holder detail found. ");
            if (signingDetail.getDone()) {
                log.info("the person with id {} already approved", personId);
                return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
            } else {
                log.info("the person with id {} is approving");
                signingDetail.setDone(true);
                signingDetail.setHolder(true);
                signingDetail.setDateApproved(OffsetDateTime.now());
                signingDetailRepository.save(signingDetail);
                return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
            }
        }).orElseGet(() -> {
            log.info("No signing detail created for person id {}", personId);
            Optional<SigningDetail> signingDetailOptional = signingDetailRepository.findAllBySignatureConditionIdAndPaymentId(signatureCondition.getId(), paymentId)
                    .stream().filter(signingDetail -> !signingDetail.getDone())
                    .filter(signingDetail -> !checkIsPartyAppointed(signingDetail))
                    .findFirst();

            return signingDetailOptional.map(signingDetail -> {
                log.info("the person with id {} is approving", personId);
                signingDetail.setPartyId(personId);
                signingDetail.setDateApproved(OffsetDateTime.now());
                signingDetail.setDone(true);
                signingDetail.setHolder(true);

                signingDetailRepository.save(signingDetail);
                //respond to the caller
                return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
            }).orElseGet(() -> {
                log.info("The account owner haven't signed. Signing ...");
                SigningDetail signingDetail = new SigningDetail();
                signingDetail.setId(GenerateKey.generateEntityId());
                signingDetail.setEntityType(EntityType.NONE.name());
                signingDetail.setPaymentId(paymentId);
                signingDetail.setPartyId(personId);
                signingDetail.setDone(true);
                signingDetail.setHolder(true);
                signingDetail.setSignatureConditionId(signatureCondition.getId());
                signingDetail.setOrderingNo(0);
                signingDetail.setDateCreated(OffsetDateTime.now());
                signingDetail.setEntityStatus(EntityStatus.ACTIVE);

                signingDetailRepository.save(signingDetail);
                return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
            });

        });
    }

    private ApprovalResponseDTO signingThePerson(String personId, String paymentId, SignatureCondition signatureCondition, Optional<SigningDetail> optionalSigningDetail, Boolean isIssuer) {
        return optionalSigningDetail.map(signingDetail -> {
            if (signingDetail.getDone()) {
                log.info("the person with id {} already approved", personId);
                return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
            } else {
                log.info("the person with id {} is approving");
                signingDetail.setDone(true);
                signingDetail.setDateApproved(OffsetDateTime.now());
                signingDetailRepository.save(signingDetail);
                return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
            }

        }).orElseGet(() -> {
            log.info("No signing detail created for person id {}", personId);
            Optional<SigningDetail> signingDetailOptional = signingDetailRepository.findAllBySignatureConditionIdAndPaymentId(signatureCondition.getId(), paymentId)
                    .stream().filter(signingDetail -> !signingDetail.getDone())
                    .filter(signingDetail -> !checkIsPartyAppointed(signingDetail))
                    .findFirst();

            return signingDetailOptional.map(signingDetail -> {
                log.info("the person with id {} is approving", personId);
                signingDetail.setPartyId(personId);
                signingDetail.setDateApproved(OffsetDateTime.now());
                signingDetail.setDone(true);

                signingDetailRepository.save(signingDetail);
                //respond to the caller
                return getNormalApprovalResponseDTO(signatureCondition.getId(), personId, paymentId);
            }).orElseGet(() -> {
                return getErrorApprovalResponseDTO(personId, signatureCondition.getId(), true, "You signature already stamped on the payment.", paymentId);
            });

        });
    }

    private Boolean getIsValidStatus(EntityStatus status) {
        if (status.equals(EntityStatus.DELETED) || status.equals(EntityStatus.DISAPPROVED)) {
            return false;
        }
        return true;
    }

    private Boolean checkIsPartyAppointed(SigningDetail signingDetail) {
        if (signingDetail.getPartyId() == null || signingDetail.getPartyId().trim().isEmpty()) {
            return false;
        }
        return true;
    }


    private ApprovalResponseDTO getApprovalResponseForZeroValues(String entityId, EntityType entityType) {
        return ApprovalResponseDTO.builder()
                .entityId(entityId)
                .remaining(0)
                .roleId(null)
                .remainingRoleOrderings(new HashMap<>())
                .entityType(entityType)
                .done(true)
                .narrative("Approval successful.")
                .build();
    }

    private ApprovalResponseDTO getErrorApprovalResponseDTO(String personId, String signatureConditionId, boolean showNextSteps, String narrative, String paymentId) {

        if (showNextSteps) {
            Long remaining = getRemainingNumberOfApprovalsForSignatureCondition(signatureConditionId, paymentId);
            final SigningDetail approvalDetail = getNextApprover(signatureConditionId, paymentId);
            final Map<Integer, String> remainingRoleOrderings = getRemainingRoleOrderings(signatureConditionId, paymentId);
            final OptionalInt min = remainingRoleOrderings.keySet().stream().mapToInt(Integer::intValue).min();
            String nextApprover = null;
            if (min.isPresent()) {
                nextApprover = remainingRoleOrderings.get(min.getAsInt());
                //sent the notification
                if (approvalDetail != null) {
                    Notification notification = new Notification();
                    notification.setId(GenerateKey.generateEntityId());
                    notification.setDateCreated(OffsetDateTime.now());
                    notification.setClientId(personId);
                    notification.setRead(false);
                    notification.setNotificationType(NotificationType.PAYMENT_APPROVAL);
//                    notification.setGroupId(approvalDetail.getGroupId());
                    notification.setEntityStatus(EntityStatus.ACTIVE);
                    notification.setMessage("Payment has been created that needs your approval. You are requested to take " +
                            "the appropriate actions.");
                    notificationService.createNotification(notification);

                }
            }
            return ApprovalResponseDTO.builder()
                    .entityId(personId)
                    .remaining(remaining.intValue())
//                    .roleId(approvalDetail == null ? null : approvalDetail.getGroupId())
                    .remainingRoleOrderings(remainingRoleOrderings)
                    .done(false)
                    .narrative(narrative + (nextApprover == null ? "" : " Next Approver is: " + nextApprover))
                    .build();
        } else {
            return ApprovalResponseDTO.builder()
                    .entityId(signatureConditionId)
                    .remaining(0)
                    .roleId(null)
                    .done(false)
                    .remainingRoleOrderings(new HashMap<>())
                    .narrative(narrative)
                    .build();
        }
    }

    private Long getRemainingNumberOfApprovalsForSignatureCondition(String signatureConditionId, String paymentId) {
        try {
            final Long remaining = signingDetailRepository.findByRemainingNumberOfApprovalsForSignatureConditionAndPaymentId(signatureConditionId, false, paymentId);
            return remaining;
        } catch (RuntimeException re) {
            return 0L;
        }
    }

    private SigningDetail getNextApprover(String signatureConditionId, String paymentId) {
        Optional<SigningDetail> optionalSigningDetail = signingDetailRepository.findFirstBySignatureConditionIdAndDoneAndPaymentId(signatureConditionId, false, paymentId);
        return optionalSigningDetail.orElseGet(() -> null);
    }

    private Map<Integer, String> getRemainingRoleOrderings(String signatureConditionId, String paymentId) {
        final List<SigningDetail> signingDetails = signingDetailRepository.findAllBySignatureConditionIdAndPaymentId(signatureConditionId, paymentId);
        List<String> signerPartyIds = signingDetails.stream().filter(signingDetail -> signingDetail.getDone())
                .map(signingDetail -> signingDetail.getPartyId())
                .collect(Collectors.toList());
        Map<Integer, String> orderingsMap = new HashMap<>();

        Optional<CashAccountMandate> cashAccountMandateOptional = cashAccountMandateRepository.findFirstBySignatureConditions_Id(signatureConditionId);
        if (cashAccountMandateOptional.isPresent()) {
            CashAccountMandate cashAccountMandate = cashAccountMandateOptional.get();

            Optional<SignatureCondition> signatureConditionOptional = cashAccountMandate.getSignatureConditions().stream().filter(signatureCondition -> getIsValidStatus(signatureCondition.getEntityStatus()))
                    .findFirst();
            if (signatureConditionOptional.isPresent()) {
                int requiredSignatureNumber = signatureConditionOptional.get().getRequiredSignatureNumber().intValue();
                if (requiredSignatureNumber <= signerPartyIds.size()) {
                    if (signatureConditionOptional.get().getSignatoryRightIndicator()) {
                        log.info("The account owner signature is required. Checking if he is included.");
                        List<RolePlayer> rolePlayers = cashAccountMandate.getMandatePartyRole().stream().filter(mandatePartyRole -> mandatePartyRole instanceof MandateIssuer)
                                .map(mandatePartyRole -> mandatePartyRole.getPlayer())
                                .flatMap(Collection::stream)
                                .collect(Collectors.toList());
                        Map<Integer, String> result = new HashMap<Integer, String>();

                        return rolePlayers.stream().findFirst().map(rolePlayer -> {
                            if (signerPartyIds.contains(rolePlayer.getId())) {
                                log.info("The account owner signature is included in the signature.");
                                return new HashMap<Integer, String>();
                            } else {
                                return getPersonIdentificationMap(orderingsMap, (Person) rolePlayer);
                            }

                        }).orElseGet(HashMap::new);

                    } else {
                        return new HashMap<Integer, String>();
                    }
                }

                if (signatureConditionOptional.get().getSignatureOrderIndicator()) {
                    log.info("The signature order is required.");
                    List<String> orderPersonsIds = Arrays.asList(signatureConditionOptional.get().getSignatureOrder().split(","));
                    Optional<String> optionalOrderPersonId = orderPersonsIds.stream().filter(s -> !signerPartyIds.contains(s)).findFirst();
                    if (optionalOrderPersonId.isPresent()) {
                        return rolePlayerRepository.findById(optionalOrderPersonId.get()).map(rolePlayer -> {
                            return getPersonIdentificationMap(orderingsMap, (Person) rolePlayer);
                        }).orElseGet(HashMap::new);
                    }
                }
                log.info("The signature order is ignored.");
                cashAccountMandate.getMandatePartyRole().stream()
                        .filter(mandatePartyRole -> {
                            if(mandatePartyRole instanceof MandateHolder) {
                                return canMandateAuthorise(((MandateHolder) mandatePartyRole).getHolderType());
                            }
                            return true;
                        })
                        .forEach(mandatePartyRole -> {
                            mandatePartyRole.getPlayer().stream().filter(rolePlayer -> !signerPartyIds.contains(rolePlayer.getId())).forEach(rolePlayer -> {
                                Person person = (Person) rolePlayer;
                                StringBuilder personNameSb = new StringBuilder();

                                person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                                    personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                                        final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                                        personNameSb.append(personFullName);
                                    });
                                });
                                orderingsMap.put(1, personNameSb.toString());
                            });
                        });


            } else {
                return new HashMap<Integer, String>();
            }

        }
        return orderingsMap;
    }

    private Map<Integer, String> getPersonIdentificationMap(Map<Integer, String> orderingsMap, Person rolePlayer) {
        Person person = rolePlayer;
        StringBuilder personNameSb = new StringBuilder();

        person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
            personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                personNameSb.append(personFullName);
            });
        });
        orderingsMap.put(1, personNameSb.toString());
        return orderingsMap;
    }

    private ApprovalResponseDTO getNormalApprovalResponseDTO(String signatureConditionId, String personId, String paymentId) {
        Long remaining = getRemainingNumberOfApprovalsForSignatureCondition(signatureConditionId, paymentId);
        final SigningDetail approvalDetail = getNextApprover(signatureConditionId, paymentId);
        final Map<Integer, String> remainingRoleOrderings = getRemainingRoleOrderings(signatureConditionId, paymentId);
        final OptionalInt min = remainingRoleOrderings.keySet().stream().mapToInt(Integer::intValue).min();
        String nextApprover = null;
        if (min.isPresent()) {
            nextApprover = remainingRoleOrderings.get(min.getAsInt());
            //sent the notification
            if (approvalDetail != null) {
                Notification notificationDTO = new Notification();
                notificationDTO.setId(GenerateKey.generateEntityId());
                notificationDTO.setDateCreated(OffsetDateTime.now());
                notificationDTO.setClientId(personId);
                notificationDTO.setRead(false);
                notificationDTO.setNotificationType(NotificationType.PAYMENT_APPROVAL);
//                notificationDTO.setGroupId(approvalDetail.getGroupId());
                notificationDTO.setEntityStatus(EntityStatus.ACTIVE);
                notificationDTO.setMessage("Payment has been created that needs your approval. You are requested to take " +
                        "the appropriate actions.");
                notificationService.createNotification(notificationDTO);
            }
        }
        return ApprovalResponseDTO.builder()
                .entityId(personId)
                .remaining(remaining.intValue())
//                .roleId(approvalDetail == null ? null : approvalDetail.getGroupId())
                .remainingRoleOrderings(remainingRoleOrderings)
                .done(nextApprover == null ? true : false)
                .narrative(nextApprover == null ? "Approval Steps for Mandate Complete" : "The next Approver is: " + nextApprover)
                .build();

    }


}
