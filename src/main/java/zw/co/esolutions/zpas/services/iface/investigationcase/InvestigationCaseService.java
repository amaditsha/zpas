package zw.co.esolutions.zpas.services.iface.investigationcase;

import zw.co.esolutions.zpas.model.InvestigationCase;

public interface InvestigationCaseService {
    InvestigationCase createInvestigationCase(InvestigationCase investigationCase);
    InvestigationCase updateInvestigationCase(InvestigationCase investigationCase);
    InvestigationCase deleteInvestigationCase(String id);
    InvestigationCase approveInvestigationCase(String id);
    InvestigationCase rejectInvestigationCase(String id);
    InvestigationCase findById(String id);
}
