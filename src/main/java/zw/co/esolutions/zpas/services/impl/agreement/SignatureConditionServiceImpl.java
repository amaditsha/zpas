package zw.co.esolutions.zpas.services.impl.agreement;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.agreement.SignatureConditionDTO;
import zw.co.esolutions.zpas.model.SignatureCondition;
import zw.co.esolutions.zpas.repository.RuleItemRepository;
import zw.co.esolutions.zpas.repository.SignatureConditionRepository;
import zw.co.esolutions.zpas.services.iface.agreement.SignatureConditionService;
import zw.co.esolutions.zpas.services.iface.signature.RuleItemService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SignatureConditionServiceImpl implements SignatureConditionService {

    @Autowired
    private SignatureConditionRepository signatureConditionRepository;

    @Autowired
    private RuleItemService ruleItemService;

    @Override
    public SignatureCondition createSignatureCondition(SignatureConditionDTO signatureConditionDTO) {
        SignatureCondition signatureCondition = new SignatureCondition();
        signatureCondition.setRequiredSignatureNumber(new BigDecimal(signatureConditionDTO.getRequiredSignatureNumber()));
        signatureCondition.setSignatoryRightIndicator(getBooleanFormValue(signatureConditionDTO.getSignatoryRightIndicator()));
        signatureCondition.setSignatureOrder(signatureConditionDTO.getSignatureOrder());
        signatureCondition.setDescription(signatureConditionDTO.getDescription());
        signatureCondition.setSignatureOrderIndicator(getBooleanFormValue(signatureConditionDTO.getSignatureOrderIndicator()));
        return signatureConditionRepository.save(signatureCondition);
    }

    private Boolean getBooleanFormValue(String booleanStringValue) {
        booleanStringValue = booleanStringValue == null ? "false" : booleanStringValue.equalsIgnoreCase("on")?
                "true": booleanStringValue.equalsIgnoreCase("true")? "true": "false";
        return Boolean.valueOf(booleanStringValue);
    }

    @Override
    public SignatureCondition updateSignatureCondition(SignatureConditionDTO signatureConditionDTO) {
        Optional<SignatureCondition> signatureConditionOptional = signatureConditionRepository.findById(signatureConditionDTO.getId());
        return signatureConditionOptional.map(signatureCondition -> {
            signatureCondition.setRequiredSignatureNumber(new BigDecimal(signatureConditionDTO.getRequiredSignatureNumber()));
            signatureCondition.setSignatoryRightIndicator(getBooleanFormValue(signatureConditionDTO.getSignatoryRightIndicator()));
            signatureCondition.setSignatureOrder(signatureConditionDTO.getSignatureOrder());
            signatureCondition.setSignatureOrderIndicator(getBooleanFormValue(signatureConditionDTO.getSignatureOrderIndicator()));

            signatureCondition.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
            return signatureConditionRepository.save(signatureCondition);
        }).orElseGet(() -> {
            log.error("Error occurred while updating the signature condition");
            return null;
        });

    }

    @Override
    public SignatureCondition deleteSignatureCondition(String id) {
        Optional<SignatureCondition> signatureConditionOptional = signatureConditionRepository.findById(id);
        SignatureCondition signatureCondition = null;
        if (signatureConditionOptional.isPresent()) {
            signatureCondition = signatureConditionOptional.get();
            signatureCondition.setEntityStatus(EntityStatus.DELETED);
            signatureCondition = signatureConditionRepository.save(signatureCondition);
        }
        return signatureCondition;
    }


    @Override
    public SignatureCondition approveSignatureCondition(String id) {
        Optional<SignatureCondition> optionalSignatureCondition = signatureConditionRepository.findById(id);
        SignatureCondition signatureCondition = null;
        if (optionalSignatureCondition.isPresent()) {
            signatureCondition = optionalSignatureCondition.get();
            if(signatureCondition.getEntityStatus() != EntityStatus.DELETED) {
                signatureCondition.setEntityStatus(EntityStatus.ACTIVE);
                signatureCondition = signatureConditionRepository.save(signatureCondition);
            } else {
                log.info("The Signature Condition status has already been deleted.");
            }
        }
        return signatureCondition;
    }

    @Override
    public SignatureCondition rejectSignatureCondition(String id) {
        Optional<SignatureCondition> optionalSignatureCondition = signatureConditionRepository.findById(id);
        SignatureCondition signatureCondition = null;
        if (optionalSignatureCondition.isPresent()) {
            signatureCondition = optionalSignatureCondition.get();
            if(signatureCondition.getEntityStatus() != EntityStatus.DELETED) {
                signatureCondition.setEntityStatus(EntityStatus.DISAPPROVED);
                signatureCondition = signatureConditionRepository.save(signatureCondition);
            } else {
                log.info("The Signature Condition status has already been deleted.");
            }
        }
        return signatureCondition;
    }

    @Override
    public Optional<SignatureCondition> getSignatureConditionById(String signatureId) {
        return signatureConditionRepository.findById(signatureId);
    }

    @Override
    public List<SignatureCondition> getSignatureConditions() {
        return signatureConditionRepository.findAll();
    }
}
