package zw.co.esolutions.zpas.services.impl.process.builder;

import zw.co.esolutions.zpas.iso.msg.camt036_001_05.ActiveCurrencyAndAmount;
import zw.co.esolutions.zpas.iso.msg.camt036_001_05.Case5;
import zw.co.esolutions.zpas.iso.msg.camt036_001_05.DebitAuthorisationConfirmation2;
import zw.co.esolutions.zpas.iso.msg.camt036_001_05.CaseAssignment5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt036_001_05.DebitAuthorisationResponseV05;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.services.impl.process.pojo.DebitAuthorisationDTO;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationPartyRole_036Util;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentInfoUtil;

import java.time.OffsetDateTime;
import java.util.Optional;

@Slf4j
@Component
public class DebitAuthorisationResponseMessageBuilder {
    @Autowired
    InvestigationPartyRole_036Util investigationPartyRoleUtil;

    @Autowired
    PaymentInfoUtil paymentInfoUtil;

    public DebitAuthorisationResponseV05 buildDebitAuthorisationResponseMsg(DebitAuthorisationDTO debitAuthorisationDTO){
        DebitAuthorisationResponseV05 debitAuthorisationResponseV05 = new DebitAuthorisationResponseV05();

        CaseAssignment5 assignment = new CaseAssignment5();
        assignment.setId(debitAuthorisationDTO.getAssignmentIdentification());
        assignment.setAssgnr(investigationPartyRoleUtil.getParty40Choice(debitAuthorisationDTO.getPaymentInvestigationCase(), Assigner.class));
        assignment.setAssgne(investigationPartyRoleUtil.getParty40Choice(debitAuthorisationDTO.getPaymentInvestigationCase(), Assignee.class));
        assignment.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        Case5 case5 = new Case5();
        case5.setId(debitAuthorisationDTO.getPaymentInvestigationCaseIdentification());
        case5.setCretr(investigationPartyRoleUtil.getParty40Choice(debitAuthorisationDTO.getPaymentInvestigationCase(), CaseCreator.class));
        case5.setReopCaseIndctn(false);

        Payment payment = getUnderlyingPayment(debitAuthorisationDTO.getPaymentInvestigationCase());

        ActiveCurrencyAndAmount amountToDebit = new ActiveCurrencyAndAmount();
        amountToDebit.setValue(payment.getInstructedAmount().getAmount());
        amountToDebit.setCcy(payment.getInstructedAmount().getCurrency().getCodeName());

        DebitAuthorisationConfirmation2 confirmation = new DebitAuthorisationConfirmation2();
        confirmation.setDbtAuthstn(debitAuthorisationDTO.getAuthorisation());
        confirmation.setAmtToDbt(amountToDebit);
        if(debitAuthorisationDTO.getValueDateToDebit() != null){
            confirmation.setValDtToDbt(XmlDateUtil.getXmlDateNoTime(debitAuthorisationDTO.getValueDateToDebit()));
        }
        confirmation.setRsn(debitAuthorisationDTO.getReason());

        debitAuthorisationResponseV05.setAssgnmt(assignment);
        debitAuthorisationResponseV05.setCase(case5);
        debitAuthorisationResponseV05.setConf(confirmation);

        return debitAuthorisationResponseV05;
    }

    private Payment getUnderlyingPayment(PaymentInvestigationCase paymentInvestigationCase){
        Optional<Payment> optionalPayment = paymentInvestigationCase.getUnderlyingPayment().stream().findFirst();
        Payment payment = null;

        if(optionalPayment.isPresent()){
            payment = optionalPayment.get();
        }

        return payment;
    }
}
