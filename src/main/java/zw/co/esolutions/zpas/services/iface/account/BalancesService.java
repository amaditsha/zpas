package zw.co.esolutions.zpas.services.iface.account;

import zw.co.esolutions.zpas.model.CashBalance;

import java.util.List;
import java.util.Optional;

public interface BalancesService {
    CashBalance createCashBalance(CashBalance cashBalance);
    Optional<CashBalance> findBalanceById(String id);
    CashBalance updateCashBalance(CashBalance cashBalance);
    CashBalance deleteCashBalance(String id);
}
