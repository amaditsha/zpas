package zw.co.esolutions.zpas.services.impl.process;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;
import zw.co.esolutions.zpas.enums.DebitCreditCode;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.enums.StatusCode;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.iso.msg.pain001_001_08.*;
import zw.co.esolutions.zpas.iso.msg.pain008_001_07.CustomerDirectDebitInitiationV07;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentExecutionRepository;
import zw.co.esolutions.zpas.services.iface.process.PaymentInitiationService;
import zw.co.esolutions.zpas.services.impl.process.builder.CustomerCreditTransferMessageBuilder;
import zw.co.esolutions.zpas.services.impl.process.builder.CustomerDirectDebitMessageBuilder;
import zw.co.esolutions.zpas.services.impl.process.pojo.StatusReasonDto;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentStatusUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@Transactional
public class PaymentInitiationServiceImpl implements PaymentInitiationService {


    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.pain001_001_08");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Value("${XSD.BASE.PATH}")
    String xsdBasePath;

    @Autowired
    CustomerCreditTransferMessageBuilder customerCreditTransferMessageBuilder;

    @Autowired
    CustomerDirectDebitMessageBuilder customerDirectDebitMessageBuilder;

    @Autowired
    PaymentExecutionRepository paymentExecutionRepository;

    @Autowired
    PaymentStatusUtil paymentStatusUtil;

    @Autowired
    MessageProducer messageProducer;

    @Override
    public ServiceResponse initiateCustomerCreditTransfer(Payment payment) {
        try {
            log.info("Handling Customer Credit Transfer Initiation request");

            //build the message
            CustomerCreditTransferInitiationV08 customerCreditTransferInitiationV08 = customerCreditTransferMessageBuilder
                    .buildCustomerCreditTransferInitiationMsg(payment);

            //create payment initiation
            this.createPaymentInitiation(payment, customerCreditTransferInitiationV08.getGrpHdr().getMsgId());

            //update payment status
            List<StatusReasonDto> statusReasonDtoList = Arrays.asList(new StatusReasonDto(StatusCode.Pending.getCodeName(),
                    Arrays.asList("Credit Transfer Initiation")));

            paymentStatusUtil.updatePaymentStatus(payment, PaymentStatusCode.Pending, statusReasonDtoList);

            //send the message to Hub
            String xml = buildXml(customerCreditTransferInitiationV08);
            String destinationBic = customerCreditTransferInitiationV08.getPmtInf()
                    .stream().findFirst().map(paymentInfo -> paymentInfo.getDbtrAgt().getFinInstnId()
                            .getBICFI()).orElse("");
            messageProducer.sendMessageToHub(xml, destinationBic);

            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_SUCCESS)
                    .narrative("Customer Credit Transfer Payment initiated successfully")
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .narrative("Customer Credit Transfer Payment initiation failed : " + e.getMessage())
                    .build();
        }
    }

    @Override
    public ServiceResponse initiateCustomerDirectDebit(Payment payment) {
        try {
            log.info("Handling Customer Direct Debit Initiation request");

            //build the message
            CustomerDirectDebitInitiationV07 customerDirectDebitInitiationV07 = customerDirectDebitMessageBuilder
                    .buildCustomerDirectDebitInitiationMsg(payment);

            //create payment initiation
            this.createPaymentInitiation(payment, customerDirectDebitInitiationV07.getGrpHdr().getMsgId());

            //update payment status
            List<StatusReasonDto> statusReasonDtoList = Arrays.asList(new StatusReasonDto(StatusCode.Pending.getCodeName(),
                    Arrays.asList("Direct Debit Initiation")));

            paymentStatusUtil.updatePaymentStatus(payment, PaymentStatusCode.Pending, statusReasonDtoList);

            //send message to Hub
            String destinationBic = customerDirectDebitInitiationV07.getPmtInf()
                    .stream().findFirst().map(paymentInfo -> paymentInfo.getCdtrAgt()
                            .getFinInstnId().getBICFI()).orElse("");
            messageProducer.sendMessageToHub(buildXml(customerDirectDebitInitiationV07), destinationBic);

            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_SUCCESS)
                    .narrative("Customer Direct Debit Transfer Payment initiated successfully")
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .narrative("Customer Direct Debit Transfer Payment initiation failed : " + e.getMessage())
                    .build();
        }
    }

    private void createPaymentInitiation(Payment payment, String messageId) {
        log.info("Creating the payment execution.");

        //create payment initiation
        PaymentExecution paymentExecution = new PaymentInitiation();
        paymentExecution.setId(Optional.ofNullable(messageId).orElse(GenerateKey.generateEntityId()));
        paymentExecution.setEntityStatus(EntityStatus.PAYMENT_INITIATED);
        paymentExecution.setDateCreated(OffsetDateTime.now());
        paymentExecution.setLastUpdated(OffsetDateTime.now());
        paymentExecution.setEntityVersion(0L);
        paymentExecution.setCreditDebitIndicator(DebitCreditCode.Credit);
        paymentExecution.setCreationDate(OffsetDateTime.now());
        paymentExecution.setAcceptanceDateTime(OffsetDateTime.now());
        paymentExecution.setPayment(Arrays.asList(payment));
        paymentExecution.setRequestedExecutionDate(payment.getValueDate());

        log.info("Payment Execution is: {}", paymentExecution);

        paymentExecutionRepository.save(paymentExecution);

        log.info("Saved the Payment Execution [Initiation] with MsgID : {}", paymentExecution.getId());

    }

    private String buildXml(CustomerCreditTransferInitiationV08 customerCreditTransferInitiationV08) throws JAXBException, SAXException {
        log.info("Building pain.001 XML message..");
        zw.co.esolutions.zpas.iso.msg.pain001_001_08.Document document = new zw.co.esolutions.zpas.iso.msg.pain001_001_08.Document();
        document.setCstmrCdtTrfInitn(customerCreditTransferInitiationV08);

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, xmlEncoding);


        StringWriter stringWriterTest = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriterTest);

        String xml1 = stringWriterTest.toString();
        log.info("The message before Validation is:\n{}\n", xml1);

//        //Setup schema validator
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema xsdSchema = sf.newSchema(new File(xsdBasePath + "/xsd/pain/pain.001.001.08.xsd"));
        jaxbMarshaller.setSchema(xsdSchema);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);

        return xml;
    }

    private String buildXml(CustomerDirectDebitInitiationV07 customerDirectDebitInitiationV07) throws JAXBException, SAXException {
        log.info("Building pain.008 XML message..");
        zw.co.esolutions.zpas.iso.msg.pain008_001_07.Document document = new zw.co.esolutions.zpas.iso.msg.pain008_001_07.Document();
        document.setCstmrDrctDbtInitn(customerDirectDebitInitiationV07);

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, xmlEncoding);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);

        return xml;
    }

}
