package zw.co.esolutions.zpas.services.iface.payments;

import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentActivationInvalidRequestException;
import zw.co.esolutions.zpas.dto.payments.ActivatePaymentRequestDTO;
import zw.co.esolutions.zpas.model.PaymentObligation;

import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 26 Apr 2019
 */
public interface PaymentObligationsService {
    List<PaymentObligation> getPaymentObligationsByClientId(String clientId);
    List<PaymentObligation> getPaymentObligationsForIds(List<String> ids);
    List<PaymentObligation> getPaymentObligationsRaisedByClient(String clientId);
    List<PaymentObligation> getPaymentObligationsTargetingClient(String clientId);
    List<PaymentObligation> saveObligations(List<PaymentObligation> paymentObligations);
    Optional<PaymentObligation> getPaymentObligationsById(String paymentObligationId);
    Optional<PaymentObligation> activateCreditorPaymentRequest(ActivatePaymentRequestDTO activatePaymentRequestDTO) throws PaymentActivationInvalidRequestException;
    Optional<PaymentObligation> approveCreditorPaymentRequest(String obligationId, String clientId);
    Optional<PaymentObligation> rejectCreditorPaymentRequest(String obligationId, String clientId);
}
