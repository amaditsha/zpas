package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt036_001_05.*;
import zw.co.esolutions.zpas.model.*;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
@Component
public class InvestigationPartyRole_036Util {
    public PartyIdentification135 getPartyIdentification135(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        PartyIdentification135  partyIdentification135  = new PartyIdentification135 ();

        Optional<Party> partyOptional = this.getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            partyIdentification135.setNm(this.getPartyName(partyOptional.get()));

            partyIdentification135.setId(this.getParty38Choice(investigationCase, investigationPartyRoleSubclass));
        }
        return partyIdentification135 ;
    }

    public Optional<InvestigationPartyRole> getInvestigationPartyRole(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        final Optional<InvestigationPartyRole> investigationPartyRoleOptional = investigationCase.getInvestigationPartyRole().stream()
                .filter(investigationPartyRole -> investigationPartyRoleSubclass.isInstance(investigationPartyRole))
                .findFirst();

        return investigationPartyRoleOptional;
    }


    public Optional<Party> getParty(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {

        final Optional<InvestigationPartyRole> investigationPartyRoleOptional = this.getInvestigationPartyRole(investigationCase, investigationPartyRoleSubclass);

        //if party role present, extract the party
        if (investigationPartyRoleOptional.isPresent()) {

            InvestigationPartyRole investigationPartyRole = investigationPartyRoleOptional.get();

            log.info("Party is present, building the info in the msg..");

            if (investigationPartyRole.isHasCustomParty()) {
                log.info("Getting custom party");
                CustomParty customParty = investigationPartyRole.getCustomParty();

                Party party = new Person();

                PersonIdentification personIdentification = new PersonIdentification();
                personIdentification.setIdentityCardNumber(customParty.getNationalId());
                personIdentification.setPassportNumber(customParty.getPassportNumber());

                PersonName personName = new PersonName();
                personName.setName(customParty.getName());

                personIdentification.setPersonName(Arrays.asList(personName));

                party.setIdentification(Arrays.asList(personIdentification));

                return Optional.of(party);

            } else {
                log.info("Getting registered party");
                Optional<RolePlayer> rolePlayerOptional = investigationPartyRole.getPlayer().stream()
                        .findFirst();

                //if role player present, return the party
                if (rolePlayerOptional.isPresent()) {

                    Party party = (Party) rolePlayerOptional.get();

                    return Optional.of(party);

                }
            }
        }

        return Optional.empty();
    }

    public Party38Choice getParty38Choice(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        Party38Choice  party38Choice  = new Party38Choice();

        Optional<Party> partyOptional = getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            Party party = partyOptional.get();

            if (party instanceof Organisation) {
                Organisation debtorOrganisation = (Organisation) party;

                OrganisationIdentification29 organisationIdentification8 = new OrganisationIdentification29();
                organisationIdentification8.setAnyBIC(debtorOrganisation.getOrganisationIdentification().stream()
                        .map(OrganisationIdentification::getAnyBIC)
                        .findFirst().orElse(""));
                party38Choice.setOrgId(organisationIdentification8);
            } else {
                Person debtorPerson = (Person) party;

                PersonIdentification13 debtorPersonIdentification13 = new PersonIdentification13();
                GenericPersonIdentification1 debtorGenericPersonIdentification1 = new GenericPersonIdentification1();
                debtorGenericPersonIdentification1.setId(debtorPerson.getPersonIdentification().stream()
                        .map(PersonIdentification::getIdentityCardNumber)
                        .findFirst().orElse(""));
                //debtorGenericPersonIdentification1.setSchmeNm(new PersonIdentificationSchemeName1Choice());
                //debtorGenericPersonIdentification1.setIssr("");

                debtorPersonIdentification13.getOthr().add(debtorGenericPersonIdentification1);

                party38Choice.setPrvtId(debtorPersonIdentification13);
            }
        }
        return party38Choice ;
    }

    public Party40Choice getParty40Choice(InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {
        Party40Choice  party40Choice  = new Party40Choice();

        Optional<Party> partyOptional = getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            Party party = partyOptional.get();

            if (party instanceof FinancialInstitution) {
                FinancialInstitution financialInstitution = (FinancialInstitution) party;

                BranchAndFinancialInstitutionIdentification6 branchAndFinancialInstitutionIdentification6
                        = new BranchAndFinancialInstitutionIdentification6();
                FinancialInstitutionIdentification18 financialInstitutionIdentification18 = new FinancialInstitutionIdentification18();
                financialInstitutionIdentification18.setBICFI(financialInstitution.getOrganisationIdentification().stream()
                        .findFirst().get().getBICFI());
                branchAndFinancialInstitutionIdentification6.setFinInstnId(financialInstitutionIdentification18);
                party40Choice.setAgt(branchAndFinancialInstitutionIdentification6);
            } else {

                PartyIdentification135 partyIdentification135 = new PartyIdentification135();

                partyIdentification135.setId(this.getParty38Choice(investigationCase, investigationPartyRoleSubclass));
                partyIdentification135.setNm(this.getPartyName(party));

                party40Choice.setPty(partyIdentification135);
            }
        }
        return party40Choice ;
    }


    public BranchAndFinancialInstitutionIdentification6 getBranchAndFinancialInstitutionIdentification5 (InvestigationCase investigationCase, Class<?> investigationPartyRoleSubclass) {

        BranchAndFinancialInstitutionIdentification6  branchAndFinancialInstitutionIdentification6 = new BranchAndFinancialInstitutionIdentification6 ();

        FinancialInstitutionIdentification18 financialInstitutionIdentification18  = new FinancialInstitutionIdentification18 ();

        Optional<Party> partyOptional = this.getParty(investigationCase, investigationPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            Organisation organisation = (Organisation) partyOptional.get();

            financialInstitutionIdentification18 .setBICFI(organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getBICFI).findFirst().orElse(""));

            financialInstitutionIdentification18 .setNm(organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getOrganisationName)
                    .flatMap(organisationNames -> organisationNames.stream())
                    .map(OrganisationName::getShortName)
                    .findFirst().orElse(""));

            branchAndFinancialInstitutionIdentification6.setFinInstnId(financialInstitutionIdentification18 );
        }
        return branchAndFinancialInstitutionIdentification6;
    }

    public String getPartyName(Party party) {
        if (party instanceof Organisation) {
            Organisation organisation = (Organisation) party;
            final Stream<OrganisationName> organisationNameStream = organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getOrganisationName)
                    .flatMap(organisationNames -> organisationNames.stream());

            return organisationNameStream.map(organisationName ->
                    organisationName == null ? "" : organisationName.getLegalName()
            ).findFirst().orElse("");
        } else {
            Person person = (Person) party;
            final Stream<PersonName> personNameStream = person.getPersonIdentification().stream()
                    .map(PersonIdentification::getPersonName)
                    .flatMap(personNames -> personNames.stream());

            return personNameStream.map(personName ->
                    personName == null ?
                            "" : personName.getBirthName() + " " + personName.getGivenName()
            ).findFirst().orElse("");
        }
    }

    /*public String getPartyName(Party party) {
        final Stream<PartyName> partyNameStream = party.getIdentification().stream()
                .map(PartyIdentificationInformation::getPartyName)
                .flatMap(partyNames -> partyNames.stream());

        if (party instanceof Organisation) {
            return partyNameStream.map(partyName -> {
                OrganisationName organisationName = (OrganisationName) partyName;
                return organisationName.getLegalName() != null ? organisationName.getLegalName() : organisationName.getName();
            }).findFirst().orElse("");
        } else {
            return partyNameStream.map(partyName -> {
                PersonName personName = (PersonName) partyName;
                return personName.getName() == null ?
                        personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName() : personName.getName();
            }).findFirst().orElse("");
        }
    }*/
}
