package zw.co.esolutions.zpas.services.iface.enversaudit;

import zw.co.esolutions.zpas.model.enversaudit.AccountAuditResult;

import java.util.List;

public interface AuditAccountService {
    List<AccountAuditResult> auditAccount(String accountId);
}
