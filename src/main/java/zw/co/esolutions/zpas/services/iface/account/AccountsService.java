package zw.co.esolutions.zpas.services.iface.account;

import zw.co.esolutions.zpas.dto.account.AccountSearchResult;
import zw.co.esolutions.zpas.dto.account.CashAccountDTO;
import zw.co.esolutions.zpas.dto.party.VirtualCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.payments.RequestInfo;
import zw.co.esolutions.zpas.model.Account;
import zw.co.esolutions.zpas.model.CashAccount;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by petros on 14 Feb 2019
 */
public interface AccountsService {
    CashAccount createCashAccount(CashAccountDTO cashAccountDTO, String clientId, String fiId);

    CashAccount deleteCashAccount(String id);

    CashAccount approveCashAccount(String id);

    CashAccount rejectCashAccount(String id);

    CashAccount findAccountById(String id);

    Optional<VirtualCodeAvailabilityCheckDTO> checkVirtualCodeAvailability(String virtualCode);
    Optional<AccountSearchResult> searchAccountByVirtualId(String virtualId);

    Optional<CashAccount> findAccountByIban(String iBan);
    Optional<CashAccount> findAccountByNumber(String number);
    Map<String, CashAccount> findAccountsByIbansIn(List<String> iBans);

    CashAccount updateCashAccount(CashAccountDTO cashAccountDTO);

    List<Account> getAccountsByClientId(String clientId);

    List<CashAccount> getCashAccountsByClientId(String clientId);

    List<CashAccount> searchAccounts(String search, String fiId);

    CashAccount aggregateAccountInfoRequest(RequestInfo requestInfo);
    String accountBalanceRequest(String clientId);
}

