package zw.co.esolutions.zpas.services.iface.audittrail;

import zw.co.esolutions.zpas.dto.audittrail.LogDTO;
import zw.co.esolutions.zpas.model.audittrail.Activity;
import zw.co.esolutions.zpas.model.audittrail.AuditTrail;

import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
public interface AuditTrailService {
    AuditTrail logActivity(LogDTO logDTO);
    AuditTrail logActivityWithNarrative(LogDTO logDTO);
    AuditTrail logChanges(String newObject, String oldObject, String entityId, String entityName,
                          Activity activity, String username, String instanceName);
    List<AuditTrail> getByEntityNameAndInstanceNameAndTimePeriod(String entityName, String instanceName, String startTime, String endTime, String fiId);
    List<AuditTrail> getByUsername(String userName, String fiId);
    List<AuditTrail> getByUsernameAndTimePeriod(String userName, String startTime, String endTime, String fiId);
    List<AuditTrail> getByActivityAndTimePeriod(String activityId, String startTime, String endTime, String fiId);
    List<AuditTrail> getByEntityNameAndEntityId(String entityName, String entityId);
    List<AuditTrail> getByEntityId(String entityId);
    List<AuditTrail> getByTimePeriod(String startTime, String endTime, String fiId);
    List<AuditTrail> searchAuditTrails(String search);
    Activity editActivity();
}
