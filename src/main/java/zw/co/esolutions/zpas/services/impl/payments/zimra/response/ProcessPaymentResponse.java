package zw.co.esolutions.zpas.services.impl.payments.zimra.response;

import lombok.Builder;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alfred on 21 August 2019
 */
@Builder
@XmlRootElement(name = "Receipt")
public class ProcessPaymentResponse {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "SerialNumber")
    private String serialNumber;

    @XmlElement(name = "ReferenceNumber")
    private String referenceNumber;

    @XmlElement(name = "BPNumber")
    private String bpNumber;

    @XmlElement(name = "ClientName")
    private String clientName;

    @XmlElement(name = "AccountNumber")
    private String accountNumber;

    @XmlElement(name = "TaxCode")
    private String taxCode;

    @XmlElement(name = "Region")
    private String region;

    @XmlElement(name = "Currency")
    private String currency;

    @XmlElement(name = "Amount")
    private Double amount;

    @XmlElement(name = "PaymentDate")
    private String paymentDate;

    @XmlElement(name = "CaptureTime")
    private String captureTime;

    @XmlElement(name = "ReceiptNumber")
    private String receiptNumber;

    @XmlElement(name = "CustomsReceiptNumber")
    private String customsReceiptNumber;

    @XmlElement(name = "ReceiptDate")
    private String receiptDate;

    @XmlElement(name = "CustomsReceiptDate")
    private String customsReceiptDate;

    @XmlElement(name = "ReceiptTime")
    private String receiptTime;

    @XmlElement(name = "Type")
    private String type;

    @XmlElement(name = "ID")
    private String id;

    @XmlElement(name = "Number")
    private String number;

    @XmlElement(name = "Message")
    private String message;

    @XmlElement(name = "LogNumber")
    private String logNumber;

    @XmlElement(name = "LogMessageNumber")
    private String logMessageNumber;

    @XmlElement(name = "MessageV1")
    private String messageV1;

    @XmlElement(name = "MessageV2")
    private String messageV2;

    @XmlElement(name = "MessageV3")
    private String messageV3;

    @XmlElement(name = "MessageV4")
    private String messageV4;

    @XmlElement(name = "CustomsMessage")
    private String customsMessage;

    @XmlElement(name = "RRN")
    private String rrn;

    @XmlElement(name = "UserID")
    private String userID;


    // Getter Methods

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getBpNumber() {
        return bpNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public String getRegion() {
        return region;
    }

    public String getCurrency() {
        return currency;
    }

    public Double getAmount() {
        return amount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public String getCaptureTime() {
        return captureTime;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public String getCustomsReceiptNumber() {
        return customsReceiptNumber;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public String getCustomsReceiptDate() {
        return customsReceiptDate;
    }

    public String getReceiptTime() {
        return receiptTime;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getMessage() {
        return message;
    }

    public String getLogNumber() {
        return logNumber;
    }

    public String getLogMessageNumber() {
        return logMessageNumber;
    }

    public String getMessageV1() {
        return messageV1;
    }

    public String getMessageV2() {
        return messageV2;
    }

    public String getMessageV3() {
        return messageV3;
    }

    public String getMessageV4() {
        return messageV4;
    }

    public String getCustomsMessage() {
        return customsMessage;
    }

    public String getRrn() {
        return rrn;
    }

    public String getUserID() {
        return userID;
    }

    public String getXmlns() {
        return xmlns;
    }

    // Setter Methods

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public void setBpNumber(String bpNumber) {
        this.bpNumber = bpNumber;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setCaptureTime(String captureTime) {
        this.captureTime = captureTime;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public void setCustomsReceiptNumber(String customsReceiptNumber) {
        this.customsReceiptNumber = customsReceiptNumber;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }

    public void setCustomsReceiptDate(String customsReceiptDate) {
        this.customsReceiptDate = customsReceiptDate;
    }

    public void setReceiptTime(String receiptTime) {
        this.receiptTime = receiptTime;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setLogNumber(String logNumber) {
        this.logNumber = logNumber;
    }

    public void setLogMessageNumber(String logMessageNumber) {
        this.logMessageNumber = logMessageNumber;
    }

    public void setMessageV1(String messageV1) {
        this.messageV1 = messageV1;
    }

    public void setMessageV2(String messageV2) {
        this.messageV2 = messageV2;
    }

    public void setMessageV3(String messageV3) {
        this.messageV3 = messageV3;
    }

    public void setMessageV4(String messageV4) {
        this.messageV4 = messageV4;
    }

    public void setCustomsMessage(String customsMessage) {
        this.customsMessage = customsMessage;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    @Override
    public String toString() {
        return "ProcessPaymentResponse{" +
                "xmlns='" + xmlns + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", referenceNumber='" + referenceNumber + '\'' +
                ", bpNumber='" + bpNumber + '\'' +
                ", clientName='" + clientName + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", taxCode='" + taxCode + '\'' +
                ", region='" + region + '\'' +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", paymentDate='" + paymentDate + '\'' +
                ", captureTime='" + captureTime + '\'' +
                ", receiptNumber='" + receiptNumber + '\'' +
                ", customsReceiptNumber='" + customsReceiptNumber + '\'' +
                ", receiptDate='" + receiptDate + '\'' +
                ", customsReceiptDate='" + customsReceiptDate + '\'' +
                ", receiptTime='" + receiptTime + '\'' +
                ", type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", number='" + number + '\'' +
                ", message='" + message + '\'' +
                ", logNumber='" + logNumber + '\'' +
                ", logMessageNumber='" + logMessageNumber + '\'' +
                ", messageV1='" + messageV1 + '\'' +
                ", messageV2='" + messageV2 + '\'' +
                ", messageV3='" + messageV3 + '\'' +
                ", messageV4='" + messageV4 + '\'' +
                ", customsMessage='" + customsMessage + '\'' +
                ", rrn='" + rrn + '\'' +
                ", userID='" + userID + '\'' +
                '}';
    }
}
