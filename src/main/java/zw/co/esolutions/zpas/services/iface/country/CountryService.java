package zw.co.esolutions.zpas.services.iface.country;

import zw.co.esolutions.zpas.dto.country.CountryDTO;
import zw.co.esolutions.zpas.model.Country;

import java.util.List;
import java.util.Optional;

public interface CountryService {

    Country createCountry(CountryDTO countryDTO);

    Country deleteCountry(String id);

    Optional<Country> findCountryById(String id);

    Country updateCountry(CountryDTO countryDTO);

    List<Country> getAllCountries();
}
