package zw.co.esolutions.zpas.services.impl.payments;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.payments.MakePaymentDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.merchant.MerchantService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.iface.tax.TaxAuthorityService;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.services.impl.payments.sequence.EndToEndSequenceProcessor;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.MakePaymentRequestType;
import zw.co.esolutions.zpas.utilities.enums.SpecialPayee;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alfred on 12 Mar 2019
 */
@Slf4j
@Component
@Transactional
public class IndividualPaymentProcessor {

    @Value("${ZIMRA.PAYMENTS.ACCOUNT}")
    private String zimraPaymentsAccount;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private CustomPartyRepository customPartyRepository;

    @Autowired
    private PaymentPartyRoleRepository paymentPartyRoleRepository;

    @Autowired
    private EndToEndSequenceProcessor endToEndSequenceProcessor;

    @Autowired
    private IndividualPaymentRepository individualPaymentRepository;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private TaxAuthorityService taxAuthorityService;

    @Autowired
    private PaymentIdentificationRepository paymentIdentificationRepository;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private CreditInstrumentRepository creditInstrumentRepository;

    @Autowired
    Environment environment;

    public Optional<IndividualPayment> createIndividualPayment(MakePaymentDTO makePaymentDTO) throws PaymentRequestInvalidArgumentException {
        validatePreconditions(makePaymentDTO);
        final IndividualPayment individualPayment = createIndividualPaymentHelper(makePaymentDTO);
        final List<PaymentPartyRole> paymentPartyRoles = individualPayment.getPartyRole();
        final List<PaymentStatus> paymentStatuses = individualPayment.getPaymentStatus();
        final List<PaymentIdentification> paymentIdentifications = individualPayment.getPaymentRelatedIdentifications();
        final List<CreditInstrument> creditInstruments = individualPayment.getCreditMethod();
        individualPayment.setPartyRole(new ArrayList<>());
        individualPayment.setPaymentStatus(new ArrayList<>());
        individualPayment.setPaymentRelatedIdentifications(new ArrayList<>());

        creditInstruments.forEach(creditInstrument -> {
            creditInstrument.setRelatedPayment(new ArrayList<>());
        });
        creditInstrumentRepository.saveAll(creditInstruments);

        final IndividualPayment savedIndividualPayment = individualPaymentRepository.save(individualPayment);
        paymentPartyRoleRepository.saveAll(paymentPartyRoles);
        paymentStatusRepository.saveAll(paymentStatuses);

        paymentIdentifications.forEach(paymentIdentification -> {
            paymentIdentification.setPayment(savedIndividualPayment);
        });
        paymentIdentificationRepository.saveAll(paymentIdentifications);

        setPaymentDetails(individualPayment, paymentIdentifications, paymentPartyRoles);

        return Optional.ofNullable(savedIndividualPayment);
    }

    private void setPaymentDetails(IndividualPayment individualPayment, List<PaymentIdentification> paymentIdentifications,
                                   List<PaymentPartyRole> paymentPartyRoles) {
        log.info("Setting payment details on individual payment.");
        setPaymentDetailsHelper(individualPayment, paymentIdentifications, paymentPartyRoles);
    }

    private void setPaymentDetailsHelper(Payment payment, List<PaymentIdentification> paymentIdentifications, List<PaymentPartyRole> paymentPartyRoles) {
        final Optional<FinancialInstitution> debtorBankOptional = getDebtorBank(paymentPartyRoles);
        final Optional<FinancialInstitution> creditorBankOptional = getCreditorBank(paymentPartyRoles);

        final PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setId(payment.getId());
        paymentDetails.setDebtorAgentBIC(debtorBankOptional.map(FinancialInstitution::getBIC).orElse(""));
        paymentDetails.setDebtorAgentName(debtorBankOptional.map(FinancialInstitution::getName).orElse(""));
        paymentDetails.setCreditorAgentBIC(creditorBankOptional.map(FinancialInstitution::getBIC).orElse(""));
        paymentDetails.setCreditorAgentName(creditorBankOptional.map(FinancialInstitution::getName).orElse(""));
        paymentDetails.setCreditorAccountNumber(getBeneficiaryAccount(paymentPartyRoles));
        paymentDetails.setDebtorName(getDebtor(paymentPartyRoles).map(Party::getName).orElse(""));
        paymentDetails.setCreditorName(getBeneficiaryName(paymentPartyRoles));
        paymentDetails.setDebtorAccountNumber(payment.getDebtorAccount());
        paymentDetails.setDebtorAccountName(payment.getAccount().getIdentification().getName());
        paymentDetails.setDebtorAccountCurrency(payment.getAccount().getBaseCurrency().getCodeName());
        paymentDetails.setEndToEndId(paymentIdentifications.get(0).getEndToEndIdentification());
        paymentDetails.setValueDate(payment.getValueDate());
        paymentDetails.setPaymentInstrument(payment.getPaymentInstrument());
        paymentDetails.setType(payment.getType());
        paymentDetails.setPayment(payment);
        paymentDetails.setDateCreated(OffsetDateTime.now());
        paymentDetails.setLastUpdated(OffsetDateTime.now());

        paymentDetails.setAmount(payment.getAmount());
        paymentDetails.setPurpose(payment.getPurpose());
        paymentDetails.setPriority(payment.getPriority());
        paymentDetails.setInstructedAmount(payment.getInstructedAmount());
        paymentDetails.setInstructionForDebtorAgent(payment.getInstructionForDebtorAgent());
        paymentDetails.setInstructionForCreditorAgent(payment.getInstructionForCreditorAgent());

        getDebtor(paymentPartyRoles).ifPresent(party -> {
            if (party instanceof Organisation) {
                paymentDetails.setOrganisationId(getDebtor(paymentPartyRoles).map(Party::getOrganisationIdentificationHelper).orElse(""));
            }
            if (party instanceof Person) {
                paymentDetails.setPrivateId(getDebtor(paymentPartyRoles).map(Party::getPersonIdentificationHelper).orElse(""));
            }
        });

        payment.setPaymentDetails(paymentDetails);
    }

    public String getBeneficiaryName(List<PaymentPartyRole> paymentPartyRoles) {
        return paymentPartyRoles.stream().filter(r -> r instanceof CreditorRole)
                .map(r -> (CreditorRole) r).findFirst().map(creditorRole -> {
                    if (creditorRole.isHasCustomParty()) {
                        return creditorRole.getCustomParty().getName();
                    } else {
                        return creditorRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                            return ((Party) rolePlayer).getName();
                        }).orElse("");
                    }
                }).orElse("");
    }

    public Optional<Party> getDebtor(List<PaymentPartyRole> paymentPartyRoles) {
        return paymentPartyRoles.stream().filter(paymentPartyRole -> paymentPartyRole instanceof DebtorRole)
                .findFirst().map(debtorRole -> {
                    return debtorRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        Party party = (Party) rolePlayer;
                        return Optional.ofNullable(party);
                    }).orElse(Optional.empty());
                }).orElse(Optional.empty());
    }

    public Optional<FinancialInstitution> getDebtorBank(List<PaymentPartyRole> paymentPartyRoles) {
        return paymentPartyRoles.stream().filter(paymentPartyRole -> paymentPartyRole instanceof DebtorAgentRole)
                .findFirst().map(debtorAgentRole -> {
                    return debtorAgentRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        if (rolePlayer instanceof FinancialInstitution) {
                            FinancialInstitution financialInstitution = (FinancialInstitution) rolePlayer;
                            return Optional.ofNullable(financialInstitution);
                        } else {
                            Optional<FinancialInstitution> result = Optional.empty();
                            return result;
                        }
                    }).orElse(Optional.empty());
                }).orElse(Optional.empty());
    }

    public Optional<FinancialInstitution> getCreditorBank(List<PaymentPartyRole> paymentPartyRoles) {
        return paymentPartyRoles.stream().filter(paymentPartyRole -> paymentPartyRole instanceof CreditorAgentRole)
                .findFirst().map(creditorAgentRole -> {
                    return creditorAgentRole.getPlayer().stream().findFirst().map(rolePlayer -> {
                        if (rolePlayer instanceof FinancialInstitution) {
                            FinancialInstitution financialInstitution = (FinancialInstitution) rolePlayer;
                            return Optional.ofNullable(financialInstitution);
                        } else {
                            Optional<FinancialInstitution> result = Optional.empty();
                            return result;
                        }
                    }).orElse(Optional.empty());
                }).orElse(Optional.empty());
    }

    public String getBeneficiaryAccount(List<PaymentPartyRole> paymentPartyRoles) {
        return paymentPartyRoles.stream().filter(r -> r instanceof CreditorRole)
                .map(r -> (CreditorRole) r).findFirst().map(creditorRole -> {
                    if (creditorRole.getCustomParty() != null) {
                        return creditorRole.getCustomParty().getAccountNumber();
                    } else {
                        return "";
                    }
                }).orElse("");
    }

    private void validatePreconditions(MakePaymentDTO makePaymentDTO) throws PaymentRequestInvalidArgumentException {
        try {
            Preconditions.checkNotNull(makePaymentDTO.getBeneficiaryReference(), "Please provide a beneficiary reference.");
            Preconditions.checkNotNull(makePaymentDTO.getAmount(), "Please provide a valid amount.");
            Preconditions.checkNotNull(makePaymentDTO.getValueDate(), "Please provide a value date.");
            Preconditions.checkNotNull(makePaymentDTO.getBeneficiaryName(), "Please provide a beneficiary name.");
            Preconditions.checkNotNull(makePaymentDTO.getDestinationAccountNumber(), "Please provide a valid destination account.");
            Preconditions.checkNotNull(makePaymentDTO.getDestinationFinancialInstitutionId(), "Please select a valid destination bank.");

            if (StringUtils.isEmpty(makePaymentDTO.getSourceAccountId())) {
                final String errorMessage = "Please select source account.";
                log.info(errorMessage);
                throw new PaymentRequestInvalidArgumentException(errorMessage);
            }
            if (makePaymentDTO.getAmount() <= 0) {
                final String errorMessage = "Amount should be bigger than zero, seriously!";
                log.info(errorMessage);
                throw new PaymentRequestInvalidArgumentException(errorMessage);
            }

            MakePaymentRequestType makePaymentRequestType = MakePaymentRequestType.valueOf(makePaymentDTO.getMakePaymentRequestType());
            switch (makePaymentRequestType) {
                case INDIVIDUAL:
                    if (StringUtils.isEmpty(makePaymentDTO.getDestinationAccountNumber())) {
                        final String errorMessage = "Please provide a destination account number.";
                        log.info(errorMessage);
                        throw new PaymentRequestInvalidArgumentException(errorMessage);
                    }
                    if (StringUtils.isEmpty(makePaymentDTO.getDestinationFinancialInstitutionId())) {
                        final String errorMessage = "Please select destination bank.";
                        log.info(errorMessage);
                        throw new PaymentRequestInvalidArgumentException(errorMessage);
                    }
                    break;
                case TAX_PAYMENT:
                    if (StringUtils.isEmpty(makePaymentDTO.getTaxAuthorityId())) {
                        final String errorMessage = "Please select a tax authority.";
                        log.info(errorMessage);
                        throw new PaymentRequestInvalidArgumentException(errorMessage);
                    }

                    if (StringUtils.isEmpty(makePaymentDTO.getTaxAuthoritySubOfficeCode())) {
                        final String errorMessage = "Please select a tax authority sub office.";
                        log.info(errorMessage);
                        throw new PaymentRequestInvalidArgumentException(errorMessage);
                    }

                    if (StringUtils.isEmpty(makePaymentDTO.getTaxIdentificationNumber())) {
                        final String errorMessage = "Please provide a tax identification number (BPN).";
                        log.info(errorMessage);
                        throw new PaymentRequestInvalidArgumentException(errorMessage);
                    }

                    if (StringUtils.isEmpty(makePaymentDTO.getTaxTypeCode())) {
                        final String errorMessage = "Please select a tax type.";
                        log.info(errorMessage);
                        throw new PaymentRequestInvalidArgumentException(errorMessage);
                    }
                    break;
                case MERCHANT_PAYMENT:
                    if (StringUtils.isEmpty(makePaymentDTO.getMerchantId())) {
                        final String errorMessage = "Please select a merchant.";
                        log.info(errorMessage);
                        throw new PaymentRequestInvalidArgumentException(errorMessage);
                    }

                    if (StringUtils.isEmpty(makePaymentDTO.getMerchantAccountNumber())) {
                        final String errorMessage = "Please provide a valid merchant number.";
                        log.info(errorMessage);
                        throw new PaymentRequestInvalidArgumentException(errorMessage);
                    }
                    break;
            }
        } catch (RuntimeException re) {
            final String errorMessage = "Missing field. " + re.getMessage();
            log.info(errorMessage);
            throw new PaymentRequestInvalidArgumentException(errorMessage);
        }
    }

    private IndividualPayment createIndividualPaymentHelper(MakePaymentDTO makePaymentDTO) throws PaymentRequestInvalidArgumentException {
        PaymentTypeCode paymentTypeCode = null;
        OffsetDateTime valueDate = null;

        try {
            paymentTypeCode = PaymentTypeCode.valueOf(makePaymentDTO.getPaymentType());
            log.info("Value date is: {}", makePaymentDTO.getValueDate());
            final LocalDate localDate = LocalDate.parse(makePaymentDTO.getValueDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            valueDate = LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.of("+02:00"));
        } catch (RuntimeException re) {
            re.printStackTrace();
        }
        final CashAccount sourceAccount = accountService.findAccountById(makePaymentDTO.getSourceAccountId());
        if (sourceAccount == null) {
            throw new PaymentRequestInvalidArgumentException("Source account not found.");
        }

        IndividualPayment individualPayment = new CreditTransfer();
        individualPayment.setPaymentObligation(new ArrayList<>());
        individualPayment.setCurrencyOfTransfer(sourceAccount.getBaseCurrency());
        individualPayment.setCreditMethod(new ArrayList<>());
        individualPayment.setType(paymentTypeCode);
        try {
            individualPayment.setSpecialPayee(SpecialPayee.valueOf(makePaymentDTO.getSpecialPayee()));
        } catch (RuntimeException re) {
            individualPayment.setSpecialPayee(SpecialPayee.NONE);
        }
        individualPayment.setPurpose(ProprietaryPurposeCode.valueOfCodeName(makePaymentDTO.getPurpose()));

        log.info("Configuring payment information.");
        configurePaymentInformationForRequestType(individualPayment, makePaymentDTO);

        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        instructedCurrencyAndAmount.setAmount(new BigDecimal(makePaymentDTO.getAmount()));
        instructedCurrencyAndAmount.setCurrency(sourceAccount.getBaseCurrency());
        individualPayment.setInstructedAmount(instructedCurrencyAndAmount);
        individualPayment.setPriority(PriorityCode.valueOf(makePaymentDTO.getPriority()));
        individualPayment.setValueDate(Optional.ofNullable(valueDate).orElse(OffsetDateTime.now()));
        final PaymentStatus paymentStatus = new PaymentStatus();
        paymentStatus.setStatus(PaymentStatusCode.Pending);
        paymentStatus.setPayment(individualPayment);
        paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
        paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
        paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
        paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.None);
        paymentStatus.setTransactionRejectionReason(TransactionReasonCode.None);
        paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
        paymentStatus.setTransactionStatus(TransactionStatusCode.None);
        paymentStatus.setCashPaymentStatus(CashPaymentStatusCode.None);
        paymentStatus.setCancellationReason(CancellationReasonCode.None);
        paymentStatus.setMandateRejectionReason(MandateReasonCode.None);
        paymentStatus.setPendingFailingSettlement(PendingFailingSettlementCode.None);
        paymentStatus.setId(GenerateKey.generateEntityId());
        paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
        paymentStatus.setDateCreated(OffsetDateTime.now());
        paymentStatus.setLastUpdated(OffsetDateTime.now());
        paymentStatus.setStatusDateTime(OffsetDateTime.now());
        paymentStatus.setStatusDescription("Initial Status");
        paymentStatus.setSettlementStatus(SecuritiesSettlementStatusCode.None);
        paymentStatus.setCancellationProcessingStatus(CancellationProcessingStatusCode.None);
        paymentStatus.setTransactionProcessingStatus(InstructionProcessingStatusCode.None);
        paymentStatus.setModificationProcessingStatus(ModificationProcessingStatusCode.None);

        final List<PaymentStatus> paymentStatuses = Arrays.asList(paymentStatus);

        individualPayment.setPaymentStatus(paymentStatuses);
        individualPayment.setPoolingAdjustmentDate(OffsetDateTime.now());
        individualPayment.setEquivalentAmount(instructedCurrencyAndAmount.getAmount());
        individualPayment.setInstructionForCreditorAgent(InstructionCode.HoldCashForCreditor);
        individualPayment.setInstructionForDebtorAgent(InstructionCode.PayTheBeneficiary);
        individualPayment.setAmount(instructedCurrencyAndAmount);
        individualPayment.setPaymentInstrument(PaymentInstrumentCode.CustomerCreditTransfer);
        individualPayment.setAccount(sourceAccount);
        individualPayment.setId(UUID.randomUUID().toString());
        individualPayment.setDateCreated(OffsetDateTime.now());
        individualPayment.setLastUpdated(OffsetDateTime.now());
        individualPayment.setEntityStatus(EntityStatus.DRAFT);
        individualPayment.setVerifyAccountInfo(true);
        addIndividualPaymentPartyRoles(individualPayment, makePaymentDTO);

        setPaymentIdentifications(makePaymentDTO, individualPayment);
        setCreditMethods(individualPayment);

        return individualPayment;
    }

    private void setCreditMethods(IndividualPayment individualPayment) {
        final List<Payment> payments = new ArrayList<>();
        payments.add(individualPayment);
        final List<CreditInstrument> creditInstruments = new ArrayList<>();
        final CreditInstrument creditInstrument = new CreditInstrument();
        creditInstrument.setId(GenerateKey.generateEntityId());
        creditInstrument.setEntityStatus(EntityStatus.ACTIVE);
        creditInstrument.setDateCreated(OffsetDateTime.now());
        creditInstrument.setLastUpdated(OffsetDateTime.now());
        creditInstrument.setRelatedPayment(payments);
        creditInstrument.setMethod(PaymentMethodCode.CreditTransfer);
        creditInstrument.setCreditInstrumentIdentification(PaymentInstrumentCode.CustomerCreditTransfer.name());
        creditInstrument.setNetAmount(individualPayment.getAmount());
        creditInstrument.setDeadline(individualPayment.getValueDate());

        creditInstruments.add(creditInstrument);
        individualPayment.setCreditMethod(creditInstruments);
    }

    private void setPaymentIdentifications(MakePaymentDTO makePaymentDTO, IndividualPayment individualPayment) {
        final List<PaymentIdentification> paymentIdentifications = new ArrayList<>();
        final PaymentIdentification paymentIdentification = new PaymentIdentification();

        Optional<FinancialInstitution> debtorBankOptional = individualPayment.getDebtorBank();

        String userCode = debtorBankOptional.map(financialInstitution -> environment.getProperty("ACH.USERCODE." + financialInstitution.getBIC()))
                .orElse("0000");

        final List<String> endToEndIds = endToEndSequenceProcessor.generateEndToEndIds("0001", userCode, OffsetDateTime.now(), 1);
//        final String paymentUniqueId = RefGen.getMessageReference("ZEEPAY", "AQS");//UUID.randomUUID().toString();
        final String paymentUniqueId = endToEndIds.get(0);

        paymentIdentification.setExecutionIdentification(paymentUniqueId);
        paymentIdentification.setEndToEndIdentification(paymentUniqueId);
        paymentIdentification.setInstructionIdentification(paymentUniqueId);
        paymentIdentification.setTransactionIdentification(paymentUniqueId);
        paymentIdentification.setClearingSystemReference(paymentUniqueId);
        paymentIdentification.setCreditorReference(makePaymentDTO.getBeneficiaryReference());
        paymentIdentification.setPayment(individualPayment);
        paymentIdentification.setId(GenerateKey.generateEntityId());
        paymentIdentification.setDateCreated(OffsetDateTime.now());
        paymentIdentification.setLastUpdated(OffsetDateTime.now());
        paymentIdentification.setEntityStatus(EntityStatus.ACTIVE);
        paymentIdentification.setCounterpartyReference(makePaymentDTO.getClientReference());
        paymentIdentification.setIdentification(paymentUniqueId);
        paymentIdentification.setCommonIdentification(paymentUniqueId);
        paymentIdentification.setMatchingReference(makePaymentDTO.getPurpose());
        paymentIdentification.setUniqueTradeIdentifier(paymentUniqueId);

        log.info("Payment Identification: {}", paymentIdentification);

        paymentIdentifications.add(paymentIdentification);
        individualPayment.setPaymentRelatedIdentifications(paymentIdentifications);
    }

    private void configurePaymentInformationForRequestType(IndividualPayment individualPayment, MakePaymentDTO makePaymentDTO) {
        individualPayment.setContributionScheduleId(makePaymentDTO.getContributionScheduleId());
        final Optional<MakePaymentRequestType> paymentRequestTypeOptional = getPaymentRequestType(makePaymentDTO.getMakePaymentRequestType());
        paymentRequestTypeOptional.ifPresent(makePaymentRequestType -> {
            switch (makePaymentRequestType) {
                case COMPANY: {
                    log.info("Handling Company Credit Transfer payment information.");
                    individualPayment.setType(PaymentTypeCode.Other);
                    List<String> unstructuredRemittanceInformation = new ArrayList<>();
                    unstructuredRemittanceInformation.add("businessPartnerNumber:" + makePaymentDTO.getTaxIdentificationNumber());
                    individualPayment.setUnstructuredRemittanceInformation(unstructuredRemittanceInformation);
                    break;
                }
                case TAX_PAYMENT: {
                    log.info("Handling Tax Payment payment information.");
                    final Optional<TaxAuthority> taxAuthorityOptional = taxAuthorityService.getTaxAuthority(makePaymentDTO.getTaxAuthorityId());
                    individualPayment.setType(PaymentTypeCode.TaxPayment);
                    taxAuthorityOptional.ifPresent(taxAuthority -> {
                        List<String> unstructuredRemittanceInformation = new ArrayList<>();
                        unstructuredRemittanceInformation.add("businessPartnerNumber:" + makePaymentDTO.getTaxIdentificationNumber());
                        unstructuredRemittanceInformation.add("clientName:" + makePaymentDTO.getClientName());
                        unstructuredRemittanceInformation.add("clientReference:" + makePaymentDTO.getPurpose());
                        unstructuredRemittanceInformation.add("taxTypeCode:" + makePaymentDTO.getTaxTypeCode());
                        unstructuredRemittanceInformation.add("taxAuthorityPaymentOfficeCode:" + makePaymentDTO.getTaxAuthoritySubOfficeCode());
                        individualPayment.setUnstructuredRemittanceInformation(unstructuredRemittanceInformation);
                    });
                    individualPayment.setSpecialPayee(SpecialPayee.ZIMRA);
                    break;
                }
                case INDIVIDUAL: {
                    log.info("Handling Individual Credit Transfer payment information.");
                    individualPayment.setType(PaymentTypeCode.Other);
                    break;
                }
                case ZIMRA_PAYMENT: {
                    log.info("Handling Zimra Credit Transfer payment information.");
                    individualPayment.setType(PaymentTypeCode.ValueAddedTaxPayment);
                    List<String> unstructuredRemittanceInformation = new ArrayList<>();
                    unstructuredRemittanceInformation.add("businessPartnerNumber:" + makePaymentDTO.getBusinessPartnerNumber());
                    unstructuredRemittanceInformation.add("zimraAssessmentNumber:" + makePaymentDTO.getZimraAssessmentNumber());
                    unstructuredRemittanceInformation.add("officeCode:" + makePaymentDTO.getZimraOfficeCode());
                    individualPayment.setUnstructuredRemittanceInformation(unstructuredRemittanceInformation);
                    individualPayment.setSpecialPayee(SpecialPayee.ZIMRA);
                    break;
                }
                case MERCHANT_PAYMENT: {
                    log.info("Handling Merchant Payment payment information.");
                    individualPayment.setType(PaymentTypeCode.CommercialCredit);
                    final Optional<Merchant> merchantOptional = merchantService.getMerchant(makePaymentDTO.getMerchantId());
                    log.info("Fetched merchant.");
                    merchantOptional.ifPresent(merchant -> {
                        List<String> unstructuredRemittanceInformation = new ArrayList<>();
                        unstructuredRemittanceInformation.add("merchantAccountNumber:" + merchant.getAccountNumber());
                        unstructuredRemittanceInformation.add("merchantBankId:" + merchant.getBank());
                        unstructuredRemittanceInformation.add("merchantCode:" + merchant.getCode());
                        unstructuredRemittanceInformation.add("merchantId:" + merchant.getId());
                        unstructuredRemittanceInformation.add("merchantName:" + merchant.getName());
                        unstructuredRemittanceInformation.add("merchantAccountNumber:" + makePaymentDTO.getMerchantAccountNumber());
                        individualPayment.setUnstructuredRemittanceInformation(unstructuredRemittanceInformation);
                    });
                    break;
                }
                default:
                    throw new IllegalArgumentException("Unknown Make Payment Request Type: " + makePaymentDTO.getMakePaymentRequestType());
            }
        });
    }

    private static Optional<MakePaymentRequestType> getPaymentRequestType(String paymentRequest) {
        try {
            return Optional.ofNullable(MakePaymentRequestType.valueOf(paymentRequest));
        } catch (RuntimeException re) {
            return Optional.empty();
        }
    }

    private void addIndividualPaymentPartyRoles(final IndividualPayment individualPayment, MakePaymentDTO makePaymentDTO)
            throws PaymentRequestInvalidArgumentException {

        /**
         * Configure Individual Payment Roles
         */
        //The Payment
        final List<Payment> individualPayments = Arrays.asList(individualPayment);
        //The affected Bulk Payment accounts
        final List<CashAccount> cashAccounts = Arrays.asList(individualPayment.getAccount());

        final List<AccountPartyRole> accountPartyRoles = individualPayment.getAccount().getPartyRole();

        //account servicing roles
        final List<AccountPartyRole> accountServicerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountServicerRole);
        }).collect(Collectors.toList());

        //The Role Players servicing this payment
        final List<RolePlayer> accountServicerRolePlayers = accountServicerRoles.stream()
                .flatMap(accountServicerRole -> accountServicerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //account owning roles
        final List<AccountPartyRole> accountOwnerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountOwnerRole);
        }).collect(Collectors.toList());

        //The Role Players owning this payment
        final List<RolePlayer> accountOwnerRolePlayers = accountOwnerRoles.stream()
                .flatMap(accountOwnerRole -> accountOwnerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //Debtor Role
        DebtorRole debtorRole = new DebtorRole();
        debtorRole.setPayment(individualPayments);
        debtorRole.setCashAccount(cashAccounts);
        debtorRole.setId(GenerateKey.generateEntityId());
        debtorRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorRole.setDateCreated(OffsetDateTime.now());
        debtorRole.setLastUpdated(OffsetDateTime.now());
        debtorRole.setPlayer(accountOwnerRolePlayers);
        debtorRole.setEntry(null);
        debtorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        //Debtor Agent Role
        DebtorAgentRole debtorAgentRole = new DebtorAgentRole();
        debtorAgentRole.setPayment(individualPayments);
        debtorAgentRole.setCashAccount(cashAccounts);
        debtorAgentRole.setId(GenerateKey.generateEntityId());
        debtorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorAgentRole.setDateCreated(OffsetDateTime.now());
        debtorAgentRole.setLastUpdated(OffsetDateTime.now());
        debtorAgentRole.setPlayer(accountServicerRolePlayers);
        debtorAgentRole.setEntry(null);
        debtorAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Initiating Party Role
        InitiatingPartyRole initiatingPartyRole = new InitiatingPartyRole();
        initiatingPartyRole.setPayment(individualPayments);
        initiatingPartyRole.setCashAccount(cashAccounts);
        initiatingPartyRole.setId(GenerateKey.generateEntityId());
        initiatingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
        initiatingPartyRole.setDateCreated(OffsetDateTime.now());
        initiatingPartyRole.setLastUpdated(OffsetDateTime.now());
        initiatingPartyRole.setPlayer(accountOwnerRolePlayers);
        initiatingPartyRole.setEntry(null);
        initiatingPartyRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Forwarding Party Role
        ForwardingAgentRole forwardingAgentRole = new ForwardingAgentRole();
        forwardingAgentRole.setPayment(individualPayments);
        forwardingAgentRole.setCashAccount(cashAccounts);
        forwardingAgentRole.setId(GenerateKey.generateEntityId());
        forwardingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        forwardingAgentRole.setDateCreated(OffsetDateTime.now());
        forwardingAgentRole.setLastUpdated(OffsetDateTime.now());
        forwardingAgentRole.setPlayer(accountServicerRolePlayers);
        forwardingAgentRole.setEntry(null);
        forwardingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Instructing Agent Role
        InstructingAgentRole instructingAgentRole = new InstructingAgentRole();
        instructingAgentRole.setPayment(individualPayments);
        instructingAgentRole.setCashAccount(cashAccounts);
        instructingAgentRole.setId(GenerateKey.generateEntityId());
        instructingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        instructingAgentRole.setDateCreated(OffsetDateTime.now());
        instructingAgentRole.setLastUpdated(OffsetDateTime.now());
        instructingAgentRole.setPlayer(accountServicerRolePlayers);
        instructingAgentRole.setEntry(null);
        instructingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        List<PaymentPartyRole> individualPaymentPartyRoles = new ArrayList<>();
        individualPaymentPartyRoles.add(debtorRole);
        individualPaymentPartyRoles.add(debtorAgentRole);
        individualPaymentPartyRoles.add(initiatingPartyRole);
        individualPaymentPartyRoles.add(forwardingAgentRole);
        individualPaymentPartyRoles.add(instructingAgentRole);

        /**
         * Configure Payment Parties for individual payments in the bulk
         */
        switch (individualPayment.getType()) {
            case TaxPayment: {
                final Optional<TaxAuthority> taxAuthorityOptional = taxAuthorityService.getTaxAuthority(makePaymentDTO.getTaxAuthorityId());
                if (!taxAuthorityOptional.isPresent()) {
                    throw new PaymentRequestInvalidArgumentException("Tax authority not found.");
                }
                taxAuthorityOptional.ifPresent(taxAuthority -> {
                    final Optional<CashAccount> cashAccountOptional = accountService.findAccountByIban(taxAuthority.getAccountNumber());
                    configureCreditorRoles(individualPaymentPartyRoles, individualPayments, makePaymentDTO, cashAccountOptional, taxAuthority, null);
                });
                break;
            }
            case ValueAddedTaxPayment: {
                final Optional<CashAccount> cashAccountOptional = accountService.findAccountByIban(zimraPaymentsAccount);
                cashAccountOptional.ifPresent(cashAccount -> {
                    makePaymentDTO.setDestinationAccountNumber(cashAccount.getAccountNumber());
                    makePaymentDTO.setBeneficiaryName(cashAccount.getAccountName());
                });
                configureCreditorRoles(individualPaymentPartyRoles, individualPayments, makePaymentDTO, cashAccountOptional, null, null);
                break;
            }
            case CommercialCredit: {
                final Optional<Merchant> merchantOptional = merchantService.getMerchant(makePaymentDTO.getMerchantId());
                if (!merchantOptional.isPresent()) {
                    throw new PaymentRequestInvalidArgumentException("Merchant not found.");
                }
                merchantOptional.ifPresent(merchant -> {
                    final Optional<CashAccount> cashAccountOptional = accountService.findAccountByNumber(merchant.getAccountNumber());
                    configureCreditorRoles(individualPaymentPartyRoles, individualPayments, makePaymentDTO, cashAccountOptional, null, merchant);
                });
                break;
            }
            case Other: {
                final Optional<CashAccount> cashAccountOptional = accountService.findAccountByNumber(makePaymentDTO.getDestinationAccountNumber());

                if (cashAccountOptional.isPresent()) {
                    CashAccount cashAccount = cashAccountOptional.get();
                    if (cashAccount.getId().equals(makePaymentDTO.getSourceAccountId())) {
                        throw new PaymentRequestInvalidArgumentException("Source account and destination account are the same.");
                    }
                }
                configureCreditorRoles(individualPaymentPartyRoles, individualPayments, makePaymentDTO, cashAccountOptional, null, null);
                break;
            }
            default:
        }

        individualPayment.setPartyRole(individualPaymentPartyRoles);
    }

    private void configureCreditorRoles(List<PaymentPartyRole> individualPaymentPartyRoles, List<Payment> individualPayments,
                                        MakePaymentDTO makePaymentDTO, Optional<CashAccount> destinationAccountOptional,
                                        TaxAuthority taxAuthority, Merchant merchant) {
        //Creditor Agent Role
        CreditorAgentRole creditorAgentRole = new CreditorAgentRole();
        creditorAgentRole.setPayment(individualPayments);
        destinationAccountOptional.ifPresent(cashAccount -> {
            if (cashAccount.getFinancialInstitution() != null) {
                creditorAgentRole.setPlayer(Arrays.asList(cashAccount.getFinancialInstitution()));
            } else {
                final Optional<FinancialInstitution> optionalFinancialInstitution =
                        financialInstitutionService.getFinancialInstitutionById(makePaymentDTO.getDestinationFinancialInstitutionId());
                optionalFinancialInstitution.ifPresent(financialInstitution -> {
                    creditorAgentRole.setPlayer(Arrays.asList(financialInstitution));
                });
            }

        });
        if (!destinationAccountOptional.isPresent()) {
            final Optional<FinancialInstitution> optionalFinancialInstitution =
                    financialInstitutionService.getFinancialInstitutionById(makePaymentDTO.getDestinationFinancialInstitutionId());
            optionalFinancialInstitution.ifPresent(financialInstitution -> {
                creditorAgentRole.setPlayer(Arrays.asList(financialInstitution));
            });
        }
        creditorAgentRole.setId(GenerateKey.generateEntityId());
        creditorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorAgentRole.setDateCreated(OffsetDateTime.now());
        creditorAgentRole.setLastUpdated(OffsetDateTime.now());
        creditorAgentRole.setEntry(null);
        creditorAgentRole.setPartyRoleCode(PartyRoleCode.FinalAgent);

        CustomParty customParty = new CustomParty();
        customParty.setId(GenerateKey.generateEntityId());
        customParty.setDateCreated(OffsetDateTime.now());
        customParty.setLastUpdated(OffsetDateTime.now());
        customParty.setEntityStatus(EntityStatus.ACTIVE);
        customParty.setName(makePaymentDTO.getBeneficiaryName());
        customParty.setNationalId(makePaymentDTO.getNationalId());
        customParty.setPassportNumber(makePaymentDTO.getPassportNumber());
        customParty.setAccountNumber(makePaymentDTO.getDestinationAccountNumber());
        customParty.setFinancialInstitutionIdentification(destinationAccountOptional.map(acc -> {
            if (acc.getFinancialInstitution() != null) {
                return acc.getFinancialInstitution().getId();
            } else {
                return "";
            }
        }).orElse(""));

        if (taxAuthority != null) {
            customParty.setAccountNumber(taxAuthority.getAccountNumber());
            customParty.setName(taxAuthority.getName());
        }

        if (merchant != null) {
            customParty.setAccountNumber(merchant.getAccountNumber());
            customParty.setName(merchant.getName());
        }

        final CustomParty savedCustomParty = customPartyRepository.save(customParty);

        //Scheme
//        final Scheme scheme = new Scheme();
//        scheme.setId(GenerateKey.generateEntityId());
//        scheme.setEntityStatus(EntityStatus.ACTIVE);
//        scheme.setDateCreated(OffsetDateTime.now());
//        scheme.setLastUpdated(OffsetDateTime.now());
//        scheme.setNameShort("Default");
//        scheme.setCode("");
//        scheme.setIdentification(new ArrayList<>());
//        scheme.setVersion("0");
//        scheme.setNameLong("Default Scheme");
//        scheme.setDescription("This is the default scheme for the creditor role.");
//        scheme.setDomainValueCode("Default");
//        scheme.setDomainValueName("Default");
//        schemeRepository.save(scheme);

        //Creditor Role
        CreditorRole creditorRole = new CreditorRole();
//        scheme.setCreditorRole(creditorRole);
//        creditorRole.setScheme(scheme);
        creditorRole.setPayment(individualPayments);
        creditorRole.setCustomParty(savedCustomParty);
        creditorRole.setHasCustomParty(true);
        creditorRole.setId(GenerateKey.generateEntityId());
        creditorRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorRole.setDateCreated(OffsetDateTime.now());
        creditorRole.setLastUpdated(OffsetDateTime.now());
        creditorRole.setEntry(null);
        creditorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        individualPaymentPartyRoles.add(creditorAgentRole);
        individualPaymentPartyRoles.add(creditorRole);
    }
}
