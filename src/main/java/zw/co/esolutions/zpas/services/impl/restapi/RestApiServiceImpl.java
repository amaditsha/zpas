package zw.co.esolutions.zpas.services.impl.restapi;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.account.AccountSearchResult;
import zw.co.esolutions.zpas.dto.currency.CurrencyCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchResponseDTO;
import zw.co.esolutions.zpas.dto.party.VirtualCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.party.VirtualIdAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.party.VirtualSuffixAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.profile.UsernameCheckDTO;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PersonNameRepository;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.currency.CurrencyService;
import zw.co.esolutions.zpas.services.iface.party.PartyService;
import zw.co.esolutions.zpas.services.iface.restapi.RestApiService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.PersonType;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alfred on 20 Mar 2019
 */
@Slf4j
@Service
public class RestApiServiceImpl implements RestApiService {
    @Autowired
    private AccountsService accountService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private PersonNameRepository personNameRepository;

    @Autowired
    private CurrencyService currencyService;

    @Override
    public List<PersonSearchResponseDTO> searchAllPersons(PersonSearchDTO personSearchDTO) {
        final String search = personSearchDTO.getSearch();
        final List<PersonName> personNames = personNameRepository.searchPersonName(search.toLowerCase());
        log.info("Found person names are:\n==================================\n{}\n=================================\n", personNames);

        return personNames.stream().map(personName -> {
            final PersonSearchResponseDTO.PersonSearchResponseDTOBuilder builder = PersonSearchResponseDTO.builder();
            Optional.ofNullable(personName.getIdentification()).ifPresent(personIdentification -> {
                Optional.ofNullable(personIdentification.getPerson()).ifPresent(person -> {
                    builder.nationalId(personIdentification.getIdentityCardNumber());
                    builder.personId(person.getId());
                    builder.personFullName(personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName());
                });
            });
            return builder.build();
        }).collect(Collectors.toList());
    }

    @Override
    public List<PersonSearchResponseDTO> searchPersonsForCashAccountMandate(PersonSearchDTO personSearchDTO) {
        final String search = personSearchDTO.getSearch();
        final List<PersonName> personNames = personNameRepository.searchPersonName(search.toLowerCase());
        log.info("Search request is \n{}. \nFound person names are:\n=============\n{}\n===================\n",personSearchDTO,  personNames);

        return personNames.stream().map(personName -> {
            final PersonSearchResponseDTO.PersonSearchResponseDTOBuilder builder = PersonSearchResponseDTO.builder();
            Optional.ofNullable(personName.getIdentification()).ifPresent(personIdentification -> {
                Optional.ofNullable(personIdentification.getPerson()).ifPresent(person -> {

                    if (personSearchDTO.getPartyType() != null && personSearchDTO.getPartyType().equals("Organisation")) {
                        log.info("Party is a organisation. Checking if person {} are employees", person.getId());

                        log.info("The person search role id is {} ", personSearchDTO.getPartyRoleId());

                        boolean isEmployee = false;
                        final EmployingPartyRole employingParty = person.getEmployingParty();
                        if(employingParty != null) {
                            log.info("The employing party is \n{}\n", employingParty);
                            isEmployee = employingParty.getId().equals(personSearchDTO.getPartyRoleId());
                        }

                        if (isEmployee) {
                            log.info("The person is employee");
                            builder.nationalId(personIdentification.getIdentityCardNumber());
                            builder.personId(person.getId());
                            builder.personFullName(personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName());

                        }
                    } else {
                        builder.nationalId(personIdentification.getIdentityCardNumber());
                        builder.personId(person.getId());
                        builder.personFullName(personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName());
                    }
                });
            });
            return builder.build();
        }).collect(Collectors.toList());
    }

    @Override
    public Optional<AccountSearchResult> searchAccountByVirtualId(String virtualId) {
        return accountService.searchAccountByVirtualId(virtualId);
    }

    @Override
    public Optional<AccountSearchResult> getAccountInfo(String accountNumber, String financialInstitutionId) {
        return accountService.findAccountByIban(accountNumber).map(cashAccount -> {
            return Optional.of(AccountSearchResult.builder()
                    .financialInstitutionId(Optional.ofNullable(cashAccount.getFinancialInstitution()).map(FinancialInstitution::getId).orElse(financialInstitutionId))
                    .financialInstitutionName(Optional.ofNullable(cashAccount.getFinancialInstitution()).map(FinancialInstitution::getName).orElse(""))
                    .virtualId(Optional.ofNullable(cashAccount.getAccountOwner()).map(Party::getVirtualId).orElse(""))
                    .accountNumber(cashAccount.getIdentification().getNumber())
                    .accountName(cashAccount.getIdentification().getName())
                    .bpn(Optional.ofNullable(cashAccount.getAccountOwner()).map(Party::getBusinessPartnerNumber).orElse(""))
                    .accountId(cashAccount.getId())
                    .accountOwnerName(Optional.ofNullable(cashAccount.getAccountOwner()).map(Party::getName).orElse(""))
                    .accountOwnerId(Optional.ofNullable(cashAccount.getAccountOwner()).map(Party::getId).orElse(""))
                    .narrative("Account found.")
                    .build());
        }).orElseGet(() -> {
            return Optional.of(AccountSearchResult.builder()
                    .financialInstitutionId(financialInstitutionId)
                    .financialInstitutionId("")
                    .virtualId("")
                    .accountNumber(accountNumber)
                    .accountName("")
                    .bpn("")
                    .accountId("")
                    .accountOwnerName("")
                    .accountOwnerId("")
                    .narrative("Account not found.")
                    .build());
        });
    }

    @Override
    public Optional<PersonSearchResponseDTO> getPersonInfoById(String personId) {
        return clientService.getPersonById(personId).map(person -> {
            final String personName = person.getName();
            final PersonName personNameObject = getPersonNameObject(person);
            PersonSearchResponseDTO personSearchResponseDTO = PersonSearchResponseDTO.builder()
                    .personId(person.getId())
                    .personFullName(personName)
                    .firstNames(personNameObject.getBirthName() + " " + personNameObject.getMiddleName())
                    .lastName(personNameObject.getGivenName())
                    .build();

            person.getContactPoint().stream().filter(cp -> cp instanceof PhoneAddress)
                    .map(contactPoint -> (PhoneAddress) contactPoint)
                    .findFirst().ifPresent(phoneAddress -> {
                personSearchResponseDTO.setMobileNumber(phoneAddress.getMobileNumber());
                personSearchResponseDTO.setPhoneNumber(phoneAddress.getPhoneNumber());
            });

            person.getContactPoint().stream().filter(cp -> cp instanceof ElectronicAddress)
                    .map(contactPoint -> (ElectronicAddress) contactPoint)
                    .findFirst().ifPresent(electronicAddress -> {
                personSearchResponseDTO.setEmail(electronicAddress.getEmailAddress());
            });
            return Optional.ofNullable(personSearchResponseDTO);
        }).orElse(Optional.empty());
    }

    @Override
    public List<PersonSearchResponseDTO> searchPersons(PersonSearchDTO personSearchDTO) {
        final String search = personSearchDTO.getSearch();
        final String organisationId = personSearchDTO.getOrganisationId();
        final String personTypeString = personSearchDTO.getPersonType();
        if (StringUtils.isEmpty(personSearchDTO.getSearch())) {
            return new ArrayList<>();
        }

        final List<PersonName> personNames = personNameRepository.searchPersonName(search.toLowerCase());
        log.info("Found person names are:\n==================================\n{}\n=================================\n", personNames);

        PersonType personType = PersonType.ALL;

        try {
            personType = PersonType.valueOf(personTypeString);
        } catch (RuntimeException re) {

        }

        Set<PersonSearchResponseDTO> personSearchResponseDTOS = new HashSet<>();
        switch (personType) {
            case ALL:
                log.info("Returning {} persons...", personType.name());
                return personNames.stream().map(personName -> {
                    final PersonSearchResponseDTO.PersonSearchResponseDTOBuilder builder = PersonSearchResponseDTO.builder();
                    Optional.ofNullable(personName.getIdentification()).ifPresent(personIdentification -> {
                        Optional.ofNullable(personIdentification.getPerson()).ifPresent(person -> {
                            builder.personId(person.getId());
                            builder.personFullName(personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName());
                        });
                    });
                    builder.organisationId(organisationId);
                    builder.organisationType("");
                    return builder.build();
                }).collect(Collectors.toList());
            case FI_EMPLOYEE:
            case NON_FI_EMPLOYEE:
                log.info("Filtering {} person...", personType.name());
                personNames.stream().forEach(personName -> {
                    log.info("Working with person name: {}", personName);
                    PersonSearchResponseDTO.PersonSearchResponseDTOBuilder builder = PersonSearchResponseDTO.builder();
                    builder.organisationId(organisationId);
                    builder.organisationType("FINANCIAL_INSTITUTION");
                    log.info("Looking for person identification.");
                    final List<RolePlayer> rolePlayers = Optional.ofNullable(personName.getIdentification())
                            .map(personIdentification -> {
                                log.info("Found person Identification: {}", personIdentification);
                                return Optional.ofNullable(personIdentification.getPerson()).map(person -> {
                                    log.info("Found person {}", personName);
                                    builder.personId(person.getId());
                                    builder.personFullName(personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName());
                                    return Optional.ofNullable(person.getEmployingParty()).map(employingPartyRole -> {
                                        log.info("Found employing party role: {}", employingPartyRole);
                                        final List<RolePlayer> players = employingPartyRole.getPlayer();
                                        log.info("Employing party role players are: {}", players);
                                        return players;
                                    }).orElse(new ArrayList<>());
                                }).orElse(new ArrayList<>());
                            }).orElse(new ArrayList<>());
                    rolePlayers.stream().filter(rolePlayer -> {
                        return rolePlayer.getId().equals(organisationId);
                    }).findFirst().ifPresent(rolePlayer -> {
                        log.info("Person {} is under FI {}", personName, organisationId);
                        personSearchResponseDTOS.add(builder.build());
                    });
                });
                break;
            case ACCOUNT_HOLDER:
                log.info("Filtering {} person...", personType.name());
                personNames.stream().forEach(personName -> {
                    PersonSearchResponseDTO.PersonSearchResponseDTOBuilder builder = PersonSearchResponseDTO.builder();
                    builder.organisationId(organisationId);
                    builder.organisationType("FINANCIAL_INSTITUTION");
                    Optional.ofNullable(personName.getIdentification()).ifPresent(personIdentification -> {
                        Optional.ofNullable(personIdentification.getPerson()).ifPresent(person -> {
                            final List<Account> clientAccounts = accountService.getAccountsByClientId(person.getId());
                            clientAccounts.stream().forEach(account -> {
                                final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionByAccountId(account.getId());
                                financialInstitutionOptional.ifPresent(financialInstitution -> {
                                    if (financialInstitution.getId().equals(organisationId)) {
                                        builder.personId(person.getId());
                                        builder.personFullName(personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName());
                                        personSearchResponseDTOS.add(builder.build());
                                    }
                                });
                            });
                        });
                    });
                });
                break;
            default:
        }

        return personSearchResponseDTOS.stream().collect(Collectors.toList());
    }

    @Override
    public Optional<VirtualIdAvailabilityCheckDTO> checkVirtualIdAvailability(String virtualId) {
        return partyService.checkVirtualIdAvailability(virtualId);
    }

    @Override
    public Optional<VirtualSuffixAvailabilityCheckDTO> checkVirtualSuffixAvailability(String virtualSuffix) {
        return financialInstitutionService.checkVirtualSuffixAvailability(virtualSuffix);
    }

    @Override
    public Optional<VirtualCodeAvailabilityCheckDTO> checkVirtualCodeAvailability(String virtualCode) {
        return accountService.checkVirtualCodeAvailability(virtualCode);
    }

    @Override
    public Optional<UsernameCheckDTO> checkUsernameAvailability(String username) {
        return profileService.checkUsernameAvailability(username);

    }

    @Override
    public Optional<CurrencyCodeAvailabilityCheckDTO> checkCurrencyCodeAvailability(String currencyCode) {
        return currencyService.checkCurrencyCodeAvailability(currencyCode);
    }

    private PersonName getPersonNameObject (Person person){
            return person.getPersonIdentification().stream().findFirst()
                    .map(personIdentification -> personIdentification.getPersonName().stream().findFirst().orElse(null))
                    .orElse(null);
        }
    }
