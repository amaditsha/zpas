package zw.co.esolutions.zpas.services.impl.account;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.OffsetDateTime;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.Resource;
import zw.co.esolutions.zpas.dto.account.CashAccountDTO;
import zw.co.esolutions.zpas.dto.account.CashAccountEntry;
import zw.co.esolutions.zpas.dto.documents.DocumentDTO;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PartyIdentificationInformationRepository;
import zw.co.esolutions.zpas.repository.UploadAccountsRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.documents.DocumentsService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.account.AccountUploadDTO;
import zw.co.esolutions.zpas.services.iface.account.UploadAccountsService;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.storage.StorageService;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by petros on 14 Feb 2019
 */
@Slf4j
@Service
@Transactional
public class UploadAccountsServiceImpl implements UploadAccountsService {

    @Autowired
    private AccountsService accountService;
    @Autowired
    private UploadAccountsRepository uploadAccountsRepository;
    @Autowired
    private DocumentsService documentsService;
    @Autowired
    private StorageService storageService;
    @Autowired
    private PartyIdentificationInformationRepository partyIdentificationInformationRepository;

    public Optional<Party> getPartyByNationalRegistrationNumber(String nationalRegistrationNumber){
        PartyIdentificationInformation partyIdentificationInformation = partyIdentificationInformationRepository
                .findByNationalRegistrationNumber(nationalRegistrationNumber);

        if(partyIdentificationInformation == null) {
            return Optional.empty();
        } else {
            log.info("The identification id details : {}", partyIdentificationInformation.getId());
            if(partyIdentificationInformation instanceof PersonIdentification) {
                PersonIdentification personIdentification = (PersonIdentification) partyIdentificationInformation;
                return Optional.ofNullable(personIdentification.getPerson());
            } else {
                OrganisationIdentification organisationIdentification = (OrganisationIdentification) partyIdentificationInformation;
                return Optional.ofNullable(organisationIdentification.getOrganisation());
            }
        }
    }


    @Override
    public List<CashAccount> uploadAccounts(AccountUploadDTO uploadDTO) {

        UploadAccounts uploadAccounts = new UploadAccounts();
        uploadAccounts.setId(GenerateKey.generateEntityId());
        uploadAccounts.setDateCreated(OffsetDateTime.now());
        uploadAccounts.setEntityStatus(EntityStatus.DRAFT);
        uploadAccounts.setPurpose(uploadDTO.getPurpose());
        uploadAccounts.setUploader(uploadDTO.getUsername());

        UploadAccounts accounts = uploadAccountsRepository.save(uploadAccounts);

        uploadDTO.setUploadFileId(accounts.getId());


        log.info("Accounts Upload DTO is: {}", uploadDTO);
        final String fileName = uploadDTO.getFileName();
        if (StringUtils.isEmpty(fileName)) {
            log.error("Invalid File ID specified. Failed to load uploaded accounts file. Upload and try again.");
            return null;
        }

        final String fileExtension = getFileExtension(fileName);

        switch (fileExtension) {
            case ".csv": {
                List<CashAccountDTO> cashAccountDTOS = readAccountsFromCSVFile(uploadDTO);
                saveDocument(uploadDTO);
                return saveCashAccounts(cashAccountDTOS);
            }
            case ".xls":
            case ".xlsx": {
                List<CashAccountDTO> cashAccountDTOS = readAccountsFromExcelFile(uploadDTO);
                saveDocument(uploadDTO);
                return saveCashAccounts(cashAccountDTOS);
            }
            default:
                throw new UnsupportedOperationException("Extension " + fileExtension + " not supported yet");
        }
    }

    @Override
    public Optional<UploadAccounts> getUploadAccounts(String id) {
        return uploadAccountsRepository.findById(id);
    }

    public List<CashAccount> saveCashAccounts(List<CashAccountDTO> cashAccountDTOS) {
        List<CashAccount> cashAccounts = new ArrayList<>();
        for(int i = 0; i < cashAccountDTOS.size(); i++) {
            CashAccountDTO cashAccountDTO = cashAccountDTOS.get(i);
            CashAccount cashAccount = accountService.createCashAccount(cashAccountDTO, cashAccountDTO.getClientId(), cashAccountDTO.getFinancialInstitutionId());

            cashAccounts.add(cashAccount);
        }

        return cashAccounts;
    }

    private static String getFileExtension(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            return "";
        }
        if (!fileName.contains(".")) {
            return "";
        }
        final String fileExtension = fileName.substring(fileName.lastIndexOf('.'));
        return fileExtension;
    }


    private List<CashAccountDTO> readAccountsFromCSVFile(AccountUploadDTO accountUploadDTO) {
        log.info("Reading uploaded bulk accounts file...");

        Resource file = storageService.loadAsResource(accountUploadDTO.getFileName());
        List<CashAccountDTO> cashAccounts = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader(Paths.get(file.getURI()))) {
            CsvToBean<CashAccountEntry> csvAccountEntryCsvToBean = new CsvToBeanBuilder(reader)
                    .withType(CashAccountEntry.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<CashAccountEntry> csvPaymentEntryIterator = csvAccountEntryCsvToBean.iterator();

            while (csvPaymentEntryIterator.hasNext()) {
                final CashAccountEntry cashAccountEntry = csvPaymentEntryIterator.next();
                CashAccountDTO cashAccountDTO = cashAccountEntry.getCashAccountDTO();
                cashAccountDTO.setUploadAccountsId(accountUploadDTO.getUploadFileId());
                cashAccountDTO.setFinancialInstitutionId(accountUploadDTO.getFinancialInstitutionId());

                log.info("The generated cash account dto {}", cashAccountDTO);
                // Get the person
                Optional<Party> partyOptional = getPartyByNationalRegistrationNumber(cashAccountDTO.getRegistrationNumber());
                partyOptional.map(party -> {
                    log.info("Client found with the registration number {} and the party id is {} ", cashAccountDTO.getRegistrationNumber(), party.getId());
                    cashAccountDTO.setClientId(party.getId());
                    cashAccounts.add(cashAccountDTO);
                    return party;
                }).orElseGet(null);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cashAccounts;
    }

    private List<CashAccountDTO> readAccountsFromExcelFile(AccountUploadDTO accountUploadDTO) {

        log.info("Reading uploaded accounts file...");
        List<CashAccountDTO> cashAccounts = new ArrayList<>();
        final Map<Integer, List<String>> accountsData = readExcelFile(accountUploadDTO.getFileName());
        for (List<String> dataList : accountsData.values()) {
            final CashAccountEntry paymentEntry = CashAccountEntry.builder()
                    .registrationNumber(dataList.get(0))
                    .name(dataList.get(1))
                    .bBAN(dataList.get(2))
                    .uPIC(dataList.get(3))
                    .iBAN(dataList.get(4))
                    .cashAccountType(dataList.get(5))
                    .level(dataList.get(6))
                    .purpose(dataList.get(7))
                    .baseCurrency(dataList.get(8))
                    .reportingCurrency(dataList.get(9))
                    .settlementCurrency(dataList.get(10))
                    .openingDate(dataList.get(11))
                    .liveDate(dataList.get(12))
                    .closingDate(dataList.get(13))
                    .build();
            CashAccountDTO cashAccountDTO = paymentEntry.getCashAccountDTO();
            cashAccountDTO.setUploadAccountsId(accountUploadDTO.getUploadFileId());
            cashAccountDTO.setFinancialInstitutionId(accountUploadDTO.getFinancialInstitutionId());
            // Get the person
            Optional<Party> partyOptional = getPartyByNationalRegistrationNumber(cashAccountDTO.getRegistrationNumber());
            partyOptional.map(party -> {
                log.info("Client found with the registration number {} and the party id is {} ", cashAccountDTO.getRegistrationNumber(), party.getId());
                cashAccountDTO.setClientId(party.getId());
                cashAccounts.add(cashAccountDTO);
                return party;
            }).orElseGet(null);

        }
        return cashAccounts;
    }


    private static Map<Integer, List<String>> readExcelFile(String fileName) {
        switch (getFileExtension(fileName)) {
            case ".xls":
                return readXLSExcelFile(fileName);
            case ".xlsx":
                readXLSXExcelFile(fileName);
            default:
                throw new RuntimeException("Excel Processor could not read unknown file extension: " + getFileExtension(fileName));
        }
    }

    private void saveDocument(AccountUploadDTO accountUploadDTO) {
        final String fileName = accountUploadDTO.getFileName();

        documentsService.createDocument(DocumentDTO.builder()
                .clientId(accountUploadDTO.getClientId())
                .copyDuplicate("")
                .dataSetType("")
                .documentName(fileName)
                .documentPath(Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName).toString())
                .issueDate(LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                .purpose(accountUploadDTO.getPurpose())
                .build());
    }

    private static Map<Integer, List<String>> readXLSExcelFile(String fileName) {
        Path excelFile = Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName);

//        HSSFWorkbook, HSSFSheet, HSSFRow, and HSSFCell
        Map<Integer, List<String>> data = new HashMap<>();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(Files.newInputStream(excelFile));
            HSSFSheet sheet = workbook.getSheetAt(0);

            int i = 0;
            final Iterator<Row> hssRows = sheet.iterator();
            while (hssRows.hasNext()) {
                final HSSFRow row = (HSSFRow) hssRows.next();
                data.put(i, new ArrayList<>());
                final Iterator<Cell> cells = row.iterator();
                while (cells.hasNext()) {
                    final HSSFCell cell = (HSSFCell) cells.next();
                    switch (cell.getCellType()) {
                        case STRING: {
                            data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
                            break;
                        }
                        case NUMERIC: {
                            if (DateUtil.isCellDateFormatted(cell)) {
                                data.get(i).add(cell.getDateCellValue() + "");
                            } else {
                                data.get(i).add(cell.getNumericCellValue() + "");
                            }
                            break;
                        }
                        case BOOLEAN: {
                            data.get(i).add(cell.getBooleanCellValue() + "");
                            break;
                        }
                        case FORMULA: {
                            data.get(i).add(cell.getCellFormula() + "");
                            break;
                        }
                        default:
                            data.get(new Integer(i)).add(" ");
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static Map<Integer, List<String>> readXLSXExcelFile(String fileName) {
        Path excelFile = Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName);

        Map<Integer, List<String>> data = new HashMap<>();
        try {
            Workbook workbook = new XSSFWorkbook(Files.newInputStream(excelFile));
            Sheet sheet = workbook.getSheetAt(0);

            int i = 0;
            for (Row row : sheet) {
                data.put(i, new ArrayList<>());
                for (Cell cell : row) {
                    switch (cell.getCellType()) {
                        case STRING: {
                            data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
                            break;
                        }
                        case NUMERIC: {
                            if (DateUtil.isCellDateFormatted(cell)) {
                                data.get(i).add(cell.getDateCellValue() + "");
                            } else {
                                data.get(i).add(cell.getNumericCellValue() + "");
                            }
                            break;
                        }
                        case BOOLEAN: {
                            data.get(i).add(cell.getBooleanCellValue() + "");
                            break;
                        }
                        case FORMULA: {
                            data.get(i).add(cell.getCellFormula() + "");
                            break;
                        }
                        default:
                            data.get(new Integer(i)).add(" ");
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }


}
