package zw.co.esolutions.zpas.services.impl.payments.cache;

import com.google.common.base.Preconditions;
import com.hazelcast.core.IMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.model.BulkPayment;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.ScheduledService;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.domain.PaymentStatusInfo;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 21 May 2019.
 */
@Slf4j
@Component
public class PaymentsCacheProcessor {
    private PaymentsCache paymentsCache;
    private PaymentRepository paymentRepository;

    @Inject
    public PaymentsCacheProcessor(PaymentsCache paymentsCache, PaymentRepository paymentRepository) {
        this.paymentsCache = paymentsCache;
        this.paymentRepository = paymentRepository;
    }

    public PaymentStatusInfo getPaymentStatusInfo(String paymentReference) {
        return getPaymentStatusInfoMap().getOrDefault(paymentReference, cachePaymentStatusInfo(paymentReference));
    }

    public IMap<String, PaymentStatusInfo> getPaymentStatusInfoMap() {
        return paymentsCache.getPaymentStatusInfoCacheInstance();
    }

    private PaymentStatusInfo cachePaymentStatusInfo(String paymentReference) {
        PaymentStatusInfo paymentStatusInfo = getPaymentStatusInfoMap().computeIfAbsent(paymentReference, key -> {
            log.info("Initialising payment info cache -> {}", key);
            try {
                Optional<Payment> paymentOptional = paymentRepository.findById(key);
                Preconditions.checkArgument(paymentOptional.isPresent(), "Payment not found: " + key);
                Payment payment = paymentOptional.get();
                log.info("Latest payment status and amount -> {}|{}", payment.getEntityStatus().name(), payment.getAmount());
                log.info("Now writing to Cache");

                PaymentStatusInfo.PaymentStatusInfoBuilder paymentStatusInfoBuilder = PaymentStatusInfo.builder();
                paymentStatusInfoBuilder.requestId(payment.getId());
                paymentStatusInfoBuilder.status(ScheduledService.getVerificationStatus(payment));
                paymentStatusInfoBuilder.info(ScheduledService.getVerificationInfo(payment));
                final List<PaymentStatusInfo> paymentStatusInfos = new ArrayList<>();
                if(payment instanceof BulkPayment) {
                    ((BulkPayment) payment).getGroups().forEach(individualPayment -> {
                        PaymentStatusInfo statusInfo = paymentStatusInfoBuilder
                                .requestId(individualPayment.getId())
                                .status(ScheduledService.getVerificationStatus(individualPayment))
                                .info(ScheduledService.getVerificationInfo(individualPayment))
                                .build();
                        paymentStatusInfos.add(statusInfo);
                    });
                } else {
                    log.info("This is an individual payment");
                }
                paymentStatusInfoBuilder.sseMessages(paymentStatusInfos);
                return paymentStatusInfoBuilder.build();
            } catch (Exception e) {
                log.error("Exception loading account into cache {}|{}", paymentReference, e.getMessage());
                e.printStackTrace();
                return null;
            }
        });

        Preconditions.checkNotNull(paymentStatusInfo, "Failed to initialize account and balance info");
        return paymentStatusInfo;
    }

}
