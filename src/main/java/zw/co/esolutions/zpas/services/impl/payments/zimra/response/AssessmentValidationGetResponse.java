package zw.co.esolutions.zpas.services.impl.payments.zimra.response;

import lombok.Builder;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by alfred on 21 August 2019
 */
@Builder
public class AssessmentValidationGetResponse {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "year")
    private String year;

    @XmlElement(name = "assNo")
    private String assNo;

    @XmlElement(name = "office")
    private String office;

    @XmlElement(name = "amount")
    private String amount;

    @XmlElement(name = "taxType")
    private String taxType;

    @XmlElement(name = "currency")
    private String currency;

    @XmlElement(name = "found")
    private String found;

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAssNo() {
        return assNo;
    }

    public void setAssNo(String assNo) {
        this.assNo = assNo;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFound() {
        return found;
    }

    public void setFound(String found) {
        this.found = found;
    }

    public String getFormattedAmount() {
        return String.format("%s%s", currency, amount);
    }
}
