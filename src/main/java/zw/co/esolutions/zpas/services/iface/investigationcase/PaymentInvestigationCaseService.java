package zw.co.esolutions.zpas.services.iface.investigationcase;

import zw.co.esolutions.zpas.dto.investigationcase.InvestigationCaseStatusDTO;
import zw.co.esolutions.zpas.dto.investigationcase.InvestigationStatusDTO;
import zw.co.esolutions.zpas.dto.investigationcase.PaymentInvestigationCaseDTO;
import zw.co.esolutions.zpas.dto.payments.RequestInfo;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.services.impl.process.pojo.DebitAuthorisationDTO;

import java.util.List;
import java.util.Optional;


public interface PaymentInvestigationCaseService {
    PaymentInvestigationCase createPaymentInvestigationCase(PaymentInvestigationCaseDTO paymentInvestigationCaseDTO, String clientId);
    PaymentInvestigationCase updatePaymentInvestigationCase(PaymentInvestigationCase paymentInvestigationCase);
    PaymentInvestigationCase deletePaymentInvestigationCase(String id);
    PaymentInvestigationCase approvePaymentInvestigationCase(String id);
    PaymentInvestigationCase rejectPaymentInvestigationCase(String id);

    PaymentInvestigationCase findPaymentInvestigationCaseById(String id);
    PaymentInvestigationCase findById(String id);
    List<PaymentInvestigationCase> getAllByCaseId(String caseId);
    PaymentInvestigationCase aggregatePaymentReq(RequestInfo requestInfo);
    List<PaymentInvestigationCase> searchInvestigationCase(String searchTerm);
    String refreshPaymentInvestigationCaseSearchIndex();

    PaymentInvestigationCase debitAuthorisationResponseInitiation(DebitAuthorisationDTO dto);

    Optional<InvestigationStatusDTO> getInvestigationStatusesByClientId(String clientId);
    PaymentInvestigationCase updateInvestigationCaseStatus(InvestigationCaseStatusDTO investigationCaseStatusDTO);

    List<PaymentInvestigationCase> getPaymentInvestigationCasesByPayment(String paymentId);
}
