package zw.co.esolutions.zpas.services.impl.enversaudit;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.model.enversaudit.AuditedRevisionEntity;
import zw.co.esolutions.zpas.repository.AuditedRevisionEntityRepository;
import zw.co.esolutions.zpas.services.iface.enversaudit.AuditReportService;
import zw.co.esolutions.zpas.utilities.audit.Auditable;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class AuditReportServiceImpl  implements AuditReportService {
    @Autowired
    EntityManager entityManager;

    Auditable auditable;

    @Autowired
    private AuditedRevisionEntityRepository auditedRevisionEntityRepository;

    @Override
    public List<? extends Auditable> getAuditEntries(String username, Class<? extends Auditable> clazz) {
        List<Integer> revisionIds = new ArrayList<>();
        final List<AuditedRevisionEntity> auditedRevisionEntities = auditedRevisionEntityRepository.findAllByUsername(username);
        revisionIds.addAll(auditedRevisionEntities.stream().map(AuditedRevisionEntity::getId).collect(Collectors.toList()));

        List revisions = AuditReaderFactory.get(entityManager)
                .createQuery()
                .forRevisionsOfEntity(clazz, true, true)
                .add(AuditEntity.revisionNumber().in(revisionIds))
//                .add(AuditEntity.property("rev").in(revisionIds))
                .getResultList();
        return revisions;
    }

    @Override
    public String getAuditableStrings() {
        try{
            return auditable.getAuditableAttributesString();
        }catch (Exception e){
            log.error("An error occurred trying to log revisions. {}", e.getMessage());
            return e.getMessage();
        }
    }
}
