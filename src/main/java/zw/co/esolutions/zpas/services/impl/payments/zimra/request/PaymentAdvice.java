package zw.co.esolutions.zpas.services.impl.payments.zimra.request;

import lombok.Builder;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by alfred on 21 August 2019
 */
@Builder
public class PaymentAdvice {
    @XmlElement(name = "SerialNumber")
    private String serialNumber;

    @XmlElement(name = "ReferenceNumber")
    private String referenceNumber;

    @XmlElement(name = "AccountNumber")
    private String accountNumber;

    @XmlElement(name = "BPNumber")
    private String bpNumber;

    @XmlElement(name = "ClientName")
    private String clientName;

    @XmlElement(name = "assNo")
    private String assNo;

    @XmlElement(name = "TaxCode")
    private String taxCode;

    @XmlElement(name = "Region")
    private String region;

    @XmlElement(name = "PaymentDate")
    private String paymentDate;

    @XmlElement(name = "CaptureTime")
    private String captureTime;

    @XmlElement(name = "Currency")
    private String currency;

    @XmlElement(name = "Amount")
    private Double amount;

    @XmlElement(name = "RRN")
    private String rrn;

    @XmlElement(name = "UserID")
    private String userID;

    @XmlAttribute(name = "xmlns")
    private String xmlns;

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getBPNumber() {
        return bpNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public String getRegion() {
        return region;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public String getCaptureTime() {
        return captureTime;
    }

    public String getCurrency() {
        return currency;
    }

    public Double getAmount() {
        return amount;
    }

    public String getRRN() {
        return rrn;
    }

    public String getUserID() {
        return userID;
    }

    public String get_xmlns() {
        return xmlns;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBPNumber(String bpNumber) {
        this.bpNumber = bpNumber;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setCaptureTime(String captureTime) {
        this.captureTime = captureTime;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setRRN(String rrn) {
        this.rrn = rrn;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void set_xmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    @Override
    public String toString() {
        return "PaymentAdvice{" +
                "serialNumber='" + serialNumber + '\'' +
                ", referenceNumber='" + referenceNumber + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", bpNumber='" + bpNumber + '\'' +
                ", clientName='" + clientName + '\'' +
                ", taxCode='" + taxCode + '\'' +
                ", region='" + region + '\'' +
                ", paymentDate='" + paymentDate + '\'' +
                ", captureTime='" + captureTime + '\'' +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", rrn='" + rrn + '\'' +
                ", userID='" + userID + '\'' +
                ", xmlns='" + xmlns + '\'' +
                '}';
    }
}
