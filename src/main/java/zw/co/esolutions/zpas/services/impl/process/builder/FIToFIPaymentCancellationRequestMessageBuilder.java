package zw.co.esolutions.zpas.services.impl.process.builder;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

import zw.co.esolutions.zpas.iso.msg.camt056_001_07.PartyIdentification125;
import zw.co.esolutions.zpas.iso.msg.camt056_001_07.BranchAndFinancialInstitutionIdentification5;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt056_001_07.Party35Choice;
import javax.xml.datatype.XMLGregorianCalendar;
import zw.co.esolutions.zpas.iso.msg.camt056_001_07.Case4;
import zw.co.esolutions.zpas.iso.msg.camt056_001_07.ControlData1;
import zw.co.esolutions.zpas.iso.msg.camt056_001_07.CaseAssignment4;

import zw.co.esolutions.zpas.iso.msg.camt056_001_07.FIToFIPaymentCancellationRequestV07;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.Assignee;
import zw.co.esolutions.zpas.model.Assigner;
import zw.co.esolutions.zpas.model.InvestigationCase;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.services.impl.process.pojo.PaymentInfoDto;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationParty_056RoleUtil;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentInfoUtil;

@Slf4j
@Component
public class FIToFIPaymentCancellationRequestMessageBuilder {
    @Autowired
    InvestigationParty_056RoleUtil investigationParty_056RoleUtil;

    @Autowired
    PaymentInfoUtil paymentInfoUtil;

    public FIToFIPaymentCancellationRequestV07 buildFiToFIPaymentCancellationRequestMsg(PaymentInvestigationCase investigationCase){

        FIToFIPaymentCancellationRequestV07 fiToFIPaymentCancellationRequestV07 = new FIToFIPaymentCancellationRequestV07();



        CaseAssignment4 assignment = new CaseAssignment4();
        assignment.setId(investigationCase.getIdentification());
        assignment.setAssgnr(this.investigationParty_056RoleUtil.getParty35Choice(investigationCase, Assigner.class));
        assignment.setAssgne(this.investigationParty_056RoleUtil.getParty35Choice(investigationCase, Assignee.class));
        assignment.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        Party35Choice Party35Choice = new Party35Choice();
        Party35Choice.setPty(this.investigationParty_056RoleUtil.getPartyIdentification125(investigationCase, PartyIdentification125.class));
        Party35Choice.setAgt(this.investigationParty_056RoleUtil.getBranchAndFinancialInstitutionIdentification5(investigationCase, BranchAndFinancialInstitutionIdentification5.class));

        Case4 case4 = new Case4();
        case4.setId(investigationCase.getIdentification());
        case4.setCretr(Party35Choice);
        case4.setReopCaseIndctn(false);


        ControlData1 controlData1 = new ControlData1();

        PaymentInfoDto paymentInfoDto = paymentInfoUtil.getPaymentInfo(investigationCase.getUnderlyingPayment());

        controlData1.setNbOfTxs(String.valueOf(paymentInfoDto.nrOfPayments));
        controlData1.setCtrlSum(paymentInfoDto.getControlSum());


        fiToFIPaymentCancellationRequestV07.setAssgnmt(assignment);
        fiToFIPaymentCancellationRequestV07.setCase(case4);
        fiToFIPaymentCancellationRequestV07.setCtrlData(controlData1);
        fiToFIPaymentCancellationRequestV07.getSplmtryData();
        fiToFIPaymentCancellationRequestV07.getUndrlyg();


        return  fiToFIPaymentCancellationRequestV07;
    }
}
