package zw.co.esolutions.zpas.services.nssa.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import zw.co.esolutions.zpas.dto.nssa.EmployeeDependentDTO;
import zw.co.esolutions.zpas.repository.OrganisationRepository;
import zw.co.esolutions.zpas.services.nssa.iface.EmployeeDependentService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class EmployeeDependentServiceImpl implements EmployeeDependentService {



    @Value("${NSSA.CONTRIBUTIONS.SYSTEM.BASE.URL}")
    private String nssaContributionsSystemBaseUrl;

    @Autowired
    OrganisationRepository organisationRepository;

    @Override
    public Optional<EmployeeDependentDTO> createEmployeeDependent(EmployeeDependentDTO employeeDependentDTO) {

        RestTemplate restTemplate = new RestTemplate();
        String saveEmployeeDependentsUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/employee/dependents";

        HttpEntity<EmployeeDependentDTO> request = new HttpEntity<>(employeeDependentDTO);
        EmployeeDependentDTO employeeDependent = restTemplate.postForObject(saveEmployeeDependentsUrl, request, EmployeeDependentDTO.class);

        return Optional.ofNullable(employeeDependent);
    }

    @Override
    public Optional<EmployeeDependentDTO> getEmployeeDependentById(String employeeDependentId) {
        RestTemplate restTemplate = new RestTemplate();
        String getEmployeeDependentUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/dependents/" + employeeDependentId;

        EmployeeDependentDTO employeeDependentDTO = restTemplate.getForObject(getEmployeeDependentUrl, EmployeeDependentDTO.class);

        return Optional.ofNullable(employeeDependentDTO);
    }

    @Override
    public List<EmployeeDependentDTO> getEmployeeDependents() {
        String getEmployeeDependentsUrl = nssaContributionsSystemBaseUrl + "/employee/dependents";

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<EmployeeDependentDTO>> response = restTemplate.exchange(
                getEmployeeDependentsUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<EmployeeDependentDTO>>() {
                });
        List<EmployeeDependentDTO> employeeDependents = response.getBody();
        return employeeDependents;
    }

    @Override
    public List<EmployeeDependentDTO> getEmployeeDependentByEmployeeId(String employeeId) {
        String getEmployeeDependentsByEmployeeUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/employee/dependent-by/employee/" + employeeId;
        log.info("trying to fetch employee dependent by employee url {}", getEmployeeDependentsByEmployeeUrl);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<EmployeeDependentDTO>> response = restTemplate.exchange(
                getEmployeeDependentsByEmployeeUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<EmployeeDependentDTO>>() {
                });
        List<EmployeeDependentDTO> employeeDependents = response.getBody();
        return employeeDependents;
    }

    @Override
    public List<EmployeeDependentDTO> getEmployeeDependentByEmployeeSsn(String employeeSsn) {
        //organisationRepository.findById(employeeSsn);
        String getEmployeeDependentsByEmployerUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/employee/dependent-by/employee-ssn/" + employeeSsn;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<EmployeeDependentDTO>> response = restTemplate.exchange(
                getEmployeeDependentsByEmployerUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<EmployeeDependentDTO>>() {
                });
        List<EmployeeDependentDTO> employeeDependents = response.getBody();
        return employeeDependents;
    }

    @Override
    public Optional<EmployeeDependentDTO> deleteEmployeeDependent(String employeeDependentId) {
        String getEmployeeDependentUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/delete/employee/dependent/" + employeeDependentId;
        return actOnEmployeeDependent(getEmployeeDependentUrl);
    }

    @Override
    public Optional<EmployeeDependentDTO> approveEmployeeDependent(String employeeDependentId) {
        String getEmployeeDependentUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/approve/employee/dependent/" + employeeDependentId;
        return actOnEmployeeDependent(getEmployeeDependentUrl);
    }

    @Override
    public Optional<EmployeeDependentDTO> rejectEmployeeDependent(String employeeDependentId) {
        String getEmployeeDependentUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/reject/employee/dependent/" + employeeDependentId;
        return actOnEmployeeDependent(getEmployeeDependentUrl);
    }

    private Optional<EmployeeDependentDTO> actOnEmployeeDependent(String actionUrl) {
        RestTemplate restTemplate = new RestTemplate();
        EmployeeDependentDTO employeeDependentDTO = restTemplate.getForObject(actionUrl, EmployeeDependentDTO.class);
        return Optional.ofNullable(employeeDependentDTO);
    }

    @Override
    public Optional<EmployeeDependentDTO> updateEmployeeDependent(EmployeeDependentDTO employeeDependentDTO) {
        employeeDependentDTO.setDateOfBirth(LocalDate.parse(employeeDependentDTO.getDateOfBirthString(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd")).atTime(OffsetTime.now()));
        employeeDependentDTO.setDateOfMarriage(LocalDate.parse(employeeDependentDTO.getDateOfMarriageString(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd")).atTime(OffsetTime.now()));

        RestTemplate restTemplate = new RestTemplate();
        String updateEmployeesUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/employee-dependents/edit";
        log.info("trying to reach update employee dependent url {}", updateEmployeesUrl);

        ResponseEntity<EmployeeDependentDTO> response = restTemplate.exchange(
                updateEmployeesUrl,
                HttpMethod.POST,
                new HttpEntity<>(employeeDependentDTO),
                new ParameterizedTypeReference<EmployeeDependentDTO>() {
                });
        EmployeeDependentDTO employee = response.getBody();
        return Optional.ofNullable(employee);
    }


}
