package zw.co.esolutions.zpas.services.iface.signature;

import zw.co.esolutions.zpas.model.SigningDetail;

import java.util.List;
import java.util.Optional;

public interface SigningDetailService {
    SigningDetail createSigningDetail(SigningDetail signingRule);
    SigningDetail updateSigningDetail(SigningDetail signingRule);
    SigningDetail deleteSigningDetail(String id);
    Optional<SigningDetail> getSigningDetailById(String signatureId);
    List<SigningDetail> getSigningDetails();
}
