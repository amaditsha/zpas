package zw.co.esolutions.zpas.services.impl.merchant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.merchant.PartyMerchantAccountDTO;
import zw.co.esolutions.zpas.model.Merchant;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.PartyMerchantAccount;
import zw.co.esolutions.zpas.repository.MerchantRepository;
import zw.co.esolutions.zpas.repository.PartyMerchantAccountRepository;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.services.iface.merchant.PartyMerchantAccountService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Slf4j
@Service
public class PartyMerchantAccountServiceImpl implements PartyMerchantAccountService {
    
    @Autowired
    PartyMerchantAccountRepository partyMerchantAccountRepository;

    @Autowired
    PartyRepository partyRepository;

    @Autowired
    MerchantRepository merchantRepository;
    
    @Override
    public PartyMerchantAccount createMerchant(PartyMerchantAccountDTO merchant) {
        log.info("Party Merchant Account DTO is: {}", merchant);
        PartyMerchantAccount partyMerchantAccount = new PartyMerchantAccount();
        partyMerchantAccount.setId(GenerateKey.generateEntityId());
        partyMerchantAccount.setDateCreated(OffsetDateTime.now());
        partyMerchantAccount.setEntityVersion(0L);
        partyMerchantAccount.setLastUpdated(OffsetDateTime.now());
        partyMerchantAccount.setEntityStatus(EntityStatus.ACTIVE);

        partyMerchantAccount.setIdentification(merchant.getIdentification());
        partyMerchantAccount.setAccountNumber(merchant.getAccountNumber());

        Optional<Party> optionalParty = partyRepository.findById(merchant.getClientId());

        optionalParty.ifPresent(party -> {
            log.info("The party to register the merchant account is found.");
            partyMerchantAccount.setLinkedParty(party);
        });

        Optional<Merchant> optionalMerchant = merchantRepository.findById(merchant.getMerchantId());

        optionalMerchant.ifPresent(
                merchant1 -> {
                    log.info("Merchant found");
                    partyMerchantAccount.setMerchant(merchant1);
                }
        );
        return partyMerchantAccountRepository.save(partyMerchantAccount);
    }

    @Override
    public PartyMerchantAccount updatePartyMerchantAccount(PartyMerchantAccountDTO merchant) {
        return partyMerchantAccountRepository.findById(merchant.getId()).map(partyMerchantAccount -> {
            partyMerchantAccount.setAccountNumber(merchant.getAccountNumber());
            partyMerchantAccount.setIdentification(merchant.getIdentification());
            partyMerchantAccount.setLastUpdated(OffsetDateTime.now());
            partyMerchantAccount.setEntityStatus(EntityStatus.ACTIVE);
            return partyMerchantAccountRepository.save(partyMerchantAccount);
        }).orElseGet(() -> {
            return null;
        });
    }

    @Override
    public PartyMerchantAccount deletePartyMerchantAccount(String id) {
        Optional<PartyMerchantAccount> merchantOptional = partyMerchantAccountRepository.findById(id);
        PartyMerchantAccount merchant = null;

        if(merchantOptional.isPresent()){
            merchant = merchantOptional.get();
            merchant.setEntityStatus(EntityStatus.DELETED);
            merchant = partyMerchantAccountRepository.save(merchant);
        }else{
            log.info("PartyMerchantAccount has already been deleted");
        }
        return merchant;
    }

    @Override
    public PartyMerchantAccount approvePartyMerchantAccount(String id) {
        Optional<PartyMerchantAccount> optionalPartyMerchantAccount = partyMerchantAccountRepository.findById(id);
        PartyMerchantAccount merchant = null;
        if (optionalPartyMerchantAccount.isPresent()){
            merchant = optionalPartyMerchantAccount.get();
            if(merchant.getEntityStatus()!= EntityStatus.DELETED){
                merchant.setEntityStatus(EntityStatus.ACTIVE);
                merchant = partyMerchantAccountRepository.save(merchant);
            }else{
                log.info("PartyMerchantAccount has already been approved");
            }
        }
        return merchant;
    }

    @Override
    public PartyMerchantAccount rejectPartyMerchantAccount(String id) {
        Optional<PartyMerchantAccount> optionalPartyMerchantAccount = partyMerchantAccountRepository.findById(id);
        PartyMerchantAccount merchant = null;
        if (optionalPartyMerchantAccount.isPresent()) {
            merchant = optionalPartyMerchantAccount.get();
            if(merchant.getEntityStatus() != EntityStatus.DELETED) {
                merchant.setEntityStatus(EntityStatus.DISAPPROVED);
                merchant = partyMerchantAccountRepository.save(merchant);
            } else {
                log.info("PartyMerchantAccount "+ id + "has already been rejected");
            }
        }
        return merchant;
    }

    @Override
    public Optional<PartyMerchantAccount> getPartyMerchantAccount(String id) {
        partyMerchantAccountRepository.findById(id).ifPresent(merchantAccount -> {

        });
        return partyMerchantAccountRepository.findById(id);
    }

    @Override
    public List<PartyMerchantAccount> getPartyMerchantAccounts() {
        return partyMerchantAccountRepository.findAll();
    }

    @Override
    public List<PartyMerchantAccount> getPartyMerchantAccountsByClientId(String clientId) {
        return partyMerchantAccountRepository.findAllByLinkedPartyId(clientId);
    }
}
