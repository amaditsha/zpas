package zw.co.esolutions.zpas.services.impl.operator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Operator;
import zw.co.esolutions.zpas.repository.OperatorRepository;
import zw.co.esolutions.zpas.services.iface.operator.OperatorService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class OperatorServiceImpl implements OperatorService {

    @Autowired
    private OperatorRepository operatorRepository;

    @Override
    public Operator createOperator(Operator operator) {
        return operatorRepository.save(operator);
    }

    @Override
    public Operator updateOperator(Operator operator) {
        operator.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return operatorRepository.save(operator);
    }

    @Override
    public Operator deleteOperator(String id) {
        Optional<Operator> optionalOperator = operatorRepository.findById(id);
        Operator operator = null;

        if (optionalOperator.isPresent()) {
            operator = optionalOperator.get();
            operator.setEntityStatus(EntityStatus.DELETED);
            operator = operatorRepository.save(operator);
        } else {
            log.info("Operator "+ id + "has been deleted");
        }
        return operator;
    }


    @Override
    public Operator approveOperator(String id) {
        Optional<Operator> optionalOperator = operatorRepository.findById(id);
        Operator operator = null;
        if (optionalOperator.isPresent()) {
            operator = optionalOperator.get();
            if(operator.getEntityStatus() != EntityStatus.DELETED) {
                operator.setEntityStatus(EntityStatus.ACTIVE);
                operator = operatorRepository.save(operator);
            } else {
                log.info("Operator "+ id + "has been approved");
            }
        }
        return operator;
    }

    @Override
    public Operator rejectOperator(String id) {
        Optional<Operator> optionalOperator = operatorRepository.findById(id);
        Operator operator = null;
        if (optionalOperator.isPresent()) {
            operator = optionalOperator.get();
            if(operator.getEntityStatus() != EntityStatus.DELETED) {
                operator.setEntityStatus(EntityStatus.DISAPPROVED);
                operator = operatorRepository.save(operator);
            } else {
                log.info("Operator "+ id + "has been rejected");
            }
        }
        return operator;
    }

    @Override
    public Optional<Operator> getOperatorById(String operatorId) {
        return operatorRepository.findById(operatorId);
    }

    @Override
    public List<Operator> getOperatorByCode(String code) {
       return operatorRepository.findByCode(code);
}

    @Override
    public List<Operator> getOperatorByName(String name) {
        return operatorRepository.findByName(name);
    }

}
