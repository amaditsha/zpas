package zw.co.esolutions.zpas.services.impl.payments.cache;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.inject.Singleton;
import java.math.BigDecimal;

/**
 * Created by alfred on 05 July 2019
 */
@Slf4j
@Singleton
@Component
public class GenerationNumberSequenceCache {
    Config cfg;
    HazelcastInstance instance;
    IMap<String, BigDecimal> cache;

    public GenerationNumberSequenceCache() {
        cfg = new Config();
        cfg.setClassLoader(this.getClass().getClassLoader());
        instance = Hazelcast.newHazelcastInstance(cfg);
        cache = instance.getMap(SystemConstants.CACHE_NAME_GENERATION_NUMBER);
        log.info("============================================\n" +
                "Initialising Generation Number Hazelcast cache...\n" +
                "===============================================");
    }

    public IMap<String, BigDecimal> getGenerationNumberCache() {
        return cache;
    }
}
