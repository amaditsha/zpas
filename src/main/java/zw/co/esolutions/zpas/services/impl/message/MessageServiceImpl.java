package zw.co.esolutions.zpas.services.impl.message;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.configs.BmgProps;
import zw.co.esolutions.zpas.dto.message.BulkMessageRequestDTO;
import zw.co.esolutions.zpas.dto.message.BulkMessageResponseDTO;
import zw.co.esolutions.zpas.dto.message.MessageRequestDTO;
import zw.co.esolutions.zpas.dto.message.MessageResponseDTO;
import zw.co.esolutions.zpas.services.iface.message.MessageService;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;


@Slf4j
@Service
public class MessageServiceImpl implements MessageService {
    @Value("${LOCAL.EMAIL}")
    private String fromEmail;

    @Autowired
    public RestTemplate restTemplate;

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    public BmgProps bmgProps;

    @Override
    public MessageResponseDTO send(MessageRequestDTO messageRequestDTO, String customerId) {
        log.info("Sending {} to {}", messageRequestDTO.getMessageChannel().name(), messageRequestDTO.getDestination());

        return messageRequestDTO.getMessageChannel().equals(MessageChannel.EMAIL) ?
                this.sendEmail(messageRequestDTO) :
                this.sendSMS(messageRequestDTO, customerId);
//        this.sendEmail(messageRequestDTO);
//        return this.sendSMS(messageRequestDTO, customerId);
    }

    @Override
    public BulkMessageResponseDTO sendBulk(BulkMessageRequestDTO messageRequestDTO) {
        /*
         * If username & password are not specified use default ones
         * */
        String username = messageRequestDTO.getUsername() == null ? bmgProps.getDefaultUsername() : messageRequestDTO.getUsername();
        String password = messageRequestDTO.getPassword() == null ? bmgProps.getDefaultPassword() : messageRequestDTO.getPassword();

        log.info("Bmg parameters {}:{} used for authorization", username, password);
        BulkMessageResponseDTO smsResponse = new BulkMessageResponseDTO();

        try {
            messageRequestDTO.setBatchNumber(messageRequestDTO.getBatchNumber() == null ? RandomStringUtils.randomNumeric(12) : messageRequestDTO.getBatchNumber());

            HttpHeaders headers = this.createHeaders(username, password);
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<BulkMessageRequestDTO> entity = new HttpEntity<>(messageRequestDTO, headers);

            final ResponseEntity<BulkMessageResponseDTO> responseEntity = restTemplate.exchange(bmgProps.getBmgUrlBatch(), HttpMethod.POST, entity, BulkMessageResponseDTO.class);

            log.info("MESSAGE OBJECT >>>>>>>>>>>> " + responseEntity);
            final BulkMessageResponseDTO responseDTO = responseEntity.getBody();

            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                responseDTO.setResponseText("Bulk Message with reference [" + messageRequestDTO.getBatchNumber() + "] was sent successfully");
                responseDTO.setResponseCode(HttpStatus.OK);
                log.info(responseDTO.getResponseText());
            } else {
                responseDTO.setResponseText("Failed to send bulk message with reference [" + messageRequestDTO.getBatchNumber() + "]");
                responseDTO.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR);
                log.warn(responseDTO.getResponseText());
            }

            return responseDTO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return smsResponse;
    }

    @Override
    public BulkMessageResponseDTO sendBulkByPolling(BulkMessageRequestDTO messageRequestDTO) {
        return null;
    }

    private MessageResponseDTO sendEmail(MessageRequestDTO email) {
        String recipientName = Optional.ofNullable(email.getRecipientName()).orElse(email.getDestination());
        String messageBody = "<html>\n" +
                "    <head>\n" +
                "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"e4e4e4\"><tr><td>\n" +
                "            <table id=\"top-message\" cellpadding=\"20\" cellspacing=\"0\" width=\"600\" align=\"center\">\n" +
                "            </table><!-- top message --> \n" +
                "            <table id=\"main\" width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"15\">\n" +
                "                <tr>\n" +
                "                    <td style=\"font-weight: bold\">\n" +
                "                        Dear " + recipientName + ",\n" +
                "                        <div align=\"left\">" + email.getMessageText() + "</div> \n " +
                "                    </td>\n" +
                "                </tr><!-- content-5 -->               \n" +
                "            </table><!-- main -->\n" +
                "            <table id=\"bottom-message\" cellpadding=\"20\" cellspacing=\"0\" width=\"600\" align=\"center\">\n" +
                "                <tr>\n" +
                "                    <td align=\"center\">\n" +
                "                        <p align=\"center\" style=\"color:#4A72AF\"><small>FOR ANY QUERIES. CONTACT OUR SERVICE DESK ON (04) 480 344</small><br/>\n" +
                "                            <span style=\"font-family: Arial, Helvetica, sans-serif; color:black\"></span></p>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table><!-- top message -->\n" +
                "            </td></tr></table><!-- wrapper -->\n" +
                "    </body>\n" +
                "</html>";
        MimeMessage mail = emailSender.createMimeMessage();
        MessageResponseDTO responseDTO = new MessageResponseDTO();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setFrom(fromEmail);
            helper.setTo(email.getDestination());
            helper.setSubject(email.getSubject());
            helper.setText(messageBody, true);

            for (String attachmentFilePathString : Optional.ofNullable(email.getAttachmentFilePaths()).orElse(new ArrayList<>())) {
                final Path attachmentPath = Paths.get(attachmentFilePathString);
                helper.addAttachment(attachmentPath.getFileName().toString(), attachmentPath.toFile());
            }
            try {
                emailSender.send(mail);
            } catch (RuntimeException re) {
                log.info("Could not send email. {}", re.getMessage());
            }

            responseDTO.setResponseCode(HttpStatus.OK);
            responseDTO.setResponseText("Email to [" + email.getDestination() + "] was successfully send");
        } catch (Exception e) {
            e.printStackTrace();
            responseDTO.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setResponseText(e.getMessage());
        }

        return responseDTO;
    }

    private MessageResponseDTO sendSMS(MessageRequestDTO sms, String customerId) {
        /*
         * If username & password are not specified use default ones
         * */
        String username = sms.getUsername() == null ? bmgProps.getDefaultUsername() : sms.getUsername();
        String password = sms.getPassword() == null ? bmgProps.getDefaultPassword() : sms.getPassword();

        log.info("Bmg parameters {}:{} used for authorization", username, password);
        MessageResponseDTO smsResponse = new MessageResponseDTO();

        try {
            if(ThreadLocalRandom.current().nextBoolean()) {
                sms.setDestination("263775436431");
            } else {
                sms.setDestination("263715636590");
            }
            sms.setMessageReference(RandomStringUtils.randomAlphanumeric(8));
            sms.setMessageDate((new SimpleDateFormat("yyyymmddhhmmss")).format(new Date()));

            HttpHeaders headers = this.createHeaders(username, password);
            HttpEntity<MessageRequestDTO> entity = new HttpEntity<>(sms, headers);

            final ResponseEntity<MessageResponseDTO> responseEntity = restTemplate.exchange(bmgProps.getBmgUrlSingle(), HttpMethod.POST, entity, MessageResponseDTO.class);

            final MessageResponseDTO responseDTO = responseEntity.getBody();

            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                responseDTO.setResponseText("Message to mobile [" + sms.getDestination() + "] was sent successfully");
                responseDTO.setResponseCode(HttpStatus.OK);
                log.info(responseDTO.getResponseText());
            } else {
                responseDTO.setResponseText("Failed to send message to mobile [" + responseDTO.getDestination() + "]");
                responseDTO.setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR);
                log.warn(responseDTO.getResponseText());
            }

            return responseDTO;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return smsResponse;
    }


    /*
     * Create Bean for Rest Template to auto wire the Rest Template object.
     * */
    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    /*
     * Utility Methods
     * */

    HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }
}

