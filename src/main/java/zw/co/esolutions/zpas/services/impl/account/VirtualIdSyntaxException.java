package zw.co.esolutions.zpas.services.impl.account;

/**
 * Created by alfred on 23 Apr 2019
 */
public class VirtualIdSyntaxException extends Exception {
    public VirtualIdSyntaxException() {
        this("Invalid Virtual Id supplied. Check syntax.");
    }

    public VirtualIdSyntaxException(String message) {
        super(message);
    }
}
