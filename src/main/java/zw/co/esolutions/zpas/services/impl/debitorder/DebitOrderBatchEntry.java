package zw.co.esolutions.zpas.services.impl.debitorder;

import com.opencsv.bean.CsvBindByName;
import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.enums.DebitOrderStatus;
import zw.co.esolutions.zpas.model.DebitOrderBatchItem;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Data
@Builder
public class DebitOrderBatchEntry {

    @CsvBindByName(column = "BankCode")
    protected String sourceBIC;

    @CsvBindByName(column = "NationalId")
    protected String nationalId;

    @CsvBindByName(column = "AccountNumber")
    protected String sourceAccount;

    @CsvBindByName(column = "Amount")
    protected String amount;

    @CsvBindByName(column = "Reference")
    protected String reference;

    @CsvBindByName(column = "ResponseCode")
    protected String responseCode;

    @CsvBindByName(column = "TransactionReference")
    protected String transactionReference;

    @CsvBindByName(column = "RecordType")
    protected String recordType;

    @CsvBindByName(column = "DestinationSortCode")
    protected String destinationSortCode;

    @CsvBindByName(column = "DestinationAccount")
    protected String destinationAccount;

    @CsvBindByName(column = "DestinationAccountType")
    protected String destinationAccountType;

    @CsvBindByName(column = "TransactionCode")
    protected String transactionCode;

    @CsvBindByName(column = "OriginSortCode")
    protected String originSortCode;

    @CsvBindByName(column = "OriginAccount")
    protected String originAccount;

    @CsvBindByName(column = "OriginAccountType")
    protected String originAccountType;

    @CsvBindByName(column = "ReferenceID")
    protected String referenceID;

    @CsvBindByName(column = "Name")
    protected String sourceName;

    @CsvBindByName(column = "DestinationName")
    protected String destinationName;

    @CsvBindByName(column = "ProcessingDate")
    protected String processingDate;

    @CsvBindByName(column = "UnpaidReason")
    protected String unpaidReason;

    @CsvBindByName(column = "Description")
    protected String narrative;

    @CsvBindByName(column = "AmountCurrencyCode")
    protected String amountCurrencyCode;

    @CsvBindByName(column = "OriginCurrencyRate")
    protected String originCurrencyRate ;

    @CsvBindByName(column = "Response")
    protected String response;

    @CsvBindByName(column = "RequestStatus")
    protected String requestStatus;

    public DebitOrderBatchEntry() {
    }

    public DebitOrderBatchEntry(String nationalId,String sourceBIC, String sourceAccount, String amount, String reference, String responseCode,
                                String transactionReference, String recordType, String destinationSortCode, String destinationAccount,
                                String destinationAccountType, String transactionCode, String originSortCode, String originAccount,
                                String originAccountType, String referenceID, String sourceName, String destinationName, String processingDate,
                                String unpaidReason, String narrative, String amountCurrencyCode, String originCurrencyRate,
                                String response, String requestStatus) {
        this.nationalId = nationalId;
        this.sourceBIC = sourceBIC;
        this.sourceAccount = sourceAccount;
        this.amount = amount;
        this.reference = reference;
        this.responseCode = responseCode;
        this.transactionReference = transactionReference;
        this.recordType = recordType;
        this.destinationSortCode = destinationSortCode;
        this.destinationAccount = destinationAccount;
        this.destinationAccountType = destinationAccountType;
        this.transactionCode = transactionCode;
        this.originSortCode = originSortCode;
        this.originAccount = originAccount;
        this.originAccountType = originAccountType;
        this.referenceID = referenceID;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
        this.processingDate = processingDate;
        this.unpaidReason = unpaidReason;
        this.narrative = narrative;
        this.amountCurrencyCode = amountCurrencyCode;
        this.originCurrencyRate = originCurrencyRate;
        this.response = response;
        this.requestStatus = requestStatus;
    }

    public DebitOrderBatchItem getDebitOrderBatchItem() {

        BigDecimal amount = new BigDecimal("0");
        OffsetDateTime processingDate = OffsetDateTime.now();

        try {
            //amount = PaymentTypeCode.valueOf(this.paymentTypeCode);
            if(this.amount != null) {
                amount = new BigDecimal(this.amount);
                amount = new BigDecimal(100).multiply(amount);
            }

            if(this.processingDate != null) {
                final LocalDate localDate = LocalDate.parse(this.processingDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                processingDate = LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
        }

        DebitOrderBatchItem debitOrderBatchItem = new DebitOrderBatchItem();
        debitOrderBatchItem.setId(GenerateKey.generateEntityId());
        debitOrderBatchItem.setStatus(EntityStatus.DRAFT.name());
        debitOrderBatchItem.setSourceBICFI(sourceBIC);
        debitOrderBatchItem.setNationalId(nationalId);
        debitOrderBatchItem.setSourceAccount(sourceAccount);
        debitOrderBatchItem.setSourceName(sourceName);
        debitOrderBatchItem.setAmount(amount);
        debitOrderBatchItem.setItemStatus(DebitOrderStatus.DRAFT);
        debitOrderBatchItem.setReference(RefGen.getReference("DOB"));
        debitOrderBatchItem.setResponse("");
        debitOrderBatchItem.setResponseCode("00");
        //debitOrderBatchItem.setTransactionReference("");
        debitOrderBatchItem.setRecordType("DEB");
        /*debitOrderBatchItem.setDestinationAccount("");
        debitOrderBatchItem.setDestinationAccountType("");*/
        debitOrderBatchItem.setTransactionCode("");
//        debitOrderBatchItem.setOriginSortCode(sourceBIC);
        debitOrderBatchItem.setOriginAccount(sourceAccount);
        debitOrderBatchItem.setOriginAccountType("");
        debitOrderBatchItem.setReferenceID(RefGen.getReference("R"));
        debitOrderBatchItem.setProcessingDate(processingDate);
        debitOrderBatchItem.setUnpaidReason("00");
        debitOrderBatchItem.setDescription(getApplicableValue(narrative));
        debitOrderBatchItem.setOriginCurrencyRate("");
        //debitOrderBatchItem.setRequestStatus("");

        return debitOrderBatchItem;
    }

    private String getApplicableValue(String value) {
        if(value == null || value.isEmpty() || value.equalsIgnoreCase("null")) {
            return "";
        }
        return value;
    }
}
