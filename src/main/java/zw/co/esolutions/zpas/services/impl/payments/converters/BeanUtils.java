package zw.co.esolutions.zpas.services.impl.payments.converters;
/*
 * Created on Jun 27, 2006
 *
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;


/**
 * @author Kumbi
 *  
 */
public class BeanUtils {

	private static final Logger log = LoggerFactory.getLogger(BeanUtils.class);


	/**
	 * @param rqDate
	 * @return
	 */
	public static Timestamp firstDayOfMonthTimestamp(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Timestamp(calendar.getTime().getTime());
	}
	
	public static int hoursBetween(Timestamp time1, Timestamp time2){
		long mili = time1.getTime() - time2.getTime();
		return (int)(mili/(1000.0*60.0*60.0));
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Timestamp lastDayOfMonthTimestamp(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.add(Calendar.MONTH, +1);
		calendar.set(Calendar.DATE, 1);
		calendar.add(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return new Timestamp(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Date firstDayOfMonthDate(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Date(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Date lastDayOfMonthDate(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.add(Calendar.MONTH, +1);
		calendar.set(Calendar.DATE, 1);
		calendar.add(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return new Date(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Timestamp dayStartTimestamp(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Timestamp(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Timestamp dayEndTimestamp(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return new Timestamp(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Date timestampToDate(Timestamp rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Date(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Date get30DaysBack(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.add(Calendar.DATE, -30);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Date(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Date get60DaysBack(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.add(Calendar.DATE, -60);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Date(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Date get90DaysBack(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.add(Calendar.DATE, -90);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Date(calendar.getTime().getTime());
	}

	/**
	 * @param rqDate
	 * @return
	 */
	public static Date get180DaysBack(Date rqDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(rqDate);
		calendar.add(Calendar.DATE, -180);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Date(calendar.getTime().getTime());
	}
	
	/**
	 * @param string
	 * @param length
	 * Pads the given string, filling in with spaces on the left side to lengthen the given string to length.
	 * Null strings are also padded. Strings that are longer than the given length causes truncation.
	 */
	public static String leftPad(String string, int length, char paddingChar) throws Exception {
		String tmp = string;
		tmp = (tmp==null)? "": tmp.trim();
		int stringLength = tmp.length();
		if (stringLength > length)
			tmp = tmp.substring(0, length);
			//throw new Exception("String '" + string + "' is too long to be padded to length '" + length + "'.");
		// do not reject padding if too long: just quietly truncate
		
		for (int i=stringLength; i<length; i++)
			tmp = paddingChar + tmp;
		
		return tmp;
	}

	/**
	 * @param string
	 * @param length
	 * Pads the given string, filling in with spaces on the right side to lengthen the given string to length.
	 * Null strings are also padded. Strings that are longer than the given length causes truncation.
	 */
	public static String rightPad(String string, int length, char paddingChar) throws Exception {
		String tmp = string;
		tmp = (tmp==null)? "": tmp.trim();
		int stringLength = tmp.length();
		if (stringLength > length)
			tmp = tmp.substring(0, length);
			// throw new Exception("String '" + string + "' is too long to be padded to length '" + length + "'.");
		// do not reject padding if too long: just quietly truncate
		
		for (int i=stringLength; i<length; i++)
			tmp = tmp + paddingChar;
		
		return tmp;
	}	
	
	
	/**
	 * Returns a date that represent the week and day of week provided.
	 * @param week
	 * @param dayOfWeek
	 * @return
	 */
	public static Date getDateFromWeekAndDayOfWeek(int week,int dayOfWeek){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.WEEK_OF_MONTH, week);
		cal.set(Calendar.DAY_OF_WEEK, dayOfWeek);
		return new Date(cal.getTimeInMillis());
	}
	
	public static Date getDateFromDateOfMonth(int dateOfMonth){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, dateOfMonth);
		return new Date(cal.getTimeInMillis());
	}
	
		
	 
	 public static int getFileID() {
		 return 34;
	 }
	 /**
	  * Given a SFIFileContents field, it generates the SFI file. It automatically overwrites the trailer
	  *  record.
	  * @param contents
	  * @throws Exception
	  */
	 public static void generateSFIFiles(SFIFileContents contents, String fileName) throws Exception {
		 BufferedWriter br = null;
		 try {
			 br = new BufferedWriter(new FileWriter(fileName));
			 // write header record
			 SFIHeaderRecord header = contents.getHeaderRecord();
			 br.write(header.getSFIHeaderID());
			 br.write(header.getSFIProcessingDate());
			 br.write(header.getSFISenderID());
			 br.write(header.getSFIReceiverID());
			 br.write(header.getSFIFileID());
			 br.write(header.getSFIWorkCode());
			 br.write(header.getSFIVersion());
			 br.write("\n");			 
			 // write the details records
			 java.util.Iterator iter = contents.getDetailRecords().iterator();
			 SFIDetailRecord sdr;
			 while (iter.hasNext()) {
				 sdr = (SFIDetailRecord)iter.next();
				 br.write(sdr.getSFIRecordType());
				 br.write(sdr.getSFIDestinationSortCode());
				 br.write(sdr.getSFIDestinationAccount());
				 br.write(sdr.getSFIDestinationAccountType());
				 br.write(sdr.getSFITransactionCode());
				 br.write(sdr.getSFIOriginSortCode());
				 br.write(sdr.getSFIOriginAccount());
				 br.write(sdr.getSFIOriginAccountType());
				 br.write(sdr.getSFIReferenceID());
				 br.write(sdr.getSFIDestinationName());
				 br.write(sdr.getSFIAmount());
				 br.write(sdr.getSFIProcessingDate());
				 br.write(sdr.getSFIUnpaidReason());
				 // for version 2, do not put narrative
				 // br.write(sdr.getSFINarrative());
				 br.write("\n");
			 }
			 // re-set the trailer record
			 contents.resetTrailerRecord();
			 SFITrailerRecord trailer = contents.getTrailerRecord();
			 // write the trailer record
			 br.write(trailer.getSFITrailerId());
			 br.write(trailer.getSFITotalDebitValue());
			 br.write(trailer.getSFITotalCreditValue());
			 br.write(trailer.getSFIDebitItemsCount());
			 br.write(trailer.getSFICreditItemsCount());
			 br.write("\n");
		 }
		 catch (Exception e) {
			 throw new Exception(e);
		 }
		 finally {
			 try {
				if (br != null) br.close(); 
			 }
			 catch (Exception e) {
			 }
		 }
	 }
	 /**
	  * Given a SFI file, it parses it and returns a SFIFileContents object
	  * @throws Exception
	  */
	 public static SFIFileContents parseSFIFile(String fileName) throws Exception {
		 BufferedReader in = null;
         int count = 0;
         int index = 0;
         try {
	         in = new BufferedReader(new FileReader(fileName));
	         String line = "";
	         SFIHeaderRecord shr = new SFIHeaderRecord();
	         Collection<SFIDetailRecord> sdrCol = new ArrayList<SFIDetailRecord>();
	         SFIDetailRecord sdr;
	         SFITrailerRecord str = new SFITrailerRecord();
	         String tmpStr;
	         int tmpInt;
	         OffsetDateTime tmpDate;
	         long tmpDouble;
	         while ((line = in.readLine()) != null) {
	        	 count ++;
	        	 if (count == 1) {
	        		 // we are processing header - make sure we are at line 1, otherwise exit!
	        		 if (line.startsWith(SFIHeaderRecord.HEADERID_DEFAULT)) {
	        			index = 0;
	        			// do header ID
	        			tmpStr = line.substring(index, index+SFIHeaderRecord.HEADERID_FIELD_LENGTH);
	        			shr.setHeaderID(tmpStr);
	        			index = index + SFIHeaderRecord.HEADERID_FIELD_LENGTH;
	        			// do date
	        			tmpStr = line.substring(index, index+SFIHeaderRecord.PROCESSINGDATE_FIELD_LENGTH);
	        			tmpDate = LocalDate.parse(tmpStr, Formats.SFIDateFormat).atTime(OffsetTime.now());
	        			shr.setProcessingDate(tmpDate.toInstant().atOffset(ZoneOffset.UTC));
	        			index = index + SFIHeaderRecord.PROCESSINGDATE_FIELD_LENGTH;
	        			// do sender id
	        			tmpStr = line.substring(index, index+SFIHeaderRecord.SENDERID_FIELD_LENGTH);
	        			shr.setSenderID(tmpStr);
	        			index = index + SFIHeaderRecord.SENDERID_FIELD_LENGTH;
	        			// do receiver id
	        			tmpStr = line.substring(index, index+SFIHeaderRecord.RECEIVERID_FIELD_LENGTH);
	        			shr.setReceiverID(tmpStr);
	        			index = index + SFIHeaderRecord.RECEIVERID_FIELD_LENGTH;
	        			// do file id
	        			tmpStr = line.substring(index, index+SFIHeaderRecord.FILEID_FIELD_LENGTH);
	        			shr.setFileID(tmpStr);
	        			index = index + SFIHeaderRecord.FILEID_FIELD_LENGTH;
	        			// do work code
	        			tmpStr = line.substring(index, index+SFIHeaderRecord.WORKCODE_FIELD_LENGTH);
	        			shr.setWorkCode(tmpStr);
	        			index = index + SFIHeaderRecord.WORKCODE_FIELD_LENGTH;
	        			// do version
	        			tmpStr = line.substring(index, index+SFIHeaderRecord.VERSION_FIELD_LENGTH);
	        			shr.setVersion(tmpStr);
	        			index = index + SFIHeaderRecord.VERSION_FIELD_LENGTH;
	        			// do sender name
	        			tmpStr = line.substring(index, index+SFIHeaderRecord.SENDER_NAME_FIELD_LENGTH);
	        			shr.setSenderName(tmpStr);
	        			index = index + SFIHeaderRecord.SENDER_NAME_FIELD_LENGTH;
	        		 }
	        		 else {
	        			 throw new Exception("Invalid SFI file. First record is not a header record.");
	        		 }
	        	 }
	        	 else {
	        		 if (line.startsWith(SFITrailerRecord.TRAILERID_DEFAULT)) {
	        			 // we are now processing trailer record
	         			index = 0;
	        			// do trailer ID
	        			log.debug("t1. " + index+ "-" + (0+index+SFITrailerRecord.TRAILERID_FIELD_LENGTH));
	        			tmpStr = line.substring(index, index+SFITrailerRecord.TRAILERID_FIELD_LENGTH);
	        			str.setTrailerId(tmpStr);
	        			index = index + SFITrailerRecord.TRAILERID_FIELD_LENGTH;
	        			log.debug("T1. " + tmpStr);
	        			//me
	        			
	        			// do currency code
                         log.debug("t2. " + index+ "-" + (0+index+SFITrailerRecord.CURRENCY_CODE_LENGTH));
	        			tmpStr = line.substring(index, index+SFITrailerRecord.CURRENCY_CODE_LENGTH);
	        			str.setCurrencyCode(tmpStr);
	        			index = index + SFITrailerRecord.CURRENCY_CODE_LENGTH;
                         log.debug("T2. => CURRENCY " + tmpStr);

	        			// do total debit value
	        			System.out.println("t3. " + index+ "-" + (0+index+SFITrailerRecord.TOTALDEBITVALUE_FIELD_LENGTH));
	        			tmpStr = line.substring(index, index+SFITrailerRecord.TOTALDEBITVALUE_FIELD_LENGTH).replace(' ', '0');
	        			tmpDouble = Long.parseLong(tmpStr);
	        			str.setTotalDebitValue(tmpDouble);
	        			index = index + SFITrailerRecord.TOTALDEBITVALUE_FIELD_LENGTH;
	        			System.out.println("T3. => TOTAL DEBIT " + tmpStr);

	        			// do total credit value
	        			System.out.println("t4. " + index+ "-" + (0+index+SFITrailerRecord.TOTALCREDITVALUE_FIELD_LENGTH));
	        			tmpStr = line.substring(index, index+SFITrailerRecord.TOTALCREDITVALUE_FIELD_LENGTH);
	        			tmpDouble = Long.parseLong(tmpStr);
	        			str.setTotalCreditValue(tmpDouble);
	        			index = index + SFITrailerRecord.TOTALCREDITVALUE_FIELD_LENGTH;
	        			System.out.println("T4. => TOTAL CREDIT " + tmpStr);

	        			// do total debit items
	        			System.out.println("t5. " + index+ "-" + (0+index+SFITrailerRecord.DEBITITEMSCOUNT_FIELD_LENGTH));
	        			tmpStr = line.substring(index, index+SFITrailerRecord.DEBITITEMSCOUNT_FIELD_LENGTH);
	        			tmpInt = Integer.parseInt(tmpStr);
	        			str.setDebitItemsCount(tmpInt);
	        			index = index + SFITrailerRecord.DEBITITEMSCOUNT_FIELD_LENGTH;
	        			System.out.println("T5. " + tmpStr);

	        			// do total credit items
	        			System.out.println("t6. " + index+ "-" + (0+index+SFITrailerRecord.CREDITITEMSCOUNT_FIELD_LENGTH));
	        			tmpStr = line.substring(index, index+SFITrailerRecord.CREDITITEMSCOUNT_FIELD_LENGTH);
	        			tmpInt = Integer.parseInt(tmpStr);
	        			str.setCreditItemsCount(tmpInt);
	        			index = index + SFITrailerRecord.CREDITITEMSCOUNT_FIELD_LENGTH;
	        			System.out.println("T6. " + tmpStr);
/*
	        			// do check sum
	        			tmpStr = line.substring(index, index+SFITrailerRecord.CHECKSUM_LENGTH);
	        			tmpInt = Integer.parseInt(tmpStr);
	        			str.setCreditItemsCount(tmpInt);
	        			index = index + SFITrailerRecord.CHECKSUM_LENGTH;
	        			System.out.println("T7. " + tmpStr);
*/
	        		 }
	        		 else {
	        			// we processing a detail record
	        			sdr = new SFIDetailRecord();
	        			index = 0;
	         			// do record type ID
	        			System.out.println("1. " + index+ "-" + (0+index+SFIDetailRecord.RECORDTYPE_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.RECORDTYPE_FIELD_LENGTH);
	         			sdr.setRecordType(tmpStr);
	         			System.out.println("Temp 1 " + tmpStr);
	         			index = index + SFIDetailRecord.RECORDTYPE_FIELD_LENGTH;
	         			// do dest sort code
	         			System.out.println("2. " + index+ "-" + (0+index+SFIDetailRecord.DESTINATIONSORTCODE_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.DESTINATIONSORTCODE_FIELD_LENGTH);
	         			sdr.setDestinationSortCode(tmpStr);
	         			System.out.println("Temp 2 " + tmpStr);
	         			index = index + SFIDetailRecord.DESTINATIONSORTCODE_FIELD_LENGTH;
	         			// do dest acc
	         			System.out.println("3. " + index+ "-" + (0+index+SFIDetailRecord.DESTINATIONACCOUNT_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.DESTINATIONACCOUNT_FIELD_LENGTH);
	         			sdr.setDestinationAccount(tmpStr.trim());
	         			System.out.println("Temp 3 " + tmpStr);
	         			index = index + SFIDetailRecord.DESTINATIONACCOUNT_FIELD_LENGTH;
	         			// dest acc type
	         			System.out.println("4. " + index+ "-" + (0+index+SFIDetailRecord.DESTINATIONACCOUNTTYPE_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.DESTINATIONACCOUNTTYPE_FIELD_LENGTH);
	         			sdr.setDestinationAccountType(tmpStr);
	         			System.out.println("Temp 4 " + tmpStr);
	         			index = index + SFIDetailRecord.DESTINATIONACCOUNTTYPE_FIELD_LENGTH;
	         		
	         			
	         		//me	
	         			
	         		// dest currency code
	         			System.out.println("5. " + index+ "-" + (0+index+SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH);
	         			sdr.setAmountCurrencyCode(tmpStr);
	         			index = index + SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH;
	         			System.out.println("Temp 5 " + tmpStr);
	         			
	         		// origin currency rate
	         			System.out.println("6. " + index+ "-" + (0+index+SFIDetailRecord.ORIGIN_CURRENCY_RATE));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.ORIGIN_CURRENCY_RATE);
	         			sdr.setAmountCurrencyCode(tmpStr);
	         			index = index + SFIDetailRecord.ORIGIN_CURRENCY_RATE;
	         			System.out.println("Temp 6 " + tmpStr);
	         			
	         			
	         			
	         			// do txn code
	         			System.out.println("7. " + index+ "-" + (0+index+SFIDetailRecord.TRANSACTIONCODE_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.TRANSACTIONCODE_FIELD_LENGTH);
	         			sdr.setTransactionCode(tmpStr);
	         			index = index + SFIDetailRecord.TRANSACTIONCODE_FIELD_LENGTH;
	         			System.out.println("Temp 7 " + tmpStr);
	         			// do orig sort code
	         			System.out.println("8. " + index+ "-" + (0+index+SFIDetailRecord.ORIGINSORTCODE_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.ORIGINSORTCODE_FIELD_LENGTH);
	         			sdr.setOriginSortCode(tmpStr);
	         			index = index + SFIDetailRecord.ORIGINSORTCODE_FIELD_LENGTH;
	         			System.out.println("Temp 8 " + tmpStr);
	         			// do orig account
	         			System.out.println("9. " + index+ "-" + (0+index+SFIDetailRecord.ORIGINACCOUNT_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.ORIGINACCOUNT_FIELD_LENGTH);
	         			sdr.setOriginAccount(tmpStr.trim());
	         			index = index + SFIDetailRecord.ORIGINACCOUNT_FIELD_LENGTH;
	         			System.out.println("Temp 9 " + tmpStr);
	         			// do orig acc type
	         			System.out.println("10. " + index+ "-" + (0+index+SFIDetailRecord.ORIGINACCOUNTTYPE_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.ORIGINACCOUNTTYPE_FIELD_LENGTH);
	         			sdr.setOriginAccountType(tmpStr);
	         			index = index + SFIDetailRecord.ORIGINACCOUNTTYPE_FIELD_LENGTH;
	         			System.out.println("Temp 10 " + tmpStr);
	         			
	         			//me 
	         			
	         		// do orig acc currency
	         			System.out.println("11. " + index+ "-" + (0+index+SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH);
	         			sdr.setAmountCurrencyCode(tmpStr);
	         			index = index + SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH;
	         			System.out.println("Temp 11 " + tmpStr);
	         			
	         		// origin currency rate
	         			System.out.println("12. " + index+ "-" + (0+index+SFIDetailRecord.ORIGIN_CURRENCY_RATE));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.ORIGIN_CURRENCY_RATE);
	         			sdr.setOriginCurrencyRate(tmpStr);
	         			index = index + SFIDetailRecord.ORIGIN_CURRENCY_RATE;
	         			System.out.println("Temp 12 " + tmpStr);
	         			
	         		//	end of me
	         			
	         			
	         			
	         			// do ref
	         			System.out.println("13. " + index+ "-" + (0+index+SFIDetailRecord.REFERENCEID_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.REFERENCEID_FIELD_LENGTH);
	         			sdr.setReferenceID(tmpStr);
	         			index = index + SFIDetailRecord.REFERENCEID_FIELD_LENGTH;
	         			System.out.println("Temp 13 " + tmpStr);
	         			// do dest name
	         			System.out.println("14. " + index+ "-" + (0+index+SFIDetailRecord.DESTINATIONNAME_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.DESTINATIONNAME_FIELD_LENGTH);
	         			sdr.setDestinationName(tmpStr);
	         			index = index + SFIDetailRecord.DESTINATIONNAME_FIELD_LENGTH;
	         			System.out.println("Temp 14 " + tmpStr);

	         			// do amount
	         			System.out.println("15. " + index+ "-" + (0+index+SFIDetailRecord.AMOUNT_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.AMOUNT_FIELD_LENGTH);
	         			tmpDouble = Long.parseLong(tmpStr);
	         			sdr.setAmount(tmpDouble);
	         			index = index + SFIDetailRecord.AMOUNT_FIELD_LENGTH;
	         			System.out.println("Temp 15 " + tmpStr);
	         			
	         			//me
	         			
	         			
	         		// do amount currency
//	         			System.out.println("16. " + index+ "-" + (0+index+SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH));
//	         			tmpStr = line.substring(index, index+SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH);
//	         			sdr.setAmountCurrencyCode(tmpStr);
//	         			index = index + SFIDetailRecord.DESTINATIONACCOUNT_CURRENCY_CODE_LENGTH;
//	         			System.out.println("Temp 16 " + tmpStr);
	         			
	         			
	         			
	         			// do processing date
                         System.out.println("16. " + index+ "-" + (0+index+SFIDetailRecord.PROCESSINGDATE_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.PROCESSINGDATE_FIELD_LENGTH);
	         			tmpDate = LocalDate.parse(tmpStr, Formats.SFIDateFormat).atTime(OffsetTime.now());
	         			sdr.setProcessingDate(tmpDate.toInstant().atOffset(ZoneOffset.UTC));
	         			index = index + SFIDetailRecord.PROCESSINGDATE_FIELD_LENGTH;
                         System.out.println("Temp 16 " + tmpStr);
	         			// do unpaid reason
                         System.out.println("17. " + index+ "-" + (0+index+SFIDetailRecord.UNPAIDREASON_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.UNPAIDREASON_FIELD_LENGTH);
	         			sdr.setUnpaidReason(tmpStr);
	         			index = index + SFIDetailRecord.UNPAIDREASON_FIELD_LENGTH;
                         System.out.println("Temp 17 " + tmpStr);
	         			// do narrative
	         			// for version 2, do not read Narrative

						 System.out.println("18. " + index+ "-" + (0+index+SFIDetailRecord.NARRATIVE_FIELD_LENGTH));
	         			tmpStr = line.substring(index, index+SFIDetailRecord.NARRATIVE_FIELD_LENGTH);
	         			sdr.setNarrative(tmpStr);
	         			index = index + SFIDetailRecord.NARRATIVE_FIELD_LENGTH;
						 System.out.println("Temp 18 " + tmpStr);

	         			// done with a detail record - put it into the details col
	         			sdrCol.add(sdr);
	        		 }
	        	 }
	         }
	         // done collecting all records - now create the file contents record
	         SFIFileContents sfc = new SFIFileContents(shr, sdrCol, str);
	         // validate it!
	         Object[] oa = sfc.validate();
	         boolean valid = ((Boolean)oa[0]).booleanValue();
	         if (!valid)
	        	 throw new Exception("File does not have valid contents: " + oa[1]);
	         // if valid, just return it
	         return sfc;
         }
         catch (Exception e) {
        	 System.err.println("Error occured while parsing file. @ line '" + count + "', field index '" + index + "'.");
        	 e.printStackTrace();
        	 throw new Exception(e);
         }
         finally {
        	 try {
        		 if (in != null) in.close();
        	 }
        	 catch(Exception e) {
        	 }
         }
	 }

        /**
         * @param rqDate
         * @return
         */
        public static Timestamp timestampExpiration(Timestamp rqDate, int numMinutes) {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(rqDate);
            calendar.add(Calendar.MINUTE, numMinutes);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            return new Timestamp(calendar.getTime().getTime());
        }

        /**
         * Lazy-Man's-Method(tm). Given a date, it returns the date x years earlier.
         * 
         * @return
         */
        public static Date getDateYearsEarlier(Date dToday, int num) {
            if (dToday == null) {
                return null;
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(dToday);
            cal.add(Calendar.YEAR, -num);
            return new Date(cal.getTime().getTime());
        }

        /**
         * @param rqDate
         * @return
         */
        public static Timestamp secondBeforeDateTimestamp(Date rqDate) {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(rqDate);
            calendar.add(Calendar.SECOND, -1);
            return new Timestamp(calendar.getTime().getTime());
        }

        /**
         * @param date
         * @return
         */
        public static String getYear(Timestamp date) {
            String year = "";
            if (date == null) {
                return null;
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            year = "" + cal.get(Calendar.YEAR);
            return year;
        }

        /**
         * @param date
         * @return
         */
        public static String getMonth(Timestamp date) {
            String month = "";
            if (date == null) {
                return null;
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            month = "" + Formats.twoDigitIntFormat.format(cal.get(Calendar.MONTH) + 1);
            return month;
        }

	 
	 /**
	  * Given a bank sort, returns the 5-digit bank sort code 
	  * @throws Exception
	  */
	 public static String getFiveDigitBankSortCode(String bankSortCode) throws Exception {
		try {
			int length = bankSortCode.length();
			return bankSortCode.substring(length-5, length);
		}
		catch (Exception e) {
			throw e;
		}
	 }
    /**
     * Checks whether the 2 dates are equal - year, month, and day. If both are null, they are also equal
     */
    public static boolean datesEqual(java.util.Date date1, java.util.Date date2) {
        if (date1 == null && date2 == null)
            return true;
        if ((date1 == null && date2 != null) || (date1 != null && date2 == null))
            return false;
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        if ((cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE)) && (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH))
                && (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)))
            return true;
        else
            return false;
    }
    
    public static String toStringOfLength(String string, int length, String preFillChar) {
    	if (string == null) {
    		string = "";
    	}
    	String result = string;    	
    	
    	int len = string.length();
    	if (len < length) {
    		for (int i=0; i<length-len; i++) {
    			result = preFillChar + result;
    		}
    	}
    	else {
    		result = string.substring(len-length,len);
    	}
    	
    	return result;
    }
    
    public static String toStringOfLengthPadRight(String string, int length, String rightFillChar) {
    	if (string == null) {
    		string = "";
    	}
    	String result = string;    	
    	
    	int len = string.length();
    	if (len < length) {
    		for (int i=0; i<length-len; i++) {
    			result = result + rightFillChar;
    		}
    	}
    	else {
    		result = string.substring(0,length);
    	}
    	
    	return result;
    }
    
    public static void main(String args []) throws Exception{
    	
   	try {	
   			String oldFile = "/data/Projects/2017-Projects/paynet/paynetfile.txt";
   			String newFile = "/data/Projects/2018-Projects/paynet/PWallet_05507489.txt";
    		SFIFileContents sfiFileContents = parseSFIFile(newFile);
    	//	SFIFileContents sfiFileContents = parseSFIFile("/data/Projects/2017-Projects/paynet/paynet_files_matinya/PM319904");
    		System.out.println("----"+sfiFileContents.getDetailRecords().size());
    		if (sfiFileContents!= null && sfiFileContents.getDetailRecords().size()>0) {
    			
    			for (SFIDetailRecord detailRecord : sfiFileContents.getDetailRecords()) {
					System.out.println(sfiFileContents.getHeaderRecord().getFileID()+ "Destination Account"+detailRecord.getDestinationAccount()+ " Amount " +detailRecord.getAmount());
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
  	
    	int index =0;
    	String line= "UTLUSD000000                  000000000000000000005896000000000001";
    	line = line.replace(' ', '0');
   // 	 UTL   0004000 004157128840200            00 USD0000 00000000000000000000     
   // 	 0-3   3-10    10-30                      30-32
   // 	String tmpStr = line.substring(0, 3);
  // 	System.out.println(line);
    }
}