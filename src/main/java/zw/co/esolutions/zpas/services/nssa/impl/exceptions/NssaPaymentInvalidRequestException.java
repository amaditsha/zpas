package zw.co.esolutions.zpas.services.nssa.impl.exceptions;

/**
 * Created by alfred on 29 Apr 2019
 */
public class NssaPaymentInvalidRequestException extends Exception {
    public NssaPaymentInvalidRequestException(String message) {
        super(message);
    }
}
