package zw.co.esolutions.zpas.services.nssa.impl.receipt;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleDTO;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by alfred on 15 Mar 2019
 */
@Slf4j
@Component
public class NssaReceiptPDFGenerator {

    public static void main(String[] args) {

        ContributionScheduleDTO contributionSchedule = new ContributionScheduleDTO();
        contributionSchedule.setDateCreated(OffsetDateTime.now());
        contributionSchedule.setLastUpdated(OffsetDateTime.now());
        contributionSchedule.setReceiptNumber("H1279298");
        final CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();
        currencyAndAmount.setAmount(new BigDecimal("0"));
        final CurrencyCode currencyCode = new CurrencyCode();
        currencyCode.setCodeName("ZWL");
        currencyCode.setName("ZWL");
        currencyAndAmount.setCurrency(currencyCode);
        contributionSchedule.setAmount(currencyAndAmount);
        contributionSchedule.setPaymentReference("PayRef");
        contributionSchedule.setMonth(OffsetDateTime.now());
        new NssaReceiptPDFGenerator().generateDocument(contributionSchedule);
    }

    public Path generateDocument(ContributionScheduleDTO contributionSchedule) {
        Document document = new Document(PageSize.A4.rotate());
        final String clientName = contributionSchedule.getEmployer().getName().replace(' ', '_');
        String receiptFileName = clientName + '_' + contributionSchedule.getReceiptNumber() + ".pdf";
        final Path fileLocation = Paths.get(StorageProperties.BASE_LOCATION).resolve(receiptFileName);
        try {
            PdfWriter.getInstance(document, new FileOutputStream(fileLocation.toFile()));

            document.open();

            //Set attributes here
            document.addAuthor("ZEEPAY-NSSA System");
            document.addCreationDate();
            document.addCreator("ZEEPAY-NSSA");
            document.addTitle("Receipt");
            document.addSubject("Receipt for NSSA Payment.");

            Path path = Paths.get(ClassLoader.getSystemResource("static/images/nssa_logo.png").toURI());
            Image img = Image.getInstance(path.toAbsolutePath().toString());
            img.scalePercent(30);
            img.setAbsolutePosition(20f, 515f);
            document.add(img);

            addHeading(document, contributionSchedule);
            addInvoiceTotals(document, contributionSchedule);
            addRemittanceTable(document, contributionSchedule);
            addTotals(document, contributionSchedule);

            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return fileLocation;
    }

    private void addHeading(Document document, ContributionScheduleDTO contributionSchedule) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        //Paragraph with color and font styles
        Paragraph paragraph = new Paragraph("", blackFont);
        paragraph.add(new Chunk("RECEIPT", blackFont));

        PdfPTable table = new PdfPTable(3); // 4 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f};
        table.setWidths(columnWidths);

        PdfPCell cell1 = new PdfPCell(new Paragraph(""));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setPaddingLeft(10);
        cell1.setPaddingBottom(10);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell2 = new PdfPCell(paragraph);
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setPaddingLeft(10);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell3 = new PdfPCell(new Paragraph(RefGen.getReference("H")));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setPaddingLeft(10);
        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);

        document.add(table);
    }

    private void addInvoiceTotals(Document document, ContributionScheduleDTO contributionSchedule) throws DocumentException {
        Font blackBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10, Font.NORMAL, new CMYKColor(0, 0, 0, 255));
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        PdfPTable table = new PdfPTable(3); // 3 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(1f); //Space before table
        table.setSpacingAfter(1f); //Space after table



        final PdfPCell pdfPSpacerCell1 = new PdfPCell(new Paragraph(Chunk.NEWLINE));
        pdfPSpacerCell1.setBorderColor(BaseColor.WHITE);
        pdfPSpacerCell1.setPaddingLeft(1);
        pdfPSpacerCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfPSpacerCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        final PdfPCell pdfPSpacerCell2 = new PdfPCell(new Paragraph(Chunk.NEWLINE));
        pdfPSpacerCell2.setBorderColor(BaseColor.WHITE);
        pdfPSpacerCell2.setPaddingLeft(1);
        pdfPSpacerCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfPSpacerCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        final PdfPCell pdfPSpacerCell3 = new PdfPCell(new Paragraph(Chunk.NEWLINE));
        pdfPSpacerCell3.setBorderColor(BaseColor.WHITE);
        pdfPSpacerCell3.setPaddingLeft(1);
        pdfPSpacerCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfPSpacerCell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        table.addCell(pdfPSpacerCell1);
        table.addCell(pdfPSpacerCell2);
        table.addCell(pdfPSpacerCell3);

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f};
        table.setWidths(columnWidths);


        Paragraph addressParagraph = new Paragraph("", blackFont);
        addressParagraph.setSpacingAfter(100);
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(new Chunk("National Social Security Authority"));
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(new Chunk("Selous Ave/ Sam Nujoma"));
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(new Chunk("P.O. Box CY 1387"));
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(new Chunk("Causeway, Harare"));
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(new Chunk("Tel: +263 242 799033 /44, 762711, 737372"));
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(Chunk.NEWLINE);
        addressParagraph.add(new Chunk("Fax: +263 242 799034"));


        Paragraph bpnSsrParagraph = new Paragraph("", blackFont);
        bpnSsrParagraph.setSpacingAfter(100);
        bpnSsrParagraph.add(Chunk.NEWLINE);
        bpnSsrParagraph.add(Chunk.NEWLINE);
        bpnSsrParagraph.add(new Chunk("Business Partner Number: ", blackBoldFont));
        bpnSsrParagraph.add(new Chunk(contributionSchedule.getEmployer().getBusinessPartnerNumber()));
        bpnSsrParagraph.add(Chunk.NEWLINE);
        bpnSsrParagraph.add(Chunk.NEWLINE);
        bpnSsrParagraph.add(new Chunk("SSR: ", blackBoldFont));
        bpnSsrParagraph.add(new Chunk(contributionSchedule.getEmployer().getSsr()));
        bpnSsrParagraph.add(Chunk.NEWLINE);
        bpnSsrParagraph.add(Chunk.NEWLINE);
        bpnSsrParagraph.add(new Chunk(contributionSchedule.getEmployer().getPhysicalAddress()));


        Paragraph receiptInfoParagraph = new Paragraph("", blackFont);
        receiptInfoParagraph.setSpacingAfter(100);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(new Chunk("Receipt Number: ", blackBoldFont));
        receiptInfoParagraph.add(new Chunk(contributionSchedule.getReceiptNumber()));
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(new Chunk("Payment/Outgoing: ", blackBoldFont));
        receiptInfoParagraph.add(new Chunk("Payment"));
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(new Chunk("Payment Doc: ", blackBoldFont));
        receiptInfoParagraph.add(new Chunk(contributionSchedule.getReceiptNumber()));
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(new Chunk("Date of Entry: ", blackBoldFont));
        receiptInfoParagraph.add(new Chunk(contributionSchedule.getLastUpdated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(new Chunk("Time of Entry: ", blackBoldFont));
        receiptInfoParagraph.add(new Chunk(contributionSchedule.getLastUpdated().format(DateTimeFormatter.ofPattern("HH:mm"))));
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(new Chunk("Posting Date: ", blackBoldFont));
        receiptInfoParagraph.add(new Chunk(contributionSchedule.getDateCreated().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(new Chunk("Value Date: ", blackBoldFont));
        receiptInfoParagraph.add(new Chunk(contributionSchedule.getLastUpdated().format(DateTimeFormatter.ofPattern("HH:mm"))));
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(Chunk.NEWLINE);
        receiptInfoParagraph.add(new Chunk("Cashier: ", blackBoldFont));
        receiptInfoParagraph.add(new Chunk("NSSA"));

        //Invoice Value Section
        PdfPCell cell1 = new PdfPCell(addressParagraph);
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setPaddingLeft(1);
        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell1.setVerticalAlignment(Element.ALIGN_TOP);

        PdfPCell cell2 = new PdfPCell(bpnSsrParagraph);
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setPaddingLeft(1);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell2.setVerticalAlignment(Element.ALIGN_TOP);

        PdfPCell cell3 = new PdfPCell(receiptInfoParagraph);
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setPaddingLeft(1);
        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell3.setVerticalAlignment(Element.ALIGN_TOP);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);

        //Invoice Charges Section

        document.add(table);
    }

    private void addTotals(Document document, ContributionScheduleDTO contributionSchedule) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        Paragraph paragraphDiscountValueHeader = new Paragraph("-Payment Total", blackFont);

        //Invoice Discount Section
        PdfPCell cell7 = new PdfPCell(new Paragraph(""));
        cell7.setBorder(PdfPCell.NO_BORDER);
        cell7.setPaddingLeft(10);
        cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell8 = new PdfPCell(paragraphDiscountValueHeader);
        cell8.setBorder(PdfPCell.NO_BORDER);
        cell8.setPaddingLeft(10);
        cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell9 = new PdfPCell(new Paragraph(contributionSchedule.getAmount().toString()));
        cell9.setBorder(PdfPCell.NO_BORDER);
        cell9.setPaddingLeft(10);
        cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell7.setUseBorderPadding(true);
        cell8.setUseBorderPadding(true);
        cell9.setUseBorderPadding(true);


        PdfPTable table = new PdfPTable(3); // 3 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f};
        table.setWidths(columnWidths);

        table.addCell(cell7);
        table.addCell(cell8);
        table.addCell(cell9);

        document.add(table);
    }

    private void addRemittanceTable(Document document, ContributionScheduleDTO contributionSchedule) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        PdfPTable table = new PdfPTable(4); // 4 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);

        PdfPCell cell1 = new PdfPCell(new Paragraph("Transaction Type", blackFont));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell1.setPaddingLeft(10);
        cell1.setPaddingBottom(10);
        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell2 = new PdfPCell(new Paragraph("RTGS - RTGS", blackFont));
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell2.setPaddingLeft(10);
        cell2.setPaddingBottom(10);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell3 = new PdfPCell(new Paragraph("Printed On:", blackFont));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell3.setPaddingLeft(10);
        cell3.setPaddingBottom(10);
        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell4 = new PdfPCell(new Paragraph(OffsetDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss a")), blackFont));
        cell4.setBorder(PdfPCell.NO_BORDER);
        cell4.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell4.setPaddingLeft(10);
        cell4.setPaddingBottom(10);
        cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);
        cell4.setUseBorderPadding(true);







        final PdfPCell pdfPSpacerCell1 = new PdfPCell(new Paragraph(Chunk.NEWLINE));
        pdfPSpacerCell1.setBorderColor(BaseColor.WHITE);
        pdfPSpacerCell1.setPaddingLeft(1);
//        pdfPSpacerCell1.setBorder(PdfPCell.NO_BORDER);
        pdfPSpacerCell1.setBorder(PdfPCell.BOTTOM);
        pdfPSpacerCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfPSpacerCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);





        PdfPCell cell5 = new PdfPCell(new Paragraph("", blackFont));
        cell5.setBorder(PdfPCell.NO_BORDER);
        cell5.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell5.setPaddingLeft(10);
        cell5.setPaddingBottom(10);
        cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell6 = new PdfPCell(new Paragraph("POBS", blackFont));
        cell6.setBorder(PdfPCell.NO_BORDER);
        cell6.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell6.setPaddingLeft(10);
        cell6.setPaddingBottom(10);
        cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell7 = new PdfPCell(new Paragraph("10012895", blackFont));
        cell7.setBorder(PdfPCell.NO_BORDER);
        cell7.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell7.setPaddingLeft(10);
        cell7.setPaddingBottom(10);
        cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell8 = new PdfPCell(new Paragraph("ZWL 0.00"));
        cell8.setBorder(PdfPCell.NO_BORDER);
        cell8.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell8.setPaddingLeft(10);
        cell8.setPaddingBottom(10);
        cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell5.setUseBorderPadding(true);
        cell6.setUseBorderPadding(true);
        cell7.setUseBorderPadding(true);
        cell8.setUseBorderPadding(true);






        PdfPCell cell9 = new PdfPCell(new Paragraph("", blackFont));
        cell9.setBorder(PdfPCell.NO_BORDER);
        cell9.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell9.setPaddingLeft(10);
        cell9.setPaddingBottom(10);
        cell9.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell10 = new PdfPCell(new Paragraph("APWCS", blackFont));
        cell10.setBorder(PdfPCell.NO_BORDER);
        cell10.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell10.setPaddingLeft(10);
        cell10.setPaddingBottom(10);
        cell10.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell10.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell11 = new PdfPCell(new Paragraph("20012895", blackFont));
        cell11.setBorder(PdfPCell.NO_BORDER);
        cell11.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell11.setPaddingLeft(10);
        cell11.setPaddingBottom(10);
        cell11.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell12 = new PdfPCell(new Paragraph(contributionSchedule.getAmount().toString(), blackFont));
        cell12.setBorder(PdfPCell.NO_BORDER);
        cell12.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell12.setPaddingLeft(10);
        cell12.setPaddingBottom(10);
        cell12.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell12.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell9.setUseBorderPadding(true);
        cell10.setUseBorderPadding(true);
        cell11.setUseBorderPadding(true);
        cell12.setUseBorderPadding(true);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);



        table.addCell(pdfPSpacerCell1);
        table.addCell(pdfPSpacerCell1);
        table.addCell(pdfPSpacerCell1);
        table.addCell(pdfPSpacerCell1);

        table.addCell(cell5);
        table.addCell(cell6);
        table.addCell(cell7);
        table.addCell(cell8);
        table.addCell(cell9);
        table.addCell(cell10);
        table.addCell(cell11);
        table.addCell(cell12);

        document.add(table);
    }
}
