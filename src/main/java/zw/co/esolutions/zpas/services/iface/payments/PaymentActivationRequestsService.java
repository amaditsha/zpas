package zw.co.esolutions.zpas.services.iface.payments;

import zw.co.esolutions.zpas.model.PaymentObligation;

/**
 * Created by alfred on 24 Apr 2019
 */
public interface PaymentActivationRequestsService {
    String receivePaymentActivationRequestMessage(String fileName);
    String sendPaymentActivationRequestStatusReport(PaymentObligation paymentObligation);
}
