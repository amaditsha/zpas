package zw.co.esolutions.zpas.services.impl.debitorder;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.*;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.debitorder.DebitOrderReportDTO;
import zw.co.esolutions.zpas.enums.DebitOrderStatus;
import zw.co.esolutions.zpas.model.DebitOrderBatch;
import zw.co.esolutions.zpas.model.DebitOrderBatchItem;
import zw.co.esolutions.zpas.repository.DebitOrderBatchItemRepository;
import zw.co.esolutions.zpas.repository.DebitOrderBatchRepository;
import zw.co.esolutions.zpas.services.iface.debitorder.DebitOrderService;
import zw.co.esolutions.zpas.services.impl.payments.converters.*;
import zw.co.esolutions.zpas.utilities.util.SortCodeUtil;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DebitOrderServiceImpl implements DebitOrderService {

    @Autowired
    private final EntityManager entityManager;

    @Autowired
    private DebitOrderBatchRepository debitOrderBatchRepository;

    @Autowired
    private DebitOrderBatchItemRepository debitOrderBatchItemRepository;

    @Autowired
    private SortCodeUtil sortCodeUtil;

    public DebitOrderServiceImpl(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    @Override
    public Optional<DebitOrderBatch> getDebitOrderBatchById(String id) {
        return debitOrderBatchRepository.findById(id);
    }

    @Override
    public List<DebitOrderBatch> getDebitOrderBatchByClientId(String clientId) {
        return debitOrderBatchRepository.findTop50ByClientId(clientId);
    }

    @Override
    public List<DebitOrderBatch> getDebitOrderBatchBySenderId(String bic) {
        return debitOrderBatchRepository.findTop50BySenderID(bic);
    }

    @Override
    public Optional<DebitOrderBatch> submitDebitOrderBatch(String debitOrderBatchId) {
        log.info("Submitting the debit order batch ..");
        Optional<DebitOrderBatch> debitOrderBatchOptional = debitOrderBatchRepository.findById(debitOrderBatchId);

        if(debitOrderBatchOptional.isPresent()) {
            log.info("Found the dob. updating ..");
            DebitOrderBatch debitOrderBatch = debitOrderBatchOptional.get();
            debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.PENDING_APPROVAL);
            List<DebitOrderBatchItem> debitOrderBatchItems = new ArrayList<>();
            for(DebitOrderBatchItem debitOrderBatchItem : debitOrderBatch.getDebitOrderBatchItems()){
                if(isDebitOrderItemValid(debitOrderBatchItem.getItemStatus())) {
                    debitOrderBatchItem.setItemStatus(DebitOrderStatus.PENDING_APPROVAL);
                    debitOrderBatchItems.add(debitOrderBatchItem);
                }
            }

            debitOrderBatchItemRepository.saveAll(debitOrderBatchItems);
            DebitOrderBatch orderBatch = debitOrderBatchRepository.save(debitOrderBatch);

            return Optional.of(orderBatch);
        } else {
            log.info("The debit order batch is not found.");
            return Optional.empty();
        }
    }

    @Override
    public Optional<DebitOrderBatch> approveDebitOrderBatch(String debitOrderBatchId) {
        log.info("Approving the debit order batch ..");
        Optional<DebitOrderBatch> debitOrderBatchOptional = debitOrderBatchRepository.findById(debitOrderBatchId);

        if(debitOrderBatchOptional.isPresent()) {
            log.info("Found the dob. updating ..");
            DebitOrderBatch debitOrderBatch = debitOrderBatchOptional.get();
            if(debitOrderBatch.getDebitOrderStatus().equals(DebitOrderStatus.PENDING_APPROVAL)) {
                debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.PENDING_PROCESSING);
                List<DebitOrderBatchItem> debitOrderBatchItems = new ArrayList<>();
                for(DebitOrderBatchItem debitOrderBatchItem : debitOrderBatch.getDebitOrderBatchItems()){
                    if(isDebitOrderItemValid(debitOrderBatchItem.getItemStatus())) {
                        debitOrderBatchItem.setItemStatus(DebitOrderStatus.PENDING_PROCESSING);
                        debitOrderBatchItems.add(debitOrderBatchItem);
                    }
                }
                debitOrderBatchItemRepository.saveAll(debitOrderBatchItems);

                DebitOrderBatch orderBatch = debitOrderBatchRepository.save(debitOrderBatch);

                log.info("Invoking the generate SFI files");
                List<DebitOrderBatchItem> debitOrderItems = debitOrderBatchItemRepository.findByDebitOrderBatch_Id(debitOrderBatch.getId());
                generateSFIFile(debitOrderBatch, debitOrderItems);

                return Optional.of(orderBatch);
            } else {
                log.info("The debit order batch can't be approved on {}", debitOrderBatch.getDebitOrderStatus().name());
                return Optional.empty();
            }
        } else {
            log.info("The debit order batch is not found.");
            return Optional.empty();
        }
    }

    @Override
    public Optional<DebitOrderBatch> initiateDebitOrderBatch(String debitOrderBatchId) {
        log.info("Initiating the debit order batch ..");
        Optional<DebitOrderBatch> debitOrderBatchOptional = debitOrderBatchRepository.findById(debitOrderBatchId);

        if(debitOrderBatchOptional.isPresent()) {
            log.info("Found the dob. updating ..");
            DebitOrderBatch debitOrderBatch = debitOrderBatchOptional.get();
            if(debitOrderBatch.getDebitOrderStatus().equals(DebitOrderStatus.PENDING_FI_APPROVAL)) {
                debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.PENDING_PROCESSING);
                DebitOrderBatch orderBatch = debitOrderBatchRepository.save(debitOrderBatch);

                log.info("Invoking the generate SFI files");
                List<DebitOrderBatchItem> debitOrderBatchItems = debitOrderBatchItemRepository.findByDebitOrderBatch_Id(debitOrderBatch.getId());
                generateSFIFile(debitOrderBatch, debitOrderBatchItems);

                return Optional.of(orderBatch);
            } else {
                log.info("The Debit order can't be initiated on status {}", debitOrderBatch.getDebitOrderStatus().name());

                return Optional.empty();
            }

        } else {
            log.info("The debit order batch is not found.");
            return Optional.empty();
        }
    }


    @Override
    public void updateDebitOrderStatuses(String debitOrderBatchId) {

    }

    @Override
    public Optional<DebitOrderBatch> rejectDebitOrderBatch(String debitOrderBatchId) {
        log.info("Rejecting the debit order batch ..");
        Optional<DebitOrderBatch> debitOrderBatchOptional = debitOrderBatchRepository.findById(debitOrderBatchId);

        if(debitOrderBatchOptional.isPresent()) {
            log.info("Found the dob. updating ..");
            DebitOrderBatch debitOrderBatch = debitOrderBatchOptional.get();
            debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.REJECTED);

            List<DebitOrderBatchItem> debitOrderBatchItems = new ArrayList<>();
            for(DebitOrderBatchItem debitOrderBatchItem : debitOrderBatch.getDebitOrderBatchItems()){
                if(isDebitOrderItemValid(debitOrderBatchItem.getItemStatus())) {
                    debitOrderBatchItem.setItemStatus(DebitOrderStatus.REJECTED);
                    debitOrderBatchItems.add(debitOrderBatchItem);
                }
            }
            debitOrderBatchItemRepository.saveAll(debitOrderBatchItems);

            DebitOrderBatch orderBatch = debitOrderBatchRepository.save(debitOrderBatch);

            return Optional.of(orderBatch);
        } else {
            log.info("The debit order batch is not found.");
            return Optional.empty();
        }
    }

    @Override
    public Optional<DebitOrderBatch> deleteDebitOrderBatch(String debitOrderBatchId) {
        log.info("Deleting the debit order batch ..");
        Optional<DebitOrderBatch> debitOrderBatchOptional = debitOrderBatchRepository.findById(debitOrderBatchId);

        if(debitOrderBatchOptional.isPresent()) {
            log.info("Found the dob. updating ..");
            DebitOrderBatch debitOrderBatch = debitOrderBatchOptional.get();
            debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.DELETED);

            List<DebitOrderBatchItem> debitOrderBatchItems = new ArrayList<>();
            for(DebitOrderBatchItem debitOrderBatchItem : debitOrderBatch.getDebitOrderBatchItems()){
                if(isDebitOrderItemValid(debitOrderBatchItem.getItemStatus())) {
                    debitOrderBatchItem.setItemStatus(DebitOrderStatus.DELETED);
                    debitOrderBatchItems.add(debitOrderBatchItem);
                }
            }
            debitOrderBatchItemRepository.saveAll(debitOrderBatchItems);

            DebitOrderBatch orderBatch = debitOrderBatchRepository.save(debitOrderBatch);

            return Optional.of(orderBatch);
        } else {
            log.info("The debit order batch is not found.");
            return Optional.empty();
        }
    }

    private Boolean isDebitOrderItemValid(DebitOrderStatus debitOrderStatus) {
        if(debitOrderStatus.equals(DebitOrderStatus.FAILED)
        || debitOrderStatus.equals(DebitOrderStatus.DELETED)
        || debitOrderStatus.equals(DebitOrderStatus.REJECTED)) {
            return false;
        }
        return true;
    }

    private void generateSFIFile(DebitOrderBatch debitOrderBatch, List<DebitOrderBatchItem> debitOrderBatchItems) {
        debitOrderBatchItems = debitOrderBatchItems.stream()
                .filter(debitOrderBatchItem -> isDebitOrderItemValid(debitOrderBatchItem.getItemStatus()))
                .collect(Collectors.toList());
        log.info("Updating the debit order batch to pending processing");
        if(debitOrderBatch != null) {
            debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.PENDING_PROCESSING);
            debitOrderBatchRepository.save(debitOrderBatch);
        } else {
            log.info("The debit order batch is not found.");
            return;
        }
        log.info("Generating SFI file ...");
        try {
            Instant startTime = Instant.now();
            SFIUtils.generateSFIContentsFromTransfers(debitOrderBatch, debitOrderBatchItems, sortCodeUtil);
            Instant endTime = Instant.now();
            log.info("Done generating SFI file. Operation took a period of {}", Duration.between(startTime, endTime));
        } catch (Exception e) {
            log.info("An error occurred while generating file.");
            e.printStackTrace();
        }

    }


    @Override
    public void processDebitOrderBatchResponse() {
        Path responsePath = Paths.get(SFIUtils.DEBIT_ORDER_RESPONSE_FOLDER);
        try {
            if (Files.notExists(responsePath)) {
                Files.createDirectories(responsePath);
            }
        } catch (IOException e) {
            log.info("Something went wrong while creating paynet directories.");
            e.printStackTrace();
        }
        try {
            Files.list(responsePath).forEach(path -> {
                log.info("About to process path {}", path.toString());
                processResponse(path.toString());


                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd_MMM_yyyy_HH_mm");
                String date = OffsetDateTime.now().format(formatter);
                String oldFileName = path.getFileName().toString();
                String newFileName = oldFileName + "_" + date;

                Path responseArchivePath = Paths.get(SFIUtils.DEBIT_ORDER_RESPONSE_ARCHIVE_FOLDER);

                try {
                    if (Files.notExists(responseArchivePath)) {
                        Files.createDirectories(responseArchivePath);
                    }
                    moveFileTo(path, responseArchivePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                log.info("Moving the file to archive directory with new file name {}", newFileName);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<DebitOrderBatch> searchByDate(DebitOrderReportDTO debitOrderReportDTO) {
        log.info("startDate: " + debitOrderReportDTO.getStartDate());
        LocalDateTime startLocalDateTime = LocalDateTime.parse(debitOrderReportDTO.getStartDate(), DateTimeFormatter.ISO_DATE_TIME);
        OffsetDateTime startOffsetDate = OffsetDateTime.of(startLocalDateTime, ZoneOffset.ofHours(2));

        LocalDateTime endLocalDateTime = LocalDateTime.parse(debitOrderReportDTO.getEndDate(), DateTimeFormatter.ISO_DATE_TIME);
        OffsetDateTime endOffsetDate = OffsetDateTime.of(endLocalDateTime, ZoneOffset.ofHours(2));

        return debitOrderBatchRepository.findByDateCreatedBetween(startOffsetDate, endOffsetDate);
    }

    @Override
    public List<DebitOrderBatch> searchDebitOrderBatches(String searchTerm) {
        FullTextEntityManager fullTextEm = org.hibernate.search.jpa.Search
                .getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEm.getSearchFactory().buildQueryBuilder().forEntity(DebitOrderBatch.class).get();

        FullTextQuery fullTextQuery = fullTextEm.createFullTextQuery(queryBuilder.keyword()
                .onFields("batchNumber","targetAccount")
                .matching(searchTerm)
                .createQuery(), DebitOrderBatch.class);

        List<DebitOrderBatch> resultList = fullTextQuery.getResultList();
        return resultList;
    }

    @Override
    public String refreshDebitOrderBatchIndex() {
        log.info("inside refresh index method!!!");
        FullTextSession fullTextSession = Search.getFullTextSession(entityManager.unwrap(Session.class));
        fullTextSession.setFlushMode(FlushMode.MANUAL);
        fullTextSession.setCacheMode(CacheMode.IGNORE);
        int BATCH_SIZE = 25;
        ScrollableResults scrollableResults = fullTextSession.createCriteria(DebitOrderBatch.class)
                .setFetchSize(BATCH_SIZE)
                .scroll(ScrollMode.FORWARD_ONLY);
        int index = 0;
        while(scrollableResults.next()) {
            index++;
            fullTextSession.index(scrollableResults.get(0)); //index each element
            if (index % BATCH_SIZE == 0) {
                fullTextSession.flushToIndexes(); //apply changes to indexes
                fullTextSession.clear(); //free memory since the queue is processed
            }
        }

        return "Debit Order Batch search indices reloaded successfully";
    }

    @Override
    public List<DebitOrderBatch> findTop50ByReceiverID(String receiverID) {
      return debitOrderBatchRepository.findTop50ByReceiverID(receiverID);
    }

    private void moveFileTo(Path source, Path destination) throws IOException {
        Path temp = Files.move
                (source, destination.resolve(source.getFileName()));
        if (temp != null) {
            System.out.println("File renamed and moved successfully");
        } else {
            System.out.println("Failed to move the file");
        }
    }

    private void processResponse(String fileName) {
        log.info("Processing debit order batch response for file {}", fileName);
        try {
            final SFIFileContents sfiFileContents = BeanUtils.parseSFIFile(fileName);
            final Collection<SFIDetailRecord> detailRecords = sfiFileContents.getDetailRecords();
            final SFIHeaderRecord headerRecord = sfiFileContents.getHeaderRecord();
            final String fileID = headerRecord.getFileID();

            log.info("Creating SFI detail record cache...");
            Map<String, SFIDetailRecord> detailRecordMap = new HashMap<>();
            detailRecords.forEach(sfiDetailRecord -> {
                detailRecordMap.put(sfiDetailRecord.getReferenceID(), sfiDetailRecord);
            });

            debitOrderBatchRepository.findByFileID(fileID)
                    .map(debitOrderBatch -> {
                        log.info("Found debit order batch corresponding to file {} with id {}", fileName, fileID);
                        List<DebitOrderBatchItem> debitOrderBatchItems = debitOrderBatchItemRepository.findByDebitOrderBatch_Id(debitOrderBatch.getId());

                        log.info("Found {} batch items", debitOrderBatchItems.size());
                        debitOrderBatchItems.forEach(debitOrderBatchItem -> {
                            log.info("Working on batch item with reference id {}", debitOrderBatchItem.getReferenceID());
                            final SFIDetailRecord sfiDetailRecord = detailRecordMap.get(debitOrderBatchItem.getReferenceID());
                            if (sfiDetailRecord != null) {
                                log.info("Matching sfi record for this item found {}", sfiDetailRecord);
                                if (sfiDetailRecord.getUnpaidReason().contains("00")) {
                                    log.info("sfi was successful. Making local payment for the debit order item...");
                                    debitOrderBatchItem.setItemStatus(DebitOrderStatus.SUCCESSFUL);
                                } else {
                                    log.info("SFI is not successful. SFI record narrative is {}.", sfiDetailRecord.getNarrative());
                                    log.info("Setting the item to failed.");

                                    debitOrderBatchItem.setItemStatus(DebitOrderStatus.FAILED);
                                }
                                debitOrderBatchItem.setNarrative(sfiDetailRecord.getNarrative());

                            } else {
                                log.info("No matching sfi record for this item found {}", debitOrderBatchItem);
                                debitOrderBatchItem.setItemStatus(DebitOrderStatus.FAILED);
                            }
                            debitOrderBatchItemRepository.save(debitOrderBatchItem);
                        });
                        log.info("Saving the debit order batch to COMPLETED");
                        debitOrderBatch.setDebitOrderStatus(DebitOrderStatus.COMPLETED);
                        debitOrderBatchRepository.save(debitOrderBatch);
                        return "";
                    })
                    .orElseGet(() -> {
                        log.info("Debit order batch with this fileId : {} is not found.", fileID);
                        return "";
                    });

        } catch (Exception e) {
            log.info("An error occurred while trying to parse file {}. Error message: {}", fileName, e.getMessage());
            e.printStackTrace();
        }
    }
}
