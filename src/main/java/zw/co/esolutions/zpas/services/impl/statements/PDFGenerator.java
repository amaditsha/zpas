package zw.co.esolutions.zpas.services.impl.statements;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.dto.messaging.ProofOfPaymentInfo;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by alfred on 15 Mar 2019
 */
@Slf4j
@Component
public class PDFGenerator {

    public static void main(String[] args) {
        ProofOfPaymentInfo proofOfPaymentInfo = ProofOfPaymentInfo.builder().build();
        new PDFGenerator().generateDocument(proofOfPaymentInfo);
    }

    public String generateDocument(ProofOfPaymentInfo proofOfPaymentInfo) {
        Document document = new Document();
        final Path fileLocation = Paths.get(StorageProperties.BASE_LOCATION).resolve("proof-of-payment.pdf");
        try {
            PdfWriter.getInstance(document, new FileOutputStream(fileLocation.toFile()));

            document.open();

            //Set attributes here
            document.addAuthor("ZEEPAY System");
            document.addCreationDate();
            document.addCreator("ZEEPAY");
            document.addTitle("Proof of Payment");
            document.addSubject("Proof of Payment.");

            Path path = Paths.get(ClassLoader.getSystemResource("static/images/ZimSwitch_Logo.png").toURI());
            Image img = Image.getInstance(path.toAbsolutePath().toString());
            img.scalePercent(10);
            img.setAbsolutePosition(500f, 800f);
            document.add(img);

            addHeading(document);
            addAddressInfo(document, proofOfPaymentInfo);
            addInvoiceTotals(document, proofOfPaymentInfo);
            addDiscountInfo(document, proofOfPaymentInfo);
            addBody(document, proofOfPaymentInfo);
            addRemittanceAdvice(document, proofOfPaymentInfo);
            addRemittanceTable(document, proofOfPaymentInfo);

            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return fileLocation.toString();
    }

    private void addHeading(Document document) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        //Paragraph with color and font styles
        Paragraph paragraph = new Paragraph("ZEEPAY", blackFont);
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("PAYEE ADVICE", blackFont));

        PdfPTable table = new PdfPTable(3); // 4 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f};
        table.setWidths(columnWidths);

        PdfPCell cell1 = new PdfPCell(new Paragraph(""));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setPaddingLeft(10);
        cell1.setPaddingBottom(10);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell2 = new PdfPCell(paragraph);
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setPaddingLeft(10);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell3 = new PdfPCell(new Paragraph(""));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setPaddingLeft(10);
        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);

        document.add(table);
    }

    private void addAddressInfo(Document document, ProofOfPaymentInfo proofOfPaymentInfo) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        Paragraph paragraph = new Paragraph("", blackFont);
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("Ref: " + proofOfPaymentInfo.getPaymentReference()));
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("Customer Ref: " + proofOfPaymentInfo.getPaymentReference()));
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("Date: " + OffsetDateTime.now().format(DateTimeFormatter.ofPattern("dd MMMM yyyy"))));
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("To: " + proofOfPaymentInfo.getCreditorName()));

        document.add(paragraph);
    }

    private void addInvoiceTotals(Document document, ProofOfPaymentInfo proofOfPaymentInfo) throws DocumentException {
        Font blackBoldFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        double invoiceCharge = 0;
        String invoiceChargeString = MoneyUtil.convertDollarsToPattern(invoiceCharge);

        Paragraph paragraphInvoiceValueHeader = new Paragraph("Invoice Total", blackBoldFont);
        Paragraph paragraphInvoiceValue = new Paragraph(proofOfPaymentInfo.getPayment().getAmount().toString(), blackBoldFont);
        Paragraph paragraphInvoiceChargeHeader = new Paragraph("- Benef. Charge", blackFont);
        Paragraph paragraphInvoiceCharge = new Paragraph(invoiceChargeString, blackFont);


        PdfPTable table = new PdfPTable(3); // 3 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f};
        table.setWidths(columnWidths);

        //Invoice Value Section
        PdfPCell cell1 = new PdfPCell(new Paragraph(""));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setPaddingLeft(10);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell2 = new PdfPCell(paragraphInvoiceValueHeader);
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setPaddingLeft(10);
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell3 = new PdfPCell(new Paragraph(paragraphInvoiceValue));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setPaddingLeft(10);
        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);

        //Invoice Charges Section
        PdfPCell cell4 = new PdfPCell(new Paragraph(""));
        cell4.setBorder(PdfPCell.NO_BORDER);
        cell4.setPaddingLeft(10);
        cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell5 = new PdfPCell(paragraphInvoiceChargeHeader);
        cell5.setBorder(PdfPCell.NO_BORDER);
        cell5.setPaddingLeft(10);
        cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell6 = new PdfPCell(new Paragraph(paragraphInvoiceCharge));
        cell6.setBorder(PdfPCell.NO_BORDER);
        cell6.setPaddingLeft(10);
        cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell4.setUseBorderPadding(true);
        cell5.setUseBorderPadding(true);
        cell6.setUseBorderPadding(true);

        final PdfPCell pdfPSpacerCell1 = new PdfPCell(new Paragraph(Chunk.NEWLINE));
        pdfPSpacerCell1.setBorderColor(BaseColor.WHITE);
        pdfPSpacerCell1.setPaddingLeft(10);
        pdfPSpacerCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfPSpacerCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        final PdfPCell pdfPSpacerCell2 = new PdfPCell(new Paragraph(Chunk.NEWLINE));
        pdfPSpacerCell2.setBorderColor(BaseColor.WHITE);
        pdfPSpacerCell2.setPaddingLeft(10);
        pdfPSpacerCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfPSpacerCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        final PdfPCell pdfPSpacerCell3 = new PdfPCell(new Paragraph(Chunk.NEWLINE));
        pdfPSpacerCell3.setBorderColor(BaseColor.WHITE);
        pdfPSpacerCell3.setPaddingLeft(10);
        pdfPSpacerCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
        pdfPSpacerCell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);
        table.addCell(cell5);
        table.addCell(cell6);

        table.addCell(pdfPSpacerCell1);
        table.addCell(pdfPSpacerCell2);
        table.addCell(pdfPSpacerCell3);

        document.add(table);
    }

    private void addDiscountInfo(Document document, ProofOfPaymentInfo proofOfPaymentInfo) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        double discountValue = 0;
        String discountValueString = MoneyUtil.convertDollarsToPattern(discountValue);
        String utrReference = proofOfPaymentInfo.getUtrReference();

        Paragraph paragraphDiscountValueHeader = new Paragraph("- Discount", blackFont);
        Paragraph paragraphDiscountValue = new Paragraph(discountValueString, blackFont);
        Paragraph paragraphUtrReferenceHeader = new Paragraph("UTR Reference", blackFont);
        Paragraph paragraphUtrReference = new Paragraph(utrReference, blackFont);

        //Invoice Discount Section
        PdfPCell cell7 = new PdfPCell(new Paragraph(""));
        cell7.setBorder(PdfPCell.NO_BORDER);
        cell7.setPaddingLeft(10);
        cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell8 = new PdfPCell(paragraphDiscountValueHeader);
        cell8.setBorder(PdfPCell.NO_BORDER);
        cell8.setPaddingLeft(10);
        cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell9 = new PdfPCell(new Paragraph(paragraphDiscountValue));
        cell9.setBorder(PdfPCell.NO_BORDER);
        cell9.setPaddingLeft(10);
        cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell7.setUseBorderPadding(true);
        cell8.setUseBorderPadding(true);
        cell9.setUseBorderPadding(true);

        //Invoice UTR Reference Section
        PdfPCell cell10 = new PdfPCell(new Paragraph(""));
        cell10.setBorder(PdfPCell.NO_BORDER);
        cell10.setPaddingLeft(10);
        cell10.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell10.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell11 = new PdfPCell(paragraphUtrReferenceHeader);
        cell11.setBorder(PdfPCell.NO_BORDER);
        cell11.setPaddingLeft(10);
        cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell12 = new PdfPCell(new Paragraph(paragraphUtrReference));
        cell12.setBorder(PdfPCell.NO_BORDER);
        cell12.setPaddingLeft(10);
        cell12.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell12.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell10.setUseBorderPadding(true);
        cell11.setUseBorderPadding(true);
        cell12.setUseBorderPadding(true);


        PdfPTable table = new PdfPTable(3); // 3 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f};
        table.setWidths(columnWidths);

        table.addCell(cell7);
        table.addCell(cell8);
        table.addCell(cell9);
        table.addCell(cell10);
        table.addCell(cell11);
        table.addCell(cell12);

        document.add(table);
    }

    private void addBody(Document document, ProofOfPaymentInfo proofOfPaymentInfo) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        //Paragraph with color and font styles
        String paymentDate = proofOfPaymentInfo.getPayment().getValueDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy"));

        document.add(new Paragraph("Dear Sir / Madam (s),", blackFont));
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph("We have on " + paymentDate + " made a payment to your account "
                + proofOfPaymentInfo.getCreditorAccountNumber() + " at " + proofOfPaymentInfo.getCreditorBank()
                + " for " + proofOfPaymentInfo.getPayment().getAmount().toString() + " as instructed by "
                + proofOfPaymentInfo.getDebtorName() + "."));
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph("Should you not receive the payment in time, please contact "
                + proofOfPaymentInfo.getDebtorName() + " for further investigations."));
        document.add(Chunk.NEWLINE);
        LineSeparator line = new LineSeparator(0f, 100f, BaseColor.BLACK, Element.ALIGN_LEFT, 1);
        document.add(new Chunk(line));
    }

    private void addRemittanceAdvice(Document document, ProofOfPaymentInfo proofOfPaymentInfo) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        document.add(new Paragraph("Remittance Advice,", blackFont));
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph("This section includes details as supplied by " + proofOfPaymentInfo.getDebtorName(), blackFont));
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph("Payment Details: ", blackFont));
    }

    private void addRemittanceTable(Document document, ProofOfPaymentInfo proofOfPaymentInfo) throws DocumentException {
        PdfPTable table = new PdfPTable(4); // 4 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);

        PdfPCell cell1 = new PdfPCell(new Paragraph("Reference"));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell1.setPaddingLeft(10);
        cell1.setPaddingBottom(10);
        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell2 = new PdfPCell(new Paragraph("Date"));
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell2.setPaddingLeft(10);
        cell2.setPaddingBottom(10);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell3 = new PdfPCell(new Paragraph("Description"));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell3.setPaddingLeft(10);
        cell3.setPaddingBottom(10);
        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell4 = new PdfPCell(new Paragraph("Amount"));
        cell4.setBorder(PdfPCell.NO_BORDER);
        cell4.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell4.setPaddingLeft(10);
        cell4.setPaddingBottom(10);
        cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);
        cell4.setUseBorderPadding(true);

        PdfPCell cell5 = new PdfPCell(new Paragraph(proofOfPaymentInfo.getPaymentReference()));
        cell5.setBorder(PdfPCell.NO_BORDER);
        cell5.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell5.setPaddingLeft(10);
        cell5.setPaddingBottom(10);
        cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell6 = new PdfPCell(new Paragraph(proofOfPaymentInfo.getPayment().getValueDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy"))));
        cell6.setBorder(PdfPCell.NO_BORDER);
        cell6.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell6.setPaddingLeft(10);
        cell6.setPaddingBottom(10);
        cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell7 = new PdfPCell(new Paragraph(""));
        cell7.setBorder(PdfPCell.NO_BORDER);
        cell7.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell7.setPaddingLeft(10);
        cell7.setPaddingBottom(10);
        cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell8 = new PdfPCell(new Paragraph(proofOfPaymentInfo.getPayment().getAmount().toString()));
        cell8.setBorder(PdfPCell.NO_BORDER);
        cell8.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell8.setPaddingLeft(10);
        cell8.setPaddingBottom(10);
        cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);
        cell4.setUseBorderPadding(true);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);
        table.addCell(cell5);
        table.addCell(cell6);
        table.addCell(cell7);
        table.addCell(cell8);

        document.add(table);
    }
}
