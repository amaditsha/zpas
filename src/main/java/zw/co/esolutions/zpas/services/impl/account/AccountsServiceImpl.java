package zw.co.esolutions.zpas.services.impl.account;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.configs.AuditLoggerConfiguration;
import zw.co.esolutions.zpas.dto.account.AccountSearchResult;
import zw.co.esolutions.zpas.dto.account.CashAccountDTO;
import zw.co.esolutions.zpas.dto.party.VirtualCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.payments.RequestInfo;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.party.PartyService;
import zw.co.esolutions.zpas.services.iface.process.AccountInfoInitiationService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.audit.AuditEvents;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.System;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by petros on 14 Feb 2019
 */
@Slf4j
@Service
@Transactional
public class AccountsServiceImpl implements AccountsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountInfoInitiationService accountInfoInitiationService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CashAccountRepository cashAccountRepository;

    @Autowired
    private AccountPartyRoleRepository accountPartyRoleRepository;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private RolePlayerRepository rolePlayerRepository;

    @Autowired
    private final EntityManager entityManager;

    @Autowired
    private AuditLoggerConfiguration auditLoggerConfiguration;

    @Autowired
    public AccountsServiceImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private CashAccount convertDtoToEntity(CashAccountDTO cashAccountDTO) {
        CashAccount cashAccount = new CashAccount();
        cashAccount.setVirtualCode(cashAccountDTO.getVirtualCode());
        OffsetDateTime closingDate;
        OffsetDateTime liveDate;
        OffsetDateTime openingDate;
        OffsetDateTime fromDateTime;
        OffsetDateTime toDateTime;
        OffsetDateTime proprietaryIssueDate;
        OffsetDateTime proprietaryExpiryDate;
        OffsetDateTime costReferenceIssueDate;
        OffsetDateTime costReferenceExpiryDate;
        OffsetDateTime issueDate;
        OffsetDateTime expiryDate;

        log.info("is cashAccountId present? : " + cashAccountDTO.getId());
        if (StringUtils.isNoneBlank(cashAccountDTO.getId())) {
            Optional<CashAccount> cashAccountOptional = cashAccountRepository.findById(cashAccountDTO.getId());

            if (cashAccountOptional.isPresent()) {
                cashAccount = cashAccountOptional.get();

                if (StringUtils.isNoneBlank(cashAccountDTO.getClosingDate())) {
                    if (cashAccount.getClosingDate() != null) {
                        closingDate = LocalDate.parse(cashAccountDTO.getClosingDate()).atTime(OffsetTime.now());
                    } else {
                        closingDate = LocalDate.parse(cashAccountDTO.getClosingDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    closingDate = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getLiveDate())) {
                    if (cashAccount.getLiveDate() != null) {
                        liveDate = LocalDate.parse(cashAccountDTO.getLiveDate()).atTime(OffsetTime.now());
                    } else {
                        liveDate = LocalDate.parse(cashAccountDTO.getLiveDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    liveDate = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getOpeningDate())) {
                    if (cashAccount.getOpeningDate() != null) {
                        openingDate = LocalDate.parse(cashAccountDTO.getOpeningDate()).atTime(OffsetTime.now());
                    } else {
                        openingDate = LocalDate.parse(cashAccountDTO.getOpeningDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    openingDate = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getFromDateTime())) {
                    if (cashAccount.getReportedPeriod().getFromDateTime() != null) {
                        fromDateTime = LocalDate.parse(cashAccountDTO.getFromDateTime()).atTime(OffsetTime.now());
                    } else {
                        fromDateTime = LocalDate.parse(cashAccountDTO.getFromDateTime()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    fromDateTime = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getToDateTime())) {
                    if (cashAccount.getReportedPeriod().getToDateTime() != null) {
                        toDateTime = LocalDate.parse(cashAccountDTO.getToDateTime()).atTime(OffsetTime.now());
                    } else {
                        toDateTime = LocalDate.parse(cashAccountDTO.getToDateTime()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    toDateTime = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getProprietaryIdentificationIssueDate())) {
                    if (cashAccount.getIdentification().getProprietaryIdentification().getIssueDate() != null) {
                        proprietaryIssueDate = LocalDate.parse(cashAccountDTO.getProprietaryIdentificationIssueDate()).atTime(OffsetTime.now());
                    } else {
                        proprietaryIssueDate = LocalDate.parse(cashAccountDTO.getProprietaryIdentificationIssueDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    proprietaryIssueDate = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getProprietaryIdentificationExpiryDate())) {
                    if (cashAccount.getIdentification().getProprietaryIdentification().getExpiryDate() != null) {
                        proprietaryExpiryDate = LocalDate.parse(cashAccountDTO.getProprietaryIdentificationExpiryDate()).atTime(OffsetTime.now());
                    } else {
                        proprietaryExpiryDate = LocalDate.parse(cashAccountDTO.getProprietaryIdentificationExpiryDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/, DateTimeFormatter.ofPattern("MM/dd/yyyy")).atTime(OffsetTime.now());
                    }
                } else {
                    proprietaryExpiryDate = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getCostReferencePatternIssueDate())) {
                    if (cashAccount.getIdentification().getCostReferencePattern().getIssueDate() != null) {
                        costReferenceIssueDate = LocalDate.parse(cashAccountDTO.getCostReferencePatternIssueDate()).atTime(OffsetTime.now());
                    } else {
                        costReferenceIssueDate = LocalDate.parse(cashAccountDTO.getCostReferencePatternIssueDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    costReferenceIssueDate = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getCostReferencePatternExpiryDate())) {
                    if (cashAccount.getIdentification().getCostReferencePattern().getExpiryDate() != null) {
                        costReferenceExpiryDate = LocalDate.parse(cashAccountDTO.getCostReferencePatternExpiryDate()).atTime(OffsetTime.now());
                    } else {
                        costReferenceExpiryDate = LocalDate.parse(cashAccountDTO.getCostReferencePatternExpiryDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    costReferenceExpiryDate = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getIssueDate())) {
                    if (cashAccount.getType().getIssueDate() != null) {
                        issueDate = LocalDate.parse(cashAccountDTO.getIssueDate()).atTime(OffsetTime.now());
                    } else {
                        issueDate = LocalDate.parse(cashAccountDTO.getIssueDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    issueDate = null;
                }

                if (StringUtils.isNoneBlank(cashAccountDTO.getExpiryDate())) {
                    if (cashAccount.getType().getExpiryDate() != null) {
                        expiryDate = LocalDate.parse(cashAccountDTO.getExpiryDate()).atTime(OffsetTime.now());
                    } else {
                        expiryDate = LocalDate.parse(cashAccountDTO.getExpiryDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
                    }
                } else {
                    expiryDate = null;
                }

                CurrencyCode settlementCurrency = cashAccount.getSettlementCurrency();
                //settlementCurrency.setCodeName(cashAccountDTO.getSettlementCurrency());

                CurrencyCode baseCurrency = cashAccount.getBaseCurrency();
                baseCurrency.setCodeName(cashAccountDTO.getBaseCurrency());

                CurrencyCode reportingCurrency = cashAccount.getReportingCurrency();
                //reportingCurrency.setCodeName(cashAccountDTO.getReportingCurrency());

                DateTimePeriod reportedPeriod = cashAccount.getReportedPeriod();
                /*reportedPeriod.setFromDateTime(fromDateTime);
                reportedPeriod.setToDateTime(toDateTime);
                reportedPeriod.setNumberOfDays(Long.valueOf(StringUtils.isNoneBlank(cashAccountDTO.getNumberOfDays()) ? cashAccountDTO.getNumberOfDays() : "0"));
*/
                GenericIdentification proprietaryIdentification = cashAccount.getIdentification().getProprietaryIdentification();
                /*proprietaryIdentification.setIdentification(cashAccountDTO.getProprietaryIdentificationId());
                proprietaryIdentification.setIssueDate(proprietaryIssueDate);
                proprietaryIdentification.setExpiryDate(proprietaryExpiryDate);*/

                GenericIdentification costReferencePattern = cashAccount.getIdentification().getCostReferencePattern();
                /*costReferencePattern.setIdentification(cashAccountDTO.getCostReferencePatternId());
                costReferencePattern.setIssueDate(costReferenceIssueDate);
                costReferencePattern.setExpiryDate(costReferenceExpiryDate);*/

                AccountIdentification identification = cashAccount.getIdentification();
                identification.setIBAN(cashAccountDTO.getIBAN());
                identification.setBBAN(cashAccountDTO.getBBAN());
                identification.setUPIC(cashAccountDTO.getUPIC());
                identification.setName(cashAccountDTO.getName());
                identification.setNumber(cashAccountDTO.getNumber());
                identification.setProprietaryIdentification(proprietaryIdentification);
                identification.setCostReferencePattern(costReferencePattern);

                AccountStatus status = cashAccount.getStatus();
                status.setStatus(AccountStatusCode.Pending);
                status.setBlocked(Boolean.FALSE);
                status.setManagementStatus(AccountManagementStatusCode.None);
                status.setEntryStatus(EntryStatusCode.None);
                status.setBalanceStatus(BalanceStatusCode.None);
                status.setBlockedReason(ReasonBlockedCode.None);

                GenericIdentification type = cashAccount.getType();
                type.setIdentification(cashAccountDTO.getIdentification());
                type.setIssueDate(issueDate);
                type.setExpiryDate(expiryDate);

                cashAccount.setCashAccountType(CashAccountTypeCode.valueOf(cashAccountDTO.getCashAccountType()));
                cashAccount.setLevel(AccountLevelCode.valueOf(cashAccountDTO.getLevel()));
                cashAccount.setSettlementCurrency(settlementCurrency);
                cashAccount.setBaseCurrency(baseCurrency);
                cashAccount.setReportingCurrency(reportingCurrency);
                cashAccount.setPurpose(cashAccountDTO.getPurpose());
                cashAccount.setClosingDate(closingDate);
                cashAccount.setLiveDate(liveDate);
                cashAccount.setReportedPeriod(reportedPeriod);
                cashAccount.setOpeningDate(openingDate);
                cashAccount.setIdentification(identification);
                cashAccount.setStatus(status);
                cashAccount.setType(type);

                cashAccount.setUploadAccountsId(cashAccountDTO.getUploadAccountsId());
            }

        } else {

            if (StringUtils.isNoneBlank(cashAccountDTO.getClosingDate())) {
                closingDate = LocalDate.parse(cashAccountDTO.getClosingDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                closingDate = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getLiveDate())) {
                liveDate = LocalDate.parse(cashAccountDTO.getLiveDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                liveDate = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getOpeningDate())) {
                openingDate = LocalDate.parse(cashAccountDTO.getOpeningDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                openingDate = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getFromDateTime())) {
                fromDateTime = LocalDate.parse(cashAccountDTO.getFromDateTime()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                fromDateTime = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getToDateTime())) {
                toDateTime = LocalDate.parse(cashAccountDTO.getToDateTime()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                toDateTime = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getProprietaryIdentificationIssueDate())) {
                proprietaryIssueDate = LocalDate.parse(cashAccountDTO.getProprietaryIdentificationIssueDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                proprietaryIssueDate = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getProprietaryIdentificationExpiryDate())) {
                proprietaryExpiryDate = LocalDate.parse(cashAccountDTO.getProprietaryIdentificationExpiryDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                proprietaryExpiryDate = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getCostReferencePatternIssueDate())) {
                costReferenceIssueDate = LocalDate.parse(cashAccountDTO.getCostReferencePatternIssueDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                costReferenceIssueDate = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getCostReferencePatternExpiryDate())) {
                costReferenceExpiryDate = LocalDate.parse(cashAccountDTO.getCostReferencePatternExpiryDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                costReferenceExpiryDate = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getIssueDate())) {
                issueDate = LocalDate.parse(cashAccountDTO.getIssueDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                issueDate = null;
            }

            if (StringUtils.isNoneBlank(cashAccountDTO.getExpiryDate())) {
                expiryDate = LocalDate.parse(cashAccountDTO.getExpiryDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/).atTime(OffsetTime.now());
            } else {
                expiryDate = null;
            }

            CurrencyCode settlementCurrency = new CurrencyCode();
            settlementCurrency.setCodeName(cashAccountDTO.getSettlementCurrency());

            CurrencyCode baseCurrency = new CurrencyCode();
            baseCurrency.setCodeName(cashAccountDTO.getBaseCurrency());

            CurrencyCode reportingCurrency = new CurrencyCode();
            reportingCurrency.setCodeName(cashAccountDTO.getReportingCurrency());

            DateTimePeriod reportedPeriod = new DateTimePeriod();
            reportedPeriod.setFromDateTime(fromDateTime);
            reportedPeriod.setToDateTime(toDateTime);
            reportedPeriod.setNumberOfDays(Long.valueOf(StringUtils.isNoneBlank(cashAccountDTO.getNumberOfDays()) ? cashAccountDTO.getNumberOfDays() : "0"));

            GenericIdentification proprietaryIdentification = new GenericIdentification();
            proprietaryIdentification.setIdentification(cashAccountDTO.getProprietaryIdentificationId());
            proprietaryIdentification.setIssueDate(proprietaryIssueDate);
            proprietaryIdentification.setExpiryDate(proprietaryExpiryDate);

            GenericIdentification costReferencePattern = new GenericIdentification();
            costReferencePattern.setIdentification(cashAccountDTO.getCostReferencePatternId());
            costReferencePattern.setIssueDate(costReferenceIssueDate);
            costReferencePattern.setExpiryDate(costReferenceExpiryDate);

            AccountIdentification identification = new AccountIdentification();
            identification.setIBAN(cashAccountDTO.getIBAN());
            identification.setBBAN(cashAccountDTO.getBBAN());
            identification.setUPIC(cashAccountDTO.getUPIC());
            identification.setName(cashAccountDTO.getName());
            identification.setNumber(cashAccountDTO.getNumber());
            identification.setProprietaryIdentification(proprietaryIdentification);
            identification.setCostReferencePattern(costReferencePattern);

            AccountStatus status = new AccountStatus();
            status.setStatus(AccountStatusCode.Pending);
            status.setBlocked(Boolean.FALSE);
            status.setManagementStatus(AccountManagementStatusCode.None);
            status.setEntryStatus(EntryStatusCode.None);
            status.setBalanceStatus(BalanceStatusCode.None);
            status.setBlockedReason(ReasonBlockedCode.None);

            GenericIdentification type = new GenericIdentification();
            type.setIdentification(cashAccountDTO.getIdentification());
            type.setIssueDate(issueDate);
            type.setExpiryDate(expiryDate);

            cashAccount.setCashAccountType(CashAccountTypeCode.valueOf(cashAccountDTO.getCashAccountType()));
            cashAccount.setLevel(AccountLevelCode.valueOf(cashAccountDTO.getLevel()));
            cashAccount.setSettlementCurrency(settlementCurrency);
            cashAccount.setBaseCurrency(baseCurrency);
            cashAccount.setReportingCurrency(reportingCurrency);
            cashAccount.setPurpose(cashAccountDTO.getPurpose());
            cashAccount.setClosingDate(closingDate);
            cashAccount.setLiveDate(liveDate);
            cashAccount.setReportedPeriod(reportedPeriod);
            cashAccount.setOpeningDate(openingDate);
            cashAccount.setIdentification(identification);
            cashAccount.setStatus(status);
            cashAccount.setType(type);

        }

        /**
         * The objects that are not relevant presently
         */
        /*cashAccount.setCashEntry(new ArrayList<>());
        cashAccount.setCashBalance(new ArrayList<>());
        cashAccount.setPaymentPartyRole(new InitiatingPartyRole());
        cashAccount.setRelatedCreditStandingOrder(new StandingOrder());
        cashAccount.setRelatedDebitStandingOrder(new StandingOrder());
        cashAccount.setCashAccountContract(new ArrayList<>());
        cashAccount.setCharges(new ArrayList<>());
        cashAccount.setTax(new Tax());
        cashAccount.setRelatedSettlementInstruction(new CashSettlement());
        //cashAccount.setCashSettlementPartyRole(new CashSettlementInstructionPartyRole());
        cashAccount.setRelatedInvoiceFinancingPartyRole(new InvoiceFinancingPartyRole());
        cashAccount.setReportedMovements(new ArrayList<>());
        cashAccount.setClosedAccountContract(new CashAccountContract());
        //cashAccount.setCashStandingOrder(new CashStandingOrder());
        cashAccount.setCashAccountService(new CashManagementService());
        cashAccount.setPayment(new CreditTransfer());
        cashAccount.setCommission(new Commission());*/
        //cashAccount.setParentAccount(new CashAccount());
        //cashAccount.setSubAccount(new ArrayList<>());
        /*cashAccount.setSettlementPartyRole(new SettlementPartyRole());
        cashAccount.setAccountRestriction(new ArrayList<>());
        cashAccount.setType(new GenericIdentification());
        cashAccount.setPartyRole(new ArrayList<>());
        cashAccount.setSystem(new ArrayList<>());
        cashAccount.setEntry(new ArrayList<>());
        cashAccount.setAccountContract(new ArrayList<>());
        cashAccount.setCurrencyExchange(new ArrayList<>());
        cashAccount.setSystemMember(new SystemMemberRole());
        cashAccount.setAccountService(new CashManagementService());
        cashAccount.setBalance(new ArrayList<>());*/

        return cashAccount;

    }

    @Override
    public CashAccount createCashAccount(CashAccountDTO cashAccountDTO, String rolePlayerId, String fiId) {
        log.info("financialInstitutionId: " + fiId + "rolePlayerId: " + rolePlayerId);
        /**
         * Persist the account
         */
        CashAccount cashAccount = convertDtoToEntity(cashAccountDTO);
        final Integer numberOfExistingAccounts = countAccountsByClientAndFinancialInstitution(fiId, rolePlayerId);
        cashAccount.setVirtualNumber(numberOfExistingAccounts + 1);
        CashAccount savedAccount = cashAccountRepository.save(cashAccount);
        List<Account> accounts = Arrays.asList(savedAccount);

        List<AccountPartyRole> accountPartyRoles = new ArrayList<>();

        /**
         * Build Account Owner Role
         */
        AccountOwnerRole accountOwnerRole = new AccountOwnerRole();
        accountOwnerRole.setAccount(accounts);
        accountOwnerRole.setId(GenerateKey.generateEntityId());
        accountOwnerRole.setEntityStatus(EntityStatus.ACTIVE);
        accountOwnerRole.setDateCreated(OffsetDateTime.now());
        List<RolePlayer> ownerPlayers = new ArrayList<>();
        rolePlayerRepository.findById(rolePlayerId).ifPresent(rolePlayer -> ownerPlayers.add(rolePlayer));
        accountOwnerRole.setPlayer(ownerPlayers);
        accountOwnerRole.setPartyRoleCode(PartyRoleCode.Custodian);

        /**
         * Build Account Servicer Role
         */
        AccountServicerRole accountServicerRole = new AccountServicerRole();
        accountServicerRole.setId(GenerateKey.generateEntityId());
        accountServicerRole.setAccount(accounts);
        accountServicerRole.setEntityStatus(EntityStatus.ACTIVE);
        accountServicerRole.setDateCreated(OffsetDateTime.now());
        List<RolePlayer> servicerPlayers = new ArrayList<>();
        financialInstitutionService.getFinancialInstitutionById(fiId).ifPresent(
                financialInstitution -> servicerPlayers.add(financialInstitution)
        );
        accountServicerRole.setPlayer(servicerPlayers);
        accountServicerRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        /**
         * Persist the two Account Party Roles
         */
        accountPartyRoles.add(accountOwnerRole);
        accountPartyRoles.add(accountServicerRole);
        accountPartyRoleRepository.saveAll(accountPartyRoles);

        auditLoggerConfiguration.auditLoggerUtil().logActivity(cashAccountDTO.getUsername(), AuditEvents.CREATE_ACCOUNT, savedAccount);

        return savedAccount;
    }

    private Integer countAccountsByClientAndFinancialInstitution(String financialInstitutionId, String clientId) {
        final List<Account> clientAccounts = roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof AccountOwnerRole).flatMap(role -> {
                    return accountRepository.findAllByPartyRole_Id(role.getId()).stream();
                }).collect(Collectors.toList());
        final List<Account> accountsAtFi = clientAccounts.stream().filter(account -> {
            final Optional<AccountServicerRole> thisFiAccountServicerRoleOptional = account.getPartyRole().stream().filter(accountPartyRole -> accountPartyRole instanceof AccountServicerRole)
                    .map(accountPartyRole -> (AccountServicerRole) accountPartyRole)
                    .filter(accountServicerRole -> {
                        final Optional<RolePlayer> fiRolePlayerOptional = accountServicerRole.getPlayer().stream().filter(rolePlayer -> rolePlayer.getId().equals(financialInstitutionId))
                                .findAny();
                        return fiRolePlayerOptional.isPresent();
                    }).findAny();
            return thisFiAccountServicerRoleOptional.isPresent();
        }).collect(Collectors.toList());
        return accountsAtFi.size();
    }

    @Override
    public Optional<VirtualCodeAvailabilityCheckDTO> checkVirtualCodeAvailability(String virtualCode) {
        String sanitisedVirtualCode = Optional.ofNullable(virtualCode).map(String::toLowerCase).orElse("");
        return accountRepository.findAccountByVirtualCode(sanitisedVirtualCode)
                .map(account -> Optional.of(VirtualCodeAvailabilityCheckDTO.builder()
                        .accountId(account.getId())
                        .accountName(Optional.ofNullable(account.getIdentification()).map(AccountIdentification::getName).orElse(""))
                        .available(false)
                        .virtualCode(account.getVirtualCode())
                        .build()))
                .orElse(Optional.of(VirtualCodeAvailabilityCheckDTO.builder()
                        .accountId("")
                        .accountName("")
                        .available(true)
                        .virtualCode(virtualCode)
                        .build()));
    }

    @Override
    public Optional<AccountSearchResult> searchAccountByVirtualId(String virtualId) {
        try {
            VirtualIdComponents virtualIdComponents = getVirtualIdComponents(virtualId);
            if (virtualIdComponents.isFetchMaster()) {
                return fetchMasterAccount(virtualId, virtualIdComponents);
            } else {
                return fetchNonMasterAccount(virtualId, virtualIdComponents);
            }
        } catch (VirtualIdSyntaxException e) {
            log.error("An error occurred while parsing virtual ID: {}", e.getMessage());
            return Optional.ofNullable(AccountSearchResult.builder()
                    .accountId("")
                    .accountName("")
                    .accountNumber("")
                    .accountOwnerName("")
                    .accountOwnerId("")
                    .virtualId(virtualId)
                    .narrative(e.getMessage())
                    .build());
        }
    }

    private Optional<AccountSearchResult> fetchMasterAccount(String virtualId, VirtualIdComponents virtualIdComponents) {
        return partyService.getPartyByVirtualId(virtualIdComponents.getVirtualId())
                .map(party -> {
                    return getAccountsByClientId(party.getId()).stream().filter(account -> account.getMaster()).findAny()
                            .map(account -> {
                                final StringBuilder fiId = new StringBuilder();
                                account.getPartyRole().stream().filter(r -> r instanceof AccountServicerRole)
                                        .map(r -> (AccountServicerRole) r)
                                        .findFirst().ifPresent(accountServicerRole -> {
                                            accountServicerRole.getPlayer().stream().findFirst().ifPresent(rolePlayer -> {
                                                fiId.append(rolePlayer.getId());
                                            });
                                });
                                return Optional.ofNullable(AccountSearchResult.builder()
                                        .financialInstitutionId(fiId.toString())
                                        .accountId(account.getId())
                                        .accountName(account.getIdentification().getName())
                                        .accountNumber(account.getIdentification().getNumber())
                                        .virtualId(virtualId)
                                        .bpn(party.getBusinessPartnerNumber())
                                        .accountOwnerName(party.getName())
                                        .accountOwnerId(party.getId())
                                        .narrative("Account found")
                                        .build());
                            })
                            .orElse(Optional.ofNullable(AccountSearchResult.builder()
                                    .financialInstitutionId("")
                                    .accountId("")
                                    .accountName("")
                                    .accountNumber("")
                                    .bpn(party.getBusinessPartnerNumber())
                                    .accountOwnerName(party.getName())
                                    .accountOwnerId(party.getId())
                                    .virtualId(virtualId)
                                    .narrative("Failed to find the account matching the given virtual id.")
                                    .build()));
                }).orElse(Optional.ofNullable(AccountSearchResult.builder()
                        .financialInstitutionId("")
                        .accountId("")
                        .accountName("")
                        .accountNumber("")
                        .bpn("")
                        .accountOwnerName("")
                        .accountOwnerId("")
                        .virtualId(virtualId)
                        .narrative("Failed to find the owner of the provided virtual Id")
                        .build()));
    }

    private Optional<AccountSearchResult> fetchNonMasterAccount(String virtualId, VirtualIdComponents virtualIdComponents) {
        return partyService.getPartyByVirtualId(virtualIdComponents.getVirtualId())
                .map(party -> {
                    return financialInstitutionService.getFinancialInstitutionByVirtualSuffix(virtualIdComponents.getVirtualSuffix())
                            .map(financialInstitution -> {
                                return findAccountByClientAndFinancialInstitutionAndVirtualId(financialInstitution.getId(), party.getId(), virtualIdComponents)
                                        .map(account -> Optional.ofNullable(AccountSearchResult.builder()
                                                .financialInstitutionId(financialInstitution.getId())
                                                .accountId(account.getId())
                                                .accountName(account.getIdentification().getName())
                                                .accountNumber(account.getIdentification().getNumber())
                                                .virtualId(virtualId)
                                                .bpn(party.getBusinessPartnerNumber())
                                                .accountOwnerName(party.getName())
                                                .accountOwnerId(party.getId())
                                                .narrative("Account found")
                                                .build()))
                                        .orElse(Optional.ofNullable(AccountSearchResult.builder()
                                                .financialInstitutionId(financialInstitution.getId())
                                                .accountId("")
                                                .accountName("")
                                                .accountNumber("")
                                                .bpn(party.getBusinessPartnerNumber())
                                                .accountOwnerName(party.getName())
                                                .accountOwnerId(party.getId())
                                                .virtualId(virtualId)
                                                .narrative("Failed to find the account matching the given virtual id.")
                                                .build()));
                            }).orElse(Optional.ofNullable(AccountSearchResult.builder()
                                    .financialInstitutionId("")
                                    .accountId("")
                                    .accountName("")
                                    .accountNumber("")
                                    .bpn(party.getBusinessPartnerNumber())
                                    .accountOwnerName(party.getName())
                                    .accountOwnerId(party.getId())
                                    .virtualId(virtualId)
                                    .narrative("Failed to find the financial institution specified in the virtual Id")
                                    .build()));
                }).orElse(Optional.ofNullable(AccountSearchResult.builder()
                        .financialInstitutionId("")
                        .accountId("")
                        .accountName("")
                        .accountNumber("")
                        .bpn("")
                        .accountOwnerName("")
                        .accountOwnerId("")
                        .virtualId(virtualId)
                        .narrative("Failed to find the owner of the provided virtual Id")
                        .build()));
    }

    private Optional<Account> findAccountByClientAndFinancialInstitutionAndVirtualId(String financialInstitutionId, String clientId, VirtualIdComponents virtualIdComponents) {
        final List<Account> clientAccounts = roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof AccountOwnerRole).flatMap(role -> {
                    return accountRepository.findAllByPartyRole_Id(role.getId()).stream();
                }).collect(Collectors.toList());
        log.info("Accounts found for client {}: {}", clientId, clientAccounts);
        final List<Account> accountsAtFi = clientAccounts.stream().filter(account -> {
            final List<AccountServicerRole> thisFiAccountServicerRoles = account.getPartyRole().stream()
                    .filter(accountPartyRole -> accountPartyRole instanceof AccountServicerRole)
                    .map(accountPartyRole -> (AccountServicerRole) accountPartyRole)
                    .filter(accountServicerRole -> {
                        final List<RolePlayer> rolePlayers = accountServicerRole.getPlayer().stream()
                                .filter(rolePlayer -> rolePlayer.getId().equals(financialInstitutionId))
                                .collect(Collectors.toList());
                        return rolePlayers.size() > 0;
                    }).collect(Collectors.toList());
            return thisFiAccountServicerRoles.size() > 0;
        }).collect(Collectors.toList());
        log.info("Accounts found at FI {}: {}", financialInstitutionId, accountsAtFi);
        return accountsAtFi.stream().filter(account -> {
            return (account.getVirtualCode().equals(virtualIdComponents.getVirtualCode())) || (account.getVirtualNumber() == virtualIdComponents.getVirtualNumber());
        }).findFirst();
    }

    public static void main(String[] args) throws VirtualIdSyntaxException {
        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred#2@cbz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred#2@c@bz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("#alfred2@cbz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred#2cbz@");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfr#ed#2@cbz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred#wallet@cbz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred@cbz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred#2#cbz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred@2@cbz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred2cbz");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred#2");
//        final VirtualIdComponents virtualIdComponents = getVirtualIdComponents("alfred@2#cbz");
        System.out.println("Virtual Id components are: " + virtualIdComponents);
    }

    private static VirtualIdComponents getVirtualIdComponents(String virtualId) throws VirtualIdSyntaxException {
        final String[] virtualIdComponents = virtualId.split("#|@");
        log.info("Virtual Id components are: {}", Arrays.toString(virtualIdComponents));
        log.info("Index of @ is {} and length of array is {}", virtualId.indexOf("@"), virtualId.length());
        for (int i = 0; i < virtualIdComponents.length; i++) {
            log.info("Value at {} is {}", i, virtualIdComponents[i]);
        }

        if (StringUtils.isEmpty(virtualId)) throw new VirtualIdSyntaxException("Could not parse empty virtual Id. " +
                "Required format is: VirtualId#VirtualNumberOrCode@VirtualSuffix");
        if (virtualIdComponents.length < 2) {
            return VirtualIdComponents.builder()
                    .virtualId(virtualIdComponents[0])
                    .fetchMaster(true)
                    .build();
        } else if (virtualIdComponents.length > 3) {
            throw new VirtualIdSyntaxException("You have way too many components in your virtual Id. You need at most 3:" +
                    "Format is VirtualId#VirtualNumberOrCode@VirtualSuffix");
        } else {
            if (virtualIdComponents.length == 2) {
                if (!virtualId.contains("@") || virtualId.contains("#")) {
                    throw new VirtualIdSyntaxException("Virtual Id. Expected Virtual Suffix, found none.");
                }
                return VirtualIdComponents.builder()
                        .virtualId(virtualIdComponents[0])
                        .virtualNumber(1)
                        .virtualSuffix(virtualIdComponents[1])
                        .fetchMaster(false)
                        .build();
            } else {
                if (virtualId.contains("#") && virtualId.contains("@")) {
                    final int indexOfPoundSign = virtualId.indexOf("#");
                    final int indexOfAtSymbol = virtualId.indexOf("@");
                    if (indexOfAtSymbol < indexOfPoundSign) {
                        throw new VirtualIdSyntaxException("# should appear before @ in a valid Virtual Id.");
                    } else if (indexOfPoundSign == 0) {
                        throw new VirtualIdSyntaxException("Your # is at a weird position, put it after the virtual Id");
                    } else if (indexOfAtSymbol == virtualId.length() - 1) {
                        throw new VirtualIdSyntaxException("Your @ is at a weird position, put it right before the virtual suffix");
                    } else {
                        final String accountIdentification = virtualIdComponents[1];
                        if (StringUtils.isNumeric(accountIdentification)) {
                            return VirtualIdComponents.builder()
                                    .virtualId(virtualIdComponents[0])
                                    .virtualNumber(Integer.valueOf(accountIdentification))
                                    .virtualSuffix(virtualIdComponents[2])
                                    .fetchMaster(false)
                                    .build();
                        } else {
                            return VirtualIdComponents.builder()
                                    .virtualId(virtualIdComponents[0])
                                    .virtualCode(accountIdentification)
                                    .virtualSuffix(virtualIdComponents[2])
                                    .fetchMaster(false)
                                    .build();
                        }
                    }
                } else {
                    throw new VirtualIdSyntaxException("Virtual Id contains duplicate delimiter.");
                }
            }
        }
    }

    @Override
    public CashAccount deleteCashAccount(String id) {
        Optional<CashAccount> cashAccountOptional = cashAccountRepository.findById(id);
        CashAccount cashAccount = null;
        if (cashAccountOptional.isPresent()) {
            cashAccount = cashAccountOptional.get();
            cashAccount.setEntityStatus(EntityStatus.DELETED);
            cashAccount.getStatus().setStatus(AccountStatusCode.Deleted);
            cashAccount = cashAccountRepository.save(cashAccount);
        }
        return cashAccount;
    }

    @Override
    public CashAccount approveCashAccount(String id) {
        Optional<CashAccount> cashAccountOptional = cashAccountRepository.findById(id);
        CashAccount cashAccount = null;
        if (cashAccountOptional.isPresent()) {
            cashAccount = cashAccountOptional.get();
            cashAccount.setEntityStatus(EntityStatus.ACTIVE);
            cashAccount.getStatus().setStatus(AccountStatusCode.Enabled);
            cashAccount = cashAccountRepository.save(cashAccount);
        }
        return cashAccount;
    }

    @Override
    public CashAccount rejectCashAccount(String id) {
        Optional<CashAccount> cashAccountOptional = cashAccountRepository.findById(id);
        CashAccount cashAccount = null;
        if (cashAccountOptional.isPresent()) {
            cashAccount = cashAccountOptional.get();
            cashAccount.setEntityStatus(EntityStatus.DISAPPROVED);
            cashAccount.getStatus().setStatus(AccountStatusCode.Disabled);
            cashAccount = cashAccountRepository.save(cashAccount);
        }
        return cashAccount;
    }

    public CashAccount findAccountById(String id) {
        Optional<CashAccount> cashAccountOptional = this.cashAccountRepository.findById(id);
        return cashAccountOptional.orElse(null);
    }

    @Override
    public Optional<CashAccount> findAccountByIban(String iBan) {
        return Optional.ofNullable(this.cashAccountRepository.findCashAccountByIdentificationIBAN(iBan));
    }

    @Override
    public Optional<CashAccount> findAccountByNumber(String number) {
        return Optional.ofNullable(this.cashAccountRepository.findCashAccountByIdentificationNumber(number));
    }

    @Override
    public Map<String, CashAccount> findAccountsByIbansIn(List<String> iBans) {
        final List<String> ibansList = iBans.stream().collect(Collectors.toSet()).stream().collect(Collectors.toList());
        log.info("IBANS are: {}", ibansList);
        return cashAccountRepository.findCashAccountsByIdentificationIbansIn(ibansList).stream()
                .collect(Collectors.toMap(cashAccount -> cashAccount.getIdentification().getIBAN(), Function.identity()));
    }

    @Override
    public CashAccount updateCashAccount(CashAccountDTO cashAccountDTO) {
        CashAccount cashAccount = convertDtoToEntity(cashAccountDTO);
        log.info("cashAccountId in impl: " + cashAccount.getId());

        CashAccount savedAccount = accountRepository.save(cashAccount);
        auditLoggerConfiguration.auditLoggerUtil().logActivity(cashAccountDTO.getUsername(), AuditEvents.UPDATE_ACCOUNT, savedAccount);

        return savedAccount;
    }

    @Override
    public List<Account> getAccountsByClientId(String clientId) {
        return roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof AccountOwnerRole).flatMap(role -> {
                    return accountRepository.findAllByPartyRole_Id(role.getId()).stream();
                }).collect(Collectors.toList());
    }

    @Override
    public List<CashAccount> getCashAccountsByClientId(String clientId) {
        return roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof AccountOwnerRole).flatMap(role -> {
                    return cashAccountRepository.findAllByPartyRoleId(role.getId()).stream();
                }).collect(Collectors.toList());
    }

    @Override
    public List<CashAccount> searchAccounts(String searchTerm, String fiId) {
        log.info("Search string in impl: " + searchTerm);
        log.info("FI id in impl: " + fiId);

        FullTextEntityManager fullTextEm = org.hibernate.search.jpa.Search
                .getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEm.getSearchFactory().buildQueryBuilder().forEntity(AccountIdentification.class).get();

        FullTextQuery fullTextQuery = fullTextEm.createFullTextQuery(queryBuilder.keyword()
                .onFields("iBAN", "bBAN", "uPIC", "name", "number")
                .matching(searchTerm)
                .createQuery(), AccountIdentification.class);

        List<AccountIdentification> resultList = new ArrayList<>();
        try {
            resultList = fullTextQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.info("Found Account Identifications: " + resultList);
        List<CashAccount> accounts = new ArrayList<>();

        /**
         * Use account identification to get accounts for fi
         */
        resultList.forEach(identification -> {
                    identification.getAccount().getPartyRole().stream()
                            .filter(role -> role instanceof AccountServicerRole).findFirst().ifPresent(accPartyRole -> {
                        CashAccount account;
                        FinancialInstitution financialInstitution;
                        financialInstitution = (FinancialInstitution) accPartyRole.getPlayer().get(0);
                        if (StringUtils.isNoneBlank(financialInstitution.getId()) && financialInstitution.getId().equals(fiId)) {
                            account = (CashAccount) identification.getAccount();
                            accounts.add(account);
                        }
                    });
                }
        );
        return accounts;
    }

    @Override
    public CashAccount aggregateAccountInfoRequest(RequestInfo requestInfo) {
        CashAccount cashAccount = this.findAccountById(requestInfo.getId());
        List<CashAccount> cashAccounts = Arrays.asList(cashAccount);
        switch (requestInfo.getRequestType()) {
            case "BalanceEnquiry":
                log.info("Processing the balance enquiry request");
                accountInfoInitiationService.initiateAccountInfoRequest(cashAccounts, "camt.052.001.07.aqs");
                break;
            case "AccountStatement":
                log.info("Processing the account statement request");
                accountInfoInitiationService.initiateAccountInfoRequest(cashAccounts, "camt.053.001.07");
                break;
            default:
                log.info("The request type is {} unknown can't be processed.");
        }
        return cashAccount;
    }

    @Override
    public String accountBalanceRequest(String clientId) {
        List<CashAccount> cashAccounts = this.getCashAccountsByClientId(clientId);

        if(cashAccounts != null && !cashAccounts.isEmpty()){
            accountInfoInitiationService.initiateAccountInfoRequest(cashAccounts, "camt.052.001.07.aqs");
            return "Account Balance Requested Successfully";
        }
        return "This client does not have any accounts";
    }
}
