package zw.co.esolutions.zpas.services.impl.taxclearance.cache;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.services.impl.taxclearance.TaxClearanceJsonResponse;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.inject.Singleton;

/**
 * Created by alfred on 21 May 2019.
 */
@Slf4j
@Singleton
@Component
public class TaxClearanceCache {
    Config cfg;
    HazelcastInstance instance;
    IMap<String, TaxClearanceJsonResponse> cache;

    public TaxClearanceCache() {
        cfg = new Config();
        cfg.setClassLoader(this.getClass().getClassLoader());
        instance = Hazelcast.newHazelcastInstance(cfg);
        cache = instance.getMap(SystemConstants.CACHE_NAME_TAX_CLEARANCE_INFO);
        log.info("--------- init Tax Clearance Hazelcast cache ----------");
    }

    public IMap<String, TaxClearanceJsonResponse> getTaxClearanceCacheInstance() {
        return cache;
    }

}
