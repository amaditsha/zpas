package zw.co.esolutions.zpas.services.impl.process.pojo;

import lombok.Data;
import lombok.Value;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;

import java.time.OffsetDateTime;

//@Value
@Data
public class DebitAuthorisationDTO {
    private String paymentInvestigationCaseId;
    private String paymentInvestigationCaseIdentification;
    private String assignmentIdentification;
    private PaymentInvestigationCase paymentInvestigationCase;
    private Boolean authorisation;
    private String authorisationString;
    private String reason;
    private String valueDateToDebitString;
    private OffsetDateTime valueDateToDebit;

    public DebitAuthorisationDTO(String paymentInvestigationCaseId, String assignmentIdentification, PaymentInvestigationCase paymentInvestigationCase,
                                 Boolean authorisation, String authorisationString, String reason, String valueDateToDebitString,
                                 OffsetDateTime valueDateToDebit){
        this.paymentInvestigationCaseId = paymentInvestigationCaseId;
        this.assignmentIdentification = assignmentIdentification;
        this.paymentInvestigationCase = paymentInvestigationCase;
        this.authorisation = authorisation;
        this.authorisationString = authorisationString;
        this.reason = reason;
        this.valueDateToDebitString = valueDateToDebitString;
        this.valueDateToDebit = valueDateToDebit;
    }

    @Override
    public String toString() {
        return "DebitAuthorisationDTO{" +
                "paymentInvestigationCaseId='" + paymentInvestigationCaseId + '\'' +
                ", authorisation=" + authorisation +
                ", reason='" + reason + '\'' +
                ", valueDateToDebit=" + valueDateToDebit +
                '}';
    }
}
