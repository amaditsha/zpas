package zw.co.esolutions.zpas.services.impl.payments.verification;
import zw.co.esolutions.zpas.iso.msg.acmt023_001_02.PostalAddress6;
import zw.co.esolutions.zpas.iso.msg.acmt023_001_02.FinancialInstitutionIdentification8;
import zw.co.esolutions.zpas.iso.msg.acmt023_001_02.BranchData2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.iso.msg.acmt023_001_02.Document;
import zw.co.esolutions.zpas.iso.msg.acmt023_001_02.*;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.impl.payments.cache.PaymentsCacheProcessor;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 16 May 2019
 */
@Slf4j
@Component
@Transactional
public class IdentificationVerificationProcessor {
    @Autowired
    private PaymentsCacheProcessor cacheProcessor;

    static JAXBContext jaxbContextForAcmt023;

    static {
        try {
            jaxbContextForAcmt023 = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.acmt023_001_02");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private MessageProducer messageProducer;

    public void initiateIdentificationVerificationRequest(Payment payment) throws PaymentRequestInvalidArgumentException {

        log.info("Initialising payment cache...");
        cacheProcessor.getPaymentStatusInfo(payment.getId());

        final IdentificationVerificationRequestV02 identificationVerificationRequestV02 = buildIdentificationVerificationRequest(payment);

        try {
            //send message to producer
            String destinationBic = identificationVerificationRequestV02.getAssgnmt().getAssgne().getAgt().getFinInstnId().getBICFI();
            log.info("Destination Bic present... " + destinationBic);
            messageProducer.sendMessageToHub(buildXml(identificationVerificationRequestV02), destinationBic);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private String buildXml(IdentificationVerificationRequestV02 identificationVerificationRequestV02) throws JAXBException {
        log.info("Building ACMT.023 XML message..");
        final Document document = new Document();
        document.setIdVrfctnReq(identificationVerificationRequestV02);

        Marshaller jaxbMarshaller = jaxbContextForAcmt023.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);

        return xml;
    }

    private IdentificationVerificationRequestV02 buildIdentificationVerificationRequest(Payment payment) throws PaymentRequestInvalidArgumentException {
        final IdentificationVerificationRequestV02 identificationVerificationRequestV02 = new IdentificationVerificationRequestV02();

        final String paymentIdentification = payment.getPaymentRelatedIdentifications().stream().findFirst()
                .map(PaymentIdentification::getEndToEndIdentification)
                .orElseThrow(() -> new PaymentRequestInvalidArgumentException("End to End Identification not set for this payment."));

        final IdentificationAssignment2 identificationAssignment2 = new IdentificationAssignment2();
        identificationAssignment2.setMsgId(paymentIdentification);
        identificationAssignment2.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        final CashAccount account = payment.getAccount();
        final Party accountOwner = account.getAccountOwner();
        final FinancialInstitution financialInstitution = account.getFinancialInstitution();

        /**
         * Creator Party
         */
        final Party12Choice creatorParty12Choice = new Party12Choice();
        final PartyIdentification43 creatorPartyIdentification43 = new PartyIdentification43();
        creatorPartyIdentification43.setNm(accountOwner.getName());
        final PostalAddress6 creatorPostalAddress6 = new PostalAddress6();
        creatorPostalAddress6.setAdrTp(AddressType2Code.BIZZ);
        accountOwner.getContactPoint().stream().filter(cp -> cp instanceof PostalAddress)
                .map(cp -> (PostalAddress) cp)
                .findFirst().ifPresent(postalAddress -> {
            creatorPostalAddress6.setDept(postalAddress.getDepartment());
            creatorPostalAddress6.setSubDept(postalAddress.getSubDepartment());
            creatorPostalAddress6.setStrtNm(postalAddress.getStreetName());
            creatorPostalAddress6.setPstCd(postalAddress.getPostCodeIdentification());
            creatorPostalAddress6.setTwnNm(postalAddress.getTownName());
            creatorPostalAddress6.setCtrySubDvsn(postalAddress.getProvince());
            creatorPostalAddress6.setCtry(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
        });
        creatorPartyIdentification43.setPstlAdr(creatorPostalAddress6);
        final Party11Choice creatorParty11Choice = new Party11Choice();
        final OrganisationIdentification8 creatorOrganisationIdentification8 = new OrganisationIdentification8();
        creatorOrganisationIdentification8.setAnyBIC(financialInstitution.getAnyBIC());
        creatorParty11Choice.setOrgId(creatorOrganisationIdentification8);
        final PersonIdentification5 creatorPersonIdentification5 = new PersonIdentification5();
        final DateAndPlaceOfBirth creatorDateAndPlaceOfBirth = new DateAndPlaceOfBirth();
        final ContactDetails2 creatorContactDetails2 = new ContactDetails2();
        accountOwner.getContactPoint().stream().filter(cp -> cp instanceof ElectronicAddress)
                .map(cp -> (ElectronicAddress) cp)
                .findFirst().ifPresent(electronicAddress -> {
            creatorContactDetails2.setEmailAdr(electronicAddress.getEmailAddress());
            creatorContactDetails2.setOthr(electronicAddress.getURLAddress());
        });
        accountOwner.getContactPoint().stream().filter(cp -> cp instanceof PhoneAddress)
                .map(cp -> (PhoneAddress) cp)
                .findFirst().ifPresent(phoneAddress -> {
            creatorContactDetails2.setPhneNb(phoneAddress.getPhoneNumber());
            creatorContactDetails2.setMobNb(phoneAddress.getMobileNumber());
            creatorContactDetails2.setFaxNb(phoneAddress.getFaxNumber());
        });
        creatorContactDetails2.setNm(accountOwner.getName());
        if (accountOwner instanceof Person) {
            final Person person = (Person) accountOwner;
            final OffsetDateTime birthDate = person.getBirthDate();
            creatorDateAndPlaceOfBirth.setBirthDt(XmlDateUtil.getXmlDate(Optional.ofNullable(birthDate).orElse(OffsetDateTime.now())));
            Optional.ofNullable(person.getPlaceOfBirth()).ifPresent(birthLocation -> {
                birthLocation.getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    creatorDateAndPlaceOfBirth.setPrvcOfBirth(postalAddress.getProvince());
                    creatorDateAndPlaceOfBirth.setCityOfBirth(postalAddress.getTownName());
                    creatorDateAndPlaceOfBirth.setCtryOfBirth(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                    creatorPartyIdentification43.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                });
            });
        }
        if (accountOwner instanceof Organisation) {
            final Organisation organisation = (Organisation) accountOwner;
            final OffsetDateTime registrationDate = organisation.getRegistrationDate();
            creatorDateAndPlaceOfBirth.setBirthDt(XmlDateUtil.getXmlDate(Optional.ofNullable(registrationDate).orElse(OffsetDateTime.now())));
            Optional.ofNullable(organisation.getPlaceOfRegistration()).ifPresent(registrationLocation -> {
                registrationLocation.getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    creatorDateAndPlaceOfBirth.setPrvcOfBirth(postalAddress.getProvince());
                    creatorDateAndPlaceOfBirth.setCityOfBirth(postalAddress.getTownName());
                    creatorDateAndPlaceOfBirth.setCtryOfBirth(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                    creatorPartyIdentification43.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                });
            });
        }
        creatorPersonIdentification5.setDtAndPlcOfBirth(creatorDateAndPlaceOfBirth);
        creatorParty11Choice.setPrvtId(creatorPersonIdentification5);
        creatorPartyIdentification43.setId(creatorParty11Choice);
        creatorContactDetails2.setNmPrfx(NamePrefix1Code.DOCT);
        creatorPartyIdentification43.setCtctDtls(creatorContactDetails2);
        creatorParty12Choice.setPty(creatorPartyIdentification43);
        final BranchAndFinancialInstitutionIdentification5 creatorBranchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();
        final FinancialInstitutionIdentification8 creatorFinancialInstitutionIdentification8 = new FinancialInstitutionIdentification8();
        creatorFinancialInstitutionIdentification8.setBICFI(financialInstitution.getAnyBIC());
        creatorFinancialInstitutionIdentification8.setNm(financialInstitution.getName());
        final PostalAddress6 creatorFIPostalAddress6 = new PostalAddress6();
        creatorFIPostalAddress6.setAdrTp(AddressType2Code.BIZZ);
        financialInstitution.getPlaceOfRegistration().getAddress().stream().findFirst().ifPresent(postalAddress -> {
            creatorFIPostalAddress6.setDept(postalAddress.getDepartment());
            creatorFIPostalAddress6.setSubDept(postalAddress.getSubDepartment());
            creatorFIPostalAddress6.setStrtNm(postalAddress.getStreetName());
            creatorFIPostalAddress6.setBldgNb(postalAddress.getBuildingIdentification());
            creatorFIPostalAddress6.setPstCd(postalAddress.getPostCodeIdentification());
            creatorFIPostalAddress6.setTwnNm(postalAddress.getTownName());
            creatorFIPostalAddress6.setCtrySubDvsn(postalAddress.getProvince());
            creatorFIPostalAddress6.setCtry(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
        });
        creatorFinancialInstitutionIdentification8.setPstlAdr(creatorFIPostalAddress6);
        final GenericFinancialIdentification1 creatorGenericFinancialIdentification1 = new GenericFinancialIdentification1();
        creatorGenericFinancialIdentification1.setId(financialInstitution.getAnyBIC());
        creatorGenericFinancialIdentification1.setIssr(financialInstitution.getName());
        creatorFinancialInstitutionIdentification8.setOthr(creatorGenericFinancialIdentification1);
        creatorBranchAndFinancialInstitutionIdentification5.setFinInstnId(creatorFinancialInstitutionIdentification8);
        creatorParty12Choice.setAgt(creatorBranchAndFinancialInstitutionIdentification5);
        identificationAssignment2.setCretr(creatorParty12Choice);

        /**
         * First Agent
         */
        final BranchAndFinancialInstitutionIdentification5 firstAgentBranchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();
        final FinancialInstitutionIdentification8 firstAgentFinancialInstitutionIdentification8 = new FinancialInstitutionIdentification8();
        firstAgentFinancialInstitutionIdentification8.setBICFI(financialInstitution.getAnyBIC());
        final ClearingSystemMemberIdentification2 firstAgentClearingSystemMemberIdentification2 = new ClearingSystemMemberIdentification2();
        firstAgentClearingSystemMemberIdentification2.setMmbId(financialInstitution.getAnyBIC());
        firstAgentFinancialInstitutionIdentification8.setClrSysMmbId(firstAgentClearingSystemMemberIdentification2);
        firstAgentFinancialInstitutionIdentification8.setNm(financialInstitution.getName());
        final PostalAddress6 firstAgentPostalAddress6 = new PostalAddress6();
        firstAgentPostalAddress6.setAdrTp(AddressType2Code.ADDR);
        financialInstitution.getPlaceOfRegistration().getAddress().stream().findFirst().ifPresent(postalAddress -> {
            firstAgentPostalAddress6.setDept(postalAddress.getDepartment());
            firstAgentPostalAddress6.setSubDept(postalAddress.getSubDepartment());
            firstAgentPostalAddress6.setStrtNm(postalAddress.getStreetName());
            firstAgentPostalAddress6.setBldgNb(postalAddress.getBuildingIdentification());
            firstAgentPostalAddress6.setPstCd(postalAddress.getPostCodeIdentification());
            firstAgentPostalAddress6.setTwnNm(postalAddress.getTownName());
            firstAgentPostalAddress6.setCtrySubDvsn(postalAddress.getProvince());
            firstAgentPostalAddress6.setCtry(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
        });
        firstAgentFinancialInstitutionIdentification8.setPstlAdr(firstAgentPostalAddress6);
        final GenericFinancialIdentification1 firstAgentGenericFinancialIdentification1 = new GenericFinancialIdentification1();
        firstAgentGenericFinancialIdentification1.setId(financialInstitution.getAnyBIC());
        firstAgentFinancialInstitutionIdentification8.setOthr(firstAgentGenericFinancialIdentification1);
        firstAgentBranchAndFinancialInstitutionIdentification5.setFinInstnId(firstAgentFinancialInstitutionIdentification8);
        identificationAssignment2.setFrstAgt(firstAgentBranchAndFinancialInstitutionIdentification5);

        /**
         * Assigner Party
         */
        final Party12Choice assignerParty12Choice = new Party12Choice();
        final PartyIdentification43 assignerPartyIdentification43 = new PartyIdentification43();
        assignerPartyIdentification43.setNm(accountOwner.getName());
        final PostalAddress6 assignerPostalAddress6 = new PostalAddress6();
        assignerPostalAddress6.setAdrTp(AddressType2Code.ADDR);
        accountOwner.getContactPoint().stream().filter(cp -> cp instanceof PostalAddress)
                .map(cp -> (PostalAddress) cp)
                .findFirst().ifPresent(postalAddress -> {
            assignerPostalAddress6.setDept(postalAddress.getDepartment());
            assignerPostalAddress6.setSubDept(postalAddress.getSubDepartment());
            assignerPostalAddress6.setStrtNm(postalAddress.getStreetName());
            assignerPostalAddress6.setBldgNb(postalAddress.getBuildingIdentification());
            assignerPostalAddress6.setPstCd(postalAddress.getPostCodeIdentification());
            assignerPostalAddress6.setTwnNm(postalAddress.getTownName());
            assignerPostalAddress6.setCtrySubDvsn(postalAddress.getProvince());
            assignerPostalAddress6.setCtry(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
        });
        assignerPartyIdentification43.setPstlAdr(assignerPostalAddress6);
        final Party11Choice assignerParty11Choice = new Party11Choice();
        final OrganisationIdentification8 assignerOrganisationIdentification8 = new OrganisationIdentification8();
        assignerOrganisationIdentification8.setAnyBIC("");
        assignerParty11Choice.setOrgId(assignerOrganisationIdentification8);
        final PersonIdentification5 assignerPersonIdentification5 = new PersonIdentification5();
        final DateAndPlaceOfBirth assignerDateAndPlaceOfBirth = new DateAndPlaceOfBirth();
        creatorContactDetails2.setNm(accountOwner.getName());
        if (accountOwner instanceof Person) {
            final Person person = (Person) accountOwner;
            final OffsetDateTime birthDate = person.getBirthDate();
            assignerDateAndPlaceOfBirth.setBirthDt(XmlDateUtil.getXmlDate(Optional.ofNullable(birthDate).orElse(OffsetDateTime.now())));
            Optional.ofNullable(person.getPlaceOfBirth()).ifPresent(birthLocation -> {
                birthLocation.getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    assignerDateAndPlaceOfBirth.setPrvcOfBirth(postalAddress.getProvince());
                    assignerDateAndPlaceOfBirth.setCityOfBirth(postalAddress.getTownName());
                    assignerDateAndPlaceOfBirth.setCtryOfBirth(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                    assignerPartyIdentification43.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                });
            });
        }
        if (accountOwner instanceof Organisation) {
            final Organisation organisation = (Organisation) accountOwner;
            final OffsetDateTime registrationDate = organisation.getRegistrationDate();
            assignerDateAndPlaceOfBirth.setBirthDt(XmlDateUtil.getXmlDate(Optional.ofNullable(registrationDate).orElse(OffsetDateTime.now())));
            Optional.ofNullable(organisation.getPlaceOfRegistration()).ifPresent(registrationLocation -> {
                registrationLocation.getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    assignerDateAndPlaceOfBirth.setPrvcOfBirth(postalAddress.getProvince());
                    assignerDateAndPlaceOfBirth.setCityOfBirth(postalAddress.getTownName());
                    assignerDateAndPlaceOfBirth.setCtryOfBirth(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                    assignerPartyIdentification43.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                });
            });
        }
        assignerPersonIdentification5.setDtAndPlcOfBirth(assignerDateAndPlaceOfBirth);
        assignerParty11Choice.setPrvtId(assignerPersonIdentification5);
        assignerPartyIdentification43.setId(assignerParty11Choice);
        final ContactDetails2 assignerContactDetails2 = new ContactDetails2();
        accountOwner.getContactPoint().stream().filter(cp -> cp instanceof ElectronicAddress)
                .map(cp -> (ElectronicAddress) cp)
                .findFirst().ifPresent(electronicAddress -> {
            assignerContactDetails2.setEmailAdr(electronicAddress.getEmailAddress());
            assignerContactDetails2.setOthr(electronicAddress.getURLAddress());
        });
        accountOwner.getContactPoint().stream().filter(cp -> cp instanceof PhoneAddress)
                .map(cp -> (PhoneAddress) cp)
                .findFirst().ifPresent(phoneAddress -> {
            assignerContactDetails2.setPhneNb(phoneAddress.getPhoneNumber());
            assignerContactDetails2.setMobNb(phoneAddress.getMobileNumber());
            assignerContactDetails2.setFaxNb(phoneAddress.getFaxNumber());
        });
        assignerContactDetails2.setNmPrfx(NamePrefix1Code.DOCT);
        assignerContactDetails2.setNm(accountOwner.getName());
        assignerPartyIdentification43.setCtctDtls(assignerContactDetails2);
        assignerParty12Choice.setPty(assignerPartyIdentification43);
        assignerParty12Choice.setAgt(firstAgentBranchAndFinancialInstitutionIdentification5);
        identificationAssignment2.setAssgnr(assignerParty12Choice);

        /**
         * Assignee Party
         */
        final Party12Choice assigneeParty12Choice = new Party12Choice();
        final PartyIdentification43 assigneePartyIdentification43 = new PartyIdentification43();
        assigneePartyIdentification43.setNm(financialInstitution.getName());
        final PostalAddress6 assigneePostalAddress6 = new PostalAddress6();
        assigneePostalAddress6.setAdrTp(AddressType2Code.ADDR);

        financialInstitution.getPlaceOfRegistration().getAddress().stream().findFirst().ifPresent(postalAddress -> {
            assigneePostalAddress6.setDept(postalAddress.getDepartment());
            assigneePostalAddress6.setSubDept(postalAddress.getSubDepartment());
            assigneePostalAddress6.setStrtNm(postalAddress.getStreetName());
            assigneePostalAddress6.setBldgNb(postalAddress.getBuildingIdentification());
            assigneePostalAddress6.setPstCd(postalAddress.getPostCodeIdentification());
            assigneePostalAddress6.setTwnNm(postalAddress.getTownName());
            assigneePostalAddress6.setCtrySubDvsn(postalAddress.getProvince());
            assigneePostalAddress6.setCtry(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
        });
        assigneePartyIdentification43.setPstlAdr(assigneePostalAddress6);
        final Party11Choice assigneeParty11Choice = new Party11Choice();
        final OrganisationIdentification8 assigneeOrganisationIdentification8 = new OrganisationIdentification8();
        assigneeOrganisationIdentification8.setAnyBIC(financialInstitution.getAnyBIC());
        assigneeParty11Choice.setOrgId(assigneeOrganisationIdentification8);
        assigneePartyIdentification43.setId(assigneeParty11Choice);
        assigneePartyIdentification43.setCtryOfRes("");
        final ContactDetails2 assigneeContactDetails2 = new ContactDetails2();
        assigneeContactDetails2.setNmPrfx(NamePrefix1Code.DOCT);
        financialInstitution.getContactPoint().stream().filter(cp -> cp instanceof ElectronicAddress)
                .map(cp -> (ElectronicAddress) cp)
                .findFirst().ifPresent(electronicAddress -> {
            assigneeContactDetails2.setEmailAdr(electronicAddress.getEmailAddress());
            assigneeContactDetails2.setOthr(electronicAddress.getURLAddress());
        });
        financialInstitution.getContactPoint().stream().filter(cp -> cp instanceof PhoneAddress)
                .map(cp -> (PhoneAddress) cp)
                .findFirst().ifPresent(phoneAddress -> {
            assigneeContactDetails2.setPhneNb(phoneAddress.getPhoneNumber());
            assigneeContactDetails2.setMobNb(phoneAddress.getMobileNumber());
            assigneeContactDetails2.setFaxNb(phoneAddress.getFaxNumber());
        });
        assigneeContactDetails2.setNm(financialInstitution.getName());
        assigneePartyIdentification43.setCtctDtls(assigneeContactDetails2);
        assigneeParty12Choice.setPty(assigneePartyIdentification43);
        final BranchAndFinancialInstitutionIdentification5 assigneeBranchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();
        final FinancialInstitutionIdentification8 assigneeFinancialInstitutionIdentification8 = new FinancialInstitutionIdentification8();
        assigneeFinancialInstitutionIdentification8.setBICFI(financialInstitution.getAnyBIC());
        final ClearingSystemMemberIdentification2 assigneeClearingSystemMemberIdentification2 = new ClearingSystemMemberIdentification2();
        assigneeClearingSystemMemberIdentification2.setMmbId(financialInstitution.getAnyBIC());
        assigneeFinancialInstitutionIdentification8.setClrSysMmbId(assigneeClearingSystemMemberIdentification2);
        assigneeFinancialInstitutionIdentification8.setNm(financialInstitution.getName());
        final GenericFinancialIdentification1 assigneeGenericFinancialIdentification1 = new GenericFinancialIdentification1();
        assigneeGenericFinancialIdentification1.setId(financialInstitution.getAnyBIC());
        assigneeFinancialInstitutionIdentification8.setOthr(assigneeGenericFinancialIdentification1);
        assigneeBranchAndFinancialInstitutionIdentification5.setFinInstnId(assigneeFinancialInstitutionIdentification8);
        final BranchData2 assigneeBranchData2 = new BranchData2();
        assigneeBranchData2.setId(financialInstitution.getAnyBIC());
        assigneeBranchData2.setNm(financialInstitution.getName());
        assigneeBranchData2.setPstlAdr(assigneePostalAddress6);
        assigneeBranchAndFinancialInstitutionIdentification5.setBrnchId(assigneeBranchData2);

        assigneeParty12Choice.setAgt(assigneeBranchAndFinancialInstitutionIdentification5);
        identificationAssignment2.setAssgne(assigneeParty12Choice);

        identificationVerificationRequestV02.setAssgnmt(identificationAssignment2);

        /**
         * Account Identification Verifications
         */
        List<IndividualPayment> individualPayments = new ArrayList<>();
        if (payment instanceof BulkPayment) {
            final List<IndividualPayment> groups = ((BulkPayment) payment).getGroups();
            individualPayments.addAll(groups);
        } else {
            individualPayments.add((IndividualPayment) payment);
        }

        individualPayments.forEach(individualPayment -> {
            if(individualPayment.getEntityStatus() == EntityStatus.DISAPPROVED) {
                log.info("Individual payment {} has already been disapproved, skipping...", individualPayment.getEndToEndId());
            } else {
                final IdentificationVerification2 identificationVerification2 = new IdentificationVerification2();
                identificationVerification2.setId(individualPayment.getEndToEndId());
                final IdentificationInformation2 identificationInformation2 = new IdentificationInformation2();

                individualPayment.getPartyRole().stream().filter(r -> r instanceof CreditorRole)
                        .map(r -> (CreditorRole) r).findFirst().ifPresent(creditorRole -> {
                    CustomParty customParty = null;
                    Party party = null;
                    if (creditorRole.isHasCustomParty()) {
                        customParty = creditorRole.getCustomParty();
                    } else {
                        final Optional<RolePlayer> optionalRolePlayer = creditorRole.getPlayer().stream().findFirst();
                        if (optionalRolePlayer.isPresent()) {
                            party = (Party) optionalRolePlayer.get();
                        }
                    }
                    if (customParty != null) {
                        configurePartyIdentification43FromCustomParty(customParty, identificationVerification2, identificationInformation2);
                    }
                    if (party != null) {
                        configurePartyIdentification43FromParty(party, identificationVerification2, identificationInformation2);
                    }

                    identificationVerificationRequestV02.getVrfctn().add(identificationVerification2);
                });
            }

        });

        return identificationVerificationRequestV02;
    }

    private void configurePartyIdentification43FromParty(Party party, IdentificationVerification2 identificationVerification2, IdentificationInformation2 identificationInformation2) {
        final PartyIdentification43 identificationInformationPartyIdentification43 = new PartyIdentification43();
        identificationInformationPartyIdentification43.setNm(party.getName());
        final PostalAddress6 identificationInformationPostalAddress6 = new PostalAddress6();
        identificationInformationPostalAddress6.setAdrTp(AddressType2Code.ADDR);
        party.getContactPoint().stream().filter(cp -> cp instanceof PostalAddress)
                .map(cp -> (PostalAddress) cp).findFirst().ifPresent(postalAddress -> {
            identificationInformationPostalAddress6.setDept(postalAddress.getDepartment());
            identificationInformationPostalAddress6.setSubDept(postalAddress.getSubDepartment());
            identificationInformationPostalAddress6.setStrtNm(postalAddress.getStreetName());
            identificationInformationPostalAddress6.setBldgNb(postalAddress.getBuildingIdentification());
            identificationInformationPostalAddress6.setPstCd(postalAddress.getPostCodeIdentification());
            identificationInformationPostalAddress6.setTwnNm(postalAddress.getTownName());
            identificationInformationPostalAddress6.setCtrySubDvsn(postalAddress.getProvince());
            identificationInformationPostalAddress6.setCtry(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
        });
        identificationInformationPartyIdentification43.setPstlAdr(identificationInformationPostalAddress6);
        final Party11Choice identificationInformationParty11Choice = new Party11Choice();
        final OrganisationIdentification8 identificationInformationOrganisationIdentification8 = new OrganisationIdentification8();
        identificationInformationOrganisationIdentification8.setAnyBIC("");
        identificationInformationParty11Choice.setOrgId(identificationInformationOrganisationIdentification8);
        final PersonIdentification5 identificationInformationPersonIdentification5 = new PersonIdentification5();
        final DateAndPlaceOfBirth identificationInformationDateAndPlaceOfBirth = new DateAndPlaceOfBirth();
        final ContactDetails2 identificationInformationContactDetails2 = new ContactDetails2();

        party.getContactPoint().stream().filter(cp -> cp instanceof ElectronicAddress)
                .map(cp -> (ElectronicAddress) cp)
                .findFirst().ifPresent(electronicAddress -> {
            identificationInformationContactDetails2.setEmailAdr(electronicAddress.getEmailAddress());
            identificationInformationContactDetails2.setOthr(electronicAddress.getURLAddress());
        });
        party.getContactPoint().stream().filter(cp -> cp instanceof PhoneAddress)
                .map(cp -> (PhoneAddress) cp)
                .findFirst().ifPresent(phoneAddress -> {
            identificationInformationContactDetails2.setPhneNb(phoneAddress.getPhoneNumber());
            identificationInformationContactDetails2.setMobNb(phoneAddress.getMobileNumber());
            identificationInformationContactDetails2.setFaxNb(phoneAddress.getFaxNumber());
        });
        if (party instanceof Person) {
            final Person person = (Person) party;
            identificationInformationContactDetails2.setNm(person.getName());
            final OffsetDateTime birthDate = person.getBirthDate();
            identificationInformationDateAndPlaceOfBirth.setBirthDt(XmlDateUtil.getXmlDate(Optional.ofNullable(birthDate).orElse(OffsetDateTime.now())));
            Optional.ofNullable(person.getPlaceOfBirth()).ifPresent(birthLocation -> {
                birthLocation.getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    identificationInformationDateAndPlaceOfBirth.setPrvcOfBirth(postalAddress.getProvince());
                    identificationInformationDateAndPlaceOfBirth.setCityOfBirth(postalAddress.getTownName());
                    identificationInformationDateAndPlaceOfBirth.setCtryOfBirth(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                    identificationInformationPartyIdentification43.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                });
            });
        }
        if (party instanceof Organisation) {
            final Organisation organisation = (Organisation) party;
            final OffsetDateTime registrationDate = organisation.getRegistrationDate();
            identificationInformationDateAndPlaceOfBirth.setBirthDt(XmlDateUtil.getXmlDate(Optional.ofNullable(registrationDate).orElse(OffsetDateTime.now())));
            Optional.ofNullable(organisation.getPlaceOfRegistration()).ifPresent(registrationLocation -> {
                registrationLocation.getAddress().stream().findFirst().ifPresent(postalAddress -> {
                    identificationInformationDateAndPlaceOfBirth.setPrvcOfBirth(postalAddress.getProvince());
                    identificationInformationDateAndPlaceOfBirth.setCityOfBirth(postalAddress.getTownName());
                    identificationInformationDateAndPlaceOfBirth.setCtryOfBirth(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                    identificationInformationPartyIdentification43.setCtryOfRes(Optional.ofNullable(postalAddress.getCountry()).map(Country::getName).orElse(postalAddress.getCountyIdentification()));
                });
            });
        }

        identificationInformationPersonIdentification5.setDtAndPlcOfBirth(identificationInformationDateAndPlaceOfBirth);
        identificationInformationParty11Choice.setPrvtId(identificationInformationPersonIdentification5);
        identificationInformationPartyIdentification43.setId(identificationInformationParty11Choice);
        identificationInformationPartyIdentification43.setCtctDtls(identificationInformationContactDetails2);
        identificationInformation2.setPty(identificationInformationPartyIdentification43);
        final AccountIdentification4Choice identificationInformationAccountIdentification4Choice = new AccountIdentification4Choice();
        identificationInformationAccountIdentification4Choice.setIBAN("");
        final GenericAccountIdentification1 identificationIdentificationGenericAccountIdentification1 = new GenericAccountIdentification1();
        identificationIdentificationGenericAccountIdentification1.setId("");
        final AccountSchemeName1Choice identificationInformationAccountSchemeName1Choice = new AccountSchemeName1Choice();
        identificationInformationAccountSchemeName1Choice.setCd("");
        identificationInformationAccountSchemeName1Choice.setPrtry("");
        identificationIdentificationGenericAccountIdentification1.setSchmeNm(identificationInformationAccountSchemeName1Choice);
        identificationIdentificationGenericAccountIdentification1.setIssr("");
        identificationInformationAccountIdentification4Choice.setOthr(identificationIdentificationGenericAccountIdentification1);
        identificationInformation2.setAcct(identificationInformationAccountIdentification4Choice);
        final BranchAndFinancialInstitutionIdentification5 identificationInformationBranchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();
        final FinancialInstitutionIdentification8 identificationInformationFinancialInstitutionIdentification8 = new FinancialInstitutionIdentification8();
        identificationInformationFinancialInstitutionIdentification8.setBICFI("");
        final ClearingSystemMemberIdentification2 identificationInformationClearingSystemMemberIdentification2 = new ClearingSystemMemberIdentification2();
        final ClearingSystemIdentification2Choice identificationInformationClearingSystemIdentification2Choice = new ClearingSystemIdentification2Choice();
        identificationInformationClearingSystemIdentification2Choice.setCd("");
        identificationInformationClearingSystemIdentification2Choice.setPrtry("");
        identificationInformationClearingSystemMemberIdentification2.setClrSysId(identificationInformationClearingSystemIdentification2Choice);
        identificationInformationClearingSystemMemberIdentification2.setMmbId("");
        identificationInformationFinancialInstitutionIdentification8.setClrSysMmbId(identificationInformationClearingSystemMemberIdentification2);
        identificationInformationFinancialInstitutionIdentification8.setNm("");
        final PostalAddress6 identificationInformationFIPostalAddress6 = new PostalAddress6();
        identificationInformationFIPostalAddress6.setAdrTp(AddressType2Code.ADDR);
        identificationInformationFIPostalAddress6.setDept("");
        identificationInformationFIPostalAddress6.setSubDept("");
        identificationInformationFIPostalAddress6.setStrtNm("");
        identificationInformationFIPostalAddress6.setBldgNb("");
        identificationInformationFIPostalAddress6.setPstCd("");
        identificationInformationFIPostalAddress6.setTwnNm("");
        identificationInformationFIPostalAddress6.setCtrySubDvsn("");
        identificationInformationFIPostalAddress6.setCtry("");
        identificationInformationFinancialInstitutionIdentification8.setPstlAdr(identificationInformationFIPostalAddress6);
        final GenericFinancialIdentification1 identificationInformationGenericFinancialIdentification1 = new GenericFinancialIdentification1();
        identificationInformationGenericFinancialIdentification1.setId("");
        final FinancialIdentificationSchemeName1Choice identificationInformationFinancialIdentificationSchemeName1Choice = new FinancialIdentificationSchemeName1Choice();
        identificationInformationFinancialIdentificationSchemeName1Choice.setCd("");
        identificationInformationFinancialIdentificationSchemeName1Choice.setPrtry("");
        identificationInformationGenericFinancialIdentification1.setSchmeNm(identificationInformationFinancialIdentificationSchemeName1Choice);
        identificationInformationGenericFinancialIdentification1.setIssr("");
        identificationInformationFinancialInstitutionIdentification8.setOthr(identificationInformationGenericFinancialIdentification1);
        identificationInformationBranchAndFinancialInstitutionIdentification5.setFinInstnId(identificationInformationFinancialInstitutionIdentification8);
        final BranchData2 identificationInformationBranchData2 = new BranchData2();
        identificationInformationBranchData2.setId("");
        identificationInformationBranchData2.setNm("");
        final PostalAddress6 identificationInformationFIBranchDataPostalAddress6 = new PostalAddress6();
        identificationInformationFIBranchDataPostalAddress6.setAdrTp(AddressType2Code.ADDR);
        identificationInformationFIBranchDataPostalAddress6.setDept("");
        identificationInformationFIBranchDataPostalAddress6.setSubDept("");
        identificationInformationFIBranchDataPostalAddress6.setStrtNm("");
        identificationInformationFIBranchDataPostalAddress6.setBldgNb("");
        identificationInformationFIBranchDataPostalAddress6.setPstCd("");
        identificationInformationFIBranchDataPostalAddress6.setTwnNm("");
        identificationInformationFIBranchDataPostalAddress6.setCtrySubDvsn("");
        identificationInformationFIBranchDataPostalAddress6.setCtry("");
        identificationInformationBranchData2.setPstlAdr(identificationInformationFIBranchDataPostalAddress6);
        identificationInformationBranchAndFinancialInstitutionIdentification5.setBrnchId(identificationInformationBranchData2);
        identificationInformation2.setAgt(identificationInformationBranchAndFinancialInstitutionIdentification5);
        identificationVerification2.setPtyAndAcctId(identificationInformation2);
    }

    private void configurePartyIdentification43FromCustomParty(CustomParty customParty, IdentificationVerification2 identificationVerification2, IdentificationInformation2 identificationInformation2) {
        final Optional<FinancialInstitution> optionalFinancialInstitution = financialInstitutionService.getFinancialInstitutionByAnyBIC(customParty.getFinancialInstitutionIdentification());
        final PartyIdentification43 identificationInformationPartyIdentification43 = new PartyIdentification43();
        identificationInformationPartyIdentification43.setNm(customParty.getName());
        final Party11Choice identificationInformationParty11Choice = new Party11Choice();
        final OrganisationIdentification8 identificationInformationOrganisationIdentification8 = new OrganisationIdentification8();
        identificationInformationOrganisationIdentification8.setAnyBIC(customParty.getFinancialInstitutionIdentification());
        identificationInformationParty11Choice.setOrgId(identificationInformationOrganisationIdentification8);
        final PersonIdentification5 identificationInformationPersonIdentification5 = new PersonIdentification5();
        identificationInformationParty11Choice.setPrvtId(identificationInformationPersonIdentification5);
        identificationInformationPartyIdentification43.setId(identificationInformationParty11Choice);
        final ContactDetails2 identificationInformationContactDetails2 = new ContactDetails2();
        identificationInformationContactDetails2.setNmPrfx(NamePrefix1Code.DOCT);
        identificationInformationContactDetails2.setNm(customParty.getName());
        identificationInformationPartyIdentification43.setCtctDtls(identificationInformationContactDetails2);
        identificationInformation2.setPty(identificationInformationPartyIdentification43);
        final AccountIdentification4Choice identificationInformationAccountIdentification4Choice = new AccountIdentification4Choice();
        identificationInformationAccountIdentification4Choice.setIBAN(customParty.getAccountNumber());
        final GenericAccountIdentification1 identificationIdentificationGenericAccountIdentification1 = new GenericAccountIdentification1();
        identificationIdentificationGenericAccountIdentification1.setId(customParty.getNationalId());
        identificationIdentificationGenericAccountIdentification1.setIssr(customParty.getFinancialInstitutionIdentification());
        identificationInformationAccountIdentification4Choice.setOthr(identificationIdentificationGenericAccountIdentification1);
        identificationInformation2.setAcct(identificationInformationAccountIdentification4Choice);
        final BranchAndFinancialInstitutionIdentification5 identificationInformationBranchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();
        final FinancialInstitutionIdentification8 identificationInformationFinancialInstitutionIdentification8 = new FinancialInstitutionIdentification8();
        identificationInformationFinancialInstitutionIdentification8.setBICFI(customParty.getFinancialInstitutionIdentification());
        identificationInformationFinancialInstitutionIdentification8.setNm(optionalFinancialInstitution.map(FinancialInstitution::getName).orElse(""));
        final GenericFinancialIdentification1 identificationInformationGenericFinancialIdentification1 = new GenericFinancialIdentification1();
        identificationInformationGenericFinancialIdentification1.setId(optionalFinancialInstitution.map(FinancialInstitution::getId).orElse(""));

        optionalFinancialInstitution.ifPresent(financialInstitution -> {
            financialInstitution.getOrganisationIdentification().stream().findFirst().ifPresent(organisationIdentification -> {
                identificationInformationGenericFinancialIdentification1.setIssr(organisationIdentification.getAnyBIC());
            });
        });

        identificationInformationFinancialInstitutionIdentification8.setOthr(identificationInformationGenericFinancialIdentification1);
        identificationInformationBranchAndFinancialInstitutionIdentification5.setFinInstnId(identificationInformationFinancialInstitutionIdentification8);
        identificationInformation2.setAgt(identificationInformationBranchAndFinancialInstitutionIdentification5);
        identificationVerification2.setPtyAndAcctId(identificationInformation2);
    }
}
