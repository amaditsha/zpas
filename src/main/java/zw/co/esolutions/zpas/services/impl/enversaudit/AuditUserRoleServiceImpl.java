package zw.co.esolutions.zpas.services.impl.enversaudit;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.model.enversaudit.AuditedRevisionEntity;
import zw.co.esolutions.zpas.model.enversaudit.UserRoleAuditResult;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.services.iface.enversaudit.AuditUserRoleService;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class AuditUserRoleServiceImpl implements AuditUserRoleService {

    @Autowired
    EntityManager entityManager;

    public List<UserRoleAuditResult> auditUserRoleRevisions(String userRoleId){
        List<Object[]> results = (List<Object[]>) AuditReaderFactory
                .get( entityManager )
                .createQuery()
                .forRevisionsOfEntity( UserRole.class, false, true )
                .add( AuditEntity.property( "id" ).eq( userRoleId ) )
                .getResultList();

        log.info("The user role revision size is {}", results.size());

        return results.stream()
                .map(objectArray ->
                        new UserRoleAuditResult((UserRole) objectArray[0], (AuditedRevisionEntity) objectArray[1]))
                .collect(Collectors.toList());
    }

    @Override
    public List<AuditedRevisionEntity> auditRevisions(String userRoleId) {
        List<AuditedRevisionEntity> results = AuditReaderFactory
                .get( entityManager )
                .createQuery()
                .forRevisionsOfEntity( AuditedRevisionEntity.class, true, false )
                .add( AuditEntity.revisionNumber().maximize().computeAggregationInInstanceContext() )
                .getResultList();

        log.info("The audit revision size is {}", results.size());
        return results;
    }

}
