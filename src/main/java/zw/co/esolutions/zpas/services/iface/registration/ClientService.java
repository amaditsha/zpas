package zw.co.esolutions.zpas.services.iface.registration;


import zw.co.esolutions.zpas.dto.registration.ClientCreationDTO;
import zw.co.esolutions.zpas.dto.registration.EmployingPartyRolePersonRequestDTO;
import zw.co.esolutions.zpas.model.*;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    Person createPerson(ClientCreationDTO clientCreationDTO);
    Optional<Person> getPersonById(String personId);
    List<Person> getPersons();
    List<Person> getPersonsByEmployerId(String employerId);
    List<EmployingPartyRole> getEmployingPartyRoleByEmployerId(String employerId);
    Person updatePerson(ClientCreationDTO clientUpdateDTO);
    PersonName updatePersonName(PersonName personName);
    PersonIdentification updatePersonIdentification(PersonIdentification personIdentification);
    PersonProfile updatePersonProfile(PersonProfile personProfile);
    PostalAddress updatePlaceOfBirth(PostalAddress postalAddress);
    Person updateGeneralInformation(Person person);
    Optional<PersonIdentification> findPersonIdentificationByIdNumber(String idNumber);
    Optional<Person> getPersonByAccountId(String accountId);
    Person deletePerson(String id);
    Person approvePerson(String id);
    Person rejectPerson(String id);
    Person findPersonById(String id);
    List<ContactPoint> getContactPointsByPersonId(String personId);
    String refreshPersonIndex();
    boolean isIdentityCardNumberAlreadyInUse(String identityCardNumber);
    EmployingPartyRole addPersonToEmployingPartyRole(EmployingPartyRolePersonRequestDTO employingPartyRolePersonRequestDTO);
    EmployingPartyRole removePersonFromEmployingPartyRole(EmployingPartyRolePersonRequestDTO employingPartyRolePersonRequestDTO);

}
