package zw.co.esolutions.zpas.services.impl.statements;

import com.google.common.collect.Lists;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.dto.messaging.AccountStatementDTO;
import zw.co.esolutions.zpas.dto.messaging.AccountStatementInfo;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import static zw.co.esolutions.zpas.utilities.util.MoneyUtil.*;

@Component
@Slf4j
public class AccountStatementPDFGenerator {
    public static void main(String[] args) {
        AccountStatementDTO accountStatementDTO = AccountStatementDTO.builder().build();
        accountStatementDTO.setRecipient("Tarisai Kunaka");
        accountStatementDTO.setEmailAddress("tarisaikunaka5@gmail.com");
        accountStatementDTO.setClientId("100928334");
        accountStatementDTO.setOpeningBalance(new BigDecimal("1000"));
        accountStatementDTO.setOpbdDbtCrdtIndicator("Credit");
        accountStatementDTO.setClosingBalance(new BigDecimal("900"));
        accountStatementDTO.setClbdDbtCrdtIndicator("Credit");
        accountStatementDTO.setAccountNumber("1002898768");
        accountStatementDTO.setAccountName("Tarisai Kunaka");
        accountStatementDTO.setAccountServicer("Steward Bank");
        accountStatementDTO.setAccountCurrency("USD");
        accountStatementDTO.setAccountStatementInfoList(Lists.newArrayList(
                AccountStatementInfo.builder()
                    .bookingDate(OffsetDateTime.now())
                    .currency("USD")
                    .debitCreditIndicator("Debit")
                    .txnAmount(new BigDecimal("100"))
                    .txnNarrative("Bought something nice")
                    .txnReference("TEHDSJIOO")
                    .txnStatus("Booked")
                    .build()
        ));

        new AccountStatementPDFGenerator().generateDocument(accountStatementDTO);
    }

    public String generateDocument(AccountStatementDTO accountStatementDTO) {
        String userpassword = accountStatementDTO.getAccountNumber().substring(accountStatementDTO.getAccountNumber().length() - 4);
        String ownerPassword = "PassworD123";
        Document document = new Document();
        final Path fileLocation = Paths.get(StorageProperties.BASE_LOCATION).resolve("account-statement.pdf");
        try {
            PdfWriter.getInstance(document, new FileOutputStream(fileLocation.toFile()))
                    .setEncryption(userpassword.getBytes(), ownerPassword.getBytes(),
                            PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);

            document.open();

            //Set attributes here
            document.addAuthor("ZEEPAY System");
            document.addCreationDate();
            document.addCreator("ZEEPAY");
            document.addTitle("Account Statement");
            document.addSubject("Account Statement.");

            Path path = Paths.get(ClassLoader.getSystemResource("static/images/logo-inverse.png").toURI());
            Image img = Image.getInstance(path.toAbsolutePath().toString());
            img.scalePercent(10);
            img.setAbsolutePosition(500f, 800f);
            document.add(img);

            addHeading(document);
            addAddressInfo(document, accountStatementDTO);
            addStatementTable(document, accountStatementDTO);

            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return fileLocation.toString();
    }

    private void addHeading(Document document) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        //Paragraph with color and font styles
        Paragraph paragraph = new Paragraph("ZEEPAY", blackFont);
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("ACCOUNT STATEMENT", blackFont));

        PdfPTable table = new PdfPTable(3); // 4 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f};
        table.setWidths(columnWidths);

        PdfPCell cell1 = new PdfPCell(new Paragraph(""));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setPaddingLeft(10);
        cell1.setPaddingBottom(10);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell2 = new PdfPCell(paragraph);
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setPaddingLeft(10);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell3 = new PdfPCell(new Paragraph(""));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setPaddingLeft(10);
        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);

        document.add(table);
    }

    private void addAddressInfo(Document document, AccountStatementDTO accountStatementDTO) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        Paragraph paragraph = new Paragraph("", blackFont);
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("Account Number: " + accountStatementDTO.getAccountNumber()));
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("Account Name: " + accountStatementDTO.getRecipient()));
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("Account Currency: " + accountStatementDTO.getAccountCurrency()));
        /*paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("Opening Balance: " + MoneyUtil.convertToDollarsPattern(accountStatementDTO.getOpeningBalance())));*/
        document.add(paragraph);
    }

    private void addStatementTable(Document document, AccountStatementDTO accountStatementDTO) throws DocumentException {
        Font blackFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, Font.NORMAL, new CMYKColor(0, 0, 0, 255));

        PdfPTable table = new PdfPTable(5); // 4 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {1f, 1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);

        PdfPCell cell1 = new PdfPCell(new Paragraph("Booking Date", blackFont));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell1.setPaddingLeft(10);
        cell1.setPaddingBottom(10);
        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell2 = new PdfPCell(new Paragraph("Reference", blackFont));
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell2.setPaddingLeft(10);
        cell2.setPaddingBottom(10);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell3 = new PdfPCell(new Paragraph("Narrative", blackFont));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell3.setPaddingLeft(10);
        cell3.setPaddingBottom(10);
        cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell4 = new PdfPCell(new Paragraph("Debit Credit Indicator", blackFont));
        cell4.setBorder(PdfPCell.NO_BORDER);
        cell4.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell4.setPaddingLeft(10);
        cell4.setPaddingBottom(10);
        cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell5 = new PdfPCell(new Paragraph("Amount", blackFont));
        cell5.setBorder(PdfPCell.NO_BORDER);
        cell5.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        cell5.setPaddingLeft(10);
        cell5.setPaddingBottom(10);
        cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        cell1.setUseBorderPadding(true);
        cell2.setUseBorderPadding(true);
        cell3.setUseBorderPadding(true);
        cell4.setUseBorderPadding(true);
        cell5.setUseBorderPadding(true);

        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);
        table.addCell(cell5);

        PdfPCell openingBalanceHeadingCell = new PdfPCell(new Paragraph("Balance at Period Start"));
        openingBalanceHeadingCell.setBorder(PdfPCell.NO_BORDER);
        openingBalanceHeadingCell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        openingBalanceHeadingCell.setPaddingLeft(10);
        openingBalanceHeadingCell.setPaddingBottom(10);
        openingBalanceHeadingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        openingBalanceHeadingCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell openingBalanceValueCell = new PdfPCell(new Paragraph(convertToDollarsPattern(accountStatementDTO.getOpeningBalance())));
        openingBalanceValueCell.setBorder(PdfPCell.NO_BORDER);
        openingBalanceValueCell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        openingBalanceValueCell.setPaddingLeft(10);
        openingBalanceValueCell.setPaddingBottom(10);
        openingBalanceValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        openingBalanceValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell openingBalanceDbtCrdtIndPdfPCell = new PdfPCell(new Paragraph(accountStatementDTO.getOpbdDbtCrdtIndicator()));
        openingBalanceDbtCrdtIndPdfPCell.setBorder(PdfPCell.NO_BORDER);
        openingBalanceDbtCrdtIndPdfPCell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        openingBalanceDbtCrdtIndPdfPCell.setPaddingLeft(10);
        openingBalanceDbtCrdtIndPdfPCell.setPaddingBottom(10);
        openingBalanceDbtCrdtIndPdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        openingBalanceDbtCrdtIndPdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell openingBalanceSpacerPdfPCell = new PdfPCell(new Paragraph(""));
        openingBalanceSpacerPdfPCell.setBorder(PdfPCell.NO_BORDER);
        openingBalanceSpacerPdfPCell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        openingBalanceSpacerPdfPCell.setPaddingLeft(10);
        openingBalanceSpacerPdfPCell.setPaddingBottom(10);
        openingBalanceSpacerPdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        openingBalanceSpacerPdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        table.addCell(openingBalanceSpacerPdfPCell);
        table.addCell(openingBalanceHeadingCell);
        table.addCell(openingBalanceSpacerPdfPCell);
        table.addCell(openingBalanceDbtCrdtIndPdfPCell);
        table.addCell(openingBalanceValueCell);

        for(AccountStatementInfo accountStatementInfo : accountStatementDTO.getAccountStatementInfoList()) {

            PdfPCell cell7 = new PdfPCell(new Paragraph(accountStatementInfo.getBookingDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy hh:mm"))));
            cell7.setBorder(PdfPCell.NO_BORDER);
            cell7.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
            cell7.setPaddingLeft(10);
            cell7.setPaddingBottom(10);
            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell8 = new PdfPCell(new Paragraph(accountStatementInfo.getTxnReference()));
            cell8.setBorder(PdfPCell.NO_BORDER);
            cell8.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
            cell8.setPaddingLeft(10);
            cell8.setPaddingBottom(10);
            cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell9 = new PdfPCell(new Paragraph(accountStatementInfo.getTxnNarrative()));
            cell9.setBorder(PdfPCell.NO_BORDER);
            cell9.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
            cell9.setPaddingLeft(10);
            cell9.setPaddingBottom(10);
            cell9.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell10 = new PdfPCell(new Paragraph(accountStatementInfo.getDebitCreditIndicator()));
            cell10.setBorder(PdfPCell.NO_BORDER);
            cell10.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
            cell10.setPaddingLeft(10);
            cell10.setPaddingBottom(10);
            cell10.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell10.setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell11 = new PdfPCell(new Paragraph(convertToDollarsPattern(accountStatementInfo.getTxnAmount())));
            cell11.setBorder(PdfPCell.NO_BORDER);
            cell11.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
            cell11.setPaddingLeft(10);
            cell11.setPaddingBottom(10);
            cell11.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);

            //To avoid having the cell border and the content overlap, if you are having thick cell borders
            cell7.setUseBorderPadding(true);
            cell8.setUseBorderPadding(true);
            cell9.setUseBorderPadding(true);
            cell10.setUseBorderPadding(true);
            cell11.setUseBorderPadding(true);
            //cell12.setUseBorderPadding(true);

            table.addCell(cell7);
            table.addCell(cell8);
            table.addCell(cell9);
            table.addCell(cell10);
            table.addCell(cell11);
            //table.addCell(cell12);
        }

        PdfPCell closingBalanceHeadingCell = new PdfPCell(new Paragraph("Balance at Period End"));
        closingBalanceHeadingCell.setBorder(PdfPCell.NO_BORDER);
        closingBalanceHeadingCell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        closingBalanceHeadingCell.setPaddingLeft(10);
        closingBalanceHeadingCell.setPaddingBottom(10);
        closingBalanceHeadingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        closingBalanceHeadingCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell closingBalanceValueCell = new PdfPCell(new Paragraph(convertToDollarsPattern(accountStatementDTO.getClosingBalance())));
        closingBalanceValueCell.setBorder(PdfPCell.NO_BORDER);
        closingBalanceValueCell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        closingBalanceValueCell.setPaddingLeft(10);
        closingBalanceValueCell.setPaddingBottom(10);
        closingBalanceValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        closingBalanceValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell closingBalanceDbtCrdtIndPdfPCell = new PdfPCell(new Paragraph(accountStatementDTO.getClbdDbtCrdtIndicator()));
        closingBalanceDbtCrdtIndPdfPCell.setBorder(PdfPCell.NO_BORDER);
        closingBalanceDbtCrdtIndPdfPCell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        closingBalanceDbtCrdtIndPdfPCell.setPaddingLeft(10);
        closingBalanceDbtCrdtIndPdfPCell.setPaddingBottom(10);
        closingBalanceDbtCrdtIndPdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        closingBalanceDbtCrdtIndPdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell closingBalanceSpacerPdfPCell = new PdfPCell(new Paragraph(""));
        closingBalanceSpacerPdfPCell.setBorder(PdfPCell.NO_BORDER);
        closingBalanceSpacerPdfPCell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        closingBalanceSpacerPdfPCell.setPaddingLeft(10);
        closingBalanceSpacerPdfPCell.setPaddingBottom(10);
        closingBalanceSpacerPdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        closingBalanceSpacerPdfPCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        table.addCell(closingBalanceSpacerPdfPCell);
        table.addCell(closingBalanceHeadingCell);
        table.addCell(closingBalanceSpacerPdfPCell);
        table.addCell(closingBalanceDbtCrdtIndPdfPCell);
        table.addCell(closingBalanceValueCell);

        document.add(table);
    }
}
