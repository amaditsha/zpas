package zw.co.esolutions.zpas.services.iface.signature;

import zw.co.esolutions.zpas.model.Signature;

import java.util.List;
import java.util.Optional;

public interface SignatureService {
    Optional<Signature> getSignatureById(String signatureId);
    List<Signature> getSignatures();
}
