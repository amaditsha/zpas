package zw.co.esolutions.zpas.services.impl.payments.zimra.response;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by alfred on 21 August 2019
 */
public class GetResponse {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "bpNumber")
    private String bpNumber;

    @XmlElement(name = "taxPayerName")
    private String taxPayerName;

    @XmlElement(name = "registered")
    private String registered;

    public String getBpNumber() {
        return bpNumber;
    }

    public String getTaxPayerName() {
        return taxPayerName;
    }

    public String getRegistered() {
        return registered;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setBpNumber(String bpNumber) {
        this.bpNumber = bpNumber;
    }

    public void setTaxPayerName(String taxPayerName) {
        this.taxPayerName = taxPayerName;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}