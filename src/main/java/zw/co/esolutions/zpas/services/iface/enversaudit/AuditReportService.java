package zw.co.esolutions.zpas.services.iface.enversaudit;

import zw.co.esolutions.zpas.utilities.audit.Auditable;

import java.util.List;

public interface AuditReportService {
    List<? extends Auditable> getAuditEntries(String username, Class<? extends Auditable> clas);

    String getAuditableStrings();
}
