package zw.co.esolutions.zpas.services.impl.payments.zimra;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.weaver.GeneratedReferenceTypeDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleDTO;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.PaymentStatus;
import zw.co.esolutions.zpas.model.payments.ExtraDetail;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.repository.payments.ExtraDetailRepository;
import zw.co.esolutions.zpas.services.impl.payments.zimra.request.PaymentAdvice;
import zw.co.esolutions.zpas.services.impl.payments.zimra.request.ProcessPaymentRequest;
import zw.co.esolutions.zpas.services.impl.payments.zimra.response.AssessmentValidationGetResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleItemDTO;
import zw.co.esolutions.zpas.services.impl.payments.zimra.response.AssessmentValidationResponse;
import zw.co.esolutions.zpas.services.impl.payments.zimra.response.ProcessPaymentResponse;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.sql.Ref;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by alfred on 21 August 2019
 */
@Slf4j
@Component
public class ZimraProcessor {

    @Value("${ZIMRA.ASSESSMENTS.SYSTEM.BASE.URL}")
    private String zimraSystemBaseUrl;

    @Autowired
    private ExtraDetailRepository extraDetailRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    public Optional<AssessmentValidationResponse> getAssessmentInfo(String zimraAssessmentNumber) {
//        RestTemplate restTemplate = new RestTemplate();
//        String getZimraAssessmentInformationUrl = zimraSystemBaseUrl + "/get/assessment/" + zimraAssessmentNumber;
//
//        log.info("About to reach URL: {}", getZimraAssessmentInformationUrl);
//
//        AssessmentValidationResponse assessmentValidationResponse = restTemplate.getForObject(getZimraAssessmentInformationUrl, AssessmentValidationResponse.class);
        final AssessmentValidationGetResponse response = AssessmentValidationGetResponse.builder()
                .amount(String.valueOf(ThreadLocalRandom.current().nextInt(100, 100_000)))
                .assNo(zimraAssessmentNumber)
                .currency("ZWL")
                .found(String.valueOf(ThreadLocalRandom.current().nextBoolean()))
                .office("HRE")
                .xmlns("http://AssessmentDetails/ValidateAssessment")
                .year(String.valueOf(LocalDate.now().getYear()))
                .build();
        AssessmentValidationResponse assessmentValidationResponse = AssessmentValidationResponse.builder()
                .xmlns("http://tempuri.org/")
                .assessmentValidationGetResponse(response)
                .build();


        return Optional.ofNullable(assessmentValidationResponse);
    }

    public List<AssessmentValidationResponse> getAssessmentInfosByBpn(String businessPartnerNumber) {
        if(StringUtils.isEmpty(businessPartnerNumber)) {
            return new ArrayList<>();
        }

//        RestTemplate restTemplate = new RestTemplate();
//        String getZimraAssessmentInformationUrl = zimraSystemBaseUrl + "/get/assessment/" + businessPartnerNumber;
//
//        log.info("About to reach URL: {}", getZimraAssessmentInformationUrl);
//
//        List<AssessmentValidationResponse> assessmentValidationResponses = restTemplate.getForObject(getZimraAssessmentInformationUrl, AssessmentValidationResponse.class);

        List<AssessmentValidationResponse> assessmentValidationResponses = new ArrayList<>();
        final int numberOfDummyAssessmentResponses = ThreadLocalRandom.current().nextInt(11);
        for(int i = 0; i < numberOfDummyAssessmentResponses; i++) {
            final String randomAssessmentNumber = getRandomAssessmentNumber();
            final AssessmentValidationGetResponse response = AssessmentValidationGetResponse.builder()
                    .amount(String.valueOf(ThreadLocalRandom.current().nextInt(100, 100_000)))
                    .assNo(randomAssessmentNumber)
                    .currency("ZWL")
                    .found(String.valueOf(ThreadLocalRandom.current().nextBoolean()))
                    .office("HRE")
                    .xmlns("http://AssessmentDetails/ValidateAssessment")
                    .year(String.valueOf(LocalDate.now().getYear()))
                    .build();
            AssessmentValidationResponse assessmentValidationResponse = AssessmentValidationResponse.builder()
                    .xmlns("http://tempuri.org/")
                    .assessmentValidationGetResponse(response)
                    .build();
            assessmentValidationResponses.add(assessmentValidationResponse);
        }

        return assessmentValidationResponses;
    }

    private String getRandomAssessmentNumber() {
        final long rand1 = ThreadLocalRandom.current().nextLong(10000L, 99999L);
        final long rand2 = ThreadLocalRandom.current().nextLong(10000L, 99999L);
        final String rand1String = String.valueOf(rand1);
        final String rand2String = String.valueOf(rand2);
        return rand1String + rand2String;
    }

    public Optional<String> handlePaymentStatusReport(Payment payment) {

        final PaymentStatusCode paymentStatusCode = payment.getLatestPaymentStatus().map(PaymentStatus::getStatus).orElse(PaymentStatusCode.None);

        StringBuilder message = new StringBuilder();
        switch (paymentStatusCode) {
            case None:
            case Received:
            case AcceptedTechnicalValidation:
            case PartiallyAccepted:
            case AcceptedCustomerProfile:
            case Pending:
            case AcceptedSettlementInProcess:
            case PartiallyAcceptedCancellationRequest:
            case PendingCancellationRequest:
                message.append("Payment has been accepted for further processing.");
                break;
            case Rejected:
                message.append("Payment has been rejected.");
                break;
            case AcceptedSettlementCompleted:
            case Accepted:
            case AcceptedWithChange:
                message.append("Payment has been completed successfully.");
                processPaymentAdvice(payment);
                break;
        }
        addExtraDetailToPayment(payment, message.toString());
        return Optional.ofNullable(message.toString());
    }

    private void addExtraDetailToPayment(Payment payment, String detail) {
        ExtraDetail extraDetail = new ExtraDetail();
        extraDetail.setId(UUID.randomUUID().toString());
        extraDetail.setDateCreated(OffsetDateTime.now());
        extraDetail.setLastUpdated(OffsetDateTime.now());
        extraDetail.setEntityStatus(EntityStatus.ACTIVE);
        extraDetail.setPayment(payment);

        extraDetail.setDetail(detail);
        extraDetailRepository.save(extraDetail);
        payment.addExtraDetail(extraDetail);
        paymentRepository.save(payment);
    }

    public Optional<ProcessPaymentResponse> processPaymentAdvice(Payment payment) {
        addExtraDetailToPayment(payment, "ZIMRA Payment advice sent");
        log.info("Processing Zimra Payment advice.");
        final PaymentAdvice paymentAdvice = PaymentAdvice.builder()
                .accountNumber(payment.getBeneficiaryAccount())
                .amount(payment.getAmount().getAmount().doubleValue())
                .currency(payment.getAmount().getCurrency().getCodeName())
                .accountNumber(payment.getAccount().getAccountNumber())
                .bpNumber(getBusinessPartnerNumber(payment))   //TODO set BP Number
                .captureTime(payment.getValueDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")))
                .clientName(payment.getDebtor().map(Party::getName).orElse(""))
                .paymentDate(payment.getValueDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")))
                .referenceNumber(payment.getEndToEndId())
                .assNo(getZimraAssessmentNumber(payment))
                .region(getOfficeCode(payment))
                .rrn(payment.getEndToEndId())
                .serialNumber(RefGen.getReference("S"))
                .taxCode("Tax")
                .userID(payment.getDebtor().map(party -> party.getPersonIdentificationHelper()).orElse(""))
                .build();
        ProcessPaymentRequest processPaymentRequest = ProcessPaymentRequest.builder()
                .xmlns("http://samaz.org/")
                .paymentAdvice(paymentAdvice)
                .build();
        addExtraDetailToPayment(payment, "ZIMRA Payment advice response returned.");

        log.info("Sending out request: \n{}\n to ZIMRA.", processPaymentRequest);

//        RestTemplate restTemplate = new RestTemplate();
//        String zimraProcessPaymentAdviceUrl = zimraSystemBaseUrl + "/process/payment/advice";
//
//        log.info("About to reach URL: {}", zimraProcessPaymentAdviceUrl);
//
//        ResponseEntity<ProcessPaymentResponse> response = restTemplate.exchange(
//                zimraProcessPaymentAdviceUrl,
//                HttpMethod.POST,
//                new HttpEntity<>(processPaymentRequest),
//                new ParameterizedTypeReference<ProcessPaymentResponse>() {
//                });
//        final ProcessPaymentResponse processPaymentResponse = response.getBody();
        log.info("Now simulating Zimra response.");
        final String receiptNumber = RefGen.getReference("R");
        final ProcessPaymentResponse processPaymentResponse = ProcessPaymentResponse.builder()
                .xmlns("http://ZOP/Receipt")
                .accountNumber(payment.getAccount().getAccountNumber())
                .amount(payment.getAmount().getAmount().doubleValue())
                .bpNumber(getBusinessPartnerNumber(payment))
                .captureTime(payment.getValueDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")))
                .clientName(payment.getDebtor().map(Party::getName).orElse(""))
                .currency(payment.getAmount().getCurrency().getCodeName())
                .customsMessage("None")
                .customsReceiptDate(OffsetDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")))
                .customsReceiptNumber(receiptNumber)
                .id(GenerateKey.generateEntityId())
                .logMessageNumber(RefGen.getReference("L"))
                .logNumber(RefGen.getReference("L"))
                .message("Receipt")
                .messageV1("Receipt")
                .messageV2("Receipt")
                .messageV3("Receipt")
                .messageV4("Receipt")
                .number(receiptNumber)
                .paymentDate(payment.getValueDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")))
                .receiptDate(payment.getValueDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")))
                .receiptTime(payment.getValueDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")))
                .referenceNumber(RefGen.getReference("R"))
                .region("")
                .rrn("")
                .serialNumber(GenerateKey.generateRef())
                .taxCode("Tax")
                .type("Tax")
                .userID(payment.getDebtor().map(party -> party.getPersonIdentificationHelper()).orElse(""))
                .build();

        return Optional.ofNullable(processPaymentResponse);
    }

    private String getBusinessPartnerNumber(Payment payment) {
        for (String s : payment.getUnstructuredRemittanceInformation()) {
            final String[] keyValuePair = s.split(":");
            if (keyValuePair[0].equals("businessPartnerNumber")) {
                return keyValuePair[1];
            }
        }
        return "";
    }

    private String getOfficeCode(Payment payment) {
        for (String s : payment.getUnstructuredRemittanceInformation()) {
            final String[] keyValuePair = s.split(":");
            if (keyValuePair[0].equals("officeCode")) {
                return keyValuePair[1];
            }
        }
        return "";
    }

    private String getZimraAssessmentNumber(Payment payment) {
        for (String s : payment.getUnstructuredRemittanceInformation()) {
            final String[] keyValuePair = s.split(":");
            if (keyValuePair[0].equals("zimraAssessmentNumber")) {
                return keyValuePair[1];
            }
        }
        return "";
    }
}
