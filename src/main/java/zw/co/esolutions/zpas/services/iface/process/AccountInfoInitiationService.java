package zw.co.esolutions.zpas.services.iface.process;

import zw.co.esolutions.zpas.model.CashAccount;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

import java.util.List;

public interface AccountInfoInitiationService {
    ServiceResponse initiateAccountInfoRequest(List<CashAccount> cashAccounts, String reqdMsgNameId);
}
