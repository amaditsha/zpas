package zw.co.esolutions.zpas.services.iface.statements;

import zw.co.esolutions.zpas.dto.messaging.AccountStatementDTO;
import zw.co.esolutions.zpas.dto.messaging.ProofOfPaymentDTO;
import zw.co.esolutions.zpas.dto.messaging.ProofOfPaymentInfo;

/**
 * Created by alfred on 14 Mar 2019
 */
public interface StatementsService {
    ProofOfPaymentInfo getProofOfPaymentInfo(String paymentId);
    ProofOfPaymentInfo sendProofOfPaymentInfo(ProofOfPaymentDTO proofOfPaymentDTO);
    AccountStatementDTO sendAccountStatementInfo(AccountStatementDTO accountStatementDTO);
}
