package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt060_001_04.*;
import zw.co.esolutions.zpas.model.*;

import java.util.Optional;
import java.util.stream.Stream;

@Component
@Slf4j
public class AccountPartyRoleUtil {
    public PartyIdentification125 getPartyIdentification125(CashAccount cashAccount, Class<?> accountPartyRoleSubclass) {
        PartyIdentification125 partyIdentification125 = new PartyIdentification125();

        Optional<Party> partyOptional = this.getParty(cashAccount, accountPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            partyIdentification125.setNm(this.getPartyName(partyOptional.get()));

            partyIdentification125.setId(this.getPartyIdentificationChoice(partyOptional.get()));
        }
        return partyIdentification125;
    }

    public BranchAndFinancialInstitutionIdentification5 getBranchAndFinancialInstitutionIdentification5(CashAccount cashAccount, Class<?> accountPartyRoleSubclass) {

        BranchAndFinancialInstitutionIdentification5 branchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();

        FinancialInstitutionIdentification8 financialInstitutionIdentification8 = new FinancialInstitutionIdentification8();

        Optional<Party> partyOptional = this.getParty(cashAccount, accountPartyRoleSubclass);

        if (partyOptional.isPresent()) {

            Organisation organisation = (Organisation) partyOptional.get();

            financialInstitutionIdentification8.setBICFI(organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getBICFI).findFirst().orElse(""));

            financialInstitutionIdentification8.setNm(organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getOrganisationName)
                    .flatMap(organisationNames -> organisationNames.stream())
                    .map(OrganisationName::getShortName)
                    .findFirst().orElse(""));

            branchAndFinancialInstitutionIdentification5.setFinInstnId(financialInstitutionIdentification8);
        }
        return branchAndFinancialInstitutionIdentification5;
    }

    public Optional<AccountPartyRole> getAccountPartyRole(CashAccount cashAccount, Class<?> accountPartyRoleSubclass) {
        final Optional<AccountPartyRole> accountPartyRoleOptional = cashAccount.getPartyRole().stream()
                .filter(accountPartyRole -> accountPartyRoleSubclass.isInstance(accountPartyRole))
                .findFirst();

        return accountPartyRoleOptional;
    }

    public Optional<Party> getParty(CashAccount cashAccount, Class<?> accountPartyRoleSubclass) {

        final Optional<AccountPartyRole> accountPartyRoleOptional = this.getAccountPartyRole(cashAccount, accountPartyRoleSubclass);

        //if party role present, extract the party
        if (accountPartyRoleOptional.isPresent()) {

            log.info("Party is present, building the info in the msg..");

            AccountPartyRole accountPartyRole = accountPartyRoleOptional.get();

            Optional<RolePlayer> rolePlayerOptional = accountPartyRole.getPlayer().stream()
                    .findFirst();

            //if role player present, return the party
            if (rolePlayerOptional.isPresent()) {

                Party party = (Party) rolePlayerOptional.get();

                return Optional.of(party);

            }

        }

        return Optional.empty();
    }

    public Party34Choice getPartyIdentificationChoice(Party party) {
        Party34Choice party34Choice = new Party34Choice();

        if (party instanceof Organisation) {
            Organisation accOwnerOrganisation = (Organisation) party;

            OrganisationIdentification8 organisationIdentification8 = new OrganisationIdentification8();
            organisationIdentification8.setAnyBIC(accOwnerOrganisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getAnyBIC)
                    .findFirst().orElse(""));
            party34Choice.setOrgId(organisationIdentification8);
        } else {
            Person accountOwner = (Person) party;

            PersonIdentification13 accountOwnerPersonIdentification13 = new PersonIdentification13();

            GenericPersonIdentification1 accOwnerGenericPersonIdentification1 = new GenericPersonIdentification1();
            accOwnerGenericPersonIdentification1.setId(accountOwner.getPersonIdentification().stream()
                    .map(PersonIdentification::getIdentityCardNumber)
                    .findFirst().orElse(""));
            //accOwnerGenericPersonIdentification1.setSchmeNm(new PersonIdentificationSchemeName1Choice());
            //accOwnerGenericPersonIdentification1.setIssr("");

            accountOwnerPersonIdentification13.getOthr().add(accOwnerGenericPersonIdentification1);

            party34Choice.setPrvtId(accountOwnerPersonIdentification13);
        }

        return party34Choice;
    }

    public String getPartyName(Party party) {
        if (party instanceof Organisation) {
            Organisation organisation = (Organisation) party;
            final Stream<OrganisationName> organisationNameStream = organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getOrganisationName)
                    .flatMap(organisationNames -> organisationNames.stream());

            return organisationNameStream.map(organisationName ->
                    organisationName == null ? "" : organisationName.getLegalName()
            ).findFirst().orElse("");
        } else {
            Person person = (Person) party;
            final Stream<PersonName> personNameStream = person.getPersonIdentification().stream()
                    .map(PersonIdentification::getPersonName)
                    .flatMap(personNames -> personNames.stream());

            return personNameStream.map(personName ->
                    personName == null ?
                            "" : personName.getBirthName() + " " + personName.getGivenName()
            ).findFirst().orElse("");
        }
        /*final Stream<PartyName> partyNameStream = party.getIdentification().stream()
                .map(PartyIdentificationInformation::getPartyName)
                .flatMap(partyNames -> partyNames.stream());

        if (party instanceof Organisation) {
            return partyNameStream.map(partyName -> {
                OrganisationName organisationName = (OrganisationName) partyName;
                return organisationName == null ? organisationName.getLegalName() : organisationName.getName();
            }).findFirst().orElse("");
        } else {
            return partyNameStream.map(partyName -> {
                PersonName personName = (PersonName) partyName;
                return personName == null ?
                        personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName() : personName.getName();
            }).findFirst().orElse("");
        }*/
    }
}
