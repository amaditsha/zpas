package zw.co.esolutions.zpas.services.iface.account;

import zw.co.esolutions.zpas.dto.account.AccountUploadDTO;
import zw.co.esolutions.zpas.model.CashAccount;
import zw.co.esolutions.zpas.model.UploadAccounts;

import java.util.List;
import java.util.Optional;

public interface UploadAccountsService {
    List<CashAccount> uploadAccounts(AccountUploadDTO uploadDTO);
    Optional<UploadAccounts> getUploadAccounts(String id);
}

