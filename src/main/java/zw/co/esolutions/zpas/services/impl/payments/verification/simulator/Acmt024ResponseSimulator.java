package zw.co.esolutions.zpas.services.impl.payments.verification.simulator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

;

/**
 * Created by alfred on 22 May 2019
 */
@Slf4j
@Component
@Transactional
public class Acmt024ResponseSimulator {
//    static JAXBContext jaxbContextForAcmt024;
//
//    static {
//        try {
//            jaxbContextForAcmt024 = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.acmt024_001_02");
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Value("${hub.reply.queue.url}")
//    public String hubReplyQueue;
//
//    @Autowired
//    CamelContext camelContext;
//
//    @Autowired
//    private PaymentRepository paymentRepository;
//
//    public boolean simulateHubResponse(String message) {
//        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
//        producerTemplate.sendBody(hubReplyQueue, message);
//        return true;
//    }
//
//    public void simulateIdentificationVerificationReport(String paymentId) {
//        Optional<Payment> optionalPayment = paymentRepository.findById(paymentId);
//        optionalPayment.ifPresent(payment -> {
//            log.info("Payment found, simulating response...");
//
//            final IdentificationVerificationReportV02 identificationVerificationReportV02 = buildIdentificationVerificationReportV02(payment);
//
//            try {
//                simulateHubResponse(buildXml(identificationVerificationReportV02));
//            } catch (JAXBException e) {
//                e.printStackTrace();
//            }
//        });
//    }
//
//    private IdentificationVerificationReportV02 buildIdentificationVerificationReportV02(Payment payment) {
//        IdentificationVerificationReportV02 identificationVerificationReportV02 = new IdentificationVerificationReportV02();
//        identificationVerificationReportV02.setAssgnmt(new IdentificationAssignment2());
//        identificationVerificationReportV02.setOrgnlAssgnmt(new MessageIdentification5());
//
//
//        List<IndividualPayment> individualPayments = new ArrayList<>();
//        if (payment instanceof BulkPayment) {
//            final List<IndividualPayment> groups = ((BulkPayment) payment).getGroups();
//            individualPayments.addAll(groups);
//        } else {
//            individualPayments.add((IndividualPayment) payment);
//        }
//
//        individualPayments.forEach(individualPayment -> {
//            boolean verificationSuccessful = true ;
//            VerificationReport2 verificationReport2 = new VerificationReport2();
//            verificationReport2.setOrgnlId(individualPayment.getEndToEndId());
//            verificationReport2.setVrfctn(verificationSuccessful);
//            VerificationReason1Choice verificationReason1Choice = new VerificationReason1Choice();
//            verificationReason1Choice.setCd(verificationSuccessful? "SUCCESS": "FAIL");
//            verificationReason1Choice.setPrtry(verificationSuccessful? "SUCCESS": "FAIL");
//
//            verificationReport2.setRsn(verificationReason1Choice);
//            verificationReport2.setOrgnlPtyAndAcctId(new IdentificationInformation2());
//            verificationReport2.setUpdtdPtyAndAcctId(new IdentificationInformation2());
//
//            identificationVerificationReportV02.getRpt().add(verificationReport2);
//        });
//
//        return identificationVerificationReportV02;
//    }
//
//    private String buildXml(IdentificationVerificationReportV02 identificationVerificationRequestV02) throws JAXBException {
//        log.info("Building ACMT.024 XML message..");
//        final Document document = new Document();
//        document.setIdVrfctnRpt(identificationVerificationRequestV02);
//
//        Marshaller jaxbMarshaller = jaxbContextForAcmt024.createMarshaller();
//        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//
//        StringWriter stringWriter = new StringWriter();
//        jaxbMarshaller.marshal(document, stringWriter);
//
//        String xml = stringWriter.toString();
//
////        log.info(xml);
//
//        return xml;
//    }
}
