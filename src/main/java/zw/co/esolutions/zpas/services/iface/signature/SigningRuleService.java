package zw.co.esolutions.zpas.services.iface.signature;

import zw.co.esolutions.zpas.dto.agreement.SigningRuleDTO;
import zw.co.esolutions.zpas.model.SigningRule;

import java.util.List;
import java.util.Optional;

public interface SigningRuleService {
    SigningRule createSigningRule(SigningRuleDTO signingRuleDTO);
    SigningRule updateSigningRule(SigningRuleDTO signingRule);
    SigningRule deleteSigningRule(String id);
    SigningRule approveSigningRule(String id);
    SigningRule rejectSigningRule(String id);
    Optional<SigningRule> getSigningRuleById(String signatureId);
    List<SigningRule> getSigningRules();
}
