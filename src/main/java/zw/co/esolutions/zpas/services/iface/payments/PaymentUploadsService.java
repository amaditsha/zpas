package zw.co.esolutions.zpas.services.iface.payments;

import zw.co.esolutions.zpas.dto.payments.CreateBulkPaymentDTO;
import zw.co.esolutions.zpas.dto.payments.PaymentUploadDTO;
import zw.co.esolutions.zpas.model.BulkPayment;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;

import java.util.Optional;

/**
 * Created by alfred on 26 Feb 2019
 */
public interface PaymentUploadsService {
    Optional<Payment> uploadBulkPayment(PaymentUploadDTO paymentUploadDTO) throws PaymentRequestInvalidArgumentException;
    Optional<Payment> processPaymentsApiPainRequest(String xml) throws PaymentRequestInvalidArgumentException;
    Optional<BulkPayment> createBulkPayment(CreateBulkPaymentDTO createBulkPaymentDTO) throws PaymentRequestInvalidArgumentException;
}
