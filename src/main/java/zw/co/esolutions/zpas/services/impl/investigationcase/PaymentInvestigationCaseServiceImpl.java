package zw.co.esolutions.zpas.services.impl.investigationcase;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.*;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.dto.investigationcase.InvestigationCaseStatusDTO;
import zw.co.esolutions.zpas.dto.investigationcase.InvestigationStatusDTO;
import zw.co.esolutions.zpas.dto.investigationcase.PaymentInvestigationCaseDTO;
import zw.co.esolutions.zpas.dto.payments.RequestInfo;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.investigationcase.PaymentInvestigationCaseService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.process.DebitAuthorisationResponseInitiationService;
import zw.co.esolutions.zpas.services.iface.process.InvestigationCaseInitiationService;
import zw.co.esolutions.zpas.services.impl.payments.sequence.EndToEndSequenceProcessor;
import zw.co.esolutions.zpas.services.impl.process.ClaimNonReceiptInvestigationCaseInitiationServiceImpl;
import zw.co.esolutions.zpas.services.impl.process.pojo.DebitAuthorisationDTO;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentPartyRoleUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.persistence.EntityManager;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class PaymentInvestigationCaseServiceImpl implements PaymentInvestigationCaseService {

    @Autowired
    private PaymentInvestigationCaseRepository paymentInvestigationCaseRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private DebitAuthorisationResponseInitiationService debitAuthorisationResponseInitiationService;

    @Autowired
    private PaymentExecutionRepository paymentExecutionRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private InvestigationCaseInitiationService investigationCaseInitiationService;

    @Autowired
    private ClaimNonReceiptInvestigationCaseInitiationServiceImpl claimNonReceiptInvestigationCaseInitiationService;

    @Autowired
    PaymentPartyRoleUtil paymentPartyRoleUtil;

    @Autowired
    PartyRepository partyRepository;

    @Autowired
    PaymentInvestigationCaseResolutionRepository paymentInvestigationCaseResolutionRepository;

    @Autowired
    EndToEndSequenceProcessor endToEndSequenceProcessor;

    @Autowired
    private final EntityManager entityManager;

    public PaymentInvestigationCaseServiceImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private PaymentInvestigationCase convertPaymentInvestigationCaseDtoToEntity(PaymentInvestigationCaseDTO paymentInvestigationCaseDTO) {
        PaymentInvestigationCase paymentInvestigationCase = new PaymentInvestigationCase();
        paymentInvestigationCase.setCancellationReason(CancellationReasonCode.valueOf(paymentInvestigationCaseDTO.getCancellationReason()));
        paymentInvestigationCase.setCaseType(paymentInvestigationCaseDTO.getCaseType());

        paymentInvestigationCase.setId(GenerateKey.generateEntityId());
        paymentInvestigationCase.setDateCreated(OffsetDateTime.now());
        paymentInvestigationCase.setLastUpdated(OffsetDateTime.now());
        paymentInvestigationCase.setEntityStatus(EntityStatus.ACTIVE);
        paymentInvestigationCase.setEntityVersion(0L);

        paymentInvestigationCase.setAssignmentIdentification(endToEndSequenceProcessor.generateMessageId("IBCRCR","0",OffsetDateTime.now()));
        if(StringUtils.isBlank(paymentInvestigationCaseDTO.getIdentification())){
            paymentInvestigationCase.setIdentification(endToEndSequenceProcessor.generateEndToEndId("IBCRCR","0",OffsetDateTime.now()));
        }else{
            paymentInvestigationCase.setIdentification(paymentInvestigationCaseDTO.getIdentification());
        }
//        paymentInvestigationCase.setReassignment(new Reassignment());
//        paymentInvestigationCase.setMissingCoverIndication(false);
//        paymentInvestigationCase.setIncorrectInformationReason(new UnableToApplyIncorrectInfoCode());
//        paymentInvestigationCase.setMissingInformationReason(new UnableToApplyMissingInformationV2Code());
        paymentInvestigationCase.setCreationDateTime(OffsetDateTime.now());

        InvestigationCaseStatus investigationCaseStatus = new InvestigationCaseStatus();
        investigationCaseStatus.setCaseStatus(CaseStatusCode.Unknown);
        investigationCaseStatus.setInvestigationCase(paymentInvestigationCase);
        investigationCaseStatus.setId(UUID.randomUUID().toString());
        investigationCaseStatus.setEntityStatus(EntityStatus.ACTIVE);
        investigationCaseStatus.setStatusDateTime(OffsetDateTime.now());
        investigationCaseStatus.setDateCreated(OffsetDateTime.now());
//        investigationCaseStatus.setValidityTime(new DateTimePeriod());
        investigationCaseStatus.setStatusDescription("Initialisation");
        investigationCaseStatus.setTransactionProcessingStatus(InstructionProcessingStatusCode.None);

        StatusReason statusReason = new StatusReason();
        statusReason.setId(UUID.randomUUID().toString());
        statusReason.setEntityStatus(EntityStatus.ACTIVE);
        statusReason.setStatus(investigationCaseStatus);
        statusReason.setReason("Initialisation");
        statusReason.setDateCreated(OffsetDateTime.now());
        statusReason.setStatus(investigationCaseStatus);

        investigationCaseStatus.setStatusReason(Arrays.asList(statusReason));

        paymentInvestigationCase.setStatus(Arrays.asList(investigationCaseStatus));


/**
 * Set the payment instruction
 * */

        if(paymentInvestigationCaseDTO.getUnderlyingInstructionId() != null) {
            Optional<PaymentExecution> paymentExecutionOptional = paymentExecutionRepository
                    .findById(paymentInvestigationCaseDTO.getUnderlyingInstructionId());
            paymentExecutionOptional.ifPresent(paymentInvestigationCase::setUnderlyingInstruction);
        }


//        paymentInvestigationCase.setDuplicateCaseResolution(new DuplicateCase());
//        paymentInvestigationCase.setInvestigationResolution(new ArrayList<>());
//        paymentInvestigationCase.setOriginalInvestigationCase(new ArrayList<>());
//        paymentInvestigationCase.setLinkedCase(new ArrayList<>());
//        paymentInvestigationCase.setReassignment(new Reassignment());


        String paymentId = paymentInvestigationCaseDTO.getPaymentId();

        if (StringUtils.isNoneBlank(paymentId)) {

            Optional<Payment> paymentOptional = paymentsService.getPaymentById(paymentId);
            paymentOptional.ifPresent(payment -> {
                paymentInvestigationCase.setUnderlyingPayment(Arrays.asList(payment));
                //paymentInvestigationCase.setUnderlyingInstruction(new PaymentExecution());
                paymentInvestigationCase.setPaymentStatus(payment.getPaymentStatus());
//                paymentInvestigationCase.setCancellationReason(CancellationReasonCode.valueOf(paymentInvestigationCaseDTO.getCancellationReason()));
//                paymentInvestigationCase.setCaseType(paymentInvestigationCaseDTO.getCaseType());
            });
        }
        return paymentInvestigationCase;
    }

    @Override
    public PaymentInvestigationCase createPaymentInvestigationCase(PaymentInvestigationCaseDTO paymentInvestigationCaseDTO, String clientId) {

        log.info("paymentId: " + paymentInvestigationCaseDTO.getPaymentId());

        PaymentInvestigationCase paymentInvestigationCase = convertPaymentInvestigationCaseDtoToEntity(paymentInvestigationCaseDTO);

        List<InvestigationPartyRole> investigationPartyRoles = new ArrayList<>();

//        paymentInvestigationCase.setInvestigationPartyRole(investigationPartyRoles);
        //get the payment for the case
        Optional<Payment> paymentOptional = paymentsService.getPaymentById(paymentInvestigationCaseDTO.getPaymentId());

        /**
         * Find CaseCreator Role Player
         */

        Payment payment = paymentOptional.get();

        Optional<InvestigationPartyRole> caseCreatorRoleOptional;

        if(payment instanceof BulkPayment) {
            caseCreatorRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(payment, DebtorRole.class)
                    .map(paymentPartyRole -> {
                        return getCaseCreatorPartyRole(paymentInvestigationCase, paymentPartyRole);
                    });
        } else {
            IndividualPayment individualPayment = (IndividualPayment)payment;
            if(individualPayment.getBulkPayment() != null) {
                caseCreatorRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(individualPayment.getBulkPayment(), DebtorRole.class)
                        .map(paymentPartyRole -> {
                            return getCaseCreatorPartyRole(paymentInvestigationCase, paymentPartyRole);
                        });
            } else {
                caseCreatorRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(payment, DebtorRole.class)
                        .map(paymentPartyRole -> {
                            return getCaseCreatorPartyRole(paymentInvestigationCase, paymentPartyRole);
                        });
            }
        }

        caseCreatorRoleOptional.ifPresent(investigationPartyRole -> investigationPartyRoles.add(investigationPartyRole));

        /**
         * Find Assigner Role Player
         */
        Optional<InvestigationPartyRole> assignerRoleOptional;
        if(payment instanceof BulkPayment) {
            assignerRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(payment, DebtorAgentRole.class)
                    .map(paymentPartyRole -> {
                        return getAssignerPartyRole(paymentInvestigationCase, paymentPartyRole);
                    });
        } else {
            IndividualPayment individualPayment = (IndividualPayment)payment;
            if(individualPayment.getBulkPayment() != null) {
                assignerRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(individualPayment.getBulkPayment(), DebtorAgentRole.class)
                        .map(paymentPartyRole -> {
                            return getAssignerPartyRole(paymentInvestigationCase, paymentPartyRole);
                        });
            } else {
                assignerRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(payment, DebtorAgentRole.class)
                        .map(paymentPartyRole -> {
                            return getAssignerPartyRole(paymentInvestigationCase, paymentPartyRole);
                        });
            }
        }

        assignerRoleOptional.ifPresent(investigationPartyRole -> investigationPartyRoles.add(investigationPartyRole));

        /**
         * Find Assignee Role Player
         */

        Optional<InvestigationPartyRole> assigneeRoleOptional;

        if(payment instanceof BulkPayment) {
            log.info("The payment is BULK PAYMENT, setting the assignee same as assigner [DebtorAgentRole]");
            assigneeRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(payment, DebtorAgentRole.class)
                    .map(paymentPartyRole -> {
                        log.info("Found DebtorAgentRole.. creating the Assigner");
                        return getAssigneePartyRole(paymentInvestigationCase, paymentPartyRole);
                    });
        } else {
            log.info("The payment is INDIVIDUAL PAYMENT, setting appropriate assignee [CreditorAgentRole]");
            assigneeRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(payment, CreditorAgentRole.class)
                    .map(paymentPartyRole -> {
                        log.info("Found CreditorAgentRole.. creating the Assignee");
                        return getAssigneePartyRole(paymentInvestigationCase, paymentPartyRole);
                    });
        }

        assigneeRoleOptional.ifPresent(investigationPartyRole -> {
            log.info("Assignee Role: " + investigationPartyRole);
            investigationPartyRoles.add(investigationPartyRole);
        });

        log.info("No. of investigation party roles: {}", investigationPartyRoles.size() + "the party roles: " + investigationPartyRoles);

        paymentInvestigationCase.setInvestigationPartyRole(investigationPartyRoles);

        payment.setRelatedInvestigationCase(paymentInvestigationCase);

        // paymentInvestigationCase.addUnderlyingPayment(underlyingPayment);

        Payment updatedPayment = paymentRepository.save(payment);

        return updatedPayment.getRelatedInvestigationCase();

    }

    private InvestigationPartyRole getAssignerPartyRole(PaymentInvestigationCase paymentInvestigationCase, PaymentPartyRole paymentPartyRole) {
        log.info("Found DebtorAgentRole.. creating the Assigner");
        Assigner assigner = new Assigner();
        assigner.setPartyRoleCode(PartyRoleCode.FirstAgent);
        //assignee.setStatus(Lists.newArrayList());
        assigner.addInvestigationCase(paymentInvestigationCase);
        assigner.setId(GenerateKey.generateEntityId());
        assigner.setEntityStatus(EntityStatus.ACTIVE);
        assigner.setHasCustomParty(paymentPartyRole.isHasCustomParty());
        if (paymentPartyRole.isHasCustomParty()) {
            log.info("Setting the custom party for for assigner");
            assigner.setCustomParty(paymentPartyRole.getCustomParty());
        }
        paymentPartyRole.getPlayer().stream().findFirst().ifPresent(rolePlayer -> {
            rolePlayer.addRole(assigner);
            assigner.addPlayer(rolePlayer);
            log.info("Added Assigner Role");
        });
        return assigner;
    }

    private InvestigationPartyRole getCaseCreatorPartyRole(PaymentInvestigationCase paymentInvestigationCase, PaymentPartyRole paymentPartyRole) {
        log.info("Found DebtorRole.. creating the CaseCreator");
        CaseCreator caseCreator = new CaseCreator();
        caseCreator.setPartyRoleCode(PartyRoleCode.FirstAgent);
        caseCreator.addInvestigationCase(paymentInvestigationCase);
        //assignee.setStatus(Lists.newArrayList());
        caseCreator.setId(GenerateKey.generateEntityId());
        caseCreator.setEntityStatus(EntityStatus.ACTIVE);
        caseCreator.setHasCustomParty(paymentPartyRole.isHasCustomParty());
        if (paymentPartyRole.isHasCustomParty()) {
            log.info("Setting the custom party for for assignee");
            caseCreator.setCustomParty(paymentPartyRole.getCustomParty());
        }
        paymentPartyRole.getPlayer().stream().findFirst().ifPresent(rolePlayer -> {
            rolePlayer.addRole(caseCreator);
            caseCreator.addPlayer(rolePlayer);
            log.info("Added CaseCreator Role");
        });

        return caseCreator;
    }

    private InvestigationPartyRole getAssigneePartyRole(PaymentInvestigationCase paymentInvestigationCase, PaymentPartyRole paymentPartyRole) {
        Assignee assignee = new Assignee();
        assignee.setPartyRoleCode(PartyRoleCode.FinalAgent);
        //assignee.setStatus(Lists.newArrayList());
        assignee.addInvestigationCase(paymentInvestigationCase);
        assignee.setId(GenerateKey.generateEntityId());
        assignee.setEntityStatus(EntityStatus.ACTIVE);
        assignee.setHasCustomParty(paymentPartyRole.isHasCustomParty());
        if (paymentPartyRole.isHasCustomParty()) {
            log.info("Setting the custom party for for assignee");
            assignee.setCustomParty(paymentPartyRole.getCustomParty());
        }
        paymentPartyRole.getPlayer().stream().findFirst().ifPresent(rolePlayer -> {
            rolePlayer.addRole(assignee);
            assignee.addPlayer(rolePlayer);
            log.info("Added Assignee Role");
        });
        return assignee;
    }

    @Override
    public PaymentInvestigationCase updatePaymentInvestigationCase(PaymentInvestigationCase paymentInvestigationCase) {
        paymentInvestigationCase.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        return paymentInvestigationCaseRepository.save(paymentInvestigationCase);
    }

    @Override
    public PaymentInvestigationCase deletePaymentInvestigationCase(String id) {

        Optional<PaymentInvestigationCase> optionalPaymentInvestigationCase = paymentInvestigationCaseRepository.findById(id);
        PaymentInvestigationCase paymentInvestigationCase = null;

        if (optionalPaymentInvestigationCase.isPresent()) {
            paymentInvestigationCase = optionalPaymentInvestigationCase.get();
            paymentInvestigationCase.setEntityStatus(EntityStatus.DELETED);
            paymentInvestigationCase = paymentInvestigationCaseRepository.save(paymentInvestigationCase);
        } else {
            log.info("Payment Investigation Case has already been deleted");
        }
        return paymentInvestigationCase;
    }

    @Override
    public PaymentInvestigationCase approvePaymentInvestigationCase(String id) {
        Optional<PaymentInvestigationCase> optionalPaymentInvestigationCase = paymentInvestigationCaseRepository.findById(id);
        PaymentInvestigationCase paymentInvestigationCase = null;
        if (optionalPaymentInvestigationCase.isPresent()) {
            paymentInvestigationCase = optionalPaymentInvestigationCase.get();
            if (paymentInvestigationCase.getEntityStatus() != EntityStatus.DELETED) {
                paymentInvestigationCase.setEntityStatus(EntityStatus.ACTIVE);
                paymentInvestigationCase = paymentInvestigationCaseRepository.save(paymentInvestigationCase);
            } else {
                log.info("Payment Investigation Case has already been approved");
            }
        }
        return null;
    }

    @Override
    public PaymentInvestigationCase rejectPaymentInvestigationCase(String id) {
        Optional<PaymentInvestigationCase> optionalPaymentInvestigationCase = paymentInvestigationCaseRepository.findById(id);
        PaymentInvestigationCase paymentInvestigationCase = null;
        if (optionalPaymentInvestigationCase.isPresent()) {
            paymentInvestigationCase = optionalPaymentInvestigationCase.get();
            if (paymentInvestigationCase.getEntityStatus() != EntityStatus.DELETED) {
                paymentInvestigationCase.setEntityStatus(EntityStatus.DISAPPROVED);
                paymentInvestigationCase = paymentInvestigationCaseRepository.save(paymentInvestigationCase);
            } else {
                log.info("Payment InvestigationCase " + id + "has already been rejected");
            }
        }
        return paymentInvestigationCase;
    }

    @Override
    public PaymentInvestigationCase findPaymentInvestigationCaseById(String id) {
        Optional<PaymentInvestigationCase> optionalPaymentInvestigationCase = this.paymentInvestigationCaseRepository.findById(id);
        PaymentInvestigationCase paymentInvestigationCase = null;

        if (optionalPaymentInvestigationCase.isPresent()) {
            paymentInvestigationCase = optionalPaymentInvestigationCase.get();
        }
        return paymentInvestigationCase;
    }

    @Override
    public PaymentInvestigationCase findById(String id) {
        return paymentInvestigationCaseRepository.findById(id).get();
    }

    @Override
    public List<PaymentInvestigationCase> getAllByCaseId(String caseId) {
        return paymentInvestigationCaseRepository.getAllByCaseId(caseId);
    }

    @Override
    public PaymentInvestigationCase aggregatePaymentReq(RequestInfo requestInfo) {
        PaymentInvestigationCase paymentInvestigationCase = null;
        switch (requestInfo.getRequestType()) {
            case "CPCR":
                log.info("Processing the customer payment cancellation request");
                paymentInvestigationCase = buildAndSendCustomerCancellationInvestigationCase(requestInfo);
                break;
            case "CNR":
                log.info("Processing the client non receipt request");
                paymentInvestigationCase = buildAndSendClaimNonReceiptInvestigationCase(requestInfo);
                break;
            default:
                log.info("The request type is {} unknown can't be processed.");
        }
        return paymentInvestigationCase;
    }

    @Override
    public List<PaymentInvestigationCase> searchInvestigationCase(String searchTerm) {
        FullTextEntityManager fullTextEm = org.hibernate.search.jpa.Search
                .getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEm.getSearchFactory().buildQueryBuilder().forEntity(PaymentInvestigationCase.class).get();

        FullTextQuery fullTextQuery = fullTextEm.createFullTextQuery(queryBuilder.keyword()
                .onFields("caseType")
                .matching(searchTerm)
                .createQuery(), PaymentInvestigationCase.class);

        List<PaymentInvestigationCase> resultList = fullTextQuery.getResultList();
        /*List<Payment> underlyingPaymentList = new ArrayList<>();

        resultList.forEach(paymentInvestigationCase -> {
            log.info("Payments: " + paymentInvestigationCase.getUnderlyingPayment());
            underlyingPaymentList.addAll(paymentInvestigationCase.getUnderlyingPayment());

        });*/

        return resultList;
    }

    @Override
    public String refreshPaymentInvestigationCaseSearchIndex() {
        FullTextSession fullTextSession = Search.getFullTextSession(entityManager.unwrap(Session.class));
        fullTextSession.setFlushMode(FlushMode.MANUAL);
        fullTextSession.setCacheMode(CacheMode.IGNORE);
        int BATCH_SIZE = 25;
        ScrollableResults scrollableResults = fullTextSession.createCriteria(PaymentInvestigationCase.class)
                .setFetchSize(BATCH_SIZE)
                .scroll(ScrollMode.FORWARD_ONLY);
        int index = 0;
        while (scrollableResults.next()) {
            index++;
            fullTextSession.index(scrollableResults.get(0)); //index each element
            if (index % BATCH_SIZE == 0) {
                fullTextSession.flushToIndexes(); //apply changes to indexes
                fullTextSession.clear(); //free memory since the queue is processed
            }
        }
        return "Message search indices reloaded successfully";
    }

    @Override
    public PaymentInvestigationCase debitAuthorisationResponseInitiation(DebitAuthorisationDTO dto) {
        PaymentInvestigationCase paymentInvestigationCase = null;

        Optional<PaymentInvestigationCase> paymentInvestigationCaseOptional = paymentInvestigationCaseRepository
                .findById(dto.getPaymentInvestigationCaseId());
        if (paymentInvestigationCaseOptional.isPresent()) {
            paymentInvestigationCase = paymentInvestigationCaseOptional.get();
        }
        dto.setPaymentInvestigationCase(paymentInvestigationCase);

        /**
         * Initiate debit authorisation response
         */
        debitAuthorisationResponseInitiationService.initiateDebitAuthorisationResponse(dto);

        return paymentInvestigationCase;
    }


    @Override
    public Optional<InvestigationStatusDTO> getInvestigationStatusesByClientId(String clientId) {
        final List<Role> clientRoles = roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof CaseCreator || role instanceof Assignee
                        || role instanceof Assigner || role instanceof StatusOriginator)
                .collect(Collectors.toList());

        Set<PaymentInvestigationCase> investigationCases =
                paymentInvestigationCaseRepository.findAllByInvestigationPartyRoleIn(clientRoles)
                        .stream().collect(Collectors.toSet());

        final long totalCases = investigationCases.size();
        final long totalOpenCases = investigationCases.stream()
                .filter(paymentInvestigationCase -> {
                    if (paymentInvestigationCase.getStatus() == null) {
                        return false;
                    }
                    Optional<InvestigationCaseStatus> optionalLatestInvestigationCaseStatus = paymentInvestigationCase.getStatus().stream()
                            .sorted(Comparator.comparing(InvestigationCaseStatus::getDateCreated).reversed())
                            .findFirst();

                    return optionalLatestInvestigationCaseStatus
                            .map(investigationCaseStatus -> !investigationCaseStatus.getCaseStatus().equals(CaseStatusCode.Closed))
                            .orElse(false);
                }).count();

        final long totalClosedCases = totalCases - totalOpenCases;

        InvestigationStatusDTO investigationStatusDTO = new InvestigationStatusDTO();
        investigationStatusDTO.setTotalInvestigations(totalCases);
        investigationStatusDTO.setTotalOpenInvestigations(totalOpenCases);
        investigationStatusDTO.setTotalClosedInvestigations(totalClosedCases);

        log.info("Total Cases....." + totalCases);
        log.info("Total Open Cases Under Investigation..." + totalOpenCases);
        log.info("Total Closed Cases..." + totalOpenCases);
        return Optional.ofNullable(investigationStatusDTO);
    }

    private PaymentInvestigationCase buildAndSendClaimNonReceiptInvestigationCase(RequestInfo requestInfo) {
        PaymentInvestigationCaseDTO paymentInvestigationCaseDTO = new PaymentInvestigationCaseDTO();
        paymentInvestigationCaseDTO.setPaymentId(requestInfo.getId());
        paymentInvestigationCaseDTO.setCancellationReason(CancellationReasonCode.RequestedByCustomer.name());
        paymentInvestigationCaseDTO.setMissingCoverIndication(false);
        paymentInvestigationCaseDTO.setIncorrectInformationReason(requestInfo.getReason());
        paymentInvestigationCaseDTO.setMissingInformationReason("");
        paymentInvestigationCaseDTO.setCaseType(InvestigationCaseType.ClaimNonReceipt.getCodeName());
        paymentInvestigationCaseDTO.setAssignmentIdentification("");
        paymentInvestigationCaseDTO.setCreationDateTime("");
        paymentInvestigationCaseDTO.setIdentification("");
        paymentInvestigationCaseDTO.setReassignment("");


        Optional<Payment> paymentOptional = paymentsService.getPaymentById(requestInfo.getId());
        paymentOptional.ifPresent(payment -> {
            log.info("Payment present for getting underlying instruction id: " + payment.getId());
            if(payment instanceof IndividualPayment) {
                log.info("Working with an Individual Payment.");
                IndividualPayment individualPayment = (IndividualPayment) payment;
                if(individualPayment.getBulkPayment() == null) {
                    log.info("Working with an individual payment that is not in a bulk.");
                    payment.getPaymentExecution().stream().findFirst().ifPresent(paymentExecution -> {
                        log.info("Payment execution present...");
                        paymentInvestigationCaseDTO.setUnderlyingInstructionId(paymentExecution.getId());
                    });
                } else {
                    log.info("Working with an individual payment that is in a bulk.");
                    BulkPayment bulkPayment = individualPayment.getBulkPayment();
                    bulkPayment.getPaymentExecution().stream().findFirst().ifPresent(paymentExecution -> {
                        log.info("Payment execution present...");
                        paymentInvestigationCaseDTO.setUnderlyingInstructionId(paymentExecution.getId());
                    });
                }
            } else {
                log.error("This payment is not a bulk payment. ");
            }
        });

        if(!paymentOptional.isPresent()) {
            log.error("The payment with ID {} is not present ", requestInfo.getId());
        }
        PaymentInvestigationCase clientNonReceiptInvestigationCase = createPaymentInvestigationCase(paymentInvestigationCaseDTO, requestInfo.getClientId());

        log.info("Invoking the claim non receipt init ..");
        claimNonReceiptInvestigationCaseInitiationService.initiateClaimNonReceiptRequest(clientNonReceiptInvestigationCase);


        return clientNonReceiptInvestigationCase;
    }

    private PaymentInvestigationCase buildAndSendCustomerCancellationInvestigationCase(RequestInfo requestInfo) {

        PaymentInvestigationCaseDTO paymentInvestigationCaseDTO = new PaymentInvestigationCaseDTO();
        paymentInvestigationCaseDTO.setPaymentId(requestInfo.getId());
        paymentInvestigationCaseDTO.setCancellationReason(CancellationReasonCode.RequestedByCustomer.name());
        paymentInvestigationCaseDTO.setMissingCoverIndication(false);
        paymentInvestigationCaseDTO.setIncorrectInformationReason(requestInfo.getReason());
        paymentInvestigationCaseDTO.setMissingInformationReason("");
        paymentInvestigationCaseDTO.setCaseType(InvestigationCaseType.PaymentCancellation.getCodeName());
        paymentInvestigationCaseDTO.setAssignmentIdentification("");
        paymentInvestigationCaseDTO.setCreationDateTime("");
        paymentInvestigationCaseDTO.setIdentification("");
        paymentInvestigationCaseDTO.setReassignment("");


        Optional<Payment> paymentOptional = paymentsService.getPaymentById(requestInfo.getId());
        paymentOptional.ifPresent(payment -> {
            log.info("Payment present for getting underlying instruction id: " + payment.getId());
            if(payment instanceof BulkPayment) {
                log.info("Working with a Bulk Payment.");
                payment.getPaymentExecution().stream().findFirst().ifPresent(paymentExecution -> {
                    log.info("Payment execution present...");
                    paymentInvestigationCaseDTO.setUnderlyingInstructionId(paymentExecution.getId());
                });
            } else {
                log.info("Working with an Individual Payment.");
                IndividualPayment individualPayment = (IndividualPayment) payment;
                if(individualPayment.getBulkPayment() == null) {
                    log.info("Working with an individual payment that is not in a bulk.");
                    payment.getPaymentExecution().stream().findFirst().ifPresent(paymentExecution -> {
                        log.info("Payment execution present...");
                        paymentInvestigationCaseDTO.setUnderlyingInstructionId(paymentExecution.getId());
                    });
                } else {
                    log.info("Working with an individual payment that is in a bulk.");
                    BulkPayment bulkPayment = individualPayment.getBulkPayment();
                    bulkPayment.getPaymentExecution().stream().findFirst().ifPresent(paymentExecution -> {
                        log.info("Payment execution present...");
                        paymentInvestigationCaseDTO.setUnderlyingInstructionId(paymentExecution.getId());
                    });
                }
            }
        });

        if(!paymentOptional.isPresent()) {
            log.error("The payment with ID {} is not present ", requestInfo.getId());
        }

        PaymentInvestigationCase customerCancellationInvestigationCase = createPaymentInvestigationCase(paymentInvestigationCaseDTO, requestInfo.getClientId());


        log.info("Invoking the payment cancellation init ..");
        investigationCaseInitiationService.initiateCustomerPaymentCancellationRequest(customerCancellationInvestigationCase);
        return customerCancellationInvestigationCase;
    }

    @Override
    public PaymentInvestigationCase updateInvestigationCaseStatus(InvestigationCaseStatusDTO investigationCaseStatusDTO) {
        log.info("Updating the investigation case status given resource below \n{}\n", investigationCaseStatusDTO);
        PaymentInvestigationCase savedPaymentInvestigationCase = paymentInvestigationCaseRepository.findById(investigationCaseStatusDTO.getPaymentInvestigationCaseId()).map(paymentInvestigationCase -> {
            InvestigationCaseStatus investigationCaseStatus = new InvestigationCaseStatus();
            investigationCaseStatus.setCaseStatus(CaseStatusCode.valueOf(investigationCaseStatusDTO.getCaseStatusCode()));
            investigationCaseStatus.setInvestigationCase(paymentInvestigationCase);
            investigationCaseStatus.setId(UUID.randomUUID().toString());
            investigationCaseStatus.setEntityStatus(EntityStatus.NONE);
            investigationCaseStatus.setDateCreated(OffsetDateTime.now());

            StatusReason statusReason = new StatusReason();
            statusReason.setId(UUID.randomUUID().toString());
            statusReason.setEntityStatus(EntityStatus.NONE);
            statusReason.setDateCreated(OffsetDateTime.now());
            statusReason.setReason(investigationCaseStatusDTO.getNarrative());
            statusReason.setRejectedStatusReason(RejectedStatusReasonCode.valueOf(investigationCaseStatusDTO.getRejectedStatusReasonCode()));
            statusReason.setFailingReason(PendingFailingReasonCode.valueOf(investigationCaseStatusDTO.getFailingReasonCode()));
            statusReason.setRejectionReason(RejectionReasonV2Code.valueOf(investigationCaseStatusDTO.getRejectionReasonV2Code()));
            statusReason.setStatus(investigationCaseStatus);

            investigationCaseStatus.setStatusReason(Lists.newArrayList(statusReason));
            investigationCaseStatus.setStatusDateTime(OffsetDateTime.now());
            investigationCaseStatus.setStatusDescription(investigationCaseStatusDTO.getNarrative());
            investigationCaseStatus.setInstructionProcessingStatus(StatusCode.valueOf(investigationCaseStatusDTO.getStatusCode()));
            investigationCaseStatus.setCancellationProcessingStatus(CancellationProcessingStatusCode.valueOf(investigationCaseStatusDTO.getCancellationProcessingStatusCode()));
//                investigationCaseStatus.setPartyRole(new InvestigationPartyRole());

            List<InvestigationCaseStatus> caseStatuses = new ArrayList<>(paymentInvestigationCase.getStatus());
            caseStatuses.add(investigationCaseStatus);
            paymentInvestigationCase.setStatus(caseStatuses);

            PaymentInvestigationCase investigationCase = paymentInvestigationCaseRepository.save(paymentInvestigationCase);
            return investigationCase;
        }).orElseGet(() -> {
            log.info("Failed to get payment investigation case by id \t{}", investigationCaseStatusDTO.getPaymentInvestigationCaseId());
            return null;
        });

        if(savedPaymentInvestigationCase != null) {
            this.generatePaymentResolution(savedPaymentInvestigationCase, investigationCaseStatusDTO);
        }
        return savedPaymentInvestigationCase;
    }

    @Override
    public List<PaymentInvestigationCase> getPaymentInvestigationCasesByPayment(String paymentId) {
        return paymentRepository.findAllById(paymentId).stream().map(
                payment -> paymentInvestigationCaseRepository.findAllByUnderlyingPayment(payment)
        ).flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private void generatePaymentResolution(PaymentInvestigationCase paymentInvestigationCase, InvestigationCaseStatusDTO investigationCaseStatusDTO) {

        //initialise the resolution
        PaymentInvestigationCaseResolution paymentInvestigationCaseResolution = new PaymentInvestigationCaseResolution();
        paymentInvestigationCaseResolution.setId(UUID.randomUUID().toString());

        InvestigationExecutionConfirmationCode investigationExecutionConfirmationCode = getInvestigationExecutionCode(investigationCaseStatusDTO.getStatusCode());

        paymentInvestigationCaseResolution.setInvestigationStatus(investigationExecutionConfirmationCode);
        //paymentInvestigationCaseResolution.setDebitAuthorisationConfirmation();
        //paymentInvestigationCaseResolution.setCoverCorrection();
        //paymentInvestigationCaseResolution.setEntryCorrection();
        //paymentInvestigationCaseResolution.setPaymentExecutionCorrection();
        //paymentInvestigationCaseResolution.setDuplicateCase();
        paymentInvestigationCaseResolution.setEntityStatus(EntityStatus.NONE);
        paymentInvestigationCaseResolution.setInvestigationCase(paymentInvestigationCase);
        paymentInvestigationCaseResolution.setInvestigationCaseReference(paymentInvestigationCase.getIdentification());

        if(investigationExecutionConfirmationCode.equals(InvestigationExecutionConfirmationCode.RejectedCancellationRequest)) {
            log.info("This Resolution was REJECTED.. updating resolution with Rejection");

            PaymentInvestigationCaseRejection paymentInvestigationCaseRejection = new PaymentInvestigationCaseRejection();
            paymentInvestigationCaseRejection.setId(UUID.randomUUID().toString());
            paymentInvestigationCaseRejection.setEntityStatus(EntityStatus.NONE);
            //    paymentInvestigationCaseRejection.setRejectedModification();
            //    paymentInvestigationCaseRejection.setRejectedCancellation();
            paymentInvestigationCaseRejection.setRejectedCancellationReason(investigationCaseStatusDTO.getNarrative());
            //    paymentInvestigationCaseRejection.setAssignmentCancellationConfirmation();
            //   paymentInvestigationCaseRejection.setRejectionReason();
            paymentInvestigationCaseRejection.setRelatedInvestigationCaseResolution(paymentInvestigationCaseResolution);
            //    paymentInvestigationCaseRejection.setInvestigationRejection();

            paymentInvestigationCaseResolution.setInvestigationCaseRejection(paymentInvestigationCaseRejection);
        }

        log.info("Payment Resolution \n{}\n", paymentInvestigationCaseResolution);

        paymentInvestigationCaseResolutionRepository.save(paymentInvestigationCaseResolution);
    }

    private InvestigationExecutionConfirmationCode getInvestigationExecutionCode(String statusCodeString) {
        StatusCode statusCode;
        try {
            statusCode = StatusCode.valueOf(statusCodeString);
        } catch (Exception e) {
            e.printStackTrace();

            statusCode = StatusCode.None;
        }

        switch (statusCode) {
            case Queued:
            case BeingCancelled:
            case Pending:
            case PendingProcessing:
                return  InvestigationExecutionConfirmationCode.PendingCancellationRequest;
            case NotReceived:
            case Rejected:
                return  InvestigationExecutionConfirmationCode.RejectedCancellationRequest;
            case CancelledBySubcustodian:
            case AcknowledgedAccepted:
            case Cancelled:
            case Completed:
            case Accepted:
                return  InvestigationExecutionConfirmationCode.CancelledAsPerRequest;
            default:
                return  InvestigationExecutionConfirmationCode.UnableToApplyWillFollow;
        }
    }

}
