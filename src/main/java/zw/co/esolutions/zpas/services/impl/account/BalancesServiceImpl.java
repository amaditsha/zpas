package zw.co.esolutions.zpas.services.impl.account;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.CashBalance;
import zw.co.esolutions.zpas.repository.CashBalanceRepository;
import zw.co.esolutions.zpas.services.iface.account.BalancesService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class BalancesServiceImpl implements BalancesService {

    @Autowired CashBalanceRepository cashBalanceRepository;

    @Override
    public CashBalance createCashBalance(CashBalance cashBalance) {
        return cashBalanceRepository.save(cashBalance);
    }

    @Override
    public Optional<CashBalance> findBalanceById(String id) {
        return this.cashBalanceRepository.findById(id);
    }

    @Override
    public CashBalance updateCashBalance(CashBalance cashBalance) {
        return cashBalanceRepository.save(cashBalance);
    }

    @Override
    public CashBalance deleteCashBalance(String id) {
        Optional<CashBalance> cashBalanceOptional = cashBalanceRepository.findById(id);
        CashBalance cashBalance = null;
        if (cashBalanceOptional.isPresent()) {
            cashBalance = cashBalanceOptional.get();
            cashBalance.setEntityStatus(EntityStatus.DELETED);
            cashBalance = cashBalanceRepository.save(cashBalance);
        }
        return cashBalance;
    }
}
