package zw.co.esolutions.zpas.services.impl.registration;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.*;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.configs.AuditLoggerConfiguration;
import zw.co.esolutions.zpas.dto.profile.ProfileDTO;
import zw.co.esolutions.zpas.dto.registration.ClientCreationDTO;
import zw.co.esolutions.zpas.dto.registration.EmployingPartyRolePersonRequestDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.services.iface.country.CountryService;
import zw.co.esolutions.zpas.services.iface.registration.ClientDAO;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.utilities.audit.AuditEvents;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.RoleLevel;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class ClientServiceImpl implements ClientService {
    private static final Logger logger = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    ClientDAO clientDAO;

    @Autowired
    PartyRepository partyRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    private EmployingPartyRoleRepository employingPartyRoleRepository;

    @Autowired
    ContactPointRepository contactPointRepository;

    @Autowired
    PostalAddressRepository postalAddressRepository;

    @Autowired
    PersonProfileRepository personProfileRepository;

    @Autowired
    PersonNameRepository personNameRepository;

    @Autowired
    PersonIdentificationRepository personIdentificationRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private AccountPartyRoleRepository accountPartyRoleRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private AuditLoggerConfiguration auditLoggerConfiguration;

    @Autowired
    NotificationConfigService notificationConfigService;

    @Override
    public Person createPerson(ClientCreationDTO clientCreationDTO) {
        Person person = new Person();
        person.setId(GenerateKey.generateEntityId());
        person.setDateCreated(OffsetDateTime.now());
        person.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        person.setLastUpdated(OffsetDateTime.now());

        GenderCode gender = GenderCode.valueOf(clientCreationDTO.getGender());
        person.setGender(gender);

        LanguageCode language = LanguageCode.valueOf(clientCreationDTO.getLanguage());
        person.setLanguage(language);

        ResidentialStatusCode residentialStatus = ResidentialStatusCode.valueOf(clientCreationDTO.getResidentialStatus());
        person.setResidentialStatus(residentialStatus);

        LocalDate birthDate = LocalDate.parse(clientCreationDTO.getBirthDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/);
        logger.info("get birth date" + clientCreationDTO.getBirthDate());
        person.setBirthDate(birthDate.atTime(OffsetTime.now()));

        person.setProfession(clientCreationDTO.getProfession());

        List<Country> countries = new ArrayList<>();
        person.setNationality(countries);

        person.setBusinessFunctionTitle(clientCreationDTO.getBusinessFunctionTitle());
        final String employingPartyId = clientCreationDTO.getEmployingParty();

        if(!StringUtils.isEmpty(employingPartyId)) {
            final Optional<Organisation> organisationOptional = organisationRepository.findById(employingPartyId);
            organisationOptional.ifPresent(organisation -> {
                final List<EmployingPartyRole> employingPartyRoles = employingPartyRoleRepository.findAllByPlayer_Id(employingPartyId);
                if(employingPartyRoles.size() == 0) {
                    final List<Person> employees = new ArrayList<>();
                    employees.add(person);
                    EmployingPartyRole employingPartyRole = new EmployingPartyRole();
                    employingPartyRole.setEmployee(employees);
                    employingPartyRole.setId(GenerateKey.generateEntityId());
                    employingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
                    employingPartyRole.setDateCreated(OffsetDateTime.now());
                    employingPartyRole.setLastUpdated(OffsetDateTime.now());
                    employingPartyRole.setPlayer(Arrays.asList(organisation));
                    employingPartyRole.setHasCustomParty(false);
                    person.setEmployingParty(employingPartyRole);
                } else {
                    final EmployingPartyRole employingPartyRole = employingPartyRoles.get(0);
                    employingPartyRole.getEmployee().add(person);
                    person.setEmployingParty(employingPartyRole);
                }
            });
        }

        ContactPersonRole contactPersonRole = new ContactPersonRole();
        person.setContactPersonRole(contactPersonRole);
        contactPersonRole.setId(GenerateKey.generateEntityId());


        CivilStatusCode civilStatus = CivilStatusCode.valueOf(clientCreationDTO.getCivilStatus());
        person.setCivilStatus(civilStatus);

        /*final ISODateTime deathDate = new ISODateTime();
        LocalDate localDate = LocalDate.parse(clientCreationDTO.getDeathDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        deathDate.setValue(java.sql.Date.valueOf(localDate));
        person.setDeathDate(new ISODateTime());*/

        LocalDate citizenshipEndDate = LocalDate.parse(clientCreationDTO.getCitizenshipEndDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/);
        person.setCitizenshipEndDate(citizenshipEndDate.atTime(OffsetTime.now()));

        LocalDate citizenshipStartDate = LocalDate.parse(clientCreationDTO.getCitizenshipStartDate()/*, DateTimeFormatter.ofPattern("MM/dd/yyyy")*/);
        person.setCitizenshipStartDate(citizenshipStartDate.atTime(OffsetTime.now()));

        PostalAddress postalAddress = new PostalAddress();
        postalAddress.setId(GenerateKey.generateEntityId());


        /*AddressTypeCode addressType = AddressTypeCode.valueOf(clientCreationDTO.getAddressType());
        postalAddress.setAddressType(addressType);*/
        postalAddress.setStreetName(clientCreationDTO.getStreetName());
        postalAddress.setStreetBuildingIdentification(clientCreationDTO.getStreetBuildingIdentification());
        postalAddress.setPostCodeIdentification(clientCreationDTO.getPostCodeIdentification());
        postalAddress.setTownName(clientCreationDTO.getTownName());
        postalAddress.setState(clientCreationDTO.getState());
        postalAddress.setBuildingName(clientCreationDTO.getBuildingName());
        postalAddress.setFloor(clientCreationDTO.getFloor());
        postalAddress.setDistrictName(clientCreationDTO.getDistrictName());
        postalAddress.setRegionIdentification(clientCreationDTO.getRegionIdentification());
        //postalAddress.setCountyIdentification(clientCreationDTO.getCountryIdentification());

        countryService.findCountryById(clientCreationDTO.getCountryIdentification()).ifPresent(country -> {
            postalAddress.setCountyIdentification(clientCreationDTO.getCountry());
        });

        postalAddress.setPostOfficeBox(clientCreationDTO.getPostOfficeBox());
        postalAddress.setProvince(clientCreationDTO.getProvince());
        postalAddress.setDepartment(clientCreationDTO.getDepartment());
        postalAddress.setSubDepartment(clientCreationDTO.getSubDepartment());
        postalAddress.setSuiteIdentification(clientCreationDTO.getSuiteIdentification());
        postalAddress.setBuildingIdentification(clientCreationDTO.getBuildingIdentification());
        postalAddress.setMailDeliverySubLocation(clientCreationDTO.getMailDeliverySubLocation());
        postalAddress.setBlock(clientCreationDTO.getBlock());
        postalAddress.setLot(clientCreationDTO.getLot());
        postalAddress.setDistrictSubDivisionIdentification(clientCreationDTO.getDistrictSubDivisionIdentification());

        PersonProfile personProfile = new PersonProfile();
        personProfile.setId(GenerateKey.generateEntityId());
        personProfile.setSourceOfWealth(clientCreationDTO.getSourceOfWealth());
        personProfile.setSalaryRange(clientCreationDTO.getSalaryRange());
        personProfile.setEmployeeTerminationIndicator(Boolean.valueOf(clientCreationDTO.getEmployeeTerminationIndicator()));

        PersonIdentification personIdentification = new PersonIdentification();
        personIdentification.setId(GenerateKey.generateEntityId());
        personIdentification.setSocialSecurityNumber(clientCreationDTO.getSocialSecurityNumber());
        personIdentification.setDriversLicenseNumber(clientCreationDTO.getDriversLicenseNumber());
        personIdentification.setAlienRegistrationNumber(clientCreationDTO.getAlienRegistrationNumber());
        personIdentification.setPassportNumber(clientCreationDTO.getPassportNumber());
        personIdentification.setIdentityCardNumber(clientCreationDTO.getIdentityCardNumber());
        personIdentification.setEmployerIdentificationNumber(clientCreationDTO.getEmployerIdentificationNumber());
        personIdentification.setNationalRegistrationNumber(clientCreationDTO.getNationalRegistrationNumber());
        personIdentification.setTaxIdentificationNumber(clientCreationDTO.getTaxIdentificationNumber());

        /*PartyTypeCode partyType = PartyTypeCode.valueOf(clientCreationDTO.getPartyType());
        personIdentification.setPartyType(partyType);*/

        try {
            TypeOfIdentificationCode typeOfIdentification = TypeOfIdentificationCode.valueOf(clientCreationDTO.getTypeOfIdentification());
            personIdentification.setTypeOfIdentification(typeOfIdentification);
        }catch (Exception e){
            personIdentification.setTypeOfIdentification(TypeOfIdentificationCode.None);
        }

        /*TypeOfIdentificationCode typeOfIdentification = TypeOfIdentificationCode.valueOf(clientCreationDTO.getTypeOfIdentification());
        personIdentification.setTypeOfIdentification(typeOfIdentification);*/



        PersonName personName = new PersonName();
        personName.setId(GenerateKey.generateEntityId());
        personName.setBirthName(clientCreationDTO.getBirthName());

        NamePrefix1Code namePrefix = NamePrefix1Code.valueOf(clientCreationDTO.getNamePrefix());
        personName.setNamePrefix(namePrefix);
        personName.setGivenName(clientCreationDTO.getGivenName());
        personName.setMiddleName(clientCreationDTO.getMiddleName());
        personName.setNameSuffix(clientCreationDTO.getNameSuffix());
        personName.setIdentification(personIdentification);

        personIdentification.setPersonName(Arrays.asList(personName));
        person.setPersonIdentification(Arrays.asList(personIdentification));
        person.setPersonProfile(personProfile);

        Location placeOfBirth = new Location();
        postalAddress.setLocation(placeOfBirth);

        placeOfBirth.setAddress(Arrays.asList(postalAddress));
        person.setPlaceOfBirth(placeOfBirth);
        placeOfBirth.setId(GenerateKey.generateEntityId());

        personProfile.setPerson(person);
        personIdentification.setPerson(person);

        person.setPersonProfile(personProfile);

        personName.setPartyIdentification(personIdentification);
        personIdentification.setPersonName(Arrays.asList(personName));

        List<ContactPoint> contactPoints = readContactPointsJsonWithObjectMapper(clientCreationDTO.getPhonesJson(), clientCreationDTO.getAddressesJson());
        contactPoints.forEach(contactPoint -> {
            log.info("Contact point is: {}", contactPoint);
            contactPoint.setRelatedParty(person);
        });
        person.setContactPoint(contactPoints);

        List<Location> residences = new ArrayList<>();
        Location residence = new Location();
        residence.setId(GenerateKey.generateEntityId());
        residence.setDateCreated(OffsetDateTime.now());
        residence.setLastUpdated(OffsetDateTime.now());
        residence.setEntityStatus(EntityStatus.DRAFT);
        residence.setEntityVersion(0L);
        residence.setNativePerson(person);
        PostalAddress residentialPostalAddress = readPostalAddressForResidence(clientCreationDTO);
        residentialPostalAddress.setRelatedParty(person);
        residentialPostalAddress.setLocation(residence);
        residence.setAddress(Arrays.asList(residentialPostalAddress));
        residences.add(residence);
        person.setResidence(residences);

        person.setVirtualId(clientCreationDTO.getVirtualId());


        final Person savedPerson = personRepository.save(person);
        if(savedPerson != null) {
            createPersonLoginProfile(clientCreationDTO, savedPerson);
            /*log.info("Generate notification configs for user with party id {}", savedPerson.getId());
            notificationConfigService.findAllByPartyId(savedPerson.getId());*/
        }

        //auditLoggerConfiguration.auditLoggerUtil().logActivity(clientCreationDTO.getAuditUsername(), AuditEvents.CREATE_PERSON, savedPerson);

        return savedPerson;
    }

    private void createPersonLoginProfile(ClientCreationDTO clientCreationDTO, Person person) {
        ProfileDTO profileDTO = ProfileDTO.builder()
                .email(clientCreationDTO.getEmailAddress())
                .firstNames(clientCreationDTO.getBirthName() + " " + clientCreationDTO.getMiddleName())
                .lastName(clientCreationDTO.getGivenName())
                .mobileNumber(clientCreationDTO.getMobileNumber())
                .personId(person.getId())
                .profileLevel(RoleLevel.CLIENT.name())
                .userRoles(clientCreationDTO.getUserRoles())
                .build();

        log.info("Profile is : {}", profileDTO);

        final Optional<Profile> createdProfileOptional = profileService.createProfile(profileDTO);
        createdProfileOptional.ifPresent(createdProfile -> {
            log.info("Created profile for: {}", createdProfile.getFirstNames() + " " + createdProfile.getLastName());
        });
    }

    public ClientCreationDTO generateClientCreationDTOFromPerson(Person person) {
        ClientCreationDTO clientCreationDTO = new ClientCreationDTO();

        clientCreationDTO.setGender(person.getGender().name());
        clientCreationDTO.setId(person.getId());
        clientCreationDTO.setLanguage(person.getLanguage().getName());


        return clientCreationDTO;
    }

    @Override
    public Optional<Person> getPersonById(String personId) {
        return personRepository.findById(personId).map(person -> {
            final List<PersonIdentification> personIdentification = person.getPersonIdentification();
            logger.info("The size of the identifications list is: {}", personIdentification.size());
            for(PersonIdentification personIdentification1: personIdentification) {
                final String partyIdentificationInformationId = personIdentification1.getId();
                final List<PersonName> personNamesByPartyIdentificationId = personNameRepository.findPersonNamesByPartyIdentificationId(partyIdentificationInformationId);
                logger.info("Fetched list of person names: {}", personNamesByPartyIdentificationId);
                personIdentification1.setPersonName(personNamesByPartyIdentificationId);
            }
            logger.info("Person Identificatoin Infos: {}", personIdentification);
            final Location placeOfBirth = person.getPlaceOfBirth();
            if(placeOfBirth != null) {
                final List<PostalAddress> postalAddresses = postalAddressRepository.findAllByLocationId(placeOfBirth.getId());
                postalAddresses.forEach(pa -> placeOfBirth.getAddress().add(pa));
                placeOfBirth.setAddress(postalAddresses);
                person.setPlaceOfBirth(placeOfBirth);
            }
            final List<Location> residences = person.getResidence();
            if(residences != null) {
                log.info("Size of residence: {}", residences.size());
                residences.forEach(location -> {
                    log.info("Residence Id: {}", location.getId());
                    final List<PostalAddress> allByLocationId = postalAddressRepository.findAllByLocationId(location.getId());
                    log.info("Postal Addresses for residence: {}", allByLocationId);
                    location.setAddress(allByLocationId);
                });
            }
            person.setPersonIdentification(personIdentification);
            sanitisePerson(person);
            return Optional.of(person);
        }).orElseGet(() -> {
            return Optional.empty();
        });
    }

    private void sanitisePerson(Person person) {
        if (person.getResidence() == null) {
            person.setResidence(new ArrayList());
        }
    }


        public List<ContactPoint> readContactPointsJsonWithObjectMapper(String phonesJson, String electronicAddressesJson) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        log.info("json data: {}", phonesJson);
        try {
            List<ContactPoint> contactPoints = new ArrayList<>();
            List<PhoneAddress> phoneAddresses = mapper.readValue(phonesJson, mapper.getTypeFactory()
                    .constructCollectionType(List.class, PhoneAddress.class));
            List<ElectronicAddress> electronicAddresses = mapper.readValue(electronicAddressesJson, mapper.getTypeFactory()
                    .constructCollectionType(List.class, ElectronicAddress.class));
            phoneAddresses.forEach(phoneAddress -> {
                phoneAddress.setId(GenerateKey.generateEntityId());
                phoneAddress.setDateCreated(OffsetDateTime.now());
                phoneAddress.setLastUpdated(OffsetDateTime.now());
                phoneAddress.setEntityStatus(EntityStatus.ACTIVE);
                phoneAddress.setEntityVersion(0L);
                contactPoints.add(phoneAddress);
            });
            electronicAddresses.forEach(electronicAddress -> {
                electronicAddress.setId(GenerateKey.generateEntityId());
                electronicAddress.setDateCreated(OffsetDateTime.now());
                electronicAddress.setLastUpdated(OffsetDateTime.now());
                electronicAddress.setEntityStatus(EntityStatus.ACTIVE);
                electronicAddress.setEntityVersion(0L);
                contactPoints.add(electronicAddress);
            });
            log.info(contactPoints + "");
            return contactPoints;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public PostalAddress readPostalAddressForResidence(ClientCreationDTO clientDTO) {
        final Optional<Country> countryOptional = countryService.findCountryById(clientDTO.getResidenceCountryId());

        final PostalAddress postalAddress = new PostalAddress();
        postalAddress.setId(GenerateKey.generateEntityId());
        postalAddress.setDateCreated(OffsetDateTime.now());
        postalAddress.setLastUpdated(OffsetDateTime.now());
        postalAddress.setEntityStatus(EntityStatus.ACTIVE);

        AddressTypeCode addressTypeCode = AddressTypeCode.Business;
        try {
            addressTypeCode = AddressTypeCode.valueOf(clientDTO.getResidenceAddressType());
        } catch (RuntimeException re) {

        }
        postalAddress.setAddressType(addressTypeCode);
        postalAddress.setStreetName(clientDTO.getResidenceStreetName());
        postalAddress.setStreetBuildingIdentification(clientDTO.getResidenceStreetBuildingIdentification());
        postalAddress.setPostCodeIdentification(clientDTO.getPostCodeIdentification());
        postalAddress.setTownName(clientDTO.getResidenceTownName());
        postalAddress.setState(clientDTO.getResidenceState());
        postalAddress.setBuildingName(clientDTO.getResidenceBuildingName());
        postalAddress.setFloor(clientDTO.getResidenceFloor());
        postalAddress.setDistrictName(clientDTO.getResidenceDistrictName());
        postalAddress.setRegionIdentification(clientDTO.getRegionIdentification());
        postalAddress.setPostOfficeBox(clientDTO.getResidencePostOfficeBox());
        postalAddress.setProvince(clientDTO.getResidenceProvince());
        postalAddress.setDepartment(clientDTO.getResidenceDepartment());
        postalAddress.setSubDepartment(clientDTO.getResidenceSubDepartment());
        postalAddress.setSuiteIdentification(clientDTO.getResidenceSuiteIdentification());
        postalAddress.setBuildingIdentification(clientDTO.getResidenceBuildingIdentification());
        postalAddress.setMailDeliverySubLocation(clientDTO.getResidenceMailDeliverySubLocation());
        postalAddress.setBlock(clientDTO.getResidenceBlock());
        postalAddress.setLot(clientDTO.getResidenceLot());

        countryOptional.ifPresent(country -> {
            postalAddress.setCountry(country);
            postalAddress.setCountyIdentification(country.getId());
        });

        postalAddress.setDistrictSubDivisionIdentification(clientDTO.getResidenceDistrictSubDivisionIdentification());
        return postalAddress;
    }

    @Override
    public List<Person> getPersons() {
        return personRepository.findAll();
    }

    @Override
    public List<Person> getPersonsByEmployerId(String employerId) {
        return employingPartyRoleRepository.findAllByPlayer_Id(employerId)
                .stream().flatMap(employingPartyRole -> employingPartyRole.getEmployee().stream()).collect(Collectors.toList());
    }

    @Override
    public List<EmployingPartyRole> getEmployingPartyRoleByEmployerId(String employerId) {
        List<EmployingPartyRole> employingPartyRoles = employingPartyRoleRepository.findAllByPlayer_Id(employerId);
        if(employingPartyRoles == null || employingPartyRoles.isEmpty()) {
            log.info("The employing party role is empty. Creating one ...");
            EmployingPartyRole employingPartyRole = new EmployingPartyRole();
            employingPartyRole.setId(GenerateKey.generateEntityId());
            employingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
            employingPartyRole.setDateCreated(OffsetDateTime.now());
            employingPartyRole.setLastUpdated(OffsetDateTime.now());

            final Optional<Party> optionalParty = partyRepository.findById(employerId);
            if(optionalParty.isPresent()) {
                employingPartyRole.setPlayer(Arrays.asList(optionalParty.get()));
                employingPartyRole.setHasCustomParty(false);

                final EmployingPartyRole employingPartyRoleSaved = employingPartyRoleRepository.save(employingPartyRole);
                log.info("Saved the role.");
                employingPartyRoles = new ArrayList<>();
                employingPartyRoles.add(employingPartyRoleSaved);
            }

        }
        log.info("The employing party roles for player id {} are {}", employerId, employingPartyRoles.size());
        return employingPartyRoles;
    }

    @Override
    public Person updatePerson(ClientCreationDTO clientUpdateDTO) {
        logger.info("Updating person in service with id: {}", clientUpdateDTO.getId());
        Person person = personRepository.getOne(clientUpdateDTO.getId());

        //Person person = generatePersonFromDTO(clientUpdateDTO);

        GenderCode gender = GenderCode.valueOf(clientUpdateDTO.getGender());
        person.setGender(gender);

        LanguageCode language = LanguageCode.valueOf(clientUpdateDTO.getLanguage());
        person.setLanguage(language);

        ResidentialStatusCode residentialStatus = ResidentialStatusCode.valueOf(clientUpdateDTO.getResidentialStatus());
        person.setResidentialStatus(residentialStatus);


        LocalDate birthDate = LocalDate.parse(clientUpdateDTO.getBirthDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        logger.info("get birth date" + clientUpdateDTO.getBirthDate());
        person.setBirthDate(birthDate.atTime(OffsetTime.now()));

        person.setProfession(clientUpdateDTO.getProfession());

        List<Country> countries = new ArrayList<>();
        person.setNationality(countries);

        person.setBusinessFunctionTitle(clientUpdateDTO.getBusinessFunctionTitle());

        EmployingPartyRole employingParty = person.getEmployingParty();
        person.setEmployingParty(employingParty);

        ContactPersonRole contactPersonRole = person.getContactPersonRole();
        person.setContactPersonRole(contactPersonRole);

        CivilStatusCode civilStatus = CivilStatusCode.valueOf(clientUpdateDTO.getCivilStatus());
        person.setCivilStatus(civilStatus);
/*
        final ISODateTime deathDate = new ISODateTime();
        LocalDate localDate = LocalDate.parse(clientUpdateDTO.getDeathDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        deathDate.setValue(java.sql.Date.valueOf(localDate));
        person.setDeathDate(new ISODateTime());*/

        LocalDate citizenshipEndDate = LocalDate.parse(clientUpdateDTO.getCitizenshipEndDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        person.setCitizenshipEndDate(citizenshipEndDate.atTime(OffsetTime.now()));

        LocalDate citizenshipStartDate = LocalDate.parse(clientUpdateDTO.getCitizenshipStartDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        person.setCitizenshipStartDate(citizenshipStartDate.atTime(OffsetTime.now()));

//        PostalAddress postalAddress = person.getPlaceOfBirth().getAddress().get(0);
//
//       /* AddressTypeCode addressType = AddressTypeCode.valueOf(clientUpdateDTO.getAddressType());
//        postalAddress.setAddressType(addressType);*/
//        postalAddress.setStreetName(clientUpdateDTO.getStreetName());
//        postalAddress.setStreetBuildingIdentification(clientUpdateDTO.getStreetBuildingIdentification());
//        postalAddress.setPostCodeIdentification(clientUpdateDTO.getPostCodeIdentification());
//        postalAddress.setTownName(clientUpdateDTO.getTownName());
//        postalAddress.setState(clientUpdateDTO.getState());
//        postalAddress.setBuildingName(clientUpdateDTO.getBuildingName());
//        postalAddress.setFloor(clientUpdateDTO.getFloor());
//        postalAddress.setDistrictName(clientUpdateDTO.getDistrictName());
//        postalAddress.setRegionIdentification(clientUpdateDTO.getRegionIdentification());
//        postalAddress.setCountyIdentification(clientUpdateDTO.getCountryIdentification());
//        postalAddress.setPostOfficeBox(clientUpdateDTO.getPostOfficeBox());
//        postalAddress.setProvince(clientUpdateDTO.getProvince());
//        postalAddress.setDepartment(clientUpdateDTO.getDepartment());
//        postalAddress.setSubDepartment(clientUpdateDTO.getSubDepartment());
//        postalAddress.setSuiteIdentification(clientUpdateDTO.getSuiteIdentification());
//        postalAddress.setBuildingIdentification(clientUpdateDTO.getBuildingIdentification());
//        postalAddress.setMailDeliverySubLocation(clientUpdateDTO.getMailDeliverySubLocation());
//        postalAddress.setBlock(clientUpdateDTO.getBlock());
//        postalAddress.setLot(clientUpdateDTO.getLot());
//        postalAddress.setDistrictSubDivisionIdentification(clientUpdateDTO.getDistrictSubDivisionIdentification());
//
//        PersonProfile personProfile = person.getPersonProfile();
//        personProfile.setSourceOfWealth(clientUpdateDTO.getSourceOfWealth());
//        personProfile.setSalaryRange(clientUpdateDTO.getSalaryRange());
//        personProfile.setEmployeeTerminationIndicator(Boolean.valueOf(clientUpdateDTO.getEmployeeTerminationIndicator()));
//
//        PersonIdentification personIdentification = person.getPersonIdentification().get(0);
//        personIdentification.setSocialSecurityNumber(clientUpdateDTO.getSocialSecurityNumber());
//        personIdentification.setDriversLicenseNumber(clientUpdateDTO.getDriversLicenseNumber());
//        personIdentification.setAlienRegistrationNumber(clientUpdateDTO.getAlienRegistrationNumber());
//        personIdentification.setPassportNumber(clientUpdateDTO.getPassportNumber());
//        personIdentification.setIdentityCardNumber(clientUpdateDTO.getIdentityCardNumber());
//        personIdentification.setEmployerIdentificationNumber(clientUpdateDTO.getEmployerIdentificationNumber());
//
//        Optional<PersonName> personNameOptional = personIdentification.getPersonName().stream().findFirst();
//        personNameOptional.ifPresent(personName -> {
//            personName.setBirthName(clientUpdateDTO.getBirthName());
//
//            NamePrefix1Code namePrefix = NamePrefix1Code.valueOf(clientUpdateDTO.getNamePrefix());
//            personName.setNamePrefix(namePrefix);
//            personName.setGivenName(clientUpdateDTO.getGivenName());
//            personName.setMiddleName(clientUpdateDTO.getMiddleName());
//            personName.setNameSuffix(clientUpdateDTO.getNameSuffix());
//            personName.setPartyIdentification(personIdentification);
//
//            personIdentification.setPersonName(Arrays.asList(personName));
//        });
//
//        person.setPersonIdentification(Arrays.asList(personIdentification));
//        person.setPersonProfile(personProfile);
//
//        Location placeOfBirth = person.getPlaceOfBirth();
//        postalAddress.setLocation(placeOfBirth);
//
//        placeOfBirth.setAddress(Arrays.asList(postalAddress));
//        person.setPlaceOfBirth(placeOfBirth);
//
//        personProfile.setPerson(person);
//        personIdentification.setPerson(person);
//
//        person.setPersonProfile(personProfile);

        return personRepository.save(person);
    }

    @Override
    public PersonName updatePersonName(PersonName personName) {
        personName.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return personNameRepository.save(personName);
    }

    @Override
    public PersonIdentification updatePersonIdentification(PersonIdentification personIdentification) {
        personIdentification.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return personIdentificationRepository.save(personIdentification);
    }

    @Override
    public PersonProfile updatePersonProfile(PersonProfile personProfile) {
        personProfile.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return personProfileRepository.save(personProfile);
    }

    @Override
    public PostalAddress updatePlaceOfBirth(PostalAddress postalAddress) {
        postalAddress.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return postalAddressRepository.save(postalAddress);
    }

    @Override
    public Person updateGeneralInformation(Person person) {
        person.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return personRepository.save(person);
    }

    @Override
    public Optional<PersonIdentification> findPersonIdentificationByIdNumber(String idNumber) {
        return Optional.ofNullable(this.personIdentificationRepository.findPersonIdentificationByIdentityCardNumber(idNumber));
    }

    @Override
    public Optional<Person> getPersonByAccountId(String accountId) {
        final CashAccount account = accountService.findAccountById(accountId);
        log.info("Account is: {}", account);
        final List<Person> persons = new ArrayList<>();
        accountPartyRoleRepository.findAllByAccount_Id(account.getId()).stream()
                .filter(role -> role instanceof AccountOwnerRole).forEach(role -> {
            final List<Person> personList = personRepository.findAllByRole_Id(role.getId());
            log.info("Person: {}", personList);
            persons.addAll(personList);
        });
        if(persons.size() > 0) {
            final Person person = persons.get(0);
            return getPersonById(person.getId());
        } else {
            return Optional.empty();
        }
    }
    @Override
    public Person deletePerson(String id) {
        Optional<Person> personOptional = personRepository.findById(id);
        Person person = null;
        if (personOptional.isPresent()){
            person = personOptional.get();
            person.setEntityStatus(EntityStatus.DELETED);
            person = personRepository.save(person);
        }

        return person;
    }

    @Override
    public Person approvePerson(String id) {
        Optional<Person> personOptional = personRepository.findById(id);
        Person person = null;
        if(personOptional.isPresent()){
            person = personOptional.get();
            person.setEntityStatus(EntityStatus.ACTIVE);
            person = personRepository.save(person);
            profileService.getProfileByPersonId(id).ifPresent(profile -> {
                final String profileFullName = profile.getFirstNames() + " " + profile.getLastName();
                log.info("Found profile for {}", profileFullName);
                final Optional<Profile> approvedProfileOptional = profileService.approveProfile(profile.getId());
                approvedProfileOptional.ifPresent(approvedProfile -> {
                    log.info("Approved profile for {}", profileFullName);
                });
            });


            log.info("Generate notification configs for user with party id {}", person.getId());
            notificationConfigService.findAllByPartyId(person.getId());
        }
        return person;
    }

    @Override
    public Person rejectPerson(String id) {
        Optional<Person> personOptional = personRepository.findById(id);
        Person person = null;
        if(personOptional.isPresent()){
            person = personOptional.get();
            person.setEntityStatus(EntityStatus.DISAPPROVED);
            person = personRepository.save(person);
        }
        return person;
    }

   @Override
    public Person findPersonById(String id) {
        Optional<Person> personOptional = this.personRepository.findById(id);
        return personOptional.orElse(null);
    }

    @Override
    public List<ContactPoint> getContactPointsByPersonId(String personId) {
        return contactPointRepository.findAllByRelatedPartyId(personId);
    }

    @Override
    public String refreshPersonIndex() {
        FullTextSession fullTextSession = Search.getFullTextSession(entityManager.unwrap(Session.class));
        fullTextSession.setFlushMode(FlushMode.MANUAL);
        fullTextSession.setCacheMode(CacheMode.IGNORE);
        int BATCH_SIZE = 25;
        ScrollableResults scrollableResults = fullTextSession.createCriteria(PersonName.class)
                .setFetchSize(BATCH_SIZE)
                .scroll(ScrollMode.FORWARD_ONLY);
        int index = 0;
        while(scrollableResults.next()) {
            index++;
            fullTextSession.index(scrollableResults.get(0)); //index each element
            if (index % BATCH_SIZE == 0) {
                fullTextSession.flushToIndexes(); //apply changes to indexes
                fullTextSession.clear(); //free memory since the queue is processed
            }
        }

        return "Person search indices reloaded successfully";
    }

    @Override
    @Transactional
    public boolean isIdentityCardNumberAlreadyInUse(String identityCardNumber) {

        boolean personInDb = true;
        if (clientDAO.getActivePerson(identityCardNumber) == null) personInDb = false;
        return personInDb;
    }

    @Override
    public EmployingPartyRole addPersonToEmployingPartyRole(EmployingPartyRolePersonRequestDTO employingPartyRolePersonRequestDTO){
        return employingPartyRoleRepository.findById(employingPartyRolePersonRequestDTO.getEmployingPartyRoleId()).map(employingPartyRole -> {
            employingPartyRole.getEmployee().forEach(person -> {
                log.info("Employed person: {}", person.getName());
            });
            personRepository.findById(employingPartyRolePersonRequestDTO.getPersonId()).ifPresent(person -> {
                log.info("Person: {} ID: {}, Requested ID is: {}", person.getName(), person.getId(), employingPartyRolePersonRequestDTO.getPersonId());
                employingPartyRole.getEmployee().add(person);
                person.setEmployingParty(employingPartyRole);

                employingPartyRoleRepository.save(employingPartyRole);
//                personRepository.save(person);
            });
            return employingPartyRole;
        }).orElse(null);
    }

    @Override
    public EmployingPartyRole removePersonFromEmployingPartyRole(EmployingPartyRolePersonRequestDTO employingPartyRolePersonRequestDTO) {
        return employingPartyRoleRepository.findById(employingPartyRolePersonRequestDTO.getEmployingPartyRoleId()).map(employingPartyRole -> {
            employingPartyRole.getEmployee().forEach(person -> {
                log.info("Employed person: {}", person.getName());
            });
            personRepository.findById(employingPartyRolePersonRequestDTO.getPersonId()).ifPresent(person -> {
                log.info("Person: {} ID: {}, Requested ID is: {}", person.getName(), person.getId(), employingPartyRolePersonRequestDTO.getPersonId());
                person.setEmployingParty(null);
                personRepository.save(person);
            });
            return employingPartyRole;
        }).orElse(null);
    }


}
