package zw.co.esolutions.zpas.services.impl.payments.zimra.response;

import lombok.Builder;
import zw.co.esolutions.zpas.services.impl.payments.zimra.request.AssessmentValidationGetRequest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alfred on 21 August 2019
 */
@Builder
@XmlRootElement(name = "ValidateAssessmentResponse")
public class AssessmentValidationResponse {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "GetResponse")
    private AssessmentValidationGetResponse assessmentValidationGetResponse;

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public AssessmentValidationGetResponse getAssessmentValidationGetResponse() {
        return assessmentValidationGetResponse;
    }

    public void setAssessmentValidationGetResponse(AssessmentValidationGetResponse assessmentValidationGetResponse) {
        this.assessmentValidationGetResponse = assessmentValidationGetResponse;
    }

    public String getAssessmentNumber() {
        return assessmentValidationGetResponse.getAssNo();
    }

    public String getOfficeCode() {
        return assessmentValidationGetResponse.getOffice();
    }

    public String getShortSummary() {
        return String.format("%s (Year: %s, Office: %s, Amount: %s)", assessmentValidationGetResponse.getAssNo(),
                assessmentValidationGetResponse.getYear(),
                assessmentValidationGetResponse.getOffice(), assessmentValidationGetResponse.getFormattedAmount());
    }
}
