package zw.co.esolutions.zpas.services.nssa.impl;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import zw.co.esolutions.zpas.dto.documents.DocumentDTO;
import zw.co.esolutions.zpas.dto.nssa.EmployeeDTO;
import zw.co.esolutions.zpas.dto.nssa.EmployerDTO;
import zw.co.esolutions.zpas.dto.nssa.UploadEmployeesDTO;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.repository.OrganisationRepository;
import zw.co.esolutions.zpas.services.iface.documents.DocumentsService;
import zw.co.esolutions.zpas.services.iface.registration.NonFinancialInstitutionService;
import zw.co.esolutions.zpas.services.impl.payments.converters.BeanUtils;
import zw.co.esolutions.zpas.services.impl.payments.converters.SFIFileContents;
import zw.co.esolutions.zpas.services.impl.payments.converters.SFIUtils;
import zw.co.esolutions.zpas.services.nssa.iface.NssaClientsService;
import zw.co.esolutions.zpas.services.nssa.impl.data.EmployeeEntry;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.storage.StorageService;
import zw.co.esolutions.zpas.utilities.util.nssa.NssaReferenceRegexUtil;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static zw.co.esolutions.zpas.utilities.util.PaymentsUtil.*;

@Slf4j
@Service
@Transactional
public class NssaClientsServiceImpl implements NssaClientsService {
    private static final String OUTPUT_FOLDER = StorageProperties.BASE_LOCATION;

    @Value("${NSSA.CONTRIBUTIONS.SYSTEM.BASE.URL}")
    private String nssaContributionsSystemBaseUrl;

    @Autowired
    StorageService storageService;

    @Autowired
    DocumentsService documentsService;

    @Autowired
    NssaClientsService nssaClientsService;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    NonFinancialInstitutionService nonFinancialInstitutionService;

    @Autowired
    NssaReferenceRegexUtil nssaReferenceRegexUtil;

    @Override
    public Optional<EmployeeDTO> createEmployee(EmployeeDTO employeeDTO) {
        final Optional<Organisation> organisationOptional = organisationRepository.findById(employeeDTO.getClientId());

        if (!organisationOptional.isPresent()) {
            //throw new Exception("Organisation not found. Make sure you are operating as an employee of the organisation.");
        }

        final Organisation organisation = organisationOptional.get();

        EmployerDTO employerDTO = EmployerDTO.builder()
                .ssr(organisation.getSsr())
                .physicalAddress(organisation.getPhysicalAddress())
                .name(organisation.getName())
                .emailAddress(organisation.getEmailAddress())
                .bic(organisation.getBIC())
                .anyBIC(organisation.getAnyBIC())
                .businessPartnerNumber(organisation.getNssaBusinessPartnerNumber())
                .build();

        employeeDTO.setDateOfBirth(LocalDate.parse(employeeDTO.getDateOfBirthString(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd")).atTime(OffsetTime.now()));
        employeeDTO.setEmploymentStartDate(LocalDate.parse(employeeDTO.getEmploymentStartDateString(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd")).atTime(OffsetTime.now()));
        employeeDTO.setEntityStatus(EntityStatus.PENDING_APPROVAL);

        employerDTO.setEmployees(new ArrayList<>(Arrays.asList(employeeDTO)));

        log.info("Employer DTO is: {}", employerDTO);

        final EmployerDTO employerDTOSaved = saveEmployees(employerDTO);

        log.info("Returned employer DTO is: {}", employerDTOSaved);
        final List<EmployeeDTO> employees = employerDTOSaved.getEmployees();
        return Optional.of(employees.get(employees.size()-1));

        /*return Optional.ofNullable(employerDTOSaved).map(savedEmployer -> {
            log.info("Saved employer found. It is: {}", savedEmployer.getName());
            return Optional.ofNullable(savedEmployer.getEmployees()).map(employees -> {
                log.info("Employees found: {}", employees.size());

                log.info("Validating ssn....");
                List<EmployeeDTO> invalidEmployees = getInvalidNssaReference(savedEmployer.getEmployees());
                if (!invalidEmployees.isEmpty()){
                    log.info("there is an invalid ssn: {}", employeeDTO.getSsn());
                }

                return employees.stream().findFirst();
                //return Optional.of(employees.get(employees.size()-1));
            }).orElse(Optional.empty());
        }).orElse(Optional.empty());*/

    }

    @Override
    public Optional<EmployeeDTO> getEmployeeById(String employeeId) {

        RestTemplate restTemplate = new RestTemplate();
        String getEmployeeUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/details/" + employeeId;

        log.info("About to reach nssa at url: {}", getEmployeeUrl);

        EmployeeDTO employeeDTO = restTemplate.getForObject(getEmployeeUrl, EmployeeDTO.class);

        return Optional.ofNullable(employeeDTO);
    }

    @Override
    public List<EmployeeDTO> getEmployees() {
        String getEmployeesUrl = nssaContributionsSystemBaseUrl + "/employees";

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<EmployeeDTO> response = restTemplate.exchange(
                getEmployeesUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<EmployeeDTO>() {
                });
        EmployeeDTO employee = response.getBody();
        return Arrays.asList(employee);
    }

    @Override
    public List<EmployeeDTO> getEmployeesByEmployerId(String employerId) {
        return organisationRepository.findById(employerId).map(organisation -> {
            String getEmployeesByEmployerUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/employees/by-employer-ssr/"
                    + Optional.ofNullable(organisation.getSsr()).orElse("invalid-ssr");

            log.info("Trying to reach getEmployeesByEmployer through: => {}", getEmployeesByEmployerUrl);

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<List<EmployeeDTO>> response = restTemplate.exchange(
                    getEmployeesByEmployerUrl,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<EmployeeDTO>>() {
                    });
            List<EmployeeDTO> employees = response.getBody();
            return employees;
        }).orElse(new ArrayList<>());
    }

    @Override
    public Optional<EmployeeDTO> updateEmployee(EmployeeDTO employeeDTO) {
        final Optional<Organisation> organisationOptional = organisationRepository.findById(employeeDTO.getClientId());

        if (!organisationOptional.isPresent()) {
        }

        final Organisation organisation = organisationOptional.get();

        EmployerDTO employerDTO = EmployerDTO.builder()
                .ssr(organisation.getSsr())
                .physicalAddress(organisation.getPhysicalAddress())
                .name(organisation.getName())
                .emailAddress(organisation.getEmailAddress())
                .bic(organisation.getBIC())
                .anyBIC(organisation.getAnyBIC())
                .businessPartnerNumber(organisation.getNssaBusinessPartnerNumber())
                .build();

        employeeDTO.setDateOfBirth(LocalDate.parse(employeeDTO.getDateOfBirthString(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd")).atTime(OffsetTime.now()));
        employeeDTO.setEmploymentStartDate(LocalDate.parse(employeeDTO.getEmploymentStartDateString(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd")).atTime(OffsetTime.now()));

        final Optional<EmployerDTO> employerBySsr = getEmployerBySsr(organisation.getSsr());
        employeeDTO.setEmployer(employerBySsr.get());


        RestTemplate restTemplate = new RestTemplate();
        String updateEmployeesUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/employees/edit";
        log.info("trying to reach update employee url {}", updateEmployeesUrl);

        ResponseEntity<EmployeeDTO> response = restTemplate.exchange(
                updateEmployeesUrl,
                HttpMethod.POST,
                new HttpEntity<>(employeeDTO),
                new ParameterizedTypeReference<EmployeeDTO>() {
                });
        EmployeeDTO employee = response.getBody();
        return Optional.ofNullable(employee);
    }

    @Override
    public Optional<EmployerDTO> getEmployerBySsr(String employerSsr) {
        String getEmployerBySsrUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/find-employer/" + employerSsr;

        log.info("Trying to reach getEmployerByIdUrl through: => {}", getEmployerBySsrUrl);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<EmployerDTO> response = restTemplate.exchange(
                getEmployerBySsrUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<EmployerDTO>() {
                });
        EmployerDTO employer = response.getBody();
        return Optional.ofNullable(employer);

    }

    @Override
    public Optional<EmployeeDTO> deleteEmployee(String employeeId) {
        RestTemplate restTemplate = new RestTemplate();
        String getEmployeeUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/delete/" + employeeId;

        EmployeeDTO employeeDTO = restTemplate.getForObject(getEmployeeUrl, EmployeeDTO.class);

        return Optional.ofNullable(employeeDTO);
    }

    @Override
    public Optional<EmployeeDTO> approveEmployee(String employeeId) {
        RestTemplate restTemplate = new RestTemplate();
        String getEmployeeUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/approve/" + employeeId;

        EmployeeDTO employeeDTO = restTemplate.getForObject(getEmployeeUrl, EmployeeDTO.class);

        return Optional.ofNullable(employeeDTO);
    }

    @Override
    public Optional<EmployeeDTO> rejectEmployee(String employeeId) {
        RestTemplate restTemplate = new RestTemplate();
        String getEmployeeUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/reject/" + employeeId;

        EmployeeDTO employeeDTO = restTemplate.getForObject(getEmployeeUrl, EmployeeDTO.class);

        return Optional.ofNullable(employeeDTO);
    }

    @Override
    public List<EmployeeDTO> createEmployees(UploadEmployeesDTO uploadEmployeesDTO) throws Exception {
        log.info("Employees Upload DTO is: {}", uploadEmployeesDTO);
        final String fileName = uploadEmployeesDTO.getFileName();
        if (StringUtils.isEmpty(fileName)) {
            log.error("Invalid File ID specified. Failed to load uploaded bulk payment file. Upload and try again.");
            throw new Exception("Invalid file");
        }

        final String fileExtension = getFileExtension(fileName);
        log.info("Extension for the uploaded file is: {}", fileExtension);

        switch (fileExtension) {
            case ".csv": {
                final EmployerDTO employerDTO = readEmployeesFromCSVFile(uploadEmployeesDTO);
                saveDocument(uploadEmployeesDTO);
                return saveEmployees(employerDTO).getEmployees();
            }
            case ".sfi": {
                final EmployerDTO employerDTO = readEmployeeFromSFIFile(uploadEmployeesDTO);
                saveDocument(uploadEmployeesDTO);
                return saveEmployees(employerDTO).getEmployees();
            }
            case ".zip": {
                final Path resolve = Paths.get(OUTPUT_FOLDER).resolve(fileName);
                final String unzippedFile = unZipIt(resolve.toUri().getPath(), OUTPUT_FOLDER);
                createEmployees(uploadEmployeesDTO.toBuilder().fileName(unzippedFile).build());
            }
            case ".xls":
            case ".xlsx": {
                final EmployerDTO employerDTO = readEmployeesFromExcelFile(uploadEmployeesDTO);
                saveDocument(uploadEmployeesDTO);
                return saveEmployees(employerDTO).getEmployees();
            }
            default:
                throw new Exception("Extension " + fileExtension + " not supported yet");
        }
    }

    @Override
    public Boolean isSSNAlreadyInUse(String ssn) {
        String validateEmployeeBySSNUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/validate/" + ssn;

        log.info("Trying to reach validateEmployeeBySSN through: => {}", validateEmployeeBySSNUrl);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Boolean> response = restTemplate.exchange(
                validateEmployeeBySSNUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Boolean>() {
                });
        Boolean employee = response.getBody();
        return employee;
    }

    @Override
    public Optional<EmployeeDTO> getEmployeeBySSN(String ssn) {
            String getEmployeeBySSNUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/employee/by-ssn/" + ssn;

            log.info("Trying to reach getEmployeeBySSN through: => {}", getEmployeeBySSNUrl);

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<EmployeeDTO> response = restTemplate.exchange(
                    getEmployeeBySSNUrl,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<EmployeeDTO>() {
                    });
            EmployeeDTO employee = response.getBody();
            return Optional.ofNullable(employee);

    }

    private void saveDocument(UploadEmployeesDTO uploadEmployeesDTO) {
        log.info("Saving document for the PaymentUploadDTO: {}", uploadEmployeesDTO);
        final String fileName = uploadEmployeesDTO.getFileName();

        documentsService.createDocument(DocumentDTO.builder()
                .clientId(uploadEmployeesDTO.getClientId())
                .copyDuplicate("")
                .dataSetType("")
                .documentName(fileName)
                .documentPath(Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName).toString())
                .issueDate(LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                .type("NSSA EmployeeDTO")
                .build());
    }

    private EmployerDTO saveEmployees(EmployerDTO employerDTO) {

        RestTemplate restTemplate = new RestTemplate();
        String saveEmployeesUrl = nssaContributionsSystemBaseUrl + "/nssa-clients/upload/employees";

        ResponseEntity<EmployerDTO> response = restTemplate.exchange(
                saveEmployeesUrl,
                HttpMethod.POST,
                new HttpEntity<>(employerDTO),
                new ParameterizedTypeReference<EmployerDTO>() {
                });
        EmployerDTO employer = response.getBody();
        log.info("Returned employer has employees: {}", employer.getEmployees());
        return employer;
    }

    private EmployerDTO readEmployeesFromCSVFile(UploadEmployeesDTO uploadEmployeesDTO) throws Exception {
        log.info("Processing CSV contribution schedule file");

        final Optional<Organisation> organisationOptional = organisationRepository.findById(uploadEmployeesDTO.getClientId());

        if (!organisationOptional.isPresent()) {
            throw new Exception("Organisation not found. Make sure you are operating as an employee of the organisation.");
        }

        final Organisation organisation = organisationOptional.get();

        log.info("Reading uploaded schedule items file...");
        Resource file = storageService.loadAsResource(uploadEmployeesDTO.getFileName());
        List<EmployeeDTO> employees = new ArrayList<>();
        List<EmployeeEntry> employeeEntries = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader(Paths.get(file.getURI()))) {
            CsvToBean<EmployeeEntry> csvEmployeeEntryCsvToBean = new CsvToBeanBuilder(reader)
                    .withType(EmployeeEntry.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<EmployeeEntry> csvEmployeeEntryIterator = csvEmployeeEntryCsvToBean.iterator();
            while (csvEmployeeEntryIterator.hasNext()) {
                employeeEntries.add(csvEmployeeEntryIterator.next());
            }
        } catch (IOException | RuntimeException re) {
            throw new Exception("Error Reading CSV File: " + re.getMessage());
        }
        for (EmployeeEntry employeeEntry : employeeEntries) {
            EmployeeDTO employee = employeeEntry.getEmployee();
            employees.add(employee);
        }
        EmployerDTO employerDTO = EmployerDTO.builder()
                .ssr(organisation.getSsr())
                .physicalAddress(organisation.getPhysicalAddress())
                .name(organisation.getName())
                .emailAddress(organisation.getEmailAddress())
                .bic(organisation.getBIC())
                .anyBIC(organisation.getAnyBIC())
                .businessPartnerNumber(organisation.getNssaBusinessPartnerNumber())
                .employees(employees)
                .build();
        return employerDTO;
    }

    private EmployerDTO readEmployeeFromSFIFile(UploadEmployeesDTO uploadEmployeesDTO) throws Exception {

        final EmployerDTO employerDTO = readSFIFileContents(SFIUtils.FILES_ROOT + uploadEmployeesDTO.getFileName(), uploadEmployeesDTO);

        return employerDTO;
    }

    private EmployerDTO readSFIFileContents(String fileName, UploadEmployeesDTO uploadEmployeesDTO) throws Exception {
        log.info("Processing CSV contribution schedule file");

        final Optional<Organisation> organisationOptional = organisationRepository.findById(uploadEmployeesDTO.getClientId());

        if (!organisationOptional.isPresent()) {
            throw new Exception("Organisation not found. Make sure you are operating as an employee of the organisation.");
        }

        final Organisation organisation = organisationOptional.get();

        final SFIFileContents sfiFileContents;
        try {
            sfiFileContents = BeanUtils.parseSFIFile(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Failed to read SFI File. Error is: " + e.getMessage());
        }

        List<EmployeeDTO> employees = new ArrayList<>();

        sfiFileContents.getDetailRecords().forEach(sfiDetailRecord -> {
            EmployeeDTO employee = EmployeeDTO.builder()
                    .id(UUID.randomUUID().toString())
                    .build();
            employees.add(employee);
        });

        EmployerDTO employerDTO = EmployerDTO.builder()
                .ssr(organisation.getSsr())
                .physicalAddress(organisation.getPhysicalAddress())
                .name(organisation.getName())
                .emailAddress(organisation.getEmailAddress())
                .bic(organisation.getBIC())
                .anyBIC(organisation.getAnyBIC())
                .businessPartnerNumber(organisation.getNssaBusinessPartnerNumber())
                .employees(employees)
                .build();

        return employerDTO;
    }

    private EmployerDTO readEmployeesFromExcelFile(UploadEmployeesDTO uploadEmployeesDTO) throws Exception {
        log.info("Processing CSV contribution schedule file");

        final Optional<Organisation> organisationOptional = organisationRepository.findById(uploadEmployeesDTO.getClientId());

        if (!organisationOptional.isPresent()) {
            throw new Exception("Organisation not found. Make sure you are operating as an employee of the organisation.");
        }

        final Organisation organisation = organisationOptional.get();


        final Map<String, EmployeeDTO> nationalIdEmployeeMap = nssaClientsService.getEmployeesByEmployerId(uploadEmployeesDTO.getClientId())
                .stream().collect(Collectors.toMap(EmployeeDTO::getNationalId, Function.identity()));

        log.info("Reading uploaded schedule items file...");

        List<EmployeeDTO> employees = new ArrayList<>();
        List<EmployeeEntry> employeeEntries = new ArrayList<>();
        final Map<Integer, List<String>> contributionItemsData = readExcelFile(uploadEmployeesDTO.getFileName());
        try {

            for (List<String> dataList : contributionItemsData.values()) {
                final EmployeeEntry employeeEntry = EmployeeEntry.builder()
                        .firstName(dataList.get(0))
                        .lastName(dataList.get(0))
                        .nationalId(dataList.get(1))
                        .ssn(dataList.get(3))
                        .build();
                employeeEntries.add(employeeEntry);

            }
        } catch (RuntimeException re) {
            log.error(re.getMessage());
            return null;
        }

        try {
            for (EmployeeEntry employeeEntry : employeeEntries) {

                List<EmployeeDTO> employeeList = new ArrayList<>();

                EmployeeDTO employee = nationalIdEmployeeMap.get(employeeEntry.getNationalId());
                if (employee == null) {
                    throw new Exception("EmployeeDTO " + employeeEntry.getFirstName() + employeeEntry.getLastName() + " not found.");
                }
                employeeList.add(employee);
            }
        } catch (RuntimeException re) {
            log.error(re.getMessage());
            throw new Exception("An error occurred. " + re.getMessage());
        }

        EmployerDTO employerDTO = EmployerDTO.builder()
                .ssr(organisation.getSsr())
                .physicalAddress(organisation.getPhysicalAddress())
                .name(organisation.getName())
                .emailAddress(organisation.getEmailAddress())
                .bic(organisation.getBIC())
                .anyBIC(organisation.getAnyBIC())
                .businessPartnerNumber(organisation.getNssaBusinessPartnerNumber())
                .employees(employees)
                .build();

        return employerDTO;
    }

    @Override
    public boolean validateSsnFormat(String ssn){
        return nssaReferenceRegexUtil.validateNssaReference(ssn);
    }


}
