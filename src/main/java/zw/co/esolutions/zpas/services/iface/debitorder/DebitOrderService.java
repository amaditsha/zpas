package zw.co.esolutions.zpas.services.iface.debitorder;

import zw.co.esolutions.zpas.dto.debitorder.DebitOrderReportDTO;
import zw.co.esolutions.zpas.model.DebitOrderBatch;

import java.util.List;
import java.util.Optional;

public interface DebitOrderService {
    Optional<DebitOrderBatch> getDebitOrderBatchById(String id);
    List<DebitOrderBatch> getDebitOrderBatchByClientId(String clientId);
    List<DebitOrderBatch> getDebitOrderBatchBySenderId(String bic);
    Optional<DebitOrderBatch> submitDebitOrderBatch(String debitOrderBatchId);
    Optional<DebitOrderBatch> approveDebitOrderBatch(String debitOrderBatchId);
    Optional<DebitOrderBatch> initiateDebitOrderBatch(String debitOrderBatchId);
    void updateDebitOrderStatuses(String debitOrderBatchId);
    Optional<DebitOrderBatch> rejectDebitOrderBatch(String debitOrderBatchId);
    Optional<DebitOrderBatch> deleteDebitOrderBatch(String debitOrderBatchId);
    void processDebitOrderBatchResponse();

    List<DebitOrderBatch> searchByDate(DebitOrderReportDTO debitOrderReportDTO);
    List<DebitOrderBatch> searchDebitOrderBatches(String searchTerm);
    String refreshDebitOrderBatchIndex();
    List<DebitOrderBatch> findTop50ByReceiverID(String receiverID);
}
