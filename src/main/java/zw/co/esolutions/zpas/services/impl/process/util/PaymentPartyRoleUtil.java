package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.pain001_001_08.*;
import zw.co.esolutions.zpas.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@Slf4j
public class PaymentPartyRoleUtil {

    public PartyIdentification43 getCreditorFromPaymentDetails(PaymentDetails paymentDetails) {
        PartyIdentification43 partyIdentification43 = new PartyIdentification43();
        partyIdentification43.setNm(paymentDetails.getCreditorName());
        partyIdentification43.setId(getPartyIdentificationChoiceFromPaymentDetails(paymentDetails));
        return partyIdentification43;
    }

    public PartyIdentification43 getPartyIdentification32(Payment payment, Class<?> paymentPartyRoleSubclass) {
        PartyIdentification43 partyIdentification43 = new PartyIdentification43();

        Optional<Party> partyOptional = this.getParty(payment, paymentPartyRoleSubclass);

        if (partyOptional.isPresent()) {
            Party party = partyOptional.get();
//            log.info("Party type is: {}", party.getClass().getSimpleName());
//            log.info("Party is present. Trying to get the name...Party has ID: => {}", party.getId());
            partyIdentification43.setNm(party.getName());
            partyIdentification43.setId(this.getPartyIdentificationChoice(party));
        } else {
//            log.info("Party is not present...getting a custom party...");
            this.getPaymentPartyRole(payment, paymentPartyRoleSubclass).map(paymentPartyRole -> {
                final CreditorRole creditorRole = (CreditorRole) paymentPartyRole;
                final CustomParty customParty = creditorRole.getCustomParty();
                if (creditorRole.isHasCustomParty() && customParty != null) {
                    partyIdentification43.setNm(customParty.getName());
                    final Party11Choice party11Choice = new Party11Choice();
                    final PersonIdentification5 personIdentification5 = new PersonIdentification5();
                    party11Choice.setPrvtId(personIdentification5);
                    partyIdentification43.setId(party11Choice);
                }
                return null;
            }).orElseGet(() -> {
//                log.info("Failed to get payment party role.");
                return null;
            });
        }
        return partyIdentification43;
    }

    public BranchAndFinancialInstitutionIdentification5 getCreditorAgentFromPaymentDetails(PaymentDetails paymentDetails) {

        BranchAndFinancialInstitutionIdentification5 branchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();
        FinancialInstitutionIdentification8 financialInstitutionIdentification8 = new FinancialInstitutionIdentification8();

        financialInstitutionIdentification8.setBICFI(paymentDetails.getCreditorAgentBIC());
        financialInstitutionIdentification8.setNm(paymentDetails.getCreditorAgentName());
        branchAndFinancialInstitutionIdentification5.setFinInstnId(financialInstitutionIdentification8);
        return branchAndFinancialInstitutionIdentification5;
    }

    public BranchAndFinancialInstitutionIdentification5 getBranchAndFinancialInstitutionIdentification5(Payment payment, Class<?> paymentPartyRoleSubclass) {

        BranchAndFinancialInstitutionIdentification5 branchAndFinancialInstitutionIdentification5 = new BranchAndFinancialInstitutionIdentification5();

        FinancialInstitutionIdentification8 financialInstitutionIdentification8 = new FinancialInstitutionIdentification8();

        Optional<Party> partyOptional = this.getParty(payment, paymentPartyRoleSubclass);

//        log.info("Party Optional present? {}", partyOptional.isPresent());

        if (partyOptional.isPresent()) {

            Organisation organisation = (Organisation) partyOptional.get();

            financialInstitutionIdentification8.setBICFI(organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getBICFI).findFirst().orElse(""));

            financialInstitutionIdentification8.setNm(organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getOrganisationName)
                    .flatMap(organisationNames -> organisationNames.stream())
                    .map(OrganisationName::getShortName)
                    .findFirst().orElse(""));

            branchAndFinancialInstitutionIdentification5.setFinInstnId(financialInstitutionIdentification8);
        }
        return branchAndFinancialInstitutionIdentification5;
    }

    public Optional<PaymentPartyRole> getPaymentPartyRole(Payment payment, Class<?> paymentPartyRoleSubclass) {
//        log.info("Payment Party roles: {}", payment.getPartyRole());
        final Optional<PaymentPartyRole> paymentPartyRoleOptional = payment.getPartyRole().stream()
                .filter(paymentPartyRole -> paymentPartyRoleSubclass.isInstance(paymentPartyRole))
                .findFirst();

        return paymentPartyRoleOptional;
    }

    public Optional<Party> getParty(Payment payment, Class<?> paymentPartyRoleSubclass) {

        final Optional<PaymentPartyRole> paymentPartyRoleOptional = this.getPaymentPartyRole(payment, paymentPartyRoleSubclass);

        //if party role present, extract the party
        if (paymentPartyRoleOptional.isPresent()) {

            PaymentPartyRole paymentPartyRole = paymentPartyRoleOptional.get();

//            log.info("Party is present, building the info in the msg..");

            if (paymentPartyRole.isHasCustomParty()) {
//                log.info("Getting custom party");
                CustomParty customParty = paymentPartyRole.getCustomParty();

                Party party = new Person();

                PersonIdentification personIdentification = new PersonIdentification();
                personIdentification.setIdentityCardNumber(customParty.getNationalId());
                personIdentification.setPassportNumber(customParty.getPassportNumber());

                PersonName personName = new PersonName();
                personName.setName(customParty.getName());

                personIdentification.setPersonName(Arrays.asList(personName));

                party.setIdentification(Arrays.asList(personIdentification));

//                return Optional.of(party);
                return Optional.empty();

            } else {
//                log.info("Getting registered party");
                Optional<RolePlayer> rolePlayerOptional = paymentPartyRole.getPlayer().stream()
                        .findFirst();

                //if role player present, return the party
                if (rolePlayerOptional.isPresent()) {

                    Party party = (Party) rolePlayerOptional.get();

                    return Optional.of(party);

                }
            }
        }

        return Optional.empty();
    }

    public Party11Choice getPartyIdentificationChoice(Party party) {
        Party11Choice party11Choice = new Party11Choice();

        if (party instanceof Organisation) {
            Organisation debtorOrganisation = (Organisation) party;

            OrganisationIdentification8 organisationIdentification8 = new OrganisationIdentification8();
            organisationIdentification8.setAnyBIC(debtorOrganisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getAnyBIC)
                    .findFirst().orElse(""));
            party11Choice.setOrgId(organisationIdentification8);
        } else {
            Person debtorPerson = (Person) party;

            PersonIdentification5 debtorPersonIdentification5 = new PersonIdentification5();
            GenericPersonIdentification1 debtorGenericPersonIdentification1 = new GenericPersonIdentification1();
            List<PersonIdentification> personIdentification1 = debtorPerson.getPersonIdentification();
            debtorGenericPersonIdentification1.setId(Optional.ofNullable(personIdentification1).orElse(new ArrayList<>()).stream()
                    .map(personIdentification -> Optional.ofNullable(personIdentification.getIdentityCardNumber()).orElse(""))
                    .findFirst().orElse(""));
//            debtorGenericPersonIdentification1.setSchmeNm(personIdentificationSchemeName1Choice);
//            debtorGenericPersonIdentification1.setIssr("");

            debtorPersonIdentification5.getOthr().add(debtorGenericPersonIdentification1);

            party11Choice.setPrvtId(debtorPersonIdentification5);
        }

        return party11Choice;
    }

    public Party11Choice getPartyIdentificationChoiceFromPaymentDetails(PaymentDetails paymentDetails) {
        Party11Choice party11Choice = new Party11Choice();

        if (!StringUtils.isEmpty(paymentDetails.getPrivateId())) {
            PersonIdentification5 debtorPersonIdentification5 = new PersonIdentification5();
            GenericPersonIdentification1 debtorGenericPersonIdentification1 = new GenericPersonIdentification1();
            debtorGenericPersonIdentification1.setId(paymentDetails.getPrivateId());
            debtorPersonIdentification5.getOthr().add(debtorGenericPersonIdentification1);
            party11Choice.setPrvtId(debtorPersonIdentification5);
        } else if (!StringUtils.isEmpty(paymentDetails.getOrganisationId())) {
            OrganisationIdentification8 organisationIdentification8 = new OrganisationIdentification8();
            organisationIdentification8.setAnyBIC(paymentDetails.getOrganisationId());
            party11Choice.setOrgId(organisationIdentification8);
        } else {
            return null;
        }

        return party11Choice;
    }

}
