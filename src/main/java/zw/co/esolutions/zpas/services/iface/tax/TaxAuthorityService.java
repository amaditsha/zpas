package zw.co.esolutions.zpas.services.iface.tax;

import zw.co.esolutions.zpas.dto.tax.TaxAuthorityDTO;
import zw.co.esolutions.zpas.dto.tax.TaxAuthorityPaymentOfficeDTO;
import zw.co.esolutions.zpas.model.TaxAuthority;
import zw.co.esolutions.zpas.model.TaxAuthorityPaymentOffice;

import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
public interface TaxAuthorityService {
    TaxAuthority createTaxAuthority(TaxAuthorityDTO taxAuthorityDTO);
    TaxAuthority updateTaxAuthority(TaxAuthorityDTO taxAuthorityDTO);
    TaxAuthority deleteTaxAuthority(String id);
    TaxAuthority approveTaxAuthority(String id);
    TaxAuthority rejectTaxAuthority(String id);
    Optional<TaxAuthority> getTaxAuthority(String id);
    List<TaxAuthority> getTaxAuthorities();

    Optional<TaxAuthorityPaymentOffice> createTaxAuthorityPaymentOffice(TaxAuthorityPaymentOfficeDTO taxAuthorityPaymentOfficeDTO);
    Optional<TaxAuthorityPaymentOffice> deleteTaxAuthorityPaymentOffice(String taxAuthorityPaymentOfficeId);
    Optional<TaxAuthorityPaymentOffice> approveTaxAuthorityPaymentOffice(String taxAuthorityPaymentOfficeId);
    Optional<TaxAuthorityPaymentOffice> rejectTaxAuthorityPaymentOffice(String taxAuthorityPaymentOfficeId);
    Optional<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOffice(String taxAuthorityPaymentOfficeId);
    Optional<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOfficeByCode(String taxAuthorityPaymentOfficeCode);
    List<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOffices();
    List<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOfficesByTaxAuthority(String taxAuthorityId);
}
