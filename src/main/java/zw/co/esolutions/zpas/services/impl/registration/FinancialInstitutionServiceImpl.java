package zw.co.esolutions.zpas.services.impl.registration;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.*;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.dto.party.VirtualSuffixAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.registration.FinancialInstitutionRegistrationDTO;
import zw.co.esolutions.zpas.enums.AddressTypeCode;
import zw.co.esolutions.zpas.enums.PartyTypeCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.country.CountryService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 13 Feb 2019
 */
@Slf4j
@Service
public class FinancialInstitutionServiceImpl implements FinancialInstitutionService {
    @Autowired
    private FinancialInstitutionRepository financialInstitutionRepository;

    @Autowired
    private PostalAddressRepository postalAddressRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private OrganisationNameRepository organisationNameRepository;

    @Autowired
    private AccountPartyRoleRepository accountPartyRoleRepository;

    @Autowired
    private SortCodeRepository sortCodeRepository;

    @Autowired
    private EmployingPartyRoleRepository employingPartyRoleRepository;

    @Autowired
    private OrganisationIdentificationRepository organisationIdentificationRepository;

    @Autowired
    private AccountsService accountService;

    @Autowired
    EntityManager entityManager;

    @Override
    public FinancialInstitution registerFinancialInstitution(FinancialInstitutionRegistrationDTO dto) {

        final PostalAddress placeOfRegistrationPostalAddress = new PostalAddress();
        AddressTypeCode addressType = AddressTypeCode.None;
        LocalDate registrationLocalDate;
        try {
            registrationLocalDate = LocalDate.parse(dto.getRegistrationDay(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            addressType = AddressTypeCode.valueOf(dto.getPlcOfRegAddressType());
        } catch (RuntimeException re) {
            registrationLocalDate = LocalDate.now();
            re.printStackTrace();
        }
        placeOfRegistrationPostalAddress.setId(GenerateKey.generateEntityId());
        placeOfRegistrationPostalAddress.setDateCreated(OffsetDateTime.now());
        placeOfRegistrationPostalAddress.setLastUpdated(OffsetDateTime.now());
        placeOfRegistrationPostalAddress.setEntityStatus(EntityStatus.ACTIVE);
        placeOfRegistrationPostalAddress.setEntityVersion(0L);
        placeOfRegistrationPostalAddress.setAddressType(addressType);
        placeOfRegistrationPostalAddress.setStreetName(dto.getStreetName());
        placeOfRegistrationPostalAddress.setStreetBuildingIdentification(dto.getStreetBuildingIdentification());
        placeOfRegistrationPostalAddress.setPostCodeIdentification(dto.getPostCodeIdentification());
        placeOfRegistrationPostalAddress.setTownName(dto.getTownName());
        placeOfRegistrationPostalAddress.setState(dto.getState());
        placeOfRegistrationPostalAddress.setBuildingName(dto.getBuildingName());
        placeOfRegistrationPostalAddress.setFloor(dto.getFloor());
        placeOfRegistrationPostalAddress.setDistrictName(dto.getDistrictName());

        countryService.findCountryById(dto.getPlcOfRegCountryId()).ifPresent(country -> {
            placeOfRegistrationPostalAddress.setCountyIdentification(dto.getPlcOfRegCountryId());
            placeOfRegistrationPostalAddress.setCountry(country);
        });
        placeOfRegistrationPostalAddress.setPostOfficeBox(dto.getPostOfficeBox());
        placeOfRegistrationPostalAddress.setProvince(dto.getProvince());
        placeOfRegistrationPostalAddress.setDepartment(dto.getDepartment());
        placeOfRegistrationPostalAddress.setSubDepartment(dto.getSubDepartment());
        placeOfRegistrationPostalAddress.setSuiteIdentification(dto.getSuiteIdentification());
        placeOfRegistrationPostalAddress.setBuildingIdentification(dto.getBuildingIdentification());
        placeOfRegistrationPostalAddress.setMailDeliverySubLocation(dto.getMailDeliverySubLocation());
        placeOfRegistrationPostalAddress.setBlock(dto.getBlock());
        placeOfRegistrationPostalAddress.setDistrictSubDivisionIdentification(dto.getDistrictSubDivisionIdentification());
        placeOfRegistrationPostalAddress.setLot(dto.getLot());

        FinancialInstitution financialInstitution = new FinancialInstitution();
        financialInstitution.setVirtualSuffix(dto.getVirtualSuffix());
        financialInstitution.setVirtualId(dto.getVirtualId());
        financialInstitution.setPurpose(dto.getPurpose());
        financialInstitution.setSsr(dto.getSsr());
        financialInstitution.setNssaBusinessPartnerNumber(dto.getNssaBusinessPartnerNumber());
        financialInstitution.setRegistrationDate(registrationLocalDate.atTime(OffsetTime.now()));

        List<OrganisationIdentification> organisationIdentificationList = new ArrayList<>();

        OrganisationIdentification organisationIdentification = new OrganisationIdentification();
        organisationIdentification.setBICFI(dto.getBicFIIdentifier());
        organisationIdentification.setOtherBIC(dto.getOtherBIC());
        log.info("other BIC in service implementation {} ", dto.getOtherBIC());
        organisationIdentification.setId(GenerateKey.generateEntityId());
        organisationIdentification.setDateCreated(OffsetDateTime.now());
        organisationIdentification.setLastUpdated(OffsetDateTime.now());
        organisationIdentification.setTaxIdentificationNumber(dto.getTaxIdentificationNumber());
        organisationIdentification.setEntityStatus(EntityStatus.ACTIVE);
        organisationIdentification.setEntityVersion(0L);
        organisationIdentification.setPartyType(PartyTypeCode.Issuer);

        organisationIdentification.setAnyBIC(dto.getAnyBICIdentifier());

        List<OrganisationName> organisationNames = new ArrayList<>();
        OrganisationName organisationName = new OrganisationName();
        if (financialInstitution.getOrganisationIdentification() != null && financialInstitution.getOrganisationIdentification().size() > 0) {
            organisationName.setOrganisation(financialInstitution.getOrganisationIdentification().get(0));
        }

        organisationName.setId(GenerateKey.generateEntityId());
        organisationName.setDateCreated(OffsetDateTime.now());
        organisationName.setLastUpdated(OffsetDateTime.now());
        organisationName.setEntityStatus(EntityStatus.ACTIVE);
        organisationName.setEntityVersion(0L);

        organisationName.setLegalName(dto.getLegalName());
        organisationName.setName(dto.getLegalName());
        organisationName.setTradingName(dto.getTradingName());
        organisationName.setShortName(dto.getShortName());
        organisationName.setOrganisation(organisationIdentification);
        organisationNames.add(organisationName);
        organisationIdentification.setOrganisationName(organisationNames);

        organisationIdentification.setOrganisation(financialInstitution);
        organisationIdentificationList.add(organisationIdentification);
        financialInstitution.setOrganisationIdentification(organisationIdentificationList);

        //select actual from already existing
        final Organisation parentOrganisation = null;
        financialInstitution.setParentOrganisation(parentOrganisation);

        List<Organisation> branches = new ArrayList();
        financialInstitution.setBranch(branches);

        List<Location> placesOfOperation = new ArrayList<>();
        Location placeOfOperation = new Location();
        placeOfOperation.setId(GenerateKey.generateEntityId());
        placeOfOperation.setDateCreated(OffsetDateTime.now());
        placeOfOperation.setLastUpdated(OffsetDateTime.now());
        placeOfOperation.setEntityStatus(EntityStatus.DRAFT);
        placeOfOperation.setEntityVersion(0L);
        placeOfOperation.setOperatingOrganisation(financialInstitution);
        PostalAddress placeOfOperationPostalAddress = readPostalAddressForPlaceOfOperation(dto);
        placeOfOperationPostalAddress.setLocation(placeOfOperation);
        placeOfOperationPostalAddress.setRelatedParty(financialInstitution);
        placeOfOperation.setAddress(Arrays.asList(placeOfOperationPostalAddress));
        placesOfOperation.add(placeOfOperation);
        financialInstitution.setPlaceOfOperation(placesOfOperation);

        final Location placeOfRegistration = new Location();
        placeOfRegistration.setId(GenerateKey.generateEntityId());
        placeOfRegistration.setDateCreated(OffsetDateTime.now());
        placeOfRegistration.setLastUpdated(OffsetDateTime.now());
        placeOfRegistration.setEntityStatus(EntityStatus.DRAFT);
        placeOfRegistration.setEntityVersion(0L);
        placeOfRegistrationPostalAddress.setRelatedParty(financialInstitution);
        placeOfRegistrationPostalAddress.setLocation(placeOfRegistration);
        placeOfRegistration.setOperatingOrganisation(financialInstitution);

        placeOfRegistration.setAddress(Arrays.asList(placeOfRegistrationPostalAddress));
        placeOfRegistration.setRegisteredOrganisation(financialInstitution);

        financialInstitution.setPlaceOfRegistration(placeOfRegistration);

        financialInstitution.setDescription(dto.getDescription());

        financialInstitution.setEstablishmentDate(OffsetDateTime.now());

        List<ContactPoint> contactPoints = readContactPointsJsonWithObjectMapper(dto.getPhonesJson(), dto.getAddressesJson());
        contactPoints.forEach(contactPoint -> {
            log.info("Contact point is: {}", contactPoint);
            contactPoint.setRelatedParty(financialInstitution);
        });
        financialInstitution.setContactPoint(contactPoints);

        List<PartyIdentificationInformation> identifications = new ArrayList<>();
        financialInstitution.setIdentification(identifications);

        List<Location> residence = new ArrayList<>();
        financialInstitution.setResidence(residence);

        financialInstitution.setId(GenerateKey.generateEntityId());
        financialInstitution.setDateCreated(OffsetDateTime.now());
        financialInstitution.setLastUpdated(OffsetDateTime.now());
        financialInstitution.setEntityStatus(EntityStatus.DRAFT);

        List<Role> roles = new ArrayList<>();
        financialInstitution.setRole(roles);

        List<SortCode> sortCodes = readSortCodeJsonWithObjectMapper(dto.getSortCodesJson());
        financialInstitution.setSortCodes(sortCodes);

        sortCodes.forEach(setCode -> {
            log.info("Set code is: {}", setCode);
            setCode.setFinancialInstitution(financialInstitution);
        });


        final DateTimePeriod dateTimePeriod = new DateTimePeriod();
        dateTimePeriod.setId(GenerateKey.generateEntityId());
        dateTimePeriod.setId(GenerateKey.generateEntityId());
        dateTimePeriod.setDateCreated(OffsetDateTime.now());
        dateTimePeriod.setLastUpdated(OffsetDateTime.now());
        dateTimePeriod.setEntityStatus(EntityStatus.DRAFT);

        final LocalDate fromStartDate = LocalDate.parse(dto.getValidityPeriodStartDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dateTimePeriod.setFromDateTime(fromStartDate.atTime(OffsetTime.now()));

        final LocalDate toEndDate = LocalDate.parse(dto.getValidityPeriodEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dateTimePeriod.setToDateTime(toEndDate.atTime(OffsetTime.now()));

        dateTimePeriod.setNumberOfDays(Period.between(fromStartDate, toEndDate).getDays());

        dateTimePeriod.setEntityVersion(0L);

        financialInstitution.setValidityPeriod(dateTimePeriod);

        return financialInstitutionRepository.save(financialInstitution);
    }

    @Override
    public Optional<FinancialInstitution> approveFinancialInstitution(String financialInstitutionId) {
        return financialInstitutionRepository.findById(financialInstitutionId).map(financialInstitution -> {
            if (financialInstitution.getEntityStatus() == EntityStatus.DRAFT) {
                log.error("Financial Institution has not been submitted, can't be approved.");
                return Optional.ofNullable(financialInstitution);
            }
            financialInstitution.setEntityStatus(EntityStatus.ACTIVE);
            final FinancialInstitution saved = financialInstitutionRepository.save(financialInstitution);
            return Optional.ofNullable(saved);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<FinancialInstitution> rejectFinancialInstitution(String financialInstitutionId) {
        return financialInstitutionRepository.findById(financialInstitutionId).map(financialInstitution -> {
            if (financialInstitution.getEntityStatus() == EntityStatus.DRAFT) {
                log.error("Financial Institution has not been submitted, can't be rejected.");
                return Optional.ofNullable(financialInstitution);
            }
            financialInstitution.setEntityStatus(EntityStatus.DISAPPROVED);
            final FinancialInstitution saved = financialInstitutionRepository.save(financialInstitution);
            return Optional.ofNullable(saved);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<FinancialInstitution> submitFinancialInstitution(String financialInstitutionId) {
        return financialInstitutionRepository.findById(financialInstitutionId).map(financialInstitution -> {
            financialInstitution.setEntityStatus(EntityStatus.PENDING_APPROVAL);
            final FinancialInstitution saved = financialInstitutionRepository.save(financialInstitution);
            return Optional.ofNullable(saved);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<FinancialInstitution> deleteFinancialInstitution(String financialInstitutionId) {
        return financialInstitutionRepository.findById(financialInstitutionId).map(financialInstitution -> {
            if (financialInstitution.getEntityStatus() != EntityStatus.DRAFT) {
                log.error("Financial Institution cannot be deleted. It's not in draft status");
            }
            financialInstitution.setEntityStatus(EntityStatus.DELETED);
            return Optional.ofNullable(financialInstitution);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<FinancialInstitution> getFinancialInstitutionByAnyBIC(String anyBIC) {
        if(StringUtils.isEmpty(anyBIC)) return Optional.empty();
        return organisationIdentificationRepository.getByAnyBICOrBICFI(anyBIC.trim()).map(oi -> {
            return financialInstitutionRepository.findByOrganisationIdentification_Id(oi.getId());
        }).orElseGet(() -> {
            log.info("Organisation not found for BIC {}", anyBIC);
            FinancialInstitution financialInstitution = null;
            return Optional.ofNullable(financialInstitution);
        });
    }

    @Override
    public Optional<FinancialInstitution> getFinancialInstitutionById(String financialInstitutionId) {
        if(StringUtils.isEmpty(financialInstitutionId)) return Optional.empty();
        return findFinancialInstitutionByIdHelper(financialInstitutionId);
    }

    @Override
    public Optional<FinancialInstitution> getFinancialInstitutionByVirtualSuffix(String virtualSuffix) {
        return financialInstitutionRepository.findFinancialInstitutionByVirtualSuffix(virtualSuffix);
    }

    @Override
    public Optional<VirtualSuffixAvailabilityCheckDTO> checkVirtualSuffixAvailability(String virtualSuffix) {
        final String sanitisedVirtualSuffix = Optional.ofNullable(virtualSuffix).map(String::toLowerCase).orElse("");
        return financialInstitutionRepository.findFinancialInstitutionByVirtualSuffix(sanitisedVirtualSuffix)
                .map(financialInstitution -> {
                    return Optional.of(VirtualSuffixAvailabilityCheckDTO.builder()
                            .available(false)
                            .financialInstitutionId(financialInstitution.getId())
                            .virtualSuffix(financialInstitution.getVirtualId())
                            .financialInstitutionName(financialInstitution.getName())
                            .build());
                }).orElseGet(() -> {
                    return Optional.of(VirtualSuffixAvailabilityCheckDTO.builder()
                            .available(true)
                            .financialInstitutionId("")
                            .virtualSuffix(virtualSuffix)
                            .financialInstitutionName("")
                            .build());
                });
    }

    private Optional<FinancialInstitution> findFinancialInstitutionByIdHelper(String financialInstitutionId) {
        return financialInstitutionRepository.findById(financialInstitutionId).map(financialInstitution -> {
            final List<OrganisationIdentification> organisationIdentifications = organisationIdentificationRepository
                    .findAllByOrganisationId(financialInstitutionId);
            log.info("Organisation Identifications: {}", organisationIdentifications);
            organisationIdentifications.forEach(organisationIdentification -> {
                final List<OrganisationName> organisationNames = organisationNameRepository.findAllByOrganisationId(organisationIdentification.getId());
                log.info("Organisation Names: {}", organisationNames);
                organisationIdentification.setOrganisationName(organisationNames);
            });

            final Location placeOfRegistration = financialInstitution.getPlaceOfRegistration();
            if (placeOfRegistration != null) {
                log.info("Place of Registration Location Id: {}", placeOfRegistration.getId());
                final List<PostalAddress> placeOfRegistrationAddresses = postalAddressRepository.findAllByLocationId(placeOfRegistration.getId());
                log.info("Postal Addresses for Place of Registration: {}", placeOfRegistrationAddresses);
                placeOfRegistration.setAddress(placeOfRegistrationAddresses);
                financialInstitution.setPlaceOfRegistration(placeOfRegistration);
            }

            final List<Location> placesOfOperation = financialInstitution.getPlaceOfOperation();
            if(placesOfOperation != null) {
                log.info("Size of place of operation: {}", placesOfOperation.size());
                placesOfOperation.forEach(location -> {
                    log.info("Place of Operation Location Id: {}", location.getId());
                    final List<PostalAddress> allByLocationId = postalAddressRepository.findAllByLocationId(location.getId());
                    log.info("Postal Addresses for place of operation: {}", allByLocationId);
                    location.setAddress(allByLocationId);
                });
            }

            financialInstitution.setOrganisationIdentification(organisationIdentifications);
            financialInstitution.getContactPoint().removeIf(contactPoint -> contactPoint instanceof PostalAddress);
            financialInstitution.getSortCodes().removeIf(sortCode -> sortCode instanceof SortCode);
            sanitiseFinancialInstitution(financialInstitution);
            final List<SortCode> financialInstitutionSortCodes = sortCodeRepository.findAllByFinancialInstitution(financialInstitution);
            financialInstitution.setSortCodes(financialInstitutionSortCodes);
            return Optional.of(financialInstitution);
        }).orElseGet(() -> {
            return Optional.empty();
        });
    }

    private void sanitiseFinancialInstitution(FinancialInstitution financialInstitution) {
        if (financialInstitution.getPurpose() == null) {
            financialInstitution.setPurpose("");
        }
        if (financialInstitution.getRegistrationDate() == null) {
            financialInstitution.setRegistrationDate(OffsetDateTime.now());
        }
        if (financialInstitution.getOrganisationIdentification() == null) {
            financialInstitution.setOrganisationIdentification(new ArrayList());
        }
        if (financialInstitution.getPlaceOfOperation() == null) {
            financialInstitution.setPlaceOfOperation(new ArrayList());
        }
        if (financialInstitution.getPlaceOfRegistration() == null) {
            financialInstitution.setPlaceOfRegistration(new Location());
        }
        if (financialInstitution.getDescription() == null) {
            financialInstitution.setDescription("");
        }
        if (financialInstitution.getEstablishmentDate() == null) {
            financialInstitution.setEstablishmentDate(OffsetDateTime.now());
        }
        if (financialInstitution.getContactPoint() == null) {
            financialInstitution.setContactPoint(new ArrayList());
        }
        if (financialInstitution.getIdentification() == null) {
            financialInstitution.setIdentification(new ArrayList());
        }
        if (financialInstitution.getTaxationConditions() == null) {
            financialInstitution.setTaxationConditions(new Tax());
        }
        if (financialInstitution.getResidence() == null) {
            financialInstitution.setResidence(new ArrayList());
        }
        if (financialInstitution.getValidityPeriod() == null) {
            financialInstitution.setValidityPeriod(new DateTimePeriod());
        }
        if (financialInstitution.getSortCodes() == null) {
            financialInstitution.setSortCodes(new ArrayList<>());
        }

    }

    @Override
    public List<FinancialInstitution> getFinancialInstitutions() {
        return financialInstitutionRepository.findAll();
    }

    @Override
    public List<FinancialInstitution> getFinancialInstitutionsByClientId(String clientId) {
        log.info("ClientId: " + clientId);
        final List<Account> accounts = accountService.getAccountsByClientId(clientId);
        final List<FinancialInstitution> financialInstitutions = new ArrayList<>();
        accounts.forEach(account -> {
            accountPartyRoleRepository.findAllByAccount_Id(account.getId()).stream()
                    .filter(role -> role instanceof AccountServicerRole).forEach(role -> {
                final List<FinancialInstitution> fis = financialInstitutionRepository.findAllByRole_Id(role.getId());
                financialInstitutions.addAll(fis);
            });
        });
        return financialInstitutions;
    }

    @Override
    public Optional<Organisation> getOrganisationByEmployeeId(String employeeId) {
        final List<EmployingPartyRole> employingPartyRoles = employingPartyRoleRepository.findAllByEmployee_Id(employeeId);
        return employingPartyRoles.stream().findFirst().map(employingPartyRole -> {
            final List<RolePlayer> employers = employingPartyRole.getPlayer();
            return employers.stream().findFirst().map(rolePlayer -> {
                Organisation organisation = (Organisation) rolePlayer;
                return Optional.ofNullable(organisation);
            }).orElse(Optional.empty());
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<FinancialInstitution> getFinancialInstitutionByAccountId(String accountId) {
        final CashAccount account = accountService.findAccountById(accountId);
        final List<FinancialInstitution> financialInstitutions = new ArrayList<>();
        accountPartyRoleRepository.findAllByAccount_Id(account.getId()).stream()
                .filter(role -> role instanceof AccountServicerRole).forEach(role -> {
            final List<FinancialInstitution> fis = financialInstitutionRepository.findAllByRole_Id(role.getId());
            financialInstitutions.addAll(fis);
        });
        if (financialInstitutions.size() > 0) {
            final FinancialInstitution financialInstitution = financialInstitutions.get(0);
            return getFinancialInstitutionById(financialInstitution.getId());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public FinancialInstitution updateFinancialInstitutionGeneralInformation(FinancialInstitution financialInstitution) {
        financialInstitution.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return financialInstitutionRepository.save(financialInstitution);
    }

    @Override
    public OrganisationIdentification updateFinancialInstitutionOrganisatinIdentification(OrganisationIdentification organisationIdentification) {
        organisationIdentification.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return organisationIdentificationRepository.save(organisationIdentification);
    }

    @Override
    public FinancialInstitution updateFinancialInstitution(FinancialInstitutionRegistrationDTO dto) {
        OrganisationIdentification organisationIdentification = new OrganisationIdentification();
        organisationIdentification.setBICFI(dto.getBicFIIdentifier());
        organisationIdentification.setDateCreated(OffsetDateTime.now());
        organisationIdentification.setLastUpdated(OffsetDateTime.now());
        organisationIdentification.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        organisationIdentification.setEntityVersion(1L);

        organisationIdentification.setAnyBIC(dto.getAnyBICIdentifier());

        List<OrganisationName> organisationNames = new ArrayList<>();
        OrganisationName organisationName = new OrganisationName();
        organisationName.setLegalName(dto.getLegalName());
        organisationName.setTradingName(dto.getTradingName());
        organisationName.setShortName(dto.getShortName());
        organisationNames.add(organisationName);
        organisationIdentification.setOrganisationName(organisationNames);

        PostalAddress postalAddress = new PostalAddress();

        AddressTypeCode addressType = AddressTypeCode.valueOf(dto.getPlcOfRegAddressType());
        postalAddress.setAddressType(addressType);
        postalAddress.setStreetName(dto.getStreetName());
        postalAddress.setStreetBuildingIdentification(dto.getStreetBuildingIdentification());
        postalAddress.setPostCodeIdentification(dto.getPostCodeIdentification());
        postalAddress.setTownName(dto.getTownName());
        postalAddress.setState(dto.getState());
        postalAddress.setBuildingName(dto.getBuildingName());
        postalAddress.setFloor(dto.getFloor());
        postalAddress.setDistrictName(dto.getDistrictName());

        postalAddress.setPostOfficeBox(dto.getPostOfficeBox());
        postalAddress.setProvince(dto.getProvince());
        postalAddress.setDepartment(dto.getDepartment());
        postalAddress.setSubDepartment(dto.getSubDepartment());
        postalAddress.setSuiteIdentification(dto.getSuiteIdentification());
        postalAddress.setBuildingIdentification(dto.getBuildingIdentification());
        postalAddress.setMailDeliverySubLocation(dto.getMailDeliverySubLocation());
        postalAddress.setBlock(dto.getBlock());
        postalAddress.setDistrictSubDivisionIdentification(dto.getDistrictSubDivisionIdentification());
        postalAddress.setLot(dto.getLot());

        FinancialInstitution financialInstitution = new FinancialInstitution();
        financialInstitution.setPurpose(dto.getPurpose());

        LocalDate registrationLocalDate = LocalDate.parse(dto.getRegistrationDay(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        financialInstitution.setRegistrationDate(registrationLocalDate.atTime(OffsetTime.now()));

        List<OrganisationIdentification> organisationIdentificationList = new ArrayList<>();
        organisationIdentification.setOrganisation(financialInstitution);
        organisationIdentificationList.add(organisationIdentification);
        financialInstitution.setOrganisationIdentification(organisationIdentificationList);

        //select actual from already existing
        final Organisation parentOrganisation = null;
        financialInstitution.setParentOrganisation(parentOrganisation);

        List<Organisation> branches = new ArrayList();
        financialInstitution.setBranch(branches);

        List<Location> placesOfOperation = new ArrayList<>();
        financialInstitution.setPlaceOfOperation(placesOfOperation);

        final Location placeOfRegistration = new Location();
        placeOfRegistration.setDateCreated(OffsetDateTime.now());
        placeOfRegistration.setLastUpdated(OffsetDateTime.now());
        placeOfRegistration.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        placeOfRegistration.setEntityVersion(1L);
//        placeOfRegistration.setDomiciledParty(new Party());
        placeOfRegistration.setRegisteredOrganisation(financialInstitution);
        placeOfRegistration.setAddress(Arrays.asList(postalAddress));

        financialInstitution.setPlaceOfRegistration(placeOfRegistration);

        financialInstitution.setDescription(dto.getDescription());

        financialInstitution.setEstablishmentDate(OffsetDateTime.now());

        List<ContactPoint> contactPoints = readContactPointsJsonWithObjectMapper(dto.getPhonesJson(), dto.getAddressesJson());
        contactPoints.forEach(contactPoint -> {
            log.info("Contact point is: {}", contactPoint);
            contactPoint.setRelatedParty(financialInstitution);
        });
        financialInstitution.setContactPoint(contactPoints);

        List<PartyIdentificationInformation> identifications = new ArrayList<>();
        financialInstitution.setIdentification(identifications);

//        final Tax taxConditions = new Tax();
//        taxConditions.setId(GenerateKey.generateEntityId());
//        taxConditions.setDateCreated(OffsetDateTime.now());
//        taxConditions.setLastUpdated(OffsetDateTime.now());
//        taxConditions.setEntityStatus(EntityStatus.DRAFT);
//        final CurrencyAndAmount amount = new CurrencyAndAmount();
//        amount.setAmount(new BigDecimal(2));
//        taxConditions.setAmount(new CurrencyAndAmount());
//        taxConditions.setRate(new PercentageRate());
//
//        financialInstitution.setTaxationConditions(taxConditions);

//        final Location domicile = new Location();
//        financialInstitution.setDomicile(domicile);

        List<Location> residence = new ArrayList<>();
        financialInstitution.setResidence(residence);
//
//        final PowerOfAttorney powerOfAttorney = new PowerOfAttorney();
//        powerOfAttorney.setId(GenerateKey.generateEntityId());
//        powerOfAttorney.setEntityStatus(EntityStatus.DRAFT);
//        powerOfAttorney.setDateCreated(OffsetDateTime.now());
//        powerOfAttorney.setLastUpdated(OffsetDateTime.now());
//        powerOfAttorney.setEntityVersion(0L);
//
//        financialInstitution.setPowerOfAttorney(powerOfAttorney);

        //  financialInstitution.setId(GenerateKey.generateEntityId());
        // financialInstitution.setDateCreated(OffsetDateTime.now());
        financialInstitution.setLastUpdated(OffsetDateTime.now());
        financialInstitution.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
//        financialInstitution.setEntityVersion(0L);

        List<Role> roles = new ArrayList<>();
        financialInstitution.setRole(roles);

        final DateTimePeriod dateTimePeriod = new DateTimePeriod();
       /* dateTimePeriod.setId(GenerateKey.generateEntityId());
        dateTimePeriod.setId(GenerateKey.generateEntityId());*/
        dateTimePeriod.setDateCreated(OffsetDateTime.now());
        dateTimePeriod.setLastUpdated(OffsetDateTime.now());
        dateTimePeriod.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);

        final LocalDate fromStartDate = LocalDate.parse(dto.getValidityPeriodStartDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dateTimePeriod.setFromDateTime(fromStartDate.atTime(OffsetTime.now()));

        final LocalDate toEndDate = LocalDate.parse(dto.getValidityPeriodEndDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        dateTimePeriod.setToDateTime(toEndDate.atTime(OffsetTime.now()));

        dateTimePeriod.setNumberOfDays(Period.between(fromStartDate, toEndDate).getDays());

        dateTimePeriod.setEntityVersion(1L);

        financialInstitution.setValidityPeriod(dateTimePeriod);
        return financialInstitutionRepository.save(financialInstitution);
    }

    public PostalAddress readPostalAddressForPlaceOfOperation(FinancialInstitutionRegistrationDTO firDTO) {
        final Optional<Country> countryOptional = countryService.findCountryById(firDTO.getPlaceOfOperationCountryId());

        final PostalAddress postalAddress = new PostalAddress();
        postalAddress.setId(GenerateKey.generateEntityId());
        postalAddress.setDateCreated(OffsetDateTime.now());
        postalAddress.setLastUpdated(OffsetDateTime.now());
        postalAddress.setEntityStatus(EntityStatus.ACTIVE);

        AddressTypeCode addressTypeCode = AddressTypeCode.Business;
        try {
            addressTypeCode = AddressTypeCode.valueOf(firDTO.getPlaceOfOperationAddressType());
        } catch (RuntimeException re) {

        }
        postalAddress.setAddressType(addressTypeCode);
        postalAddress.setStreetName(firDTO.getPlaceOfOperationStreetName());
        postalAddress.setStreetBuildingIdentification(firDTO.getPlaceOfOperationStreetBuildingIdentification());
        postalAddress.setPostCodeIdentification(firDTO.getPlaceOfOperationPostCodeIdentification());
        postalAddress.setTownName(firDTO.getPlaceOfOperationTownName());
        postalAddress.setState(firDTO.getPlaceOfOperationState());
        postalAddress.setBuildingName(firDTO.getPlaceOfOperationBuildingName());
        postalAddress.setFloor(firDTO.getPlaceOfOperationFloor());
        postalAddress.setDistrictName(firDTO.getPlaceOfOperationDistrictName());
        postalAddress.setRegionIdentification("");
        postalAddress.setPostOfficeBox(firDTO.getPlaceOfOperationPostOfficeBox());
        postalAddress.setProvince(firDTO.getPlaceOfOperationProvince());
        postalAddress.setDepartment(firDTO.getPlaceOfOperationDepartment());
        postalAddress.setSubDepartment(firDTO.getPlaceOfOperationSubDepartment());
        postalAddress.setSuiteIdentification(firDTO.getPlaceOfOperationSuiteIdentification());
        postalAddress.setBuildingIdentification(firDTO.getPlaceOfOperationBuildingIdentification());
        postalAddress.setMailDeliverySubLocation(firDTO.getPlaceOfOperationMailDeliverySubLocation());
        postalAddress.setBlock(firDTO.getPlaceOfOperationBlock());
        postalAddress.setLot(firDTO.getPlaceOfOperationLot());

        countryOptional.ifPresent(country -> {
            postalAddress.setCountry(country);
            postalAddress.setCountyIdentification(country.getId());
        });

        postalAddress.setDistrictSubDivisionIdentification(firDTO.getPlaceOfOperationDistrictSubDivisionIdentification());
        return postalAddress;

    }

    public List<ContactPoint> readContactPointsJsonWithObjectMapper(String phonesJson, String electronicAddressesJson) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        try {
            List<ContactPoint> contactPoints = new ArrayList<>();
            List<PhoneAddress> phoneAddresses = mapper.readValue(phonesJson, mapper.getTypeFactory()
                    .constructCollectionType(List.class, PhoneAddress.class));
            List<ElectronicAddress> electronicAddresses = mapper.readValue(electronicAddressesJson, mapper.getTypeFactory()
                    .constructCollectionType(List.class, ElectronicAddress.class));
            phoneAddresses.forEach(phoneAddress -> {
                phoneAddress.setId(GenerateKey.generateEntityId());
                phoneAddress.setDateCreated(OffsetDateTime.now());
                phoneAddress.setLastUpdated(OffsetDateTime.now());
                phoneAddress.setEntityStatus(EntityStatus.ACTIVE);
                phoneAddress.setEntityVersion(0L);
                contactPoints.add(phoneAddress);
            });
            electronicAddresses.forEach(electronicAddress -> {
                electronicAddress.setId(GenerateKey.generateEntityId());
                electronicAddress.setDateCreated(OffsetDateTime.now());
                electronicAddress.setLastUpdated(OffsetDateTime.now());
                electronicAddress.setEntityStatus(EntityStatus.ACTIVE);
                electronicAddress.setEntityVersion(0L);
                contactPoints.add(electronicAddress);
            });
            log.info(contactPoints + "");
            return contactPoints;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<SortCode> readSortCodeJsonWithObjectMapper(String sortCodesJson){
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        try {
            List<SortCode> sortCodeList = new ArrayList<>();
            List<SortCode> sortCodes = mapper.readValue(sortCodesJson, mapper.getTypeFactory()
                    .constructCollectionType(List.class, SortCode.class));
            sortCodes.forEach(sortCode -> {
                sortCode.setId(GenerateKey.generateEntityId());
                sortCode.setDateCreated(OffsetDateTime.now());
                sortCode.setLastUpdate(OffsetDateTime.now());
                sortCode.setEntityStatus(EntityStatus.ACTIVE);
                sortCodeList.add(sortCode);
            });
            log.info(sortCodes + "");
            return sortCodes;
        }catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public String refreshOrganisationIndex() {
        log.info("Refreshing Financial Institution#################");
        FullTextSession fullTextSession = Search.getFullTextSession(entityManager.unwrap(Session.class));
        fullTextSession.setFlushMode(FlushMode.MANUAL);
        fullTextSession.setCacheMode(CacheMode.IGNORE);
        int BATCH_SIZE = 25;
        ScrollableResults scrollableResults = fullTextSession.createCriteria(OrganisationName.class)
                .setFetchSize(BATCH_SIZE)
                .scroll(ScrollMode.FORWARD_ONLY);
        int index = 0;
        while(scrollableResults.next()) {
            index++;
            fullTextSession.index(scrollableResults.get(0)); //index each element
            if (index % BATCH_SIZE == 0) {
                fullTextSession.flushToIndexes(); //apply changes to indexes
                fullTextSession.clear(); //free memory since the queue is processed
            }
        }

        return "Organisation search indices reloaded successfully";
    }

}
