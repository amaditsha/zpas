package zw.co.esolutions.zpas.services.impl.process.pojo;

import lombok.Value;
import zw.co.esolutions.zpas.model.IndividualPayment;

import java.math.BigDecimal;
import java.util.List;

@Value
public final class PaymentInfoDto {
    public final List<IndividualPayment> individualPayments;
    public final Integer nrOfPayments;
    public final BigDecimal controlSum;

    public PaymentInfoDto(List<IndividualPayment> individualPayments, Integer nrOfPayments, BigDecimal controlSum) {
        this.individualPayments = individualPayments;
        this.nrOfPayments = nrOfPayments;
        this.controlSum = controlSum;
    }
}
