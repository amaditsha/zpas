package zw.co.esolutions.zpas.services.iface.hubconfig;

import zw.co.esolutions.zpas.model.HubConfiguration;

import java.util.List;
import java.util.Optional;

public interface HubConfigurationService {
    HubConfiguration createConfig(HubConfiguration configuration);
    HubConfiguration updateConfig(HubConfiguration configuration);
    HubConfiguration deleteHubConfig(String configurationId);
    HubConfiguration approveHubConfig(String id);
    HubConfiguration rejectHubConfig(String id);
    void deleteConfig(String configurationId);
    Optional<HubConfiguration> getConfig(String BIC);
    List<HubConfiguration> getConfigs();
}
