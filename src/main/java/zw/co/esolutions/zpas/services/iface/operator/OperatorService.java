package zw.co.esolutions.zpas.services.iface.operator;

import zw.co.esolutions.zpas.model.Operator;

import java.util.List;
import java.util.Optional;

public interface OperatorService {
    Operator createOperator(Operator operator);
    Operator updateOperator(Operator operator);
    Operator deleteOperator(String id);
    Operator approveOperator(String id);
    Operator rejectOperator(String id);
    Optional<Operator> getOperatorById(String operatorId);
    List<Operator> getOperatorByCode(String code);
    List<Operator> getOperatorByName(String name);

}
