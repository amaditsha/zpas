package zw.co.esolutions.zpas.services.impl.statements;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.dto.message.MessageResponseDTO;
import zw.co.esolutions.zpas.dto.message.NotificationRequestDTO;
import zw.co.esolutions.zpas.dto.messaging.AccountStatementDTO;
import zw.co.esolutions.zpas.dto.messaging.AccountStatementInfo;
import zw.co.esolutions.zpas.dto.messaging.ProofOfPaymentDTO;
import zw.co.esolutions.zpas.dto.messaging.ProofOfPaymentInfo;
import zw.co.esolutions.zpas.model.CashAccount;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.iface.statements.StatementsService;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static zw.co.esolutions.zpas.utilities.util.StringUtil.formatCamelCaseToSpacedString;

/**
 * Created by alfred on 14 Mar 2019
 */
@Service
@Slf4j
@Transactional
public class StatementsServiceImpl implements StatementsService {
    @Autowired
    private NotificationConfigService notificationConfigService;

    @Autowired
    private PDFGenerator pdfGenerator;

    @Autowired
    private AccountStatementPDFGenerator accountStatementPDFGenerator;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private ClientService clientService;

    @Override
    public ProofOfPaymentInfo getProofOfPaymentInfo(String paymentId) {
        return paymentsService.getPaymentById(paymentId).map(payment -> {
            final ProofOfPaymentInfo.ProofOfPaymentInfoBuilder builder = ProofOfPaymentInfo.builder();
            final CashAccount account = payment.getAccount();
            builder.paymentReference(payment.getEndToEndId());
            Optional.ofNullable(account).ifPresent(cashAccount -> {
                log.info("Cash account found for payment: {}", cashAccount);
                final Optional<FinancialInstitution> financialInstitutionOptional = Optional.ofNullable(cashAccount.getFinancialInstitution());
                financialInstitutionOptional.ifPresent(financialInstitution -> {
                    log.info("Found Financial Institution: ===>{}", financialInstitution);
                    final String financialInstitutionName = financialInstitution.getName();
                    log.info("Found Debtor bank: {}", financialInstitutionName);
                    builder.debtorBank(financialInstitutionName);
                });

                final Optional<Party> partyOptional = Optional.ofNullable(cashAccount.getAccountOwner());
                partyOptional.ifPresent(party -> {
                    final String partyName = party.getName();
                    log.info("Found Debtor: {}", partyName);
                    builder.debtorName(partyName);
                });
            });

            builder.creditorAccountNumber(payment.getBeneficiaryAccount());
            builder.creditorBank(payment.getBeneficiaryBank());
            builder.creditorName(payment.getBeneficiaryName());

            return builder.payment(payment).build();
        }).orElse(ProofOfPaymentInfo.builder().build());
    }

    @Override
    public ProofOfPaymentInfo sendProofOfPaymentInfo(ProofOfPaymentDTO proofOfPaymentDTO) {
        final ProofOfPaymentInfo proofOfPaymentInfo = getProofOfPaymentInfo(proofOfPaymentDTO.getPaymentId());
        final Payment payment = proofOfPaymentInfo.getPayment();

        if(payment != null) {
            final Path generatedDocumentPath = Paths.get(pdfGenerator.generateDocument(proofOfPaymentInfo));

            String currencyCodeName = "";
            final CashAccount account = payment.getAccount();
            if(account != null) {
                currencyCodeName = Optional.ofNullable(account.getBaseCurrency()).map(currencyCode -> currencyCode.getCodeName()).orElse("");
            } else {
                StringBuilder sb = new StringBuilder();
                Optional.ofNullable(payment.getAmount()).ifPresent(currencyAndAmount -> {
                    Optional.ofNullable(currencyAndAmount.getCurrency()).ifPresent(currencyCode -> {
                        Optional.ofNullable(currencyCode.getCodeName()).ifPresent(s -> {
                            sb.append(s);
                        });
                    });
                });
                currencyCodeName = sb.toString();
            }

            String amount = MoneyUtil.convertDollarsToPattern(0);
            StringBuilder moneySb = new StringBuilder();
            Optional.ofNullable(payment.getAmount()).ifPresent(currencyAndAmount -> {
                Optional.ofNullable(currencyAndAmount.getAmount()).ifPresent(bigDecimal -> {
                    Optional.ofNullable(bigDecimal.doubleValue()).ifPresent(aDouble -> {
                        moneySb.append(MoneyUtil.convertDollarsToPattern(aDouble));
                    });
                });
            });
            amount = moneySb.toString();

            final String from = "noreply@esolutions.co.zw";
            final String subject = "Proof Of Payment for Payment with reference: " + payment.getEndToEndId();
            final String messageText = "Hi " + proofOfPaymentDTO.getRecipientName() + ",\n" +
                    "This email serves to inform you that " + proofOfPaymentInfo.getDebtorName() + " of bank " +
                    proofOfPaymentInfo.getDebtorBank() + " made a " + formatCamelCaseToSpacedString(payment.getClass().getSimpleName()) +
                    " payment of " + currencyCodeName + " " + amount + " on " +
                    DateTimeFormatter.ofPattern("dd MMMM yyyy").format(payment.getValueDate()) + " to account number " +
                    proofOfPaymentInfo.getCreditorAccountNumber() + " of bank " + proofOfPaymentInfo.getCreditorBank() +
                    " belonging to " + proofOfPaymentInfo.getCreditorName() + ". The payment reference is " + proofOfPaymentInfo.getPaymentReference() + ".";


            final MessageResponseDTO messageResponseDTO = notificationConfigService.processAndSendNotifications(NotificationRequestDTO.builder()
                    .attachmentFilePaths(Arrays.asList(generatedDocumentPath.toString()))
                    .messageChannel(MessageChannel.EMAIL)
                    .messageText(messageText)
                    .destination(proofOfPaymentDTO.getEmail())
                    .originator(from)
                    .subject(subject)
                    .messageDate(OffsetDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                    .clientId(proofOfPaymentDTO.getClientId())
                    .build());

            return proofOfPaymentInfo;
        } else {
            return proofOfPaymentInfo;
        }
    }

    @Override
    public AccountStatementDTO sendAccountStatementInfo(AccountStatementDTO accountStatementDTO) {
        //final ProofOfPaymentInfo proofOfPaymentInfo = getProofOfPaymentInfo(proofOfPaymentDTO.getPaymentId());
        final List<AccountStatementInfo> accountStatementInfos = accountStatementDTO.getAccountStatementInfoList();

        if(accountStatementInfos != null && !accountStatementInfos.isEmpty()) {
            final Path generatedDocumentPath = Paths.get(accountStatementPDFGenerator.generateDocument(accountStatementDTO));

            final String from = "noreply@esolutions.co.zw";
            final String subject = "Account Statement for: " + accountStatementDTO.getRecipient();
            final String messageText = "Hi " + accountStatementDTO.getRecipient() + ",\n" +
                    "This email serves to deliver your account statement as per your request. " +
                    "Should you need further clarification, please contact the bank.";

            final MessageResponseDTO messageResponseDTO = notificationConfigService.processAndSendNotifications(NotificationRequestDTO.builder()
                    .attachmentFilePaths(Arrays.asList(generatedDocumentPath.toString()))
                    .messageChannel(MessageChannel.EMAIL)
                    .messageText(messageText)
                    .destination(accountStatementDTO.getEmailAddress())
                    .originator(from)
                    .subject(subject)
                    .messageDate(OffsetDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                    .clientId(accountStatementDTO.getClientId())
                    .build());

            return accountStatementDTO;
        } else {
            return accountStatementDTO;
        }
    }
}
