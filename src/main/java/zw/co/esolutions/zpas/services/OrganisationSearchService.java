package zw.co.esolutions.zpas.services;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.*;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.OrganisationIdentification;
import zw.co.esolutions.zpas.model.OrganisationName;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class OrganisationSearchService {

    @Autowired
    private final EntityManager entityManager;


    public OrganisationSearchService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Organisation> searchOrganisation(String searchTerm){

        FullTextEntityManager fullTextEm = org.hibernate.search.jpa.Search
                .getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEm.getSearchFactory().buildQueryBuilder().forEntity(OrganisationName.class).get();

        FullTextQuery fullTextQuery = fullTextEm.createFullTextQuery(queryBuilder.keyword()
                .onFields("legalName", "shortName", "tradingName")
                .matching(searchTerm)
                .createQuery(), OrganisationName.class);

        List<OrganisationName> resultList = fullTextQuery.getResultList();
        List<Organisation> organisationList = new ArrayList<>();

        resultList.forEach(organisationName -> {
            OrganisationIdentification organisationIdentification = organisationName.getOrganisation();
            Organisation organisation = organisationIdentification.getOrganisation();

            organisationList.add(organisation);
        });

        return organisationList;

    }

}
