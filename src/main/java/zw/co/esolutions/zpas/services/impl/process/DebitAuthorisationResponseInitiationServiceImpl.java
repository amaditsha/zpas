package zw.co.esolutions.zpas.services.impl.process;
import java.io.StringWriter;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.iso.msg.camt036_001_05.Document;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.iso.msg.camt036_001_05.DebitAuthorisationResponseV05;
import zw.co.esolutions.zpas.model.DebitAuthorisation;
import zw.co.esolutions.zpas.repository.DebitAuthorisationRepository;
import zw.co.esolutions.zpas.services.iface.process.DebitAuthorisationResponseInitiationService;
import zw.co.esolutions.zpas.services.impl.process.builder.DebitAuthorisationResponseMessageBuilder;
import zw.co.esolutions.zpas.services.impl.process.pojo.DebitAuthorisationDTO;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

@Slf4j
@Service
public class DebitAuthorisationResponseInitiationServiceImpl implements DebitAuthorisationResponseInitiationService {
    @Autowired DebitAuthorisationResponseMessageBuilder debitAuthorisationResponseMessageBuilder;
    @Autowired DebitAuthorisationRepository debitAuthorisationRepository;
    @Autowired MessageProducer messageProducer;

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt036_001_05");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ServiceResponse initiateDebitAuthorisationResponse(DebitAuthorisationDTO debitAuthorisationDTO) {
        try {
            log.info("Handling Debit Authorisation Response Initiation");

            //build the message
            DebitAuthorisationResponseV05 debitAuthorisationResponseV05 = debitAuthorisationResponseMessageBuilder
                    .buildDebitAuthorisationResponseMsg(debitAuthorisationDTO);

            /**
             * create debit authorisation
             **/
            Optional<Payment> optionalPayment = debitAuthorisationDTO.getPaymentInvestigationCase()
                    .getUnderlyingPayment().stream().findFirst();
            Payment underlyingPayment = null;

            if(optionalPayment.isPresent()){
                underlyingPayment = optionalPayment.get();
            }

            CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();
            currencyAndAmount.setAmount(underlyingPayment.getAmount().getAmount());
            currencyAndAmount.setCurrency(underlyingPayment.getAmount().getCurrency());

            DebitAuthorisation debitAuthorisation = new DebitAuthorisation();
            debitAuthorisation.setId(GenerateKey.generateEntityId());
            debitAuthorisation.setEntityStatus(EntityStatus.ACTIVE);
            debitAuthorisation.setValueDateToDebit(debitAuthorisationDTO.getValueDateToDebit());
            debitAuthorisation.setDebitAuthorisationDecision(debitAuthorisationDTO.getAuthorisation());
            debitAuthorisation.setAmountToDebit(currencyAndAmount);
            debitAuthorisation.setReason(debitAuthorisationDTO.getReason());
            debitAuthorisation.setAuthorisedReturn(underlyingPayment);
            /*debitAuthorisation.setRelatedInvestigationCaseResolution(new PaymentInvestigationCaseResolution());*/

            debitAuthorisationRepository.save(debitAuthorisation);

            //send the message to Hub
            String destinationBic = debitAuthorisationResponseV05.getAssgnmt().getAssgne().getAgt().getFinInstnId().getBICFI();
            log.info("destination bic... " + destinationBic);
            messageProducer.sendMessageToHub(buildXml(debitAuthorisationResponseV05), destinationBic);

            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_SUCCESS)
                    .narrative("Debit Authorisation Response initiated successfully")
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .narrative("Debit Authorisation Response initiation failed : " + e.getMessage())
                    .build();
        }
    }

    private String buildXml(DebitAuthorisationResponseV05 debitAuthorisationResponseV05) throws JAXBException {
        log.info("Building camt.036 XML message..");

        Document document = new Document();
        document.setDbtAuthstnRspn(debitAuthorisationResponseV05);

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, xmlEncoding);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);

        return xml;
    }
}
