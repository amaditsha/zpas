package zw.co.esolutions.zpas.services.iface.organisations;

import zw.co.esolutions.zpas.dto.registration.EmployerDetailsDTO;
import zw.co.esolutions.zpas.dto.registration.OrganisationDTO;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.OrganisationIdentification;

import java.util.List;
import java.util.Optional;

public interface OrganisationService {
    List<Organisation> getAllOrganisations();
    Optional<OrganisationIdentification> getOrganisationIdentificationByAnyBIC(String anyBIC);
    Optional<OrganisationIdentification> getOrganisationIdentificationByBICFI(String bICFI);
    List<OrganisationDTO> searchOrganisation(EmployerDetailsDTO organisationDTO);
    List<OrganisationDTO> searchNonFIOrganisation(EmployerDetailsDTO employerDetailsDTO);
}
