package zw.co.esolutions.zpas.services.impl.taxclearance.cache;

import com.google.common.base.Preconditions;
import com.hazelcast.core.IMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.services.impl.taxclearance.TaxClearanceJsonResponse;

import javax.inject.Inject;

/**
 * Created by alfred on 21 May 2019.
 */
@Slf4j
@Component
public class TaxClearanceCacheProcessor {
    private TaxClearanceCache taxClearanceCache;

    @Inject
    public TaxClearanceCacheProcessor(TaxClearanceCache taxClearanceCache) {
        this.taxClearanceCache = taxClearanceCache;
    }

    public TaxClearanceJsonResponse getTaxClearanceJsonResponse(String bpn) {
        return getTaxClearanceJsonResponseMap().getOrDefault(bpn, cacheTaxClearanceJsonResponse(bpn));
    }

    public IMap<String, TaxClearanceJsonResponse> getTaxClearanceJsonResponseMap() {
        return taxClearanceCache.getTaxClearanceCacheInstance();
    }

    private TaxClearanceJsonResponse cacheTaxClearanceJsonResponse(String bpn) {
        TaxClearanceJsonResponse taxClearanceJson = getTaxClearanceJsonResponseMap().computeIfAbsent(bpn, key -> {
            log.info("Initialising Tax Clearance info cache -> {}", key);
            log.info("Writing to Cache");

            TaxClearanceJsonResponse taxClearanceJsonResponse = TaxClearanceJsonResponse.builder()
                    .bpn(bpn)
                    .verificationCompleted(false)
                    .companyName("")
                    .build();
            return taxClearanceJsonResponse;
        });

        Preconditions.checkNotNull(taxClearanceJson, "Failed to initialize tax clearance cache");
        return taxClearanceJson;
    }

}
