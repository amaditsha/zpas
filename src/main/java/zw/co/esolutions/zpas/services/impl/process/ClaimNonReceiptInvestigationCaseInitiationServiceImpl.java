package zw.co.esolutions.zpas.services.impl.process;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;
import zw.co.esolutions.zpas.enums.CaseStatusCode;
import zw.co.esolutions.zpas.enums.InstructionProcessingStatusCode;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.iso.msg.camt027_001_06.ClaimNonReceiptV06;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentExecutionRepository;
import zw.co.esolutions.zpas.services.iface.process.ClaimNonReceiptInvestigationCaseInitiationService;
import zw.co.esolutions.zpas.services.impl.process.builder.ClaimNonReceiptMessageBuilder;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationCaseStatusUtil;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationPartyRoleUtil;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentStatusUtil;
import zw.co.esolutions.zpas.services.impl.process.util.StatusReasonUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

@Service
@Slf4j
@Transactional
public class ClaimNonReceiptInvestigationCaseInitiationServiceImpl implements ClaimNonReceiptInvestigationCaseInitiationService {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt027_001_06");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Value("${XSD.BASE.PATH}")
    String xsdBasePath;

    @Autowired
    ClaimNonReceiptMessageBuilder claimNonReceiptMessageBuilder;

    @Autowired
    PaymentExecutionRepository paymentExecutionRepository;

    @Autowired
    PaymentStatusUtil paymentStatusUtil;

    @Autowired
    InvestigationCaseStatusUtil investigationCaseStatusUtil;

    @Autowired
    StatusReasonUtil statusReasonUtil;

    @Autowired
    InvestigationPartyRoleUtil investigationPartyRoleUtil;

    @Autowired
    MessageProducer messageProducer;

    @Override
    public ServiceResponse initiateClaimNonReceiptRequest(PaymentInvestigationCase paymentInvestigationCase) {
        try{

            log.info("Handling claim non receipt Request Initiation request");
            //build the message
            ClaimNonReceiptV06 claimNonReceiptV06 = claimNonReceiptMessageBuilder
                    .buildClaimNonReceiptRequestMsg(paymentInvestigationCase);

            //send message to hub
            String destinationBic = claimNonReceiptV06.getAssgnmt().getAssgne().getAgt().getFinInstnId().getBICFI();
            messageProducer.sendMessageToHub(buildXml(claimNonReceiptV06), destinationBic);

            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_SUCCESS)
                    .narrative("Claim non receipt Request initiated successfully")
                    .build();

        }catch (Exception e){
            e.printStackTrace();
            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .narrative("Claim non receipt Request initiation failed : " + e.getMessage())
                    .build();
        }
    }

    private String buildXml(ClaimNonReceiptV06 claimNonReceiptV06) throws JAXBException, SAXException {
        log.info("Building camt.027 XML message..");

        zw.co.esolutions.zpas.iso.msg.camt027_001_06.Document document = new zw.co.esolutions.zpas.iso.msg.camt027_001_06.Document();
        document.setClmNonRct(claimNonReceiptV06);

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, xmlEncoding);

        //Setup schema validator
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema xsdSchema = sf.newSchema(new File(xsdBasePath + "/xsd/camt/camt.027.001.06.xsd"));
        jaxbMarshaller.setSchema(xsdSchema);


        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);

        return xml;
    }

}
