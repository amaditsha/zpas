package zw.co.esolutions.zpas.services.iface.debitorder;

import zw.co.esolutions.zpas.dto.debitorder.DebitOrderBatchDTO;
import zw.co.esolutions.zpas.dto.debitorder.DebitOrderBatchUploadDTO;
import zw.co.esolutions.zpas.model.DebitOrderBatch;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;

import java.util.Optional;

public interface DebitOrdersUploadService {
        Optional<DebitOrderBatch> uploadDebitOrderBatch(DebitOrderBatchDTO debitOrderBatchDTO) throws PaymentRequestInvalidArgumentException;
}
