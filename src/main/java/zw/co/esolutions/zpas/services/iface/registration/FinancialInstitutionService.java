package zw.co.esolutions.zpas.services.iface.registration;

import zw.co.esolutions.zpas.dto.party.VirtualSuffixAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.registration.FinancialInstitutionRegistrationDTO;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.OrganisationIdentification;

import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 13 Feb 2019
 */
public interface FinancialInstitutionService {
    FinancialInstitution registerFinancialInstitution(FinancialInstitutionRegistrationDTO financialInstitutionRegistrationDTO);
    Optional<FinancialInstitution> getFinancialInstitutionById(String financialInstitutionId);
    Optional<FinancialInstitution> getFinancialInstitutionByVirtualSuffix(String virtualSuffix);
    Optional<VirtualSuffixAvailabilityCheckDTO> checkVirtualSuffixAvailability(String virtualSuffix);
    Optional<FinancialInstitution> approveFinancialInstitution(String financialInstitutionId);
    Optional<FinancialInstitution> rejectFinancialInstitution(String financialInstitutionId);
    Optional<FinancialInstitution> submitFinancialInstitution(String financialInstitutionId);
    Optional<FinancialInstitution> deleteFinancialInstitution(String financialInstitutionId);
    Optional<FinancialInstitution> getFinancialInstitutionByAnyBIC(String anyBIC);
    List<FinancialInstitution> getFinancialInstitutions();
    FinancialInstitution updateFinancialInstitution(FinancialInstitutionRegistrationDTO updateInstitutionRegistrationDTO);
    List<FinancialInstitution> getFinancialInstitutionsByClientId(String clientId);
    Optional<Organisation> getOrganisationByEmployeeId(String employeeId);
    Optional<FinancialInstitution> getFinancialInstitutionByAccountId(String accountId);
    FinancialInstitution updateFinancialInstitutionGeneralInformation(FinancialInstitution financialInstitution);
    OrganisationIdentification updateFinancialInstitutionOrganisatinIdentification(OrganisationIdentification organisationIdentification);
    String refreshOrganisationIndex();
}
