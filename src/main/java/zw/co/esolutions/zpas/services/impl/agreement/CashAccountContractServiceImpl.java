package zw.co.esolutions.zpas.services.impl.agreement;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.CashAccountContract;
import zw.co.esolutions.zpas.model.SystemStatus;
import zw.co.esolutions.zpas.repository.CashAccountContractRepository;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountContractService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CashAccountContractServiceImpl implements CashAccountContractService {

    @Autowired
    private CashAccountContractRepository cashAccountContractRepository;

    @Override
    public CashAccountContract createCashAccountContract(CashAccountContract cashAccountContract) {
        return cashAccountContractRepository.save(cashAccountContract);
    }

    @Override
    public CashAccountContract updateCashAccountContract(CashAccountContract cashAccountContract) {
        cashAccountContract.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return cashAccountContractRepository.save(cashAccountContract);
    }

    @Override
    public CashAccountContract deleteCashAccountContract(String id) {
        Optional<CashAccountContract> cashAccountContractOptional = cashAccountContractRepository.findById(id);
        CashAccountContract cashAccountContract = null;
        if (cashAccountContractOptional.isPresent()) {
            cashAccountContract = cashAccountContractOptional.get();
            cashAccountContract.setEntityStatus(EntityStatus.DELETED);
            cashAccountContract = cashAccountContractRepository.save(cashAccountContract);
        }
        return cashAccountContract;
    }

    @Override
    public Optional<CashAccountContract> getCashAccountContractById(String signatureId) {
        return cashAccountContractRepository.findById(signatureId);
    }

    @Override
    public List<CashAccountContract> getCashAccountContracts() {
        return cashAccountContractRepository.findAll();
    }
}
