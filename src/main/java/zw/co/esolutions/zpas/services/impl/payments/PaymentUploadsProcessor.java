package zw.co.esolutions.zpas.services.impl.payments;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.documents.DocumentDTO;
import zw.co.esolutions.zpas.dto.payments.CreateBulkPaymentDTO;
import zw.co.esolutions.zpas.dto.payments.PaymentUploadDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.documents.DocumentsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsAcquiringService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.impl.payments.converters.*;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.services.impl.payments.sequence.EndToEndSequenceProcessor;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.storage.StorageService;
import zw.co.esolutions.zpas.utilities.util.*;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static zw.co.esolutions.zpas.utilities.util.PaymentsUtil.*;


/**
 * Created by alfred on 26 Feb 2019
 */
@Slf4j
@Component
@Transactional
public class PaymentUploadsProcessor {
    private static final String OUTPUT_FOLDER = StorageProperties.BASE_LOCATION;


    @Autowired
    private AccountsService accountService;
    //    private final FinancialInstitutionService financialInstitutionService;

    @Autowired
    private DocumentsService documentsService;

    @Autowired
    private PaymentsAcquiringService paymentsAcquiringService;

    @Autowired
    private PaymentPartyRoleRepository paymentPartyRoleRepository;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Autowired
    private StorageService storageService;

    @Autowired
    private BulkPaymentRepository bulkPaymentRepository;

    @Autowired
    private IndividualPaymentRepository individualPaymentRepository;

    @Autowired
    private CustomPartyRepository customPartyRepository;

    @Autowired
    private PaymentIdentificationRepository paymentIdentificationRepository;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private SortCodeUtil sortCodeUtil;

    @Autowired
    private AccountRegexUtil accountRegexUtil;

    @Autowired
    private EndToEndSequenceProcessor endToEndSequenceProcessor;

    @Autowired
    Environment environment;

    public Optional<BulkPayment> createBulkPaymentFromJson(CreateBulkPaymentDTO createBulkPaymentDTO) throws PaymentRequestInvalidArgumentException {
        BulkPayment bulkPayment = createBulkPaymentFromDTOHelper(createBulkPaymentDTO);
        return savePayments(bulkPayment, bulkPayment.getGroups());
    }

    private BulkPayment createBulkPaymentFromDTOHelper(CreateBulkPaymentDTO createBulkPaymentDTO) throws PaymentRequestInvalidArgumentException {
        PaymentUploadDTO paymentUploadDTO = createBulkPaymentDTO.getPaymentUploadDTO();
        List<PaymentEntry> paymentEntries = createBulkPaymentDTO.getPayments();
        final CashAccount cashAccount = accountService.findAccountById(paymentUploadDTO.getAccountId());
        final Map<String, FinancialInstitution> bicFinancialInstitutionMap = financialInstitutionService.getFinancialInstitutions()
                .stream().collect(Collectors.toMap(FinancialInstitution::getBIC, Function.identity()));
        //additional bulk payment info

        final BulkPayment bulkPayment = new BulkPayment();
        configureBulkPaymentInformation(bulkPayment, paymentUploadDTO, cashAccount);

        setPaymentIdentifications(bulkPayment, paymentUploadDTO);
        addSourcePaymentPartyRoles(bulkPayment);

        log.info("Reading uploaded bulk payments file...");
        BigDecimal totalAmount = new BigDecimal(0);

        List<IndividualPayment> individualPayments = new ArrayList<>();
        for (PaymentEntry paymentEntry : paymentEntries) {
            IndividualPayment individualPayment = paymentEntry.getIndividualPayment();
            individualPayment.setVerifyAccountInfo(paymentUploadDTO.isVerifyAccountInfo());
            individualPayment.setStopWhenFailedVerification(paymentUploadDTO.isStopWhenFailedVerification());
            addIndividualPaymentPartyRoles(individualPayment, bulkPayment.getPartyRole(), paymentEntry, bicFinancialInstitutionMap);
            individualPayment.setBulkPayment(bulkPayment);
            individualPayments.add(individualPayment);
        }
        for (IndividualPayment individualPayment : individualPayments) {
            final BigDecimal individualPaymentAmount = individualPayment.getAmount().getAmount();
            totalAmount = totalAmount.add(individualPaymentAmount);
        }

        //final configuration on batch and bulk
        configureAmountsOnBulkPayment(cashAccount, bulkPayment, totalAmount);

        bulkPayment.setGroups(individualPayments);
        return bulkPayment;
    }

    public Optional<Payment> uploadBulkPayment(PaymentUploadDTO paymentUploadDTO) throws PaymentRequestInvalidArgumentException {
        log.info("Payment Upload DTO is: {}", paymentUploadDTO);
        final String fileName = paymentUploadDTO.getFileName();
        if (StringUtils.isEmpty(fileName)) {
            log.error("Invalid File ID specified. Failed to load uploaded bulk payment file. Upload and try again.");
            throw new PaymentRequestInvalidArgumentException("Invalid file");
        }

        final String fileExtension = getFileExtension(fileName);

        switch (fileExtension) {
            case ".csv": {
                final BulkPayment bulkPayment = readPaymentsFromCSVFile(paymentUploadDTO);
                saveDocument(paymentUploadDTO, bulkPayment.getAmount());
                return savePayments(bulkPayment, bulkPayment.getGroups()).map(b -> Optional.ofNullable((Payment) bulkPayment))
                        .orElse(Optional.empty());
            }
            case ".sfi": {
                final BulkPayment bulkPayment = readPaymentsFromSFIFile(paymentUploadDTO);
                log.info("Done creating bulk payment from SFI file");
                saveDocument(paymentUploadDTO, bulkPayment.getAmount());
                log.info("Done saving document...");
                return savePayments(bulkPayment, bulkPayment.getGroups()).map(b -> Optional.ofNullable((Payment) bulkPayment))
                        .orElse(Optional.empty());
            }
            case ".zip": {
                final Path resolve = Paths.get(OUTPUT_FOLDER).resolve(fileName);
                final String unzippedFile = unZipIt(resolve.toUri().getPath(), OUTPUT_FOLDER);
                uploadBulkPayment(paymentUploadDTO.toBuilder().fileName(unzippedFile).build());
            }
            case ".xls":
            case ".xlsx": {
                final BulkPayment bulkPayment = readPaymentsFromExcelFile(paymentUploadDTO);
                saveDocument(paymentUploadDTO, bulkPayment.getAmount());
                return savePayments(bulkPayment, bulkPayment.getGroups()).map(b -> Optional.ofNullable((Payment) bulkPayment))
                        .orElse(Optional.empty());
            }
            case ".xml": {
                final BulkPayment bulkPayment = (BulkPayment) paymentsAcquiringService.buildPaymentFromFile(fileName);
                saveDocument(paymentUploadDTO, bulkPayment.getAmount());
                return savePayments(bulkPayment, bulkPayment.getGroups()).map(b -> Optional.ofNullable((Payment) bulkPayment))
                        .orElse(Optional.empty());
            }
            default:
                throw new PaymentRequestInvalidArgumentException("Extension " + fileExtension + " not supported yet");
        }
    }

    public Optional<Payment> savePaymentInfo(BulkPayment bulkPayment) {
        return savePayments(bulkPayment, bulkPayment.getGroups()).map(b -> Optional.ofNullable((Payment) bulkPayment))
                .orElse(Optional.empty());
    }

    private void saveDocument(PaymentUploadDTO paymentUploadDTO, CurrencyAndAmount paymentAmount) {
        log.info("Saving document for the PaymentUploadDTO: {}", paymentUploadDTO);
        final String fileName = paymentUploadDTO.getFileName();

        documentsService.createDocument(DocumentDTO.builder()
                .clientId(paymentUploadDTO.getClientId())
                .copyDuplicate("")
                .dataSetType("")
                .documentName(fileName)
                .documentPath(Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName).toString())
                .issueDate(LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
                .purpose(paymentUploadDTO.getPurpose())
                .type(paymentUploadDTO.getPaymentType())
                .remittedAmount(paymentAmount)
                .build());
    }

    private Optional<BulkPayment> savePayments(BulkPayment bulkPayment, List<IndividualPayment> individualPayments) {
        setEndToEndIds(bulkPayment);

        setPaymentDetails(bulkPayment);

        //persist the bulk and its entries
        log.info("Bulk Payment is {}", bulkPayment.getEndToEndId());
        log.info("{} Individual Payments in bulk reference {}", individualPayments.size(), bulkPayment.getEndToEndId());
        bulkPayment.setGroups(new ArrayList<>());

        final List<PaymentPartyRole> bulkPartyRoles = bulkPayment.getPartyRole();
        final List<PaymentPartyRole> individualPaymentPartyRoles = individualPayments.stream()
                .flatMap(individualPayment -> individualPayment.getPartyRole().stream())
                .collect(Collectors.toList());
        final List<PaymentIdentification> bulkPaymentRelatedIdentifications = bulkPayment.getPaymentRelatedIdentifications();
        final List<PaymentIdentification> individualPaymentIdentifications = individualPayments.stream()
                .flatMap(individualPayment -> individualPayment.getPaymentRelatedIdentifications().stream())
                .collect(Collectors.toList());
        final List<PaymentStatus> individualPaymentStatuses = individualPayments.stream()
                .flatMap(individualPayment -> individualPayment.getPaymentStatus().stream())
                .collect(Collectors.toList());
        bulkPayment.setPartyRole(new ArrayList<>());
        bulkPayment.setPaymentRelatedIdentifications(new ArrayList<>());
        individualPayments.forEach(individualPayment -> {
            individualPayment.setPartyRole(new ArrayList<>());
            individualPayment.setPaymentRelatedIdentifications(new ArrayList<>());
            individualPayment.setPaymentStatus(new ArrayList<>());
        });

        final BulkPayment savedBulkPayment = bulkPaymentRepository.save(bulkPayment);
        individualPayments.forEach(individualPayment -> {
            individualPayment.setBulkPayment(savedBulkPayment);
        });
        individualPaymentRepository.saveAll(individualPayments);

        bulkPaymentRelatedIdentifications.forEach(paymentIdentification -> {
            paymentIdentification.setPayment(savedBulkPayment);
        });

        paymentIdentificationRepository.saveAll(bulkPaymentRelatedIdentifications);
        paymentIdentificationRepository.saveAll(individualPaymentIdentifications);

        paymentPartyRoleRepository.saveAll(bulkPartyRoles);
        paymentPartyRoleRepository.saveAll(individualPaymentPartyRoles);

        paymentStatusRepository.saveAll(individualPaymentStatuses);

        return Optional.ofNullable(bulkPayment);
    }

    private void setPaymentDetails(BulkPayment bulkPayment) {
        log.info("Setting payment details on bulk payment.");
        setPaymentDetailsHelper(bulkPayment);
        log.info("Setting payment details on individual payments.");
        bulkPayment.getGroups().forEach(individualPayment -> {
            setPaymentDetailsHelper(individualPayment);
            individualPayment.getPaymentDetails().setDebtorAgentBIC(bulkPayment.getPaymentDetails().getDebtorAgentBIC());
            individualPayment.getPaymentDetails().setDebtorAgentName(bulkPayment.getPaymentDetails().getDebtorAgentName());
            individualPayment.getPaymentDetails().setDebtorName(bulkPayment.getPaymentDetails().getDebtorName());
            individualPayment.getPaymentDetails().setBulkPaymentId(bulkPayment.getId());
            individualPayment.getPaymentDetails().setDebtorAccountNumber(bulkPayment.getAccount().getIdentification().getNumber());
            individualPayment.getPaymentDetails().setDebtorAccountName(bulkPayment.getAccount().getIdentification().getName());
            individualPayment.getPaymentDetails().setDebtorAccountCurrency(bulkPayment.getAccount().getBaseCurrency().getCodeName());

            individualPayment.getDebtor().ifPresent(party -> {
                if(party instanceof Organisation) {
                    individualPayment.getPaymentDetails().setOrganisationId(individualPayment.getDebtor().map(Party::getOrganisationIdentificationHelper).orElse(""));
                }
                if(party instanceof Person) {
                    individualPayment.getPaymentDetails().setPrivateId(individualPayment.getDebtor().map(Party::getPersonIdentificationHelper).orElse(""));
                }
            });
        });
    }

    private void setPaymentDetailsHelper(Payment payment) {
        final Optional<FinancialInstitution> debtorBankOptional = payment.getDebtorBank();
        final Optional<FinancialInstitution> creditorBankOptional = payment.getCreditorBank();

        final PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setId(payment.getId());
        paymentDetails.setDebtorAgentBIC(debtorBankOptional.map(FinancialInstitution::getBIC).orElse(""));
        paymentDetails.setDebtorAgentName(debtorBankOptional.map(FinancialInstitution::getName).orElse(""));
        paymentDetails.setCreditorAgentBIC(creditorBankOptional.map(FinancialInstitution::getBIC).orElse(""));
        paymentDetails.setCreditorAgentName(creditorBankOptional.map(FinancialInstitution::getName).orElse(""));
        paymentDetails.setDebtorName(payment.getDebtor().map(Party::getName).orElse(""));
        paymentDetails.setCreditorName(payment.getBeneficiaryName());
        paymentDetails.setDebtorAccountNumber(payment.getDebtorAccount());
        paymentDetails.setCreditorAccountNumber(payment.getBeneficiaryAccount());
        paymentDetails.setEndToEndId(payment.getEndToEndId());
        paymentDetails.setValueDate(payment.getValueDate());
        paymentDetails.setPaymentInstrument(payment.getPaymentInstrument());
        paymentDetails.setType(payment.getType());
        paymentDetails.setPayment(payment);
        paymentDetails.setDateCreated(OffsetDateTime.now());
        paymentDetails.setLastUpdated(OffsetDateTime.now());
        payment.setPaymentDetails(paymentDetails);

        paymentDetails.setAmount(payment.getAmount());
        paymentDetails.setPurpose(payment.getPurpose());
        paymentDetails.setPriority(payment.getPriority());
        paymentDetails.setInstructedAmount(payment.getInstructedAmount());
        paymentDetails.setInstructionForDebtorAgent(payment.getInstructionForDebtorAgent());
        paymentDetails.setInstructionForCreditorAgent(payment.getInstructionForCreditorAgent());

        payment.getDebtor().ifPresent(party -> {
            if(party instanceof Organisation) {
                paymentDetails.setOrganisationId(payment.getDebtor().map(Party::getOrganisationIdentificationHelper).orElse(""));
            }
            if(party instanceof Person) {
                paymentDetails.setPrivateId(payment.getDebtor().map(Party::getPersonIdentificationHelper).orElse(""));
            }
        });

    }

    private void setEndToEndIds(BulkPayment bulkPayment) {
        final int size = bulkPayment.getGroups().size();

        Optional<FinancialInstitution> debtorBankOptional = bulkPayment.getDebtorBank();

        String userCode = debtorBankOptional.map(financialInstitution -> environment.getProperty("ACH.USERCODE."
                + financialInstitution.getBIC())).orElse("0000");

//        final List<String> endToEndIds = endToEndSequenceProcessor.generateEndToEndIds("0000", userCode, OffsetDateTime.now(), size);

        String generateMessageId = endToEndSequenceProcessor.generateMessageId("ZEEPAYM", userCode, OffsetDateTime.now());
        List<String> endToEndIds = endToEndSequenceProcessor.generateEndToEndIdsGivenMessageId(generateMessageId, "1111", size);
        bulkPayment.getPaymentRelatedIdentifications().forEach(paymentIdentification -> {
            paymentIdentification.setExecutionIdentification(generateMessageId);
            paymentIdentification.setEndToEndIdentification(generateMessageId);
            paymentIdentification.setInstructionIdentification(generateMessageId);
            paymentIdentification.setTransactionIdentification(generateMessageId);
            paymentIdentification.setClearingSystemReference(generateMessageId);
            paymentIdentification.setIdentification(generateMessageId);
            paymentIdentification.setUniqueTradeIdentifier(generateMessageId);
        });

        int index = 0;
        for(IndividualPayment individualPayment: bulkPayment.getGroups()) {
            if(individualPayment.getPaymentRelatedIdentifications().size() > 0) {
                final PaymentIdentification paymentIdentification = individualPayment.getPaymentRelatedIdentifications().get(0);
                final String individualPaymentEndToEndId = endToEndIds.get(index++);
                paymentIdentification.setExecutionIdentification(individualPaymentEndToEndId);
                paymentIdentification.setEndToEndIdentification(individualPaymentEndToEndId);
                paymentIdentification.setInstructionIdentification(individualPaymentEndToEndId);
                paymentIdentification.setTransactionIdentification(individualPaymentEndToEndId);
                paymentIdentification.setClearingSystemReference(individualPaymentEndToEndId);
                paymentIdentification.setIdentification(individualPaymentEndToEndId);
                paymentIdentification.setUniqueTradeIdentifier(individualPaymentEndToEndId);
            }
        }
    }

    private BulkPayment readPaymentsFromSFIFile(PaymentUploadDTO paymentUploadDTO) throws PaymentRequestInvalidArgumentException {
        final CashAccount cashAccount = accountService.findAccountById(paymentUploadDTO.getAccountId());
        if (cashAccount == null) {
            throw new PaymentRequestInvalidArgumentException("Source account not found");
        }
        final BulkPayment bulkPayment = readSFIFileContents(SFIUtils.FILES_ROOT + paymentUploadDTO.getFileName(), cashAccount, paymentUploadDTO);
        bulkPayment.setType(PaymentTypeCode.valueOf(paymentUploadDTO.getPaymentType()));

        final CurrencyCode baseCurrency = cashAccount.getBaseCurrency();

        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        final Optional<BigDecimal> totalAmountOptional = bulkPayment.getGroups().stream().map(p -> p.getAmount().getAmount()).reduce((bigDecimal, bigDecimal2) -> bigDecimal.add(bigDecimal2));
        instructedCurrencyAndAmount.setAmount(totalAmountOptional.orElse(new BigDecimal(0)));
        instructedCurrencyAndAmount.setCurrency(baseCurrency);
        bulkPayment.setAmount(instructedCurrencyAndAmount);
        bulkPayment.setValueDate(OffsetDateTime.now());
        return bulkPayment;
    }

    private BulkPayment readSFIFileContents(String fileName, CashAccount cashAccount, PaymentUploadDTO paymentUploadDTO) throws PaymentRequestInvalidArgumentException {
        final SFIFileContents sfiFileContents;
        try {
            sfiFileContents = BeanUtils.parseSFIFile(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            throw new PaymentRequestInvalidArgumentException("Failed to read SFI File. Error is: " + e.getMessage());
        }

        BulkPayment bulkPayment = new BulkPayment();
        bulkPayment.setId(UUID.randomUUID().toString());
        bulkPayment.setDateCreated(OffsetDateTime.now());
        bulkPayment.setLastUpdated(OffsetDateTime.now());
        bulkPayment.setEntityStatus(EntityStatus.DRAFT);
        bulkPayment.setAccount(cashAccount);
        addSourcePaymentPartyRoles(bulkPayment);
        readBulkPaymentInformation(bulkPayment, sfiFileContents);

        List<IndividualPayment> individualPayments = readIndividualPayments(bulkPayment, sfiFileContents, cashAccount);
        bulkPayment.setGroups(individualPayments);

        setPaymentIdentifications(bulkPayment, paymentUploadDTO);
        return bulkPayment;
    }

    private BulkPayment readBulkPaymentInformation(final BulkPayment bulkPayment, SFIFileContents sfiFileContents) throws PaymentRequestInvalidArgumentException {
        final SFIHeaderRecord headerRecord = sfiFileContents.getHeaderRecord();
        final SFITrailerRecord trailerRecord = sfiFileContents.getTrailerRecord();
        bulkPayment.setHeaderID(headerRecord.getHeaderID());
        bulkPayment.setProcessingDate(headerRecord.getProcessingDate());
        bulkPayment.setSenderID(headerRecord.getSenderID());
        bulkPayment.setReceiverID(headerRecord.getReceiverID());
        bulkPayment.setFileID(headerRecord.getFileID());
        bulkPayment.setWorkCode(headerRecord.getWorkCode());
        try {
            bulkPayment.setSfiVersion(headerRecord.getSFIVersion());
        } catch (Exception e) {
            e.printStackTrace();
            throw new PaymentRequestInvalidArgumentException("Invalid SFI Version: Error is: " + e.getMessage() + ". Please use " + SFIHeaderRecord.VERSION_DEFAULT);
        }
        bulkPayment.setBatchResponseCode(headerRecord.getSenderName());
        bulkPayment.setTrailerId(trailerRecord.getTrailerId());
        CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();
        currencyAndAmount.setAmount(new BigDecimal(trailerRecord.getTotalDebitValue()));
        CurrencyCode currencyCode = new CurrencyCode();
        currencyCode.setCodeName(trailerRecord.getCurrencyCode());
        currencyAndAmount.setCurrency(currencyCode);
        bulkPayment.setAmount(currencyAndAmount);

        return bulkPayment;
    }

    private List<IndividualPayment> readIndividualPayments(BulkPayment bulkPayment, SFIFileContents sfiFileContents, CashAccount cashAccount) throws PaymentRequestInvalidArgumentException {
        List<IndividualPayment> individualPayments = new ArrayList<>();
        BigDecimal totalAmount = new BigDecimal(0);
        final Map<String, FinancialInstitution> bicFinancialInstitutionMap = financialInstitutionService.getFinancialInstitutions()
                .stream().collect(Collectors.toMap(FinancialInstitution::getBIC, Function.identity()));

        List<PaymentEntry> paymentEntries = new ArrayList<>();
        for (SFIDetailRecord sdr : sfiFileContents.getDetailRecords()) {
            final PaymentEntry paymentEntry;
            try {
                paymentEntry = PaymentEntry.builder()
                        .beneficiaryName(sdr.getDestinationName())
                        .nationalId("")
                        .sourceAccount(sdr.getSFIOriginAccount())
                        .sourceBIC(sortCodeUtil.getBIC(sdr.getOriginSortCode()).get())
                        .destinationBIC(sortCodeUtil.getBIC(sdr.getDestinationSortCode()).get())
                        .beneficiaryAccount(sdr.getDestinationAccount())
                        .amount(Double.valueOf(MoneyUtil.convertToDollars(sdr.getAmount())))
                        .paymentTypeCode("CashTransaction")
                        .valueDate(sdr.getProcessingDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                        .paymentPurpose(sdr.getNarrative())
                        .build();
                paymentEntries.add(paymentEntry);
            } catch (Exception e) {
                e.printStackTrace();
                throw new PaymentRequestInvalidArgumentException("Invalid payment information: Error is: " + e.getMessage());
            }
        }

        log.info("Validating beneficiary accounts...");
        List<PaymentEntry> invalidEntries = getInvalidAccounts(paymentEntries);
        if(!invalidEntries.isEmpty()){
            throw new PaymentRequestInvalidArgumentException("There are " + invalidEntries.size() +
                    " accounts with invalid formats. Make sure they are valid and re-upload the bulk payment. ", invalidEntries);
        }

        for(PaymentEntry paymentEntry: paymentEntries) {

            IndividualPayment individualPayment = paymentEntry.getIndividualPayment();
            final BigDecimal individualPaymentAmount = individualPayment.getAmount().getAmount();
            totalAmount = totalAmount.add(individualPaymentAmount);
            individualPayment.setBulkPayment(bulkPayment);

            final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
            instructedCurrencyAndAmount.setAmount(individualPaymentAmount);
            instructedCurrencyAndAmount.setCurrency(cashAccount.getBaseCurrency());
            individualPayment.setAmount(instructedCurrencyAndAmount);
            individualPayment.setVerifyAccountInfo(false);
            individualPayment.setStopWhenFailedVerification(false);

            individualPayment.setAccount(cashAccount);
            addSourcePaymentPartyRoles(individualPayment);
            addIndividualPaymentPartyRoles(individualPayment, bulkPayment.getPartyRole(), paymentEntry, bicFinancialInstitutionMap);
            individualPayments.add(individualPayment);


            individualPayments.add(individualPayment);
        }
        return individualPayments;
    }

    private BulkPayment readPaymentsFromExcelFile(PaymentUploadDTO paymentUploadDTO) throws PaymentRequestInvalidArgumentException {
        final CashAccount sourceCashAccount = accountService.findAccountById(paymentUploadDTO.getAccountId());
        if (sourceCashAccount == null) {
            throw new PaymentRequestInvalidArgumentException("Source account not found.");
        }
        final Map<String, FinancialInstitution> bicFinancialInstitutionMap = financialInstitutionService.getFinancialInstitutions()
                .stream().collect(Collectors.toMap(FinancialInstitution::getBIC, Function.identity()));
        //additional bulk payment info

        final BulkPayment bulkPayment = new BulkPayment();

        configureBulkPaymentInformation(bulkPayment, paymentUploadDTO, sourceCashAccount);
        setPaymentIdentifications(bulkPayment, paymentUploadDTO);

        log.info("Reading uploaded bulk payments file...");
        BigDecimal totalAmount = new BigDecimal(0);

        List<IndividualPayment> individualPayments = new ArrayList<>();
        final Map<Integer, List<String>> paymentsData = readExcelFile(paymentUploadDTO.getFileName());
        try {
            List<PaymentEntry> paymentEntries = new ArrayList<>();

            for (List<String> dataList : paymentsData.values()) {
                final PaymentEntry paymentEntry = PaymentEntry.builder()
                        .beneficiaryName(dataList.get(0))
                        .nationalId(dataList.get(1))
                        .sourceAccount(dataList.get(2))
                        .sourceBIC(dataList.get(3))
                        .destinationBIC(dataList.get(4))
                        .beneficiaryAccount(dataList.get(5))
                        .amount(Double.valueOf(dataList.get(6)))
                        .paymentTypeCode(dataList.get(7))
                        .valueDate(dataList.get(8))
                        .paymentPurpose(dataList.get(9))
                        .build();
                paymentEntries.add(paymentEntry);
            }

            log.info("Validating beneficiary accounts...");
            List<PaymentEntry> invalidEntries = getInvalidAccounts(paymentEntries);
            if(!invalidEntries.isEmpty()){
                throw new PaymentRequestInvalidArgumentException("There are " + invalidEntries.size() +
                        " accounts with invalid formats. Make sure they are valid and re-upload the bulk payment. ", invalidEntries);
            }

//            List<String> accountIbans = paymentEntries.stream().map(PaymentEntry::getSourceAccount).collect(Collectors.toList());
//            final Map<String, CashAccount> accountsMap = accountService.findAccountsByIbansIn(accountIbans);

            for (PaymentEntry paymentEntry : paymentEntries) {
                final IndividualPayment individualPayment = paymentEntry.getIndividualPayment();
                individualPayment.setVerifyAccountInfo(paymentUploadDTO.isVerifyAccountInfo());
                individualPayment.setStopWhenFailedVerification(paymentUploadDTO.isStopWhenFailedVerification());
                final BigDecimal individualPaymentAmount = individualPayment.getAmount().getAmount();
                totalAmount = totalAmount.add(individualPaymentAmount);
//                final CashAccount cashAccount = accountsMap.get(paymentEntry.getSourceAccount());
//                if (cashAccount == null) {
//                    throw new RuntimeException("Account number not found: " + paymentEntry.getSourceAccount());
//                }

                final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
                instructedCurrencyAndAmount.setAmount(individualPaymentAmount);
                instructedCurrencyAndAmount.setCurrency(sourceCashAccount.getBaseCurrency());
                individualPayment.setAmount(instructedCurrencyAndAmount);

                individualPayment.setAccount(sourceCashAccount);
                addSourcePaymentPartyRoles(individualPayment);
                addIndividualPaymentPartyRoles(individualPayment, bulkPayment.getPartyRole(), paymentEntry, bicFinancialInstitutionMap);
                individualPayments.add(individualPayment);
            }
        } catch (RuntimeException re) {
            log.error(re.getMessage());
            return null;
        }

        //final configuration on batch and bulk
        configureAmountsOnBulkPayment(sourceCashAccount, bulkPayment, totalAmount);
        bulkPayment.setAccount(sourceCashAccount);
        addSourcePaymentPartyRoles(bulkPayment);

        bulkPayment.setGroups(individualPayments);
        return bulkPayment;
    }

    private BulkPayment readPaymentsFromCSVFile(PaymentUploadDTO paymentUploadDTO) throws PaymentRequestInvalidArgumentException {
        log.info("Processing CSV bulk file");
        final CashAccount sourceCashAccount = accountService.findAccountById(paymentUploadDTO.getAccountId());
        if (sourceCashAccount == null) {
            throw new PaymentRequestInvalidArgumentException("Source account not found");
        }
        final Map<String, FinancialInstitution> bicFinancialInstitutionMap = financialInstitutionService.getFinancialInstitutions()
                .stream().collect(Collectors.toMap(FinancialInstitution::getBIC, Function.identity()));
        //additional bulk payment info

        final BulkPayment bulkPayment = new BulkPayment();

        configureBulkPaymentInformation(bulkPayment, paymentUploadDTO, sourceCashAccount);
        setPaymentIdentifications(bulkPayment, paymentUploadDTO);

        log.info("Reading uploaded bulk payments file...");
        BigDecimal totalAmount = new BigDecimal(0);

        Resource file = storageService.loadAsResource(paymentUploadDTO.getFileName());
        List<IndividualPayment> individualPayments = new ArrayList<>();
        List<PaymentEntry> paymentEntries = new ArrayList<>();
        try (Reader reader = Files.newBufferedReader(Paths.get(file.getURI()))) {
            CsvToBean<PaymentEntry> csvPaymentEntryCsvToBean = new CsvToBeanBuilder(reader)
                    .withType(PaymentEntry.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<PaymentEntry> csvPaymentEntryIterator = csvPaymentEntryCsvToBean.iterator();
            while (csvPaymentEntryIterator.hasNext()) {
                paymentEntries.add(csvPaymentEntryIterator.next());
            }
        } catch (IOException | RuntimeException re) {
            throw new PaymentRequestInvalidArgumentException("Error Reading CSV File: " + re.getMessage());
        }

        log.info("Validating beneficiary accounts...");
        List<PaymentEntry> invalidEntries = getInvalidAccounts(paymentEntries);
        if(!invalidEntries.isEmpty()){
            throw new PaymentRequestInvalidArgumentException("There are " + invalidEntries.size() +
                    " accounts with invalid formats. Make sure they are valid and re-upload the bulk payment. ", invalidEntries);
        }

        log.info("Fetching source accounts.");

        try {
            for (PaymentEntry paymentEntry : paymentEntries) {
                IndividualPayment individualPayment = paymentEntry.getIndividualPayment();
                final BigDecimal individualPaymentAmount = individualPayment.getAmount().getAmount();
                totalAmount = totalAmount.add(individualPaymentAmount);
                individualPayment.setBulkPayment(bulkPayment);

                final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
                instructedCurrencyAndAmount.setAmount(individualPaymentAmount);
                instructedCurrencyAndAmount.setCurrency(sourceCashAccount.getBaseCurrency());
                individualPayment.setAmount(instructedCurrencyAndAmount);
                individualPayment.setInstructedAmount(instructedCurrencyAndAmount);

                individualPayment.setVerifyAccountInfo(paymentUploadDTO.isVerifyAccountInfo());
                individualPayment.setStopWhenFailedVerification(paymentUploadDTO.isStopWhenFailedVerification());

                individualPayment.setAccount(sourceCashAccount);
                addSourcePaymentPartyRoles(individualPayment);
                addIndividualPaymentPartyRoles(individualPayment, bulkPayment.getPartyRole(), paymentEntry, bicFinancialInstitutionMap);
                individualPayments.add(individualPayment);
            }
        } catch (RuntimeException re) {
            log.error(re.getMessage());
            throw new PaymentRequestInvalidArgumentException("An error occurred. " + re.getMessage());
        }

        //final configuration and bulk
        configureAmountsOnBulkPayment(sourceCashAccount, bulkPayment, totalAmount);

        addSourcePaymentPartyRoles(bulkPayment);

        bulkPayment.setGroups(individualPayments);
        return bulkPayment;
    }

    private void configureBulkPaymentInformation(BulkPayment bulkPayment, PaymentUploadDTO paymentUploadDTO, CashAccount sourceCashAccount) throws PaymentRequestInvalidArgumentException {

        final String purpose = paymentUploadDTO.getPurpose();
        try {
            bulkPayment.setPurpose(ProprietaryPurposeCode.valueOfCodeName(purpose));
        } catch (RuntimeException re) {
            final String formattedString = String.format("An error occurred. Invalid bulk payment purpose code \"%s\"", purpose);
            log.error(formattedString);
            throw new PaymentRequestInvalidArgumentException(formattedString);
        }

        bulkPayment.setGroups(new ArrayList<>());
        bulkPayment.setPaymentObligation(new ArrayList<>());
        bulkPayment.setCurrencyOfTransfer(new CurrencyCode());
        bulkPayment.setCreditMethod(new ArrayList<>());
        bulkPayment.setType(PaymentTypeCode.valueOf(paymentUploadDTO.getPaymentType()));
        bulkPayment.setVerifyAccountInfo(paymentUploadDTO.isVerifyAccountInfo());
        bulkPayment.setBatchBookingEnabled(paymentUploadDTO.isBatchBookingEnabled());
        bulkPayment.setStopWhenFailedVerification(paymentUploadDTO.isStopWhenFailedVerification());

        PriorityCode priorityCode = PriorityCode.Normal;
        PaymentTypeCode paymentTypeCode = PaymentTypeCode.CashTransaction;
        PaymentInstrumentCode paymentInstrumentCode = PaymentInstrumentCode.CustomerCreditTransfer;
        try {
            paymentTypeCode = PaymentTypeCode.valueOf(paymentUploadDTO.getPaymentType());
            priorityCode = PriorityCode.valueOf(paymentUploadDTO.getPriority());
            paymentInstrumentCode = PaymentInstrumentCode.valueOf(paymentUploadDTO.getPaymentInstrument());
        } catch (RuntimeException re) {

        }
        bulkPayment.setPriority(priorityCode);
        bulkPayment.setType(paymentTypeCode);
        bulkPayment.setPaymentInstrument(paymentInstrumentCode);

        bulkPayment.setValueDate(OffsetDateTime.now());
        final PaymentStatus paymentStatus = new PaymentStatus();
        paymentStatus.setId(GenerateKey.generateEntityId());
        paymentStatus.setEntityStatus(EntityStatus.DRAFT);
        paymentStatus.setDateCreated(OffsetDateTime.now());
        bulkPayment.setPartyRole(new ArrayList<>());
        bulkPayment.setTaxOnPayment(new ArrayList<>());
        bulkPayment.setPaymentExecution(new ArrayList<>());
        bulkPayment.setPoolingAdjustmentDate(OffsetDateTime.now());

        bulkPayment.setCurrencyExchange(new ArrayList<>());
        bulkPayment.setInstructionForCreditorAgent(InstructionCode.HoldCashForCreditor);
        bulkPayment.setInstructionForDebtorAgent(InstructionCode.PayTheBeneficiary);

        bulkPayment.setStandardSettlementInstructions("");
        bulkPayment.setInvoiceReconciliation(new ArrayList<>());
        bulkPayment.setAccount(sourceCashAccount);
        bulkPayment.setId(UUID.randomUUID().toString());
        bulkPayment.setDateCreated(OffsetDateTime.now());
        bulkPayment.setLastUpdated(OffsetDateTime.now());
        bulkPayment.setEntityStatus(EntityStatus.DRAFT);
    }

    private void configureAmountsOnBulkPayment(CashAccount cashAccount, BulkPayment bulkPayment, BigDecimal totalAmount) {
        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        instructedCurrencyAndAmount.setAmount(totalAmount);
        instructedCurrencyAndAmount.setCurrency(cashAccount.getBaseCurrency());
        log.info("Bulk Amount is: {}", instructedCurrencyAndAmount);

        bulkPayment.setInstructedAmount(instructedCurrencyAndAmount);
        bulkPayment.setEquivalentAmount(instructedCurrencyAndAmount.getAmount());
        bulkPayment.setAmount(instructedCurrencyAndAmount);
    }

    private void setPaymentIdentifications(Payment payment, PaymentUploadDTO paymentUploadDTO) {
        final List<PaymentIdentification> paymentIdentifications = new ArrayList<>();
        final PaymentIdentification paymentIdentification = new PaymentIdentification();
//        final String paymentUniqueId = RefGen.getMessageReference("ZEEPAY", "AQS");
        paymentIdentification.setPayment(payment);
        paymentIdentification.setId(GenerateKey.generateEntityId());
        paymentIdentification.setDateCreated(OffsetDateTime.now());
        paymentIdentification.setLastUpdated(OffsetDateTime.now());
        paymentIdentification.setEntityStatus(EntityStatus.ACTIVE);
        paymentIdentification.setCommonIdentification(paymentUploadDTO.getClientReference());
        paymentIdentification.setMatchingReference(paymentUploadDTO.getClientReference());
        paymentIdentification.setCounterpartyReference(paymentUploadDTO.getClientReference());
        paymentIdentification.setCreditorReference(paymentUploadDTO.getClientReference());

        paymentIdentifications.add(paymentIdentification);
        payment.setPaymentRelatedIdentifications(paymentIdentifications);
    }

    private void addSourcePaymentPartyRoles(final Payment payment) {

        /**
         * Configure Bulk Payment Roles
         */
        //The affected Bulk Payments
        final List<Payment> bulkPayments = Arrays.asList(payment);
        //The affected Bulk Payment accounts
        final List<CashAccount> cashAccounts = Arrays.asList(payment.getAccount());

        final List<AccountPartyRole> accountPartyRoles = payment.getAccount().getPartyRole();

        //account servicing roles
        final List<AccountPartyRole> accountServicerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountServicerRole);
        }).collect(Collectors.toList());

        //The Role Players servicing this payment
        final List<RolePlayer> accountServicerRolePlayers = accountServicerRoles.stream()
                .flatMap(accountServicerRole -> accountServicerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //account owning roles
        final List<AccountPartyRole> accountOwnerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountOwnerRole);
        }).collect(Collectors.toList());

        //The Role Players owning this payment
        final List<RolePlayer> accountOwnerRolePlayers = accountOwnerRoles.stream()
                .flatMap(accountOwnerRole -> accountOwnerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //Debtor Role
        DebtorRole debtorRole = new DebtorRole();
        debtorRole.setPayment(bulkPayments);
        debtorRole.setCashAccount(cashAccounts);
        debtorRole.setId(GenerateKey.generateEntityId());
        debtorRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorRole.setDateCreated(OffsetDateTime.now());
        debtorRole.setLastUpdated(OffsetDateTime.now());
        debtorRole.setEntityVersion(0L);
        debtorRole.setPlayer(accountOwnerRolePlayers);
        debtorRole.setEntry(null);
        debtorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        //Debtor Agent Role
        DebtorAgentRole debtorAgentRole = new DebtorAgentRole();
        debtorAgentRole.setPayment(bulkPayments);
        debtorAgentRole.setCashAccount(cashAccounts);
        debtorAgentRole.setId(GenerateKey.generateEntityId());
        debtorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorAgentRole.setDateCreated(OffsetDateTime.now());
        debtorAgentRole.setLastUpdated(OffsetDateTime.now());
        debtorAgentRole.setEntityVersion(0L);
        debtorAgentRole.setPlayer(accountServicerRolePlayers);
        debtorAgentRole.setEntry(null);
        debtorAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Initiating Party Role
        InitiatingPartyRole initiatingPartyRole = new InitiatingPartyRole();
        initiatingPartyRole.setPayment(bulkPayments);
        initiatingPartyRole.setCashAccount(cashAccounts);
        initiatingPartyRole.setId(GenerateKey.generateEntityId());
        initiatingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
        initiatingPartyRole.setDateCreated(OffsetDateTime.now());
        initiatingPartyRole.setLastUpdated(OffsetDateTime.now());
        initiatingPartyRole.setEntityVersion(0L);
        initiatingPartyRole.setPlayer(accountOwnerRolePlayers);
        initiatingPartyRole.setEntry(null);
        initiatingPartyRole.setPartyRoleCode(PartyRoleCode.Custodian);

        //Forwarding Party Role
        ForwardingAgentRole forwardingAgentRole = new ForwardingAgentRole();
        forwardingAgentRole.setPayment(bulkPayments);
        forwardingAgentRole.setCashAccount(cashAccounts);
        forwardingAgentRole.setId(GenerateKey.generateEntityId());
        forwardingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        forwardingAgentRole.setDateCreated(OffsetDateTime.now());
        forwardingAgentRole.setLastUpdated(OffsetDateTime.now());
        forwardingAgentRole.setEntityVersion(0L);
        forwardingAgentRole.setPlayer(accountServicerRolePlayers);
        forwardingAgentRole.setEntry(null);
        forwardingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Instructing Agent Role
        InstructingAgentRole instructingAgentRole = new InstructingAgentRole();
        instructingAgentRole.setPayment(bulkPayments);
        instructingAgentRole.setCashAccount(cashAccounts);
        instructingAgentRole.setId(GenerateKey.generateEntityId());
        instructingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        instructingAgentRole.setDateCreated(OffsetDateTime.now());
        instructingAgentRole.setLastUpdated(OffsetDateTime.now());
        instructingAgentRole.setEntityVersion(0L);
        instructingAgentRole.setPlayer(accountServicerRolePlayers);
        instructingAgentRole.setEntry(null);
        instructingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        List<PaymentPartyRole> bulkPaymentPartyRoles = Arrays.asList(
                debtorRole, debtorAgentRole, initiatingPartyRole, forwardingAgentRole, instructingAgentRole
        );
        payment.setPartyRole(bulkPaymentPartyRoles);
    }

    private void addIndividualPaymentPartyRoles(final IndividualPayment individualPayment,
                                                List<PaymentPartyRole> bulkPaymentPartyRoles,
                                                PaymentEntry paymentEntry,
                                                Map<String, FinancialInstitution> bicFinancialInstitutionMap) throws PaymentRequestInvalidArgumentException {
        String destinationBIC = paymentEntry.getDestinationBIC();
        Optional<FinancialInstitution> optionalFinancialInstitution = Optional.ofNullable(bicFinancialInstitutionMap.get(destinationBIC));
        if (!optionalFinancialInstitution.isPresent()) {
            throw new PaymentRequestInvalidArgumentException("Destination Financial Institution not found for " + paymentEntry.getBeneficiaryName() + ". Please check your file.");
        }

        /**
         * Configure Individual Payment Roles
         */
        //The Payment
        final List<Payment> individualPayments = Arrays.asList(individualPayment);

        /**
         * Configure Payment Parties for individual payments in the bulk
         */
        CustomParty customParty = new CustomParty();
        customParty.setId(GenerateKey.generateEntityId());
        customParty.setDateCreated(OffsetDateTime.now());
        customParty.setLastUpdated(OffsetDateTime.now());
        customParty.setEntityStatus(EntityStatus.ACTIVE);
        customParty.setEntityVersion(0L);
        customParty.setName(paymentEntry.getBeneficiaryName());
        customParty.setNationalId(paymentEntry.getNationalId());
        customParty.setPassportNumber(paymentEntry.getPassportNumber());
        customParty.setAccountNumber(paymentEntry.getBeneficiaryAccount());
        optionalFinancialInstitution.ifPresent(financialInstitution -> {
            customParty.setFinancialInstitutionIdentification(destinationBIC);
        });

        final CustomParty savedCustomParty = customPartyRepository.save(customParty);

        //Creditor Role
        CreditorRole creditorRole = new CreditorRole();
        creditorRole.setPayment(individualPayments);
        creditorRole.setCustomParty(savedCustomParty);
        creditorRole.setHasCustomParty(true);
        creditorRole.setId(GenerateKey.generateEntityId());
        creditorRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorRole.setDateCreated(OffsetDateTime.now());
        creditorRole.setLastUpdated(OffsetDateTime.now());
        creditorRole.setEntityVersion(0L);
        creditorRole.setEntry(null);
        creditorRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Creditor Agent Role
        CreditorAgentRole creditorAgentRole = new CreditorAgentRole();
        creditorAgentRole.setPayment(individualPayments);
        optionalFinancialInstitution.ifPresent(financialInstitution -> {
            creditorAgentRole.setPlayer(Arrays.asList(financialInstitution));
        });
        creditorAgentRole.setId(GenerateKey.generateEntityId());
        creditorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorAgentRole.setDateCreated(OffsetDateTime.now());
        creditorAgentRole.setLastUpdated(OffsetDateTime.now());
        creditorAgentRole.setEntityVersion(0L);
        creditorAgentRole.setEntry(null);
        creditorAgentRole.setPartyRoleCode(PartyRoleCode.FinalAgent);

        List<PaymentPartyRole> individualPaymentPartyRoles = new ArrayList<>();
        individualPaymentPartyRoles.addAll(Arrays.asList(creditorRole, creditorAgentRole));
        individualPaymentPartyRoles.addAll(bulkPaymentPartyRoles);

        individualPayment.setPartyRole(individualPaymentPartyRoles);
    }

    public static void main(String[] args) {
        String fileName = "DebitOrderTest.xls";
        final Map<Integer, List<String>> data = readExcelFile(fileName);

        log.info("Read data from Excel file {}.", fileName);
        data.values().forEach(strings -> {
            log.info("Data: {}", strings);
        });
    }

    private List<PaymentEntry> getInvalidAccounts(List<PaymentEntry> paymentEntries){
        List<PaymentEntry> invalidEntries = new ArrayList<>();

        for (PaymentEntry paymentEntry : paymentEntries){
            String destinationBIC = paymentEntry.getDestinationBIC();
            log.info("BIC is {}", destinationBIC);
            if(accountRegexUtil.validateAccount(destinationBIC, paymentEntry.getBeneficiaryAccount()).equals(Boolean.FALSE)){
                invalidEntries.add(paymentEntry);
            } else {
                log.info("Valid account format for BIC: {}", destinationBIC);
            }
        }

        return invalidEntries;

    }
}
