package zw.co.esolutions.zpas.services.impl.audittrail;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.audittrail.LogDTO;
import zw.co.esolutions.zpas.model.audittrail.Activity;
import zw.co.esolutions.zpas.model.audittrail.AuditTrail;
import zw.co.esolutions.zpas.repository.audittrail.ActivityRepository;
import zw.co.esolutions.zpas.repository.audittrail.AuditTrailRepository;
import zw.co.esolutions.zpas.services.iface.audittrail.AuditTrailService;
import zw.co.esolutions.zpas.utilities.audit.AuditEvents;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MapUtil;

import javax.persistence.EntityManager;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Slf4j
@Service
public class AuditTrailServiceImpl implements AuditTrailService {

    @Autowired ActivityRepository activityRepository;
    @Autowired AuditTrailRepository auditTrailRepository;
    @Autowired EntityManager entityManager;

    @Override
    public AuditTrail logActivity(LogDTO logDTO) {
        AuditTrail auditTrail = null;
        LogDTO request = logDTO;
        Activity activity = activityRepository.findByName(logDTO.getActivityName());
        if(activity == null){
            activity = new Activity();
            activity.setId(GenerateKey.generateEntityId());
            activity.setName(logDTO.getActivityName());
            activity.setLogged(true);
            activity = activityRepository.save(activity);
        }

        if(activity.isLogged()){
            log.info("Activity is logged : {}", activity.getName());
            /*if(AuditEvents.RESET_MOBILE_PROFILE_PIN.equals(activity.getName())){
                auditTrail = this.logPinReset(request.getEntityId(), request.getEntityName(), activity, request.getUserName(),
                        request.getInstanceName(), request.getOldObject()).toCompletableFuture().join();
            }else{*/
                try {
                    auditTrail = this.logChanges(request.getNewObject(), request.getOldObject(), request.getEntityId(),
                            request.getEntityName(), activity, request.getUserName(), request.getInstanceName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            /*}*/
        }
        log.info("Audit Trail : {}", auditTrail);
        return (auditTrail);
    }

    @Override
    public AuditTrail logActivityWithNarrative(LogDTO logDTO) {
        AuditTrail auditTrail = null;
        Activity activity = activityRepository.findByName(logDTO.getActivityName());
        activity = activityRepository.save(activity);
        if (activity.isLogged()) {
            try {
                auditTrail = new AuditTrail();
                auditTrail.setAuditTrailId(GenerateKey.generateEntityId());
                auditTrail.setUsername(logDTO.getUserName().toUpperCase());
                auditTrail.setActivity(activity);
                auditTrail.setTime(OffsetDateTime.now());
                auditTrail.setNarrative(logDTO.getNarrative().toUpperCase());
                auditTrail = auditTrailRepository.save(auditTrail);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }
        log.info("Audit Trail : {}", auditTrail);
        return auditTrail;
    }

    @Override
    public AuditTrail logChanges(String newObject, String oldObject, String entityId, String entityName, Activity activity, String username, String instanceName) {
        AuditTrail auditTrail;
        try {
            auditTrail = new AuditTrail();
            auditTrail.setAuditTrailId(GenerateKey.generateEntityId());
            auditTrail.setUsername(username.toUpperCase());
            auditTrail.setEntityName(entityName);
            auditTrail.setActivity(activity);
            auditTrail.setEntityName(entityName);
            auditTrail.setInstanceName(instanceName);
            auditTrail.setTime(OffsetDateTime.now());
            auditTrail.setEntityId(entityId);
            if (oldObject == null) {
                //If its a case of object creation
                Map<String, String> newObjectMap = MapUtil
                        .convertAttributesStringToMap(newObject);
                String narrative = "";
                for (Object fieldName : newObjectMap.keySet()) {
                    narrative = narrative + fieldName.toString() + " set to " + newObjectMap.get(fieldName).toString() + ", ";
                }

                //System.out.println("##########"+narrative+"##########");
                auditTrail.setNarrative(narrative.toUpperCase());
            } else {
                //In the case of object update
                Map<String, String> newObjectMap = MapUtil
                        .convertAttributesStringToMap(newObject);
                Map<String, String> oldObjectMap = MapUtil
                        .convertAttributesStringToMap(oldObject);
                String narrative = "";
                for (Object fieldName : newObjectMap.keySet()) {
                    String oldPropertyValue = oldObjectMap.get(fieldName);
                    String newPropertyValue = newObjectMap.get(fieldName);
                    if (!oldPropertyValue.equalsIgnoreCase(newPropertyValue)) {
                        narrative = narrative + fieldName.toString() + " changed from "
                                + oldPropertyValue + " to " + newPropertyValue
                                + ", ";
                    }
                }
                log.debug("1 ##########" + oldObject + "##########");
                log.debug("2 ##########" + newObject + "##########");
                log.debug("3 ########## Narrative = " + narrative.length() + "##########");
                log.debug("1 ########## Entity Name = " + entityName.length() + "##########");
                log.debug("1 ########## User Name = " + username.length() + "##########");
                log.debug("1 ##########" + oldObject + "##########");
                auditTrail.setNarrative(narrative.toUpperCase());
            }
            return auditTrailRepository.save(auditTrail);
            //return auditTrail;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public List<AuditTrail> getByEntityNameAndInstanceNameAndTimePeriod(String entityName, String instanceName, String startTime, String endTime, String fiId) {
        return null;
    }

    @Override
    public List<AuditTrail> getByUsername(String userName, String fiId) {
        return null;
    }

    @Override
    public List<AuditTrail> getByUsernameAndTimePeriod(String userName, String startTime, String endTime, String fiId) {
        return null;
    }

    @Override
    public List<AuditTrail> getByActivityAndTimePeriod(String activityId, String startTime, String endTime, String fiId) {
        return null;
    }

    @Override
    public List<AuditTrail> getByEntityNameAndEntityId(String entityName, String entityId) {
        return null;
    }

    @Override
    public List<AuditTrail> getByEntityId(String entityId) {
        return null;
    }

    @Override
    public List<AuditTrail> getByTimePeriod(String startTime, String endTime, String fiId) {
        return null;
    }

    @Override
    public List<AuditTrail> searchAuditTrails(String search) {
        FullTextEntityManager fullTextEm = org.hibernate.search.jpa.Search
                .getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEm.getSearchFactory().buildQueryBuilder().forEntity(AuditTrail.class).get();

        FullTextQuery fullTextQuery = fullTextEm.createFullTextQuery(queryBuilder.keyword()
                .onFields("auditTrailId", "instanceName", "entityName", "entityId", "username")
                .matching(search)
                .createQuery(), AuditTrail.class);

        List<AuditTrail> resultList = fullTextQuery.getResultList();

        return resultList;
    }

    @Override
    public Activity editActivity() {
        return null;
    }
}
