package zw.co.esolutions.zpas.services.impl.investigationcase;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.InvestigationCase;
import zw.co.esolutions.zpas.repository.InvestigationCaseRepository;
import zw.co.esolutions.zpas.services.iface.investigationcase.InvestigationCaseService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.Optional;

@Service
@Slf4j
public class InvestigationCaseServiceImpl implements InvestigationCaseService {

    @Autowired
    private InvestigationCaseRepository investigationCaseRepository;

    @Override
    public InvestigationCase createInvestigationCase(InvestigationCase investigationCase) {
        return investigationCaseRepository.save(investigationCase);
    }

    @Override
    public InvestigationCase updateInvestigationCase(InvestigationCase investigationCase) {
        investigationCase.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        return investigationCaseRepository.save(investigationCase);
    }

    @Override
    public InvestigationCase deleteInvestigationCase(String id) {
        Optional<InvestigationCase> optionalInvestigationCase = investigationCaseRepository.findById(id);
        InvestigationCase investigationCase = null;

        if(optionalInvestigationCase.isPresent()){
            investigationCase = optionalInvestigationCase.get();
            investigationCase.setEntityStatus(EntityStatus.DELETED);
            investigationCase =investigationCaseRepository.save(investigationCase);
        }else{
            log.info("InvestigationCase has already been deleted");
        }
        return investigationCase;
    }

    @Override
    public InvestigationCase approveInvestigationCase(String id) {
        Optional<InvestigationCase> optionalInvestigationCase = investigationCaseRepository.findById(id);
        InvestigationCase investigationCase = null;
        if (optionalInvestigationCase.isPresent()){
            investigationCase = optionalInvestigationCase.get();
            if(investigationCase.getEntityStatus()!= EntityStatus.DELETED){
                investigationCase.setEntityStatus(EntityStatus.ACTIVE);
                investigationCase = investigationCaseRepository.save(investigationCase);
            }else{
                log.info("InvestigationCase has already been approved");
            }
        }
        return null;
    }

    @Override
    public InvestigationCase rejectInvestigationCase(String id) {
        Optional<InvestigationCase> optionalInvestigationCase = investigationCaseRepository.findById(id);
        InvestigationCase investigationCase = null;
        if (optionalInvestigationCase.isPresent()) {
            investigationCase = optionalInvestigationCase.get();
            if(investigationCase.getEntityStatus() != EntityStatus.DELETED) {
                investigationCase.setEntityStatus(EntityStatus.DISAPPROVED);
                investigationCase = investigationCaseRepository.save(investigationCase);
            } else {
                log.info("InvestigationCase "+ id + "has already been rejected");
            }
        }
        return investigationCase;
    }

    @Override
    public InvestigationCase findById(String id) {
        Optional<InvestigationCase> optionalInvestigationCase = this.investigationCaseRepository.findById(id);
        InvestigationCase investigationCase = null;

        if(optionalInvestigationCase.isPresent()){
            investigationCase = optionalInvestigationCase.get();
        }
        return investigationCase;
    }
}
