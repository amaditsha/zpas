package zw.co.esolutions.zpas.services.impl.merchant;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.*;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Merchant;
import zw.co.esolutions.zpas.model.OrganisationName;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MerchantSearchService {
    @Autowired
    private final EntityManager entityManager;

    public MerchantSearchService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Merchant> searchMerchant(String searchTerm){

        FullTextEntityManager fullTextEm = org.hibernate.search.jpa.Search
                .getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEm.getSearchFactory().buildQueryBuilder().forEntity(Merchant.class).get();

        FullTextQuery fullTextQuery = fullTextEm.createFullTextQuery(queryBuilder.keyword()
                .onFields("name", "code", "accountNumber", "bank")
                .matching(searchTerm)
                .createQuery(), Merchant.class);

        log.info("fulltextquery: " + fullTextQuery);
        List<Merchant> resultList = fullTextQuery.getResultList();


        log.info("result list: {}", resultList);
        return resultList;
    }
}
