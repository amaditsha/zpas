package zw.co.esolutions.zpas.services.iface.registration;

import zw.co.esolutions.zpas.model.PersonIdentification;

public interface ClientDAO {
    PersonIdentification getActivePerson(String identityCardNumber);

}
