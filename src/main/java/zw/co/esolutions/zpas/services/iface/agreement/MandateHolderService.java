package zw.co.esolutions.zpas.services.iface.agreement;

import zw.co.esolutions.zpas.dto.agreement.MandateHolderInfo;
import zw.co.esolutions.zpas.model.MandateHolder;
import zw.co.esolutions.zpas.model.RolePlayer;

import java.util.List;

public interface MandateHolderService {
    List<RolePlayer> getMandateHolderRoles(String partyId);
    void deleteMandateHolder(String id);
    MandateHolder saveMandateHolder(MandateHolderInfo mandateHolderInfo);
}
