package zw.co.esolutions.zpas.services.iface.enversaudit;

import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.enversaudit.AuditedRevisionEntity;
import zw.co.esolutions.zpas.model.enversaudit.UserRoleAuditResult;
import zw.co.esolutions.zpas.security.model.UserRole;

import java.util.List;

@Service
public interface AuditUserRoleService {
    List<UserRoleAuditResult> auditUserRoleRevisions(String userRoleId);
    List<AuditedRevisionEntity> auditRevisions(String userRoleId);
}
