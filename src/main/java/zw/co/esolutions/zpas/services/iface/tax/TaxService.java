package zw.co.esolutions.zpas.services.iface.tax;

import zw.co.esolutions.zpas.model.Tax;

import java.util.Optional;

public interface TaxService {

    Tax createTax(Tax tax);

    Tax deleteTax(String id);

    Tax findTaxById(String id);

    Tax updateTax(Tax tax);

    Optional<Tax> getTaxById(String taxId);
}
