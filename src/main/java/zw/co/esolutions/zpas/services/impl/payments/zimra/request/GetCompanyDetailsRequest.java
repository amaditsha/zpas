package zw.co.esolutions.zpas.services.impl.payments.zimra.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by alfred on 21 August 2019
 */
public class GetCompanyDetailsRequest {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "bpNumber")
    private String bpNumber;


    // Getter Methods

    public String getBpNumber() {
        return bpNumber;
    }

    public String getXmlns() {
        return xmlns;
    }

    // Setter Methods

    public void setBpNumber(String bpNumber) {
        this.bpNumber = bpNumber;
    }

    public void set_xmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}