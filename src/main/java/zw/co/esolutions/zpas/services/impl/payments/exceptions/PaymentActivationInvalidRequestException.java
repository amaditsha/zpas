package zw.co.esolutions.zpas.services.impl.payments.exceptions;

/**
 * Created by alfred on 29 Apr 2019
 */
public class PaymentActivationInvalidRequestException extends Exception {
    public PaymentActivationInvalidRequestException(String message) {
        super(message);
    }
}
