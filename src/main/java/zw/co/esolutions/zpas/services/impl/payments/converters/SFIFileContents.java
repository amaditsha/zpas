package zw.co.esolutions.zpas.services.impl.payments.converters;
/**
 * 
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;


/**
 * @author Gibson Mukarakate
 *
 */
public class SFIFileContents {
	private SFIHeaderRecord headerRecord;
	private Collection<SFIDetailRecord> detailRecords;
	private SFITrailerRecord trailerRecord;
	private String fileName;

	
	
	/**
	 * @param headerRecord
	 * @param detailRecords
	 * @param trailerRecord
	 */
	public SFIFileContents(SFIHeaderRecord headerRecord, Collection<SFIDetailRecord> detailRecords, SFITrailerRecord trailerRecord) {
		super();
		this.headerRecord = headerRecord;
		this.detailRecords = detailRecords;
		this.trailerRecord = trailerRecord;
	}

	
	public Collection<SFIDetailRecord> getDetailRecords() {
		return detailRecords;
	}


	public void setDetailRecords(Collection<SFIDetailRecord> detailRecords) {
		this.detailRecords = detailRecords;
	}


	/**
	 * @return the headerRecord
	 */
	public SFIHeaderRecord getHeaderRecord() {
		return headerRecord;
	}
	/**
	 * @param headerRecord the headerRecord to set
	 */
	public void setHeaderRecord(SFIHeaderRecord headerRecord) {
		this.headerRecord = headerRecord;
	}
	/**
	 * @return the trailerRecord
	 */
	public SFITrailerRecord getTrailerRecord() {
		return trailerRecord;
	}
	/**
	 * @param trailerRecord the trailerRecord to set
	 */
	public void setTrailerRecord(SFITrailerRecord trailerRecord) {
		this.trailerRecord = trailerRecord;
	}
	/**
	 * Resets the trailer record of this object. It basically recalculates the trailer details and
	 *  recreates the trailer record.
	 *
	 */
	public void resetTrailerRecord() throws Exception {
		// collect the trailer info & create the trailer record
		 Iterator iter = this.getDetailRecords().iterator();
		 SFIDetailRecord sdr;
		 int drCount = 0;
		 int crCount = 0;
		 double drTotal = 0;
		 double crTotal = 0;
		 while (iter.hasNext()) {
			 sdr = (SFIDetailRecord)iter.next();
			 if (sdr.isDebit()) {
				 drCount ++;
				 drTotal += sdr.getAmount();
			 }
			 else {
				 crCount ++;
				 crTotal += sdr.getAmount();
			 }
		 }
		 SFITrailerRecord trailer = new SFITrailerRecord(SFITrailerRecord.TRAILERID_DEFAULT, drTotal, crTotal, drCount, crCount,"USD");
		 // now reset the trailer record
		 this.setTrailerRecord(trailer);
	}
	/**
	 * Splits the detail records of this SFIFileContents object by the destination bank sort code. It returns
	 *  a collection of SFIFileContents that have been split.
	 * @throws Exception
	 */
	public Collection splitByBankSortCode() throws Exception {
		// create a map of (sortCode, col_of_detail_records)
		Iterator iter = this.getDetailRecords().iterator();
		HashMap map = new HashMap();
		SFIDetailRecord sdr;
		Collection bankCol;
		String sortCode;
		while (iter.hasNext()) {
			sdr = (SFIDetailRecord)iter.next();
			sortCode = sdr.getDestinationSortCode().trim();
			bankCol = (Collection)map.get(sortCode);
			if (bankCol == null) {
				// we haven't got a collection for this sort code yet. Create a new collection, insert
				//   the detail record in that col & put the collection into the map
				bankCol = new ArrayList();
				bankCol.add(sdr);
				map.put(sortCode, bankCol);
			}
			else {
				bankCol.add(sdr);
			}
		}
		// we have all the collections in the map now. just put them into the result col
		Collection resultCol = new ArrayList();
		iter = map.keySet().iterator();
		SFIHeaderRecord parentHeader = this.getHeaderRecord();
		SFIFileContents sfc;
		SFIHeaderRecord childHeader;
		while (iter.hasNext()) {
			sortCode = (String)iter.next();
			bankCol = (Collection)map.get(sortCode);
			// Create a header for each bank
			childHeader = (SFIHeaderRecord)parentHeader.cloneIt();
			// set file ID
			childHeader.setFileID(""+BeanUtils.getFileID());
			// set the reciver id to the destination bank
			childHeader.setReceiverID(sortCode);
			
			// now create the SFIFileContents object for each bank & add it to the result col
			sfc = new SFIFileContents(childHeader, bankCol, null);
			resultCol.add(sfc);
		}
		
		return resultCol;
	}
	/**
	 * Validates the integrity of this SFIFileContents object.
	 *
	 */
	public Object[] validate() throws Exception {
		// must have the trailer record
		SFITrailerRecord trailer = this.getTrailerRecord();
		if (trailer == null)
			throw new Exception("This object must have a trailer record before it is validated.");
		// collect the trailer info
		 Iterator iter = this.getDetailRecords().iterator();
		 SFIDetailRecord sdr;
		 int drCount = 0;
		 int crCount = 0;
		 double drTotal = 0;
		 double crTotal = 0;
		 while (iter.hasNext()) {
			 sdr = (SFIDetailRecord)iter.next();
			 if (sdr.isDebit()) {
				 drCount ++;
				 drTotal += sdr.getAmount();
			 }
			 else {
				 crCount ++;
				 crTotal += sdr.getAmount();
			 }
		 }
		 System.out.println("dr count = "+drCount+ " -trailer dr count ="+trailer.getDebitItemsCount() );
		 System.out.println("cr count = "+crCount+ " -trailer cr count ="+trailer.getCreditItemsCount() );
		 // now check validity
		 boolean valid = true;
		 String reason = "OK";
/*		 if (drCount != trailer.getDebitItemsCount()) {
			 valid = false;
			 reason = "Debit Items count does not tally";
		 }
		 else if (crCount != trailer.getCreditItemsCount()) {
			 valid = false;
			 reason = "Credit Items count does not tally";
		 }
		 else if (drTotal != trailer.getTotalDebitValue()) {
			 valid = false;
			 reason = "Debit Items total value does not tally";
		 }
		 else if (crTotal != trailer.getTotalCreditValue()) {
			 valid = false;
			 reason = "Credit Items total value does not tally";
		 }
	*/	 
		 return new Object[] {new Boolean(valid), reason};
	}

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
