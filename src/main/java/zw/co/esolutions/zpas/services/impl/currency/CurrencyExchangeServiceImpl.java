package zw.co.esolutions.zpas.services.impl.currency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Adjustment;
import zw.co.esolutions.zpas.model.CurrencyExchange;
import zw.co.esolutions.zpas.repository.AdjustmentRepository;
import zw.co.esolutions.zpas.repository.CurrencyExchangeRepository;
import zw.co.esolutions.zpas.services.iface.currency.CurrencyExchangeService;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.util.Optional;

@Service
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {
    @Autowired
    protected CurrencyExchangeRepository currencyExchangeRepository;
    @Autowired
    protected AdjustmentRepository adjustmentRepository;

    @Override
    public CurrencyExchange createCurrencyExchange(CurrencyExchange currencyExchange) {
        currencyExchange.setId(GenerateKey.generateEntityId());
        return this.currencyExchangeRepository.save(currencyExchange);
    }

    @Override
    public CurrencyExchange deleteCurrencyExchange(String id) {
        return null;
    }

    @Override
    public Optional<CurrencyExchange> findCurrencyExchangeById(String id) {
        return currencyExchangeRepository.findById(id).map(currencyExchange -> {
            final Adjustment byCurrencyExchangeId = adjustmentRepository.findByCurrencyExchangeId(id);
            currencyExchange.setAdjustment(byCurrencyExchangeId);
            return Optional.of(currencyExchange);
        }).orElseGet(() -> {
            return Optional.empty();
        });
    }

    @Override
    public CurrencyExchange updateCurrencyExchange(CurrencyExchange currencyExchange) {
        return null;
    }
}
