package zw.co.esolutions.zpas.services.impl.taxclearance;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.dto.TaxClearanceDTO;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.model.NonFinancialInstitution;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.taxclearance.TaxClearance;
import zw.co.esolutions.zpas.repository.NonFinancialInstitutionRepository;
import zw.co.esolutions.zpas.repository.OrganisationIdentificationRepository;
import zw.co.esolutions.zpas.repository.taxclearance.TaxClearanceRepository;
import zw.co.esolutions.zpas.services.iface.taxclearance.TaxClearanceService;
import zw.co.esolutions.zpas.services.impl.taxclearance.cache.TaxClearanceCacheProcessor;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.io.StringWriter;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by alfred on 22 May 2019
 */
@Slf4j
@Service
@Transactional
public class TaxClearanceServiceImpl implements TaxClearanceService {
    @Autowired
    private TaxClearanceCacheProcessor taxClearanceCacheProcessor;

    @Autowired
    private MessageProducer messageProducer;

    @Autowired
    private TaxClearanceRepository taxClearanceRepository;

    @Autowired
    private NonFinancialInstitutionRepository nonFinancialInstitutionRepository;

    @Autowired
    private OrganisationIdentificationRepository organisationIdentificationRepository;

    private final SseEmitter emitter = new SseEmitter(Long.MAX_VALUE);
    private String bpn;

    @Override
    public SseEmitter getBPNVerificationMessages(String bpn) {
        this.bpn = bpn;
        return emitter;
    }

    @Scheduled(fixedDelay = 2000)
    void timerHandler() {
        if (!StringUtils.isEmpty(bpn)) {
            TaxClearanceJsonResponse taxClearanceJsonResponse = taxClearanceCacheProcessor.getTaxClearanceJsonResponse(bpn);

            try {
                emitter.send(taxClearanceJsonResponse, MediaType.APPLICATION_JSON);
            } catch (Exception e) {
                emitter.completeWithError(e);
                bpn = null;
            }
        }
    }

    @Scheduled(cron = "${schedule.renew.taxclearances}")
    void sendUpdateTaxClearancesRequest() {
        log.info("Renewing Tax Clearances...");
        List<TaxClearanceJsonRequest> taxClearanceRenewalCandidates = getTaxClearanceRenewalCandidates();
        if(taxClearanceRenewalCandidates.isEmpty()) {
            log.info("No Tax Clearance Renewal Candidates found...skipping process");
        }
        String zimraRequestMessage = getTaxRenewalRequestJsonString(taxClearanceRenewalCandidates);
        log.info(zimraRequestMessage);
        messageProducer.sendMessageToZimra(zimraRequestMessage);
    }

    private String getTaxRenewalRequestJsonString(List<TaxClearanceJsonRequest> taxClearanceJsonRequests) {
        ObjectMapper mapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(writer, taxClearanceJsonRequests);//Prettified JSON
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        final String zimraRequestMessage = writer.toString();
        return zimraRequestMessage;
    }

    @Override
    public List<TaxClearance> getTaxClearances() {
        List<NonFinancialInstitution> nonFinancialInstitutions = nonFinancialInstitutionRepository.findAll();
        if (nonFinancialInstitutions.size() == 0) return new ArrayList<>();
        return taxClearanceRepository.getCurrentTaxClearances(nonFinancialInstitutions.stream()
                .map(NonFinancialInstitution::getId).collect(Collectors.toList()));
    }

    public List<TaxClearanceJsonRequest> getTaxClearanceRenewalCandidates() {
        final List<NonFinancialInstitution> nonFinancialInstitutions = nonFinancialInstitutionRepository.findAll();
        final List<String> nonFiIds = nonFinancialInstitutions.stream().map(NonFinancialInstitution::getId).collect(Collectors.toList());
        final OffsetDateTime cutOffDate = OffsetDateTime.now().plusDays(30);

        if(nonFiIds.size() == 0) {
            log.info("No Non FIs found.");
            return new ArrayList<>();
        }

        final List<TaxClearance> taxClearanceNonRenewalCandidates = taxClearanceRepository.getTaxClearanceNonRenewalCandidates(nonFiIds, cutOffDate);
        final Map<String, NonFinancialInstitution> nonRenewalCandidates = taxClearanceNonRenewalCandidates.stream()
                .map(tc -> tc.getNonFinancialInstitution()).collect(Collectors.toMap(NonFinancialInstitution::getId, Function.identity()));
        nonFinancialInstitutions.removeIf(nonFinancialInstitution -> {
            return nonRenewalCandidates.containsKey(nonFinancialInstitution.getId());
        });
        log.info("{} Renewal candidates found.", nonFinancialInstitutions.size());
        return nonFinancialInstitutions.stream().map(nonFinancialInstitution -> TaxClearanceJsonRequest.builder()
                .bpn(nonFinancialInstitution.getBusinessPartnerNumber())
                .companyName(nonFinancialInstitution.getName())
                .build()).collect(Collectors.toList());
    }

    @Override
    public Optional<TaxClearance> getCurrentTaxClearanceByBPN(String bpn) {
        return organisationIdentificationRepository.getByBusinessPartnerNumber(bpn)
                .map(organisationIdentification -> {
                    Organisation organisation = organisationIdentification.getOrganisation();
                    return taxClearanceRepository.findAllByNonFinancialInstitutionId(organisation.getId())
                            .stream().findFirst();
                }).orElseGet(() -> taxClearanceRepository.getTaxClearanceByBpn(bpn)
                        .map(Optional::of)
                        .orElse(Optional.empty()));
    }

    @Override
    public Optional<TaxClearance> createTaxClearance(TaxClearanceDTO taxClearanceDTO) {
        TaxClearance taxClearance = new TaxClearance();
        taxClearance.setId(GenerateKey.generateEntityId());
        taxClearance.setDateCreated(OffsetDateTime.now());
        taxClearance.setLastUpdated(OffsetDateTime.now());
        taxClearance.setEntityStatus(EntityStatus.ACTIVE);
        taxClearance.setStartDate(OffsetDateTime.now());
        taxClearance.setExpiryDate(taxClearanceDTO.getExpiryDate());
        taxClearance.setRegisteredName(taxClearanceDTO.getCompanyName());
        taxClearance.setBpn(taxClearanceDTO.getBpn());
        taxClearance.setNonFinancialInstitution(taxClearanceDTO.getNonFinancialInstitution());
        TaxClearance savedTaxClearance = taxClearanceRepository.save(taxClearance);
        return Optional.ofNullable(savedTaxClearance);
    }

    @Override
    public Optional<TaxClearance> updateTaxClearance(TaxClearanceDTO taxClearanceDTO) {
        return taxClearanceRepository.findById(taxClearanceDTO.getTaxClearanceId()).map(taxClearance -> {
            taxClearance.setRegisteredName(taxClearanceDTO.getCompanyName());
            taxClearance.setExpiryDate(taxClearanceDTO.getExpiryDate());
            TaxClearance savedTaxClearance = taxClearanceRepository.save(taxClearance);
            return Optional.ofNullable(savedTaxClearance);
        }).orElse(Optional.empty());
    }

    @Override
    public List<TaxClearance> refreshTaxClearances(List<TaxClearanceJsonResponse> taxClearanceJsons) {
        //All Non FIs cache
        Map<String, NonFinancialInstitution> nonFinancialInstitutionMap = nonFinancialInstitutionRepository.findAll().stream()
                .collect(Collectors.toMap(NonFinancialInstitution::getBusinessPartnerNumber, Function.identity(), (k1, k2) -> k1));

        //New Tax Clearances
        List<TaxClearance> taxClearances = new ArrayList<>();

        if (taxClearanceJsons.size() == 1) {
            //single request, update cache
            updateTaxClearanceCache(taxClearanceJsons.get(0));
        }

        taxClearanceJsons.forEach(taxClearanceJson -> {
            NonFinancialInstitution nonFinancialInstitution = nonFinancialInstitutionMap.get(taxClearanceJson.getBpn());
            if (nonFinancialInstitution == null) {
                log.error("Organisation not found for BPN {}", taxClearanceJson.getBpn());
            }

            TaxClearance taxClearance = new TaxClearance();
            taxClearance.setId(GenerateKey.generateEntityId());
            taxClearance.setDateCreated(OffsetDateTime.now());
            taxClearance.setLastUpdated(OffsetDateTime.now());
            taxClearance.setEntityStatus(EntityStatus.ACTIVE);
            taxClearance.setStartDate(parseDate(taxClearanceJson.getStartDate()));
            taxClearance.setExpiryDate(parseDate(taxClearanceJson.getExpiryDate()));
            taxClearance.setRegisteredName(taxClearanceJson.getCompanyName());
            taxClearance.setBpn(taxClearanceJson.getBpn());
            taxClearance.setNonFinancialInstitution(nonFinancialInstitution);
            taxClearances.add(taxClearance);
        });
//        final List<TaxClearance> taxClearancesSaved = taxClearanceRepository.saveAll(taxClearances);
        final List<TaxClearance> taxClearancesSaved = new ArrayList<>();
        taxClearances.forEach(taxClearance -> {
            taxClearanceRepository.getDanglingTaxClearance(taxClearance.getBpn()).map(danglingTaxClearance -> {
                taxClearanceRepository.delete(danglingTaxClearance);
                final TaxClearance savedTaxClearance = taxClearanceRepository.save(taxClearance);
                taxClearancesSaved.add(savedTaxClearance);
                return savedTaxClearance;
            }).orElseGet(() -> {
                final TaxClearance savedTaxClearance = taxClearanceRepository.save(taxClearance);
                taxClearancesSaved.add(savedTaxClearance);
                return savedTaxClearance;
            });
        });
        return taxClearancesSaved;
    }

    private TaxClearanceJsonResponse updateTaxClearanceCache(TaxClearanceJsonResponse taxClearanceResponse) {
        return taxClearanceCacheProcessor.getTaxClearanceJsonResponseMap()
                .computeIfPresent(taxClearanceResponse.getBpn(), (key, value) -> taxClearanceResponse);
    }

    @Override
    public Optional<TaxClearanceJsonResponse> verifyTaxClearanceByBPN(String bpn) {
        return getCurrentTaxClearanceByBPN(bpn)
                .map(taxClearance -> Optional.of(
                        TaxClearanceJsonResponse.builder()
                                .companyName(taxClearance.getRegisteredName())
                                .verificationCompleted(true)
                                .exists(true)
                                .bpn(bpn)
                                .expiryDate(taxClearance.getExpiryDate().format(DATE_TIME_FORMATTER))
                                .startDate(taxClearance.getStartDate().format(DATE_TIME_FORMATTER))
                                .build()
                )).orElseGet(() -> {
                    log.info("Initialising tax clearance cache...");
                    TaxClearanceJsonResponse taxClearanceJsonResponse = taxClearanceCacheProcessor.getTaxClearanceJsonResponse(bpn);
                    log.info("No Tax clearance found in system database. Verifying tax clearance with zimra...");
                    List<TaxClearanceJsonRequest> taxClearanceJsonRequests = new ArrayList<>();
                    taxClearanceJsonRequests.add(TaxClearanceJsonRequest.builder()
                            .bpn(bpn)
                            .companyName("")
                            .build());
                    String zimraRequestMessage = getTaxRenewalRequestJsonString(taxClearanceJsonRequests);
                    log.info("Requesting Tax Clearance Verification from ZIMRA, {}", zimraRequestMessage);
                    messageProducer.sendMessageToZimra(zimraRequestMessage);
                    return Optional.empty();
                });
    }

    private OffsetDateTime parseDate(String date) {
        try {
            LocalDate localDate = LocalDate.parse(date, DATE_TIME_FORMATTER);
            return localDate.atTime(OffsetTime.now());
        } catch (RuntimeException re) {
            return OffsetDateTime.now();
        }
    }
}
