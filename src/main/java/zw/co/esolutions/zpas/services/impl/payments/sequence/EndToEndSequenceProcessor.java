package zw.co.esolutions.zpas.services.impl.payments.sequence;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.services.impl.payments.cache.GenerationNumberCacheProcessor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by alfred on 05 July 2019
 */
@Slf4j
@Component
public class EndToEndSequenceProcessor {

    //Date/UserCode/CustomerCode/GenerationNumber/SequenceNumber
    //e.g. [YYYYMMDD/0-9{4,4}/0-9A-Z{4,4}/0-9{7,7}/0-9{7,7}]

    @Autowired
    private GenerationNumberCacheProcessor generationNumberCacheProcessor;

    public List<String> generateEndToEndIds(String customerCode, String userCode, OffsetDateTime sequenceDate, int numberOfEntries) {
        final BigDecimal generationNumber = generationNumberCacheProcessor.getGenerationNumber(customerCode, userCode, sequenceDate);
        final String date = OffsetDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        List<String> ids = new ArrayList<>();
        IntStream.range(1, numberOfEntries + 1).forEach(value -> {
            final String endToEndIdFormat = String.format("%s/%s/%s/%s/%s", date, userCode, customerCode,
                    formatGenerationNumber(generationNumber.longValue()), formatSequenceNumber(value));
            ids.add(endToEndIdFormat);
        });
        return ids;
    }

    //Date/UserCode/CustomerCode/GenerationNumber/SequenceNumber
    //e.g. [YYYYMMDD/0-9{4,4}/0-9A-Z{4,4}/0-9{7,7}/0-9{7,7}]

    public String generateEndToEndId(String customerCode, String userCode, OffsetDateTime sequenceDate) {
        final BigDecimal generationNumber = generationNumberCacheProcessor.getGenerationNumber(customerCode, userCode, sequenceDate);
        final String date = OffsetDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        final String endToEndIdFormat = String.format("%s/%s/%s/%s/%s", date, userCode, customerCode,
                formatGenerationNumber(generationNumber.longValue()), formatSequenceNumber(1));
        return endToEndIdFormat;
    }

    /**
     * Date/ServiceIdentificationCode/UserCode/GenerationNumber
     * e.g. YYYYMMDD/IBCRPAY/0-9{4,4}/0-9{7,7}
     * @param serviceId
     * @param userCode
     * @param sequenceDate
     * @return String
     */
    public String generateMessageId(String serviceId, String userCode, OffsetDateTime sequenceDate) {
        final BigDecimal generationNumber = generationNumberCacheProcessor.getGenerationNumber(serviceId, userCode, sequenceDate);
        final String date = OffsetDateTime.now().format(GenerationNumberCacheProcessor.FORMATTER);

         return String.format("%s/%s/%s/%s", date, serviceId, userCode, formatGenerationNumber(generationNumber.longValue()));
    }

    public List<String> generateEndToEndIdsGivenMessageId(String messageId, String customerCode, int numberOfEntries) {
        String[] messageParts = messageId.split("/");
        if(messageParts.length != 4) {
            return new ArrayList<>();
        }

        final String date = messageParts[0];
        final String serviceIdCode = messageParts[1];
        final String userCode = messageParts[2];
        final String generationNumber = messageParts[3];
        List<String> ids = new ArrayList<>();
        IntStream.range(1, numberOfEntries + 1).forEach(value -> {
            final String endToEndIdFormat = String.format("%s/%s/%s/%s/%s", date, userCode, customerCode,
                    generationNumber, formatSequenceNumber(value));
            ids.add(endToEndIdFormat);
        });
        return ids;
    }

    public String generateEndToEndId(String customerCode, String userCode, long generationNumber, OffsetDateTime sequenceDate, int sequenceNumber) {
        final String sequenceDateFormat = GenerationNumberCacheProcessor.FORMATTER.format(sequenceDate);
        return String.format("%s/%s/%s/%s/%s", sequenceDateFormat, userCode, customerCode,
                formatGenerationNumber(generationNumber), formatSequenceNumber(sequenceNumber));
    }

    private String formatSequenceNumber(int value) {
        return formatGenerationNumber((long) value);
    }

    private String formatGenerationNumber(Long generationNumber) {
        final String formatedValue = String.format("%d", generationNumber);
        return StringUtils.leftPad(formatedValue, 7, "0");
    }
}
