package zw.co.esolutions.zpas.services.iface.signature;

import zw.co.esolutions.zpas.dto.agreement.ApprovalRequestDTO;
import zw.co.esolutions.zpas.dto.agreement.ApprovalResponseDTO;

public interface ApprovalProcessorService {
    ApprovalResponseDTO approveReq(ApprovalRequestDTO approvalRequestDTO);
}
