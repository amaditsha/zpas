package zw.co.esolutions.zpas.services.impl.payments.zimra.request;

import lombok.Builder;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alfred on 21 August 2019
 */
@Builder
@XmlRootElement(name = "ProcessPayment")
public class ProcessPaymentRequest {
    @XmlElement(name = "PaymentAdvice")
    private PaymentAdvice paymentAdvice;

    @XmlAttribute(name = "xmlns")
    private String xmlns;

    public PaymentAdvice getPaymentAdvice() {
        return paymentAdvice;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setPaymentAdvice(PaymentAdvice paymentAdvice) {
        this.paymentAdvice = paymentAdvice;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    @Override
    public String toString() {
        return "ProcessPaymentRequest{" +
                "paymentAdvice=" + paymentAdvice +
                ", xmlns='" + xmlns + '\'' +
                '}';
    }
}
