package zw.co.esolutions.zpas.services.impl.payments;
import java.time.OffsetDateTime;
import zw.co.esolutions.zpas.enums.PaymentTypeCode;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.enums.PaymentInstrumentCode;

import com.opencsv.bean.CsvBindByName;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.PaymentCategory;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.lang.System;
import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alfred on 25 Nov 2018
 */
@Data
@Builder
@Slf4j
public class PaymentEntry {

    @CsvBindByName(column = "PaymentTypeCode", required = true)
    private String paymentTypeCode;

    @CsvBindByName(column = "CurrencyCodeName")
    private String currencyCodeName;

    @CsvBindByName(column = "CurrencyCode")
    private String currencyCodeString;

    @CsvBindByName(column = "PaymentCategory")
    private String paymentCategory;

    @CsvBindByName(column = "NationalId", required = true)
    private String nationalId;

    @CsvBindByName(column = "PassportNumber")
    private String passportNumber;

    @CsvBindByName(column = "Amount", required = true)
    private Double amount;

    @CsvBindByName(column = "ClientId")
    private String clientId;

    @CsvBindByName(column = "PaymentPurpose")
    private String paymentPurpose;

    @CsvBindByName(column = "CorrelationId")
    private String correlationId;

    @CsvBindByName(column = "BeneficiaryAccount", required = true)
    private String beneficiaryAccount;

    @CsvBindByName(column = "BeneficiaryName", required = true)
    private String beneficiaryName;

    @CsvBindByName(column = "SourceAccount")
    private String sourceAccount;

    @CsvBindByName(column = "SourceAccountName")
    private String sourceAccountName;

    @CsvBindByName(column = "SourceBIC")
    private String sourceBIC;

    @CsvBindByName(column = "DestinationBIC", required = true)
    private String destinationBIC;

    @CsvBindByName(column = "ProcessingCode")
    private String processingCode;

    @CsvBindByName(column = "TransmissionDate")
    private String transmissionDate;

    @CsvBindByName(column = "ValueDate")
    private String valueDate;

    @CsvBindByName(column = "SourceReference")
    private String sourceReference;

    @CsvBindByName(column = "OriginalReference")
    private String originalReference;

    @CsvBindByName(column = "Narrative")
    private String narrative;

    @CsvBindByName(column = "BatchRef")
    private String batchRef;

    @CsvBindByName(column = "BulkPaymentRef")
    private String bulkPaymentRef;

    @CsvBindByName(column = "BulkPaymentId")
    private String bulkPaymentId;

    @CsvBindByName(column = "BatchId")
    private String batchId;

    public PaymentEntry() {
    }

    public PaymentEntry(String paymentTypeCode, String currencyCodeName, String currencyCodeString,
                        String paymentCategory, String nationalId, String passportNumber, Double amount,
                        String clientId, String paymentPurpose, String correlationId, String beneficiaryAccount,
                        String beneficiaryName, String sourceAccount, String sourceAccountName, String sourceBIC,
                        String destinationBIC, String processingCode, String transmissionDate, String valueDate,
                        String sourceReference, String originalReference, String narrative, String batchRef,
                        String bulkPaymentRef, String bulkPaymentId, String batchId) {
        this.paymentTypeCode = paymentTypeCode;
        this.currencyCodeName = currencyCodeName;
        this.currencyCodeString = currencyCodeString;
        this.paymentCategory = paymentCategory;
        this.nationalId = nationalId;
        this.passportNumber = passportNumber;
        this.amount = amount;
        this.clientId = clientId;
        this.paymentPurpose = paymentPurpose;
        this.correlationId = correlationId;
        this.beneficiaryAccount = beneficiaryAccount;
        this.beneficiaryName = beneficiaryName;
        this.sourceAccount = sourceAccount;
        this.sourceAccountName = sourceAccountName;
        this.sourceBIC = sourceBIC;
        this.destinationBIC = destinationBIC;
        this.processingCode = processingCode;
        this.transmissionDate = transmissionDate;
        this.valueDate = valueDate;
        this.sourceReference = sourceReference;
        this.originalReference = originalReference;
        this.narrative = narrative;
        this.batchRef = batchRef;
        this.bulkPaymentRef = bulkPaymentRef;
        this.bulkPaymentId = bulkPaymentId;
        this.batchId = batchId;
    }

    public IndividualPayment getIndividualPayment() throws PaymentRequestInvalidArgumentException {

        PaymentTypeCode paymentTypeCode = null;
        PaymentCategory paymentCategory = null;
        OffsetDateTime transmissionDate = null;
        OffsetDateTime valueDate = null;

        try {
            paymentTypeCode = PaymentTypeCode.valueOf(this.paymentTypeCode);
            if (this.paymentCategory != null) {
                paymentCategory = PaymentCategory.valueOf(this.paymentCategory);
            }

            if (this.transmissionDate != null) {
                final LocalDate localDate = LocalDate.parse(this.transmissionDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                transmissionDate = LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
        }

        try {
            if (this.valueDate != null) {
                final LocalDate localDate = LocalDate.parse(this.valueDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                valueDate = LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
            }
        } catch (RuntimeException re) {
            re.printStackTrace();
            valueDate = OffsetDateTime.now();
        }

        IndividualPayment payment = new CreditTransfer();
        payment.setPaymentObligation(new ArrayList<>());
        payment.setCurrencyOfTransfer(new CurrencyCode());
        payment.setCreditMethod(new ArrayList<>());
        payment.setType(paymentTypeCode);

        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        instructedCurrencyAndAmount.setAmount(new BigDecimal(amount));
        CurrencyCode currencyCode = new CurrencyCode();
        currencyCode.setCodeName(currencyCodeName);
        currencyCode.setName(currencyCodeName);
        instructedCurrencyAndAmount.setCurrency(currencyCode);

        try {
            payment.setPurpose(ProprietaryPurposeCode.valueOfCodeName(paymentPurpose));
        } catch (RuntimeException re) {
            final String formattedString = String.format("An error occurred. Invalid payment purpose code \"%s\"", paymentPurpose);
            log.error(formattedString);
            throw new PaymentRequestInvalidArgumentException(formattedString);
        }
        payment.setInstructedAmount(instructedCurrencyAndAmount);
        payment.setPriority(PriorityCode.Normal);
        payment.setValueDate(valueDate);
        payment.setPartyRole(Arrays.asList(new InitiatingPartyRole()));
        payment.setTaxOnPayment(new ArrayList<>());
        payment.setPaymentExecution(new ArrayList<>());
        payment.setPoolingAdjustmentDate(OffsetDateTime.now());
        payment.setEquivalentAmount(instructedCurrencyAndAmount.getAmount());
        payment.setInstructionForCreditorAgent(InstructionCode.HoldCashForCreditor);
        payment.setInstructionForDebtorAgent(InstructionCode.PayTheBeneficiary);
        payment.setPaymentRelatedIdentifications(new ArrayList<>());
        payment.setAmount(instructedCurrencyAndAmount);
        payment.setStandardSettlementInstructions("");
        payment.setInvoiceReconciliation(new ArrayList<>());
        payment.setPaymentInstrument(PaymentInstrumentCode.CustomerDebitTransfer);
        payment.setId(UUID.randomUUID().toString());
        payment.setDateCreated(OffsetDateTime.now());
        payment.setLastUpdated(OffsetDateTime.now());
        payment.setEntityStatus(EntityStatus.DRAFT);

        List<PaymentIdentification> paymentIdentifications = new ArrayList<>();
        final String identification = RefGen.getMessageReference("ZP", "AQS");
        PaymentIdentification paymentIdentification = new PaymentIdentification();
        paymentIdentification.setExecutionIdentification(identification);
        paymentIdentification.setEndToEndIdentification(identification);
        paymentIdentification.setInstructionIdentification(identification);
        paymentIdentification.setTransactionIdentification(identification);
        paymentIdentification.setClearingSystemReference(GenerateKey.generateRef());
        paymentIdentification.setCreditorReference(GenerateKey.generateRef());
        paymentIdentification.setPayment(payment);
        paymentIdentification.setId(GenerateKey.generateEntityId());
        paymentIdentification.setDateCreated(OffsetDateTime.now());
        paymentIdentification.setLastUpdated(OffsetDateTime.now());
        paymentIdentification.setEntityStatus(EntityStatus.ACTIVE);
        paymentIdentification.setCounterpartyReference(GenerateKey.generateRef());
        paymentIdentification.setIdentification(identification);
        paymentIdentification.setCommonIdentification(identification);
        paymentIdentification.setMatchingReference(GenerateKey.generateRef());
        paymentIdentification.setUniqueTradeIdentifier(identification);
        paymentIdentifications.add(paymentIdentification);
        payment.setPaymentRelatedIdentifications(paymentIdentifications);

        List<PaymentStatus> paymentStatuses = new ArrayList<>();
        PaymentStatus paymentStatus = new PaymentStatus();
        paymentStatus.setId(UUID.randomUUID().toString());
        paymentStatus.setDateCreated(OffsetDateTime.now());
        paymentStatus.setLastUpdated(OffsetDateTime.now());
        paymentStatus.setPayment(payment);
        paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
        paymentStatus.setStatus(PaymentStatusCode.Pending);
        paymentStatus.setStatusDescription(PaymentStatusCode.Pending.name());
        paymentStatuses.add(paymentStatus);
        payment.setPaymentStatus(paymentStatuses);

        return payment;
    }
}
