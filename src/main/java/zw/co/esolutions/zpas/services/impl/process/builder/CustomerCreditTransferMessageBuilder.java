package zw.co.esolutions.zpas.services.impl.process.builder;

import zw.co.esolutions.zpas.enums.PaymentPurposeCode;
import zw.co.esolutions.zpas.iso.msg.pain001_001_08.ActiveOrHistoricCurrencyAndAmount;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.codeset.MMCode;
import zw.co.esolutions.zpas.iso.msg.pain001_001_08.*;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.services.impl.process.pojo.PaymentInfoDto;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentInfoUtil;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentPartyRoleUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CustomerCreditTransferMessageBuilder {

    @Autowired
    PaymentPartyRoleUtil paymentPartyRoleUtil;

    @Autowired
    PaymentInfoUtil paymentInfoUtil;

    public CustomerCreditTransferInitiationV08 buildCustomerCreditTransferInitiationMsg(Payment payment) {

        log.info("Building pain.001 message..");

        CustomerCreditTransferInitiationV08 customerCreditTransferInitiationV08 = new CustomerCreditTransferInitiationV08();

        //Get the payment info and summary
//        PaymentInfoDto paymentInfoDto = paymentInfoUtil.getPaymentInfo(payment);
        final List<PaymentDetails> paymentDetailsList = paymentInfoUtil.getPaymentDetailsList(payment);

        int numberOfPayments = paymentDetailsList.size();

        final BigDecimal controlSum = paymentDetailsList.stream()
                .map(individualPayment -> individualPayment.getAmount().getAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        /**
         * Build the GroupHeader
         */

        log.info("Building the Group Header..");

        GroupHeader48 groupHeader48 = new GroupHeader48();
        groupHeader48.setMsgId(payment.getEndToEndId());
        groupHeader48.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));
        groupHeader48.setNbOfTxs(String.valueOf(numberOfPayments));
        groupHeader48.setCtrlSum(controlSum);

        /**
         * Set the Initiating Party
         */

        log.info("Setting the Initiating Party..");

        groupHeader48.setInitgPty(paymentPartyRoleUtil.getPartyIdentification32(payment, InitiatingPartyRole.class));

        /**
         * Set the Forwarding Agent
         */

        groupHeader48.setFwdgAgt(paymentPartyRoleUtil.getBranchAndFinancialInstitutionIdentification5(payment, ForwardingAgentRole.class));

        /**
         * Set the GroupHeader
         */
        customerCreditTransferInitiationV08.setGrpHdr(groupHeader48);

        /**
         * Set the Global Payment Info
         */

        //Group PaymentInstruction22s by requested execution date
        Map<LocalDate, List<PaymentDetails>> paymentDetailsGroupedByValueDate =
                paymentDetailsList.stream().collect(Collectors.groupingBy(pd -> {
                    OffsetDateTime valueDateOffsetDateTime = pd.getPayment().getValueDate();
                    if(valueDateOffsetDateTime != null) {
                        return valueDateOffsetDateTime.toLocalDate();
                    } else {
                        return LocalDate.now();
                    }
                }));

        List<PaymentInstruction22> paymentInstruction22List = new ArrayList<>();

        paymentDetailsGroupedByValueDate.entrySet().forEach(paymentDetailsEntry -> {
            final LocalDate paymentValueDate = paymentDetailsEntry.getKey();
            final List<PaymentDetails> paymentDetails = paymentDetailsEntry.getValue();

            int numberOfPaymentsPerValueDate = paymentDetails.size();

            final BigDecimal controlSumPerValueDate = paymentDetails.stream()
                    .map(individualPayment -> individualPayment.getAmount().getAmount())
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            PaymentInstruction22 paymentInstruction22 = new PaymentInstruction22();
            paymentInstruction22.setPmtInfId(RefGen.getMessageReference("ZEEPAY", "AQS"));
            paymentInstruction22.setPmtMtd(PaymentMethod3Code.TRF);
            paymentInstruction22.setBtchBookg(false);
            paymentInstruction22.setNbOfTxs(String.valueOf(numberOfPaymentsPerValueDate));
            paymentInstruction22.setCtrlSum(controlSumPerValueDate);

            PaymentTypeInformation19 paymentTypeInformation19 = new PaymentTypeInformation19();
            paymentTypeInformation19.setInstrPrty(Priority2Code.valueOf(payment.getPriority().getCodeName()));
//        paymentTypeInformation19.setLclInstrm(new LocalInstrument2Choice());
//        final CategoryPurpose1Choice categoryPurpose1Choice = new CategoryPurpose1Choice();
//        categoryPurpose1Choice.setCd("TAXS");
//        categoryPurpose1Choice.setPrtry("TAXS");
//        paymentTypeInformation19.setCtgyPurp(categoryPurpose1Choice);

            paymentInstruction22.setPmtTpInf(paymentTypeInformation19);

            DateAndDateTimeChoice dateAndDateTimeChoice = new DateAndDateTimeChoice();
            dateAndDateTimeChoice.setDt(XmlDateUtil.getXmlDateNoTime(new Date(paymentValueDate.atTime(OffsetTime.now().withHour(12)).toEpochSecond() * 1000L)));
//        dateAndDateTimeChoice.setDtTm(XmlDateUtil.getXmlDate(new Date(payment.getValueDate().toEpochSecond() * 1000L)));

            paymentInstruction22.setReqdExctnDt(dateAndDateTimeChoice);
            paymentInstruction22.setPoolgAdjstmntDt(XmlDateUtil.getXmlDate(Optional.ofNullable(payment.getPoolingAdjustmentDate()).orElse(payment.getValueDate())));

            /**
             * Set Debtor Information
             */

            //Set Debtor

            paymentInstruction22.setDbtr(paymentPartyRoleUtil.getPartyIdentification32(payment, DebtorRole.class));

            //Set Debtor Account
            paymentInstruction22.setDbtrAcct(this.getCashAccount(payment.getAccount()));

            //Set Batch Booking
            paymentInstruction22.setBtchBookg(payment.isBatchBookingEnabled());

            /**
             * Set the Debtor Agent
             */

            paymentInstruction22.setDbtrAgt(paymentPartyRoleUtil.getBranchAndFinancialInstitutionIdentification5(payment, DebtorAgentRole.class));

            //No need to set debtor agent account
//        paymentInstruction22.setDbtrAgtAcct(new CashAccount24());

            //Ultimate debtor is the same with debtor

            //paymentInstruction22.setUltmtDbtr();

            paymentInstruction22.setChrgBr(ChargeBearerType1Code.DEBT);
//        paymentInstruction22.setChrgsAcct(new CashAccount24());
//        paymentInstruction22.setChrgsAcctAgt(new BranchAndFinancialInstitutionIdentification5());

            /**
             * Set the Credit Transfer Transactions
             */

//        List<CreditTransferTransaction26> creditTransferTransaction26List = this.buildCreditTransferTransactions(paymentInfoDto.individualPayments);
            List<CreditTransferTransaction26> creditTransferTransaction26List = this.buildCreditTransferTransactionsFromPaymentDetails(paymentDetails);

            paymentInstruction22.getCdtTrfTxInf().addAll(creditTransferTransaction26List);

            paymentInstruction22List.add(paymentInstruction22);
        });

        customerCreditTransferInitiationV08.getPmtInf().addAll(paymentInstruction22List);

        /**
         * Set Supplementary Data
         */

        return customerCreditTransferInitiationV08;
    }

    private List<CreditTransferTransaction26> buildCreditTransferTransactionsFromPaymentDetails(List<PaymentDetails> paymentDetailsList) {

        List<CreditTransferTransaction26> creditTransferTransaction26List = new ArrayList<>();

        for (PaymentDetails paymentDetails : paymentDetailsList) {
            if (paymentDetails.getPayment().getEntityStatus() == EntityStatus.DISAPPROVED) {
//                log.info("Ignoring payment with reference {}, it has been rejected already", paymentDetails1.getEndToEndId());
                continue;
            }

            CreditTransferTransaction26 creditTransferTransaction26 = new CreditTransferTransaction26();

            PaymentIdentification1 paymentIdentification1 = new PaymentIdentification1();
            paymentIdentification1.setInstrId(paymentDetails.getEndToEndId());
            paymentIdentification1.setEndToEndId(paymentDetails.getEndToEndId());

            creditTransferTransaction26.setPmtId(paymentIdentification1);

            PaymentTypeInformation19 creditTraPaymentTypeInformation191 = new PaymentTypeInformation19();
            creditTraPaymentTypeInformation191.setInstrPrty(Priority2Code.valueOf(paymentDetails.getPriority().getCodeName()));

            creditTransferTransaction26.setPmtTpInf(creditTraPaymentTypeInformation191);

            //Set amount
            final String currencyOfTransfer = Optional.ofNullable(
                    paymentDetails.getAmount())
                    .map(currencyAndAmount -> Optional.ofNullable(currencyAndAmount.getCurrency())
                            .map(MMCode::getCodeName)
                            .orElse(""))
                    .orElse("");
            ActiveOrHistoricCurrencyAndAmount instructedAmount = new ActiveOrHistoricCurrencyAndAmount();
            instructedAmount.setValue(paymentDetails.getInstructedAmount().getAmount());
            instructedAmount.setCcy(currencyOfTransfer);

            AmountType4Choice amountType4Choice = new AmountType4Choice();
            amountType4Choice.setInstdAmt(instructedAmount);
            final ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount = new ActiveOrHistoricCurrencyAndAmount();
            activeOrHistoricCurrencyAndAmount.setCcy(currencyOfTransfer);
            activeOrHistoricCurrencyAndAmount.setValue(paymentDetails.getInstructedAmount().getAmount());

            creditTransferTransaction26.setAmt(amountType4Choice);

            creditTransferTransaction26.setXchgRateInf(new ExchangeRate1());

            creditTransferTransaction26.setChrgBr(ChargeBearerType1Code.DEBT);

            creditTransferTransaction26.setInstrForDbtrAgt(paymentDetails.getInstructionForDebtorAgent().getCodeName());

            /**
             * Set Creditor Agent Information
             */

            //Set Creditor Agent Information
            creditTransferTransaction26.setCdtrAgt(paymentPartyRoleUtil.getCreditorAgentFromPaymentDetails(paymentDetails));

            /**
             * Set Creditor Information
             */

            creditTransferTransaction26.setCdtr(paymentPartyRoleUtil.getCreditorFromPaymentDetails(paymentDetails));

            creditTransferTransaction26.setCdtrAcct(this.getCashAccountFromPaymentDetails(paymentDetails));

            creditTransferTransaction26.setUltmtCdtr(paymentPartyRoleUtil.getCreditorFromPaymentDetails(paymentDetails));

            //Instructions
            List<InstructionForCreditorAgent1> instructionForCreditorAgent1s = new ArrayList<>();

            InstructionForCreditorAgent1 instructionForCreditorAgent1 = new InstructionForCreditorAgent1();
            instructionForCreditorAgent1.setCd(Instruction3Code.valueOf(paymentDetails.getInstructionForCreditorAgent().getCodeName()));
//            instructionForCreditorAgent1.setInstrInf("");

            instructionForCreditorAgent1s.add(instructionForCreditorAgent1);

            creditTransferTransaction26.getInstrForCdtrAgt().add(instructionForCreditorAgent1);
            creditTransferTransaction26.setInstrForDbtrAgt(paymentDetails.getInstructionForDebtorAgent().getCodeName());

            //Purpose
            Purpose2Choice purpose2Choice = new Purpose2Choice();
            purpose2Choice.setPrtry(Optional.ofNullable(paymentDetails.getPurpose()).map(p -> p.getCodeName()).orElse(PaymentPurposeCode.Rent.getCodeName()));
//
            creditTransferTransaction26.setPurp(purpose2Choice);

            //Other Information
            creditTransferTransaction26.getRgltryRptg().addAll(new ArrayList<>());
            creditTransferTransaction26.setTax(new TaxInformation3());
            creditTransferTransaction26.getRltdRmtInf().addAll(new ArrayList<>());

            final RemittanceInformation11 remittanceInformation11 = new RemittanceInformation11();
            final List<String> unstructuredRemittanceInformation = paymentDetails.getUnstructuredRemittanceInformation();
            remittanceInformation11.getUstrd().addAll(unstructuredRemittanceInformation);
            creditTransferTransaction26.setRmtInf(remittanceInformation11);

            creditTransferTransaction26List.add(creditTransferTransaction26);
        }

        return creditTransferTransaction26List;
    }

    private List<CreditTransferTransaction26> buildCreditTransferTransactions(List<IndividualPayment> individualPayments) {

        List<CreditTransferTransaction26> creditTransferTransaction26List = new ArrayList<>();

        for (IndividualPayment individualPayment : individualPayments) {
            if (individualPayment.getEntityStatus() == EntityStatus.DISAPPROVED) {
//                log.info("Ignoring payment with reference {}, it has been rejected already", individualPayment.getEndToEndId());
                continue;
            }

            CreditTransfer creditTransfer = (CreditTransfer) individualPayment;

            CreditTransferTransaction26 creditTransferTransaction26 = new CreditTransferTransaction26();

            PaymentIdentification1 paymentIdentification1 = new PaymentIdentification1();
            paymentIdentification1.setInstrId(creditTransfer.getEndToEndId());
            paymentIdentification1.setEndToEndId(creditTransfer.getEndToEndId());

            creditTransferTransaction26.setPmtId(paymentIdentification1);

            PaymentTypeInformation19 creditTraPaymentTypeInformation191 = new PaymentTypeInformation19();
            creditTraPaymentTypeInformation191.setInstrPrty(Priority2Code.valueOf(creditTransfer.getPriority().getCodeName()));
//            creditTraPaymentTypeInformation191.setSvcLvl(new ServiceLevel8Choice());
//            creditTraPaymentTypeInformation191.setLclInstrm(new LocalInstrument2Choice());
//            creditTraPaymentTypeInformation191.setCtgyPurp(new CategoryPurpose1Choice());

            creditTransferTransaction26.setPmtTpInf(creditTraPaymentTypeInformation191);

            //Set amount
            final String currencyOfTransfer = Optional.ofNullable(
                    creditTransfer.getAmount())
                    .map(currencyAndAmount -> Optional.ofNullable(currencyAndAmount.getCurrency())
                            .map(MMCode::getCodeName)
                            .orElse(""))
                    .orElse("");
            ActiveOrHistoricCurrencyAndAmount instructedAmount = new ActiveOrHistoricCurrencyAndAmount();
            instructedAmount.setValue(creditTransfer.getInstructedAmount().getAmount());
            instructedAmount.setCcy(currencyOfTransfer);

            AmountType4Choice amountType4Choice = new AmountType4Choice();
            amountType4Choice.setInstdAmt(instructedAmount);
//            final EquivalentAmount2 equivalentAmount2 = new EquivalentAmount2();
            final ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount = new ActiveOrHistoricCurrencyAndAmount();
            activeOrHistoricCurrencyAndAmount.setCcy(currencyOfTransfer);
            activeOrHistoricCurrencyAndAmount.setValue(creditTransfer.getInstructedAmount().getAmount());
//            equivalentAmount2.setAmt(activeOrHistoricCurrencyAndAmount);
//            equivalentAmount2.setCcyOfTrf(currencyOfTransfer);

//            amountType4Choice.setEqvtAmt(equivalentAmount2);

            creditTransferTransaction26.setAmt(amountType4Choice);

            creditTransferTransaction26.setXchgRateInf(new ExchangeRate1());

            creditTransferTransaction26.setChrgBr(ChargeBearerType1Code.DEBT);
//            creditTransferTransaction26.setChqInstr(new Cheque7());

            creditTransferTransaction26.setInstrForDbtrAgt(individualPayment.getInstructionForDebtorAgent().getCodeName());

            //creditTransferTransaction26.setUltmtDbtr(paymentPartyRoleUtil.getPartyIdentification43(payment, UltimateDebtorRole.class));

            //Set Intermediary Information
//            creditTransferTransaction26.setIntrmyAgt1(new BranchAndFinancialInstitutionIdentification5());
//            creditTransferTransaction26.setIntrmyAgt1Acct(new CashAccount24());
//            creditTransferTransaction26.setIntrmyAgt2(new BranchAndFinancialInstitutionIdentification5());
//            creditTransferTransaction26.setIntrmyAgt2Acct(new CashAccount24());
//            creditTransferTransaction26.setIntrmyAgt3(new BranchAndFinancialInstitutionIdentification5());
//            creditTransferTransaction26.setIntrmyAgt3Acct(new CashAccount24());

            /**
             * Set Creditor Agent Information
             */

            //Set Creditor Agent Information
            creditTransferTransaction26.setCdtrAgt(paymentPartyRoleUtil.getBranchAndFinancialInstitutionIdentification5(creditTransfer, CreditorAgentRole.class));

            //No need to set creditor agent account
//            creditTransferTransaction26.setCdtrAgtAcct(new CashAccount24());

            /**
             * Set Creditor Information
             */

            creditTransferTransaction26.setCdtr(paymentPartyRoleUtil.getPartyIdentification32(creditTransfer, CreditorRole.class));

            Optional<PaymentPartyRole> creditorRoleOptional = paymentPartyRoleUtil.getPaymentPartyRole(creditTransfer, CreditorRole.class);

            creditorRoleOptional.ifPresent(paymentPartyRole -> {
//                log.info("Party role: {}", paymentPartyRole);
                Optional<CashAccount> cashAccountOptional = paymentPartyRole.getCashAccount().stream().findFirst();

                cashAccountOptional.map(cashAccount -> {
                    //Set Creditor Account
                    creditTransferTransaction26.setCdtrAcct(this.getCashAccount(cashAccount));
                    return null;
                }).orElseGet(() -> {
                    creditTransferTransaction26.setCdtrAcct(this.getCashAccountFromCustomParty((CreditorRole) paymentPartyRole));
                    return null;
                });
            });

            creditTransferTransaction26.setUltmtCdtr(paymentPartyRoleUtil.getPartyIdentification32(creditTransfer, UltimateCreditorRole.class));

            //Instructions
            List<InstructionForCreditorAgent1> instructionForCreditorAgent1s = new ArrayList<>();

            InstructionForCreditorAgent1 instructionForCreditorAgent1 = new InstructionForCreditorAgent1();
            instructionForCreditorAgent1.setCd(Instruction3Code.valueOf(creditTransfer.getInstructionForCreditorAgent().getCodeName()));
//            instructionForCreditorAgent1.setInstrInf("");

            instructionForCreditorAgent1s.add(instructionForCreditorAgent1);

            creditTransferTransaction26.getInstrForCdtrAgt().add(instructionForCreditorAgent1);
            creditTransferTransaction26.setInstrForDbtrAgt(creditTransfer.getInstructionForDebtorAgent().getCodeName());

            //Purpose
            Purpose2Choice purpose2Choice = new Purpose2Choice();
//            purpose2Choice.setCd(Optional.ofNullable(individualPayment.getPurpose()).map(p -> p.getCodeName()).orElse(PaymentPurposeCode.Rent.getCodeName()));
            purpose2Choice.setPrtry(Optional.ofNullable(individualPayment.getPurpose()).map(p -> p.getCodeName()).orElse(PaymentPurposeCode.Rent.getCodeName()));
//
            creditTransferTransaction26.setPurp(purpose2Choice);

            //Other Information
            creditTransferTransaction26.getRgltryRptg().addAll(new ArrayList<>());
            creditTransferTransaction26.setTax(new TaxInformation3());
            creditTransferTransaction26.getRltdRmtInf().addAll(new ArrayList<>());

            final RemittanceInformation11 remittanceInformation11 = new RemittanceInformation11();
            final List<String> unstructuredRemittanceInformation = individualPayment.getUnstructuredRemittanceInformation();
            remittanceInformation11.getUstrd().addAll(unstructuredRemittanceInformation);
            creditTransferTransaction26.setRmtInf(remittanceInformation11);

            creditTransferTransaction26List.add(creditTransferTransaction26);
        }

        return creditTransferTransaction26List;
    }

    private CashAccount24 getCashAccountFromPaymentDetails(PaymentDetails paymentDetails) {
//        String cccountIBAN = cashAccount.getIdentification().getIBAN();
        final String cashAccountNumber = paymentDetails.getCreditorAccountNumber();
        String accountCcy = paymentDetails.getDebtorAccountCurrency();
        String accountName = paymentDetails.getCreditorName();

        CashAccount24 cashAccount24 = new CashAccount24();

        AccountIdentification4Choice creditorAccountIdentification4Choice = new AccountIdentification4Choice();
        final GenericAccountIdentification1 genericAccountIdentification1 = new GenericAccountIdentification1();
        genericAccountIdentification1.setId(cashAccountNumber);
        creditorAccountIdentification4Choice.setOthr(genericAccountIdentification1);

        cashAccount24.setId(creditorAccountIdentification4Choice);
        cashAccount24.setCcy(accountCcy);
        cashAccount24.setNm(accountName);

        return cashAccount24;
    }

    private CashAccount24 getCashAccount(CashAccount cashAccount) {
//        String cccountIBAN = cashAccount.getIdentification().getIBAN();
        final String cashAccountNumber = cashAccount.getIdentification().getNumber();
        String accountCcy = cashAccount.getBaseCurrency().getCodeName();
        String accountName = cashAccount.getIdentification().getName();

        CashAccount24 cashAccount24 = new CashAccount24();

        AccountIdentification4Choice creditorAccountIdentification4Choice = new AccountIdentification4Choice();
//        creditorAccountIdentification4Choice.setIBAN(cccountIBAN);
        final GenericAccountIdentification1 genericAccountIdentification1 = new GenericAccountIdentification1();
        genericAccountIdentification1.setId(cashAccountNumber);
        creditorAccountIdentification4Choice.setOthr(genericAccountIdentification1);

        cashAccount24.setId(creditorAccountIdentification4Choice);
//        cashAccount24.setTp(new CashAccountType2Choice());
        cashAccount24.setCcy(accountCcy);
        cashAccount24.setNm(accountName);

        return cashAccount24;
    }

    private CashAccount24 getCashAccountFromCustomParty(CreditorRole creditorRole) {
        final CustomParty customParty = creditorRole.getCustomParty();
        String cashAccountNumber = "";
        String accountName = "";
//        String cccountIBAN = "";
        String accountCcy = "";

        if (creditorRole.isHasCustomParty() && customParty != null) {
            cashAccountNumber = customParty.getAccountNumber();
            accountName = customParty.getName();
        }

        CashAccount24 cashAccount24 = new CashAccount24();

        AccountIdentification4Choice creditorAccountIdentification4Choice = new AccountIdentification4Choice();
//        creditorAccountIdentification4Choice.setIBAN(cccountIBAN);
        final GenericAccountIdentification1 genericAccountIdentification1 = new GenericAccountIdentification1();
        genericAccountIdentification1.setId(cashAccountNumber);
        creditorAccountIdentification4Choice.setOthr(genericAccountIdentification1);

        cashAccount24.setId(creditorAccountIdentification4Choice);
//        cashAccount24.setTp(new CashAccountType2Choice());
//        cashAccount24.setCcy(accountCcy);
        cashAccount24.setNm(accountName);

        return cashAccount24;
    }
}