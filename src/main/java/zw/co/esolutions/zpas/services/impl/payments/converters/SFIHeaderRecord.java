package zw.co.esolutions.zpas.services.impl.payments.converters;


import java.time.OffsetDateTime;
import java.util.Date;

/**
 * 
 * @author Gibson Mukarakate
 *
 */
public class SFIHeaderRecord {
	private String headerID;				// XxX = UHL
	private OffsetDateTime processingDate;	// 9 (8)
	private String senderID;				// X (9)
	private String receiverID;				// X (9) or 9 (9)?
	private String fileID;					// 9 (6)
	private String workCode;				// X (15)
	private String version;					// 999 : 002 fro SFI v2
	private String senderName;
	
	public static final int HEADERID_FIELD_LENGTH = 3;
	public static final int PROCESSINGDATE_FIELD_LENGTH = 8;
	public static final int SENDERID_FIELD_LENGTH = 9;
	public static final int RECEIVERID_FIELD_LENGTH = 9;
	public static final int FILEID_FIELD_LENGTH = 6;
	public static final int WORKCODE_FIELD_LENGTH = 15;
	public static final int VERSION_FIELD_LENGTH = 3;
	public static final String HEADERID_DEFAULT = "UHL";
	public static final String VERSION_DEFAULT = "004";
	public static final int SENDER_NAME_FIELD_LENGTH = 30;

	public SFIHeaderRecord() {
	}
	
	public SFIHeaderRecord(String headerID, OffsetDateTime processingDate, String senderID, String receiverID, String fileID, String workCode, String version, String senderName) throws Exception {
		super();
        this.senderName = senderName;
        this.setHeaderID(headerID);
		this.setProcessingDate(processingDate);
		this.setSenderID(senderID);
		this.setReceiverID(receiverID);
		this.setFileID(fileID);
		this.setWorkCode(workCode);
		this.setVersion(version);
	}

	public Object cloneIt() throws Exception {
		SFIHeaderRecord myClone = new SFIHeaderRecord(this.headerID, this.processingDate, this.senderID, this.receiverID, this.fileID, this.workCode, this.version, this.senderName);
		return myClone;
	}
	
	/**
	 * @return the fileID
	 */
	public String getFileID() {
		return fileID;
	}
	/**
	 * @return the headerID
	 */
	public String getHeaderID() {
		return headerID;
	}
	/**
	 * @return the processingDate
	 */
	public OffsetDateTime getProcessingDate() {
		return processingDate;
	}
	/**
	 * @return the receiverID
	 */
	public String getReceiverID() {
		return receiverID;
	}
	/**
	 * @return the senderID
	 */
	public String getSenderID() {
		return senderID;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @return the workCode
	 */
	public String getWorkCode() {
		return workCode;
	}

	public String getSenderName() {
		return senderName;
	}
	/**
	 * @param fileID the fileID to set
	 */
	public void setFileID(String fileID) throws Exception {
		// must a number with of 6 digits max
		try {
			if (Integer.parseInt(fileID) > 999999)
				throw new Exception();
		}
		catch (Exception e) {
			throw new Exception("Invalid file ID. It must a number of 6 digits maximum.");
		}
		fileID = fileID.trim().toUpperCase();
		this.fileID = fileID;
	}
	/**
	 * @param headerID the headerID to set
	 */
	public void setHeaderID(String headerID) throws Exception {
		if (!SFIHeaderRecord.HEADERID_DEFAULT.equalsIgnoreCase(headerID))
			throw new Exception("Invalid header ID. It must be '" + SFIHeaderRecord.HEADERID_DEFAULT + "'.");
		headerID = headerID.toUpperCase().trim();
		this.headerID = headerID;
	}
	/**
	 * @param processingDate the processingDate to set
	 */
	public void setProcessingDate(OffsetDateTime processingDate) {
		this.processingDate = processingDate;
	}
	/**
	 * @param receiverID the receiverID to set
	 */
	public void setReceiverID(String receiverID) throws Exception {
		if (receiverID != null)
			receiverID = receiverID.toUpperCase().trim();
		this.receiverID = receiverID;
	}
	/**
	 * @param senderID the senderID to set
	 */
	public void setSenderID(String senderID) {
		if (senderID != null)
			senderID = senderID.toUpperCase().trim();
		this.senderID = senderID;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @param workCode the workCode to set
	 */
	public void setWorkCode(String workCode) {
		if (workCode != null)
			workCode = workCode.toUpperCase().trim();
		this.workCode = workCode;
	}
	/**
	 * @param senderName the senderName to set
	 */
	public void setSenderName(String senderName) {
		if (senderName != null)
			senderName = senderName.toUpperCase().trim();
		this.workCode = senderName;
	}

	public String getSFIFileID() throws Exception {
		return BeanUtils.leftPad(this.getFileID(), FILEID_FIELD_LENGTH, '0');
	}
	public String getSFIHeaderID() throws Exception {
		return BeanUtils.rightPad(this.getHeaderID(), HEADERID_FIELD_LENGTH, ' ');
	}
	public String getSFIProcessingDate() throws Exception {
		String tmp = (this.getProcessingDate()==null)? "": Formats.SFIDateFormat.format(this.getProcessingDate());
		return BeanUtils.leftPad(tmp, PROCESSINGDATE_FIELD_LENGTH, ' ');
	}
	public String getSFIReceiverID() throws Exception {
		return BeanUtils.rightPad(this.getReceiverID(), RECEIVERID_FIELD_LENGTH, ' ');
	}
	public String getSFISenderID() throws Exception {
		return BeanUtils.rightPad(this.getSenderID(), SENDERID_FIELD_LENGTH, ' ');
	}
	public String getSFIVersion() throws Exception {
		return BeanUtils.leftPad(this.getVersion(), VERSION_FIELD_LENGTH, '0');
	}
	public String getSFIWorkCode() throws Exception {
		return BeanUtils.rightPad(this.getWorkCode(), WORKCODE_FIELD_LENGTH, ' ');
	}
	public String getSFISenderName() throws Exception {
		return BeanUtils.rightPad(this.getSenderName(), SENDER_NAME_FIELD_LENGTH, ' ');
	}

}
