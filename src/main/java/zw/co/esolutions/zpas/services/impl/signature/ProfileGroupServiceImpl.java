package zw.co.esolutions.zpas.services.impl.signature;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.ProfileGroup;
import zw.co.esolutions.zpas.repository.ProfileGroupRepository;
import zw.co.esolutions.zpas.services.iface.signature.ProfileGroupService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ProfileGroupServiceImpl implements ProfileGroupService {

    @Autowired
    private ProfileGroupRepository profileGroupRepository;

    @Override
    public ProfileGroup createProfileGroup(ProfileGroup profileGroup) {
        return profileGroupRepository.save(profileGroup);
    }

    @Override
    public ProfileGroup updateProfileGroup(ProfileGroup profileGroup) {
        profileGroup.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return profileGroupRepository.save(profileGroup);
    }

    @Override
    public ProfileGroup deleteProfileGroup(String id) {
        Optional<ProfileGroup> profileGroupOptional = profileGroupRepository.findById(id);
        ProfileGroup profileGroup = null;
        if (profileGroupOptional.isPresent()) {
            profileGroup = profileGroupOptional.get();
            profileGroup.setEntityStatus(EntityStatus.DELETED);
            profileGroup = profileGroupRepository.save(profileGroup);
        }
        return profileGroup;
    }


    @Override
    public ProfileGroup approveProfileGroup(String id) {
        Optional<ProfileGroup> optionalProfileGroup = profileGroupRepository.findById(id);
        ProfileGroup profileGroup = null;
        if (optionalProfileGroup.isPresent()) {
            profileGroup = optionalProfileGroup.get();
            if(profileGroup.getEntityStatus() != EntityStatus.DELETED) {
                profileGroup.setEntityStatus(EntityStatus.ACTIVE);
                profileGroup = profileGroupRepository.save(profileGroup);
            } else {
                log.info("The profile group status has already been deleted.");
            }
        }
        return profileGroup;
    }

    @Override
    public ProfileGroup rejectProfileGroup(String id) {
        Optional<ProfileGroup> optionalProfileGroup = profileGroupRepository.findById(id);
        ProfileGroup profileGroup = null;
        if (optionalProfileGroup.isPresent()) {
            profileGroup = optionalProfileGroup.get();
            if(profileGroup.getEntityStatus() != EntityStatus.DELETED) {
                profileGroup.setEntityStatus(EntityStatus.DISAPPROVED);
                profileGroup = profileGroupRepository.save(profileGroup);
            } else {
                log.info("The profile group status has already been deleted.");
            }
        }
        return profileGroup;
    }

    @Override
    public Optional<ProfileGroup> getProfileGroupById(String signatureId) {
        return profileGroupRepository.findById(signatureId);
    }

    @Override
    public List<ProfileGroup> getProfileGroups() {
        return profileGroupRepository.findAll();
    }
}
