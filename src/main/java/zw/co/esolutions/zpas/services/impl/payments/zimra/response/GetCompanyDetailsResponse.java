package zw.co.esolutions.zpas.services.impl.payments.zimra.response;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by alfred on 21 August 2019
 */
public class GetCompanyDetailsResponse {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "bpNumber")
    private String bpNumber;

    @XmlElement(name = "taxPayerName")
    private String taxPayerName;

    public String getBpNumber() {
        return bpNumber;
    }

    public String getTaxPayerName() {
        return taxPayerName;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setBpNumber(String bpNumber) {
        this.bpNumber = bpNumber;
    }

    public void setTaxPayerName(String taxPayerName) {
        this.taxPayerName = taxPayerName;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}
