package zw.co.esolutions.zpas.services.impl.payments;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.PaymentObligation;
import zw.co.esolutions.zpas.services.iface.payments.PaymentActivationRequestsService;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.storage.StorageService;

/**
 * Created by alfred on 24 Apr 2019
 */
@Slf4j
@Service
public class PaymentActivationRequestsServiceImpl implements PaymentActivationRequestsService {
    private static final String FILE_FOLDER = StorageProperties.BASE_LOCATION;

    @Autowired
    private StorageService storageService;

    @Override
    public String receivePaymentActivationRequestMessage(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            log.error("Invalid File ID specified. Failed to load uploaded bulk payment file. Upload and try again.");
            return null;
        }

        final String fileExtension = getFileExtension(fileName);
        return null;
    }

    @Override
    public String sendPaymentActivationRequestStatusReport(PaymentObligation paymentObligation) {
        return null;
    }

    private static String getFileExtension(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            return "";
        }
        if (!fileName.contains(".")) {
            return "";
        }
        final String fileExtension = fileName.substring(fileName.lastIndexOf('.'));
        return fileExtension;
    }
}
