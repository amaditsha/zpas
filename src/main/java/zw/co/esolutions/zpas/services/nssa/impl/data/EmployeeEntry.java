package zw.co.esolutions.zpas.services.nssa.impl.data;


import com.opencsv.bean.CsvBindByName;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import zw.co.esolutions.zpas.dto.nssa.EmployeeDTO;

import java.util.UUID;

@Data
@Builder
@Slf4j
public class EmployeeEntry {
    @CsvBindByName(column = "FullName", required = true)
    private String firstName;

    @CsvBindByName(column = "LastName", required = true)
    private String lastName;

    @CsvBindByName(column = "NationalId")
    private String nationalId;

    @CsvBindByName(column = "SSN")
    private String ssn;

    public EmployeeEntry() {
    }

    public EmployeeEntry(String firstName, String lastName, String nationalId, String ssn) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationalId = nationalId;
        this.ssn = ssn;
    }

    public EmployeeDTO getEmployee() throws Exception{
        EmployeeDTO employee = EmployeeDTO.builder()
                .id(UUID.randomUUID().toString())
                .firstName(firstName)
                .lastName(lastName)
                .nationalId(nationalId)
                .ssn(ssn)
                .build();

        return employee;
    }

}
