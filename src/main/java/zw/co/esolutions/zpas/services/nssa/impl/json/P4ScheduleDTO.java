package zw.co.esolutions.zpas.services.nssa.impl.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Create by alfred on 23 Jul 2019
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class P4ScheduleDTO {
    private String employerSSRNumber;
    private String employerEC;
    private String employerName;
    private String employerPhysicalAddress;
    private String ic;
    private String paymentMonthYear;
    private String sector;
    private String emailAddress;
    private String contactTelephoneNo;

    private List<P4ScheduleItem> p4ScheduleItems;
}
