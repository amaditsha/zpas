package zw.co.esolutions.zpas.services.iface.process;

import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

public interface PaymentProcessService {

    ServiceResponse initiatePayment(String paymentId);
    ServiceResponse initiatePayment(Payment payment);
}
