package zw.co.esolutions.zpas.services.iface.registration;

import zw.co.esolutions.zpas.dto.registration.NonFinancialInstitutionRegistrationDTO;
import zw.co.esolutions.zpas.model.NonFinancialInstitution;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.OrganisationIdentification;

import java.util.List;
import java.util.Optional;

public interface NonFinancialInstitutionService {
    NonFinancialInstitution registerNonFinancialInstitution(NonFinancialInstitutionRegistrationDTO nonFinancialInstitutionRegistrationDTO);
    Optional<NonFinancialInstitution> getNonFinancialInstitutionById(String nonFinancialInstitutionId);
    Optional<NonFinancialInstitution> approveNonFinancialInstitution(String nonFinancialInstitutionId);
    Optional<NonFinancialInstitution> rejectNonFinancialInstitution(String nonFinancialInstitutionId);
    Optional<NonFinancialInstitution> submitNonFinancialInstitution(String nonFinancialInstitutionId);
    Optional<NonFinancialInstitution> deleteNonFinancialInstitution(String nonFinancialInstitutionId);
    Optional<NonFinancialInstitution> getNonFinancialInstitutionByAnyBIC(String nonFinancialInstitutionId);
    List<NonFinancialInstitution> getNonFinancialInstitutions();
    NonFinancialInstitution updateNonFinancialInstitution(NonFinancialInstitutionRegistrationDTO updateInstitutionRegistrationDTO);
    List<NonFinancialInstitution> getNonFinancialInstitutionsByClientId(String clientId);
    Optional<Organisation> getOrganisationByEmployeeId(String employeeId);
    Optional<NonFinancialInstitution> getNonFinancialInstitutionByAccountId(String accountId);
    NonFinancialInstitution updateNonFinancialInstitutionGeneralInformation(NonFinancialInstitution nonFinancialInstitution);
    OrganisationIdentification updateNonFinancialInstitutionOrganisatinIdentification(OrganisationIdentification organisationIdentification);
}
