package zw.co.esolutions.zpas.services.iface.merchant;

import zw.co.esolutions.zpas.model.Merchant;

import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
public interface MerchantService {
    Merchant createMerchant(Merchant merchant);
    Merchant updateMerchant(Merchant merchant);
    Merchant deleteMerchant(String id);
    Merchant approveMerchant(String id);
    Merchant rejectMerchant(String id);
    Optional<Merchant> getMerchant(String id);
    List<Merchant> getMerchants();
    String refreshMerchantSearchIndex();
}
