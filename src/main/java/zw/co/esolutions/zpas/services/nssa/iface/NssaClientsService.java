package zw.co.esolutions.zpas.services.nssa.iface;

import zw.co.esolutions.zpas.dto.nssa.EmployeeDTO;
import zw.co.esolutions.zpas.dto.nssa.EmployerDTO;
import zw.co.esolutions.zpas.dto.nssa.UploadEmployeesDTO;

import java.util.List;
import java.util.Optional;

public interface NssaClientsService {
    Optional<EmployeeDTO> createEmployee(EmployeeDTO employeeDTO);
    Optional<EmployeeDTO> getEmployeeById(String employeeId);
    List<EmployeeDTO> getEmployees();
    List<EmployeeDTO> getEmployeesByEmployerId(String employerId);
    Optional<EmployeeDTO> updateEmployee(EmployeeDTO employeeDTO);
    Optional<EmployerDTO> getEmployerBySsr(String employerSsr);
    Optional<EmployeeDTO> deleteEmployee(String id);
    Optional<EmployeeDTO> approveEmployee(String id);
    Optional<EmployeeDTO> rejectEmployee(String id);
    List<EmployeeDTO> createEmployees(UploadEmployeesDTO uploadEmployeesDTO) throws Exception;
    Boolean isSSNAlreadyInUse(String identityCardNumber);
    Optional<EmployeeDTO> getEmployeeBySSN(String ssn);
    boolean validateSsnFormat(String ssn);

}
