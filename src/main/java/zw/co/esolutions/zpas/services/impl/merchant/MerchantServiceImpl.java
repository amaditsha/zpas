package zw.co.esolutions.zpas.services.impl.merchant;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.*;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Merchant;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.PartyMerchantAccount;
import zw.co.esolutions.zpas.repository.MerchantRepository;
import zw.co.esolutions.zpas.repository.PartyMerchantAccountRepository;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.services.iface.merchant.MerchantService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.persistence.EntityManager;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Slf4j
@Service
public class MerchantServiceImpl implements MerchantService {
    @Autowired
    EntityManager entityManager;
    
    @Autowired
    MerchantRepository merchantRepository;

    @Autowired
    PartyRepository partyRepository;

    @Autowired
    PartyMerchantAccountRepository partyMerchantAccountRepository;
    
    @Override
    public Merchant createMerchant(Merchant merchant) {
        merchant.setId(GenerateKey.generateEntityId());
        merchant.setDateCreated(OffsetDateTime.now());
        merchant.setLastUpdated(OffsetDateTime.now());
        merchant.setEntityStatus(EntityStatus.PENDING_APPROVAL);


        return merchantRepository.save(merchant);
    }

    @Override
    public Merchant updateMerchant(Merchant merchant) {
        merchant.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        merchant.setLastUpdated(OffsetDateTime.now());
        return merchantRepository.save(merchant);

    }

    @Override
    public Merchant deleteMerchant(String id) {
        Optional<Merchant> merchantOptional = merchantRepository.findById(id);
        Merchant merchant = null;

        if(merchantOptional.isPresent()){
            merchant = merchantOptional.get();
            merchant.setEntityStatus(EntityStatus.DELETED);
            merchant = merchantRepository.save(merchant);
        }else{
            log.info("Merchant has already been deleted");
        }
        return merchant;
    }

    @Override
    public Merchant approveMerchant(String id) {
        Optional<Merchant> optionalMerchant = merchantRepository.findById(id);
        Merchant merchant = null;
        if (optionalMerchant.isPresent()){
            merchant = optionalMerchant.get();
            if(merchant.getEntityStatus()!= EntityStatus.DELETED){
                merchant.setEntityStatus(EntityStatus.ACTIVE);
                merchant = merchantRepository.save(merchant);
            }else{
                log.info("Merchant has already been approved");
            }
        }
        return merchant;
    }

    @Override
    public Merchant rejectMerchant(String id) {
        Optional<Merchant> optionalMerchant = merchantRepository.findById(id);
        Merchant merchant = null;
        if (optionalMerchant.isPresent()) {
            merchant = optionalMerchant.get();
            if(merchant.getEntityStatus() != EntityStatus.DELETED) {
                merchant.setEntityStatus(EntityStatus.DISAPPROVED);
                merchant = merchantRepository.save(merchant);
            } else {
                log.info("Merchant "+ id + "has already been rejected");
            }
        }
        return merchant;
    }

    @Override
    public Optional<Merchant> getMerchant(String id) {
        merchantRepository.findById(id).ifPresent(merchant -> {

        });
        return merchantRepository.findById(id);
    }

    @Override
    public List<Merchant> getMerchants() {
        return merchantRepository.findAll();
    }

    @Override
    public String refreshMerchantSearchIndex() {
        FullTextSession fullTextSession = Search.getFullTextSession(entityManager.unwrap(Session.class));
        fullTextSession.setFlushMode(FlushMode.MANUAL);
        fullTextSession.setCacheMode(CacheMode.IGNORE);
        int BATCH_SIZE = 25;
        ScrollableResults scrollableResults = fullTextSession.createCriteria(Merchant.class)
                .setFetchSize(BATCH_SIZE)
                .scroll(ScrollMode.FORWARD_ONLY);
        int index = 0;
        while(scrollableResults.next()) {
            index++;
            fullTextSession.index(scrollableResults.get(0)); //index each element
            if (index % BATCH_SIZE == 0) {
                fullTextSession.flushToIndexes(); //apply changes to indexes
                fullTextSession.clear(); //free memory since the queue is processed
            }
        }

        return "Merchant search indices reloaded successfully";    }


}
