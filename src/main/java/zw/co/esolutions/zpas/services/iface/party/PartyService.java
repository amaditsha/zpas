package zw.co.esolutions.zpas.services.iface.party;

import zw.co.esolutions.zpas.dto.party.VirtualIdAvailabilityCheckDTO;
import zw.co.esolutions.zpas.model.Party;

import java.util.Optional;

/**
 * Created by alfred on 08 Apr 2019
 */
public interface PartyService {
    Optional<Party> getPartyById(String partyId);
    Optional<Party> getPartyByVirtualId(String virtualId);

    Optional<VirtualIdAvailabilityCheckDTO> checkVirtualIdAvailability(String virtualId);
}
