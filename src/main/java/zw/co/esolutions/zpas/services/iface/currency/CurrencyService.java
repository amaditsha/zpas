package zw.co.esolutions.zpas.services.iface.currency;

import zw.co.esolutions.zpas.dto.currency.CurrencyCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.currency.CurrencyDTO;
import zw.co.esolutions.zpas.model.Currency;

import java.util.List;
import java.util.Optional;

/**
 * Created by petros on 14 Feb 2019
 */
public interface CurrencyService {
    Currency createCurrency(CurrencyDTO currency);

    Currency deleteCurrency(String id);

    Optional<Currency> findCurrencyById(String id);

    Currency updateCurrency(CurrencyDTO currencyDTO);

    List<Currency> getAllCurrencies();

    Optional<CurrencyCodeAvailabilityCheckDTO> checkCurrencyCodeAvailability(String currencyCode);

    List<Currency> refreshCurrencies();
}

