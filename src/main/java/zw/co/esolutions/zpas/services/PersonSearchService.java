package zw.co.esolutions.zpas.services;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.jpa.*;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PersonSearchService {

    @Autowired
    private final EntityManager entityManager;

    @Autowired
    private AccountsService accountService;

    @Autowired
    public PersonSearchService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    public List<Person> searchPerson(String searchTerm, String fiId){

        FullTextEntityManager fullTextEm = org.hibernate.search.jpa.Search
                .getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEm.getSearchFactory().buildQueryBuilder().forEntity(PersonName.class).get();

        FullTextQuery fullTextQuery = fullTextEm.createFullTextQuery(queryBuilder.keyword()
                .onFields("givenName", "birthName", "middleName")
                .matching(searchTerm)
                .createQuery(), PersonName.class);

        List<PersonName> resultList = fullTextQuery.getResultList();
        List<Person> personList = new ArrayList<>();

        resultList.forEach(personName -> {
            PersonIdentification personIdentification = personName.getIdentification();
            Person person = personIdentification.getPerson();


            personList.add(person);

            /**
             * Filter Person by Financial Institution
             * +
             */
            //get accounts by client id
            /*List<Account> accounts = accountService.getAccountsByClientId(person.getId());

            accounts.forEach(account ->
                //get account servicer role
                account.getPartyRole().stream()
                        .filter(role -> role instanceof AccountServicerRole).findFirst().ifPresent(accPartyRole -> {
                    FinancialInstitution financialInstitution;
                    //get financial institution (inside role player for account servicer role)
                    financialInstitution = (FinancialInstitution) accPartyRole.getPlayer().get(0);
                    //compare financial institution id with fiId
                    if(StringUtils.isNoneBlank(financialInstitution.getId()) && financialInstitution.getId().equals(fiId)){
                        personList.add(person);
                    }
                }));*/
        });

        return personList;

    }


}
