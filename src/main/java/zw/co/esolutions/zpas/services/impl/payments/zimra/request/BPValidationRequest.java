package zw.co.esolutions.zpas.services.impl.payments.zimra.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alfred on 21 August 2019
 */
@XmlRootElement(name = "ValidatewithBPNumber")
public class BPValidationRequest {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "GetCompanyDetailsRequest")
    private GetCompanyDetailsRequest GetCompanyDetailsRequestObject;

    public GetCompanyDetailsRequest getGetCompanyDetailsRequest() {
        return GetCompanyDetailsRequestObject;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setGetCompanyDetailsRequest(GetCompanyDetailsRequest GetCompanyDetailsRequestObject) {
        this.GetCompanyDetailsRequestObject = GetCompanyDetailsRequestObject;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}