package zw.co.esolutions.zpas.services.iface.alerts;

import zw.co.esolutions.zpas.model.Notification;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

public interface NotificationService {
    void onStartUp(HttpSession session);
    void notify(Notification notification, String username);
    Notification createNotification(Notification notification);
    List<Notification> getNotifications(String clientId);
    List<Notification> getUnReadNotificationsForGroupId(String groupId, String clientId);
    List<Notification> getUnReadNotificationsForClientId(String clientId);
    List<Notification> getUnReadNotificationsForUsername(String username);

    Notification markNotificationAsRead(String notificationId);
    Notification getNotificationByGroupIdAndEntityId(String groupId, String entityId, String clientId);
}
