package zw.co.esolutions.zpas.services.impl.payments.zimra.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alfred on 21 August 2019
 */
@XmlRootElement(name = "ValidatewithContractNumber")
public class BPValidationRequestWithTaxCode {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "GetRequest")
    private GetRequest getRequest;

    public GetRequest getGetRequest() {
        return getRequest;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setGetRequest(GetRequest getRequest) {
        this.getRequest = getRequest;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}