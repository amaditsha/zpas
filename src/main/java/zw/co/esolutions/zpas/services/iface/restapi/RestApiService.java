package zw.co.esolutions.zpas.services.iface.restapi;

import zw.co.esolutions.zpas.dto.account.AccountSearchResult;
import zw.co.esolutions.zpas.dto.currency.CurrencyCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchResponseDTO;
import zw.co.esolutions.zpas.dto.party.VirtualCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.party.VirtualIdAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.party.VirtualSuffixAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.profile.UsernameCheckDTO;

import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 20 Mar 2019
 */
public interface RestApiService {
    List<PersonSearchResponseDTO> searchPersons(PersonSearchDTO personSearchDTO);
    List<PersonSearchResponseDTO> searchAllPersons(PersonSearchDTO personSearchDTO);
    List<PersonSearchResponseDTO> searchPersonsForCashAccountMandate(PersonSearchDTO personSearchDTO);
    Optional<AccountSearchResult> searchAccountByVirtualId(String virtualId);
    Optional<AccountSearchResult> getAccountInfo(String accountNumber, String financialInstitutionId);
    Optional<PersonSearchResponseDTO> getPersonInfoById(String personId);
    Optional<VirtualIdAvailabilityCheckDTO> checkVirtualIdAvailability(String virtualId);
    Optional<VirtualSuffixAvailabilityCheckDTO> checkVirtualSuffixAvailability(String virtualSuffix);
    Optional<VirtualCodeAvailabilityCheckDTO> checkVirtualCodeAvailability(String virtualCode);
    Optional<UsernameCheckDTO> checkUsernameAvailability(String username);
    Optional<CurrencyCodeAvailabilityCheckDTO> checkCurrencyCodeAvailability(String currencyCode);
}
