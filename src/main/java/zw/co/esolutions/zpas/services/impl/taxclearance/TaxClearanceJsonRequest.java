package zw.co.esolutions.zpas.services.impl.taxclearance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by alfred on 23 May 2019
 */
@Data
@Builder
public class TaxClearanceJsonRequest implements Serializable {
    private String bpn;
    private String companyName;

    public TaxClearanceJsonRequest(
            @JsonProperty("bpn") String bpn,
            @JsonProperty("companyName") String companyName) {
        this.bpn = bpn;
        this.companyName = companyName;
    }
}
