package zw.co.esolutions.zpas.services.iface.agreement;

import zw.co.esolutions.zpas.dto.agreement.SignatureConditionDTO;
import zw.co.esolutions.zpas.model.SignatureCondition;

import java.util.List;
import java.util.Optional;

public interface SignatureConditionService {
    SignatureCondition createSignatureCondition(SignatureConditionDTO signatureConditionDTO);
    SignatureCondition updateSignatureCondition(SignatureConditionDTO signatureConditionDTO);
    SignatureCondition deleteSignatureCondition(String id);
    SignatureCondition approveSignatureCondition(String id);
    SignatureCondition rejectSignatureCondition(String id);
    Optional<SignatureCondition> getSignatureConditionById(String signatureId);
    List<SignatureCondition> getSignatureConditions();
}
