package zw.co.esolutions.zpas.services.impl.hubconfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.HubConfiguration;
import zw.co.esolutions.zpas.repository.HubConfigurationRepository;
import zw.co.esolutions.zpas.services.iface.hubconfig.HubConfigurationService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class HubConfigurationServiceImpl implements HubConfigurationService {

    @Autowired
    HubConfigurationRepository hubConfigurationRepository;

    @Override
    public HubConfiguration createConfig(HubConfiguration configuration) {
        configuration.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        return hubConfigurationRepository.save(configuration);
    }

    @Override
    public HubConfiguration updateConfig(HubConfiguration configuration) {
        configuration.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return hubConfigurationRepository.save(configuration);
    }

    @Override
    public HubConfiguration deleteHubConfig(String configurationId) {
        Optional<HubConfiguration> hubConfigurationOptional = hubConfigurationRepository.findById(configurationId);
        HubConfiguration hubConfiguration = null;

        if(hubConfigurationOptional.isPresent()){
            hubConfiguration = hubConfigurationOptional.get();
            hubConfiguration.setEntityStatus(EntityStatus.DELETED);
            hubConfiguration = hubConfigurationRepository.save(hubConfiguration);
        }else{
            log.info("Hub Configuration has already been deleted");
        }
        return hubConfiguration;
    }

    @Override
    public HubConfiguration approveHubConfig(String id) {
        Optional<HubConfiguration> optionalHubConfiguration = hubConfigurationRepository.findById(id);
        HubConfiguration hubConfiguration = null;
        if (optionalHubConfiguration.isPresent()){
            hubConfiguration = optionalHubConfiguration.get();
            if(hubConfiguration.getEntityStatus()!= EntityStatus.DELETED){
                hubConfiguration.setEntityStatus(EntityStatus.ACTIVE);
                hubConfiguration = hubConfigurationRepository.save(hubConfiguration);
            }else{
                log.info("Hub config has already been approved");
            }
        }
        return hubConfiguration;
    }

    @Override
    public HubConfiguration rejectHubConfig(String id) {
        Optional<HubConfiguration> optionalHubConfiguration = hubConfigurationRepository.findById(id);
        HubConfiguration hubConfiguration = null;
        if (optionalHubConfiguration.isPresent()) {
            hubConfiguration = optionalHubConfiguration.get();
            if(hubConfiguration.getEntityStatus() != EntityStatus.DELETED) {
                hubConfiguration.setEntityStatus(EntityStatus.DISAPPROVED);
                hubConfiguration = hubConfigurationRepository.save(hubConfiguration);
            } else {
                log.info("HubConfiguration "+ id + "has already been rejected");
            }
        }
        return hubConfiguration;
    }

    @Override
    public void deleteConfig(String configurationId) {
        hubConfigurationRepository.deleteById(configurationId);
    }

    @Override
    public Optional<HubConfiguration> getConfig(String BIC) {
        return hubConfigurationRepository.findById(BIC);
    }

    @Override
    public List<HubConfiguration> getConfigs() {
        return hubConfigurationRepository.findAll();
    }
}
