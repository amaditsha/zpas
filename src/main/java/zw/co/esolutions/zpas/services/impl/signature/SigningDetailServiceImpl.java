package zw.co.esolutions.zpas.services.impl.signature;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.SigningDetail;
import zw.co.esolutions.zpas.repository.SigningDetailRepository;
import zw.co.esolutions.zpas.services.iface.signature.SigningDetailService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SigningDetailServiceImpl implements SigningDetailService {

    @Autowired
    private SigningDetailRepository signingDetailRepository;

    @Override
    public SigningDetail createSigningDetail(SigningDetail signingDetail) {
        return signingDetailRepository.save(signingDetail);
    }

    @Override
    public SigningDetail updateSigningDetail(SigningDetail signingDetail) {
        return signingDetailRepository.save(signingDetail);
    }

    @Override
    public SigningDetail deleteSigningDetail(String id) {
        Optional<SigningDetail> signingDetailOptional = signingDetailRepository.findById(id);
        SigningDetail signingDetail = null;
        if (signingDetailOptional.isPresent()) {
            signingDetail = signingDetailOptional.get();
            signingDetail.setEntityStatus(EntityStatus.DELETED);
            signingDetail = signingDetailRepository.save(signingDetail);
        }
        return signingDetail;
    }

    @Override
    public Optional<SigningDetail> getSigningDetailById(String signatureId) {
        return signingDetailRepository.findById(signatureId);
    }

    @Override
    public List<SigningDetail> getSigningDetails() {
        return signingDetailRepository.findAll();
    }
}
