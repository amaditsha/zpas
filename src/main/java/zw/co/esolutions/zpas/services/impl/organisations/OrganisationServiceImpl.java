package zw.co.esolutions.zpas.services.impl.organisations;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.controller.registration.ClientController;
import zw.co.esolutions.zpas.dto.registration.EmployerDetailsDTO;
import zw.co.esolutions.zpas.dto.registration.OrganisationDTO;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.OrganisationIdentificationRepository;
import zw.co.esolutions.zpas.repository.OrganisationNameRepository;
import zw.co.esolutions.zpas.repository.OrganisationRepository;
import zw.co.esolutions.zpas.services.iface.organisations.OrganisationService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrganisationServiceImpl implements OrganisationService {
    @Autowired
    private OrganisationRepository organisationRepository;

    @Autowired
    private OrganisationNameRepository organisationNameRepository;

    @Autowired
    private OrganisationIdentificationRepository organisationIdentificationRepository;

    @Override
    public List<Organisation> getAllOrganisations() {
        return organisationRepository.findAll();
    }

    @Override
    public Optional<OrganisationIdentification> getOrganisationIdentificationByAnyBIC(String anyBIC) {
        return Optional.ofNullable(this.organisationIdentificationRepository.findOrganisationIdentificationByAnyBIC(anyBIC));
    }

    @Override
    public Optional<OrganisationIdentification> getOrganisationIdentificationByBICFI(String bICFI) {
        return Optional.ofNullable(this.organisationIdentificationRepository.findOrganisationIdentificationByBICFI(bICFI));
    }

    @Override
    public List<OrganisationDTO> searchOrganisation(EmployerDetailsDTO employerDetailsDTO) {
        final String search = employerDetailsDTO.getSearch();
        final List<OrganisationName> organisationNames = organisationNameRepository.searchOrganisationName(search.toLowerCase());
        log.info("Found organisation names are:\n==================================\n{}\n=================================\n", organisationNames);

        return organisationNames.stream().map(organisationName -> {
            log.info("Mapping the organisation name to the organisation dto");
            final OrganisationDTO.OrganisationDTOBuilder builder = OrganisationDTO.builder();
            Optional.ofNullable(organisationName.getOrganisation()).ifPresent(organisationIdentification -> {
                log.info("The party identification is present");
                Optional.ofNullable(organisationIdentification.getOrganisation()).ifPresent(organisation -> {
                    log.info("The identification party is present");
                    builder.organisationId(organisation.getId());
                    builder.organisationName(organisationName.getLegalName());
                });
            });
            return builder.build();
        }).collect(Collectors.toList());
    }

    @Override
    public List<OrganisationDTO> searchNonFIOrganisation(EmployerDetailsDTO employerDetailsDTO) {
        final String search = employerDetailsDTO.getSearch();
        final List<OrganisationName> organisationNames = organisationNameRepository.searchOrganisationName(search.toLowerCase());
        log.info("Found organisation names are:\n==================================\n{}\n=================================\n", organisationNames);

        return organisationNames.stream().map(organisationName -> {
            log.info("Mapping the organisation name to the organisation dto");
            final OrganisationDTO.OrganisationDTOBuilder builder = OrganisationDTO.builder();
            Optional.ofNullable(organisationName.getOrganisation()).ifPresent(organisationIdentification -> {
                log.info("The party identification is present");
                Optional.ofNullable(organisationIdentification.getOrganisation()).ifPresent(organisation -> {
                    log.info("The identification party is present. Filtering the for the none FI");

//                    if(organisation instanceof NonFinancialInstitution) {
                        log.info("The identification party present is {} and name {}", organisation.getId(), organisationName.getLegalName());
                        builder.organisationId(organisation.getId());
                        builder.organisationName(organisationName.getLegalName());
//                    } else {
//                        log.info("Organisation  class is {}", organisation.getClass().getSimpleName());
//                    }
                });
            });
            return builder.build();
        }).collect(Collectors.toList());
    }
}
