package zw.co.esolutions.zpas.services.nssa.impl.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Create by alfred on 23 Jul 2019
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NssaP4RequestDTO {
    private String requestId;
    private P4ScheduleDTO requestData;
}
