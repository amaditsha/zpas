package zw.co.esolutions.zpas.services.nssa.iface;

import zw.co.esolutions.zpas.dto.nssa.EmployeeDependentDTO;

import java.util.List;
import java.util.Optional;

public interface EmployeeDependentService {
    Optional<EmployeeDependentDTO> createEmployeeDependent(EmployeeDependentDTO employeeDependentDTO);
    Optional<EmployeeDependentDTO> getEmployeeDependentById(String id);
    List<EmployeeDependentDTO> getEmployeeDependents();
    List<EmployeeDependentDTO> getEmployeeDependentByEmployeeId(String employeeId);
    List<EmployeeDependentDTO> getEmployeeDependentByEmployeeSsn(String employeeSsn);
    Optional<EmployeeDependentDTO> deleteEmployeeDependent(String id);
    Optional<EmployeeDependentDTO> approveEmployeeDependent(String id);
    Optional<EmployeeDependentDTO> rejectEmployeeDependent(String id);
    Optional<EmployeeDependentDTO> updateEmployeeDependent(EmployeeDependentDTO employeeDependentDTO);
}
