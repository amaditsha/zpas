package zw.co.esolutions.zpas.services.impl.payments.verification.sse.domain;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class PaymentStatusInfo implements Serializable {
    private String status;
    private String info;
    private String requestId;
    private List<PaymentStatusInfo> sseMessages;
}
