package zw.co.esolutions.zpas.services.impl.process;
import org.springframework.beans.factory.annotation.Value;
import org.xml.sax.SAXException;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.InvestigationPartyRole;
import zw.co.esolutions.zpas.model.DateTimePeriod;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.iso.msg.camt055_001_07.CustomerPaymentCancellationRequestV07;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentExecutionRepository;
import zw.co.esolutions.zpas.services.iface.process.InvestigationCaseInitiationService;
import zw.co.esolutions.zpas.services.impl.process.builder.CustomerPaymentCancellationRequestMessageBuilder;
import zw.co.esolutions.zpas.services.impl.process.pojo.StatusReasonDto;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationPartyRoleUtil;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentStatusUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.RefGen;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
@Transactional
public class InvestigationCaseInitiationServiceImpl implements InvestigationCaseInitiationService {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt055_001_07");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Value("${XSD.BASE.PATH}")
    String xsdBasePath;

    @Autowired
    CustomerPaymentCancellationRequestMessageBuilder customerPaymentCancellationRequestMessageBuilder;

    @Autowired
    PaymentExecutionRepository paymentExecutionRepository;

    @Autowired
    PaymentStatusUtil paymentStatusUtil;

    @Autowired
    InvestigationPartyRoleUtil investigationPartyRoleUtil;

    @Autowired
    MessageProducer messageProducer;

    @Override
    public ServiceResponse initiateCustomerPaymentCancellationRequest(PaymentInvestigationCase paymentInvestigationCase) {
        try{

            log.info("Handling Customer Payment Cancellation Request Initiation request");

            //build the message
            CustomerPaymentCancellationRequestV07 customerPaymentCancellationRequestV07 = customerPaymentCancellationRequestMessageBuilder
                    .buildCustomerPaymentCancellationRequestMsg(paymentInvestigationCase);

            //update payment status
            List<StatusReasonDto> statusReasonDtoList = Arrays.asList(new StatusReasonDto(StatusCode.BeingCancelled.getCodeName(),
                    Arrays.asList("Customer Cancellation Request")));

            paymentInvestigationCase.getUnderlyingPayment().forEach(payment1 -> {
                paymentStatusUtil.updatePaymentStatus(payment1, PaymentStatusCode.PendingCancellationRequest, statusReasonDtoList);
            });

            InvestigationCaseStatus investigationCaseStatus = new InvestigationCaseStatus();
            investigationCaseStatus.setCaseStatus(CaseStatusCode.Assigned);
            investigationCaseStatus.setInvestigationCase(paymentInvestigationCase);
            investigationCaseStatus.setId(RefGen.getReference("CX"));
            investigationCaseStatus.setEntityStatus(EntityStatus.ACTIVE);
            investigationCaseStatus.setStatusDateTime(OffsetDateTime.now());
//            investigationCaseStatus.setValidityTime(new DateTimePeriod());
            investigationCaseStatus.setStatusDescription("Cancellation Request");
            investigationCaseStatus.setCancellationProcessingStatus(CancellationProcessingStatusCode.PendingCancellation);
//            investigationCaseStatus.setPartyRole(new InvestigationPartyRole());

            StatusReason statusReason = new StatusReason();
            statusReason.setId(UUID.randomUUID().toString());
            statusReason.setEntityStatus(EntityStatus.ACTIVE);
            statusReason.setStatus(investigationCaseStatus);
            statusReason.setReason(paymentInvestigationCase.getCancellationReason().getCodeName());
            statusReason.setAdditionalInfo(new ArrayList<>());
            statusReason.setCancellationReason(CancelledStatusReasonV2Code.CancelledByClient);

            investigationCaseStatus.setStatusReason(Arrays.asList(statusReason));

            if(paymentInvestigationCase.getStatus() != null) {
                paymentInvestigationCase.getStatus().add(investigationCaseStatus);
            }

            log.info("Building the message for sending to hub..");

            //send message to hub
            String destinationBic = customerPaymentCancellationRequestV07.getAssgnmt().getAssgne().getAgt().getFinInstnId().getBICFI();
            log.info("Destination BIC... " + destinationBic);
            messageProducer.sendMessageToHub(buildXml(customerPaymentCancellationRequestV07), destinationBic);

            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_SUCCESS)
                    .narrative("Customer Payment Cancellation Request initiated successfully")
                    .build();

        }catch (Exception e){
            e.printStackTrace();
            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .narrative("Customer Payment Cancellation Request initiation failed : " + e.getMessage())
                    .build();
        }
    }

    private String buildXml(CustomerPaymentCancellationRequestV07 customerPaymentCancellationRequestV07) throws JAXBException, SAXException {
        log.info("Building camt.055 XML message..");

        zw.co.esolutions.zpas.iso.msg.camt055_001_07.Document document = new zw.co.esolutions.zpas.iso.msg.camt055_001_07.Document();
        document.setCstmrPmtCxlReq(customerPaymentCancellationRequestV07);

        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, xmlEncoding);

        StringWriter stringWriterTest = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriterTest);

        String xml = stringWriterTest.toString();

        log.info(xml);

        //Setup schema validator
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema xsdSchema = sf.newSchema(new File(xsdBasePath + "/xsd/camt/camt.055.001.07.xsd"));
        jaxbMarshaller.setSchema(xsdSchema);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

//        String xml = stringWriter.toString();
//
//        log.info(xml);

        return xml;
    }

}
