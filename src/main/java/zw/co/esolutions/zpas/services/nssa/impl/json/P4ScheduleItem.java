package zw.co.esolutions.zpas.services.nssa.impl.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Create by alfred on 23 Jul 2019
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class P4ScheduleItem {
    private String ssr;
    private String employeeNumber;
    private String ssn;
    private String nationalIdNumber;
    private String period;
    private String dateOfBirth;
    private String firstNames;
    private String surname;
    private String commencementDate;
    private String cessationDate;
    private String reasonForCessation;
    private String natureOfEmployement;
    private double npsInsurableEarning;
    private double totalNpsContributionThisMonth;
    private double basicSalaryWCIFExAllowances;
    private double actualInsurableEarning;
}
