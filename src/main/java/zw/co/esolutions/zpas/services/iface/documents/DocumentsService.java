package zw.co.esolutions.zpas.services.iface.documents;

import zw.co.esolutions.zpas.dto.documents.DocumentDTO;
import zw.co.esolutions.zpas.model.Document;

import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 11 Mar 2019
 */
public interface DocumentsService {
    Optional<Document> createDocument(DocumentDTO documentDTO);
    List<Document> getDocumentsByClientId(String clientId);
    Optional<Document> getDocumentById(String documentId);
}
