package zw.co.esolutions.zpas.services.iface.process;

import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

public interface InvestigationCaseProcessService {

    ServiceResponse withdrawBulk(PaymentInvestigationCase paymentInvestigationCase);
    ServiceResponse recallEFTCredits(PaymentInvestigationCase paymentInvestigationCase);

}
