package zw.co.esolutions.zpas.services.impl.country;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.country.CountryDTO;
import zw.co.esolutions.zpas.model.Country;
import zw.co.esolutions.zpas.repository.CountryRepository;
import zw.co.esolutions.zpas.services.iface.country.CountryService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    protected CountryRepository countryRepository;

    @Override
    public Country createCountry(CountryDTO countryDTO) {
        Country country = this.convertDTOtoEntity(countryDTO);
        final Country saved = this.countryRepository.save(country);
        return saved;
    }

    @Override
    public Country deleteCountry(String id) {
        Optional<Country> countryOptional = countryRepository.findById(id);
        Country country = null;
        if (countryOptional.isPresent()) {
            country = countryOptional.get();
            country.setEntityStatus(EntityStatus.DELETED);
            country = countryRepository.save(country);
        }
        return country;
    }

    @Override
    public Optional<Country> findCountryById(String id) {
        return this.countryRepository.findById(id);
    }

    @Override
    public Country updateCountry(CountryDTO countryDTO) {
        Country country = convertDTOtoEntity(countryDTO);
        return countryRepository.save(country);
    }

    @Override
    public List<Country> getAllCountries() {
        return this.countryRepository.findAll();
    }

    public Country convertDTOtoEntity(CountryDTO countryDTO){
        DateTimeFormatter parser = DateTimeFormatter.ofPattern("[MM-dd-yyyy][MM/dd/yyyy]");
        Country country = new Country();
        country.setId(countryDTO.getId() == null? GenerateKey.generateEntityId() : countryDTO.getId());
        country.setCode(countryDTO.getCode());
        country.setName(countryDTO.getName());

        return country;
    }
}
