package zw.co.esolutions.zpas.services.impl.tax;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.model.Operator;
import zw.co.esolutions.zpas.model.Tax;
import zw.co.esolutions.zpas.repository.TaxRepository;
import zw.co.esolutions.zpas.services.iface.tax.TaxService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.Optional;

@Slf4j
@Service
public class TaxServiceImpl implements TaxService {

    @Autowired
    TaxRepository taxRepository;

    @Override
    public Tax createTax(Tax tax) {
        return taxRepository.save(tax);
    }

    @Override
    public Tax updateTax(Tax tax) {
        tax.setEntityStatus(EntityStatus.PENDING_EDIT_APPROVAL);
        return taxRepository.save(tax);
    }

    @Override
    public Tax deleteTax(String id) {
        Optional<Tax> optionalTax = taxRepository.findById(id);
        Tax tax = null;

        if (optionalTax.isPresent()) {
            tax = optionalTax.get();
            tax.setEntityStatus(EntityStatus.DELETED);
            tax = taxRepository.save(tax);
        } else {
            log.info("Details for this Tax have been deleted");
        }
        return tax;
    }

    @Override
    public Tax findTaxById(String id) {
        return null;
    }



    @Override
    public Optional<Tax> getTaxById(String taxId) {
        return taxRepository.findById(taxId);
    }
}
