package zw.co.esolutions.zpas.services.impl.payments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.payments.CreateBulkPaymentDTO;
import zw.co.esolutions.zpas.dto.payments.PaymentUploadDTO;
import zw.co.esolutions.zpas.model.BulkPayment;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.services.iface.payments.PaymentUploadsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsAcquiringService;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;

import java.util.Optional;

/**
 * Created by alfred on 26 Feb 2019
 */
@Service
public class PaymentUploadsServiceImpl implements PaymentUploadsService {

    @Autowired
    private PaymentUploadsProcessor paymentUploadsProcessor;

    @Autowired
    private PaymentsAcquiringService paymentsAcquiringService;

    @Override
    public Optional<Payment> uploadBulkPayment(PaymentUploadDTO paymentUploadDTO) throws PaymentRequestInvalidArgumentException {
        return paymentUploadsProcessor.uploadBulkPayment(paymentUploadDTO);
    }

    @Override
    public Optional<Payment> processPaymentsApiPainRequest(String xml) throws PaymentRequestInvalidArgumentException {
        final Optional<Payment> paymentOptional = paymentsAcquiringService.buildPaymentFromXml(xml);
        return paymentOptional.map(payment -> {
            return paymentUploadsProcessor.savePaymentInfo((BulkPayment) payment);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<BulkPayment> createBulkPayment(CreateBulkPaymentDTO createBulkPaymentDTO) throws PaymentRequestInvalidArgumentException {
        return paymentUploadsProcessor.createBulkPaymentFromJson(createBulkPaymentDTO);
    }
}
