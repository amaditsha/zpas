package zw.co.esolutions.zpas.services.impl.currency;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.currency.CurrencyCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.currency.CurrencyDTO;
import zw.co.esolutions.zpas.enums.CurrencyCode;
import zw.co.esolutions.zpas.model.Currency;
import zw.co.esolutions.zpas.repository.CurrencyRepository;
import zw.co.esolutions.zpas.security.model.Privilege;
import zw.co.esolutions.zpas.services.iface.currency.CurrencyService;
import zw.co.esolutions.zpas.utilities.enums.AccessRights;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Service
@Slf4j
public class CurrencyServiceImpl implements CurrencyService {
    @Autowired
    protected CurrencyRepository currencyRepository;

    @Override
    public Currency createCurrency(CurrencyDTO currencyDTO) {
        Currency currency = this.convertDTOtoEntity(currencyDTO);
        final Currency saved = this.currencyRepository.save(currency);
        return saved;
    }

    @Override
    public Currency deleteCurrency(String id) {
        Optional<Currency> currencyOptional = currencyRepository.findById(id);
        Currency currency = null;
        if (currencyOptional.isPresent()) {
            currency = currencyOptional.get();
            currency.setEntityStatus(EntityStatus.DELETED);
            currency = currencyRepository.save(currency);
        }
        return currency;
    }

    @Override
    public Optional<Currency> findCurrencyById(String id) {
        return this.currencyRepository.findById(id);
    }

    @Override
    public Currency updateCurrency(CurrencyDTO currencyDTO) {
        Currency currency = convertDTOtoEntity(currencyDTO);
        log.info("Default Currency: " + currency.getDefaultCurrency());
        return currencyRepository.save(currency);
    }

    @Override
    public List<Currency> getAllCurrencies() {
        return this.currencyRepository.findAll();
    }

    @Override
    public Optional<CurrencyCodeAvailabilityCheckDTO> checkCurrencyCodeAvailability(String currencyCode) {
        String sanitisedCurrencyCode = Optional.ofNullable(currencyCode).map(String::toLowerCase).orElse("");
        return currencyRepository.findCurrencyById(sanitisedCurrencyCode)
                .map(currency -> Optional.of(CurrencyCodeAvailabilityCheckDTO.builder()
                        .available(false)
                        //.currencyCode(CurrencyCode.valueOf(""))
                        .build()))
                .orElse(Optional.of(CurrencyCodeAvailabilityCheckDTO.builder()
                        .available(true)
                        //.currencyCode(CurrencyCode.valueOf(""))
                        .build()));
    }

    public Currency convertDTOtoEntity(CurrencyDTO currencyDTO){
        DateTimeFormatter parser = DateTimeFormatter.ofPattern("[MM-dd-yyyy][MM/dd/yyyy]");
        Currency currency = new Currency();
        currency.setId(currencyDTO.getId() == null? GenerateKey.generateEntityId() : currencyDTO.getId());
        currency.setCode(currencyDTO.getCode());
        currency.setName(currencyDTO.getName());
        currency.setDefaultCurrency(currencyDTO.getDefaultCurrency() == null ? Boolean.FALSE : currencyDTO.getDefaultCurrency());
        currency.setDateCreated(OffsetDateTime.now());
        currency.setEntityStatus(EntityStatus.ACTIVE);
        if(StringUtils.isNoneBlank(currencyDTO.getId())){
            currency.setStartDate(LocalDate.parse(currencyDTO.getStartDate()).atTime(OffsetTime.now()));
            currency.setEndDate(LocalDate.parse(currencyDTO.getEndDate()).atTime(OffsetTime.now()));
        }else {
            currency.setStartDate(LocalDate.parse(currencyDTO.getStartDate(), parser).atTime(OffsetTime.now()));
            currency.setEndDate(LocalDate.parse(currencyDTO.getEndDate(), parser).atTime(OffsetTime.now()));
        }

        return currency;
    }

    @Override
    public List<Currency> refreshCurrencies() {
        final Set<Currency> existingCurrencies = getAllCurrencies().stream().collect(Collectors.toSet());
        log.info("Existing currencies are: {}", existingCurrencies.size());
        final Set<Currency> allCurrencies = Stream.of(CurrencyCode.values())
                .map(code -> {
                    Currency currency = new Currency();
                    currency.setDefaultAttributes();
                    currency.setId(GenerateKey.generateEntityId());
                    currency.setCode(code.name());
                    currency.setName(code.getCodeName());
                    currency.setDefaultCurrency(false);
                    currency.setDateCreated(OffsetDateTime.now());
                    currency.setStartDate(OffsetDateTime.now());
                    currency.setEndDate(OffsetDateTime.now());
                    currency.setVersion(0L);
                    currency.setEntityStatus(EntityStatus.ACTIVE);
                    return currency;
                })
                .collect(toSet());
        log.info("All Currencies are: {}", allCurrencies.size());

        allCurrencies.removeAll(existingCurrencies);

        log.info("Currencies to be persisted: {}", allCurrencies.size());
        allCurrencies.forEach(currency -> {
            log.info("Currency: {} {}", currency.getCode(), currency.getName());
        });

        currencyRepository.saveAll(allCurrencies);
        return allCurrencies.stream().collect(Collectors.toList());
    }
}
