package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.dto.message.MessageResponseDTO;
import zw.co.esolutions.zpas.dto.message.NotificationRequestDTO;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.util.DateUtil;

import java.time.OffsetDateTime;

@Service
@Slf4j
public class NotificationUtil {

    @Autowired
    public NotificationConfigService notificationConfigService;

    public void sendNotification(String partyId, String destination, String originator, OffsetDateTime messageDate, String messageText, MessageChannel messageChannel, String subject){
        log.info("Requesting sending notification of channel {} to bmg", messageChannel.name());
        String bmgDate = DateUtil.convertToBMGFormat(messageDate);
        NotificationRequestDTO messageRequestDTO = NotificationRequestDTO.builder()
                .destination(destination)
                .originator(originator)
                .messageDate(bmgDate)
                .messageText(messageText)
                .messageChannel(messageChannel)
                .subject(subject)
                .clientId(partyId)
                .build();

        MessageResponseDTO messageResponseDTO = notificationConfigService.processAndSendNotifications(messageRequestDTO);
        log.info("The notification response | {}", messageResponseDTO);
    }
}
