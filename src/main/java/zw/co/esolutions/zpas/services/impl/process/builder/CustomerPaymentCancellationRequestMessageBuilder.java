package zw.co.esolutions.zpas.services.impl.process.builder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt055_001_07.*;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentInvestigationCaseRepository;
import zw.co.esolutions.zpas.services.iface.investigationcase.PaymentInvestigationCaseService;
import zw.co.esolutions.zpas.services.impl.process.pojo.PaymentInfoDto;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationPartyRoleUtil;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentInfoUtil;

import java.lang.System;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class CustomerPaymentCancellationRequestMessageBuilder {

    @Autowired
    InvestigationPartyRoleUtil investigationPartyRoleUtil;

    @Autowired
    PaymentInfoUtil paymentInfoUtil;

    @Autowired
    PaymentInvestigationCaseRepository paymentInvestigationCaseRepository;

    public static void main(String[] args) {

        CustomerPaymentCancellationRequestMessageBuilder builder
                = new CustomerPaymentCancellationRequestMessageBuilder();

        PaymentInvestigationCase paymentInvestigationCase = builder.test();
        System.out.println(paymentInvestigationCase.toString());
        CustomerPaymentCancellationRequestV07 customerPaymentCancellationRequestV07
                = builder.buildCustomerPaymentCancellationRequestMsg(new PaymentInvestigationCase());

        System.out.println(customerPaymentCancellationRequestV07.toString());
    }


    public PaymentInvestigationCase test() {

        return this.paymentInvestigationCaseRepository.findById("VH0Y1558965868658205").orElse(null);
    }

    public CustomerPaymentCancellationRequestV07 buildCustomerPaymentCancellationRequestMsg(PaymentInvestigationCase investigationCase) {

        log.info("building camt055 message.....");

        CustomerPaymentCancellationRequestV07 customerPaymentCancellationRequestV07 = new CustomerPaymentCancellationRequestV07();

        PaymentInfoDto paymentInfoDto = paymentInfoUtil.getPaymentInfo(investigationCase.getUnderlyingPayment());

        /**
         * Building Case Assignment
         * */

        CaseAssignment4 caseAssignment4 = new CaseAssignment4();
        caseAssignment4.setId(investigationCase.getAssignmentIdentification());

        log.info("Setting the assigner details");
        Party35Choice assignerChoice = new Party35Choice();
        assignerChoice.setAgt(this.investigationPartyRoleUtil.getBranchAndFinancialInstitutionIdentification5(investigationCase, Assigner.class));

        caseAssignment4.setAssgnr(assignerChoice);

        log.info("Setting the assignee details");
        Party35Choice assigneeChoice = new Party35Choice();
        assigneeChoice.setAgt(this.investigationPartyRoleUtil.getBranchAndFinancialInstitutionIdentification5(investigationCase, Assignee.class));

        caseAssignment4.setAssgne(assigneeChoice);
        caseAssignment4.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        Case4 case4 = new Case4();
        case4.setId(investigationCase.getIdentification());

        log.info("Setting the creater details");
        Party35Choice cretrChoice = new Party35Choice();
        cretrChoice.setPty(this.investigationPartyRoleUtil.getPartyIdentification125(investigationCase, CaseCreator.class));

        case4.setCretr(cretrChoice);
        case4.setReopCaseIndctn(false);


        ControlData1 controlData1 = new ControlData1();

        controlData1.setNbOfTxs(String.valueOf(paymentInfoDto.nrOfPayments));
        controlData1.setCtrlSum(paymentInfoDto.controlSum);

        customerPaymentCancellationRequestV07.setAssgnmt(caseAssignment4);
        customerPaymentCancellationRequestV07.setCase(case4);
        customerPaymentCancellationRequestV07.setCtrlData(controlData1);

        /**
         * Set the Underlying Transanction */
        List<UnderlyingTransaction21> underlyingTransaction21List = buildUnderlyingTransactions(investigationCase);
        customerPaymentCancellationRequestV07.getUndrlyg().addAll(underlyingTransaction21List);

        return customerPaymentCancellationRequestV07;

    }

    public String getOriginalMessageIdentification(Payment payment) {
        if (payment instanceof BulkPayment) {
            return payment.getPaymentRelatedIdentifications().stream().findFirst()
                    .get()
                    .getEndToEndIdentification();
        } else {
            IndividualPayment individualPayment = (IndividualPayment) payment;

            if (individualPayment.getBulkPayment() != null) {
                return individualPayment.getBulkPayment()
                        .getPaymentRelatedIdentifications().stream().findFirst()
                        .get()
                        .getEndToEndIdentification();
            } else {
                return individualPayment.getPaymentRelatedIdentifications().stream().findFirst()
                        .get()
                        .getEndToEndIdentification();
            }
        }
    }


    private List<UnderlyingTransaction21> buildUnderlyingTransactions(final PaymentInvestigationCase paymentInvestigationCase) {

        List<Payment> payments = paymentInvestigationCase.getUnderlyingPayment();

        Payment firstPayment = payments.get(0);

        int nrOfPayments = payments.size();

        log.info("Setting underlying instruction for payments of size {}", nrOfPayments);

        OriginalGroupHeader10 originalGroupHeader10 = new OriginalGroupHeader10();
        originalGroupHeader10.setGrpCxlId(paymentInvestigationCase.getId());
        originalGroupHeader10.setOrgnlMsgId(this.getOriginalMessageIdentification(firstPayment));
        originalGroupHeader10.setOrgnlMsgNmId("pain.001.001.07");
        originalGroupHeader10.setOrgnlCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        PaymentInfoDto paymentInfo = this.paymentInfoUtil.getPaymentInfo(payments);

        originalGroupHeader10.setNbOfTxs(String.valueOf(paymentInfo.getNrOfPayments()));
        originalGroupHeader10.setCtrlSum(paymentInfo.getControlSum());

        boolean groupCancellation = false;

        if (nrOfPayments == 1) {
            Payment payment = payments.get(0);

            if (payment instanceof BulkPayment) {
                log.info("Payment is a Bulk Payment, setting GroupCancellation to TRUE");
                groupCancellation = true;

                originalGroupHeader10.setNbOfTxs(String.valueOf(paymentInfo.getNrOfPayments()));
                originalGroupHeader10.setCtrlSum(paymentInfo.getControlSum());
            } else {
                log.info("Payment is a Single Payment..");
            }
        }


        List<UnderlyingTransaction21> underlyingTransaction21List = new ArrayList<>();

        UnderlyingTransaction21 underlyingTransaction21 = new UnderlyingTransaction21();

        if (groupCancellation) {

            originalGroupHeader10.setGrpCxl(true);

            underlyingTransaction21.setOrgnlGrpInfAndCxl(originalGroupHeader10);

            underlyingTransaction21List.add(underlyingTransaction21);

        } else {

            payments.forEach(payment -> {

                log.info("The payment is of type individual payment.");

                List<IndividualPayment> individualPayments = Arrays.asList((IndividualPayment) payment);

                underlyingTransaction21.setOrgnlGrpInfAndCxl(originalGroupHeader10);

                List<OriginalPaymentInstruction29> originalPaymentInstruction29List = buildOriginalPaymentInstructions(individualPayments, paymentInvestigationCase);
                underlyingTransaction21.getOrgnlPmtInfAndCxl().addAll(originalPaymentInstruction29List);

                underlyingTransaction21List.add(underlyingTransaction21);

            });
        }

        return underlyingTransaction21List;

    }

    private List<OriginalPaymentInstruction29> buildOriginalPaymentInstructions(List<IndividualPayment> individualPayments, final PaymentInvestigationCase paymentInvestigationCase) {

        List<OriginalPaymentInstruction29> originalPaymentInstruction29List = new ArrayList<>();

        log.info("Setting OriginalPaymentInstruction for individual payments of size {}", individualPayments.size());
        individualPayments.forEach(payment -> {

            OriginalGroupInformation29 originalGroupInformation29 = new OriginalGroupInformation29();
            originalGroupInformation29.setOrgnlMsgId(this.getOriginalMessageIdentification(individualPayments.get(0)));
            originalGroupInformation29.setOrgnlMsgNmId("pain.001");
            originalGroupInformation29.setOrgnlCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

            OriginalPaymentInstruction29 originalPaymentInstruction29 = new OriginalPaymentInstruction29();
            originalPaymentInstruction29.setPmtCxlId(paymentInvestigationCase.getId());
            originalPaymentInstruction29.setOrgnlPmtInfId(this.getOriginalMessageIdentification(individualPayments.get(0)));
            originalPaymentInstruction29.setOrgnlGrpInf(originalGroupInformation29);

            PaymentInfoDto paymentInfo = this.paymentInfoUtil.getPaymentInfo(payment);
            originalPaymentInstruction29.setNbOfTxs(String.valueOf(paymentInfo.getNrOfPayments()));
            originalPaymentInstruction29.setCtrlSum(paymentInfo.getControlSum());
            originalPaymentInstruction29.setPmtInfCxl(false);


            List<PaymentCancellationReason4> paymentCancellationReason4List = buildPaymentCancellationReasons(individualPayments, paymentInvestigationCase);
            originalPaymentInstruction29.getCxlRsnInf().addAll(paymentCancellationReason4List);

            List<PaymentTransaction95> paymentTransaction95List = buildPaymentTransactions(individualPayments, paymentInvestigationCase);
            originalPaymentInstruction29.getTxInf().addAll(paymentTransaction95List);

            originalPaymentInstruction29List.add(originalPaymentInstruction29);

        });

        return  originalPaymentInstruction29List;
    }

    private List<PaymentCancellationReason4> buildPaymentCancellationReasons(List<IndividualPayment> individualPayments, final PaymentInvestigationCase paymentInvestigationCase) {

        List<PaymentCancellationReason4> paymentCancellationReason4List = new ArrayList<>();
        individualPayments.forEach(payment -> {

            CancellationReason33Choice cancellationReason33Choice = new CancellationReason33Choice();
            cancellationReason33Choice.setCd(paymentInvestigationCase.getCancellationReason().getCodeName());
           // cancellationReason33Choice.setPrtry("");

            PaymentCancellationReason4 paymentCancellationReason4 = new PaymentCancellationReason4();
            paymentCancellationReason4.setRsn(cancellationReason33Choice);

            paymentCancellationReason4List.add(paymentCancellationReason4);

        });

        return paymentCancellationReason4List;
    }

    private List<PaymentTransaction95> buildPaymentTransactions(List<IndividualPayment> individualPayments, final PaymentInvestigationCase paymentInvestigationCase) {

        List<PaymentTransaction95> paymentTransaction95List = new ArrayList<>();
        individualPayments.forEach(payment -> {

            ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount = new ActiveOrHistoricCurrencyAndAmount();
            activeOrHistoricCurrencyAndAmount.setValue(payment.getAmount().getAmount());
            activeOrHistoricCurrencyAndAmount.setCcy(payment.getAmount().getCurrency().getCodeName());

            DateAndDateTime2Choice dateAndDateTime2Choice = new DateAndDateTime2Choice();
            dateAndDateTime2Choice.setDt(XmlDateUtil.getXmlDateNoTime(paymentInvestigationCase.getUnderlyingInstruction().getRequestedExecutionDate()));
            //dateAndDateTime2Choice.setDtTm(XmlDateUtil.getXmlDate(paymentInvestigationCase.getUnderlyingInstruction().getRequestedExecutionDate()));

            PaymentTransaction95 paymentTransaction95 = new PaymentTransaction95();
            paymentTransaction95.setCxlId(paymentInvestigationCase.getId());

            log.info("Setting the originalInstrId {}", this.getOriginalMessageIdentification(individualPayments.get(0)));
            paymentTransaction95.setOrgnlInstrId(this.getOriginalMessageIdentification(individualPayments.get(0)));
            paymentTransaction95.setOrgnlEndToEndId(payment.getPaymentRelatedIdentifications().stream().findFirst()
                    .map(PaymentIdentification::getEndToEndIdentification)
                    .orElse(""));
            paymentTransaction95.setOrgnlInstdAmt(activeOrHistoricCurrencyAndAmount);
            paymentTransaction95.setOrgnlReqdExctnDt(dateAndDateTime2Choice);
            paymentTransaction95.setOrgnlReqdColltnDt(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

            paymentTransaction95List.add(paymentTransaction95);
        });

        return paymentTransaction95List;
    }
}