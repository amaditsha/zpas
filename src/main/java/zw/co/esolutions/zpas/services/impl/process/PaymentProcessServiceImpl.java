package zw.co.esolutions.zpas.services.impl.process;

import com.hazelcast.util.executor.CompletedFuture;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.enums.PaymentInstrumentCode;
import zw.co.esolutions.zpas.model.BulkPayment;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.process.PaymentInitiationService;
import zw.co.esolutions.zpas.services.iface.process.PaymentProcessService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.ErrorCode;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Service
@Slf4j
@Transactional
public class PaymentProcessServiceImpl implements PaymentProcessService {

    @Autowired
    PaymentInitiationService paymentInitiationService;

    @Autowired
    PaymentsService paymentsService;

    @Override
    public ServiceResponse initiatePayment(String paymentId) {
        Optional<Payment> paymentOptional = paymentsService.getPaymentById(paymentId);
        if (paymentOptional.isPresent()) {
            return initiatePayment(paymentOptional.get());
        }
        return ServiceResponse.builder()
                .responseCode(SystemConstants.RC_ERROR)
                .errorCode(ErrorCode.ZPE109.name())
                .narrative("Payment not found")
                .build();
    }

    @Override
    public ServiceResponse initiatePayment(Payment payment) {
        log.info("Initiating payment");

        PaymentInstrumentCode paymentInstrumentCode = payment.getPaymentInstrument();

        log.info("Payment Instrument Code is: {}|{}", paymentInstrumentCode, paymentInstrumentCode.getCodeName());

        if (PaymentInstrumentCode.CustomerCreditTransfer.equals(paymentInstrumentCode)) {
            log.info("Initiating a Customer Credit Transfer");
            payment.setEntityStatus(EntityStatus.PROCESSING_IN_PROGRESS);
            if(payment instanceof BulkPayment) {
                ((BulkPayment) payment).getGroups().forEach(individualPayment -> {
                    individualPayment.setEntityStatus(EntityStatus.PROCESSING_IN_PROGRESS);
                });
            }
            return paymentInitiationService.initiateCustomerCreditTransfer(payment);
        } else if (PaymentInstrumentCode.CustomerDebitTransfer.equals(paymentInstrumentCode)) {
            log.info("Initiating a Customer Direct Debit");
            payment.setEntityStatus(EntityStatus.PROCESSING_IN_PROGRESS);
            if(payment instanceof BulkPayment) {
                ((BulkPayment) payment).getGroups().forEach(individualPayment -> {
                    individualPayment.setEntityStatus(EntityStatus.PROCESSING_IN_PROGRESS);
                });
            }
            return paymentInitiationService.initiateCustomerDirectDebit(payment);
        } else {
            return ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .errorCode(ErrorCode.ZPE109.name())
                    .narrative("Invalid payment instrument code")
                    .build();
        }
    }
}
