package zw.co.esolutions.zpas.services.impl.payments.exceptions;

import lombok.Data;
import zw.co.esolutions.zpas.services.impl.payments.PaymentEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alfred on 29 Apr 2019
 */
@Data
public class PaymentRequestInvalidArgumentException extends Exception {
    private List<PaymentEntry> invalidEntries;
    public PaymentRequestInvalidArgumentException(String message) {
        super(message);
        this.invalidEntries = new ArrayList<>();
    }

    public PaymentRequestInvalidArgumentException(String message, List<PaymentEntry> invalidEntries) {
        super(message);
        this.invalidEntries = invalidEntries;
    }
}
