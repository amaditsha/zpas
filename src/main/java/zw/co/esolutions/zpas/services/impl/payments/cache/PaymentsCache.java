package zw.co.esolutions.zpas.services.impl.payments.cache;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.domain.PaymentStatusInfo;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.inject.Singleton;

/**
 * Created by alfred on 21 May 2019.
 */
@Slf4j
@Singleton
@Component
public class PaymentsCache {
    Config cfg;
    HazelcastInstance instance;
    IMap<String, PaymentStatusInfo> cache;

    public PaymentsCache() {
        cfg = new Config();
        cfg.setClassLoader(this.getClass().getClassLoader());
        instance = Hazelcast.newHazelcastInstance(cfg);
        cache = instance.getMap(SystemConstants.CACHE_NAME_PAYMENT_INFO);
        log.info("--------- init Payments Hazelcast cache ----------");
    }

    public IMap<String, PaymentStatusInfo> getPaymentStatusInfoCacheInstance() {
        return cache;
    }

}
