package zw.co.esolutions.zpas.services.impl.payments;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.dto.payments.*;
import zw.co.esolutions.zpas.enums.PaymentInstrumentCode;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.enums.PaymentTypeCode;
import zw.co.esolutions.zpas.enums.StatusCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.process.PaymentProcessService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.services.impl.payments.verification.IdentificationVerificationProcessor;
import zw.co.esolutions.zpas.services.impl.process.pojo.StatusReasonDto;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentStatusUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alfred on 28 Feb 2019
 */
@Slf4j
@Service
@Transactional
public class PaymentsServiceImpl implements PaymentsService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Autowired
    private BulkPaymentRepository bulkPaymentRepository;

    @Autowired
    private IndividualPaymentRepository individualPaymentRepository;

    @Autowired
    private PaymentIdentificationRepository paymentIdentificationRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private IndividualPaymentProcessor individualPaymentProcessor;

    @Autowired
    private IdentificationVerificationProcessor identificationVerificationProcessor;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private PaymentProcessService paymentProcessService;

    @Autowired
    private PaymentStatusUtil paymentStatusUtil;

    @Override
    public Optional<Payment> getPaymentById(String id) {
        return paymentRepository.findById(id);
    }

    @Override
    public Optional<Payment> getPaymentByEndToEndId(String id) {
        return paymentIdentificationRepository.findByEndToEndIdentification(id).map(paymentIdentification -> {
            return Optional.ofNullable(paymentIdentification.getPayment());
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<PaymentsStatusDTO> getPaymentsStatusesByClientId(String clientId) {
        final List<Role> clientRoles = roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof CreditorRole || role instanceof DebtorRole)
                .collect(Collectors.toList());
        final Set<Payment> paymentSet = paymentRepository.findAllByPartyRoleIn(clientRoles).stream().collect(Collectors.toSet());
        final long totalPayments = paymentSet.size();
        final long totalFailedPayments = paymentSet.stream().filter(this::checkFailure).count();
        final long totalSuccessfulPayments = totalPayments - totalFailedPayments;

        final Set<BulkPayment> bulkPayments = paymentSet.stream().filter(payment -> payment instanceof BulkPayment)
                .map(payment -> (BulkPayment) payment).collect(Collectors.toSet());
        bulkPayments.addAll(
                paymentSet.stream()
                        .filter(payment -> payment instanceof IndividualPayment)
                        .map(payment -> (IndividualPayment) payment)
                        .filter(individualPayment -> individualPayment.getBulkPayment() != null)
                        .map(individualPayment -> individualPayment.getBulkPayment()).collect(Collectors.toList()));
        final long totalBulkPayments = bulkPayments.size();
        final long totalFailedBulkPayments = bulkPayments.stream().filter(this::checkFailure).count();
        final long totalSuccessfulBulkPayments = totalBulkPayments - totalFailedBulkPayments;


        final List<IndividualPayment> individualPayments = paymentSet.stream()
                .filter(payment -> payment instanceof IndividualPayment)
                .map(payment -> (IndividualPayment) payment)
                .filter(individualPayment -> individualPayment.getBulkPayment() == null)
                .collect(Collectors.toList());
        final long totalIndividualPayments = individualPayments.size();
        final long totalFailedIndividualPayments = individualPayments.stream().filter(this::checkFailure).count();
        final long totalSuccessfulIndividualPayments = totalIndividualPayments - totalFailedIndividualPayments;

        return Optional.ofNullable(PaymentsStatusDTO.builder()

                //All Payment stats
                .totalPayments(totalPayments)
                .totalFailedPayments(totalFailedPayments)
                .totalSuccessfulPayments(totalSuccessfulPayments)

                //Bulk Payments stats
                .totalBulkPayments(totalBulkPayments)
                .totalFailedBulkPayments(totalFailedBulkPayments)
                .totalSuccessfulBulkPayments(totalSuccessfulBulkPayments)

                //Individual Payment Statuses
                .totalIndividualPayments(totalIndividualPayments)
                .totalFailedIndividualPayments(totalFailedIndividualPayments)
                .totalSuccessfulIndividualPayments(totalSuccessfulIndividualPayments)
                .build());
    }

    private boolean checkFailure(Payment payment) {
        final List<PaymentStatus> paymentStatuses = payment.getPaymentStatus();
        paymentStatuses.sort(Comparator.comparing(paymentStatus -> paymentStatus.getDateCreated()));
        if (paymentStatuses.size() == 0) {
            return false;
        }
        final PaymentStatus latestPaymentStatus = paymentStatuses.get(0);
        final boolean cancellationReasonAvailable = latestPaymentStatus.getCancellationReason() != null;
        final boolean mandateRejectionReasonAvailable = latestPaymentStatus.getMandateRejectionReason() != null;
        final boolean suspendedStatusReasonAvailable = latestPaymentStatus.getSuspendedStatusReason() != null;
        final boolean transactionRejectionReasonAvailable = latestPaymentStatus.getTransactionRejectionReason() != null;
        final boolean unmatchedStatusReasonAvailable = latestPaymentStatus.getUnmatchedStatusReason() != null;
        final boolean unsuccessfulStatusReasonAvailable = latestPaymentStatus.getUnsuccessfulStatusReason() != null;
        return cancellationReasonAvailable || mandateRejectionReasonAvailable || suspendedStatusReasonAvailable
                || transactionRejectionReasonAvailable || transactionRejectionReasonAvailable
                || unmatchedStatusReasonAvailable || unsuccessfulStatusReasonAvailable;
    }

    @Override
    public List<Payment> getPaymentsByClientId(String clientId) {
        final List<Role> clientRoles = roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof CreditorRole || role instanceof DebtorRole).collect(Collectors.toList());
        final Set<Payment> paymentSet = paymentRepository.findAllByPartyRoleIn(clientRoles).stream().collect(Collectors.toSet());
        return paymentSet.stream().collect(Collectors.toList());
    }

    @Override
    public Page<IndividualPayment> getPaginatedIndividualPaymentsByBulkPaymentId(String bulkPaymentId, Pageable pageable) {
        return bulkPaymentRepository.findById(bulkPaymentId).map(bulkPayment -> {
            final Page<IndividualPayment> individualPaymentsPage = individualPaymentRepository.findAllByBulkPayment(bulkPayment, pageable);

            log.info("The total number of elements found is: {}", individualPaymentsPage.getTotalElements());
            log.info("The total number of pages found is: {}", individualPaymentsPage.getTotalPages());
            log.info("The size of the content is: {}", individualPaymentsPage.getContent().size());
            return individualPaymentsPage;
        }).orElse(Page.empty());
    }

    @Override
    public List<Payment> getLatestPaymentsByClientId(String clientId) {
        final List<Role> clientRoles = roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof CreditorRole || role instanceof DebtorRole).collect(Collectors.toList());
        final Set<Payment> paymentSet = paymentRepository.findTop25ByPartyRoleInOrderByDateCreatedDesc(clientRoles).stream().collect(Collectors.toSet());
        return paymentSet.stream().collect(Collectors.toList());
    }

    @Override
    public List<Payment> searchPayments(SearchDTO searchDTO) {
        final Set<Payment> paymentSet = new HashSet<>();
        final List<Role> clientRoles = roleRepository.findAllByPlayer_Id(searchDTO.getClientId());
        clientRoles.removeIf(role -> {
            boolean notAPaymentPartyRole = !(role instanceof PaymentPartyRole);
            return notAPaymentPartyRole;
        });

        final String searchPhrase = searchDTO.getSearchPhrase();

        final List<Role> searchedRoles = roleRepository.searchRoleByCustomPartyName(searchPhrase);
        log.info("Found {} client roles.", clientRoles.size());

        if (StringUtils.isEmpty(searchPhrase)) {
            paymentSet.addAll(paymentRepository.findAllByPartyRoleIn(clientRoles));
            log.info("Found {} payments before filtering.", paymentSet.size());
            //filter out those without the specified date created
            log.info("Filter out those without the specified date created");
            filterPaymentsByDateCreated(paymentSet, searchDTO.getOffsetDateCreatedFrom(), searchDTO.getOffsetDateCreatedTo());
            log.info("Found {} payments after filtering by date created.", paymentSet.size());

            //filter out those without the specified value date
            log.info("Filter out those without the specified value date");
            filterPaymentsByValueDate(paymentSet, searchDTO.getOffsetValueDateFrom(), searchDTO.getOffsetValueDateTo());
            log.info("Found {} payments after filtering by value date.", paymentSet.size());

            //filter out those without the specified source acc
            log.info("Filter out those without the specified account");
            filterPaymentsByAccount(paymentSet, searchDTO.getAccountId());
            log.info("Found {} payments after filtering by specified account.", paymentSet.size());

            //filter out those without the specified payment instrument
            log.info("Filter out those without the specified payment instrument");
            filterPaymentsByPaymentInstrument(paymentSet, searchDTO.getPaymentInstrumentCode());
            log.info("Found {} payments after filtering by specified payment instrument.", paymentSet.size());

            //filter out those without the specified payment type
            log.info("Filter out those without the specified payment type");
            filterPaymentsByPaymentType(paymentSet, searchDTO.getPaymentTypeCode());
            log.info("Found {} payments after filtering by specified payment type.", paymentSet.size());
        } else {
            final List<Payment> searchedPayments = paymentRepository.searchPayments(searchPhrase.toLowerCase());
            paymentSet.addAll(paymentRepository.findAllByPartyRoleIn(searchedRoles));
            paymentSet.addAll(searchedPayments);

            //return only payments in which this client plays a role
            paymentSet.removeIf(payment -> {
                final List<PaymentPartyRole> partyRoles = payment.getPartyRole();
                partyRoles.retainAll(clientRoles);
                return partyRoles.size() > 0;
            });
        }
        return paymentSet.stream().collect(Collectors.toList());
    }

    private void filterPaymentsByPaymentType(final Set<Payment> paymentSet, PaymentTypeCode paymentTypeCode) {
        if (paymentTypeCode == null) return;
        paymentSet.removeIf(payment -> !(payment.getType() == paymentTypeCode));
    }

    private void filterPaymentsByPaymentInstrument(final Set<Payment> paymentSet, PaymentInstrumentCode paymentInstrumentCode) {
        if (paymentInstrumentCode == null) return;
        paymentSet.removeIf(payment -> !(payment.getPaymentInstrument() == paymentInstrumentCode));
    }

    private void filterPaymentsByAccount(final Set<Payment> paymentSet, String accountId) {
        if (StringUtils.isEmpty(accountId)) return;
        paymentSet.removeIf(payment -> {
            if (payment.getAccount() == null) {
                return true;
            } else {
                return !payment.getAccount().getId().equals(accountId);
            }
        });
    }

    private void filterPaymentsByValueDate(final Set<Payment> paymentSet, OffsetDateTime valueDateFrom, OffsetDateTime valueDateTo) {
        if (valueDateFrom == null && valueDateTo == null) return;
        if (valueDateFrom == null) {
            paymentSet.removeIf(payment -> payment.getValueDate().isAfter(valueDateTo));
            return;
        }
        if (valueDateTo == null) {
            paymentSet.removeIf(payment -> payment.getValueDate().isBefore(valueDateFrom));
            return;
        }
        paymentSet.removeIf(payment -> {
            final boolean validValueDatePeriod = payment.getValueDate().isAfter(valueDateFrom) &&
                    payment.getValueDate().isBefore(valueDateTo);
            return !validValueDatePeriod;
        });
    }

    private void filterPaymentsByDateCreated(final Set<Payment> paymentSet, OffsetDateTime dateCreatedFrom, OffsetDateTime dateCreatedTo) {
        if (dateCreatedFrom == null && dateCreatedTo == null) return;
        if (dateCreatedFrom == null) {
            paymentSet.removeIf(payment -> payment.getDateCreated().isAfter(dateCreatedTo));
            return;
        }
        if (dateCreatedTo == null) {
            paymentSet.removeIf(payment -> payment.getDateCreated().isBefore(dateCreatedFrom));
            return;
        }
        paymentSet.removeIf(payment -> {
            final boolean validDateCreatedPeriod = payment.getDateCreated().isAfter(dateCreatedFrom) &&
                    payment.getDateCreated().isBefore(dateCreatedTo);
            return !validDateCreatedPeriod;
        });
    }

    @Override
    public List<CustomPartyDTO> getPaymentBeneficiariesInfoByPaymentIds(List<String> paymentIds) {
        log.info("{} payment infos requested.", paymentIds.size());
        if (paymentIds == null || paymentIds.size() == 0) {
            return new ArrayList<>();
        }
        final List<Payment> payments = paymentRepository.getPaymentsByIds(paymentIds);
        log.info("{} payments found.", payments.size());
        return payments.stream().map(payment -> {
            final CustomPartyDTO.CustomPartyDTOBuilder customPartyDTOBuilder = CustomPartyDTO.builder();
            customPartyDTOBuilder.paymentId(payment.getId());
            payment.getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof CreditorRole)
                    .findFirst().ifPresent(creditorRole -> {
                log.info("Creditor role found: {}", creditorRole);
                if (creditorRole.isHasCustomParty()) {
                    Optional.ofNullable(creditorRole.getCustomParty()).ifPresent(customParty -> {
                        final String accountNumber = customParty.getAccountNumber();
                        financialInstitutionService.getFinancialInstitutionById(customParty.getFinancialInstitutionIdentification())
                                .ifPresent(financialInstitution -> {
                                    log.info("Found Financial Institution: ===>{}", financialInstitution);
                                    final String financialInstitutionName = financialInstitution.getName();
                                    log.info("Creditor Bank: {}", financialInstitutionName);
                                    customPartyDTOBuilder.beneficiaryBank(financialInstitutionName);
                                });
                        log.info("Creditor account number: {}", accountNumber);
                        log.info("Creditor name: {}", customParty.getName());
                        customPartyDTOBuilder.beneficiaryName(customParty.getName());
                        customPartyDTOBuilder.beneficiaryAccount(customParty.getAccountNumber());
                    });
                }
            });
            payment.getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof CreditorAgentRole)
                    .findFirst().ifPresent(creditorAgentRole -> {
                log.info("Creditor Agent role found: {}", creditorAgentRole);
                creditorAgentRole.getPlayer().stream().findFirst().ifPresent(rolePlayer -> {
                    if (rolePlayer instanceof FinancialInstitution) {
                        FinancialInstitution financialInstitution = (FinancialInstitution) rolePlayer;
                        customPartyDTOBuilder.beneficiaryBank(financialInstitution.getName());
                    }
                });
            });
            payment.getPartyRole().stream().filter(paymentPartyRole -> paymentPartyRole instanceof CreditorRole)
                    .findFirst().ifPresent(creditorRole -> {
                log.info("Creditor role found: {}", creditorRole);
                creditorRole.getPlayer().stream().findFirst().ifPresent(rolePlayer -> {
                    customPartyDTOBuilder.beneficiaryName(((Party) rolePlayer).getName());
                });
            });
            return customPartyDTOBuilder.build();
        }).collect(Collectors.toList());
    }

    @Override
    public Optional<IndividualPayment> makePayment(MakePaymentDTO makePaymentDTO) throws PaymentRequestInvalidArgumentException {
        return individualPaymentProcessor.createIndividualPayment(makePaymentDTO);
    }

    @Override
    public Optional<Payment> submitPayment(String paymentId) {
        return paymentRepository.findById(paymentId).map(payment -> {
            EntityStatus newStatus = EntityStatus.PENDING_VERIFICATION;
            if(!payment.isVerifyAccountInfo() && payment instanceof BulkPayment) {
                newStatus = EntityStatus.PENDING_APPROVAL;
            }
            if(!payment.verificationNeeded() && payment instanceof IndividualPayment) {
                newStatus = EntityStatus.PENDING_APPROVAL;
            }
            final EntityStatus finalNewStatus = newStatus;
            payment.setEntityStatus(finalNewStatus);
            payment.setVerificationStartTime(OffsetDateTime.now());
            if(payment instanceof BulkPayment) {
                ((BulkPayment) payment).getGroups().forEach(individualPayment -> {
                    payment.setVerificationStartTime(OffsetDateTime.now());
                    if(individualPayment.getEntityStatus() != EntityStatus.DISAPPROVED) {
                        individualPayment.setEntityStatus(finalNewStatus);
                    }
                });
            }
            if(finalNewStatus == EntityStatus.PENDING_VERIFICATION) {
                try {
                    identificationVerificationProcessor.initiateIdentificationVerificationRequest(payment);
                } catch (PaymentRequestInvalidArgumentException e) {
                    log.error("Payment request invalid: {}", e.getMessage());
                    e.printStackTrace();
                }
            }
            return Optional.ofNullable(payment);
        }).orElse(Optional.empty());
    }

    @Override
    public void sendIdentificationRequest(String paymentId) throws PaymentRequestInvalidArgumentException {
        final Optional<Payment> paymentOptional = paymentRepository.findById(paymentId);
        if(!paymentOptional.isPresent()) {
            log.error("Payment not found. Could not send Identification Verification request.");
            return;
        }
        if(paymentOptional.isPresent()) {
            final Payment payment = paymentOptional.get();
            identificationVerificationProcessor.initiateIdentificationVerificationRequest(payment);
        }
    }

    @Override
    public Optional<Payment> approvePayment(String paymentId) {
        return paymentRepository.findById(paymentId).map(payment -> {
            if (payment.getEntityStatus() == EntityStatus.DRAFT || payment.getEntityStatus() == EntityStatus.PENDING_VERIFICATION) {
                log.error("Payment has not been submitted, can't be approved.");
                return Optional.ofNullable(payment);
            }
            payment.setEntityStatus(EntityStatus.PENDING_DISPATCH);
            if(payment instanceof BulkPayment) {
                ((BulkPayment) payment).getGroups().forEach(individualPayment -> {
                    if(individualPayment.getEntityStatus() != EntityStatus.DISAPPROVED) {
                        individualPayment.setEntityStatus(EntityStatus.PENDING_DISPATCH);
                    }
                });
            }
            ServiceResponse serviceResponse = paymentProcessService.initiatePayment(payment.getId());
            log.info("Initiating payment: Narrative: {}, Service Response: {}", serviceResponse.getNarrative(), serviceResponse.getResponseCode());
            return Optional.ofNullable(payment);
        }).orElse(Optional.empty());
    }

    @Override
    public void updateBulkPaymentStatuses(String bulkId) {
        bulkPaymentRepository.findById(bulkId).ifPresent(bulkPayment -> {
            final List<IndividualPayment> individualPayments = bulkPayment.getGroups();

            log.info("Checking bulk with reference: {}", bulkPayment.getEndToEndId());

            final int totalNumberOfTransactions = bulkPayment.getGroups().size();

            log.info("No. of transactions in bulk: {}", totalNumberOfTransactions);

            final List<PaymentStatusCode> paymentStatuses = individualPayments.stream()
                    .filter(payment -> payment.getLatestPaymentStatus().isPresent())
                    .map(individualPayment -> individualPayment.getLatestPaymentStatus().get())
                    .map(paymentStatus -> paymentStatus.getStatus())
                    .collect(Collectors.toList());

            final int numberOfPendingTransactions = paymentStatuses.stream()
                    .filter(status -> !isTerminalStatus(status))
                    .collect(Collectors.toList())
                    .size();

            log.info("Number of pending transactions : {}", numberOfPendingTransactions);

            log.info("Number of completed transactions : {}", totalNumberOfTransactions - numberOfPendingTransactions);

            if (numberOfPendingTransactions > 0) {
                log.info("There are pending transactions.. ignore this bulk");
            } else {
                log.info("This bulk has finished processing.. adding to notification list");

                boolean hasAcceptedStatuses = paymentStatuses.stream()
                        .filter(status -> status.name().startsWith("Accepted"))
                        .findAny()
                        .isPresent();

                boolean hasAcceptedSettlementCompletedStatuses = paymentStatuses.stream()
                        .filter(status -> PaymentStatusCode.AcceptedSettlementCompleted.equals(status))
                        .findAny()
                        .isPresent();

                log.info("Has Accepted_ statuses in payments: {}", hasAcceptedStatuses);
                log.info("Has AcceptedSettlementCompleted statuses in payments: {}", hasAcceptedSettlementCompletedStatuses);

                bulkPayment.setEntityStatus(EntityStatus.PROCESSED);
                bulkPayment.getGroups().forEach(individualPayment -> {
                    individualPayment.setEntityStatus(EntityStatus.PROCESSED);
                });

                PaymentStatus paymentStatus = new PaymentStatus();
                paymentStatus.setId(UUID.randomUUID().toString());
                paymentStatus.setDateCreated(OffsetDateTime.now());
                paymentStatus.setLastUpdated(OffsetDateTime.now());
                paymentStatus.setPayment(bulkPayment);
                paymentStatus.setEntityStatus(EntityStatus.ACTIVE);

                paymentStatus.setStatus(hasAcceptedStatuses ?
                        (hasAcceptedSettlementCompletedStatuses ?
                                PaymentStatusCode.AcceptedSettlementCompleted : PaymentStatusCode.Accepted)
                        : PaymentStatusCode.Rejected);
                log.info("Payment status is {}", paymentStatus);

                final PaymentStatus savedPaymentStatus = paymentStatusRepository.save(paymentStatus);
                log.info("Saved payment status. Now saving bulk payment");

                bulkPayment.addPaymentStatus(savedPaymentStatus);
                log.info("Added status to bulk payment. Now saving bulk payment.");

                bulkPaymentRepository.save(bulkPayment);
                log.info("Bulk payment saved.");
            }

        });
    }

    private boolean isTerminalStatus(PaymentStatusCode paymentStatusCode) {
        if (PaymentStatusCode.Accepted.equals(paymentStatusCode)
                || PaymentStatusCode.AcceptedSettlementCompleted.equals(paymentStatusCode)
                || PaymentStatusCode.Rejected.equals(paymentStatusCode)
                || PaymentStatusCode.AcceptedCancellationRequest.equals(paymentStatusCode)
                || PaymentStatusCode.RejectedCancellationRequest.equals(paymentStatusCode)) {
            return true;
        }

        return false;
    }

    @Override
    public Optional<Payment> rejectPayment(String paymentId) {
        return paymentRepository.findById(paymentId).map(payment -> {
            if (payment.getEntityStatus() == EntityStatus.DRAFT && payment instanceof BulkPayment) {
                log.error("Bulk Payment has not been submitted, can't be rejected. Delete and upload a fresh instance.");
                return Optional.ofNullable(payment);
            }
            payment.setEntityStatus(EntityStatus.DISAPPROVED);
            if(payment instanceof BulkPayment) {
                ((BulkPayment) payment).getGroups().forEach(individualPayment -> {
                    individualPayment.setEntityStatus(EntityStatus.DISAPPROVED);
                });
            }
            final Payment saved = paymentRepository.save(payment);
            return Optional.ofNullable(saved);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<Payment> deletePayment(String paymentId) {
        return paymentRepository.findById(paymentId).map(payment -> {
            if (payment.getEntityStatus() != EntityStatus.DRAFT) {
                log.error("Payment cannot be deleted. It's not in draft status");
            }
            payment.setEntityStatus(EntityStatus.DELETED);
            if(payment instanceof BulkPayment) {
                ((BulkPayment) payment).getGroups().forEach(individualPayment -> {
                    individualPayment.setEntityStatus(EntityStatus.DELETED);
                });
            }
            return Optional.ofNullable(payment);
        }).orElse(Optional.empty());
    }

    @Override
    public Optional<Payment> resolvePayment(ResolvePaymentDTO resolvePaymentDTO) {
        Optional<Payment> optionalPayment = this.getPaymentById(resolvePaymentDTO.getPaymentId());
        Payment payment;

        List<StatusReasonDto> statusReasonDtoList = Arrays.asList(new StatusReasonDto(resolvePaymentDTO.getReason(),
                Arrays.asList("Manually resolved payment.")));

        if(optionalPayment.isPresent()){
            payment = optionalPayment.get();
            paymentStatusUtil.updatePaymentStatus(payment,
                    PaymentStatusCode.valueOf(resolvePaymentDTO.getNewStatus()), statusReasonDtoList);
            if(payment instanceof BulkPayment &&
                    !resolvePaymentDTO.getNewStatus().equals(PaymentStatusCode.PartiallyAccepted.name()) &&
                    !resolvePaymentDTO.getNewStatus().equals(PaymentStatusCode.PartiallyAcceptedCancellationRequest.name())){
                BulkPayment bulkPayment = (BulkPayment) payment;
                bulkPayment.getGroups().forEach(individualPayment -> {
                    paymentStatusUtil.updatePaymentStatus(individualPayment,
                            PaymentStatusCode.valueOf(resolvePaymentDTO.getNewStatus()), statusReasonDtoList);
                });
            }
            return Optional.of(payment);
        }
        return Optional.empty();
    }

    @Override
    public List<Payment> getPaymentsByStatusCode(PaymentStatusCode paymentStatusCode, String bulkPaymentId){
        List<Payment> payments = new ArrayList<>();

        Optional<Payment> optionalPayment = paymentRepository.findById(bulkPaymentId);

        if(optionalPayment.isPresent() && optionalPayment.get() instanceof BulkPayment){
            BulkPayment bulkPayment = (BulkPayment) optionalPayment.get();

            List<IndividualPayment> individualPayments = bulkPayment.getGroups();
            individualPayments.stream()
                    .filter(individualPayment ->
                            individualPayment.getUnformattedLatestPaymentStatusString().equals(paymentStatusCode.name()))
                    .forEach(individualPayment -> payments.add(individualPayment));
        }

        return payments;
    }

    @Override
    public List<Payment> getPaymentsByEntityStatus(EntityStatus entityStatus, String clientId) {
        final List<Role> clientRoles = roleRepository.findAllByPlayer_Id(clientId).stream()
                .filter(role -> role instanceof CreditorRole || role instanceof DebtorRole).collect(Collectors.toList());
        final Set<Payment> paymentSet = paymentRepository.findAllByPartyRoleIn(clientRoles).stream().collect(Collectors.toSet());
        List<Payment> payments = paymentSet.stream().filter(payment -> payment.getEntityStatus().equals(entityStatus))
                .collect(Collectors.toList());

        //return paymentRepository.findAllByEntityStatus(entityStatus);
        return payments;
    }
}
