package zw.co.esolutions.zpas.services.impl.process.builder;

import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.pain008_001_07.CustomerDirectDebitInitiationV07;
import zw.co.esolutions.zpas.iso.msg.pain008_001_07.GroupHeader55;
import zw.co.esolutions.zpas.model.Payment;

import java.util.ArrayList;

@Component
public class CustomerDirectDebitMessageBuilder {

    public CustomerDirectDebitInitiationV07 buildCustomerDirectDebitInitiationMsg(Payment payment) {
        CustomerDirectDebitInitiationV07 customerDirectDebitInitiationV07 = new CustomerDirectDebitInitiationV07();
        customerDirectDebitInitiationV07.setGrpHdr(new GroupHeader55());
        customerDirectDebitInitiationV07.getPmtInf().addAll(new ArrayList<>());
        customerDirectDebitInitiationV07.getSplmtryData().addAll(new ArrayList<>());

        return customerDirectDebitInitiationV07;
    }
}
