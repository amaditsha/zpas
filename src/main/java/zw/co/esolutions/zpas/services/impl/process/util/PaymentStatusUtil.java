package zw.co.esolutions.zpas.services.impl.process.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.PaymentStatus;
import zw.co.esolutions.zpas.model.StatusReason;
import zw.co.esolutions.zpas.repository.PaymentStatusRepository;
import zw.co.esolutions.zpas.repository.StatusReasonRepository;
import zw.co.esolutions.zpas.services.impl.process.pojo.StatusReasonDto;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class PaymentStatusUtil {

    @Autowired
    PaymentStatusRepository paymentStatusRepository;

    @Autowired
    StatusReasonRepository statusReasonRepository;

    public void updatePaymentStatus(Payment payment,
                                    PaymentStatusCode paymentStatusCode,
                                    List<StatusReasonDto> statusReasons) {
        log.info("Creating payment status...");
        //update payment status
        PaymentStatus paymentStatus = new PaymentStatus();
        paymentStatus.setStatus(paymentStatusCode);
        paymentStatus.setStatusDescription(paymentStatusCode.name());
        paymentStatus.setPayment(payment);
        paymentStatus.setId(UUID.randomUUID().toString());
        paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
        paymentStatus.setDateCreated(OffsetDateTime.now());
        paymentStatus.setLastUpdated(OffsetDateTime.now());

        paymentStatus = paymentStatusRepository.save(paymentStatus);

        for (StatusReasonDto statusReasonDto: statusReasons) {
            log.info("Creating status reason..");
            StatusReason statusReason = new StatusReason();
            statusReason.setId(UUID.randomUUID().toString());
            statusReason.setEntityStatus(EntityStatus.ACTIVE);
            statusReason.setDateCreated(OffsetDateTime.now());
            statusReason.setLastUpdated(OffsetDateTime.now());
            statusReason.setStatus(paymentStatus);
            statusReason.setReason(statusReasonDto.code);
            statusReason.setAdditionalInfo(statusReasonDto.narratives);

            statusReasonRepository.save(statusReason);
        }

        log.info("Updated payment status and reasons successfully for payment: {}", payment.getId());
    }

    public void updateTransactionStatus(Payment payment,
                                        PaymentStatusCode transactionStatusCode,
                                        List<StatusReasonDto> statusReasons) {
        log.info("Create transaction status..");
        //update payment status
        this.updatePaymentStatus(payment, transactionStatusCode, statusReasons);
    }
}
