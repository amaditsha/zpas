package zw.co.esolutions.zpas.services.impl.payments.zimra.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alfred on 21 August 2019
 */
@XmlRootElement(name = "ValidateAssessment")
public class AssessmentValidationRequest {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "GetRequest")
    private AssessmentValidationGetRequest assessmentValidationGetRequest;

    public AssessmentValidationGetRequest getAssessmentValidationGetRequest() {
        return assessmentValidationGetRequest;
    }

    public void setAssessmentValidationGetRequest(AssessmentValidationGetRequest assessmentValidationGetRequest) {
        this.assessmentValidationGetRequest = assessmentValidationGetRequest;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}
