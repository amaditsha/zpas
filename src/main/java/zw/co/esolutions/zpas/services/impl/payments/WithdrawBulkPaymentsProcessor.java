package zw.co.esolutions.zpas.services.impl.payments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.repository.BulkPaymentRepository;

@Component
public class WithdrawBulkPaymentsProcessor {

    private final BulkPaymentRepository bulkPaymentRepository;

    @Autowired
    public WithdrawBulkPaymentsProcessor(BulkPaymentRepository bulkPaymentRepository) {
        this.bulkPaymentRepository = bulkPaymentRepository;
    }
}
