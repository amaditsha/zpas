package zw.co.esolutions.zpas.services.impl.process;

import org.springframework.beans.factory.annotation.Autowired;
import zw.co.esolutions.zpas.enums.CancellationReasonCode;
import zw.co.esolutions.zpas.enums.PaymentInstrumentCode;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.services.iface.investigationcase.InvestigationCaseService;
import zw.co.esolutions.zpas.services.iface.process.InvestigationCaseProcessService;
import zw.co.esolutions.zpas.utilities.enums.ErrorCode;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import java.util.Optional;

public class InvestigationCaseProcessServiceImpl implements InvestigationCaseProcessService {
    @Autowired
    InvestigationCaseProcessService investigationCaseProcessService;

    @Autowired
    InvestigationCaseInitiationServiceImpl investigationCaseInitiationService;

    @Autowired
    InvestigationCaseService investigationCaseService;

    @Override
    public ServiceResponse withdrawBulk(PaymentInvestigationCase paymentInvestigationCase) {
        CancellationReasonCode cancellationReasonCode = paymentInvestigationCase.getCancellationReason();

        if (    CancellationReasonCode.RequestedByCustomer.equals(cancellationReasonCode)){
            return investigationCaseInitiationService.initiateCustomerPaymentCancellationRequest(paymentInvestigationCase);
        }else {
            return  ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .errorCode(ErrorCode.ZPE109.name())
                    .narrative("Invalid payment instrument code")
                    .build();
        }
    }

    @Override
    public ServiceResponse recallEFTCredits(PaymentInvestigationCase paymentInvestigationCase) {
        CancellationReasonCode cancellationReasonCode = paymentInvestigationCase.getCancellationReason();

        if (CancellationReasonCode.RequestedByCustomer.equals(cancellationReasonCode)){
            return investigationCaseInitiationService.initiateCustomerPaymentCancellationRequest(paymentInvestigationCase);
        }else {
            return  ServiceResponse.builder()
                    .responseCode(SystemConstants.RC_ERROR)
                    .errorCode(ErrorCode.ZPE109.name())
                    .narrative("Invalid payment instrument code")
                    .build();
            }
    }

}
