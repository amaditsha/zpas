package zw.co.esolutions.zpas.services.iface.taxclearance;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import zw.co.esolutions.zpas.dto.TaxClearanceDTO;
import zw.co.esolutions.zpas.model.taxclearance.TaxClearance;
import zw.co.esolutions.zpas.services.impl.taxclearance.TaxClearanceJsonResponse;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 22 May 2019
 */
public interface TaxClearanceService {
    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    SseEmitter getBPNVerificationMessages(String bpn);
    List<TaxClearance> getTaxClearances();
    Optional<TaxClearance> getCurrentTaxClearanceByBPN(String bpn);
    Optional<TaxClearance> createTaxClearance(TaxClearanceDTO taxClearanceDTO);
    Optional<TaxClearance> updateTaxClearance(TaxClearanceDTO taxClearanceDTO);
    List<TaxClearance> refreshTaxClearances(List<TaxClearanceJsonResponse> taxClearanceJsons);
    Optional<TaxClearanceJsonResponse> verifyTaxClearanceByBPN(String bpn);
}
