package zw.co.esolutions.zpas.services.iface.agreement;

import zw.co.esolutions.zpas.model.CashAccountContract;
import zw.co.esolutions.zpas.model.RuleItem;

import java.util.List;
import java.util.Optional;

public interface CashAccountContractService {
    CashAccountContract createCashAccountContract(CashAccountContract cashAccountContract);
    CashAccountContract updateCashAccountContract(CashAccountContract cashAccountContract);
    CashAccountContract deleteCashAccountContract(String id);
    Optional<CashAccountContract> getCashAccountContractById(String signatureId);
    List<CashAccountContract> getCashAccountContracts();
}
