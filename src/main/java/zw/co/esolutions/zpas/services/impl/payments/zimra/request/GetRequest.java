package zw.co.esolutions.zpas.services.impl.payments.zimra.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by alfred on 21 August 2019
 */
public class GetRequest {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    @XmlElement(name = "bpNumber")
    private String bpNumber;

    @XmlElement(name = "taxCode")
    private String taxCode;

    public String getBpNumber() {
        return bpNumber;
    }

    public void setBpNumber(String bpNumber) {
        this.bpNumber = bpNumber;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }
}