package zw.co.esolutions.zpas.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniquePersonValidator implements ConstraintValidator<UniqueIdentityCardNumber, String> {
    @Autowired
    private ClientService clientService;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value != null && !clientService.isIdentityCardNumberAlreadyInUse(value);
    }
}
