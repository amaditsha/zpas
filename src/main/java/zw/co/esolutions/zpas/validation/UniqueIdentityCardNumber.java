package zw.co.esolutions.zpas.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = UniquePersonValidator.class)
@Retention(RUNTIME)
@Target({ FIELD, METHOD })
public @interface UniqueIdentityCardNumber {

    String message() default "There is already a person with this identity card number!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default{};

}
