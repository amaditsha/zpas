package zw.co.esolutions.zpas.configs;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="bmg.parameters")
@Data
public class BmgProps {
  private String bmgUrlSingle = "https://mobile.esolutions.co.zw/bmg/api/single";
  private String bmgUrlBatch  = "https://mobile.esolutions.co.zw/bmg/api/bulk";
  private String defaultUsername = "<BMG_CUSTOMER_ID>";
  private String defaultPassword = "<BMG_PASSWORD>";
}