package zw.co.esolutions.zpas.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import zw.co.esolutions.zpas.process.AuditLogger;

@Configuration
public class AuditLoggerConfiguration {
    @Bean
    public AuditLogger auditLoggerUtil(){
        return new AuditLogger();
    }
}
