package zw.co.esolutions.zpas.configs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Enable and configure Stomp over WebSocket.
 */
@Slf4j
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Value("${stomp.broker.relay.host}")
    public String stompBrokerRelayHost;
    @Value("${stomp.broker.relay.port}")
    public Integer stompBrokerRelayPort;
    @Value("${stomp.broker.relay.login}")
    public String stompBrokerRelayLogin;
    @Value("${stomp.broker.relay.passcode}")
    public String stompBrokerRelayPasscode;

    /**
     * Register Stomp endpoints: the url to open the WebSocket connection.
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {

        // Register the "/ws" endpoint, enabling the SockJS protocol.
        // SockJS is used (both client and server side) to allow alternative
        // messaging options if WebSocket is not available.
        registry.addEndpoint("/ws").withSockJS();
        return;
    }

    /**
     * Configure the message broker.
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {

        // Enable a simple memory-based message broker to send messages to the
        // client on destinations prefixed with "/queue".
        // Simple message broker handles subscription requests from clients, stores
        // them in memory, and broadcasts messages to connected clients with
        // matching destinations.
        config.enableSimpleBroker("/queue");

        // Use this for enabling a Full featured broker like RabbitMQ
//        config.enableStompBrokerRelay("/queue")
//                .setRelayHost(stompBrokerRelayHost)
//                .setRelayPort(stompBrokerRelayPort)//61613
//                .setClientLogin(stompBrokerRelayLogin)
//                .setClientPasscode(stompBrokerRelayPasscode);

        return;
    }

} // class WebSocketConfig
