package zw.co.esolutions.zpas.configs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.services.iface.debitorder.DebitOrderService;

@Component
@Slf4j
public class ScheduledDebitOrderService {
    @Autowired
    DebitOrderService debitOrderService;

    @Scheduled(fixedDelay = 100000)
    public void timerHandler() {
        log.info("Timer {}s. Checking debit order new SFI files", 100);
        debitOrderService.processDebitOrderBatchResponse();
    }
}
