package zw.co.esolutions.zpas.enums;

public enum ReservationTypeCode {
    
     CashReservation("CARE"),
     UrgentPaymentReservation("UPAR"),
     NetSSSReservation("NSSR"),
     HighlyUrgentPaymentReservation("HPAR"),
     ThresholdForInvestment("THRE"),
     Blocked("BLKD");

     private  String codeName;

     ReservationTypeCode(String codeName){
         this.codeName  = codeName;
     }

     public String getCodeName(){
         return codeName;
     }
}

