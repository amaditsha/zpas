package zw.co.esolutions.zpas.enums;

public enum BillingCurrencyTypeCode {
    Account("ACCT"),
    Settlement("STLM"),
    Pricing("PRCG"),
    Host("HOST");

    private String codeName;

    BillingCurrencyTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
