package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum PaymentMethodCode {
    Cheque("CHK"),
    CreditTransfer("TRF"),
    DirectDebit("DD"),
    TransferAdvice("TRA"),
    Direct("DIRE"),
    Classical("CLAS");

    private String codeName;

    public static PaymentMethodCode valueOfCodeName(String code) {
        return Arrays.stream(values()).filter(v -> v.getCodeName().equals(code)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown Code: " + code));
    }

    PaymentMethodCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

}
