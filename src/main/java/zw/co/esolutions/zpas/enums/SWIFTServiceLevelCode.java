package zw.co.esolutions.zpas.enums;

public enum SWIFTServiceLevelCode {

   SWIFTPay("SPAY"),
   Priority("SPRI"),
   Standard("SSTD");

    private String codeName;

    SWIFTServiceLevelCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
