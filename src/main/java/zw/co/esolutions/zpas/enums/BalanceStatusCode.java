package zw.co.esolutions.zpas.enums;

public enum BalanceStatusCode {
    Pending("PDNG"),
    Settled("STLD"),
    Suspended("SUSP"),
    SettledUnregistered("SETU"),
    SettledRegistered("SETR"),
    None("NONE");

    private String codeName;

    BalanceStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
