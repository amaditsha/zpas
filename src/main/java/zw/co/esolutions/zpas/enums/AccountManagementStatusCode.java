package zw.co.esolutions.zpas.enums;

public enum AccountManagementStatusCode {
    Received("RECE"),
    Accepted("ACCP"),
    ProcessingOngoing("EXEC"),
    SentToNextParty("STNP"),
    None("NONE");

    private String codeName;

    AccountManagementStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
