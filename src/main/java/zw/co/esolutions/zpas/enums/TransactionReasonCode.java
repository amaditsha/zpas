package zw.co.esolutions.zpas.enums;

public enum TransactionReasonCode {
    None("NONE"),
    IncorrectAccountNumber("AC01"),
    NonNumericAccountNumber("AC02"),
    InvalidAccountNumberForClearingCode("AC03"),
    ClosedAccountNumber("AC04"),
    InvalidAccountNumberWithInstitution("AC05"),
    BlockedAccount("AC06"),
    ZeroAmount("AM01"),
    NotAllowedAmount("AM02"),
    NotAllowedCurrency("AM03"),
    InsufficientFunds("AM04"),
    Duplication("AM05"),
    TooLowAmount("AM06"),
    BlockedAmount("AM07"),
    ChargeDisagreement("AM08"),
    InconsistentWithEndCustomer("BE01"),
    UnknownCreditor("BE02"),
    NoLongerValidCreditor("BE03"),
    MissingCreditorAddress("BE04"),
    UnrecognisedInitiatingParty("BE05"),
    TransactionForbidden("AG01"),
    InvalidBankOperationCode("AG02"),
    InvalidDate("DT01"),
    NotSpecifiedReason("MS01"),
    UnknownAccount("PY01"),
    NotUniqueTransactionReference("RF01"),
    BankIdentifierIncorrect("RC01"),
    NonNumericRoutingCode("RC02"),
    NotValidRoutingCode("RC03"),
    ClosedBranch("RC04"),
    CutOffTime("TM01"),
    CorrespondentBankNotPossible("ED01"),
    TransactionReasonNonReportable("ED02"),
    BalanceInfoRequested("ED03"),
    ChargeDetailsNotCorrect("ED04"),
    NotSpecifiedReasonAgentGenerated("MS03"),
    NotSpecifiedReasonCustomerGenerated("MS02"),
    UnknownEndCustomer("BE06"),
    MissingDebtorAddress("BE07"),
    WrongAmount("AM09"),
    InvalidControlSum("AM10"),
    NoMandate("MD01"),
    MissingMandatoryInformationInMandate("MD02"),
    InvalidFileFormatForOtherReasonThanGroupingIndicator("MD03"),
    InvalidFileFormatForGroupingIndicator("MD04"),
    RefundRequestByEndCustomer("MD06"),
    EndCustomerDeceased("MD07"),
    CollectionNotDue("MD05"),
    InvalidName("AC07"),
    SettlementFailed("ED05"),
    Narrative("NARR");

    private String codeName;

    TransactionReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
