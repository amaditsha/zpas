package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum DataSetTypeCode {
    None("NONE"),
    TransportDataSet("TRDS"),
    CommercialDataSet("CODS"),
    InsuranceDataSet("INDS"),
    CertificateDataSet("CEDS"),
    OtherCertificateDataSet("OCDS")
    ;

    private String codeName;

    DataSetTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
