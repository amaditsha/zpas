package zw.co.esolutions.zpas.enums;

public enum StatusCode {
    None("NONE"),
   PendingProcessing("PPRC"),
   AcknowledgedAccepted("PAAC"),
   Rejected("REJT"),
   Accepted("PACK"),
   Completed("COMP"),
   NotReceived("NOIN"),
   Cancelled("CAND"),
   BeingCancelled("CANP"),
   ReceivedByIssuerOrRegistrar("RCIS"),
   Pending("PDNG"),
   StandingInstruction("STIN"),
   Queued("QUED"),
   CancelledBySubcustodian("CSUB");

    private String codeName;

    StatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
