package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum MemberStatusCode {
    Enabled("ENBL"),
    Disabled("DSBL"),
    Deleted("DLTD"),
    Joining("JOIN")
    ;

    private String codeName;

    MemberStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
