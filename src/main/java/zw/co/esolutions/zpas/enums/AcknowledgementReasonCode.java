package zw.co.esolutions.zpas.enums;

public enum AcknowledgementReasonCode {
    AccountServicerDeadlineMissed("ADEA"),
    MarketPracticeRuleDiscrepency("SMPG"),
    MarketDeadlineMissed("LATE"),
    NotStraightThroughProcessing("NSTP"),
    AcceptedWithoutVotingRights("RQWV"),
    Other("OTHR"),
    ConditionalCurrency("CDCY"),
    ConditionalRegistrar("CDRG"),
    ConditionalRealignement("CDRE"),
    NarrativeReason("NARR");

    private String codeName;

    AcknowledgementReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
