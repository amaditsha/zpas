package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum PaymentInstrumentCode {
    BankDebitTransfer("BDT"),
    BankCreditTransfer("BCT"),
    CustomerDebitTransfer("CDT"),
    CustomerCreditTransfer("CCT"),
    Cheque("CHK"),
    BookTransfer("BKT"),
    DebitCardPayment("DCP"),
    CreditCardPayment("CCP"),
    Return("RTI"),
    CancellationRequest("CAN")
    ;
    private String codeName;

    public static PaymentInstrumentCode valueOfCodeName(String code) {
        return Arrays.stream(values()).filter(v -> v.getCodeName().equals(code)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown Code: " + code));
    }

    PaymentInstrumentCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
