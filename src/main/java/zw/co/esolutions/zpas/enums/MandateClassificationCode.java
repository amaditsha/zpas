package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum MandateClassificationCode {

    Fixed("FIXE"),
    Variable("VARI"),
    UsageBased("USGB")
    ;

    private String codeName;

    MandateClassificationCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
