package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum TransactionCancellationStatusReasonCode {
    ValidMandateSupplied("1"),
    CustomerDecision("2"),
    UnableToAction("3"),
    UnableToContactCustomer("4"),
    CancellationAppliedToTransaction("5"),
    PendingCancellation("6"),
    InsufficientFunds("7"),
    LegalDecision("8"),
    NoOriginalTransactionReceived("9"),
    ClosedAccountNumber("A"),
    AlreadyActioned("B"),
    TBD("C");

    private String codeName;

    TransactionCancellationStatusReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
