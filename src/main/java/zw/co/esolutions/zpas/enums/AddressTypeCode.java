package zw.co.esolutions.zpas.enums;

public enum AddressTypeCode {
    None("NONE"),
    Residential("HOME"),
    Business("BIZZ"),
    Postal("ADDR"),
    POBox("PBOX"),
    MailTo("MLTO"),
    DeliveryTo("DLVY");

    private String codeName;

    AddressTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
