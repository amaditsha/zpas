package zw.co.esolutions.zpas.enums;

public enum ReasonBlockedCode {

    Bankruptcy("BKRP"),
    Commitment("CMNT"),
    Confiscation("CNFS"),
    MortisCausa("MORT"),
    Pledged("PLDG"),
    Reregistration("TRPE"),
    Transfer("TRNF"),
    None("NONE");

    private String codeName;

    ReasonBlockedCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
