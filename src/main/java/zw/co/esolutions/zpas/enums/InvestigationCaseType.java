package zw.co.esolutions.zpas.enums;

public enum  InvestigationCaseType {
    ClaimNonReceipt("Claim Non Receipt"),
    PaymentCancellation("Payment Cancellation");

    private String codeName;

    InvestigationCaseType(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
