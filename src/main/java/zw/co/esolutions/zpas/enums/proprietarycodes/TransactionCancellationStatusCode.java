package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum TransactionCancellationStatusCode {
    RejectedCancellationRequest("RJCR"),
    AcceptedCancellationRequest("ACCR"),
    PendingCancellationRequest("PDCR"),
    ResolvedClaimNonReceipt("RCNR");

    private String codeName;

    TransactionCancellationStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
