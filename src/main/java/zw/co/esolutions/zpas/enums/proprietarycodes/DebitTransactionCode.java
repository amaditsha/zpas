package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum  DebitTransactionCode {
    InsurancePayment("21"),
    PensionFundContribution("22"),
    MedicalAidFundContribution("23"),
    UnitTrustContribution("26"),
    CharitableOrReligiousContribution("28"),
    HPPayment("31"),
    AccountRepayment("32"),
    LoanRepaymentOtherThanMortgage("33"),
    RentalLeaseOtherThanProperty("34"),
    ServiceChargeMaintenanceOfServiceAgreements("35"),
    ServiceChargeVariableAmounts("36"),
    ValueAddedTaxVatAllocation("37"),
    RentProperty("41"),
    BondRepayment("42"),
    BankUseDebitTransfer("44"),
    BankUseChequeCardDebits("46"),
    IPSUseOnlyFinancialReturnOfDisputesGreaterThan40Days("49"),
    MunicipalAccountsWaterLights("51"),
    MunicipalAccounts("52"),
    TelephoneAccounts("53"),
    BankUseCreditMerchantElectronicFundTransfer("54"),
    BankUseServiceChargeExemptFromDutyAndServiceFee("56"),
    BankUseGarageCard("57"),
    BankUseServiceCharge("58");

    private String codeName;

    DebitTransactionCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
