package zw.co.esolutions.zpas.enums;

public enum AgreementFrameworkCode {
    FBAAgreement("FBAA"),
    BBAAgreement("BBAA"),
    GermanRahmenvertragAgreement("DERV"),
    ISDAAgreement("ISDA"),
    NoReference("NONR");

    private String codeName;

    AgreementFrameworkCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
