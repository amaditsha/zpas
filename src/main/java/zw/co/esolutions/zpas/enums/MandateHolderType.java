package zw.co.esolutions.zpas.enums;

public enum MandateHolderType {
    INITIATE,
    AUTHORISE,
    BOTH
}