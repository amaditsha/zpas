package zw.co.esolutions.zpas.enums;

public enum ServiceLevelCode {

    SameDayValue("SDVA"),
    SingleEuroPaymentsArea("SEPA"),
    EBAPriorityService("PRPT");

    private String codeName;

    ServiceLevelCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
