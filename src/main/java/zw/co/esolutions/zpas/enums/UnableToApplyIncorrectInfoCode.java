package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum UnableToApplyIncorrectInfoCode {

    IncorrectRelatedReference("IN01"),
    IncorrectBankOperationCode("IN02"),
    IncorrectInstructionCode("IN03"),
    IncorrectRequestedExecutionDate("IN04"),
    IncorrectValueDate("IN05"),
    IncorrectInterbankSettledAmount("IN06"),
    IncorrectDebtor("IN07"),
    IncorrectDebtorAccount("IN08"),
    IncorrectReceiverCorrespondent("IN09"),
    IncorrectThirdReimbursementInstitution("IN10"),
    IncorrectPaymentScheme("IN11"),
    IncorrectAccountOfBeneficiaryInstitution("IN12"),
    IncorrectCreditor("IN13"),
    IncorrectCreditorAccount("IN14"),
    IncorrectRemittanceInformation("IN15"),
    IncorrectPaymentPurpose("IN16"),
    IncorrectDetailsOfCharges("IN17"),
    IncorrectSenderToReceiverInformation("IN18"),
    IncorrectInstructionForFinalAgent("IN19"),
    MismatchCreditorNameAccount("MM20"),
    MismatchDebtorNameAccount("MM21"),
    MismatchFinalAgentNameAccount("MM22"),
    InsufficientDebtorDetails("MM23"),
    InsufficientCreditorDetails("MM24");

    private String codeName;

    UnableToApplyIncorrectInfoCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

    public static UnableToApplyIncorrectInfoCode valueOfCodeName(String codeName) {
        return Arrays.stream(values()).filter(v -> v.codeName.equals(codeName)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown codeName : " + codeName));
    }
}
