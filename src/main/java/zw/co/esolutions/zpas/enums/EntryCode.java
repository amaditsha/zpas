package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum EntryCode {
    Trial("TRIA"),
    Official("OFFI"),
    Requested("REQU"),

    ;

    private String codeName;

    EntryCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
