package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum PaymentTypeCode {
    None("NONE"),
    CashTransaction("CSH"),
    DomesticPayment("DPS"),
    DomesticHighValuePayment("DPH"),
    DomesticPriorityPayment("DPP"),
    CrossBorderPayment("CBS"),
    CrossBorderPriorityPayment("CBP"),
    CrossBorderHighValuePayment("CBH"),
    ThirdCurrencyPayment("TCS"),
    ThirdCurrencyHighValuePayment("TCH"),
    ThirdCurrencyPriorityPayment("TCP"),
    TradeSettlementPayment("COR"),
    ForeignExchange("FEX"),
    EquivalentAmount("EQA"),
    Loan("LOA"),
    LoanRepayment("LOR"),
    Securities("SEC"),
    LockBox("LBX"),
    Dividend("DIV"),
    CrossedCheque("CSQ"),
    OpenCheque("OCQ"),
    OrderCheque("ODC"),
    CircularCheque("CCQ"),
    TravellersCheques("TCK"),
    BankDraft("BKD"),
    CashLetter("CLR"),
    DocumentaryCredit("DCR"),
    BillOfExchange("BOE"),
    Collection("COL"),
    CashManagementTransfer("CMT"),
    SweepAccount("CSW"),
    TopAccount("CTO"),
    ZeroBalanceAccount("CZB"),
    StandingFacilities("STF"),
    SwiftPayServiceLevelCredit("PAY"),
    PriorityServiceLevelCredit("PRI"),
    StandardServiceLevelCredit("STD"),
    LiquidityTransfer("LIQ"),
    AdvancePayment("ADV"),
    ValueDateAdjustment("VDA"),
    DVPGross("DPG"),
    DVPNet("DPN"),
    Netting("NET"),
    LimitPayment("LMT"),
    BackUp("BCK"),
    ExpressPayment("EXP"),
    CentralBankOperation("CTR"),
    CLSPayment("CLS"),
    EuroDomesticPayment("EUP"),
    AgriculturalTransfer("AGT"),
    AlimonyPayment("AMY"),
    BalanceRetail ("BAL"),
    BonusPayment("BON"),
    BrokerageFee("BRF"),
    CapitalBuilding("CBF"),
    CharityPayment("CHR"),
    ChildBenefit("BEC"),
    CommercialCredit("COC"),
    Commission("COM"),
    CommodityTransfer("CDT"),
    Costs("COS"),
    Copyright("CPY"),
    GovernmentPayment("GVT"),
    InstalmentHirePurchaseAgreement("IHP"),
    InsurancePremium("INS"),
    IntraCompanyPayment("INC"),
    Interest("INT"),
    LicenseFee("LCF"),
    Metals("MET"),
    PensionPayment("PEN"),
    PurchaseSaleOfGoods("GDS"),
    Refund("REF"),
    Rent("REN"),
    Royalties("ROY"),
    PurchaseSaleOfServices("SCV"),
    SalaryPayment("SAL"),
    SocialSecurityBenefit("SSB"),
    StandingOrder("STO"),
    Subscription("SBS"),
    TreasuryStatePayment("TRP"),
    UnemploymentDisabilityBenefit("BEN"),
    ValueAddedTaxPayment("VAT"),
    WithHoldingTax("WHT"),
    TaxPayment("TAX"),
    Miscellaneous("MSC"),
    OvernightDeposit("OND"),
    MarginalLending("MGL"),
    Other("OTHR");

    private String codeName;

    PaymentTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

    public static PaymentTypeCode valueOfCodeName(String code) {
        return Arrays.stream(values()).filter(v -> v.getCodeName().equals(code)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown Code: " + code));
    }
}
