package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum CancellationReasonCode {
    None("NONE"),
    DuplicatePayment("DUPL"),
    IncorrectAgent("AGNT"),
    IncorrectCurrency("CURR"),
    RequestedByCustomer("CUST"),
    UnduePayment("UPAY"),
    SuspiciousPayment("SUSP"),
    InsufficientDebtorDetails("MM23"),
    InsufficientCreditorDetails("MM24"),
    CancelUponUnableToApply("CUTA"),
    TechnicalProblem("TECH"),
    FraudulentOrigin("FRAD");

    private String codeName;

    CancellationReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

    public static CancellationReasonCode valueOfCodeName(String codeName) {
        return Arrays.stream(values()).filter(v -> v.codeName.equals(codeName)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown codeName : " + codeName));
    }
}
