package zw.co.esolutions.zpas.enums;

public enum CaseForwardingNotificationCode {
    FurtherInvestigation("FTHI"),
    RequestToCancel("CANC"),
    RequestToModify("MODI"),
    RequestDebitAuthorisation("DTAU"),
    SentAdditionalInformation("SAIN"),
    FoundInvestigatingAgent("FIAG"),
    MineInvestigationCase("MINE");

    private String codeName;

    CaseForwardingNotificationCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
