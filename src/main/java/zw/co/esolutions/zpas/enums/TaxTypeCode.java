package zw.co.esolutions.zpas.enums;

public enum TaxTypeCode {

    ValueAddedTaxOfZeroRate("VATB"),
    Provincial("PROV"),
    NationalTax("NATI"),
    StateTax("STAT"),
    WithholdingTax("WITH"),
    PayAsYouEarn("PAYE"),
    CapitalGainTax("KAPA"),
    InterimProfitTax("INPO"),
    StampDuty("STAM"),
    WealthTax("WTAX"),
    InheritanceTax("INHT"),
    SolidaritySurcharge("SOSU"),
    TaxCredit("CTAX"),
    Equalisation("EQUL"),
    GiftTax("GIFT"),
    ConsumptionTax("COAX"),
    AlternativeMinimumTax("ALMI"),
    LocalTax("LOCL"),
    NationalFederalTax("COUN"),
    PaymentLevyTax("LEVY"),
    StockExchangeTax("STEX"),
    TransactionTax("TRAX"),
    TransferTax("TRAN"),
    ValueAddedTax("VATA"),
    LocalBrokerCommission("LOCO"),
    ExecutingBrokerCommission("EXEC"),
    EUTaxRetention("EUTR"),
    Aktiengewinn1("AKT1"),
    Aktiengewinn2("AKT2"),
    Zwischengewinn("ZWIS"),
    CustomsTax("CUST"),
    Other("OTHR"),
    Mietgewinn("MIET"),
    GermanLocalTax3("LOTE"),
    GermanLocalTax4("LYDT"),
    GermanLocalTax2("LIDT"),
    WithholdingOfForeignTax("WITF"),
    WithholdingOfLocalTax("WITL"),
    CapitalLossCredit("NKAP");

    private String codeName;

    TaxTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
