package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum UnableToApplyMissingInformationV2Code {

    MissingRemittanceInformation("MS01"),
    MissingInstructionForNextAgent("MS02"),
    MissingDebtor("MS03"),
    MissingDebtorAccount("MS04"),
    MissingDebtorAgent("MS05"),
    MissingAmount("MS06"),
    MissingSettlementAccount("MS07"),
    MissingIntermediary("MS08"),
    MissingInstructingReimbursementAgent("MS09"),
    MissingInstructedReimbursementAgent("MS10"),
    MissingThirdReimbursementAgent("MS11"),
    MissingCreditor("MS12"),
    MissingCreditorAccount("MS13"),
    MissingInstruction("MS14"),
    MissingCreditorAgent("MS15"),
    MissingInstructionForCreditorAgent("MS16"),
    MissingInstructionForDebtorAgent("MS17"),
    Narrative("NARR"),
    InsufficientCreditorDetails("MM24");

    private String codeName;

    UnableToApplyMissingInformationV2Code(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

    public static UnableToApplyMissingInformationV2Code valueOfCodeName(String codeName) {
        return Arrays.stream(values()).filter(v -> v.codeName.equals(codeName)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown codeName : " + codeName));
    }
}
