package zw.co.esolutions.zpas.enums;

public enum CashSystemTypeCode {
    ACH("ACHS"),
    ChequeClearing("CHCL"),
    RTGS("RTGS");

    private String codeName;

    CashSystemTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
