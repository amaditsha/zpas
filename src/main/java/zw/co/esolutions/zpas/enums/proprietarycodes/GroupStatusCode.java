package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum GroupStatusCode {
    MessageWithAllTransactionsHaveBeenAccepted("ACCP"),
    PartiallyAccepted("PART"),
    MessageWithAllTransactionsHaveBeenRejectedByACH("RJCT");

    private String codeName;

    GroupStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
