package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum  LanguageCode{
    None("NONE", "NONE"),
    aa("aar", "Afar"),
    ab("ab", "Abkhazian"),
    af("af", "Afrikaans"),
    ak("ak", "Akan"),
    sq("sq", "Albanian"),
    am("am", "Amharic"),
    ar("ar", "Arabic"),
    an("an", "Aragonese"),
    hy("hy", "Armenian"),
    as("as", "Assamese"),
    av("av", "Avaric"),
    ae("ae", "Avestan"),
    ay("ay", "Aymara"),
    az("az", "Azerbaijani"),
    ba("ba", "Bashkir"),
    bm("bm", "Bambara"),
    eu("eu", "Basque"),
    be("be", "Belarusian"),
    bn("bn", "Bengali"),
    bh("bh", "Bihari languages"),
    bi("bi", "Bislama"),
    bo("bi", "Tibetan"),
    bs("bs", "Bosnian"),
    br("br", "BosnianBosnian"),
    bg("bg", "Bulgarian"),
    my("my", "Burmese"),
    ca("ca", "Catalan; Valencian"),
    en("en", "English"),
    sn("sn", "Shona"),
    nr("nr", "Ndebele")
    ;

    private String codeName;
    private String name;

    public static LanguageCode valueOfCodeName(String code) {
        return Arrays.stream(values()).filter(v -> v.getCodeName().equals(code)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown Code: " + code));
    }

    LanguageCode(String codeName, String name){
        this.codeName = codeName;
        this.name = name;
    }

    public String getCodeName(){
        return codeName;
    }

    public String getName(){
        return name;
    }

}
