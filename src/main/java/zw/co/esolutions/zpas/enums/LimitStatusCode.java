package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum LimitStatusCode {

    Enabled("ENAB"),
    Disabled("DISA"),
    Deleted("DELD"),
    Requested("REQD")
    ;

    private String codeName;

    LimitStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
