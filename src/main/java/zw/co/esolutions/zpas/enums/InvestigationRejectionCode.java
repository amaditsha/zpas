package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum InvestigationRejectionCode {
    UnderlyingPaymentNotFound("NFND"),
    NotAuthorisedToInvestigate("NAUT"),
    UnknownCase("UKNW"),
    PaymentPreviouslyCancelledOrRejected("PCOR"),
    WrongMessage("WMSG"),
    RejectNonCashRelated("RNCR"),
    MissingResolutionOfInvestigation("MROI")
    ;

    private String codeName;

    InvestigationRejectionCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
