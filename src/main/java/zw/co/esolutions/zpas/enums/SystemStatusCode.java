package zw.co.esolutions.zpas.enums;

public enum SystemStatusCode {
    Suspended("SUSP"),
    Active("ACTV"),
    Closed("CLSD"),
    Closing("CLSG");

    private String codeName;

    SystemStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
