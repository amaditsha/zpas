package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum PaymentStatusCode {
    None("NONE"),
    AcceptedTechnicalValidation("ACTC"),
    Received("RCVD"),
    PartiallyAccepted("PART"),
    Rejected("RJCT"),
    Pending("PDNG"),
    AcceptedCustomerProfile("ACCP"),
    AcceptedSettlementInProcess("ACSP"),
    AcceptedSettlementCompleted("ACSC"),
    Accepted("ACPT"),
    AcceptedCancellationRequest("ACCR"),
    RejectedCancellationRequest("RJCR"),
    AcceptedWithChange("ACWC"),
    PartiallyAcceptedCancellationRequest("PACR"),
    PendingCancellationRequest("PDCR");

    private String codeName;

    PaymentStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName(){
        return codeName;
    }

    public static PaymentStatusCode valueOfCodeName(String codeName) {
        return Arrays.stream(values()).filter(v -> v.codeName.equals(codeName)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown codeName : " + codeName));
    }
}
