package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum InvestigationExecutionConfirmationCode {

    NONE("NONE"),
    ResolvedNonClaim("RCNT"),
    CancelledAsPerRequest("CNCL"),
    ModifiedAsPerRequest("MODI"),
    AcceptedDebitAuthorisation("ACDA"),
    PaymentInitiated("IPAY"),
    CoverInitiated("ICOV"),
    CoverModified("MCOV"),
    PaymentInstructionInitiated("IPYI"),
    AdditionalInformationSent("INFO"),
    ConfirmationOfPayment("CONF"),
    CancellationWillFollow("CWFW"),
    ModificationWillFollow("MWFW"),
    UnableToApplyWillFollow("UWFW"),
    PartiallyExecutedCancellationRequest("PECR"),
    RejectedCancellationRequest("RJCR"),
    PendingCancellationRequest("PDCR"),
    StatementEntryCorrect("SMTC"),
    StatementEntryIncorrect("SMTI"),
    ChargesDetailsProvided("CHRG"),
    PurposeDetailsProvided("PURP"),
    InstructionIsDuplicate("IDUP")
    ;
    private String codeName;

    InvestigationExecutionConfirmationCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
