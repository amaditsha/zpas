package zw.co.esolutions.zpas.enums;

public enum BalanceCounterPartyCode {
    Bilateral("BILA"),
    Multilateral("MULT");

    private String codeName;

    BalanceCounterPartyCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
