package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum DebitCreditCode {
    Credit("CRDT"),
    Debit("DBIT");

    private String codeName;

    DebitCreditCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

    public static DebitCreditCode valueOfCodeName(String codeName) {
        return Arrays.stream(values()).filter(v -> v.codeName.equals(codeName)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown codeName : " + codeName));
    }
}
