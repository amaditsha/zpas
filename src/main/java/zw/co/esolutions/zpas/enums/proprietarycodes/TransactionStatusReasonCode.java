package zw.co.esolutions.zpas.enums.proprietarycodes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum TransactionStatusReasonCode {
    InvalidInstructionId("TR 1"),
    SupplierNotRegisteredAgainstUserCode("TR 2"),
    StatusOfSupplierNotActive("TR 3"),
    DuplicateEndToEndId("TR 4"),
    InvalidDateSpecifiedInEndToEndId("TR 5"),
    UserCodeInEndToEndIdDoesNotMatchHeader("TR 6"),
    GenerationNumberInEndToEndDoesNotMatchHeader("TR 7"),
    InvalidSequenceNumberInEndToEndId("TR 8"),
    InvalidTrackingDaysSpecified("TR 9"),
    TransactionLimitExceeded("TR 10"),
    TransactionAmountIsZero("TR 11"),
    InvalidChargeBearer("TR 12"),
    InvalidCreditorName("TR 13"),
    InvalidCreditorOrganisationBICCode("TR 14"),
    InvalidCreditorIDIssuer("TR 15"),
    InvalidCreditorAccountNumber("TR 16"),
    InvalidCreditorAccountType("TR 17"),
    CreditorAgentBICDoesNotMatchInstructingAgentBICInHeader("TR 18"),
    InvalidDebtorName("TR 19"),
    InvalidDebtorOrganisationBICCode("TR 20"),
    InvalidDebtorAccountNumber("TR 21"),
    InvalidDebtorAccountType("TR 22"),
    InvalidEntryClassCode("TR 23"),
    NoMatchOnInstructionID("TR 24"),
    SupplierNolongerValid("TR 25"),
    NoMatchOnEndToEndID("TR 26"),
    DebtorBICDoesNotMatchOriginalRequest("TR 27"),
    DebtorIssuerNotValid("TR 28"),
    DebtorAccountNumberDoesNotMatchTheOriginalRequest("TR 29"),
    DebtorAccountTypeDoesNotMatchTheOriginalRequest("TR 30"),
    DebtorAgentBICDoesNotMatchTheOriginalRequest("TR 31"),
    CreditorAgentBICDoesNotMatchTheOriginalRequest("TR 32"),
    CreditorBICDoesNotMatchTheOriginalRequest("TR 33"),
    CreditorAccountDoesNotMatchTheOriginalRequest("TR 34"),
    CreditorAccountTypeDoesNotMatchTheOriginalRequest("TR 35"),
    OriginalMessageIDIndicatedDoesNotMatchAValidTransaction("TR 36"),
    InvalidOriginalMessageNameID("TR 37"),
    InvalidGroupStatus("TR 38"),
    InvalidTransactionStatus("TR 39"),
    InvalidTransactionStatusReason("TR 40"),
    InvalidMissingInformationCode("TR 41"),
    InvalidIncorrectInformationCode("TR 42"),
    GroupCancellationSectionAndTransactionCancellationSectionHaveBeenSpecified("TR 43"),
    InvalidGroupCancellationIndicatorSpecified("TR 44"),
    InvalidCancellationReasonCodeSpecified("TR 45"),
    InvalidBICCode("TR 46"),
    InvalidStatusConfirmationCode("TR 47"),
    InvalidStatusCode("TR 48"),
    InvalidStatusReasonCode("TR 49"),
    OriginalSettlementAmountDoesNotMatchOriginalTransaction("TR 50"),
    InvalidReturnedAmount("TR 51"),
    ReturnedAmountExceedsOriginalInterbankSettlementTotal("TR 52"),
    InvalidReturnReasonCode("TR 53"),
    DebtorAgentSpecifiedDoesNotMatchTheOriginalTransaction("TR 54"),
    CreditorAgentSpecifiedDoesNotMatchTheOriginalTransaction("TR 55"),
    InvalidCreditorAgentBICCode("TR 56"),
    CreditorOrganizationNotPresentNorPostalAddress("TR 57"),
    PostalAddressNotPresentNorCreditorOrganization("TR 58"),
    InterbankSettlementDoesNotMatchGroupHeaderInterbankSettlementDate("TR 59"),
    TransactionExpired("TR 60"),
    SessionExpired("TR 61"),
    InvalidDebtorAgentBIC("TR 62"),
    DuplicateTransactionId("TR 63"),
    SupplierNotRegistered("TR 64"),
    CreditorAccountCDVFailed("TR 65"),
    DebtorAccountCDVFailed("TR 66"),
    TransactionDuplicateThresholdReached("TR 67"),
    RejectedTransactionThresholdReached("TR 68"),
    DuplicateCaseId("TR 69"),
    InvalidDateSpecifiedInCaseId("TR 70"),
    UserCodeInCaseIDDoesNotMatchAssignmentId("TR 71"),
    GenerationNumberInCaseIDDoesNotMatchHeader("TR 72"),
    InvalidSequenceNumberInCaseId("TR 73"),
    CancellationRequestReceived("TR 74"),
    InsufficientFundsPending("TR 75"),
    InsufficientFundsRejected("TR 76"),
    StopPaymentRejected("TR 77"),
    NoQueryOnSpecifiedEndToEndId("TR 78"),
    NonBusinessDayAndNoTrackingIfDebitRequest("TR 79"),
    PendingOrRejectedResponseSequenceInvalid("TR 80"),
    DuplicateTransactionWithMessage("TR 81"),
    OriginalEndToEndIdInvalidForOriginalMessageId("TR 82"),
    AMLInformationNotSupplied("TR 83"),
    PaymentReturnAlreadyActioned("TR 84"),
    RejectedDebitResponseReceived("TR 85"),
    TransactionAlreadyRecalled("TR 86"),
    TransactionAlreadyDisputed("TR 87");

    private String codeName;

    TransactionStatusReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

    /*public static TransactionStatusReasonCode getTxnRsn(String value) {
        try {
            return TransactionStatusReasonCode.valueOf(value);
        } catch (Exception e) {
            log.warn("The transaction reason code is not specified. Setting the default | {}",
                    TransactionStatusReasonCode.AMLInformationNotSupplied.getCodeName());
            return TransactionStatusReasonCode.AMLInformationNotSupplied;
        }
    }*/


    public static TransactionStatusReasonCode getTxnRsn(String value) {
        if (value == null) {
            return TransactionStatusReasonCode.StopPaymentRejected;
        }
        switch (value) {
            case "05":
            case "06":
                return TransactionStatusReasonCode.InvalidIncorrectInformationCode;
            case "16":
            case "51":
                return TransactionStatusReasonCode.InsufficientFundsRejected;
            case "61":
            case "65":
                return TransactionStatusReasonCode.TransactionLimitExceeded;
            case "94":
                return TransactionStatusReasonCode.DuplicateTransactionId;
            default:
                return TransactionStatusReasonCode.InvalidCancellationReasonCodeSpecified;
        }
    }

}
