package zw.co.esolutions.zpas.enums.nssa;

public enum NatureOfEmployment {
    Arduous("Arduos"),
    Normal("Normal");

    private String codeName;

    NatureOfEmployment(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
