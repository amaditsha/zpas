package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum PartyTypeCode {

    SaleSystem("SALE"),
    POIComponent( "PCPT"),
    Issuer("ISUR"),
    Holder("HLDR"),
    Investor("INVE"),
    ExecutingFirm("EXEC"),
    BrokerOfCredit("BROK"),
    CorrespondentClearingFirm("CORR"),
    ContraFirm("COFI"),
    ContraClearingFirm("COCL"),
    UnderlyingContraFirm("UNDE"),
    GiveUpClearingFirm("GIVE"),
    OrderOriginationFirm("ORDE"),
    SponsoringFirm("SPON"),
    Clearingfirm("CLEA"),
    IntroducingFirm("INTR"),
    EnteringFirm("ENTE"),
    Client("CLIE"),
    StepInBroker("STEP"),
    AffirmingParty("AFFI"),
    ETCServiceProvider1("ETC1"),
    ETCServiceProvider2("ETC2"),
    RequestedBroker("RQBR"),
    TaxAuthority("TAXH"),
    DelegateIssuer("DLIS"),
    OriginatingPOI("OPOI"),
    CardIssuer( "CISS"),
    Acceptor("ACCP"),
    Merchant("MERC"),
    Acquirer("ACQR"),
    IntermediaryAgent("ITAG"),
    MasterTerminalManager("MTMG"),
    TerminalManager("TMGT"),
    AcquirerProcessor("ACQP"),
    CardIssuerProcessor("CISP"),
    CardScheme("CSCH"),
    CardSchemeProcessor("SCHP"),
    ATMManager("ATMG"),
    HostingEntity("HSTG"),
    OriginatingATM("OATM"),
    OriginatingTerminal("OTRM"),
    CardApplication("ICCA"),
    POISystem("PSYS"),
    POIGroup("PGRP"),
    SinglePOI("PSNG"),
    AccountFromBank("BKAF"),
    AccountToBank("BKAT")
    ;
    private String codeName;

    PartyTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
