package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum  AccountType {
    CurrentChequeAccount("1"),
    SavingsAccounts("2"),
    TransmissionAccounts("3"),
    BondAccounts("4"),
    SubscriptionShareAccounts("6");

    private String codeName;

    AccountType(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
