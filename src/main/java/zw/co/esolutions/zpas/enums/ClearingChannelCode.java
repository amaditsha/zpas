package zw.co.esolutions.zpas.enums;

public enum ClearingChannelCode {
    RealTimeGrossSettlementSystem("RTGS"),
    RealTimeNetSettlementSystem("RTNS"),
    MassPaymentNetSystem("MPNS"),
    BookTransfer("BOOK");

    private String codeName;

    ClearingChannelCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
