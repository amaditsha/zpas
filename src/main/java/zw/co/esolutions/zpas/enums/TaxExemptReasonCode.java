package zw.co.esolutions.zpas.enums;

public enum TaxExemptReasonCode {
     None("NONE"),
     MaxiISA("MASA"),
     MiniCashISA("MISA"),
     MiniStocksAndSharesISA("SISA"),
     MiniInsuranceISA("IISA"),
     CurrentYearPayment("CUYP"),
     PriorYearPayment("PRYP"),
     AssetTransfer("ASTR"),
     EmployeePriorYear("EMPY"),
     EmployeeCurrentYear("EMCY"),
     EmployerPriorYear("EPRY"),
     EmployerCurrentYear("ECYE"),
     NonFundPrototypeIRA("NFPI"),
     NonFundQualifiedPlan("NFQP"),
     DefinedContributionPlan("DECP"),
     IndividualRetirementAccount("IRAC"),
     IndividualRetirementAccountRollover("IRAR"),
     KEOGH("KEOG"),
     ProfitSharingPlan("PFSP"),
     FourHundredAndOneK("401K"),
     SelfDirectedIRA("SIRA"),
     FourHundredAndThreeB("403B"),
     FourHundredFiftySeven("457X"),
     RothIRAFundPrototype("RIRA"),
     RothIRANonPrototype("RIAN"),
     RothConversionIRAFundPrototype("RCRF"),
     RothConversionIRANonPrototype("RCIP"),
     EducationIRAFundPrototype("EIFP"),
     EducationIRANonPrototype("EIOP"),
     Other("OTHR"),
     Minor("MINO"),
     Incapacity("INCA"),
     Foreigner("FORE"),
     Ordinary("ORDR"),
     Association("ASSO"),
     Domestic("DOME"),
     OneOrBothForeign("FORP"),
     RightsHolder("RIHO"),
     PensionFund("PENF"),
     Diplomat("DIPL"),
     Refugee("REFU"),
     OneNonResident("OANR"),
     TwoNonResident("TANR"),
     Administrator("ADMI");

         private String codeName;

    TaxExemptReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
