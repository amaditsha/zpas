package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum ExchangeRateTypeCode {
    Spot("SPOT"),
    Sale("SALE"),
    Agreed("AGRD")
    ;

    private String codeName;

    ExchangeRateTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
