package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum DocumentTypeCode {
    None("None"),
    MeteredServiceInvoice("MSIN"),
    CreditNoteRelatedToFinancialAdjustment("CNFA"),
    DebitNoteRelatedToFinancialAdjustment("DNFA"),
    CommercialInvoice("CINV"),
    CreditNote("CREN"),
    DebitNote("DEBN"),
    HireInvoice("HIRI"),
    SelfBilledInvoice("SBIN"),
    RemittanceAdviceMessage("RADM"),
    RelatedPaymentInstruction("RPIN"),
    CommercialContract("CMCN"),
    ForeignExchangeDealReference("FXDR"),
    StatementOfAccount("SOAC"),
    DispatchAdvice("DISP"),
    PurchaseOrder("PUOR"),
    StructuredCommunicationReference("SCOR"),
    BillOfLading("BOLD"),
    Voucher("VCHR"),
    AccountReceivableOpenItem("AROI"),
    TradeServicesUtilityTransaction("TSUT");

    private String codeName;

    DocumentTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
