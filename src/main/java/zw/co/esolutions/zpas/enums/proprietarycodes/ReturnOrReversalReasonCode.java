package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum ReturnOrReversalReasonCode {
    IncorrectCreditorAccountNumber("RR01"),
    IncorrectDebtorAccountNumber("RR02"),
    ClosedCreditorAccountNumber("RR03"),
    ClosedDebtorAccountNumber("RR04"),
    BlockedCreditorAccount("RR05"),
    BlockedDebtorAccount("RR06"),
    EndCustomerDeceased("RR07"),
    IncorrectCreditorAgentAccount("RR08"),
    IncorrectDebtorAgentAccount("RR09"),
    InsufficientFunds("RR10"),
    IncorrectRelatedTransactionReference("RR11"),
    UnrecognisedInitiatingParty("RR12"),
    RegulatoryReason("RR13"),
    NotAllowedPaymentCredit("RR14"),
    NotAllowedPaymentDebit("RR15"),
    RefundRequestByEndCustomer("RR16"),
    ResponseOrRequestReceivedAfterCutOffTime("RR17"),
    ResolutionResponseExceedsSLACutOff("NA01"),
    PaymentStoppedByAccountHolder("NA04"),
    RecallOrWithdrawal("NA28"),
    NoAuthorisationToDebit("NA30"),
    DebitInContraventionOfTheAuthorisedPayer("NA32"),
    AuthorisationCancelled("NA34"),
    PreviouslyStoppedViaStopPaymentAdvice("NA36"),
    InvalidSupplier("INV1"),
    IncorrectTaxReferenceNumberSupplied("TX01");

    private String codeName;

    ReturnOrReversalReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
