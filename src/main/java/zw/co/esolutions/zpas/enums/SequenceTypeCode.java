package zw.co.esolutions.zpas.enums;

public enum SequenceTypeCode {

    First("FRST"),
    Recurring("RCUR"),
    Final("FNAL"),
    OneOff("OOFF"),
    Represented("RPRE");

    private String codeName;

    SequenceTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
