package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum LimitTypeCode {

    Multilateral("MULT"),
    Bilateral("BILI"),
    NetBilateral("NELI"),
    IndirectBilateral("INBI"),
    Global("GLBL"),
    MandatoryBilateral("MAND"),
    DiscretionaryBilateral("DISC"),
    DirectDebit("DIDB"),
    SingleCustomerDirectDebit("SPLC"),
    SingleFinancialInstitutionDirectDebit("SPLF"),
    TotalDailyCustomerDirectDebit("TDLC"),
    TotalDailyFinancialInstitutionDirectDebit("TDLF"),
    AutoCollateralisation("ACOL"),
    UnsecuredCredit("UCDT"),
    ExternalGuarantee("EXGT")
    ;

    private String codeName;

    LimitTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
