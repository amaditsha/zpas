package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum  IdIssuerCode {
    NationalID("1"),
    Passport("2"),
    BankIdentifier("3"),
    DriversLicense("4"),
    Other("5");

    private String codeName;

    IdIssuerCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
