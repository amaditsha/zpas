package zw.co.esolutions.zpas.enums;

public enum CancelledStatusReasonV2Code {
    CancelledByYourself("CANI"),
    CancelledBySystem("CANS"),
    CancelledByAgent("CSUB"),
    CancelledByHub("CANH"),
    CancelledByInstructingParty("CANP"),
    EndOfLife("CXLR"),
    CancelledByOther("CANO"),
    CancelledByTransferAgent("CNTA"),
    CancelledByClient("CNCL"),
    CancelledByIntermediary("CNIN"),
    CancelledDueToTransformation("CANT"),
    CancelledSplitPartialSettlement("CANZ"),
    CancelledDueToCorporateAction("CORP"),
    CancelledByIssuerRegistrar("CREG"),
    SecuritiesNoLongerEligible("SCEX"),
    NarrativeReason("NARR"),
    Other("OTHR"),
    CancelledByThirdParty("CTHP");

    private String codeName;

    CancelledStatusReasonV2Code(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
