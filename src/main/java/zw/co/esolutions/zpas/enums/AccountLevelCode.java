package zw.co.esolutions.zpas.enums;

public enum AccountLevelCode {
    Intermediate("INTM"),
    Summary("SMRY"),
    Detail("DETL");

    private String codeName;

    AccountLevelCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}