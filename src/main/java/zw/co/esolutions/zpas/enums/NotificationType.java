package zw.co.esolutions.zpas.enums;

public enum NotificationType {
    NONE,
    PAYMENT_APPROVAL,
    DEBIT_CREDIT,
    PAYMENT_CONFIRMATION,
    CASE_ASSIGNMENT,
    CASE_RESOLUTION
}