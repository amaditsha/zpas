package zw.co.esolutions.zpas.enums;

public enum WithholdingTaxRateTypeCode {

    FATCATax("FTCA"),
    NRATax("NRAT"),
    BackUpWithholding("BWIT");

    private String codeName;

    WithholdingTaxRateTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
