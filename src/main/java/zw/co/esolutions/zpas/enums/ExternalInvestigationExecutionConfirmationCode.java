package zw.co.esolutions.zpas.enums;

public enum  ExternalInvestigationExecutionConfirmationCode {
    CancelledAsPerRequest("CNCL"),
    PendingCancellationRequest("PDCR"),
    RejectedCancellationRequest("RJCR"),
    ResolvedClaimNonReceipt("RCNT"),
    ModifiedAsPerRequest("MODI");


    private String codeName;

    ExternalInvestigationExecutionConfirmationCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
