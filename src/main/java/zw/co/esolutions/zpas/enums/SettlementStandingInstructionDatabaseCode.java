package zw.co.esolutions.zpas.enums;

public enum SettlementStandingInstructionDatabaseCode {

    InternalDatabase("INTE"),
    BrokerDatabase("BRKR"),
    VendorDatabase("VEND");

    private String codeName;

    SettlementStandingInstructionDatabaseCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
