package zw.co.esolutions.zpas.enums;

public enum PowerOfAttorneyLegalisationCode {

    Notary("NOTA"),
    LocalAuthority("LOCA"),
    Apostilled("APOS"),
    Consularized("COUN");

    private String codeName;

    PowerOfAttorneyLegalisationCode(String codeName){
        this.codeName = codeName;
    }

    public String getCodeName(){
        return codeName;
    }
}
