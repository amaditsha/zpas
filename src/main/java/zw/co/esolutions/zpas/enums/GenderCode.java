package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum GenderCode {
    Male("MALE"),
    Female("FEMA")
    ;
    private String codeName;

    GenderCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
