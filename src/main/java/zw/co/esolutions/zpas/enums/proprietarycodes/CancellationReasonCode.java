package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum CancellationReasonCode {
    SystemErrorDuplicate("1"),
    SystemErrorMalfunction("2"),
    CustomerErrorIncorrectBeneficiary("3"),
    CustomerErrorIncorrectAmount("4"),
    CustomerDisputeNoMandateToDebit("5"),
    CustomerDisputeMandateInvalid("6"),
    CustomerDisputePossibleDuplicate("7"),
    CustomerDisputeSuspendedFraud("8"),
    CustomerDisputeIncorrectAmount("8");

    private String codeName;

    CancellationReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}

