package zw.co.esolutions.zpas.enums;

public enum AdjustmentDirectionCode {
    Added("ADDD"),
    Substracted("SUBS");

    private String codeName;

    AdjustmentDirectionCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
