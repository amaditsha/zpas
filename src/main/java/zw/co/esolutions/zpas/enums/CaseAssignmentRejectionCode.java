package zw.co.esolutions.zpas.enums;

public enum CaseAssignmentRejectionCode {
    UnderlyingPaymentNotFound("NFND"),
    NotAuthorisedToInvestigate("NAUT"),
    UnknownCase("UKNW"),
    PaymentRejected("RJCT"),
    PaymentCancelled("CNCL"),
    PaymentPreviouslyCancelledOrRejected("PCOR");

    private String codeName;

    CaseAssignmentRejectionCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
