package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum StatusConfirmationCode {
    CancelledAsPerRequest("CNCL"),
    PendingCancellationRequest("PDCR"),
    RejectedCancellationRequest("RJCR"),
    ResolvedClaimNonReceipt("RCNR");

    private String codeName;

    StatusConfirmationCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
