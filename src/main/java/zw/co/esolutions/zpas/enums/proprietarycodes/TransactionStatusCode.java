package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum TransactionStatusCode {
    TransactionHasBeenRejectedByNamclear("RJCT"),
    TransactionHasBeenAcceptedByNamclear("ACCP");

    private String codeName;

    TransactionStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
