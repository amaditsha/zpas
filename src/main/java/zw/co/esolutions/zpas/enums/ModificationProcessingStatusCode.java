package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum ModificationProcessingStatusCode {
    None("NONE"),
    Completed("MODC"),
    Pending("MODP"),
    Denied("DEND"),
    Accepted("PACK"),
    Rejected("REJT"),
    InRepair("REPR")
    ;

    private String codeName;

    ModificationProcessingStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
