package zw.co.esolutions.zpas.enums.nssa;

public enum DependentType {
    Spouse("Spouse"),
    Child("Child");

    private String codeName;

    DependentType(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
