package zw.co.esolutions.zpas.enums;

public enum SettlementMethodCode {

     InstructedAgent("INDA"),
     InstructingAgent("INGA"),
     CoverMethod("COVE"),
     ClearingSystem("CLRG");

    private String codeName;

    SettlementMethodCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
