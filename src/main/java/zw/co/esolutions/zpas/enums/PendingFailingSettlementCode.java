package zw.co.esolutions.zpas.enums;

public enum  PendingFailingSettlementCode {
    None("NONE"),
    AwaitingMoney ("AWMO"),
    AwaitingSecuritiesFromCounterparty ("AWSH"),
    AwaitingOtherTransaction("LAAW"),
    AwaitingDocumentsOrEndorsementsFromYou ("DOCY"),
    CounterpartyTooLateForSettlement ("CLAT"),
    WrongCertificateNumbers("CERT"),
    MinimumSettlementAmount("MINO"),
    PhysicalDeliveryDelay ("PHSE"),
    SecuritiesBlocked ("SBLO"),
    CounterpartyReturnedSecurities ("DKNY"),
    ConfirmationDiscrepancy("STCD"),
    BeneficialOwnershipDisagreement ("BENO"),
    LackOfSecurities("LACK"),
    TooLateForSettlement ("LATE"),
    CancellationConfirmationRequest ("CANR"),
    MoneyTooLateForSettlement ("MLAT"),
    UnderObjection("OBJT"),
    AwaitingDocumentsOrEndorsementsFromCounterparty("DOCC"),
    AccountBlocked ("BLOC"),
    EnquirySent("CHAS"),
    NewIssues ("NEWI"),
    CounterpartyInsufficientSecurities("CLAC"),
    TradeSettlesInPartials("PART"),
    CounterpartyInsufficientMoney ("CMON"),
    SecuritiesPledgedAsCollateral("COLL"),
    RefusedDepositForIssueOfDepositaryReceipts ("DEPO"),
    MaximumForeignLimitReached  ("FLIM"),
    NoForeignExchangeInstruction ("NOFX"),
    IncomeAdjustementRequired ("INCA"),
    PendingLinkedInstruction("LINK"),
    BuyInProcedure("BYIY"),
    AwaitingSecurities ("CAIS"),
    SecuritiesLoanedOut ("LALO"),
    InsufficientMoney ("MONY"),
    ConfirmationNotReceived("NCON"),
    CollateralShortage ("YCOL"),
    NotInGoodOrder  ("REFS"),
    LackOfStampDutyInformation ("SDUT"),
    AwaitingNextSettlementCycle ("CYCL"),
    ProcessingBatchDifference ("BATC"),
    GuaranteedDeliveryIndicatorDifference ("GUAD"),
    PreadviceInstructed ("PREA"),
    GlobalFormSecurities ("GLOB"),
    CounterpartyInReceivership ("CPEC"),
    MultipleSettlementAmount("MUNO");

    private String codeName;

    PendingFailingSettlementCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName(){
        return codeName;
    }
}
