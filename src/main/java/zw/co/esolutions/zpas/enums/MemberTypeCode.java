package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum MemberTypeCode {
    Direct("DRCT"),
    Indirect("IDRT"),
    Remote("RMTE"),
    EURO1("EURO"),
    STEP1("STEP")
    ;

    private String codeName;

    MemberTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
