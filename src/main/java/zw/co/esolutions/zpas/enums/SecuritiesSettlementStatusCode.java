package zw.co.esolutions.zpas.enums;

public enum SecuritiesSettlementStatusCode {
   None("NONE"),
   Pending("PEND"),
   Failing("PENF"),
   Unsettled ("USET"),
   PartialSettlement("PAIN"),
   Settled("SETT");

   private  String codeName;

   SecuritiesSettlementStatusCode(String codeName){
       this.codeName = codeName;
   }

   public String getCodeName(){
       return  codeName;
   }

}
