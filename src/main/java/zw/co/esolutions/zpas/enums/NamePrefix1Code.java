package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum NamePrefix1Code {
    Doctor("DOCT"),
    Mister("MIST"),
    Miss("MISS"),
    Madam("MADM"),
    Ms("MS"),
    Mrs("MRS")
    ;

    private String codeName;

    NamePrefix1Code(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
