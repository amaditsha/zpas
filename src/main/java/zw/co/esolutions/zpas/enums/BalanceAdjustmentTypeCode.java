package zw.co.esolutions.zpas.enums;

public enum BalanceAdjustmentTypeCode {
    Ledger("LDGR"),
    Float("FLOT"),
    Collected("CLLD");

    private String codeName;

    BalanceAdjustmentTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
