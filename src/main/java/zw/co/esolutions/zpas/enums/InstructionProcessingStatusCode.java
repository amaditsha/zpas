package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum InstructionProcessingStatusCode {
    None("NONE"),
    Cancellation1("CAN1"),
    Cancellation2("CAN2"),
    Cancellation3("CAN3"),
    Cancelled("CAND"),
    CancelledByAnotherParty("CANO"),
    PendingCancellation("CANP"),
    Generated("CGEN"),
    FullyExecutedConfirmationSent("COSE"),
    CancellationRequested("CPRC"),
    DefaultAction("DFLA"),
    Done("DONE"),
    PartiallyFilledOrder("DONF"),
    ReceivedAtStockExchange("EXCH"),
    FullyExecutedExecutionSent("EXSE"),
    Future("FUTU"),
    ReceivedAtIntermediary("INTE"),
    NoInstruction("NOIN"),
    AlreadyMatchedAndAffirmed("NOTC"),
    OpenOrder("OPOD"),
    OverAllocated("OVER"),
    AcknowledgedAccepted("PACK"),
    PartialFill("PAFI"),
    PartialCancel("PART"),
    PendingProcessing("PPRC"),
    Rejected("REJT"),
    InRepair("REPR"),
    SettlementInstructionSent("SESE"),
    StandingInstruction("STIN"),
    TradingSuspendedByStockExchange("SUSP"),
    Treated("TREA"),
    UnderAllocated("UNDE"),
    ModificationRequested("MPRC"),
    AcknowledgedAcceptedByAccountOwner("ACAO"),
    RejectedByAccountOwner("RJAO")
    ;

    private String codeName;

    InstructionProcessingStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
