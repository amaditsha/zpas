package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum PartyRoleCode {
    LiquidityManager("LQMG"),
    LimitManager("LMMG"),
    PaymentManager("PYMG"),
    Reader("REDR"),
    BackupManager("BKMG"),
    SettlementManager("STMG"),
    FundManagementCompany("FMCO"),
    TransferAgent("TRAG"),
    FundAccountant("FACT"),
    Registrar("REGI"),
    InvestmentManager("INVE"),
    Custodian("CUST"),
    Auditor("AUDT"),
    PayingAgent("PAYI"),
    CashCorrespondent("CACO"),
    DataProvider("DATP"),
    FinalAgent("FIAG"),
    Investor("INVS"),
    FirstAgent("FTAG"),
    Intermediary("INTR"),
    Distributor("DIST"),
    Concentrator("CONC"),
    UnderlyingClient1("UCL1"),
    UnderlyingClient2("UCL2"),
    TransmittingAgent("TRAN"),
    FundBroker("FNBR"),
    FinancialAdvisor("FIAD"),
    ContactPersonAtInstructingPartyInstitution("CONI"),
    ContactPersonAtExecutingPartyInstitution("CONE"),
    NameOfAgentToOrder("CONA"),
    PrimeBroker("PRBR"),
    Internal("INTC")
    ;

    private String codeName;

    PartyRoleCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
