package zw.co.esolutions.zpas.enums;

public enum CashPaymentStatusCode {
    None("NONE"),
    Pending("PDNG"),
    Final("FINL"),
    Cancelled("CANC");

    private String codeName;

    CashPaymentStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
