package zw.co.esolutions.zpas.enums;

public enum CancellationProcessingStatusCode {
    None("NONE"),
    CancellationCompleted("CAND"),
    PendingCancellation("CANP"),
    Denied("DEND"),
    ReceivedAtStockExchange("EXCH"),
    ReceivedAtIntermediary("INTE"),
    Accepted("PACK"),
    PartiallyFilled("PARF"),
    Rejected("REJT"),
    InRepair("REPR");

    private String codeName;

    CancellationProcessingStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
