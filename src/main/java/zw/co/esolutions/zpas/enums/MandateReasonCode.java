package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum MandateReasonCode {
    None("NONE"),
    IncorrectAgent("AGNT"),
    IncorrectCurrency("CURR"),
    RequestedByCustomer("CUST"),
    InsufficientDebtorDetails("DBTR"),
    InsufficientCreditorDetails("CDTR"),
    IncorrectAccount("ACCT")
    ;

    private String codeName;

    MandateReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
