package zw.co.esolutions.zpas.enums;

public enum CopyDuplicateCode {
    None("None"),
    CopyDuplicate("CODU"),
    Copy("COPY"),
    Duplicate("DUPL");

    private String codeName;

    CopyDuplicateCode(String codeName) {
        this.codeName = codeName;
    }
}
