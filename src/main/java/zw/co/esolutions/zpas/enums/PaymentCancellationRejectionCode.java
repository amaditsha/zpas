package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum PaymentCancellationRejectionCode {

        None("NONE"),
        LegalDecision("LEGL"),
		AgentDecision("AGNT"),
		CustomerDecision("CUST"),
		AlreadyReturned("ARDT"),
		NoAnswerFromCustomer("NOAS"),
		NoOriginalTransactionReceived("NOOR"),
		ClosedAccountNumber("AC04"),
		InsufficientFunds("AM04")
    ;

    private String codeName;

    PaymentCancellationRejectionCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
