package zw.co.esolutions.zpas.enums.nssa;

public enum Gender {
    Male("Male"),
    Female("Female");

    private String codeName;

    Gender(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
