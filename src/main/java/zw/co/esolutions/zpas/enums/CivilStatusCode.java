package zw.co.esolutions.zpas.enums;

public enum CivilStatusCode {
    Single("SING"),
    Married("MARR"),
    LegallyDivorced("LDIV"),
    Divorced("DIVO"),
    Widow("WIDO"),
    StableUnion("UNIO"),
    Separated("SEPA");

    private String codeName;

    CivilStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
