package zw.co.esolutions.zpas.enums;

public enum SuspendedStatusReasonCode {
    None("NONE"),
    PriceSuspension("PRIC"),
    Overflow("FLOW"),
    SuspendedByYourself("SUBY"),
    SuspendedBySystem("SUBS"),
    Other("OTHR");

    private String codeName;

    SuspendedStatusReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
