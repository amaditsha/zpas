package zw.co.esolutions.zpas.enums;

public enum CashSettlementSystemCode {
    BankOfKoreaWire("BOKW"),
    Cheque("CHEC"),
    UKDomestic("GBSC"),
    GrossSettlementSystem("GROS"),
    NetSettlementSystem("NETS"),
    USChips("USCH"),
    FedWireUS("USFW");

    private String codeName;

    CashSettlementSystemCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
