package zw.co.esolutions.zpas.enums;

public enum CaseStatusCode {
    Closed("CLSD"),
    Assigned("ASGN"),
    UnderInvestigation("INVE"),
    Unknown("UKNW"),
    Overdue("ODUE");

    private String codeName;

    CaseStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
