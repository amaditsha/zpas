package zw.co.esolutions.zpas.enums;

public enum PriorityCode {

//   Urgent("URGT"),
   High("HIGH"),
   Normal("NORM");
//   Low("LOWW");

   private String codeName;

   PriorityCode(String codeName){
       this.codeName = codeName;
   }

   public String getCodeName(){
       return codeName;
   }
}
