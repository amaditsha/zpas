package zw.co.esolutions.zpas.enums;

public enum AccountStatusCode {
    Enabled("ENAB"),
    Disabled("DISA"),
    Deleted("DELE"),
    ProForma("FORM"),
    Pending("PEND"),
    None("NONE");

    private String codeName;

    AccountStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
