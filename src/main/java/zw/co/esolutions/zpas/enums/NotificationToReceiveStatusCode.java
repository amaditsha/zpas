package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum NotificationToReceiveStatusCode {
    None("NONE"),
    ReceivedButDifferent("RCBD"),
    Received("RCVD"),
    NotReceived("NRCD"),

    ;

    private String codeName;

    NotificationToReceiveStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
