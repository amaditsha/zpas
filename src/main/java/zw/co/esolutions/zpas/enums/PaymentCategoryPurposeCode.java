package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum PaymentCategoryPurposeCode {

    IntraCompanyPayment("INTC"),
    TradeSettlementPayment("CORT"),
    SalaryPayment("SALA"),
    TreasuryPayment("TREA"),
    CashManagementTransfer("CASH"),
    Dividend("DIVI"),
    GovernmentPayment("GOVT"),
    Interest("INTE"),
    Loan("LOAN"),
    PensionPayment("PENS"),
    Securities("SECU"),
    SocialSecurityBenefit("SSBE"),
    TaxPayment("TAXS"),
    ValueAddedTaxPayment("VATX"),
    SupplierPayment("SUPP"),
    Hedging("HEDG"),
    Trade("TRAD"),
    WithHolding("WHLD")
    ;
    private String codeName;

    PaymentCategoryPurposeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
