package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum FloorLimitTypeCode {
    Credit("CRED"),
    Debit("DEBT"),
    Both("BOTH"),

    ;
    private String codeName;

    FloorLimitTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
