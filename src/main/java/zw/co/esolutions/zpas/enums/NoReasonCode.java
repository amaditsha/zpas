package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum NoReasonCode {

    NoReason("NORE")
    ;

    private String codeName;

    NoReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
