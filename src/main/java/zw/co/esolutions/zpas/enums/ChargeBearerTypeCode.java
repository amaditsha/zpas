package zw.co.esolutions.zpas.enums;

public enum ChargeBearerTypeCode {
    BorneByDebtor("DEBT"),
    BorneByCreditor("CRED"),
    Shared("SHAR"),
    FollowingServiceLevel("SLEV");

    private String codeName;

    ChargeBearerTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
