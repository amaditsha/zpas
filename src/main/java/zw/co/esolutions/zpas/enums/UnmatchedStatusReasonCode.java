package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

public enum UnmatchedStatusReasonCode {
    None("NONE");

    private String codeName;

    UnmatchedStatusReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName(){
        return codeName;
    }

    public static UnmatchedStatusReasonCode valueOfCodeName(String codeName) {
        return Arrays.stream(values()).filter(v -> v.codeName.equals(codeName)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown codeName : " + codeName));
    }
}
