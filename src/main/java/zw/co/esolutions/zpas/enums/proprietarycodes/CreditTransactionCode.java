package zw.co.esolutions.zpas.enums.proprietarycodes;

public enum CreditTransactionCode {
    Salary("61"),
    Pension("62"),
    PAYE("63"),
    SalaryDeductionMortgageBondRepayment("64"),
    SalaryDeductionInsuranceRepayment("65"),
    MortgageBondRepayment("66"),
    ValueAddedTaxVatPayment("67"),
    BankUseMortgageBondSettlement("68"),
    SalaryDeductionMiscellaneous("69"),
    Annuity("70"),
    Dividend("71"),
    Interest("72"),
    AgentsCommission("75"),
    BankUseChequeCardCreditReversals("76"),
    InsurancePremiumRefund("80"),
    PaymentToCreditor("81"),
    PaymentOfInsuranceClaim("82"),
    MedicalAidRefunds("83"),
    UnitTrustRepurchase("86"),
    BankUseCreditTransfer("88"),
    BankUseCreditCardMerchantElectronicFundsTransfer("94"),
    BankUseCreditCardHolderElectronicFundsTransfer("95"),
    BankUseCommissionPaymentGarageCard("96"),
    BankUseRefundGarageCard("97");


    private String codeName;

    CreditTransactionCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
