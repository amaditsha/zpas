package zw.co.esolutions.zpas.enums;

public enum RateTypeCode {
    
    Fixed("FIXE"),
    Forfeit("FORF"),
    Variable("VARI"),
    Open("OPEN"),
    Unknown("UKWN"),
    NilPayment("NILP"),
    AdditionalTax("ATAX"),
    Charges("CHAR"),
    CashInLieuOfSecurities ("CINL"),
    Gross("GRSS"),
    CashIncentive("INCE"),
    Net("NETT"),
    Sollication("SOFE"),
    StampDuty("STAM"),
    StockExchangeTax("STEX"),
    WithholdingTax("TAXR"),
    TransferTax("TRAN"),
    TransactionTax("TRAX"),
    TaxDeferred("TXDF"),
    TaxFeeAmount("TXFR"),
    WithholdingOfForeignTax("WITF"),
    WithholdingOfLocalTax("WITL"),
    Imputed("IMPU"),
    Precompte("PREC"),
    OneTierTax("TIER"),
    LocalTax("LIDT"),
    Scheduled("SCHD"),
    Unscheduled("USCD"),
    AnyAndAll("ANYA");

    private String codeName;
    
    RateTypeCode(String codeName){
        this.codeName = codeName;
    }
    
    public String getCodeName(){
        return  codeName;
    }
        
}
