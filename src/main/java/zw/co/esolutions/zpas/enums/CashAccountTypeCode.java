package zw.co.esolutions.zpas.enums;

public enum CashAccountTypeCode {
    CashPayment("CASH"),
    Charges("CHAR"),
    Commission("COMM"),
    Tax("TAXE"),
    CashIncome("CISH"),
    CashTrading("TRAS"),
    Settlement("SACC"),
    Current("CACC"),
    Savings("SVGS"),
    OverNightDeposit("ONDP"),
    MarginalLending("MGLD"),
    NonResidentExternal("NREX"),
    MoneyMarket("MOMA"),
    Loan("LOAN"),
    Salary("SLRY"),
    Overdraft("ODFT"),
    Lending("LEND"),
    Collateral("COLL"),
    FinancialSettlement("SETT"),
    MarginReturn("MARR"),
    Segregated("SEGT");

    private String codeName;

    CashAccountTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
