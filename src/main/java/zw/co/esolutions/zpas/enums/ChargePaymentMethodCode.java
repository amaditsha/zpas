package zw.co.esolutions.zpas.enums;

public enum ChargePaymentMethodCode {
    Cash("CASH"),
    Unit("UNIT");

    private String codeName;

    ChargePaymentMethodCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
