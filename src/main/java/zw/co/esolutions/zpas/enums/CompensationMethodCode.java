package zw.co.esolutions.zpas.enums;

public enum CompensationMethodCode {
    No("NOCP"),
    Debited("DBTD"),
    Invoiced("INVD"),
    DelayedDebit("DDBT");

    private String codeName;

    CompensationMethodCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
