package zw.co.esolutions.zpas.enums;

public enum CalculationBasisCode {
    Average("AVER"),
    Daily("DAIL"),
    Other("OTHR"),
    Monthly("MNTH"),
    Annual("YEAR");

    private String codeName;

    CalculationBasisCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
