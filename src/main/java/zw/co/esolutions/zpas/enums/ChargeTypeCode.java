package zw.co.esolutions.zpas.enums;

public enum ChargeTypeCode {
    BrokerageFee("BRKF"),
    Commission("COMM"),
    BackEndLoad("BEND"),
    FrontEndLoad("FEND"),
    Switch("SWIT"),
    DilutionLevy("DLEV"),
    Discount("DISC"),
    ManagementFee("MANF"),
    TransferFee("TRAN"),
    MatchingFees("MACO"),
    PostageCharge("POST"),
    RegulatoryFee("REGF"),
    ShippingCharge("SHIP"),
    ServiceProvisionFee("CHAR"),
    SpecialConcessions("SPCN"),
    PartAcquis("PACQ"),
    Penalty("PENA"),
    ContingencyDeferredSalesCharge("CDSC"),
    Other ("OTHR"),
    Equalisation("EQUL"),
    CorrespondentBankCharge("CBCH"),
    Premium("PREM"),
    Initial("INIT"),
    AdvisoryFee("ADVI"),
    CustodyFee("CUST"),
    PublicationFee("PUBL"),
    AccountingFee("ACCT"),
    SignatureService("SIGN"),
    StorageAtDestination("STDE"),
    StorageAtOrigin("STOR"),
    Packaging("PACK"),
    PickUp("PICK"),
    DangerousGoodsFee("DNGR"),
    SecurityCharge("SECU"),
    InsurancePremium("INSU"),
    CollectFreight("COLF"),
    ClearanceAndHandlingAtOrigin("CHOR"),
    ClearanceAndHandlingAtDestination("CHDE"),
    AirWayBillFee("AIRF"),
    TransportCharges("TRPT"),
    UCITSCommission("UCIC"),
    SpeciallyAgreedFrontEndLoad("SFEN"),
    ADRFee("ADRF"),
    ChargeTypeCode("ISSC"),
    MiscellaneousFee("MISC"),
    IssuanceFee("ISSU");

    private String codeName;

    ChargeTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
