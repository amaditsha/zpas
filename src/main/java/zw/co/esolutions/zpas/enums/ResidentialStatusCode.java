package zw.co.esolutions.zpas.enums;

public enum ResidentialStatusCode {

    Resident("RESI"),
    PermanentResident("PRES"),
    NonResident("NRES");

    private  String codeName;

    ResidentialStatusCode(String codeName){
        this.codeName = codeName;
    }

    public String getCodeName(){
        return  codeName;
    }
}
