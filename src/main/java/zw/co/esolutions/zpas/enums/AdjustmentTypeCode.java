package zw.co.esolutions.zpas.enums;

public enum AdjustmentTypeCode {
    Rebate("REBA"),
    Discount("DISC"),
    CreditNote("CREN"),
    Surcharge("SURC");

    private String codeName;

    AdjustmentTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
