package zw.co.esolutions.zpas.enums;

public enum BalanceTypeCode {
    Receivables("RECE"),
    Payables("PAYA"),
    Other("OTHR"),
    Expenses("EXPN"),
    CashAndCashEquivalents("CASE"),
    InvestorInflowOutflow("IIOF"),
    Revenues("REVE"),
    Borrowings("BORR"),
    Investments("INVE"),
    Opening("OPNG"),
    Interim("INTM"),
    Closing("CLSG"),
    Book("BOOK"),
    Current("CRRT"),
    Pending("PDNG"),
    LimitRelated("LRLD"),
    Available("AVLB"),
    LiquidityTransfer("LTSF"),
    Credit("CRDT"),
    EligibleAssets("EAST"),
    Payment("PYMT"),
    Blocked("BLCK"),
    Expected("XPCD"),
    DaylightOverdraft("DLOD"),
    ExpectedCredit("XCRD"),
    ExpectedDebit("XDBT"),
    Adjustment("ADJT"),
    ProgressiveAverage("PRAV"),
    Debit("DBIT"),
    Threshold("THRE"),
    Noted("NOTE"),
    Self("SELF"),
    Master("MSTR"),
    ForecastSettlement("FSET"),
    BlockedTrades("BLOC"),
    OtherBlockedTrades("OTHB"),
    Custody("CUST"),
    CashForecast("FORC"),
    DividendsCollection("COLC"),
    NetFunding("FUND"),
    PayInPayOut("PIPO"),
    ExchangeForecast("XCHG"),
    CentralCounterparty("CCPS"),
    TotalOnHold("TOHB"),
    DebitOnHold("DOHB"),
    TotalProcessed("TPBL"),
    CreditProcessed("CPBL"),
    DebitProcessed("DPBL"),
    TotalFuture("FUTB"),
    TotalRejected("REJB"),
    OpeningAvailable("OPAV"),
    InterimAvailable("ITAV"),
    ClosingAvailable("CLAV"),
    ForwardAvailable("FWAV"),
    ClosingBooked("CLBD"),
    InterimBooked("ITBD"),
    OpeningBooked("OPBD"),
    PreviouslyClosedBooked("PRCD"),
    InvestmentOpeningAvailable("IOPA"),
    InvestmentInterimAvailable("IITA"),
    InvestmentClosingAvailable("ICLA"),
    InvestmentForwardAvailable("IFWA"),
    InvestmentClosingBooked("ICLB"),
    InvestmentInterimBooked("IITB"),
    InvestmentOpeningBooked("IOPB"),
    InvestmentExpected("IXPC"),
    DisbursementOpeningAvailable("DOPA"),
    DisbursementInterimAvailable("DITA"),
    DisbursementClosingAvailable("DCLA"),
    DisbursementForwardAvailable("DFWA"),
    DisbursementClosingBooked("DCLB"),
    DisbursementInterimBooked("DITB"),
    DisbursementOpeningBooked("DOPB"),
    DisbursementExpected("DXPC"),
    CollectionOpeningAvailable("COPA"),
    CollectionInterimAvailable("CITA"),
    CollectionClosingAvailable("CCLA"),
    CollectionForwardAvailable("CFWA"),
    CollectionClosingBooked("CCLB"),
    CollectionInterimBooked("CITB"),
    CollectionOpeningBooked("COPB"),
    CollectionExpected("CXPC"),
    FirmCollateralisation("FCOL"),
    UsedAmountsFirmCollateralisation("FCOU"),
    SelfCollateralisation("SCOL"),
    UsedAmountsSelfCollateralisation("SCOU"),
    CustodyActual("CUSA"),
    CCPGuaranteedForecasting("XCHC"),
    NonCCPGuaranteedForecasting("XCHN"),
    DefinitiveSettledSecurities("DSET"),
    LackOfHoldingsTransactions("LACK"),
    NonSettledSecurities("NSET"),
    CCPGuaranteedOTCTransactions("OTCC"),
    ForecastOTCTransactions("OTCG"),
    NonCCPGuaranteedOTCTransactions("OTCN"),
    SAPDirectDebitAmount("SAPD"),
    SAPDirectCreditAmount("SAPC"),
    CMUPRepoDebit("REPD"),
    CMUPRepoCredit("REPC"),
    BulkSettlementCMUPDebit("BSCD"),
    BulkSettlementCMUPCredit("BSCC"),
    SAPQueueAmount("SAPP"),
    IntradayRepoLimit("IRLT"),
    IntradayRepoDrawings("IRDR"),
    DiscountWindowRepoDrawings("DWRD"),
    AvailableDMVForDiscountWindowRepo("ADWR"),
    AvailableDMVForIntradayRepo("AIDR"),
    Elected("ELEC"),
    ReservedForDistribution("RDIS"),
    ReservedForRemoval("RREM"),
    Restricted("REST"),
    TotalEntitled("TENT"),
    Unelected("UNEL"),
    Information("INFO");

    private String codeName;

    BalanceTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
