package zw.co.esolutions.zpas.enums;

public enum TaxationBasisCode {

    Flat("FLAT"),
    PerUnit("PERU"),
    Percentage("PRCT"),
    RatePerBrackets("BRAC"),
    RateWithMinimumAmount("MINI"),
    RateWithMaximumAmount("MAXI"),
    Other("OTHR"),
    GrossAmount("GRAM"),
    NetAmount("NEAM"),
    NetAssetValuePrice("NAVP");

    private String codeName;

    TaxationBasisCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
