package zw.co.esolutions.zpas.enums;

public enum TaxIdentificationNumberTypeCode {

   TaxIdentificationNumber("GTIN"),
   GlobalIntermediaryIdentificationNumber("GIIN"),
    ;
    private String codeName;

    TaxIdentificationNumberTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
