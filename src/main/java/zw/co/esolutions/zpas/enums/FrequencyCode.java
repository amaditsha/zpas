package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum FrequencyCode {
    Annual("YEAR"),
    Monthly("MNTH"),
    Quarterly("QURT"),
    SemiAnnual("MIAN"),
    Weekly("WEEK"),
    Daily("DAIL"),
    Adhoc("ADHO"),
    IntraDay("INDA"),
    Overnight("OVNG"),
    TenDays("TEND"),
    Fortnightly("FRTN"),
    TriggeredByMovement("MOVE"),
    Never("NEVR"),
    Rate("RATE"),
    OnCreditEvent("CRED"),
    Upfront("UPFR"),
    OnExpiry("EXPI"),

    ;
    private String codeName;

    FrequencyCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
