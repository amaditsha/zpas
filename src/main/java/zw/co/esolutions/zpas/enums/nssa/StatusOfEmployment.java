package zw.co.esolutions.zpas.enums.nssa;

public enum StatusOfEmployment {
    Permanent("Permanent"),
    FixedTerm("FixedTerm"),
    PartTime("PartTime"),
    Casual("Casual");

    private String codeName;

    StatusOfEmployment(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
