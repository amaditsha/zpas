package zw.co.esolutions.zpas.enums;

public enum StandingOrderTypeCode {

    UserDefinedStandingOrder("USTO"),
    PredefinedStandingOrder("PSTO");

    private  String codeName;

    StandingOrderTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
