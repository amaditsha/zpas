package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum ExposureConventionTypeCode {
    Gross("GROS"),
    Net("NET1"),

    ;

    private String codeName;

    ExposureConventionTypeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
