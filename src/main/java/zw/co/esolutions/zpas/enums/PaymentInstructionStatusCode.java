package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 13 Feb 2019
 */
public enum PaymentInstructionStatusCode {
    None("NONE"),
    Accepted("ACPD"),
    Validated("VALD"),
    Authorised("AUTD"),
    Invalid("INVD"),
    Matched("MATD"),
    Unmatched("UMAC"),
    Mismatched("MMTD"),
    SettlementEligible("STLE"),
    SettlementMature("STLM"),
    Suspended("SSPD"),
    PendingCancellation("PCAN"),
    PendingSettlement("PSTL"),
    PendingFailingSettlement("PFST"),
    SenderMultilateralLimitRelated("SMLR"),
    ReceiverMultilateralLimitRelated("RMLR"),
    SenderReceiverBilateralLimitRelated("SRBL"),
    Settled("STLD"),
    Rejected("RJTD"),
    Cancelled("CAND"),
    Finalised("FNLD"),
    HeldForServiceAvailability("AVLB"),
    SenderReceiverMultilateralLimitRelated("SRML")
    ;
    private String codeName;

    PaymentInstructionStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
