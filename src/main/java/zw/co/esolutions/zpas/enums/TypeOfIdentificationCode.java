package zw.co.esolutions.zpas.enums;

public enum TypeOfIdentificationCode {
    None("NONE"),
    AlienRegistrationNumber("ARNU"),
    PassportNumber("CCPT"),
    TaxExemptIdentificationNumber("CHTY"),
    CorporateIdentification("CORP"),
    DriverLicenseNumber("DRLC"),
    ForeignInvestmentIdentityNumber("FIIN"),
    TaxIdentificationNumber("TXID");

    private String codeName;

    TypeOfIdentificationCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
