package zw.co.esolutions.zpas.enums;

public enum TransactionStatusCode {
    None("NONE"),
    Reversal("RVSL"),
    Rebooked("REBO"),
    Cancelled("CANC");

    private String codeName;

    TransactionStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
