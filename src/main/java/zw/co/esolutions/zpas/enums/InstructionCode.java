package zw.co.esolutions.zpas.enums;

import java.util.Arrays;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum InstructionCode {
        PayTheBeneficiary("PBEN")
        ,TimeTill("TTIL")
		,TimeFrom("TFRO")
		,PayCreditorByCheque("CHQB")
		,HoldCashForCreditor("HOLD")
		,PhoneBeneficiary("PHOB")
		,Telecom("TELB")
		,PhoneNextAgent("PHOA")
		,TelecomNextAgent("TELA")
    ;
    private String codeName;

    public static InstructionCode valueOfCodeName(String code) {
        return Arrays.stream(values()).filter(v -> v.getCodeName().equals(code)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown Code: " + code));
    }

    public String getCodeName() {
        return codeName;
    }

    InstructionCode(String codeName) {
        this.codeName = codeName;
    }
}
