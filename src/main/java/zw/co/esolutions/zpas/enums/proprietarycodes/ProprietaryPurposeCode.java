package zw.co.esolutions.zpas.enums.proprietarycodes;

import zw.co.esolutions.zpas.enums.PaymentPurposeCode;

import java.util.Arrays;

public enum ProprietaryPurposeCode {
    InsurancePayment("21"),
    PensionFundContribution("22"),
    MedicalAidFundContribution("23"),
    UnitTrustContribution("26"),
    CharitableOrReligiousContribution("28"),
    HPPayment("31"),
    AccountRepayment("32"),
    LoanRepaymentOtherThanMortgage("33"),
    RentalLeaseOtherThanProperty("34"),
    ServiceChargeMaintenanceOfServiceAgreements("35"),
    ServiceChargeVariableAmounts("36"),
    ValueAddedTaxVatAllocation("37"),
    RentProperty("41"),
    BondRepayment("42"),
    BankUseDebitTransfer("44"),
    BankUseChequeCardDebits("46"),
    IPSUseOnlyFinancialReturnOfDisputesGreaterThan40Days("49"),
    MunicipalAccountsWaterLights("51"),
    MunicipalAccounts("52"),
    TelephoneAccounts("53"),
    BankUseCreditMerchantElectronicFundTransfer("54"),
    BankUseServiceChargeExemptFromDutyAndServiceFee("56"),
    BankUseGarageCard("57"),
    BankUseServiceCharge("58"),
    Salary("61"),
    Pension("62"),
    PAYE("63"),
    SalaryDeductionMortgageBondRepayment("64"),
    SalaryDeductionInsuranceRepayment("65"),
    MortgageBondRepayment("66"),
    ValueAddedTaxVatPayment("67"),
    BankUseMortgageBondSettlement("68"),
    SalaryDeductionMiscellaneous("69"),
    Annuity("70"),
    Dividend("71"),
    Interest("72"),
    AgentsCommission("75"),
    BankUseChequeCardCreditReversals("76"),
    InsurancePremiumRefund("80"),
    PaymentToCreditor("81"),
    PaymentOfInsuranceClaim("82"),
    MedicalAidRefunds("83"),
    UnitTrustRepurchase("86"),
    BankUseCreditTransfer("88"),
    BankUseCreditCardMerchantElectronicFundsTransfer("94"),
    BankUseCreditCardHolderElectronicFundsTransfer("95"),
    BankUseCommissionPaymentGarageCard("96"),
    BankUseRefundGarageCard("97");


    private String codeName;

    ProprietaryPurposeCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

    public static ProprietaryPurposeCode valueOfCodeName(String code) {
        return Arrays.stream(values()).filter(v -> v.getCodeName().equals(code)).findFirst()
                .orElseThrow(() -> new RuntimeException("Unknown Code: " + code));
    }
}
