package zw.co.esolutions.zpas.enums.proprietarycodes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum  GroupStatusReasonCode {
    DuplicateMsgId("GR 1"),
    InvalidDateInMsgId("GR 2"),
    InvalidServiceIdCodeInMsgId("GR 3"),
    InvalidUserCodeInMsgId("GR 4"),
    InvalidGenerationNumberInMsgId("GR 5"),
    NumberOfTransactionInvalid("GR 6"),
    InvalidControlSum("GR 7"),
    InvalidTotalInterbankSettlementAmount("GR 8"),
    UserLimitExceeded("GR 9"),
    FutureInterbankSettlementBankDateOutOfBounds("GR 10"),
    PostDatedInterbankSettlementDate("GR 11"),
    SettlementMethodIsNotCLRG("GR 12"),
    InstructingAgentBICInvalid("GR 13"),
    InstructedAgentBICInvalid("GR 14"),
    InvalidInterbankSettlementBank("GR 15"),
    DuplicateControlSumAndNumberOfTransaction("GR 16"),
    DuplicateCaseId("GR 17"),
    InvalidDateInCaseId("GR 18"),
    InvalidUserCodeInCaseId("GR 19"),
    InvalidGenerationNumberInCaseId("GR 20"),
    InvalidSequenceNumberInCaseId("GR 21"),
    CreatorBICCodeInvalid("GR 22"),
    CannotPopulateBothOrgnlGrpInfAndCxlAndTxInf("GR 23"),
    OriginalMessageIdNotFound("GR 24"),
    OriginalMessageNameIsInvalid("GR 25"),
    GroupCancellationInvalid("GR 26"),
    GroupCancellationReasonCodeInvalid("GR 27"),
    DuplicateResolvedCaseId("GR 28"),
    InvalidDateInResolvedCaseId("GR 29"),
    InvalidUserCodeInResolvedCaseId("GR 30"),
    InvalidGenerationNumberInResolvedCaseId("GR 31"),
    InvalidSequenceNumberInResolvedCaseId("GR 32"),
    InvalidStatusConfirmationCode("GR 33"),
    FileValidationFailed("GR 34"),
    TransactionThresholdPerFileReached("GR 35"),
    RejectedTransactionThresholdReached("GR 36"),
    DuplicateDataSet("GR 37"),
    FileDeleted("GR 38"),
    FileReceivedPastDailyServiceCutOffTime("GR 39");

    private String codeName;

    GroupStatusReasonCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

    public static GroupStatusReasonCode getGrpRsn(String value) {
        try {
            return GroupStatusReasonCode.valueOf(value);
        } catch (Exception e) {
            log.warn("The transaction reason code is not specified. Setting the default | {}",
                    GroupStatusReasonCode.FileValidationFailed.getCodeName());
            return GroupStatusReasonCode.FileValidationFailed;
        }
    }
}
