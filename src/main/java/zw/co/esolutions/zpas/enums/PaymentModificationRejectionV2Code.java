package zw.co.esolutions.zpas.enums;

public enum PaymentModificationRejectionV2Code {

    None("NONE"),
    UnableToModifyRelatedReference("UM01"),
    UnableToModifyPaymentServiceLevel("UM02"),
    UnableToModifyCategoryPurpose("UM03"),
    UnableToModifyRequestedExecutionDate("UM04"),
    UnableToModifyInterbankSettlementDate("UM05"),
    UnableToModifyInterbankSettlementAccount("UM06"),
    UnableToModifyDebtor("UM07"),
    UnableToModifyDebtorAccount("UM08"),
    UnableToModifyInstructedReimbursementAgent("UM09"),
    UnableToModifyThirdReimbursementAgent("UM10"),
    UnableToModifyPaymentClearingChannel("UM11"),
    UnableToModifyCreditorAgentAccount("UM12"),
    UnableToModifyCreditor("UM13"),
    UnableToModifyCreditorAccount("UM14"),
    UnableToModifyRemittanceInformation("UM15"),
    UnableToModifyPaymentPurpose("UM16"),
    UnableToModifyChargeBearer("UM17"),
    UnableToModifyInstructionForNextAgent("UM18"),
    UnableToModifyInstructionForCreditorAgent("UM19"),
    InstructionCancelledSubmitNewInstruction("UM20"),
    UnableToModifySubmitCancellation("UM21"),
    UnableToModifyDebtorAgentAccount("UM22"),
    UnableToModifyInterbankSettlementAmount("UM23"),
    UnableToModifyInstructionForDebtorAgent("UM24"),
    UnableToModifyRequestedCollectionDate("UM25"),
    UnableToModifyPaymentType("UM26"),
    UnableToModifyInstructedAmount("UM27"),
    ;

    private String codeName;

    PaymentModificationRejectionV2Code(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName(){
        return codeName;
    }
}
