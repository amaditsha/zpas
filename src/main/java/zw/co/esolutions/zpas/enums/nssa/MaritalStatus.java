package zw.co.esolutions.zpas.enums.nssa;

public enum MaritalStatus {
    Single("Single"),
    Married("Married"),
    Seperated("Seperated"),
    Widowed("Widowed");

    private String codeName;

    MaritalStatus(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
