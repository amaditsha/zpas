package zw.co.esolutions.zpas.enums;

public enum TransactionChannelCode {

    MailOrder("MAIL"),
    TelephoneOrder("TLPH"),
    ElectronicCommerce("ECOM"),
    TelevisionPayment("TVPY"),
    OfficeOrBranch("BRAN"),
    HomeBanking("HOBA"),
    FinancialAdvisor("FIAD"),
    MobilePayment("MOBL"),
    SecuredElectronicCommerce("SECM"),
    MobilePOS("MPOS");

    private String codeName;

    TransactionChannelCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
