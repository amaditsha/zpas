package zw.co.esolutions.zpas.enums;

public enum BillingChargeMethodCode {
    UnitPriced("UPRC"),
    StampDuty("STAM"),
    BaseCharge("BCHG"),
    DiscountPrice("DPRC"),
    FlatCharge("FCHG"),
    ListPrice("LPRC"),
    MinimumCharge("MCHG"),
    MaximumReduction("MXRD"),
    Tier1("TIR1"),
    Tier2("TIR2"),
    Tier3("TIR3"),
    Tier4("TIR4"),
    Tier5("TIR5"),
    Tier6("TIR6"),
    Tier7("TIR7"),
    Tier8("TIR8"),
    Tier9("TIR9"),
    ThresholdPrice("TPRC"),
    ZonePrice("ZPRC"),
    BalanceBased("BBSE");

    private String codeName;

    BillingChargeMethodCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
