package zw.co.esolutions.zpas.enums.nssa;

public enum  Title {
    Mister("Mr"),
    Mrs("Mrs"),
    Ms("Ms"),
    Miss("Miss"),
    Doctor("Dr"),
    Sir("Sir"),
    Professor("Professor");

    private String codeName;

    Title(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }

}
