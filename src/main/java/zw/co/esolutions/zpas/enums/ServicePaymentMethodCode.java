package zw.co.esolutions.zpas.enums;

public enum ServicePaymentMethodCode {

    BalanceCompensable("BCMP"),
    HardCharge("FLAT"),
    PreviouslyCharged("PVCH"),
    InvoicedSeparately("INVS"),
    Waived("WVED"),
    Free("FREE");

    private String codeName;

    ServicePaymentMethodCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
