package zw.co.esolutions.zpas.enums;

public enum RemittanceLocationMethodCode {

    Fax("FAXI"),
    ElectronicDataInterchange("EDIC"),
    UniformResourceIdentifier("URID"),
    EMail("EMAL"),
    Post("POST"),
    SMS("SMSM");

    private String codeName;

    RemittanceLocationMethodCode(String codeName){
        this.codeName = codeName;
    }

    public String getCodeName(){
       return codeName;
    }

}
