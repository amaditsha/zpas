package zw.co.esolutions.zpas.enums;

/**
 * Created by jnkomo on 12 Feb 2019
 */
public enum EntryStatusCode {

    Booked("BOOK"),
    Pending("PDNG"),
    Information("INFO"),
    Future("FUTR"),
    None("NONE");

    private String codeName;

    EntryStatusCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
