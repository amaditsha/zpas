package zw.co.esolutions.zpas.enums;

public enum CommunicationMethodCode {
    SWIFTMT("SWMT"),
    SWIFTMX("SWMX"),
    Fax("FAXI"),
    Email("EMAL"),
    Proprietary("PROP"),
    Online("ONLI"),
    Phone("PHON"),
    PostalService("POST"),
    File("FILE");

    private String codeName;

    CommunicationMethodCode(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeName() {
        return codeName;
    }
}
