package zw.co.esolutions.zpas.process;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import zw.co.esolutions.zpas.dto.audittrail.LogDTO;
import zw.co.esolutions.zpas.model.audittrail.AuditTrail;
import zw.co.esolutions.zpas.services.iface.audittrail.AuditTrailService;
import zw.co.esolutions.zpas.utilities.audit.Auditable;

@Slf4j
public class AuditLogger {

    @Autowired
    AuditTrailService auditTrailService;

    public AuditTrail logActivity(String username, String activityName, Auditable entity) {
        try {
            log.info("Logged the following activity: " + activityName);
            return auditTrailService.logActivity(LogDTO.builder()
                    .userName(username).activityName(activityName)
                    .entityId(entity.getId()).entityName(entity.getEntityName())
                    .newObject(entity.getAuditableAttributesString()).instanceName(entity.getInstanceName()).build());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Failed to log activity : " + e.getMessage());
            return null;
        }
    }
}
