package zw.co.esolutions.zpas.datatype;

import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by alfred on 23 Jan 2019
 */
@Data
//@Builder
@NoArgsConstructor
@Embeddable
public class CurrencyAndAmount implements Serializable {
    protected BigDecimal amount;

    @Column(nullable = false)
    @Embedded
    protected CurrencyCode currency;


    @Override
    public String toString() {
        return (currency == null? "": currency.getCodeName()) + " " + (amount == null? "": MoneyUtil.convertDollarsToPattern(amount.doubleValue()));
    }
}
