package zw.co.esolutions.zpas.datatype;

import lombok.Data;
import zw.co.esolutions.zpas.codeset.ActiveCurrencyCode;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by tanatsa on 23 Jan 2019
 */
@Data
@Embeddable
public class ActiveCurrencyAndAmount implements Serializable {
    protected BigDecimal amount;
    protected ActiveCurrencyCode currency;
}
