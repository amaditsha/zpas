package zw.co.esolutions.zpas.codeset;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
@Embeddable
@Data
public class MMCode implements Serializable {
    protected String codeName;
    protected String name;
}
