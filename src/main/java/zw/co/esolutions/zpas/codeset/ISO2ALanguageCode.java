package zw.co.esolutions.zpas.codeset;

import lombok.Data;

import javax.persistence.Embeddable;

/**
 * Created by jnkomo on 24 Jan 2019
 */
@Embeddable
@Data
public class ISO2ALanguageCode extends MMCode {
}
