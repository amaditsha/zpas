package zw.co.esolutions.zpas.codeset;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

/**
 * Created by alfred on 23 Jan 2019
 */
@Data

@NoArgsConstructor
@Embeddable
public class CurrencyCode extends MMCode {
    @Override
    public String toString() {
        return "CurrencyCode{" +
                "codeName='" + codeName + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
