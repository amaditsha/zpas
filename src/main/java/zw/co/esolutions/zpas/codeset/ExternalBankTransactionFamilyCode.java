package zw.co.esolutions.zpas.codeset;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

/**
 * Created by mabuza on 31 Jan 2019
 */

@Data
@NoArgsConstructor

@Embeddable
public class ExternalBankTransactionFamilyCode extends MMCode {
}
