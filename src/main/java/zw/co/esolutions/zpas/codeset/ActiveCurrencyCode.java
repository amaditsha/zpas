package zw.co.esolutions.zpas.codeset;

import lombok.Data;

import javax.persistence.Embeddable;

/**
 * Created by tanatsa on 23 Jan 2019
 */
@Data
@Embeddable
public class ActiveCurrencyCode extends MMCode {
}
