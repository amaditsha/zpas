package zw.co.esolutions.zpas.codeset;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Data

@NoArgsConstructor
@Embeddable
public class ExternalBillingCompensationTypeCode extends MMCode {
}
