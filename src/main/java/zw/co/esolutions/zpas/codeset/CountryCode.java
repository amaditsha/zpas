package zw.co.esolutions.zpas.codeset;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

/**
 * Created by alfred on 23 Jan 2019
 */
@Data
@Embeddable
@NoArgsConstructor
public class CountryCode extends MMCode {
    @Override
    public String toString() {
        return "CountryCode{" +
                "codeName='" + codeName + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
