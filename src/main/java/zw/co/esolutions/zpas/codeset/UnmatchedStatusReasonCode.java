package zw.co.esolutions.zpas.codeset;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

/**
 * Created by alfred on 29 Jan 2019
 */

@Data

@NoArgsConstructor
@Embeddable
public class UnmatchedStatusReasonCode extends MMCode {
}
