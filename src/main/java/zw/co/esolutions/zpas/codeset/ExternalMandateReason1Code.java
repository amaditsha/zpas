package zw.co.esolutions.zpas.codeset;

import lombok.Data;

import javax.persistence.Embeddable;

/**
 * Created by alfred on 23 Jan 2019
 */
@Embeddable
@Data
public class ExternalMandateReason1Code extends MMCode {
}
