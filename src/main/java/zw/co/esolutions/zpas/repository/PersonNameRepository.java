package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PersonName;

import java.util.List;

/**
 * Created by tanatsa on 29 Jan 2019
 */

@Repository
public interface PersonNameRepository extends JpaRepository<PersonName, String> {
    List<PersonName> findPersonNamesByPartyIdentificationId(String partyIdentificationId);
    List<PersonName> findAllByGivenName(String givenName);

    @Query("SELECT pn FROM PersonName pn WHERE LOWER(pn.birthName) LIKE %:search% OR " +
            "LOWER(pn.middleName) LIKE %:search% OR LOWER(pn.givenName) LIKE %:search% " +
            "OR LOWER(CONCAT(pn.birthName, ' ', pn.middleName, ' ', pn.givenName)) LIKE %:search% " +
            "ORDER BY pn.dateCreated DESC")
    List<PersonName> searchPersonName(@Param("search") String search);
}
