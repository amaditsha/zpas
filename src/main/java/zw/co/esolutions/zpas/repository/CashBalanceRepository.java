package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.CashBalance;

import java.util.List;

@Repository
public interface CashBalanceRepository extends JpaRepository<CashBalance, String> {
}
