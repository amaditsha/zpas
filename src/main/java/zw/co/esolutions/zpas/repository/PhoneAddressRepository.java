package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PhoneAddress;

/**
 * Created by jnkomo on 30 Jan 2019
 */
@Repository
public interface PhoneAddressRepository extends JpaRepository<PhoneAddress, String> {
}
