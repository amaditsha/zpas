package zw.co.esolutions.zpas.repository;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.Role;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.List;

/**
 * Created by alfred on 27 Jan 2019
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, String> {
    List<Payment> findAllByPartyRole_Id(String partyRoleId);
    List<Payment> findAllByPartyRoleIn(List<Role> roles);
    List<Payment> findTop25ByPartyRoleInOrderByDateCreatedDesc(List<Role> roles);

    @Query("SELECT p FROM Payment p, PaymentIdentification pi WHERE pi.payment.id = p.id AND " +
            "(LOWER(pi.executionIdentification) LIKE %:searchPhrase% " +
            "OR LOWER(pi.endToEndIdentification) LIKE %:searchPhrase% " +
            "OR LOWER(pi.instructionIdentification) LIKE %:searchPhrase% " +
            "OR LOWER(pi.transactionIdentification) LIKE %:searchPhrase% " +
            "OR LOWER(pi.clearingSystemReference) LIKE %:searchPhrase% " +
            "OR LOWER(pi.creditorReference) LIKE %:searchPhrase%) " +
            "ORDER BY p.dateCreated DESC")
    List<Payment> searchPayments(@Param("searchPhrase") String searchPhrase);

    @Query("SELECT p FROM Payment p WHERE p.id IN :paymentIds ORDER BY p.dateCreated DESC")
    List<Payment> getPaymentsByIds(@Param("paymentIds") List<String> paymentIds);

    List<Payment> findAllById(String paymentId);

    List<Payment> findAllByEntityStatus(EntityStatus entityStatus);
}
