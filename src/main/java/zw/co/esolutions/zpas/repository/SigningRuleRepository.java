package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.SigningRule;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

@Repository
public interface SigningRuleRepository extends JpaRepository<SigningRule, String> {
//    SigningRule findByEntityStatusAndEntityTypeAndEntityId(EntityStatus entityStatus, String entityType, String entityId);

    @Query("select sr from SigningRule sr where sr.entityStatus <> :entityStatus AND sr.entityType = :entityType " +
            "AND sr.entityId = :entityId AND (:amount > sr.minAmount AND :amount <= sr.maxAmount)")
    SigningRule findByEntityStatusNotAndEntityTypeAndEntityIdAndAmount(@Param("entityStatus") EntityStatus entityStatus,
                                                           @Param("entityType") String entityType,
                                                           @Param("entityId") String entityId,
                                                           @Param("amount") Long amount);
}
