package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.EmployingPartyRole;
import zw.co.esolutions.zpas.model.Role;

import java.util.List;

@Repository
public interface EmployingPartyRoleRepository extends JpaRepository<EmployingPartyRole, String> {
    List<EmployingPartyRole> findAllByPlayer_Id(String rolePlayerId);
    List<EmployingPartyRole> findAllByEmployee_Id(String employeeId);
}
