package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PersonIdentification;

import java.util.Optional;

@Repository
public interface PersonIdentificationRepository extends JpaRepository<PersonIdentification, String> {
    @Query("SELECT pi FROM PersonIdentification pi WHERE pi.identityCardNumber = :idNumber")
    PersonIdentification findPersonIdentificationByIdentityCardNumber(@Param("idNumber") String idNumber);

    @Query("SELECT pi FROM PersonIdentification pi WHERE pi.alienRegistrationNumber = :field " +
            "OR pi.nationalRegistrationNumber = :field OR pi.otherIdentification = :field")
    Optional<PersonIdentification> getPersonIdentificationByUniqueField(@Param("field") String field);
}
