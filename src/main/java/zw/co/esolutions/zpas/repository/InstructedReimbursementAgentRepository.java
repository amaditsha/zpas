package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.InstructedReimbursementAgent;

import java.util.List;

@Repository
public interface InstructedReimbursementAgentRepository extends JpaRepository<InstructedReimbursementAgent, String> {
    List<InstructedReimbursementAgent> findByEntityStatus(String instructingReimbursementAgentStatus);
}
