package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.model.Notification;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, String> {
    Notification findByGroupIdAndEntityIdAndClientId(String groupId, String entityId, String clientId);
    List<Notification> findAllByClientId(String clientId);
    List<Notification> findAllByGroupIdAndReadAndClientId(String groupId, Boolean isRead, String clientId);
    List<Notification> findAllByReadAndClientId(Boolean isRead, String clientId);
    List<Notification> findAllByReadAndUsername(Boolean isRead, String username);
    @Transactional
    @Modifying
    @Query("UPDATE Notification n SET n.read = :read WHERE n.id = :notificationId")
    Integer updateNotification(@Param("notificationId") String notificationId, @Param("read") Boolean read);

}
