package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PaymentIdentification;

import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 27 Jan 2019
 */
@Repository
public interface PaymentIdentificationRepository extends JpaRepository<PaymentIdentification, String> {
    Optional<PaymentIdentification> findByExecutionIdentification(String identification);
    Optional<PaymentIdentification> findByInstructionIdentification(String identification);
    Optional<PaymentIdentification> findByEndToEndIdentification(String identification);

    @Query("SELECT pi FROM PaymentIdentification pi WHERE LOWER(pi.endToEndIdentification) LIKE %:searchPhrase% ORDER BY pi.dateCreated DESC")
    List<PaymentIdentification> searchByEndToEndIdentification(@Param("searchPhrase") String searchPhrase);
}
