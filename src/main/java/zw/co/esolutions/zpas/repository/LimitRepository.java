package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Limit;

@Repository
public interface LimitRepository extends JpaRepository<Limit, String> {
}
