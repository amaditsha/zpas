package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.CashAccountMandate;

import java.util.List;
import java.util.Optional;

@Repository
public interface CashAccountMandateRepository extends JpaRepository<CashAccountMandate, String> {
    Optional<CashAccountMandate> findFirstBySignatureConditions_Id(String signatureConditionId);

    /*@Query("SELECT cam FROM CashAccountMandate cam " +
            "WHERE cam.cashAccountContract.cashAccount.id = :cashAccountId ORDER BY cam.dateCreated ASC ")
    List<CashAccountMandate> findCashAccountMandatesByCashAccountId(@Param("cashAccountId") String cashAccountId);*/
}
