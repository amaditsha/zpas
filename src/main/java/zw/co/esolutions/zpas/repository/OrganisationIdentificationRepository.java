package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.OrganisationIdentification;

import java.util.List;
import java.util.Optional;

/**
 * Created by tanatsa on 29 Jan 2019
 */

@Repository
public interface OrganisationIdentificationRepository extends JpaRepository<OrganisationIdentification, String> {
    List<OrganisationIdentification> findAllByOrganisationId(String organisationId);
    OrganisationIdentification findOrganisationIdentificationByAnyBIC(String anyBIC);
    OrganisationIdentification findOrganisationIdentificationByBICFI(String bICFI);

    @Query("SELECT oi FROM OrganisationIdentification oi WHERE oi.anyBIC = :bic OR oi.bICFI = :bic")
    Optional<OrganisationIdentification> getByAnyBICOrBICFI(@Param("bic") String bic);

    @Query("SELECT oi FROM OrganisationIdentification oi WHERE oi.taxIdentificationNumber = :bpn")
    Optional<OrganisationIdentification> getByBusinessPartnerNumber(@Param("bpn") String bpn);
}
