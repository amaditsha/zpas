package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.SettlementPartyRole;

@Repository
public interface SettlementPartyRoleRepository extends JpaRepository<SettlementPartyRole,String> {
}
