package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.LineItem;

/**
 * Created by alfred on 28 Jan 2019
 */
@Repository
public interface LineItemRepository extends JpaRepository<LineItem,String> {
}
