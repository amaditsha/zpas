package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.RuleItem;

import java.util.List;

@Repository
public interface RuleItemRepository extends JpaRepository<RuleItem, String> {
    @Query("SELECT ri FROM RuleItem ri WHERE ri.signingRule.id = :id")
    List<RuleItem> findAllBySigningRuleId(@Param("id") String id);
}
