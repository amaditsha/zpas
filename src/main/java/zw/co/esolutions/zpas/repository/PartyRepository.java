package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Party;

import java.util.List;
import java.util.Optional;

/**
 * Created by tanatsa on 29 Jan 2019
 */

@Repository
public interface PartyRepository extends JpaRepository<Party, String> {
    List<Party> findAllByRole_Id(String roleId);

    Optional<Party> findPartyByVirtualId(String virtualId);
}
