package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PaymentTerms;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.List;

/**
 * Created by alfred on 27 Jan 2019
 */
@Repository
public interface PaymentTermsRepository extends JpaRepository<PaymentTerms, String> {
}
