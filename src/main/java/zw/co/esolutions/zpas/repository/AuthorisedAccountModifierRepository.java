package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.AuthorisedAccountModifier;

/**
 * Created by jnkomo on 30 Jan 2019
 */
@Repository
public interface AuthorisedAccountModifierRepository extends JpaRepository<AuthorisedAccountModifier, String> {
}
