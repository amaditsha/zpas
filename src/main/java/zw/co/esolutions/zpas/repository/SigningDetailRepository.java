package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.SigningDetail;

import java.util.List;
import java.util.Optional;

@Repository
public interface SigningDetailRepository extends JpaRepository<SigningDetail, String> {

    @Query("SELECT COUNT(sd) FROM SigningDetail sd WHERE sd.done = :done AND sd.signatureConditionId = :signatureConditionId AND sd.paymentId = :paymentId")
    Long findByRemainingNumberOfApprovalsForSignatureConditionAndPaymentId(@Param("signatureConditionId") String signatureConditionId,
                                                               @Param("done") Boolean done, @Param("paymentId") String paymentId);

//    @Query("SELECT sd FROM SigningDetail sd " +
//            "WHERE sd.done = :done AND sd.entityId = :entityId " +
//            "AND sd.orderingNo = (SELECT MIN(a.orderingNo) FROM SigningDetail a WHERE a.done = :done) ORDER BY sd.dateCreated ASC ")
//    SigningDetail getNextApprover(@Param("entityId") String entityId,
//                                  @Param("done") Boolean done);

    Optional<SigningDetail> findFirstBySignatureConditionIdAndDoneAndPaymentId(String signatureConditionId, Boolean done, String paymentId);

//    @Query("SELECT sd FROM SigningDetail sd " +
//            "WHERE sd.done = :done AND sd.signatureConditionId = :signatureConditionId ORDER BY sd.orderingNo ASC ")
//    List<SigningDetail> getRemainingApprovalDetails(@Param("signatureConditionId") String signatureConditionId,
//                                                    @Param("done") Boolean done);

    List<SigningDetail> findAllBySignatureConditionIdAndPaymentId(String signatureConditionId, String paymentId);
    Optional<SigningDetail> findByPartyIdAndSignatureConditionIdAndPaymentId(String partyId, String signatureConditionId, String paymentId);
    List<SigningDetail> findByPaymentIdAndDone(String paymentId, Boolean done);
    List<SigningDetail> findByPaymentIdAndSignatureConditionIdAndDone(String paymentId, String signatureConditionId, Boolean done);
    List<SigningDetail> findByPaymentIdAndSignatureConditionIdAndDoneAndHolder(String paymentId, String signatureConditionId, Boolean done, Boolean holder);
    Optional<SigningDetail> findFirstByPaymentId(String paymentId);
    Optional<SigningDetail> findTopByPaymentIdAndSignatureConditionIdAndDoneAndHolderOrderByDateCreatedDesc(String paymentId, String signatureConditionId, Boolean done, Boolean holder);
}
