package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Person;
import zw.co.esolutions.zpas.model.PersonIdentification;

import java.util.List;
import java.util.Optional;

/**
 * Created by tanatsa on 29 Jan 2019
 */

@Repository
public interface PersonRepository extends JpaRepository<Person, String> {
    List<Person> findAllByRole_Id(String roleId);
    @Query("SELECT cam FROM Person cam WHERE cam.profession LIKE \'%:param%\' ORDER BY cam.dateCreated ASC ")
    List<Person> findPersonByAnything(@Param("param") String param);

    Optional<Person> findPersonByIdentification(PersonIdentification personIdentification);

    List<Person> findAllByEmployingParty(String employingParty);
}
