package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.RolePlayer;

import java.util.List;

/**
 * Created by tanatsa on 29 Jan 2019
 */

@Repository
public interface RolePlayerRepository extends JpaRepository<RolePlayer, String> {
    List<RolePlayer> findByIdIn(List<String> ids);
}
