package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.ThirdReimbursementAgent;

import java.util.List;

@Repository
public interface ThirdReimbursementAgentRepository extends JpaRepository<ThirdReimbursementAgent, String> {
    List<ThirdReimbursementAgent> findByEntityStatus(String thirdReimbursementAgentStatus);
}
