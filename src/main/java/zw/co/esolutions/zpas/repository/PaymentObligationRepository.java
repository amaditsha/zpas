package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.PaymentObligation;
import zw.co.esolutions.zpas.model.PaymentObligationPartyRole;
import zw.co.esolutions.zpas.model.Role;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.List;

/**
 * Created by alfred on 25 Jan 2019
 */
@Repository
public interface PaymentObligationRepository extends JpaRepository<PaymentObligation, String> {
    List<PaymentObligation> findAllByPartyRoleIn(List<PaymentObligationPartyRole> roles);
    List<PaymentObligation> findAllByIdIn(List<String> ids);
}
