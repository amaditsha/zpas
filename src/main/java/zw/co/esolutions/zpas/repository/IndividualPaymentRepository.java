package zw.co.esolutions.zpas.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.BulkPayment;
import zw.co.esolutions.zpas.model.IndividualPayment;
import zw.co.esolutions.zpas.model.Payment;

import java.util.List;

/**
 * Created by alfred on 27 Jan 2019
 */
@Repository
public interface IndividualPaymentRepository extends JpaRepository<IndividualPayment, String> {
    List<IndividualPayment> findAllByBulkPayment(BulkPayment bulkPayment);
    Page<IndividualPayment> findAllByBulkPayment(BulkPayment bulkPayment, Pageable pageable);
}
