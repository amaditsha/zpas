package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Account;
import zw.co.esolutions.zpas.model.Currency;

import java.util.Optional;

/**
 * Created by alfred on 27 Jan 2019
 */
@Repository
public interface CurrencyRepository extends JpaRepository<Currency, String> {
    Optional<Currency> findCurrencyById(String code);
}
