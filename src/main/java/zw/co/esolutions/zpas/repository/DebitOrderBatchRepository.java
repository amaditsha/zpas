package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.DebitOrderBatch;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface DebitOrderBatchRepository extends JpaRepository<DebitOrderBatch, String> {
    Optional<DebitOrderBatch> findByFileID(String fileId);
    List<DebitOrderBatch> findAllByClientId(String clientId);
    List<DebitOrderBatch> findTop50ByClientId(String clientId);
    List<DebitOrderBatch> findAllBySenderID(String bic);
    List<DebitOrderBatch> findTop50BySenderID(String bic);
    List<DebitOrderBatch> findByDateCreatedBetween(OffsetDateTime startDate, OffsetDateTime endDate);
    List<DebitOrderBatch> findTop50ByReceiverID(String receiverID);
}
