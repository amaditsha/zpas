package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Role;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
    List<Role> findAllByPlayer_Id(String rolePlayerId);

    @Query("SELECT r FROM Role r WHERE LOWER(r.customParty.name) LIKE %:searchPhrase% " +
            "OR LOWER(r.customParty.nationalId) LIKE %:searchPhrase% " +
            "OR LOWER(r.customParty.passportNumber) LIKE %:searchPhrase% " +
            "OR LOWER(r.customParty.accountNumber) LIKE %:searchPhrase%")
    List<Role> searchRoleByCustomPartyName(@Param("searchPhrase") String searchPhrase);
}
