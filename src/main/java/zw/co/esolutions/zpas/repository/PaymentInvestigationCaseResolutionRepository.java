package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PaymentInvestigationCaseResolution;

import java.util.List;

@Repository
public interface PaymentInvestigationCaseResolutionRepository extends JpaRepository<PaymentInvestigationCaseResolution, String> {
    List<PaymentInvestigationCaseResolution> findAllByInvestigationCase_id(String id);
}
