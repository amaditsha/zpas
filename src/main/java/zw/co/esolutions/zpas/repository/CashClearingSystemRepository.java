package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.CashClearingSystem;

import java.util.List;

/**
 * Created by mabuza on 30 Jan 2019
 */
@Repository
public interface CashClearingSystemRepository extends JpaRepository<CashClearingSystem, String> {
}
