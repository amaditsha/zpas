package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.AccountIdentification;
import zw.co.esolutions.zpas.model.MandatePartyRole;

import java.util.List;

@Repository
public interface MandatePartyRoleRepository extends JpaRepository<MandatePartyRole, String> {
    List<MandatePartyRole> findByIdIn(List<String> ids);
}
