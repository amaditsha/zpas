package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PaymentDetails;

import java.util.List;

/**
 * Created by alfred on 16 Jun 2019
 */
@Repository
public interface PaymentDetailsRepository extends JpaRepository<PaymentDetails, String> {
    List<PaymentDetails> findAllByBulkPaymentId(String bulkPaymentId);
}
