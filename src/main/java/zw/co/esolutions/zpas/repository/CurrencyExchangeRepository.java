package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.CurrencyExchange;

/**
 * Created by alfred on 27 Jan 2019
 */
@Repository
public interface CurrencyExchangeRepository extends JpaRepository<CurrencyExchange, String> {
}
