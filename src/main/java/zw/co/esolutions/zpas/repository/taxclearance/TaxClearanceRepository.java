package zw.co.esolutions.zpas.repository.taxclearance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import zw.co.esolutions.zpas.model.taxclearance.TaxClearance;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 20 May 2019
 */
public interface TaxClearanceRepository extends JpaRepository<TaxClearance, String> {

    @Query("SELECT tc FROM TaxClearance tc WHERE tc.nonFinancialInstitution.id IN :nonFIIDs AND CURRENT_DATE < tc.expiryDate")
    List<TaxClearance> getCurrentTaxClearances(@Param("nonFIIDs") List<String> nonFinancialInstitutionIds);

    @Query("SELECT tc FROM TaxClearance tc WHERE tc.nonFinancialInstitution.id IN :nonFIIDs AND tc.expiryDate < :cutOffDate")
    List<TaxClearance> getTaxClearanceRenewalCandidates(@Param("nonFIIDs") List<String> nonFinancialInstitutionIds,
                                                        @Param("cutOffDate")OffsetDateTime cutOffDate);

    @Query("SELECT tc FROM TaxClearance tc WHERE tc.nonFinancialInstitution.id IN :nonFIIDs AND tc.expiryDate >= :cutOffDate")
    List<TaxClearance> getTaxClearanceNonRenewalCandidates(@Param("nonFIIDs") List<String> nonFinancialInstitutionIds,
                                                        @Param("cutOffDate") OffsetDateTime cutOffDate);

    @Query("SELECT tc FROM TaxClearance tc WHERE tc.nonFinancialInstitution.id = :nonFinancialInstitutionId AND CURRENT_DATE < tc.expiryDate")
    List<TaxClearance> findAllByNonFinancialInstitutionId(@Param("nonFinancialInstitutionId") String nonFinancialInstitutionId);

    @Query("SELECT tc FROM TaxClearance tc WHERE tc.nonFinancialInstitution IS NULL AND tc.bpn = :bpn")
    Optional<TaxClearance> getDanglingTaxClearance(@Param("bpn") String bpn);

    Optional<TaxClearance> getTaxClearanceByBpn(String bpn);
}
