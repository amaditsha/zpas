package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Commission;

import java.util.List;

@Repository
public interface CommissionRepository extends JpaRepository<Commission, String> {
    @Query("SELECT c FROM Commission c WHERE c.entityStatus = :entityStatus")
    List<Commission> findCommissionByStatus(@Param("entityStatus") String entityStatus);
}
