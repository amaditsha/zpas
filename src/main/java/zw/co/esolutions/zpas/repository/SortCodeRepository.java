package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.model.SortCode;

import java.util.List;

@Repository
public interface SortCodeRepository extends JpaRepository<SortCode, String> {
    List<SortCode> findAllByFinancialInstitution(FinancialInstitution financialInstitution);
}
