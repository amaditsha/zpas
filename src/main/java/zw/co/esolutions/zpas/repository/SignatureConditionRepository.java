package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.SignatureCondition;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.util.List;

/**
 * Created by anotida on 29 Jan 2019
 */
@Repository
public interface SignatureConditionRepository extends JpaRepository<SignatureCondition, String> {
    @Query("SELECT sc FROM SignatureCondition sc " +
            "WHERE sc.id in :ids ORDER BY sc.dateCreated DESC")
    List<SignatureCondition> getSignatureConditionsInIds(@Param("ids") List<String> ids);

//    @Query("select sc from SignatureCondition sc where sc.entityStatus <> :entityStatus AND sc.entityType = :entityType " +
//            "AND sc.entityId = :entityId AND (:amount > sc.minAmount AND :amount <= sc.maxAmount)")
//    SignatureCondition findByEntityStatusNotAndEntityTypeAndEntityIdAndAmount(@Param("entityStatus") EntityStatus entityStatus,
//                                                                       @Param("entityType") String entityType,
//                                                                       @Param("entityId") String entityId,
//                                                                       @Param("amount") Long amount);
}
