package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.MasterAgreement;

/**
 * Created by anotida on 27 Jan 2019
 */
@Repository
public interface MasterAgreementRepository extends JpaRepository<MasterAgreement, String> {
}
