package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.DebitOrderBatchItem;

import java.util.List;
import java.util.Optional;

@Repository
public interface DebitOrderBatchItemRepository  extends JpaRepository<DebitOrderBatchItem, String> {
    List<DebitOrderBatchItem> findByDebitOrderBatch_Id(String batchId);
}
