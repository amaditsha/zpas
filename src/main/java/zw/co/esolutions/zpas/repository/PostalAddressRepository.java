package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Person;
import zw.co.esolutions.zpas.model.PostalAddress;

import java.util.List;

/**
 * Created by jnkomo on 30 Jan 2019
 */
@Repository
public interface PostalAddressRepository extends JpaRepository<PostalAddress, String> {
    List<PostalAddress> findAllByLocationId(String locationId);

}
