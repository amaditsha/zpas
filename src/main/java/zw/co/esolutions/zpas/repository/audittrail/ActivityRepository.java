package zw.co.esolutions.zpas.repository.audittrail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.audittrail.Activity;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Repository
public interface ActivityRepository extends JpaRepository<Activity, String> {
    Activity findByName(String activityName);
}
