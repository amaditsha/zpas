package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Location;

import java.util.List;

/**
 * Created by jnkomo on 30 Jan 2019
 */
@Repository
public interface LocationRepository extends JpaRepository<Location, String> {
    List<Location> findAllByOperatingOrganisationId(String operatingOrganisationId);
    List<Location> findAllByRegisteredOrganisationId(String registeredOrganisationId);
}
