package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Operator;

import java.util.Optional;

import java.util.List;

@Repository
public interface OperatorRepository extends JpaRepository<Operator, String> {
    Optional<Operator> getOperatorById(String id);
    List<Operator> findByCode(String operator);
    List<Operator> findByName(String operator);
}
