package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.model.Role;

import java.util.List;

@Repository
public interface PaymentInvestigationCaseRepository extends JpaRepository<PaymentInvestigationCase,String> {
    @Query("SELECT ic FROM PaymentInvestigationCase ic WHERE ic.id = :caseId ORDER BY ic.dateCreated DESC ")
    List<PaymentInvestigationCase> getAllByCaseId(@Param("caseId") String caseId);
    List<PaymentInvestigationCase> findAllByInvestigationPartyRoleIn(List<Role> roles);
    List<PaymentInvestigationCase> findAllByUnderlyingPayment(Payment payment);
    PaymentInvestigationCase findByIdentification(String identification);
}
