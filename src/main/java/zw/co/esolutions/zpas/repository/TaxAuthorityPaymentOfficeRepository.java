package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.TaxAuthorityPaymentOffice;

import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 10 Apr 2019
 */
@Repository
public interface TaxAuthorityPaymentOfficeRepository extends JpaRepository<TaxAuthorityPaymentOffice, String> {
    List<TaxAuthorityPaymentOffice> findAllByTaxAuthorityId(String taxAuthorityId);

    Optional<TaxAuthorityPaymentOffice> findByCode(String code);
}

