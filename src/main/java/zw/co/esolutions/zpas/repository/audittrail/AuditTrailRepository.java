package zw.co.esolutions.zpas.repository.audittrail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.audittrail.AuditTrail;

import java.util.List;

/**
 * Created by jnkomo on 28 Mar 2019
 */
@Repository
public interface AuditTrailRepository extends JpaRepository<AuditTrail, String> {
    List<AuditTrail> findByEntityNameAndInstanceName(String entityName, String instanceName);
}
