package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.TaxPartyRole;

@Repository
public interface TaxPartyRoleRepository extends JpaRepository<TaxPartyRole, String> {
}
