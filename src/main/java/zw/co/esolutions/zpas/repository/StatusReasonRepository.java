package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.StatusReason;

@Repository
public interface StatusReasonRepository  extends JpaRepository<StatusReason, String> {
}
