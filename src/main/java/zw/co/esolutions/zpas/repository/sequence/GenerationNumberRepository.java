package zw.co.esolutions.zpas.repository.sequence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.sequence.GenerationNumber;

import java.util.Optional;

/**
 * Created by alfred on 05 July 2019
 */
@Repository
public interface GenerationNumberRepository extends JpaRepository<GenerationNumber, String> {
    Optional<GenerationNumber> findByCustomerCodeAndUserCodeAndSequenceDate(String customerCode, String userCode, String sequenceDate);
}
