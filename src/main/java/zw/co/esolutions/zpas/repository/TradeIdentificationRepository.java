package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.TradeIdentification;

/**
 * Created by alfred on 27 Jan 2019
 */
@Repository
public interface TradeIdentificationRepository extends JpaRepository<TradeIdentification, String> {
}
