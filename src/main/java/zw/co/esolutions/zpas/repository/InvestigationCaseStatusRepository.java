package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.InvestigationCaseStatus;

import java.util.List;

/**
 * Created by mabuza on 29 Jan 2019
 */
@Repository
public interface InvestigationCaseStatusRepository extends JpaRepository<InvestigationCaseStatus, String> {
    List<InvestigationCaseStatus> findAllByInvestigationCase_id(String id);
}
