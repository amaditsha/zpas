package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PaymentInvestigationCaseRejection;

/**
 * Created by mabuza on 29 Jan 2019
 */
@Repository
public interface PaymentInvestigationCaseRejectionRepository extends JpaRepository<PaymentInvestigationCaseRejection, String> {
}
