package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PartyMerchantAccount;

import java.util.List;

/**
 * Created by jnkomo on 27 Mar 2019
 */
@Repository
public interface PartyMerchantAccountRepository extends JpaRepository<PartyMerchantAccount, String> {
    List<PartyMerchantAccount> findAllByLinkedPartyId(String partyId);
}
