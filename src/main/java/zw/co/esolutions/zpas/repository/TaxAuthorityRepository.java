package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.TaxAuthority;

/**
 * Created by jnkomo on 27 Mar 2019
 */
@Repository
public interface TaxAuthorityRepository extends JpaRepository<TaxAuthority, String> {
}
