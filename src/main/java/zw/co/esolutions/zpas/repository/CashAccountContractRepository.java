package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.CashAccountContract;

import java.util.List;

/**
 * Created by anotida on 27 Jan 2019
 */
@Repository
public interface CashAccountContractRepository extends JpaRepository<CashAccountContract, String> {
    List<CashAccountContract> findAllByCashAccount_Id(String cashAccountId);
}
