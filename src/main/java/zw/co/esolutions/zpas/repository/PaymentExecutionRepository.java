package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PaymentExecution;

@Repository
public interface PaymentExecutionRepository extends JpaRepository<PaymentExecution, String> { }
