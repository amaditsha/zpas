package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.FinancialInstitution;

import java.util.List;
import java.util.Optional;

/**
 * Created by tanatsa on 29 Jan 2019
 */

@Repository
public interface FinancialInstitutionRepository extends JpaRepository<FinancialInstitution, String> {
    List<FinancialInstitution> findAllByRole_Id(String roleId);
    Optional<FinancialInstitution> findFinancialInstitutionByVirtualSuffix(String virtualSuffix);

    Optional<FinancialInstitution> findByOrganisationIdentification_Id(String paymentIdentificationId);
}

