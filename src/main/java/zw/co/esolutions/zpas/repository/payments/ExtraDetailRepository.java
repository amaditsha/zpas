package zw.co.esolutions.zpas.repository.payments;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.esolutions.zpas.model.AccountIdentification;
import zw.co.esolutions.zpas.model.payments.ExtraDetail;

/**
 * Created by alfred on 28 August 2019
 */
public interface ExtraDetailRepository extends JpaRepository<ExtraDetail, String> {
}
