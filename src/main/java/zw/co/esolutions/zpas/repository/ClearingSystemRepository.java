package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.ClearingSystem;

import java.util.List;

@Repository
public interface ClearingSystemRepository extends JpaRepository<ClearingSystem, String> {
    List<ClearingSystem> findByEntityStatus(String clearingSystemStatus);
}
