package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.PaymentObligationPartyRole;
import zw.co.esolutions.zpas.model.Role;

import java.util.List;

@Repository
public interface PaymentObligationPartyRoleRepository extends JpaRepository<PaymentObligationPartyRole, String> {
    List<PaymentObligationPartyRole> findAllByPlayer_Id(String rolePlayerId);
}
