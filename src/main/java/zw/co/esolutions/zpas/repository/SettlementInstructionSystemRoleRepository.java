package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.SettlementInstructionSystemRole;

import java.util.List;

@Repository
public interface SettlementInstructionSystemRoleRepository extends JpaRepository<SettlementInstructionSystemRole, String> {
    List<SettlementInstructionSystemRole> findByEntityStatus(String status);
}
