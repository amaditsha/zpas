package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.esolutions.zpas.model.DocumentPartyRole;

import java.util.List;

/**
 * Created by alfred on 11 Mar 2019
 */
public interface DocumentPartyRoleRepository extends JpaRepository<DocumentPartyRole, String> {
    List<DocumentPartyRole> findAllByPlayer_Id(String rolePlayerId);
}
