package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.CashAccount;

import java.util.List;

@Repository
public interface CashAccountRepository extends JpaRepository<CashAccount, String> {
    @Query("SELECT ca FROM CashAccount ca WHERE ca.identification.iBAN = :iBan")
    CashAccount findCashAccountByIdentificationIBAN(@Param("iBan") String iBan);

    @Query("SELECT ca FROM CashAccount ca WHERE ca.identification.number = :number")
    CashAccount findCashAccountByIdentificationNumber(@Param("number") String number);

    @Query("SELECT ca FROM CashAccount ca WHERE ca.identification.iBAN IN :iBans")
    List<CashAccount> findCashAccountsByIdentificationIbansIn(@Param("iBans") List<String> iBans);

    List<CashAccount> findAllByPartyRoleId(String partyRoleId);
}
