package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.CustomParty;

/**
 * Created by alfred on 13 Mar 2019
 */
@Repository
public interface CustomPartyRepository extends JpaRepository<CustomParty, String> {
}
