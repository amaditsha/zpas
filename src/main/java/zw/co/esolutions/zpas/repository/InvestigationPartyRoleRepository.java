package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.InitiatingPartyRole;
import zw.co.esolutions.zpas.model.InvestigationPartyRole;

@Repository
public interface InvestigationPartyRoleRepository extends JpaRepository<InvestigationPartyRole, String> {
}
