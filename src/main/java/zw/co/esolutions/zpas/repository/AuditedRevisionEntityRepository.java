package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.esolutions.zpas.model.enversaudit.AuditedRevisionEntity;

import java.util.List;

public interface AuditedRevisionEntityRepository extends JpaRepository<AuditedRevisionEntity, Integer> {
    List<AuditedRevisionEntity> findAllByUsername(String username);
}
