package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.AccountIdentification;
import zw.co.esolutions.zpas.model.UploadAccounts;

@Repository
public interface UploadAccountsRepository extends JpaRepository<UploadAccounts, String> {
}
