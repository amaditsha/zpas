package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.InstructingReimbursementAgent;

import java.util.List;

@Repository
public interface InstructingReimbursementAgentRepository extends JpaRepository<InstructingReimbursementAgent, String> {
    List<InstructingReimbursementAgent> findByEntityStatus(String instructingReimbursementAgentStatus);
}
