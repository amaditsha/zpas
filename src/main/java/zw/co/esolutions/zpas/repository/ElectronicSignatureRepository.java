package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.ElectronicSignature;

@Repository
public interface ElectronicSignatureRepository extends JpaRepository<ElectronicSignature, String> {
}
