package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.ObligationFulfilment;

/**
 * Created by alfred on 01 Feb 2019
 */
@Repository
public interface ObligationFulfilmentRepository extends JpaRepository<ObligationFulfilment, String> {
}
