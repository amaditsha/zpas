package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.esolutions.zpas.model.Scheme;

/**
 * Created by alfred on 07 Mar 2019
 */
public interface SchemeRepository extends JpaRepository<Scheme, String> {
}
