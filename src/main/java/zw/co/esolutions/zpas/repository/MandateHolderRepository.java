package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.Account;
import zw.co.esolutions.zpas.model.MandateHolder;

import java.util.List;

@Repository
public interface MandateHolderRepository extends JpaRepository<MandateHolder, String> {
    List<MandateHolder> findAllByPlayer_Id(String partyId);
}
