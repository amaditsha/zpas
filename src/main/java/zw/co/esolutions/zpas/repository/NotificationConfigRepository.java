package zw.co.esolutions.zpas.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.model.NotificationConfig;

import java.util.List;

@Repository
public interface NotificationConfigRepository extends CrudRepository<NotificationConfig, String> {
    List<NotificationConfig> findAllByParty_id(String id);
    NotificationConfig findFirstByNotificationTypeAndParty_id(NotificationType notificationType, String partyId);
}
