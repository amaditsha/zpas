package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.InvestmentAccountContract;

/**
 * Created by anotida on 27 Jan 2019
 */
@Repository
public interface InvestmentAccountContractRepository extends JpaRepository<InvestmentAccountContract, String> {
}
