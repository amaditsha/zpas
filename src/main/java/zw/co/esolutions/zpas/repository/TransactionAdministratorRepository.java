package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.TransactionAdministrator;

import java.util.List;

@Repository
public interface TransactionAdministratorRepository extends JpaRepository<TransactionAdministrator, String> {
    List<TransactionAdministrator> findByEntityStatus(String transactionAdministratorStatus);
}
