package zw.co.esolutions.zpas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.esolutions.zpas.model.OrganisationName;

import java.util.List;

/**
 * Created by tanatsa on 29 Jan 2019
 */

@Repository
public interface OrganisationNameRepository extends JpaRepository<OrganisationName, String> {
    List<OrganisationName> findAllByOrganisationId(String organisationIdentificationId);
    @Query("SELECT onm FROM OrganisationName onm WHERE LOWER(onm.legalName) LIKE %:search% OR " +
            "LOWER(onm.tradingName) LIKE %:search% OR LOWER(onm.shortName) LIKE %:search% " +
            "OR LOWER(CONCAT(onm.legalName, ' ', onm.tradingName, ' ', onm.shortName)) LIKE %:search% " +
            "ORDER BY onm.dateCreated DESC")
    List<OrganisationName> searchOrganisationName(@Param("search") String search);
}
