package zw.co.esolutions.zpas.controller.currency;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.enums.AdjustmentDirectionCode;
import zw.co.esolutions.zpas.enums.AdjustmentTypeCode;
import zw.co.esolutions.zpas.enums.ExchangeRateTypeCode;
import zw.co.esolutions.zpas.enums.TaxationBasisCode;
import zw.co.esolutions.zpas.model.Adjustment;
import zw.co.esolutions.zpas.model.CurrencyExchange;
import zw.co.esolutions.zpas.services.iface.currency.CurrencyExchangeService;

import javax.validation.Valid;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/currency-exchange")
public class CurrencyExchangeController {

    @Autowired
    private CurrencyExchangeService currencyExchangeService;

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CURRENCY_EXCHANGE')")
    public String createCurrencyExchangePage(Model model) {
        CurrencyExchange currencyExchange = new CurrencyExchange();
        Adjustment adjustment = new Adjustment();
        adjustment.setCalculationMethod(TaxationBasisCode.Flat);
        adjustment.setDirection(AdjustmentDirectionCode.Added);
        adjustment.setType(AdjustmentTypeCode.Discount);
        currencyExchange.setAdjustment(adjustment);
        currencyExchange.setRateType(ExchangeRateTypeCode.Agreed);

        model.addAttribute("currencyExchange", currencyExchange);
        model.addAttribute("rateTypes", ExchangeRateTypeCode.values());
        model.addAttribute("calculationMethods", TaxationBasisCode.values());
        model.addAttribute("directions", AdjustmentDirectionCode.values());
        model.addAttribute("adjustmentTypes", AdjustmentTypeCode.values());
        return "currency/create_currency_exchange";
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CURRENCY_EXCHANGE')")
    public String createCurrencyExchange(@Valid @ModelAttribute("currencyExchange") @RequestBody CurrencyExchange currencyExchange, Errors errors, Model model) {
        final CurrencyExchange currencyExchange1 = currencyExchangeService.createCurrencyExchange(currencyExchange);
        model.addAttribute("currencyExchange", currencyExchange1);
        return "currency/view_currency_exchange";
    }

    @GetMapping("/view/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_CURRENCY_EXCHANGE')")
    public String viewCurrencyExchangeById(Model model, @PathVariable(name = "id") String id) {
        final Optional<CurrencyExchange> currencyExchangeOptional = currencyExchangeService.findCurrencyExchangeById(id);
        return currencyExchangeOptional.map(currencyExchange -> {
            model.addAttribute("currencyExchange", currencyExchange);
            model.addAttribute("title", "View Currency Exchange");
            return "currency/view_currency_exchange";
        }).orElseGet(() -> {
            model.addAttribute("currencyExchange", null);
            return "currency/view_currency_exchange";
        });
    }
}
