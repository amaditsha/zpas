package zw.co.esolutions.zpas.controller.payments;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.agreement.ApprovalRequestDTO;
import zw.co.esolutions.zpas.dto.agreement.ApprovalResponseDTO;
import zw.co.esolutions.zpas.dto.payments.*;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountMandateService;
import zw.co.esolutions.zpas.services.iface.beneficiary.BeneficiaryService;
import zw.co.esolutions.zpas.services.iface.investigationcase.PaymentInvestigationCaseService;
import zw.co.esolutions.zpas.services.iface.merchant.MerchantService;
import zw.co.esolutions.zpas.services.iface.merchant.PartyMerchantAccountService;
import zw.co.esolutions.zpas.services.iface.party.PartyService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentObligationsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentUploadsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.process.PaymentProcessService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.iface.signature.ApprovalProcessorService;
import zw.co.esolutions.zpas.services.iface.tax.TaxAuthorityService;
import zw.co.esolutions.zpas.services.iface.taxclearance.TaxClearanceService;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentActivationInvalidRequestException;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.ReactiveService;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.ScheduledService;
import zw.co.esolutions.zpas.services.impl.payments.zimra.ZimraProcessor;
import zw.co.esolutions.zpas.services.impl.payments.zimra.response.AssessmentValidationResponse;
import zw.co.esolutions.zpas.services.impl.taxclearance.TaxClearanceJsonResponse;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.MakePaymentRequestType;
import zw.co.esolutions.zpas.utilities.enums.SpecialPayee;
import zw.co.esolutions.zpas.utilities.storage.StorageFileNotFoundException;
import zw.co.esolutions.zpas.utilities.storage.StorageService;
import zw.co.esolutions.zpas.utilities.util.ServiceResponse;
import zw.co.esolutions.zpas.utilities.util.StringUtil;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by alfred on 26 Feb 2019
 */
@Slf4j
@Controller
@Scope("session")
@RequestMapping("/payments")
public class PaymentsController {

    @Autowired
    private PaymentUploadsService paymentUploadsService;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private PaymentObligationsService paymentObligationsService;

    @Autowired
    private BeneficiaryService beneficiaryService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private PartyMerchantAccountService partyMerchantAccountService;

    @Autowired
    private TaxAuthorityService taxAuthorityService;

    @Autowired
    private PaymentProcessService paymentProcessService;

    @Autowired
    private PaymentInvestigationCaseService paymentInvestigationCaseService;

    @Autowired
    private ApprovalProcessorService approvalProcessorService;

    @Autowired
    private CashAccountMandateService cashAccountMandateService;

    @Autowired
    private ScheduledService scheduledService;

    @Autowired
    private TaxClearanceService taxClearanceService;

    @Autowired
    private ReactiveService reactiveService;

    @Autowired
    private ZimraProcessor zimraProcessor;

    @RequestMapping(path = "/sse/infinite/{requestId}", method = RequestMethod.GET)
    public SseEmitter getInfiniteMessages(HttpSession session, @PathVariable("requestId") String requestId) {
        return scheduledService.getInfiniteMessages(requestId);
    }

    @RequestMapping(path = "/sse/tax-clearances/infinite/{requestId}", method = RequestMethod.GET)
    public SseEmitter getBPNVerificationInfiniteMessages(HttpSession session, @PathVariable("requestId") String requestId) {
        return taxClearanceService.getBPNVerificationMessages(requestId);
    }

    @RequestMapping(path = "/sse/finite/{count}", method = RequestMethod.GET)
    public SseEmitter getFiniteMessages(@PathVariable int count) {
        return reactiveService.getFiniteMessages(count);
    }

    @GetMapping("/upload")
    @PreAuthorize("hasAuthority('ROLE_UPLOAD_BULK_PAYMENT')")
    public String uploadBulkPaymentPage(Model model, HttpSession session,
                                        @RequestParam(value = "accountId", required = false) String accountId) {
        String clientId = (String) session.getAttribute("loginAsId");

        final List<Account> clientAccounts = accountService.getAccountsByClientId(clientId);
        final List<PaymentInstrumentCode> paymentInstrumentCodes = Arrays.asList(PaymentInstrumentCode.CustomerCreditTransfer);

        if (!StringUtils.isEmpty(accountId)) {
            Optional.ofNullable(accountService.findAccountById(accountId)).ifPresent(account -> {
                model.addAttribute("account", account);
            });
        }

        model.addAttribute("title", "Upload Payment");
        model.addAttribute("clientAccounts", clientAccounts);
        model.addAttribute("paymentTypeCodes", PaymentTypeCode.values());
        model.addAttribute("paymentInstrumentCodes", paymentInstrumentCodes);
        model.addAttribute("priorityCodes", PriorityCode.values());
        model.addAttribute("purposeCodes", ProprietaryPurposeCode.values());
        return "payments/upload_bulk_payments";
    }

    @GetMapping("/upload/errors")
    @PreAuthorize("hasAuthority('ROLE_UPLOAD_BULK_PAYMENT')")
    public String paymentUploadErrors(Model model, HttpSession session) {
        return "payments/upload_bulk_payment_errors";
    }

    @GetMapping("/tax-clearances/verify/{bpn}")
    @ResponseBody
    public TaxClearanceJsonResponse getCurrentTaxClearanceByBPN(@PathVariable("bpn") String bpn) {
        log.info("Fetching current tax clearance for company with bpn: {}", bpn);
        TaxClearanceJsonResponse taxClearanceJsonResponse = taxClearanceService.verifyTaxClearanceByBPN(bpn).orElse(null);
        log.info("Tax Clearance Json Response: {}", taxClearanceJsonResponse);
        return taxClearanceJsonResponse;
    }

    @PostMapping("/upload")
    @PreAuthorize("hasAuthority('ROLE_UPLOAD_BULK_PAYMENT')")
    public String uploadBulkPayment(@RequestParam("paymentsFile") MultipartFile paymentsFile,
                                    @RequestParam("purpose") String purpose,
                                    @RequestParam("clientReference") String clientReference,
                                    @RequestParam("accountId") String accountId,
                                    @RequestParam("paymentType") String paymentType,
                                    @RequestParam("paymentInstrument") String paymentInstrument,
                                    @RequestParam("priority") String priority,
                                    @RequestParam(value = "verifyAccountInfo", required = false) boolean verifyAccountInfo,
                                    @RequestParam(value = "stopWhenFailedVerification", required = false) boolean stopWhenFailedVerification,
                                    @RequestParam(value = "batchBookingEnabled", required = false) boolean batchBookingEnabled,
                                    RedirectAttributes redirectAttributes, HttpSession session) {
        if (paymentsFile == null) {
            redirectAttributes.addFlashAttribute("error", "Tried to upload an empty file, not allowed.");
            return "redirect:/payments/upload";
        }
        try {
            storageService.store(paymentsFile);
        } catch (RuntimeException re) {
            redirectAttributes.addFlashAttribute("error", "An error occurred trying to save file. " + re.getMessage());
            return "redirect:/payments/upload";
        }

        String clientId = (String) session.getAttribute("clientId");

        final PaymentUploadDTO paymentUploadDTO = PaymentUploadDTO.builder()
                .fileName(paymentsFile.getOriginalFilename())
                .accountId(accountId)
                .paymentType(paymentType)
                .paymentInstrument(paymentInstrument)
                .priority(priority)
                .clientId(clientId)
                .purpose(purpose)
                .clientReference(clientReference)
                .verifyAccountInfo(verifyAccountInfo)
                .stopWhenFailedVerification(stopWhenFailedVerification)
                .batchBookingEnabled(batchBookingEnabled)
                .build();
        log.info("Payment Upload DTO is: {}", paymentUploadDTO);
        try {
            return paymentUploadsService.uploadBulkPayment(paymentUploadDTO).map(payment -> {
                redirectAttributes.addFlashAttribute("message",
                        "You successfully uploaded " + paymentsFile.getOriginalFilename());
                return "redirect:view/" + payment.getId();
            }).orElseGet(() -> {
                throw new RuntimeException("No payment returned.");
            });
        } catch (PaymentRequestInvalidArgumentException e) {
            redirectAttributes.addFlashAttribute("error", "An error occurred. " + e.getMessage());

            if (!e.getInvalidEntries().isEmpty()) {
                redirectAttributes.addFlashAttribute("invalidEntries", e.getInvalidEntries());
                return "redirect:/payments/upload/errors";
            }
            return "redirect:/payments/upload";
        }
    }

    @GetMapping("/make-payment")
    @PreAuthorize("hasAuthority('ROLE_MAKE_PAYMENT')")
    public String makePaymentPage(Model model, HttpSession session,
                                  @RequestParam(value = "beneficiaryId", required = false) String beneficiaryId,
                                  @RequestParam(value = "accountId", required = false) String accountId,
                                  @RequestParam(value = "type", required = false) String type) {
        String clientId = (String) session.getAttribute("loginAsId");
        final List<Account> clientAccounts = accountService.getAccountsByClientId(clientId);
        final List<FinancialInstitution> clientFinancialInstitutions = financialInstitutionService.getFinancialInstitutionsByClientId(clientId);
        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        final List<PaymentInstrumentCode> paymentInstrumentCodes = Arrays.asList(PaymentInstrumentCode.CustomerCreditTransfer);
        final List<Merchant> merchants = merchantService.getMerchants();
        final List<PartyMerchantAccount> partyMerchantAccountsByClientId = partyMerchantAccountService.getPartyMerchantAccountsByClientId(clientId);
        final List<AssessmentValidationResponse> assessmentValidationResponses = new ArrayList<>();

        final List<TaxAuthority> taxAuthorities = taxAuthorityService.getTaxAuthorities();
        final Optional<Party> partyOptional = partyService.getPartyById(clientId);
        partyOptional.ifPresent(party -> {
            final String businessPartnerNumber = party.getBusinessPartnerNumber();
            final List<AssessmentValidationResponse> assessmentInfosByBpn = zimraProcessor.getAssessmentInfosByBpn(businessPartnerNumber);
            assessmentValidationResponses.addAll(assessmentInfosByBpn);
            log.info("Found party: {}. The business partner number is: {}", party.getName(), businessPartnerNumber);
            model.addAttribute("taxIdentificationNumber", businessPartnerNumber);
        });

        if (!StringUtils.isEmpty(beneficiaryId)) {
            beneficiaryService.getBeneficiary(beneficiaryId).ifPresent(beneficiary -> {
                model.addAttribute("beneficiary", beneficiary);
            });
        }

        if (!StringUtils.isEmpty(accountId)) {
            Optional.ofNullable(accountService.findAccountById(accountId)).ifPresent(account -> {
                model.addAttribute("account", account);
            });
        }

        MakePaymentRequestType makePaymentRequestType = null;
        try {
            makePaymentRequestType = MakePaymentRequestType.valueOf(type);
        } catch (RuntimeException re) {
            makePaymentRequestType = MakePaymentRequestType.INDIVIDUAL;
        }

        model.addAttribute("title", "Make Payment");
        model.addAttribute("clientAccounts", clientAccounts);
        model.addAttribute("merchants", merchants);
        model.addAttribute("assessmentValidationResponses", assessmentValidationResponses);
        model.addAttribute("partyMerchantAccounts", partyMerchantAccountsByClientId);
        model.addAttribute("taxAuthorities", taxAuthorities);
        model.addAttribute("merchantAccountNumber", "");
        model.addAttribute("requestedPaymentType", makePaymentRequestType.name());
        model.addAttribute("clientFinancialInstitutions", clientFinancialInstitutions);
        model.addAttribute("financialInstitutions", financialInstitutions);
        model.addAttribute("paymentTypeCodes", PaymentTypeCode.values());
        model.addAttribute("paymentInstrumentCodes", paymentInstrumentCodes);
        model.addAttribute("priorityCodes", PriorityCode.values());
        model.addAttribute("purposeCodes", ProprietaryPurposeCode.values());
        return "payments/create_payment";
    }

    @PostMapping("/make-payment")
    @PreAuthorize("hasAuthority('ROLE_MAKE_PAYMENT')")
    public String makePayment(@Valid MakePaymentDTO makePaymentDTO, RedirectAttributes redirectAttributes, HttpSession httpSession) {

        log.info("The DTO is: {}", makePaymentDTO);
        String clientId = (String) httpSession.getAttribute("clientId");
        String loginAsId = (String) httpSession.getAttribute("loginAsId");
        if (StringUtils.isEmpty(makePaymentDTO.getClientName())) {
            makePaymentDTO.setClientName((String) httpSession.getAttribute("clientName"));
        }
        String sourceAccountId = makePaymentDTO.getSourceAccountId();
        ApprovalResponseDTO initiateResponseDTO = cashAccountMandateService.getPaymentInitiators(sourceAccountId, clientId);
        if (initiateResponseDTO.isDone()) {
            log.info("The client id {} is allowed to initiate the payment. The response is {}", clientId, initiateResponseDTO.getNarrative());
            Optional<IndividualPayment> individualPaymentOptional = null;
            try {
                individualPaymentOptional = paymentsService.makePayment(makePaymentDTO);
            } catch (PaymentRequestInvalidArgumentException e) {
                e.printStackTrace();
                redirectAttributes.addFlashAttribute("error", "An error occurred. " + e.getMessage());
                return "redirect:/payments/make-payment";
            }
            redirectAttributes.addFlashAttribute("message", "Make payment request submitted successfully.");
            return individualPaymentOptional.map(individualPayment -> "redirect:view/" + individualPayment.getId()).orElseGet(() -> "redirect:view/not-found");
        } else {
            log.info("The client id {} is not allowed to initiate the payment. The response is {}", clientId, initiateResponseDTO.getNarrative());
            redirectAttributes.addFlashAttribute("error", "You are not authorised to initiate payment for this account.");
            return "redirect:/payments/make-payment";
        }
    }

    @ResponseBody
    @PostMapping("/beneficiaries/info")
    public List<CustomPartyDTO> getPaymentBeneficiariesInfo(@RequestBody List<String> paymentIds) {
        List<CustomPartyDTO> customPartyDTOS = paymentsService.getPaymentBeneficiariesInfoByPaymentIds(paymentIds);
        return customPartyDTOS;
    }

    @GetMapping("/status/dashboard")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PAYMENT')")
    public String paymentsStatusDashboard(Model model, HttpSession session) {
        String clientId = (String) session.getAttribute("loginAsId");
        model.addAttribute("title", "Payments Statuses Dashboard");
        return "payments/payments_status_dashboard";
    }

    @GetMapping("/status/dashboard/fetch")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PAYMENT')")
    @ResponseBody
    public PaymentsStatusDTO fetchPaymentsStatus(HttpSession session) {
        String clientId = (String) session.getAttribute("loginAsId");

        return paymentsService.getPaymentsStatusesByClientId(clientId).orElseGet(() -> PaymentsStatusDTO.builder().build());
    }

    @GetMapping("/requests")
    @PreAuthorize("hasAuthority('ROLE_ACTIVATE_PAYMENT_REQUEST')")
    public String creditorPaymentActivationRequest(Model model, HttpSession session,
                                                   @RequestParam(value = "beneficiaryId", required = false) String beneficiaryId,
                                                   @RequestParam(value = "accountId", required = false) String accountId) {
        String clientId = (String) session.getAttribute("loginAsId");

        final List<Account> clientAccounts = accountService.getAccountsByClientId(clientId);
        final List<FinancialInstitution> clientFinancialInstitutions = financialInstitutionService.getFinancialInstitutionsByClientId(clientId);
        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        final List<PaymentInstrumentCode> paymentInstrumentCodes = Arrays.asList(PaymentInstrumentCode.CustomerCreditTransfer);
        final List<Merchant> merchants = merchantService.getMerchants();
        final List<PartyMerchantAccount> partyMerchantAccountsByClientId = partyMerchantAccountService.getPartyMerchantAccountsByClientId(clientId);

        final List<TaxAuthority> taxAuthorities = taxAuthorityService.getTaxAuthorities();
        final Optional<Party> partyOptional = partyService.getPartyById(clientId);
        partyOptional.ifPresent(party -> {
            party.getIdentification().stream().findFirst().ifPresent(partyIdentificationInformation -> {
                model.addAttribute("taxIdentificationNumber", partyIdentificationInformation.getTaxIdentificationNumber());
            });
        });

        if (!StringUtils.isEmpty(beneficiaryId)) {
            beneficiaryService.getBeneficiary(beneficiaryId).ifPresent(beneficiary -> {
                model.addAttribute("beneficiary", beneficiary);
            });
        }

        if (!StringUtils.isEmpty(accountId)) {
            Optional.ofNullable(accountService.findAccountById(accountId)).ifPresent(account -> {
                model.addAttribute("account", account);
            });
        }

        model.addAttribute("title", "Payment Activation Request");
        model.addAttribute("clientAccounts", clientAccounts);
        model.addAttribute("merchants", merchants);
        model.addAttribute("partyMerchantAccounts", partyMerchantAccountsByClientId);
        model.addAttribute("taxAuthorities", taxAuthorities);
        model.addAttribute("merchantAccountNumber", "");
        model.addAttribute("clientFinancialInstitutions", clientFinancialInstitutions);
        model.addAttribute("financialInstitutions", financialInstitutions);
        model.addAttribute("paymentTypeCodes", PaymentTypeCode.values());
        model.addAttribute("paymentInstrumentCodes", paymentInstrumentCodes);
        model.addAttribute("priorityCodes", PriorityCode.values());
        return "payments/payment_activation_request";
    }

    @PostMapping("/requests")
    @PreAuthorize("hasAuthority('ROLE_ACTIVATE_PAYMENT_REQUEST')")
    public String creditorPaymentActivationRequest(@Valid ActivatePaymentRequestDTO activatePaymentRequestDTO, RedirectAttributes redirectAttributes, HttpSession httpSession) {
        String clientId = (String) httpSession.getAttribute("loginAsId");
        String clientName = (String) httpSession.getAttribute("clientName");
        activatePaymentRequestDTO.setClientId(clientId);
        activatePaymentRequestDTO.setClientName(clientName);
        log.info("The DTO is: {}", activatePaymentRequestDTO);
        try {
            paymentObligationsService.activateCreditorPaymentRequest(activatePaymentRequestDTO);
        } catch (PaymentActivationInvalidRequestException e) {
            redirectAttributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/payments/requests";
        }
        redirectAttributes.addFlashAttribute("message", "Creditor Payment Activation request submitted successfully.");
        return "redirect:/payments/obligations";
    }

    @GetMapping("/requests/approve/{obligationId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_PAYMENT_REQUEST')")
    public String approveCreditorPaymentActivationRequest(@PathVariable("obligationId") String obligationId, RedirectAttributes redirectAttributes, HttpSession httpSession) {
        log.info("Approve payment request.");
        String clientId = (String) httpSession.getAttribute("loginAsId");
        final Optional<PaymentObligation> paymentObligationOptional = paymentObligationsService.approveCreditorPaymentRequest(obligationId, clientId);
        return paymentObligationOptional.map(paymentObligation -> {
            redirectAttributes.addFlashAttribute("message", "Creditor Payment Activation approved.");
            return "redirect:/payments/obligations/view/" + obligationId;
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("error", "Creditor Payment Activation approval failed. Note that you can't approve your own requests.");
            return "redirect:/payments/obligations/view/" + obligationId;
        });
    }

    @GetMapping("/requests/reject/{obligationId}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_PAYMENT_REQUEST')")
    public String rejectCreditorPaymentActivationRequest(@PathVariable("obligationId") String obligationId, RedirectAttributes redirectAttributes, HttpSession httpSession) {
        log.info("Reject payment request.");
        String clientId = (String) httpSession.getAttribute("loginAsId");
        final Optional<PaymentObligation> paymentObligationOptional = paymentObligationsService.rejectCreditorPaymentRequest(obligationId, clientId);
        return paymentObligationOptional.map(paymentObligation -> {
            redirectAttributes.addFlashAttribute("message", "Creditor Payment Activation rejected.");
            return "redirect:/payments/obligations";
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("error", "Creditor Payment Activation rejection failed. Note that you can't reject your own requests.");
            return "redirect:/payments/obligations";
        });
    }

    @GetMapping("/obligations")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PAYMENT_OBLIGATIONS')")
    public String viewPaymentObligations(Model model, HttpSession session) {
        String clientId = (String) session.getAttribute("loginAsId");

        final List<PaymentObligation> paymentObligationsRaisedByClient = paymentObligationsService.getPaymentObligationsRaisedByClient(clientId);
        final List<PaymentObligation> paymentObligationsTargetingClient = paymentObligationsService.getPaymentObligationsTargetingClient(clientId);
        model.addAttribute("title", "Payment Obligations");
        model.addAttribute("paymentObligationsRaisedByClient", paymentObligationsRaisedByClient);
        model.addAttribute("paymentObligationsTargetingClient", paymentObligationsTargetingClient);
        return "payments/view_payment_obligations";
    }

    @GetMapping("/obligations/view/{paymentObligationId}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PAYMENT_OBLIGATION')")
    public String viewPaymentObligation(Model model, HttpSession session, @PathVariable("paymentObligationId") String paymentObligationId) {

        final Optional<PaymentObligation> optionalPaymentObligation = paymentObligationsService.getPaymentObligationsById(paymentObligationId);
        return optionalPaymentObligation.map(paymentObligation -> {
            model.addAttribute("title", "Payment Obligation");
            model.addAttribute("paymentObligation", paymentObligation);
            model.addAttribute("obligationStatus", paymentObligation.getObligationStatus());
            return "payments/view_payment_obligation";
        }).orElseGet(() -> {
            model.addAttribute("title", "Payment Obligation");
            model.addAttribute("paymentObligation", null);
            model.addAttribute("status", "pending");
            model.addAttribute("error", "Not found");
            return "payments/view_payment_obligation";
        });
    }

    @GetMapping("/")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PAYMENTS')")
    public String viewPayments(Model model, HttpSession session,
                               @RequestParam(value = "searchPhrase", required = false) String searchPhrase,
                               @RequestParam(value = "accountId", required = false) String accountId,
                               @RequestParam(value = "paymentInstrument", required = false) String paymentInstrument,
                               @RequestParam(value = "paymentType", required = false) String paymentType,
                               @RequestParam(value = "specialPayee", required = false) String specialPayee,
                               @RequestParam(value = "valueDateFrom", required = false) String valueDateFrom,
                               @RequestParam(value = "valueDateTo", required = false) String valueDateTo,
                               @RequestParam(value = "dateCreatedFrom", required = false) String dateCreatedFrom,
                               @RequestParam(value = "dateCreatedTo", required = false) String dateCreatedTo) {

        final List<Payment> payments = new ArrayList<>();
        String clientId = (String) session.getAttribute("loginAsId");

        SearchDTO searchDTO = SearchDTO.builder()
                .clientId(clientId)
                .searchPhrase(searchPhrase)
                .accountId(accountId)
                .paymentInstrument(paymentInstrument)
                .paymentType(paymentType)
                .valueDateFrom(valueDateFrom)
                .valueDateTo(valueDateTo)
                .dateCreatedFrom(dateCreatedFrom)
                .dateCreatedTo(dateCreatedTo)
                .build();

        if (searchDTO.isValid()) {
            log.info("Search DTO is: {}", searchDTO);
            payments.addAll(paymentsService.searchPayments(searchDTO));
        } else {
            payments.addAll(paymentsService.getLatestPaymentsByClientId(clientId));
        }
        final List<Account> clientAccounts = accountService.getAccountsByClientId(clientId);
        final List<PaymentInstrumentCode> paymentInstrumentCodes = Arrays.asList(PaymentInstrumentCode.CustomerCreditTransfer);

        try {
            final SpecialPayee specialPayeeEnum = SpecialPayee.valueOf(specialPayee);
            payments.removeIf(p -> p.getSpecialPayee() != specialPayeeEnum);
        } catch (RuntimeException re) {

        }

        model.addAttribute("clientAccounts", clientAccounts);
        model.addAttribute("title", "Payments");
        model.addAttribute("payments", payments);
        model.addAttribute("paymentTypeCodes", PaymentTypeCode.values());
        model.addAttribute("paymentInstrumentCodes", paymentInstrumentCodes);
        model.addAttribute("priorityCodes", PriorityCode.values());
        return "payments/view_payments";
    }

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_BULK_PAYMENT')")
    public String createBulkPaymentPage(Model model, HttpSession session,
                                        @RequestParam(value = "accountId", required = false) String accountId) {
        String clientId = (String) session.getAttribute("clientId");

        final List<Account> clientAccounts = accountService.getAccountsByClientId(clientId);
        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutionsByClientId(clientId);
        final List<PaymentInstrumentCode> paymentInstrumentCodes = Arrays.asList(PaymentInstrumentCode.CustomerCreditTransfer);

        if (!StringUtils.isEmpty(accountId)) {
            Optional.ofNullable(accountService.findAccountById(accountId)).ifPresent(account -> {
                model.addAttribute("account", account);
            });
        }

        model.addAttribute("title", "Create Payment");
        model.addAttribute("clientAccounts", clientAccounts);
        model.addAttribute("financialInstitutions", financialInstitutions);
        model.addAttribute("paymentTypeCodes", PaymentTypeCode.values());
        model.addAttribute("paymentInstrumentCodes", paymentInstrumentCodes);
        model.addAttribute("priorityCodes", PriorityCode.values());
        return "payments/create_bulk_payments";
    }

    @ResponseBody
    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_BULK_PAYMENT')")
    public CreateBulkPaymentDTO createBulkPayment(@RequestBody CreateBulkPaymentDTO createBulkPaymentDTO, RedirectAttributes redirectAttributes) {
        log.info("The request body is : -> {} " + createBulkPaymentDTO);
        try {
            return paymentUploadsService.createBulkPayment(createBulkPaymentDTO).map(bulkPayment -> {
                createBulkPaymentDTO.setBulkPaymentId(bulkPayment.getId());
                return createBulkPaymentDTO;
            }).orElseGet(() -> {
                return null;
            });
        } catch (PaymentRequestInvalidArgumentException e) {
            redirectAttributes.addFlashAttribute("error", "Failed to create bulk payment. " + e.getMessage());
            return null;
        }
    }

    @GetMapping("/view/{paymentId}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PAYMENT')")
    public String viewPaymentPage(@PathVariable("paymentId") String paymentId,
                                  @RequestParam("page") Optional<Integer> page,
                                  @RequestParam("size") Optional<Integer> size, Model model) {
        return paymentsService.getPaymentById(paymentId)
                .map(payment -> viewPaymentPageHelper(payment, page, size, model))
                .orElseGet(() -> viewPaymentPageHelper(null, page, size, model));
    }

    private String viewPaymentPageHelper(Payment payment, Optional<Integer> page, Optional<Integer> size, Model model) {
        if (payment != null) {
            final int currentPage = page.map(integer -> {
                if (integer < 0) {
                    return 1;
                }
                return integer;
            }).orElse(1);
            final int pageSize = size.orElse(10);
            Pageable pageable = PageRequest.of(currentPage - 1, pageSize, Sort.by("dateCreated").descending());
            final Page<IndividualPayment> individualPaymentsPage =
                    paymentsService.getPaginatedIndividualPaymentsByBulkPaymentId(payment.getId(), pageable);

            model.addAttribute("individualPaymentsPage", individualPaymentsPage);
            model.addAttribute("paymentUrl", "/payments/view/" + payment.getId());

            int totalPages = individualPaymentsPage.getTotalPages();
            log.info("Central page is: {}", currentPage);
            int firstPage = currentPage - 5;
            if (firstPage < 1) {
                firstPage = 1;
            }
            log.info("First page is: {}", firstPage);
            int lastPage = firstPage + 9;
            if (lastPage > totalPages) {
                lastPage = totalPages;
            }
            log.info("Last page is: {}", lastPage);

            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(firstPage, lastPage)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
                model.addAttribute("currentPage", currentPage);
                model.addAttribute("totalPages", totalPages);
            }

            payment.getPaymentStatus().sort(Comparator.comparing(Status::getDateCreated).reversed());
            log.info("Payment Credit methods:\n{}\n", payment.getCreditMethod());
            Optional<PaymentStatus> paymentStatus = payment.getPaymentStatus().stream().findFirst();
            Boolean isPaymentCancellationAction = false;

            if (paymentStatus.isPresent()) {
                PaymentStatus paymentStatus1 = paymentStatus.get();
                if (paymentStatus1.getInstructionStatus().getCodeName().equals(PaymentInstructionStatusCode.Settled.getCodeName())) {
                    isPaymentCancellationAction = true;
                }
            }

            List<PaymentInvestigationCase> paymentInvestigationCases = paymentInvestigationCaseService.getPaymentInvestigationCasesByPayment(payment.getId());
            log.info("view payment investigation case by payment {}", paymentInvestigationCases);
            model.addAttribute("paymentInvestigationCases", paymentInvestigationCases);

            int acceptedPaymentsCount = 0;
            int rejectedPaymentsCount = 0;

            List<Payment> acceptedPayments = new ArrayList<>();
            List<Payment> rejectedPayments = new ArrayList<>();

            if (payment instanceof BulkPayment) {
                acceptedPayments = paymentsService.getPaymentsByStatusCode(PaymentStatusCode.Accepted, payment.getId());
                rejectedPayments = paymentsService.getPaymentsByStatusCode(PaymentStatusCode.Rejected, payment.getId());
            }

            acceptedPaymentsCount = acceptedPayments.size();
            rejectedPaymentsCount = rejectedPayments.size();
            log.info("AcceptedPaymentsCount>>>>>> " + acceptedPaymentsCount + " RejectedPaymentsCount>>>>>> " + rejectedPaymentsCount);

            Map<String, String> remittanceInfoMap = new HashMap<>();
            configureRemittanceInformation(remittanceInfoMap, payment);
            model.addAttribute("acceptedPaymentsCount", acceptedPaymentsCount);
            model.addAttribute("rejectedPaymentsCount", rejectedPaymentsCount);
            model.addAttribute("paymentStatuses", PaymentStatusCode.values());
            model.addAttribute("remittanceInfoMap", remittanceInfoMap);
            model.addAttribute("isPaymentCancellationAction", isPaymentCancellationAction);
            model.addAttribute("payment", payment);
            model.addAttribute("latestPaymentStatus", payment.getLatestPaymentStatus().orElse(null));
            model.addAttribute("title", "Payment: " + payment.getEndToEndId());
        } else {
            model.addAttribute("title", "Payment");
        }
        return "payments/view_payment";
    }

    @PostMapping("/resolve")
    public String resolvePayment(@Valid ResolvePaymentDTO resolvePaymentDTO) {
        log.info("Inside resolve payments...");
        Optional<Payment> paymentOptional = paymentsService.resolvePayment(resolvePaymentDTO);

        return "redirect:/payments/view/" + resolvePaymentDTO.getPaymentId();
    }

    private void configureRemittanceInformation(Map<String, String> remittanceInfoMap, Payment payment) {
        Optional.ofNullable(payment.getUnstructuredRemittanceInformation()).ifPresent(strings -> {
            strings.stream().forEach(s -> {
                final String[] keyValuePair = s.split(":");
                if (keyValuePair.length == 2) {
                    final String key = keyValuePair[0];
                    final String value = keyValuePair[1];
                    if (key.contains("Id")) {
                        if (key.contains("BankId")) {
                            final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(value);
                            switch (key) {
                                case "taxAuthorityBankId":
                                    financialInstitutionOptional.ifPresent(financialInstitution -> {
                                        final String financialInstitutionName = financialInstitution.getName();
                                        remittanceInfoMap.put("TAX AUTHORITY BANK", financialInstitutionName);
                                    });
                                    break;
                                case "merchantBankId":
                                    financialInstitutionOptional.ifPresent(financialInstitution -> {
                                        final String financialInstitutionName = financialInstitution.getName();
                                        remittanceInfoMap.put("MERCHANT BANK", financialInstitutionName);
                                    });
                                    break;
                            }
                        } else if (key.equals("merchantId")) {
                            merchantService.getMerchant(value).ifPresent(merchant -> {
                                remittanceInfoMap.put("MERCHANT", merchant.getName());
                            });
                        } else if (key.equals("taxAuthorityId")) {
                            taxAuthorityService.getTaxAuthority(value).ifPresent(taxAuthority -> {
                                remittanceInfoMap.put("TAX AUTHORITY", taxAuthority.getName());
                            });
                        }
                    } else if (key.equals("taxType")) {
                        remittanceInfoMap.put(StringUtil.formatCamelCaseToSpacedString(key).toUpperCase(), value.toUpperCase().replace('_', ' '));
                    } else if (key.equals("Code")) {
                        taxAuthorityService.getTaxAuthorityPaymentOfficeByCode(value).ifPresent(taxAuthorityPaymentOffice -> {
                            remittanceInfoMap.put("PAYMENT OFFICE", taxAuthorityPaymentOffice.getName());
                        });
                    } else {
                        remittanceInfoMap.put(StringUtil.formatCamelCaseToSpacedString(key).toUpperCase(), value.toUpperCase());
                    }
                } else {
                    log.error("Ill-formatted line, skipping.");
                }
            });
        });
    }

    @GetMapping("/approve/{paymentId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_PAYMENT')")
    public String approvePayment(@PathVariable("paymentId") String paymentId, Model model, RedirectAttributes redirectAttributes, HttpSession session) {
        String clientId = (String) session.getAttribute("clientId");
        Optional<Payment> optionalPayment = paymentsService.getPaymentById(paymentId);
        return optionalPayment.map(payment -> {
            String cashAccountId = Optional.ofNullable(payment.getAccount()).map(CashAccount::getId).orElse("");
            ApprovalRequestDTO approvalRequestDTO = ApprovalRequestDTO.builder()
                    .personId(clientId)
                    .paymentId(paymentId)
                    .cashAccountId(cashAccountId).build();
            ApprovalResponseDTO responseDTO = approvalProcessorService.approveReq(approvalRequestDTO);
            if (responseDTO.isDone()) {
                paymentsService.approvePayment(paymentId);
                redirectAttributes.addFlashAttribute("message", "Payment with reference " + payment.getEndToEndId() + " approved.");
            } else {
                redirectAttributes.addFlashAttribute("message", responseDTO.getNarrative());
            }
            return "redirect:/payments/view/" + paymentId;
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("error", "Oops! Payment can't be found.");
            return "redirect:/payments/view/" + paymentId;
        });
    }

    @GetMapping("/reject/{paymentId}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_PAYMENT')")
    public String rejectPayment(@PathVariable("paymentId") String paymentId, Model model, RedirectAttributes redirectAttributes) {
        paymentsService.rejectPayment(paymentId);
        redirectAttributes.addFlashAttribute("message", "Payment with rejected.");
        return "redirect:/payments/view/" + paymentId;
    }

    @GetMapping("/submit/{paymentId}")
    public String submitPayment(@PathVariable("paymentId") String paymentId, Model model, RedirectAttributes redirectAttributes, HttpSession httpSession) {
        String clientId = (String) httpSession.getAttribute("clientId");
        Optional<Payment> optionalPayment = paymentsService.getPaymentById(paymentId);
        return optionalPayment.map(payment -> {
            String cashAccountId = Optional.ofNullable(payment.getAccount()).map(CashAccount::getId).orElse("");
            ApprovalResponseDTO initiateResponseDTO = cashAccountMandateService.getPaymentInitiators(cashAccountId, clientId);

            if (initiateResponseDTO.isDone()) {
                log.info("The client id {} is allowed to initiate the payment. The response is {}", clientId, initiateResponseDTO.getNarrative());
                paymentsService.submitPayment(paymentId);
                redirectAttributes.addFlashAttribute("message", "You successfully submitted payment with reference " + payment.getEndToEndId());
            } else {
                log.info("The client id {} is not allowed to initiate the payment. The response is {}", clientId, initiateResponseDTO.getNarrative());
                redirectAttributes.addFlashAttribute("error", "You are not authorised to initiate payment this account.");
            }
            return "redirect:/payments/view/" + paymentId;
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("error", "Failed to get payment.");
            return "redirect:/payments/view/" + paymentId;
        });
    }

    @GetMapping("/delete/{paymentId}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_PAYMENT')")
    public String deletePayment(@PathVariable("paymentId") String paymentId, Model model, RedirectAttributes redirectAttributes) {
        paymentsService.deletePayment(paymentId);
        redirectAttributes.addFlashAttribute("error", "Payment with deleted.");
        return "redirect:/payments/view/" + paymentId;
    }

    @GetMapping("/initiate/{paymentId}")
    @PreAuthorize("hasAuthority('ROLE_INITIATE_PAYMENT')")
    public String initiatePayment(@PathVariable("paymentId") String paymentId, Model model, RedirectAttributes redirectAttributes) {
        ServiceResponse serviceResponse = paymentProcessService.initiatePayment(paymentId);
        redirectAttributes.addFlashAttribute("message", "Payment Initiation Response [" + serviceResponse.getResponseCode() + "] :" + serviceResponse.getNarrative());
        return "redirect:/payments/view/" + paymentId;
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/request")
    @PreAuthorize("hasAuthority('ROLE_INITIALISE_PAYMENT_REQUEST')")
    public String paymentRequest(@ModelAttribute("requestInfo") RequestInfo requestInfo, RedirectAttributes redirectAttributes) {
        log.info("The request info DTO is: {}", requestInfo);
        Optional<Payment> optionalPayment = paymentsService.getPaymentById(requestInfo.getId());
        return optionalPayment.map(payment -> {
            if (payment.getEntityStatus().equals(EntityStatus.DRAFT)) {
                redirectAttributes.addFlashAttribute("message", "Payment with reference  " + payment.getEndToEndId() + " and status " + payment.getEntityStatus().name() + " can not be investigated.");
                return "redirect:/payments/view/" + requestInfo.getId();
            }
            PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.aggregatePaymentReq(requestInfo);
            log.info("Created investigation case {}", paymentInvestigationCase);

            redirectAttributes.addFlashAttribute("message", "Payment request submitted successfully.");
            return "redirect:/payments/view/" + requestInfo.getId();
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("error", "Payment not found.");
            return "redirect:/payments/view/" + requestInfo.getId();
        });
    }

    @GetMapping("/to-approve")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_PAYMENT')")
    public String getPaymentsToApprove(Model model, HttpSession session) {
        String clientId = (String) session.getAttribute("loginAsId");
        log.info("ClientId... " + clientId);
        List<Payment> payments = paymentsService.getPaymentsByEntityStatus(EntityStatus.PENDING_APPROVAL, clientId);
        log.info("Payments found.... " + payments.size());

        model.addAttribute("payments", payments);
        model.addAttribute("title", "Approve Payments");

        return "payments/approve_payments";
    }

    @GetMapping("/multiple-approve/{paymentId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_PAYMENT')")
    public String approvePaymentFromMultipleApprove(@PathVariable("paymentId") String paymentId, Model model, RedirectAttributes redirectAttributes, HttpSession session) {
        String clientId = (String) session.getAttribute("clientId");
        Optional<Payment> optionalPayment = paymentsService.getPaymentById(paymentId);
        return optionalPayment.map(payment -> {
            String cashAccountId = Optional.ofNullable(payment.getAccount()).map(CashAccount::getId).orElse("");
            ApprovalRequestDTO approvalRequestDTO = ApprovalRequestDTO.builder()
                    .personId(clientId)
                    .paymentId(paymentId)
                    .cashAccountId(cashAccountId).build();
            ApprovalResponseDTO responseDTO = approvalProcessorService.approveReq(approvalRequestDTO);
            if (responseDTO.isDone()) {
                paymentsService.approvePayment(paymentId);
                redirectAttributes.addFlashAttribute("message", "Payment with reference " + payment.getEndToEndId() + " approved.");
            } else {
                redirectAttributes.addFlashAttribute("message", responseDTO.getNarrative());
            }
            return "redirect:/payments/to-approve/";
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("error", "Oops! Payment can't be found.");
            return "redirect:/payments/to-approve/";
        });
    }

    @GetMapping("/multiple-reject/{paymentId}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_PAYMENT')")
    public String rejectPaymentFromMultipleApprove(@PathVariable("paymentId") String paymentId, Model model, RedirectAttributes redirectAttributes) {
        Optional<Payment> optionalPayment = paymentsService.rejectPayment(paymentId);
        Payment payment = null;

        if (optionalPayment.isPresent()) {
            payment = optionalPayment.get();
        }
        redirectAttributes.addFlashAttribute("message", "Payment with reference " + payment.getEndToEndId() + " rejected.");
        return "redirect:/payments/to-approve/";
    }
}
