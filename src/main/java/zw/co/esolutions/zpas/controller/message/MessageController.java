package zw.co.esolutions.zpas.controller.message;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.message.*;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.services.iface.message.MessageService;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.storage.StorageService;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private StorageService storageService;


    @GetMapping("/create/bulk/sms")
    @PreAuthorize("hasAuthority('ROLE_CREATE_MESSAGE')")
    public String createMessagePage(Model model) {
        model.addAttribute("bulkMessageDTO", new BulkMessageRequestDTO());
        return "message/create_message";
    }

    @PostMapping("/create/bulk/sms")
    @PreAuthorize("hasAuthority('ROLE_CREATE_MESSAGE')")
    public String createMessage(@Valid BulkMessageRequestDTO messageRequestDTO, @RequestParam("bulkFile") MultipartFile bulkFile, Model model, Errors errors, RedirectAttributes redirectAttributes){
        List<String> recepients = new ArrayList<>();
        List<SingleMessageRequestDTO> dtos = new ArrayList<>();
        BulkMessageRequestDTO requestDTO = new BulkMessageRequestDTO();
        if (errors.hasErrors()) {
            return "message/create_message";
        }
        messageRequestDTO.setBatchNumber("B".concat(RandomStringUtils.randomNumeric(12)));
        messageRequestDTO.setMessageChannel(MessageChannel.SMS);

        if(!bulkFile.isEmpty()){
            byte[] bytes = new byte[0];
            try {
                bytes = bulkFile.getBytes();
                ByteArrayInputStream inputFilestream = new ByteArrayInputStream(bytes);
                BufferedReader br = new BufferedReader(new InputStreamReader(inputFilestream));
                String line = "";
                while ((line = br.readLine()) != null) {
                    recepients.add(line.split(",")[0]);
                }
                br.close();

                recepients.stream().filter(number -> number.matches("-?\\d+(\\.\\d+)?")).forEach(validNumber -> {
                    dtos.add(new SingleMessageRequestDTO(messageRequestDTO.getOriginator(), validNumber, messageRequestDTO.getMessageText(), RandomStringUtils.randomAlphanumeric(8)));
                });

                messageRequestDTO.setMessages(dtos);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            redirectAttributes.addFlashAttribute("message","Select a Bulk file or Provide Mobile Numbers");
            return "message/create_message";
        }

        final BulkMessageResponseDTO messageResponseDTO = this.messageService.sendBulk(messageRequestDTO);

        //Put to allow re-rendering
        model.addAttribute("messageResponseDTO", messageResponseDTO);

        redirectAttributes.addFlashAttribute("message","Your bulk message file "+bulkFile.getOriginalFilename()+" was successfully uploaded ");
        return "message/create_message";
    }

    @GetMapping("/create/email")
    @PreAuthorize("hasAuthority('ROLE_CREATE_EMAIL')")
    public String createEmailPage(Model model) {
        model.addAttribute("messageDTO", MessageRequestDTO.builder().build());
        return "message/create_email";
    }

    @PostMapping("/create/email")
    @PreAuthorize("hasAuthority('ROLE_CREATE_EMAIL')")
    public String createEmail(@Valid MessageRequestDTO messageRequestDTO, Model model, Errors errors){
        if (errors.hasErrors()) {
            return "message/create_email";
        }
        messageRequestDTO.setMessageChannel(MessageChannel.EMAIL);
        messageRequestDTO.setSubject("PLACEHOLDER SUBJECT");
        final MessageResponseDTO send = this.messageService.send(messageRequestDTO, "");
        //Put to allow re-rendering
        model.addAttribute("messageDTO", MessageRequestDTO.builder().build());
        return "message/create_email";
    }

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("ONE", "TWO", "THREE");
        final Optional<String> first = strings.stream().filter(s -> s.length() == 5).findFirst();
        first.ifPresent(s -> System.out.println(s));
    }
}


