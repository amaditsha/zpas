package zw.co.esolutions.zpas.controller.merchant;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.dto.merchant.PartyMerchantAccountDTO;
import zw.co.esolutions.zpas.model.PartyMerchantAccount;
import zw.co.esolutions.zpas.services.iface.merchant.MerchantService;
import zw.co.esolutions.zpas.services.iface.merchant.PartyMerchantAccountService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 29 Mar 2019
 */


@Controller
@Slf4j
@RequestMapping("/party-merchant-accounts")
public class PartyMerchantAccountController {

    @Autowired
    ClientService clientService;

    @Autowired
    PartyMerchantAccountService partyMerchantAccountService;

    @Autowired
    MerchantService merchantService;

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_MERCHANT')")
    public String createPartyMerchantAccountPage(Model model, HttpSession session) {
        model.addAttribute("title", "Create Party Merchant Account");
        return "merchant/create_party_merchant_account";
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_MERCHANT')")
    @ResponseBody
    public PartyMerchantAccount createPartyMerchantAccount(@RequestBody PartyMerchantAccountDTO partyMerchantAccount, Model model, BindingResult bindingResult, HttpSession session) {
        log.info("Entering Party Merchant Account Controller for Create");

        model.addAttribute("title", "Create Party Merchant Account");
        if (bindingResult.hasErrors()) {
            return null;
        }

        String clientId = (String) session.getAttribute("clientId");
        partyMerchantAccount.setClientId(clientId);

        final PartyMerchantAccount partyMerchantAccountCreated = partyMerchantAccountService.createMerchant(partyMerchantAccount);
        log.info("PartyMerchantAccount ID is {}", partyMerchantAccountCreated.getId());

        return partyMerchantAccountCreated;
    }

    @GetMapping("/view/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_MERCHANT')")
    public String viewPartyMerchantAccount(Model model, @PathVariable(name = "id") String id) {
        final Optional<PartyMerchantAccount> partyMerchantAccountOptional = partyMerchantAccountService.getPartyMerchantAccount(id);

        model.addAttribute("title", "View Party Merchant Account");
        model.addAttribute("partyMerchantAccount", partyMerchantAccountOptional.get());

        return "merchant/view_party_merchant_account";
    }

    @GetMapping("/manage")
    @PreAuthorize("hasAuthority('ROLE_VIEW_MERCHANT')")
    public String managePartyMerchantAccounts(Model model) {
        List<PartyMerchantAccount> merchantAccountList = partyMerchantAccountService.getPartyMerchantAccounts();
        model.addAttribute("merchantAccounts", merchantAccountList);
        return "merchant/manage_party_merchant_accounts";
    }

    @GetMapping("/get/{partyMerchantAccountId}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_MERCHANT')")
    @ResponseBody
    public PartyMerchantAccountDTO managePartyMerchantAccounts(@PathVariable("partyMerchantAccountId") String partyMerchantAccountId) {
        return partyMerchantAccountService.getPartyMerchantAccount(partyMerchantAccountId)
                .map(partyMerchantAccount -> PartyMerchantAccountDTO.builder()
                        .accountNumber(partyMerchantAccount.getAccountNumber())
                        .identification(partyMerchantAccount.getIdentification())
                        .merchantId(partyMerchantAccount.getMerchant().getId())
                        .linkedPartyId(partyMerchantAccount.getLinkedParty().getId())
                        .clientId("")
                        .build())
                .orElse(PartyMerchantAccountDTO.builder()
                        .accountNumber("")
                        .identification("")
                        .merchantId("")
                        .linkedPartyId("")
                        .clientId("")
                        .build());
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_MERCHANT')")
    public ResponseEntity<?> deletePartyMerchantAccountViaAjax(@PathVariable("id") String id) {
        PartyMerchantAccount merchantAccount = partyMerchantAccountService.deletePartyMerchantAccount(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (merchantAccount.getEntityStatus().equals(EntityStatus.DELETED)) {
            result.setNarrative("Successfully deleted.");
            result.setResponseCode("00");
            result.setObject(merchantAccount.getEntityStatus().name());
        } else {
            result.setNarrative("The merchant account can't be deleted");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/reject/{id}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_MERCHANT')")
    public ResponseEntity<?> rejectPartyMerchantAccountViaAjax(@PathVariable("id") String id) {
        PartyMerchantAccount merchantAccount = partyMerchantAccountService.rejectPartyMerchantAccount(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (merchantAccount.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            result.setNarrative("Successfully rejected.");
            result.setResponseCode("00");
            result.setObject(merchantAccount.getEntityStatus().name());
        } else {
            result.setNarrative("The merchant account can't be rejected");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/approve/{id}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_MERCHANT')")
    public ResponseEntity<?> approvePartyMerchantAccountViaAjax(@PathVariable("id") String id) {
        PartyMerchantAccount merchantAccount = partyMerchantAccountService.approvePartyMerchantAccount(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (merchantAccount.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(merchantAccount.getEntityStatus().name());
        } else {
            result.setNarrative("The merchant Account can't be approved");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @PostMapping("/edit")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_MERCHANT')")
    public ResponseEntity<?> editPartyMerchantAccount(@ModelAttribute("beneficiary") PartyMerchantAccountDTO formPartyMerchantAccount, Model model, Errors errors, HttpSession session) {

        log.info("Inside PartyMerchantAccount Edit Controller : {}", formPartyMerchantAccount.getId());
        final Optional<PartyMerchantAccount> merchantAccountOptional = partyMerchantAccountService.getPartyMerchantAccount(formPartyMerchantAccount.getId());

        log.info("Found PartyMerchantAccount? : {} ", merchantAccountOptional.isPresent());

        AjaxResponseBody result = new AjaxResponseBody();

        merchantAccountOptional.ifPresent(merchantAccount -> {

            //add the details to be edited
            merchantAccount.setIdentification(formPartyMerchantAccount.getIdentification());
            merchantAccount.setAccountNumber(formPartyMerchantAccount.getAccountNumber());


            log.info("Account Number before updating: {} ", merchantAccount.getAccountNumber());

            PartyMerchantAccount updatedPartyMerchantAccount = partyMerchantAccountService.updatePartyMerchantAccount(formPartyMerchantAccount);
            log.info("Account Number after updating: {} ", updatedPartyMerchantAccount.getAccountNumber());

            if (updatedPartyMerchantAccount.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                PartyMerchantAccountController.PartyMerchantAccountTemp merchantAccountTemp = new PartyMerchantAccountController.PartyMerchantAccountTemp();
                merchantAccountTemp.setId(updatedPartyMerchantAccount.getId());
                merchantAccountTemp.setIdentification(updatedPartyMerchantAccount.getIdentification());
                merchantAccountTemp.setAccountNumber(updatedPartyMerchantAccount.getAccountNumber());
                merchantAccountTemp.setEntityStatus(updatedPartyMerchantAccount.getEntityStatus());

                result.setObject(merchantAccountTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The merchant account can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);

    }

    @Data
    public class PartyMerchantAccountTemp {
        protected String id;
        protected String identification;
        protected String accountNumber;
        protected EntityStatus entityStatus;

    }

}
