package zw.co.esolutions.zpas.controller.nssa;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.agreement.ApprovalResponseDTO;
import zw.co.esolutions.zpas.dto.nssa.AccountDTO;
import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleDTO;
import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleItemDTO;
import zw.co.esolutions.zpas.dto.nssa.MakeNssaPaymentDTO;
import zw.co.esolutions.zpas.enums.PaymentInstrumentCode;
import zw.co.esolutions.zpas.model.Account;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountMandateService;
import zw.co.esolutions.zpas.services.iface.signature.ApprovalProcessorService;
import zw.co.esolutions.zpas.services.nssa.iface.ContributionsService;
import zw.co.esolutions.zpas.utilities.storage.StorageService;

import javax.servlet.http.HttpSession;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 26 Feb 2019
 */
@Slf4j
@Controller
@Scope("session")
@RequestMapping("/nssa/payments")
public class ContributionsController {

    @Value("${NSSA.CONTRIBUTIONS.SYSTEM.BASE.URL}")
    private String nssaContributionsSystemBaseUrl;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private ContributionsService contributionsService;

    @Autowired
    private ApprovalProcessorService approvalProcessorService;

    @Autowired
    private CashAccountMandateService cashAccountMandateService;


    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_MAKE_NSSA_PAYMENT')")
    public String makeNssaPaymentPage(Model model, HttpSession session,
                                      @RequestParam(value = "accountId", required = false) String accountId) {
        String clientId = (String) session.getAttribute("loginAsId");

        final List<Account> clientAccounts = accountService.getAccountsByClientId(clientId);
        final List<PaymentInstrumentCode> paymentInstrumentCodes = Arrays.asList(PaymentInstrumentCode.CustomerCreditTransfer);

        if (!StringUtils.isEmpty(accountId)) {
            Optional.ofNullable(accountService.findAccountById(accountId)).ifPresent(account -> {
                model.addAttribute("account", account);
            });
        }

        model.addAttribute("title", "Create NSSA Contribution Schedule");
        model.addAttribute("clientAccounts", clientAccounts);
        return "nssa/capture_contribution_schedule";
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_MAKE_NSSA_PAYMENT')")
    public String uploadNSSGetAPayment(@RequestParam("scheduleFile") MultipartFile scheduleFile,
                                       @RequestParam("clientReference") String clientReference,
                                       @RequestParam("accountId") String accountId,
                                       @RequestParam("month") String month,
                                       @RequestParam("apwcs") double apwcs,
                                       RedirectAttributes redirectAttributes, HttpSession session) {
        if (scheduleFile == null) {
            redirectAttributes.addFlashAttribute("error", "Tried to upload an empty file, not allowed.");
            return "redirect:/nssa/payments";
        }
        try {
            storageService.store(scheduleFile);
        } catch (RuntimeException re) {
            redirectAttributes.addFlashAttribute("error", "An error occurred trying to save file. " + re.getMessage());
            return "redirect:/nssa/payments";
        }

        String loginAsId = (String) session.getAttribute("loginAsId");

        if (StringUtils.isEmpty(loginAsId)) {
            redirectAttributes.addFlashAttribute("error", "You are not in a capacity to make a NSSA payment. Sign in as an" +
                    " employee of the organisation you work for, then try again");
            return "redirect:/nssa/payments";
        }

        String clientId = (String) session.getAttribute("clientId");

        ApprovalResponseDTO initiateResponseDTO = cashAccountMandateService.getPaymentInitiators(accountId, clientId);
        if (initiateResponseDTO.isDone()) {
            final MakeNssaPaymentDTO makeNssaPaymentDTO = MakeNssaPaymentDTO.builder()
                    .fileName(scheduleFile.getOriginalFilename())
                    .sourceAccountId(accountId)
                    .month(month)
                    .clientId(loginAsId)
                    .clientReference(clientReference)
                    .apwcs(apwcs)
                    .build();
            log.info("NSSA Payment Upload DTO is: {}", makeNssaPaymentDTO);

            try {
                return contributionsService.createContributionSchedule(makeNssaPaymentDTO).map(contributionSchedule -> {
                    redirectAttributes.addFlashAttribute("message",
                            "You successfully uploaded " + scheduleFile.getOriginalFilename());
                    return "redirect:/nssa/payments/view/" + contributionSchedule.getId();
                }).orElseThrow(() -> {
                    throw new RuntimeException("No payment returned.");
                });
            } catch (Throwable e) {
                e.printStackTrace();
                redirectAttributes.addFlashAttribute("error", "An error occurred. " + e.getMessage());
                return "redirect:/nssa/payments";
            }
        } else {
            log.info("The client id {} is not allowed to initiate the payment. The response is {}", clientId, initiateResponseDTO.getNarrative());
            redirectAttributes.addFlashAttribute("error", "You are not authorised to initiate payment for this account.");
            return "redirect:/nssa/payments";
        }
    }

    @GetMapping("/view/{contributionScheduleId}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PAYMENT_CONTRIBUTION_SCHEDULE')")
    public String viewContributionSchedule(@PathVariable("contributionScheduleId") String contributionScheduleId,
                                           @RequestParam("page") Optional<Integer> page,
                                           @RequestParam("size") Optional<Integer> size, Model model) {
        return contributionsService.getContributionScheduleById(contributionScheduleId)
                .map(contributionSchedule -> viewContributionSchedulePageHelper(contributionSchedule, model))
                .orElseGet(() -> viewContributionSchedulePageHelper(null, model));
    }

    private String viewContributionSchedulePageHelper(ContributionScheduleDTO contributionSchedule, Model model) {
        if (contributionSchedule != null) {
            log.info("Contribution schedule for: {}", contributionSchedule.getMonth());
            model.addAttribute("contributionSchedule", contributionSchedule);
            model.addAttribute("nssaContributionsSystemBaseUrl", nssaContributionsSystemBaseUrl);
        }
        model.addAttribute("title", "Contribution Schedule");
        return "nssa/view_contribution_schedule";
    }

    @GetMapping("/view/item/{contributionScheduleItemId}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PAYMENT_CONTRIBUTION_SCHEDULE_ITEM')")
    public String viewContributionScheduleItem(@PathVariable("contributionScheduleItemId") String contributionScheduleItemId,
                                           @RequestParam("page") Optional<Integer> page,
                                           @RequestParam("size") Optional<Integer> size, Model model) {
        return contributionsService.getContributionScheduleItem(contributionScheduleItemId)
                .map(contributionScheduleItem -> viewContributionScheduleItemPageHelper(contributionScheduleItem, model))
                .orElseGet(() -> viewContributionSchedulePageHelper(null, model));
    }

    private String viewContributionScheduleItemPageHelper(ContributionScheduleItemDTO contributionScheduleItem, Model model) {
        if (contributionScheduleItem != null) {
            log.info("Contribution schedule item is: {}", contributionScheduleItem);
            model.addAttribute("contributionScheduleItem", contributionScheduleItem);
        } else {
            log.info("Contribution schedule item not found.");
        }
        model.addAttribute("title", "Contribution Schedule Item");
        return "nssa/view_contribution_schedule_item";
    }

    @GetMapping("/approve/{contributionScheduleId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CONTRIBUTION_SCHEDULE')")
    public String approveContributionSchedule(@PathVariable("contributionScheduleId") String contributionScheduleId, Model model, RedirectAttributes redirectAttributes, HttpSession session) {
        String clientId = (String) session.getAttribute("clientId");
        String loginAsId = (String) session.getAttribute("loginAsId");
        log.info("Client ID is: {}", clientId);
        log.info("Login As ID is: {}", loginAsId);
        Optional<ContributionScheduleDTO> optionalContributionSchedule = contributionsService.getContributionScheduleById(contributionScheduleId);
        return optionalContributionSchedule.map(contributionSchedule -> {
            String cashAccountId = Optional.ofNullable(contributionSchedule.getAccount()).map(AccountDTO::getId).orElse("");
//            ApprovalRequestDTO approvalRequestDTO = ApprovalRequestDTO.builder()
//                    .personId(clientId)
//                    .paymentId(contributionSchedule.getPaymentId())
//                    .cashAccountId(cashAccountId).build();
//            ApprovalResponseDTO responseDTO = approvalProcessorService.approveReq(approvalRequestDTO);
//            if (responseDTO.isDone()) {
            contributionsService.approveContributionSchedule(contributionScheduleId).map(contributionScheduleDTO -> {
                redirectAttributes.addFlashAttribute("message", "Contribution Schedule for " + contributionSchedule.getMonth().format(DateTimeFormatter.ofPattern("MMMM yyyy")) + " approved.");
                return contributionScheduleDTO;
            }).orElseGet(() -> {
                redirectAttributes.addFlashAttribute("error", "Contribution Schedule for " + contributionSchedule.getMonth().format(DateTimeFormatter.ofPattern("MMMM yyyy")) + " approval failed.");
                return null;
            });
//            } else {
//                redirectAttributes.addFlashAttribute("message", responseDTO.getNarrative());
//            }
            return "redirect:/nssa/payments/view/" + contributionScheduleId;
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("error", "Oops! Contribution Schedule can't be found.");
            return "redirect:/nssa/payments/view/" + contributionScheduleId;
        });
    }

    @GetMapping("/reject/{contributionScheduleId}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_CONTRIBUTION_SCHEDULE')")
    public String rejectContributionSchedule(@PathVariable("contributionScheduleId") String contributionScheduleId, Model model, RedirectAttributes redirectAttributes) {
        contributionsService.rejectContributionSchedule(contributionScheduleId);
        redirectAttributes.addFlashAttribute("message", "Contribution Schedule with rejected.");
        return "redirect:/nssa/payments/view/" + contributionScheduleId;
    }

    @GetMapping("/history/all")
    @PreAuthorize("hasAuthority('ROLE_VIEW_CONTRIBUTION_SCHEDULES')")
    public String viewContributionSchedules(Model model, HttpSession session) {
        String clientId = (String) session.getAttribute("loginAsId");
        final List<ContributionScheduleDTO> contributionSchedules = contributionsService.getContributionSchedulesByClientId(clientId);
        model.addAttribute("contributionSchedules", contributionSchedules);
        return "nssa/view_contribution_schedules";
    }

    @GetMapping("/submit/{contributionScheduleId}")
    public String submitContributionSchedule(@PathVariable("contributionScheduleId") String contributionScheduleId, RedirectAttributes redirectAttributes, HttpSession httpSession) {
        String clientId = (String) httpSession.getAttribute("clientId");
        Optional<ContributionScheduleDTO> optionalContributionSchedule = contributionsService.getContributionScheduleById(contributionScheduleId);
        return optionalContributionSchedule.map(contributionSchedule -> {
//            String cashAccountId = Optional.ofNullable(contributionSchedule.getAccount()).map(AccountDTO::getId).orElse("");
//            ApprovalResponseDTO initiateResponseDTO = cashAccountMandateService.getPaymentInitiators(cashAccountId, clientId);

//            if (initiateResponseDTO.isDone()) {
//                log.info("The client id {} is allowed to initiate the contributionSchedule. The response is {}", clientId, initiateResponseDTO.getNarrative());
            contributionsService.submitContributionSchedule(contributionScheduleId).map(contributionScheduleDTO -> {
                redirectAttributes.addFlashAttribute("message", "You successfully submitted contribution schedule for month " + contributionSchedule.getMonth().format(DateTimeFormatter.ofPattern("MMMM yyyy")));
                return contributionScheduleDTO;
            }).orElseGet(() -> {
                redirectAttributes.addFlashAttribute("message", "Failed to submit contribution schedule for month " + contributionSchedule.getMonth().format(DateTimeFormatter.ofPattern("MMMM yyyy")));
                return null;
            });
//            } else {
//                log.info("The client id {} is not allowed to initiate the contribution schedule. The response is {}", clientId, initiateResponseDTO.getNarrative());
//                redirectAttributes.addFlashAttribute("error", "You are not authorised to initiate contribution schedule payment on this account.");
//            }
            return "redirect:/nssa/payments/view/" + contributionScheduleId;
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("error", "Failed to get contributionSchedule.");
            return "redirect:/nssa/payments/view/" + contributionScheduleId;
        });
    }

    @GetMapping("/delete/{contributionScheduleId}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_CONTRIBUTION_SCHEDULE')")
    public String deleteContributionSchedule(@PathVariable("contributionScheduleId") String contributionScheduleId, Model model, RedirectAttributes redirectAttributes) {
        contributionsService.deleteContributionSchedule(contributionScheduleId);
        redirectAttributes.addFlashAttribute("error", "Contribution Schedule with deleted.");
        return "redirect:/nssa/payments/view/" + contributionScheduleId;
    }
}
