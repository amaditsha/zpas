package zw.co.esolutions.zpas.controller.currency;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.currency.CurrencyDTO;
import zw.co.esolutions.zpas.model.Currency;
import zw.co.esolutions.zpas.services.iface.currency.CurrencyService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/currency")
public class CurrencyController {

    @Autowired
    private CurrencyService currencyService;

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CURRENCY')")
    public String createCurrencyPage(Model model) {
        model.addAttribute("currencyDTO", new CurrencyDTO());
        return "currency/create_currency";
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CURRENCY')")
    public String createCurrency(@Valid CurrencyDTO currency, Errors errors){
        if (errors.hasErrors()) {
            return "currency/create_currency";
        }
        //currency.setId(GenerateKey.generateEntityId());
        final Currency currencyCreated = currencyService.createCurrency(currency);

        return "redirect:/currency/details/"+currencyCreated.getId();
    }

    @GetMapping("/details/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_CURRENCY')")
    public String viewCurrency(Model model, @PathVariable(name = "id") String id) {
        final Optional<Currency> currencyRetrieved = currencyService.findCurrencyById(id);
        log.debug("Currency Retrieved : {}, {}", id, currencyRetrieved.isPresent());

        if(currencyRetrieved.isPresent()){
            model.addAttribute("currency", currencyRetrieved.get());
            return "currency/view_currency";
        }else{
            return "currency/view_currency";
        }
    }

    @GetMapping("/manage")
    @PreAuthorize("hasAuthority('ROLE_VIEW_CURRENCY')")
    public String manageCurrencies(Model model) {
        List<Currency> currencyList = currencyService.getAllCurrencies();
        model.addAttribute("currencies", currencyList);
        return "currency/manage_currencies";
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CURRENCY')")
    public String showCurrencyEditPage(Model model, @PathVariable(name = "id") String id) {
        final Optional<Currency> currency = currencyService.findCurrencyById(id);
        model.addAttribute("currency", currency.get());
        model.addAttribute("statuses", EntityStatus.values());
        return "currency/edit_currency";
    }

    @PostMapping("/update/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CURRENCY')")
    public String editCurrency(@Valid CurrencyDTO currencyDTO, @PathVariable("id") String id,
                                  Model model, Errors errors) {
        log.info("currencyId: " + currencyDTO.getId());
        log.info("defaultCurrency: " + currencyDTO.getDefaultCurrency());
        Currency currency = this.currencyService.updateCurrency(currencyDTO);
        model.addAttribute("currency", currency);
        return "redirect:currency/view_currency";
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_CURRENCY')")
    public String deleteCurrency(@PathVariable("id") String id, RedirectAttributes redirectAttributes){
        log.info("Delete the currency");
        Currency currency = currencyService.deleteCurrency(id);
        redirectAttributes.addFlashAttribute("message", "Currency with code " + currency.getCode() + " was deleted.");
        return "redirect:/currency/details/" + currency.getId();
    }

    @ResponseBody
    @GetMapping("/currencies/refresh")
//    @PreAuthorize("hasAuthority('ROLE_MANAGE_CURRENCY')")
    public String refreshCurrencies(RedirectAttributes redirectAttributes) {

        log.info("Refreshing currencies...");
        final List<Currency> currencies = currencyService.refreshCurrencies();
        if(currencies.size() > 0) {
            redirectAttributes.addAttribute("message", currencies.size() + " new currency found.");
        } else {
            redirectAttributes.addAttribute("message", "No currencies found.");
        }
        return "Done";
    }
}
