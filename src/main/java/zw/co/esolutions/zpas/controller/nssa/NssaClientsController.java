package zw.co.esolutions.zpas.controller.nssa;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.nssa.EmployeeDTO;
import zw.co.esolutions.zpas.dto.nssa.EmployeeDependentDTO;
import zw.co.esolutions.zpas.dto.nssa.UploadEmployeesDTO;
import zw.co.esolutions.zpas.enums.nssa.*;
import zw.co.esolutions.zpas.services.nssa.iface.EmployeeDependentService;
import zw.co.esolutions.zpas.services.nssa.iface.NssaClientsService;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.storage.StorageService;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/nssa-clients")
public class NssaClientsController {

    @Autowired
    EmployeeDependentService employeeDependentService;

    @Autowired
    NssaClientsService nssaClientsService;

    @Autowired
    StorageService storageService;

    @GetMapping("/create-page")
    //@PreAuthorize("hasAuthority('ROLE_CREATE_CLIENT')")
    public String createEmployeePage(Model model){
        model.addAttribute("titles", Title.values());
        model.addAttribute("genders", Gender.values());
        model.addAttribute("maritalStatuses", MaritalStatus.values());
        model.addAttribute("naturesOfEmployment", NatureOfEmployment.values());
        model.addAttribute("statusesOfEmployment", StatusOfEmployment.values());
        return "nssa/create_employee";
    }

    @GetMapping("/details/{id}")
    //@PreAuthorize("hasAuthority('ROLE_VIEW_CLIENT')")
    public String viewEmployeeById(Model model, @PathVariable(name = "id") String id) {
        model.addAttribute("dependentTypes", DependentType.values());
        model.addAttribute("titles", Title.values());
        model.addAttribute("genders", Gender.values());
        model.addAttribute("maritalStatuses", MaritalStatus.values());
        model.addAttribute("naturesOfEmployment", NatureOfEmployment.values());
        model.addAttribute("statusesOfEmployment", StatusOfEmployment.values());

        final List<EmployeeDependentDTO> employeeDependents = employeeDependentService.getEmployeeDependentByEmployeeId(id);
        employeeDependents.forEach(employeeDependentDTO -> {
            log.info("Dependent is: {}", employeeDependentDTO);
        });
        log.info("Found {} employee dependents.", employeeDependents.size());
        model.addAttribute("employeeDependents", employeeDependents);

        final Optional<EmployeeDTO> employeeOptional = nssaClientsService.getEmployeeById(id);
        return employeeOptional.map(employee -> {
            log.info("employee is {}", employee);
            model.addAttribute("employee", employee);
            model.addAttribute("title", "View EmployeeDTO");
            return "nssa/view_employee";
        }).orElseGet(() -> {
            model.addAttribute("employee", null);
            return "nssa/view_employee";
        });
    }

    @GetMapping("/upload-employee-file")
    //@PreAuthorize("hasAuthority('ROLE_CREATE_CLIENT')")
    public String createEmployeeFile(Model model, HttpSession session){
        log.info("creating employee file");
        model.addAttribute("title", "Upload employees file");
        return "nssa/upload_employee_file";
    }

    @PostMapping("/upload/employees")
    //@PreAuthorize("hasAuthority('ROLE_MAKE_NSSA_PAYMENT')")
    public String uploadEmployees(@RequestParam("employeesFile") MultipartFile employeesFile,
                                  RedirectAttributes redirectAttributes, HttpSession session) {

        String clientId = (String) session.getAttribute("loginAsId");

        log.info("uploading nssa employee file");

        if(employeesFile == null) {
            redirectAttributes.addFlashAttribute("error", "Tried to upload an empty file, not allowed.");
            return "redirect:/nssa-clients/upload-employee-file";
        }
        try {
            storageService.store(employeesFile);
        } catch (RuntimeException re) {
            redirectAttributes.addFlashAttribute("error", "An error occurred trying to save file. " + re.getMessage());
            return "redirect:/nssa-clients/upload-employee-file";
        }

        final UploadEmployeesDTO uploadEmployeesDTO = UploadEmployeesDTO.builder()
                .fileName(employeesFile.getOriginalFilename())
                .clientId(clientId)
                .build();
        log.info("Employees Upload DTO is: {}", uploadEmployeesDTO);

        try {
            final List<EmployeeDTO> employees = nssaClientsService.createEmployees(uploadEmployeesDTO);
            return "redirect:/nssa-clients/view/all/employees";

        } catch (Exception e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("error", "An error occurred. " + e.getMessage());
            return "redirect:/nssa-clients/upload-employee-file";
        }
    }

    @GetMapping("/download/{fileName}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        final Path fileLocation = Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName);
        // Load file as Resource
        Resource resource = storageService.loadAsResource(fileLocation.toString());

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/pdf";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }


    @PostMapping("/employees")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CLIENT')")
    public String createEmployee(@Valid EmployeeDTO employeeDTO, BindingResult bindingResult, Model model, HttpSession session){
        String clientId = (String) session.getAttribute("loginAsId");
        employeeDTO.setClientId(clientId);
        log.info("Employee DTO is: {}", employeeDTO);
        model.addAttribute("title", "Employees");
        if (bindingResult.hasErrors()) {
            return "nssa/create_employee";
        }
        final Optional<EmployeeDTO> employeeCreatedDTOOptional = nssaClientsService.createEmployee(employeeDTO);
        return viewEmployeeHelper(model, employeeCreatedDTOOptional);
    }

    @RequestMapping(value = "/view/all/employees", method = RequestMethod.GET)
    public String viewAllEmployees(Model model, HttpSession session){

        String clientId = (String) session.getAttribute("loginAsId");
        final List<EmployeeDTO> employees = nssaClientsService.getEmployeesByEmployerId(clientId);
        log.info("Found {} employees.", employees.size());
        model.addAttribute("employees", employees);
        return "nssa/view_employees";
    }

    @GetMapping(value = "/view/employee/dependent/{id}")
    public String viewEmployeeDependentById(Model model, @PathVariable("id") String id){
        model.addAttribute("dependentTypes", DependentType.values());
        model.addAttribute("titles", Title.values());
        model.addAttribute("genders", Gender.values());
        final Optional<EmployeeDependentDTO> optionalEmployeeDependent = employeeDependentService.getEmployeeDependentById(id);
        return  optionalEmployeeDependent.map(employeeDependent -> {
            log.info("employee dependent is {}", employeeDependent);
            model.addAttribute("employeeDependent", employeeDependent);
            model.addAttribute("title", "EmployeeDTO Dependent Details");
            return "nssa/view_employee_dependent";
        }).orElseGet(() -> {
            model.addAttribute("employeeDependent", null);
            log.info("employee dependent not found {}");
            return "nssa/view_employee_dependent";
        });
    }

    @GetMapping("/approve/{id}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CLIENT')")
    public String approveEmployee(Model model, @PathVariable(name = "id") String id) {
        log.info("Approving employee: " + id);
        final Optional<EmployeeDTO> employeeDTOOptional = nssaClientsService.approveEmployee(id);
        return viewEmployeeHelper(model, employeeDTOOptional);
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CLIENT')")
    public String deleteEmployee(Model model, @PathVariable(name = "id") String id) {
        log.info("Delete employee: " + id);
        final Optional<EmployeeDTO> employeeDTOOptional = nssaClientsService.deleteEmployee(id);
        return viewEmployeeHelper(model, employeeDTOOptional);
    }

    @GetMapping("/reject/{id}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CLIENT')")
    public String rejectEmployee(Model model, @PathVariable(name = "id") String id) {
        log.info("Reject employee: " + id);
        final Optional<EmployeeDTO> employeeDTOOptional = nssaClientsService.rejectEmployee(id);
        return viewEmployeeHelper(model, employeeDTOOptional);
    }

    private String viewEmployeeHelper(Model model, Optional<EmployeeDTO> employeeDTOOptional) {
        return employeeDTOOptional.map(employeeDTO -> {
            model.addAttribute("employee", employeeDTO);
            return "redirect:/nssa-clients/details/" + employeeDTO.getId();
        }).orElseGet(() -> {
            model.addAttribute(RequestDispatcher.ERROR_STATUS_CODE, "404");
            return "redirect:/error";
        });
    }

    @PostMapping("/employee/dependents")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CLIENT')")
    public String createEmployeeDependent(@Valid EmployeeDependentDTO employeeDependentDTO, BindingResult bindingResult, Model model, HttpSession session){
        model.addAttribute("title", "EmployeeDTO Dependents");
        if (bindingResult.hasErrors()) {
            return "nssa/view_employee";
        }
        final Optional<EmployeeDependentDTO> employeeDependentDTOOptional = employeeDependentService.createEmployeeDependent(employeeDependentDTO);
        return employeeDependentDTOOptional.map(employeeDependentDTOCreated -> {
            log.info("The dependent is {}", employeeDependentDTOCreated);
            return "redirect:/nssa-clients/details/" + employeeDependentDTOCreated.getEmployeeid();
        }).orElseGet(() -> {
            model.addAttribute(RequestDispatcher.ERROR_STATUS_CODE, "404");
            return "redirect:/error";
        });
    }

    @GetMapping("/approve/employee-dependent/{employeeDependentId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CLIENT')")
    public String approveEmployeeDependent(Model model, @PathVariable(name = "employeeDependentId") String employeeDependentId) {
        log.info("Approving employee dependent: {}",  employeeDependentId);
        final Optional<EmployeeDependentDTO> employeeDependentDTOOptional = employeeDependentService.approveEmployeeDependent(employeeDependentId);
        return viewEmployeeDependentHelper(model, employeeDependentDTOOptional);
    }

    @GetMapping("/delete/employee-dependent/{employeeDependentId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CLIENT')")
    public String deleteEmployeeDependent(Model model, @PathVariable(name = "employeeDependentId") String employeeDependentId) {
        log.info("Delete employee dependent: {}",  employeeDependentId);
        final Optional<EmployeeDependentDTO> employeeDependentDTOOptional = employeeDependentService.deleteEmployeeDependent(employeeDependentId);
        return viewEmployeeDependentHelper(model, employeeDependentDTOOptional);
    }

    @GetMapping("/reject/employee-dependent/{employeeDependentId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CLIENT')")
    public String rejectEmployeeDependent(Model model, @PathVariable(name = "employeeDependentId") String employeeDependentId) {
        log.info("Reject employee dependent: {}", employeeDependentId);
        final Optional<EmployeeDependentDTO> employeeDependentDTOOptional = employeeDependentService.rejectEmployeeDependent(employeeDependentId);
        return viewEmployeeDependentHelper(model, employeeDependentDTOOptional);
    }

    private String viewEmployeeDependentHelper(Model model, Optional<EmployeeDependentDTO> employeeDependentDTOOptional) {
        return employeeDependentDTOOptional.map( employeeDependentDTO -> {
            model.addAttribute("employeeDependent", employeeDependentDTO);
            return "redirect:/nssa-clients/view/employee/dependent/" + employeeDependentDTO.getId();
        }).orElseGet(() -> {
            model.addAttribute(RequestDispatcher.ERROR_STATUS_CODE, "404");
            return "redirect:/error";
        });
    }

    @PostMapping("/employees/edit")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CLIENT')")
    public String updateEmployee(@Valid EmployeeDTO employeeDTO, BindingResult bindingResult, Model model, HttpSession session){
        String clientId = (String) session.getAttribute("loginAsId");
        employeeDTO.setClientId(clientId);
        log.info("reached update employee method in the zpas controller with clientId "+ clientId);
        model.addAttribute("titles", Title.values());
        model.addAttribute("genders", Gender.values());
        model.addAttribute("title", "Employees");
        if (bindingResult.hasErrors()) {
            return "nssa/view_employee";
        }
        final Optional<EmployeeDTO> employeeCreatedDTOOptional = nssaClientsService.updateEmployee(employeeDTO);
        return viewEmployeeHelper(model, employeeCreatedDTOOptional);
    }

    @PostMapping("/employee-dependents/edit")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CLIENT')")
    public String updateEmployeeDependent(@Valid EmployeeDependentDTO employeeDependentDTO, BindingResult bindingResult, Model model){
        model.addAttribute("dependentTypes", DependentType.values());
        model.addAttribute("titles", Title.values());
        model.addAttribute("genders", Gender.values());
        log.info("reached update employee dependent method in the zpas controller");
        model.addAttribute("title", "Employee Dependents");
        if (bindingResult.hasErrors()) {
            return "nssa/view_employee";
        }
        final Optional<EmployeeDependentDTO> employeeDependentCreatedDTOOptional = employeeDependentService.updateEmployeeDependent(employeeDependentDTO);
        return viewEmployeeDependentHelper(model, employeeDependentCreatedDTOOptional);
    }

    @ResponseBody
    @GetMapping("/validate/{ssn}")
    public Boolean  validateSSN(@PathVariable("ssn") String ssn) {
        final boolean ssnAlreadyInUse = nssaClientsService.isSSNAlreadyInUse(ssn);
        ValidateSSNResponse validateSSNResponse = new ValidateSSNResponse();
//        validateSSNResponse.setSsn(ssn);
        if(ssnAlreadyInUse) {
            validateSSNResponse.setMessage("There is already an employee with this ssn!");
            return true;
        } else {
            return false;
        }
    }

    @ResponseBody
    @GetMapping("/retrieve/employee-by-ssn/{ssn}")
    public ValidateSSNResponse  validateEmployeeSSN(@PathVariable("ssn") String ssn) {
        ValidateSSNResponse validateSSNResponse = new ValidateSSNResponse();
        validateSSNResponse.setSsn(ssn);
        nssaClientsService.getEmployeeBySSN(ssn).map(employeeDTO -> {
            try{
                validateSSNResponse.setFirstName(employeeDTO.getFirstName());
                validateSSNResponse.setLastName(employeeDTO.getLastName());
                validateSSNResponse.setEmployeeId(employeeDTO.getId());
                log.info("the employee id is {}", employeeDTO.getId());
            }catch (Exception e){
                log.info("name is null");
                validateSSNResponse.setSsn(null);

            }
            return employeeDTO;
        }).orElse(null);
        return validateSSNResponse;
    }

    @ResponseBody
    @GetMapping(value = "/validate/ssn-format/{ssn}")
    public boolean validateSsnFormat(@PathVariable String ssn){
        return nssaClientsService.validateSsnFormat(ssn);
    }

    @Data
    public class ValidateSSNResponse {
        protected String ssn;
        protected String message;
        protected String code;

        protected String firstName;
        protected String lastName;
        protected String fullName;
        protected String employeeId;
    }
}
