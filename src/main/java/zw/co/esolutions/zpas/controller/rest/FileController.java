package zw.co.esolutions.zpas.controller.rest;

/**
 * Created by alfred on 02 Apr 2019
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import zw.co.esolutions.zpas.dto.payments.PaymentUploadDTO;
import zw.co.esolutions.zpas.services.iface.payments.PaymentActivationRequestsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentUploadsService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.utilities.storage.StorageService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private StorageService storageService;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private PaymentUploadsService paymentUploadsService;

    @Autowired
    private PaymentActivationRequestsService paymentActivationRequestsService;

    @PostMapping("/api/payment-uploads/upload-file")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file,
                                         @RequestParam("purpose") String purpose,
                                         @RequestParam("paymentType") String paymentType,
                                         @RequestParam("paymentInstrument") String paymentInstrument,
                                         @RequestParam("priority") String priority,
                                         HttpSession session) {
        String fileName = storageService.storeFile(file);

        String clientId = (String) session.getAttribute("loginAsId");

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download-file/")
                .path(fileName)
                .toUriString();

        final PaymentUploadDTO paymentUploadDTO = PaymentUploadDTO.builder()
                .fileName(file.getOriginalFilename())
                .paymentType(paymentType)
                .paymentInstrument(paymentInstrument)
                .priority(priority)
                .clientId(clientId)
                .purpose(purpose)
                .build();

        try {
            paymentUploadsService.uploadBulkPayment(paymentUploadDTO)
                    .map(uploadedPayment -> paymentsService.submitPayment(uploadedPayment.getId())
                            .map(submittedPayment -> paymentsService.approvePayment(submittedPayment.getId())
                                    .map(sp -> Optional.ofNullable(sp))
                                    .orElse(Optional.empty()))
                            .orElse(Optional.empty()))
                    .orElse(Optional.empty());

            return new UploadFileResponse(fileName, fileDownloadUri,
                    file.getContentType(), file.getSize());
        } catch (PaymentRequestInvalidArgumentException e) {
            e.printStackTrace();

            return new UploadFileResponse(fileName, fileDownloadUri,
                    file.getContentType(), file.getSize());
        }
    }

    @PostMapping("/api/payment-uploads/activation/upload-file")
    public UploadFileResponse uploadPaymentActivationRequestFile(@RequestParam("file") MultipartFile file) {

        return null;
    }

//    @PostMapping("/upload-multiple-files")
//    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//        return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
//    }

    @GetMapping("/api/payment-uploads/download-file/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = storageService.loadAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping("/api/nssa/receipt/download/{fileName:.+}")
    public ResponseEntity<Resource> downloadReceiptFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = storageService.loadNssaReceiptAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}