package zw.co.esolutions.zpas.controller.main;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.profile.OtpDTO;
import zw.co.esolutions.zpas.dto.profile.PasswordDTO;
import zw.co.esolutions.zpas.integration.camel.config.util.Konstants;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.Person;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.security.email.MailSendUtil;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.repo.ProfileRepository;
import zw.co.esolutions.zpas.security.services.OtpService;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;
import zw.co.esolutions.zpas.services.iface.agreement.MandateHolderService;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alfred on 13 Feb 2019
 */
@Slf4j
@Controller
public class MainController {

    @Autowired
    OtpService otpService;

    @Autowired
    private MailSendUtil mailSendUtil;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private MandateHolderService mandateHolderService;

    @GetMapping("/")
    public String index(Model model, HttpSession session) {
        String profileId = (String) session.getAttribute("profileId");
        model.addAttribute("title", "ZEEPAY Acquiring System");
        return profileService.getProfileById(profileId).map(profile -> {
            if (profile.isChangePassword()) {
                model.addAttribute("message", "Please change your password");
                return "redirect:/change-password/page";
            } else {
                return "main/index";
            }
        }).orElse("main/index");
    }

    @GetMapping("/hello")
    public String hello(Model model, @RequestParam(value = "name", required = false, defaultValue = "ISO 20022!") String name) {
        model.addAttribute("name", name);
        return "main/index";
    }

    @GetMapping("/login")
    public String loginPage(Model model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("title", "ZEEPAY Acquiring System");
        return "main/login_page";
    }

    @GetMapping("/login/error")
    public String loginError(Model model) {
        model.addAttribute("title", "ZEEPAY Acquiring System");
        model.addAttribute("message", "Bad Credentials");
        return "main/login_page";
    }

    @GetMapping("/logout/user")
    public String logoutUser(HttpSession httpSession) {
        httpSession.invalidate();
        return "redirect:/logout";
    }

    @GetMapping("/logout-success")
    public String logoutPage() {
        return "main/logout";
    }

    @GetMapping("/update-session/{loginAsId}")
    public String updateLoginAsSession(@PathVariable String loginAsId, HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes) {
        log.info("Login as id {} and user id is {}", loginAsId, httpServletRequest.getSession().getAttribute("clientId"));
        httpServletRequest.getSession().setAttribute("loginAsId", loginAsId);

        final String profileId = (String)httpServletRequest.getSession().getAttribute("profileId");

        String partyName = getPartyName(partyRepository.findById(loginAsId)).getName();
        httpServletRequest.getSession().setAttribute("loginAsName", partyName);

        profileRepository.updateLastLoggedInAs(loginAsId, partyName, profileId);

        log.info("New login as id is {} and party name is {}", httpServletRequest.getSession().getAttribute("loginAsId"), partyName);
        //redirectAttributes.addFlashAttribute("message", "You are logged in as " + partyName);
        return "redirect:/";
    }

    @ResponseBody
    @GetMapping("/get-roles")
    public List<PartyInfo> userRoles(HttpServletRequest httpServletRequest) {
        String clientId = (String) httpServletRequest.getSession().getAttribute("clientId");
        String loginAsId = (String) httpServletRequest.getSession().getAttribute("loginAsId");
        String clientName = (String) httpServletRequest.getSession().getAttribute("clientName");

        List<PartyInfo> rolesIssuers = mandateHolderService.getMandateHolderRoles(clientId).stream()
                .map(rolePlayer -> getPartyName(Optional.of((Party) rolePlayer)))
                .collect(Collectors.toList());

        Set<PartyInfo> uniqueRoleIssuers = new HashSet<>();

        uniqueRoleIssuers = rolesIssuers.stream()
                .peek(partyInfo -> {
                    if (partyInfo.getPartyId().equals(loginAsId)) {
                        partyInfo.setStatus("ACTIVE");
                    }
                })
                .collect(Collectors.toSet());
        boolean anyMatch = uniqueRoleIssuers.stream().anyMatch(partyInfo -> partyInfo.getStatus().equals("ACTIVE"));
        if (anyMatch) {
            PartyInfo partyInfo = new PartyInfo();
            partyInfo.setName(clientName);
            partyInfo.setPartyId(clientId);
            partyInfo.setStatus("INACTIVE");
            uniqueRoleIssuers.add(partyInfo);
        }


        return new ArrayList<>(uniqueRoleIssuers);
    }

    @Data
    class PartyInfo {
        String name;
        String partyId;
        String status;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PartyInfo partyInfo = (PartyInfo) o;
            return Objects.equals(partyId, partyInfo.partyId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(partyId);
        }
    }

    private PartyInfo getPartyName(Optional<Party> optionalParty) {
        return optionalParty.map(party -> {
            final StringBuilder partyNameSb = new StringBuilder();
            String partyId = null;
            if (party instanceof Person) {
                Person person = (Person) party;

                person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                    personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                        final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                        partyNameSb.append(personFullName);
                    });
                });
            } else if (party instanceof Organisation) {
                Organisation organisation = (Organisation) party;

                organisation.getOrganisationIdentification().stream().findFirst().ifPresent(organisationIdentification -> {
                    organisationIdentification.getOrganisationName().stream().findFirst().ifPresent(organisationName -> {
                        final String personFullName = organisationName.getLegalName();
                        partyNameSb.append(personFullName);
                    });
                });
            }
            partyId = party.getId();

            PartyInfo partyInfo = new PartyInfo();
            partyInfo.setName(partyNameSb.toString());
            partyInfo.setPartyId(partyId);
            partyInfo.setStatus("INACTIVE");

            return partyInfo;
        }).orElseGet(() -> new PartyInfo());
    }

    @GetMapping("/otp/page")
    public String otpPage() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            log.info("Principal username is -> {}", username);

            String otp = String.valueOf(otpService.generateOTP(username));
            log.info("OTP is -----> {}", otp);
            Optional<Profile> profileOptional = profileService.getProfileByUsername(username);
            profileOptional.ifPresent(profile -> {
                mailSendUtil.sendOTP(profile, otp);
            });
        } else {
            String username = principal.toString();
        }
        return "main/otp";
    }

    @PostMapping("/otp")
    public String otp(@Valid OtpDTO otpDTO, BindingResult bindingResult, Model model, HttpSession httpSession) {

        model.addAttribute("title", "OTP");

        if (bindingResult.hasErrors()) {
            return "logout";
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            String cachedOtp = String.valueOf(otpService.getOtp(username));
            if (otpDTO.getValue().equals(cachedOtp)) {
                httpSession.setAttribute(Konstants.OTP_AUTHENTICATED, Konstants.OTP_AUTHENTICATED);
                return "redirect:/";
            } else {
                otpService.clearOTP(username);
                return "redirect:/logout";
            }
        }
        return "redirect:/logout";
    }

    @GetMapping("/change-password/page")
    public String changePasswordPage() {
        return "main/change_password";
    }

    @PostMapping("/change-password")
    public String changePassword(@Valid PasswordDTO passwordDTO, BindingResult bindingResult, Model model, HttpSession session) {

        model.addAttribute("title", "Change Password");

        if (bindingResult.hasErrors()) {
            return "main/change_password";
        }

        final String profileId = (String) session.getAttribute("profileId");
        return profileService.getProfileById(profileId).map(profile -> {
            passwordDTO.setUsername(profile.getUsername());
            final Optional<String> stringOptional = profileService.changeProfilePassword(passwordDTO);
            return stringOptional.map(s -> {
                switch (s) {
                    case SystemConstants.INVALID_OLD_PASSWORD:
                        model.addAttribute("message", "Invalid old password.");
                        return "main/change_password";
                    case SystemConstants.CHANGE_PASSWORD_SUCCESS:
                        model.addAttribute("message", "Password successfully updated");
                        return "redirect:/";
                    default:
                        model.addAttribute("message", "An error occurred. Please try again later.\n" + s);
                        return "main/change_password";
                }
            }).orElseGet(() -> {
                model.addAttribute("message", "An error occurred. Please try again later. Got no response from server.");
                return "main/change_password";
            });
        }).orElseGet(() -> {
            model.addAttribute("message", "An error occurred. Please try again later. Got no response from server.");
            return "main/change_password";
        });
    }
}
