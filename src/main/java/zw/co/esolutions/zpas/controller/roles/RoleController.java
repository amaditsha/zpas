package zw.co.esolutions.zpas.controller.roles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import zw.co.esolutions.zpas.model.Role;
import zw.co.esolutions.zpas.repository.RoleRepository;

import javax.validation.Valid;

@Controller
public class RoleController {

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping("/roles")
    public String showAddRoleForm(Role role) {
        return "add-role";
    }

    @PostMapping("/add-role")
    public String addRole(@Valid Role role, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-role";
        }

        roleRepository.save(role);
        model.addAttribute("roles", roleRepository.findAll());
        return "index";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") String id, Model model) {
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid role Id:" + id));

        model.addAttribute("role", role);
        return "update-role";
    }

    @PostMapping("/update/{id}")
    public String updateRole(@PathVariable("id") String id, @Valid Role role,
                             BindingResult result, Model model) {
        if (result.hasErrors()) {
            role.setId(id);
            return "update-role";
        }

        roleRepository.save(role);
        model.addAttribute("roles", roleRepository.findAll());
        return "index";
    }

    @GetMapping("/delete/{id}")
    public String deleteRole(@PathVariable("id") String id, Model model) {
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid role Id:" + id));
        roleRepository.delete(role);
        model.addAttribute("roles", roleRepository.findAll());
        return "index";
    }

}
