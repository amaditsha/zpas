package zw.co.esolutions.zpas.controller.registration;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.registration.NonFinancialInstitutionRegistrationDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.OrganisationIdentificationRepository;
import zw.co.esolutions.zpas.services.OrganisationSearchService;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.country.CountryService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.iface.registration.NonFinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/non-financial-institutions")
public class NonFinancialInstitutionController {
    @Autowired
    private NonFinancialInstitutionService nonFinancialInstitutionService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private OrganisationIdentificationRepository organisationIdentificationRepository;

    @Autowired
    private OrganisationSearchService organisationSearchService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @GetMapping("/create-page")
    @PreAuthorize("hasAuthority('ROLE_CREATE_NON_FINANCIAL_INSTITUTION')")
    public String registerNonFinancialInstitutionPage(Model model) {
        final List<NonFinancialInstitution> nonFinancialInstitutions = nonFinancialInstitutionService.getNonFinancialInstitutions();
        final List<Country> countries = countryService.getAllCountries();

        model.addAttribute("title", "Register Non Financial Institution");
        model.addAttribute("addressTypes", AddressTypeCode.values());
        model.addAttribute("exemptionReasons", TaxExemptReasonCode.values());
        model.addAttribute("types", TaxTypeCode.values());
        model.addAttribute("countries", countries);
        model.addAttribute("basis", TaxationBasisCode.values());
        model.addAttribute("taxRateTypes", RateTypeCode.values());
        model.addAttribute("withholdingTaxTypes", WithholdingTaxRateTypeCode.values());
        model.addAttribute("taxIdentificationTypes", TaxIdentificationNumberTypeCode.values());
        model.addAttribute("nonFinancialInstitutions", nonFinancialInstitutions);

        return "nonfinancialinstitution/create_non_financial_institution";
    }

    @GetMapping("/")
    @PreAuthorize("hasAuthority('ROLE_VIEW_NON_FINANCIAL_INSTITUTION')")
    public String viewNonFinancialInstitutions(Model model) {
        model.addAttribute("title", "Register Non Financial Institution");
        model.addAttribute("addressTypes", AddressTypeCode.values());
        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        model.addAttribute("financialInstitutions", financialInstitutions);
        return "nonfinancialinstitution/view_non_financial_institutions";
    }

    @GetMapping("/approve/{nonFinancialInstitutionId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_NON_FINANCIAL_INSTITUTION')")
    public String approveNonFinancialInstitution(@PathVariable("nonFinancialInstitutionId") String nonFinancialInstitutionId, RedirectAttributes redirectAttributes, HttpSession session) {
        return nonFinancialInstitutionService.approveNonFinancialInstitution(nonFinancialInstitutionId).map(nonFinancialInstitution -> {
            return "redirect:/non-financial-institutions/details/" + nonFinancialInstitution.getId();
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Approval for Non Financial Institution failed. Can't be found.");
            return "redirect:/non-financial-institutions/details/" + nonFinancialInstitutionId;
        });
    }

    @GetMapping("/reject/{nonFinancialInstitutionId}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_NON_FINANCIAL_INSTITUTION')")
    public String rejectNonFinancialInstitution(@PathVariable("nonFinancialInstitutionId") String nonFinancialInstitutionId, RedirectAttributes redirectAttributes) {
        return nonFinancialInstitutionService.rejectNonFinancialInstitution(nonFinancialInstitutionId).map(nonFinancialInstitution -> {
            redirectAttributes.addFlashAttribute("message", "NonFinancial Institution  rejected.");
            return "redirect:/non-financial-institutions/details/" + nonFinancialInstitution.getId();
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Non Financial Institution rejection failed.");
            return "redirect:/non-financial-institutions/details/" + nonFinancialInstitutionId;
        });
    }

    @GetMapping("/submit/{nonFinancialInstitutionId}")
    public String submitNonFinancialInstitution(@PathVariable("nonFinancialInstitutionId") String nonFinancialInstitutionId, RedirectAttributes redirectAttributes) {
        return nonFinancialInstitutionService.submitNonFinancialInstitution(nonFinancialInstitutionId).map(nonFinancialInstitution -> {
            redirectAttributes.addFlashAttribute("message", "You have successfully submitted Non Financial Institution.");
            return "redirect:/non-financial-institutions/details/" + nonFinancialInstitution.getId();
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Non Financial Institution submission failed.");
            return "redirect:/non-financial-institutions/details/" + nonFinancialInstitutionId;
        });
    }

    @GetMapping("/delete/{nonFinancialInstitutionId}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_NON_FINANCIAL_INSTITUTION')")
    public String deleteNonFinancialInstitution(@PathVariable("nonFinancialInstitutionId") String nonFinancialInstitutionId, RedirectAttributes redirectAttributes) {
        return nonFinancialInstitutionService.deleteNonFinancialInstitution(nonFinancialInstitutionId).map(nonFinancialInstitution -> {
            redirectAttributes.addFlashAttribute("message", "Non Financial Institution deleted.");
            return "redirect:/non-financial-institutions/";
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Non Financial Institution deletion failed.");
            return "redirect:/non-financial-institutions/details/" + nonFinancialInstitutionId;
        });
    }

    @GetMapping("/details/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_NON_FINANCIAL_INSTITUTION')")
    public String viewNonFinancialInstitutionById(Model model, @PathVariable(name = "id") String id) {
        final Optional<NonFinancialInstitution> nonFinancialInstitutionOptional = nonFinancialInstitutionService.getNonFinancialInstitutionById(id);
        return nonFinancialInstitutionOptional.map(nonFinancialInstitution -> {
            String organisationName = nonFinancialInstitution.getName();
            final List<Account> clientAccounts = accountService.getAccountsByClientId(nonFinancialInstitution.getId());

            final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
            model.addAttribute("financialInstitutions", financialInstitutions);

            log.info("NonFI is {}", nonFinancialInstitution);
            model.addAttribute("nonFinancialInstitution", nonFinancialInstitution);
            model.addAttribute("title", "View Non Financial Institution");
            model.addAttribute("identification", new OrganisationIdentification());
            model.addAttribute("name", organisationName);
            model.addAttribute("clientAccounts", clientAccounts);
            model.addAttribute("genders", GenderCode.values());
            model.addAttribute("languages", LanguageCode.values());
            model.addAttribute("civilStatuses", CivilStatusCode.values());
            model.addAttribute("namePrefixes", NamePrefix1Code.values());
            return "nonfinancialinstitution/view_non_financial_institution";
        }).orElseGet(() -> {
            model.addAttribute("nonFinancialInstitution", null);
            return "nonfinancialinstitution/view_non_financial_institution";
        });
    }


    @PostMapping("/")
    public String registerNonFinancialInstitution(@Valid NonFinancialInstitutionRegistrationDTO nonFinancialInstitutionRegistrationDTO,
                                               BindingResult bindingResult, Model model, HttpSession session) {
        model.addAttribute("title", "Non Financial Institutions");
        if (bindingResult.hasErrors()) {
            return "nonfinancialinstitution/create_non_financial_institution";
        }

        log.info("The NonFI enable debit order value is {}", nonFinancialInstitutionRegistrationDTO.getEnableDebitOrder());
        final NonFinancialInstitution nonFinancialInstitutionCreated =
                nonFinancialInstitutionService.registerNonFinancialInstitution(nonFinancialInstitutionRegistrationDTO);

        session.setAttribute("nonFiId", nonFinancialInstitutionCreated.getId());
        log.info("nonFiId in session: " + session.getAttribute("nonFiId"));

        return "redirect:/non-financial-institutions/details/" + nonFinancialInstitutionCreated.getId();
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROLE_SEARCH_FINANCIAL_INSTITUTIONS')")
    public String searchOrganisation(@RequestParam(value = "search", required = false) String q, Model model) {
        List<Organisation> searchResults = null;
        log.info("Search string: " + q);
        try {
            searchResults = organisationSearchService.searchOrganisation(q);

        } catch (Exception ex) {
            // here you should handle unexpected errors
            // ...
            // throw ex;
            searchResults = new ArrayList<>();
        }
        log.info("Search results are: {}", searchResults);
        financialInstitutionService.refreshOrganisationIndex();
        model.addAttribute("search", searchResults);
        return "registration/search_non_financial_institution";

    }

    @GetMapping("/update/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_NON_FINANCIAL_INSTITUTION')")
    public String showNonFinancialInstitutionEditPage(@PathVariable(name = "id") String id, Model model) {
        final Optional<NonFinancialInstitution> nonFinancialInstitutionOptional = nonFinancialInstitutionService.getNonFinancialInstitutionById(id);

        return nonFinancialInstitutionOptional.map(nonFinancialInstitution -> {
            log.info("NonFI is {}", nonFinancialInstitution);
            NonFinancialInstitutionRegistrationDTO institutionRegistrationDTO = generateNonFinancialInstitutionRegistrationDTOFromFI(nonFinancialInstitution);
            model.addAttribute("nonFinancialInstitution", nonFinancialInstitution);
            model.addAttribute("title", "Update Financial Institution");
            model.addAttribute("institutionRegistrationDTO", institutionRegistrationDTO);
            model.addAttribute("addressTypes", AddressTypeCode.values());
            return "nonfinancialinstitution/update_financial_institution";
        }).orElseGet(() -> {
            model.addAttribute("nonFinancialInstitution", null);
            return "nonfinancialinstitution/view_non_financial_institution";
        });
    }

    public NonFinancialInstitutionRegistrationDTO generateNonFinancialInstitutionRegistrationDTOFromFI(NonFinancialInstitution nonFinancialInstitution){
        NonFinancialInstitutionRegistrationDTO dto = new NonFinancialInstitutionRegistrationDTO();
        dto.setId(nonFinancialInstitution.getId());

        dto.setPurpose(nonFinancialInstitution.getPurpose() == null ? "" :nonFinancialInstitution.getPurpose());
        dto.setEnableDebitOrder(nonFinancialInstitution.getEnableDebitOrder() == null ? "" :nonFinancialInstitution.getEnableDebitOrder().toString());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        dto.setRegistrationDay(nonFinancialInstitution.getRegistrationDate().format(formatter));

        dto.setParentOrganisationId(nonFinancialInstitution.getParentOrganisation() == null ? "" : nonFinancialInstitution.getParentOrganisation().getId());
        dto.setDescription(nonFinancialInstitution.getDescription() == null ? "" : nonFinancialInstitution.getDescription());
        dto.setEstablishmentDate(nonFinancialInstitution.getEstablishmentDate().format(formatter));

        dto.setRoles(nonFinancialInstitution.getRole() == null ? "": nonFinancialInstitution.getRole().toString());

        dto.setBicFIIdentifier(nonFinancialInstitution.getOrganisationIdentification().get(0).getBICFI());
        dto.setAnyBICIdentifier(nonFinancialInstitution.getOrganisationIdentification().get(0).getAnyBIC());

        if(nonFinancialInstitution.getOrganisationIdentification().size() > 0) {
            dto.setLegalName(nonFinancialInstitution.getOrganisationIdentification().get(0).getOrganisationName().get(0).getLegalName());
            dto.setTradingName(nonFinancialInstitution.getOrganisationIdentification().get(0).getOrganisationName().get(0).getTradingName());
            dto.setShortName(nonFinancialInstitution.getOrganisationIdentification().get(0).getOrganisationName().get(0).getShortName());
        }

        /*    private String phonesJson;
        private String addressesJson;
        private String postalAddressesJson;*/

        dto.setPlcOfRegAddressType(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getAddressType().name());

        //Postal Address Fields
        dto.setStreetName(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getStreetName());
        dto.setStreetBuildingIdentification(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getStreetBuildingIdentification());
        dto.setPostCodeIdentification(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getPostCodeIdentification());
        dto.setTownName(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getTownName());
        dto.setState(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getState());
        dto.setBuildingName(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getBuildingName());
        dto.setFloor(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getFloor());
        dto.setDistrictName(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getDistrictName());
        dto.setPostOfficeBox(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getPostOfficeBox());
        dto.setProvince(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getProvince());
        dto.setDepartment(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getDepartment());
        dto.setSubDepartment(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getSubDepartment());
        dto.setSuiteIdentification(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getSuiteIdentification());
        dto.setBuildingIdentification(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getBuildingIdentification());
        dto.setMailDeliverySubLocation(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getMailDeliverySubLocation());
        dto.setBlock(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getBlock());
        dto.setDistrictSubDivisionIdentification(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getDistrictSubDivisionIdentification());
        dto.setLot(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getLot());

        dto.setPlcOfRegCountryId(nonFinancialInstitution.getPlaceOfRegistration().getAddress().get(0).getCountyIdentification());

        dto.setTaxConditionsId(nonFinancialInstitution.getTaxationConditions().getId());
        dto.setValidityPeriodStartDate(nonFinancialInstitution.getValidityPeriod().getFromDateTime().format(formatter));
        dto.setValidityPeriodEndDate(nonFinancialInstitution.getValidityPeriod().getToDateTime().format(formatter));

        return dto;
    }

    @PostMapping("/edit/general/information/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_NON_FINANCIAL_INSTITUTION')")
    public ResponseEntity<?> editGeneralNonFIinformationDetails(@ModelAttribute("nonFinancialInstitution") NonFinancialInstitution formNonFI, Errors errors){
        log.info("Editing non financial institution details in controller | " + formNonFI.getId());

        Optional<NonFinancialInstitution> nonFinancialInstitutionOptional = nonFinancialInstitutionService.getNonFinancialInstitutionById(formNonFI.getId());
        log.info("NonFI general details found? : {} ", nonFinancialInstitutionOptional);

        AjaxResponseBody result = new AjaxResponseBody();

        nonFinancialInstitutionOptional.ifPresent(nonFinancialInstitution -> {

            log.info("Setting old value {} " + nonFinancialInstitution.getPurpose() + " to -> new value {} " + formNonFI.getPurpose());
            nonFinancialInstitution.setPurpose(formNonFI.getPurpose());
            boolean enableDebitOrder = formNonFI.getEnableDebitOrder() == null ? false : formNonFI.getEnableDebitOrder();
            nonFinancialInstitution.setEnableDebitOrder(enableDebitOrder);

            if(nonFinancialInstitution.getTaxationConditions() != null && nonFinancialInstitution.getTaxationConditions().getId() == null) {
                nonFinancialInstitution.setTaxationConditions(null);
            }


            NonFinancialInstitution updatedNonFinancialInstitution = nonFinancialInstitutionService.updateNonFinancialInstitutionGeneralInformation(nonFinancialInstitution);
            log.info("NonFI Purpose after updating: {} " + updatedNonFinancialInstitution.getPurpose());

            if(updatedNonFinancialInstitution.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                GeneralnformationTemp generalnformationTemp = new GeneralnformationTemp();
                generalnformationTemp.setId(updatedNonFinancialInstitution.getId());
                generalnformationTemp.setPurpose(updatedNonFinancialInstitution.getPurpose());
                generalnformationTemp.setEnableDebitOrder(updatedNonFinancialInstitution.getEnableDebitOrder());

                result.setObject(generalnformationTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The general information can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }

    @PostMapping("/edit/organisation/identification/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_NON_FINANCIAL_INSTITUTION')")
    public ResponseEntity<?> editOrganisationIdentificationDetails(@ModelAttribute("organisationIdentification") OrganisationIdentification formOrganisationIdentification, Errors errors){
        log.info("Edit the organisation identification details in controller | " + formOrganisationIdentification.getId());

        Optional<OrganisationIdentification> organisationIdentificationOptional = organisationIdentificationRepository.findById(formOrganisationIdentification.getId());
        log.info("Organisation Identification details found? : {} " + organisationIdentificationOptional);

        AjaxResponseBody result = new AjaxResponseBody();

        organisationIdentificationOptional.ifPresent(organisationIdentification -> {
            log.info("Setting old value {} " + organisationIdentification.getBICFI() + " to -> new value {} " + formOrganisationIdentification.getBICFI());
            organisationIdentification.setBICFI(formOrganisationIdentification.getBICFI());
            organisationIdentification.setAnyBIC(formOrganisationIdentification.getAnyBIC());

//            List<OrganisationName> organisationNames = new ArrayList<>();
//            OrganisationName organisationName = new OrganisationName();
//            organisationName.setLegalName(formOrganisationIdentification.getOrganisationName().get(0).getLegalName());
//            organisationName.setTradingName(formOrganisationIdentification.getOrganisationName().get(0).getTradingName());
//            organisationName.setShortName(formOrganisationIdentification.getOrganisationName().get(0).getShortName());
//            organisationNames.add(organisationName);
//            organisationIdentification.setOrganisationName(organisationNames);

            OrganisationIdentification updatedOrganisationIdentification = nonFinancialInstitutionService.updateNonFinancialInstitutionOrganisatinIdentification(organisationIdentification);
            log.info("BIC after updating: {} " + updatedOrganisationIdentification.getBICFI());

            if(updatedOrganisationIdentification.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                OrganisationIdentificationTemp organisationIdentificationTemp = new OrganisationIdentificationTemp();
                organisationIdentificationTemp.setId(updatedOrganisationIdentification.getId());
                organisationIdentificationTemp.setBICFI(updatedOrganisationIdentification.getBICFI());
                organisationIdentificationTemp.setAnyBICIdentifier(updatedOrganisationIdentification.getAnyBIC());
//                organisationIdentificationTemp.setLegalName(updatedOrganisationIdentification.getOrganisationName().get(0).getLegalName());
//                organisationIdentificationTemp.setShortName(updatedOrganisationIdentification.getOrganisationName().get(0).getShortName());
//                organisationIdentificationTemp.setTradingName(updatedOrganisationIdentification.getOrganisationName().get(0).getTradingName());
                result.setObject(organisationIdentificationTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The organisation identification can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }

    @Data
    public class GeneralnformationTemp{
        protected String id;
        protected String purpose;
        protected Boolean enableDebitOrder;
    }

    @Data
    public class OrganisationIdentificationTemp{
        protected String id;
        protected String bICFI;
        protected String anyBICIdentifier;
        protected String legalName;
        protected String tradingName;
        protected String shortName;
    }

}
