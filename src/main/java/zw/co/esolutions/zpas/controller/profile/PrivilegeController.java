package zw.co.esolutions.zpas.controller.profile;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.security.model.Privilege;
import zw.co.esolutions.zpas.security.services.iface.privilege.PrivilegeService;

import java.util.List;

/**
 * Created by alfred on 05 Apr 2019
 */
@Slf4j
@Controller
public class PrivilegeController {

    @Autowired
    private PrivilegeService privilegeService;

    @GetMapping("/privileges/refresh")
    @PreAuthorize("hasAuthority('ROLE_MANAGE_ACCESS_RIGHTS')")
    public String refreshAccessRights(RedirectAttributes redirectAttributes, @RequestParam("roleId") String roleId) {

        log.info("Refreshing access rights.");
        final List<Privilege> privileges = privilegeService.refreshPrivileges();
        if(privileges.size() > 0) {
            redirectAttributes.addAttribute("message", privileges.size() + " new privileges found.");
        } else {
            redirectAttributes.addAttribute("message", "No privileges found.");
        }
        return "redirect:/userrole/details/" + roleId;
    }
}
