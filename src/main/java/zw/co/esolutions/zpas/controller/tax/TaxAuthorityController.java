package zw.co.esolutions.zpas.controller.tax;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchResponseDTO;
import zw.co.esolutions.zpas.dto.registration.EmployerDetailsDTO;
import zw.co.esolutions.zpas.dto.registration.OrganisationDTO;
import zw.co.esolutions.zpas.dto.tax.TaxAuthorityDTO;
import zw.co.esolutions.zpas.dto.tax.TaxAuthorityPaymentOfficeDTO;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.services.iface.organisations.OrganisationService;
import zw.co.esolutions.zpas.services.iface.restapi.RestApiService;
import zw.co.esolutions.zpas.services.iface.tax.TaxAuthorityService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityType;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping(path = "/tax-authority")
public class TaxAuthorityController {
    @Autowired
    private TaxAuthorityService taxAuthorityService;

    @Autowired
    private RestApiService jsonApiService;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private OrganisationService organisationService;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");


    @GetMapping(value = "/home")
    public String renderHome(Model model) {
        log.info("Tax authority home page GET request.");
//        model.addAttribute("entityTypes", EntityType.values());
        return "taxauthority/home_taxauthority";
    }


    @GetMapping
    public String renderTaxAuthority(Model model) {
        log.info("Tax authoritypage GET request.");
        model.addAttribute("entityTypes", EntityType.values());
        return "taxauthority/create_taxauthority";
    }

    @PostMapping
    public String saveTaxAuthority(@Valid @ModelAttribute("taxAuthority") TaxAuthorityDTO taxAuthority, Errors errors, Model model) {
        log.info("Tax authority page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "taxauthority/create_taxauthority";
        }
        // Save the signature condition design...

        log.info("Tax authority: {}", taxAuthority);
        TaxAuthority persistedTaxAuthority = taxAuthorityService.createTaxAuthority(taxAuthority);
        return "redirect:/tax-authority/" + persistedTaxAuthority.getId();
    }

    @PostMapping("/payment-offices")
    public String createTaxAuthorityPaymentOffice(@Valid @ModelAttribute("taxAuthorityPaymentOfficeDTO") TaxAuthorityPaymentOfficeDTO taxAuthorityPaymentOfficeDTO, Errors errors, Model model) {
        log.info("Tax authority Payment office page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "taxauthority/create_taxauthority";
        }
        // Save the signature condition design...

        log.info("Tax authority Payment office: {}", taxAuthorityPaymentOfficeDTO);
        return taxAuthorityService.createTaxAuthorityPaymentOffice(taxAuthorityPaymentOfficeDTO).map(taxAuthorityPaymentOffice -> {
            return "redirect:/tax-authority/" + taxAuthorityPaymentOffice.getTaxAuthority().getId();
        }).orElseGet(() -> {
            return "redirect:/tax-authority/" + taxAuthorityPaymentOfficeDTO.getTaxAuthorityId();
        });
    }

    @GetMapping("/{id}")
    public String getTaxAuthorityById(@PathVariable("id") String id, Model model) {
        Optional<TaxAuthority> taxAuthorityOptional = taxAuthorityService.getTaxAuthority(id);
        return taxAuthorityOptional.map(taxAuthority -> {
            final List<TaxAuthorityPaymentOffice> taxAuthorityPaymentOfficesByTaxAuthority =
                    taxAuthorityService.getTaxAuthorityPaymentOfficesByTaxAuthority(taxAuthority.getId());
            model.addAttribute("formatter", formatter);
            model.addAttribute("taxAuthority", taxAuthority);
            model.addAttribute("paymentOffices", taxAuthorityPaymentOfficesByTaxAuthority);
            model.addAttribute("title", "View Tax Authority");
            return "taxauthority/view_taxauthority";
        }).orElseGet(() -> {
            return "taxauthority/create_taxauthority";
        });
    }

    @GetMapping("edit/{id}")
    public String renderUpdateRule(@PathVariable("id") String id, Model model) {
        log.info("Update signature condition page request.");
        model.addAttribute("title", "Create signature condition");
        Optional<TaxAuthority> taxAuthorityOptional = taxAuthorityService.getTaxAuthority(id);
        return taxAuthorityOptional.map(taxAuthority -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            model.addAttribute("formatter", formatter);
            model.addAttribute("taxAuthority", taxAuthority);
            model.addAttribute("moneyUtil", new MoneyUtil());
            model.addAttribute("entityTypes", EntityType.values());
            return "taxauthority/edit_taxauthority";
        }).orElseGet(() -> {
            return "tax-authority/home";
        });
    }

    @PostMapping("/edit")
    public String updateTaxAuthority(@Valid @ModelAttribute("taxAuthority") TaxAuthorityDTO taxAuthority, Errors errors, Model model) {
        log.info("Tax authority page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "tax-authority/home";
        }
        log.info("Updating the signature condition | {}", taxAuthority);
        TaxAuthority updatedTaxAuthority = taxAuthorityService.updateTaxAuthority(taxAuthority);
        return "redirect:/tax-authority/" + updatedTaxAuthority.getId();
    }



    @GetMapping("/approve/{id}")
    public String approveTaxAuthority(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        log.info("Approving the college");
        TaxAuthority taxAuthority = taxAuthorityService.approveTaxAuthority(id);

        Notification notification = new Notification();
        if(taxAuthority.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            notification.setType("success");
            notification.setMessage("Successfully approved.");
        } else {
            notification.setType("error");
            notification.setMessage("Failed to approve.");
        }

        redirectAttributes.addFlashAttribute("notification", notification);
        return "redirect:/tax-authority/" + taxAuthority.getId();
    }

    @GetMapping("/reject/{id}")
    public String rejectTaxAuthority(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        log.info("Reject the tax authority");
        TaxAuthority taxAuthority = taxAuthorityService.rejectTaxAuthority(id);
        Notification notification = new Notification();
        if(taxAuthority.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            notification.setType("success");
            notification.setMessage("Successfully dissaproved.");
        } else {
            notification.setType("error");
            notification.setMessage("Failed to disapprove.");
        }

        redirectAttributes.addFlashAttribute("notification", notification);
        return "redirect:/tax-authority/" + taxAuthority.getId();
    }

    @GetMapping("/delete/{id}")
    public String deleteTaxAuthority(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        log.info("Delete the college");
        TaxAuthority taxAuthority = taxAuthorityService.deleteTaxAuthority(id);
        Notification notification = new Notification();
        if (taxAuthority.getEntityStatus().equals(EntityStatus.DELETED)) {
            notification.setMessage("Successfully deleted.");
            notification.setType("success");
        } else {
            notification.setMessage("Failed to delete.");
            notification.setType("error");
        }

        return "redirect:/tax-authority/" + taxAuthority.getId();
    }

    @ResponseBody
    @GetMapping("/search/persons/")
    public List<PersonSearchResponseDTO> getPersonsViaAjax(@RequestParam("search") String search) {
        log.info("Get persons by search name {}", search);

        PersonSearchDTO personSearchDTO = PersonSearchDTO.builder().search(search).build();
        List<PersonSearchResponseDTO> searchResponseDTOS = jsonApiService.searchAllPersons(personSearchDTO);
        return searchResponseDTOS;
    }

    @ResponseBody
    @GetMapping("/payment-office/{taxAuthorityId}")
    public List<TaxAuthorityPaymentOffice> getTaxAuthorityPaymentOfficesByTaxAuthority(@PathVariable("taxAuthorityId") String taxAuthorityId) {
        return taxAuthorityService.getTaxAuthorityPaymentOfficesByTaxAuthority(taxAuthorityId);
    }

    @Data
    class PersonInfo {
        private String name;
        private String title;
    }

    @ResponseBody
    @GetMapping("/get/partyId/{partyId}")
    public PersonInfo getPersonNamesInfo(@PathVariable("partyId") String partyId) {
        log.info("The party id is {}", partyId);
        Optional<Party> optionalParty = partyRepository.findById(partyId);
        PersonInfo personInfo = new PersonInfo();
        StringBuilder personNameSb = new StringBuilder();
        log.info("Party is: {}", optionalParty.map(Party::getId).orElse("No party found."));
        optionalParty.map(party -> {
            if(party instanceof Person) {
                List<PersonIdentification> personIdentifications = ((Person) party).getPersonIdentification();
                log.info("Person Identifications: {}", personIdentifications);
                personIdentifications.stream().findFirst().ifPresent(personIdentification -> {
                    personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                        final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                        personNameSb.append(personFullName);
                    });
                });
                personInfo.setName(personNameSb.toString());
            } else if(party instanceof Organisation) {
                List<OrganisationIdentification> organisationIdentifications = ((Organisation) party).getOrganisationIdentification();
                log.info("Organisation Identifications: {}", organisationIdentifications);
                organisationIdentifications.stream().findFirst().ifPresent(organisationIdentification -> {
                    organisationIdentification.getOrganisationName().stream().findFirst().ifPresent(organisationName -> {
                        final String legalName = organisationName.getLegalName();
                        personNameSb.append(legalName);
                    });
                });
                personInfo.setName(personNameSb.toString());
            }
            return null;
        }).orElseGet(() -> null);
        log.info("The identification name is {}", personInfo);
        return personInfo;
    }

    @ResponseBody
    @GetMapping("/search/banks/")
    public List<OrganisationDTO> getFIOrganisationViaAjax(@RequestParam("search") String search) {
        log.info("Get bank by search name {}", search);
        final EmployerDetailsDTO employerDetailsDTO = EmployerDetailsDTO.builder().search(search).build();
        List<OrganisationDTO> organisationDTOS = organisationService.searchOrganisation(employerDetailsDTO);
        log.info("The list of organisations, {}", organisationDTOS);
        return organisationDTOS;
    }

    @ResponseBody
    @GetMapping("/search/non/fi/")
    public List<OrganisationDTO> getNonFIOrganisationViaAjax(@RequestParam("search") String search) {
        log.info("Get non fi by search name {}", search);
        final EmployerDetailsDTO employerDetailsDTO = EmployerDetailsDTO.builder().search(search).build();
        List<OrganisationDTO> organisationDTOS = organisationService.searchNonFIOrganisation(employerDetailsDTO);
        log.info("The list of non fi organisations, {}", organisationDTOS);
        return organisationDTOS;
    }

    @GetMapping("/all")
    public String viewTaxAuthorities(Model model) {
        List<TaxAuthority> taxAuthorities = taxAuthorityService.getTaxAuthorities();
        model.addAttribute("taxAuthorities", taxAuthorities);
        Notification notification = new Notification();
        if (taxAuthorities == null || taxAuthorities.size() == 0) {
            notification.setType("error");
            notification.setMessage("The tax authorities was not found.");
        }
        model.addAttribute("notification", notification);
        model.addAttribute("formatter", formatter);
        model.addAttribute("moneyUtil", new MoneyUtil());
        return "taxauthority/view_taxauthorities";
    }

    @Data
    class Notification {
        protected String message;
        protected String type;
    }
}
