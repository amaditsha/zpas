package zw.co.esolutions.zpas.controller.alerts;
import java.time.OffsetDateTime;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.model.Notification;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@Slf4j
public class NotificationController {
    @Autowired
    protected NotificationService notificationService;

    /**
     * GET  /  -> show the index page.
     */
    @RequestMapping("/notification")
    public String index() {
        return "alert/nots";
    }

    /**
     * GET  /notifications  -> show the notifications page.
     */
    @GetMapping("/notifications")
    public String notifications() {
        return "alert/notifications";
    }

    /**
     * POST  /some-action  -> do an action.
     *
     * After the action is performed will be notified UserA.
     */
    @RequestMapping(value = "/some-action", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> someAction(HttpSession session) {
        // Do an action here
        // ...
        // Send the notification to "UserA" (by username)
        String clientId = (String) session.getAttribute("clientId");
        String username = (String) session.getAttribute("username");

        Notification notification = new Notification();
        notification.setId(GenerateKey.generateEntityId());
        notification.setDateCreated(OffsetDateTime.now());
        notification.setEntityStatus(EntityStatus.DRAFT);
        notification.setMessage("Test message 1");
        notification.setRead(false);
        notification.setEntityId("");
        notification.setClientId(clientId);
        notification.setGroupId("");
        notification.setVersion(0L);
        log.info("Called the some action for username with username {}", username);
        notificationService.notify(notification, username);
        // Return an http 200 status code
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/notification/read/{id}")
    public ResponseEntity<?> markNotificationAsRead(@PathVariable(name = "id") String id) {
        Notification notification = notificationService.markNotificationAsRead(id);

        AjaxResponseBody result = new AjaxResponseBody();
        result.setNarrative("Successfully marked as read.");
        result.setResponseCode("00");
        result.setObject(notification);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/notification/unread")
    public ResponseEntity<?> getUnReadNotificationsForGroupId(HttpSession session) {
        String clientId = (String) session.getAttribute("clientId");

        if(StringUtils.isEmpty(clientId)) {
            clientId = "OIEG1553508308118563";
        }
        List<Notification> unReadNotificationsForClientId = notificationService.getUnReadNotificationsForClientId(clientId);

        AjaxResponseBody result = new AjaxResponseBody();
        result.setNarrative("Successfully query the unread notifications.");
        result.setResponseCode("00");
        result.setObject(unReadNotificationsForClientId);
        return ResponseEntity.ok(result);
    }
}
