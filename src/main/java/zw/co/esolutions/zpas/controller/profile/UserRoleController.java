package zw.co.esolutions.zpas.controller.profile;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.dto.profile.UserRoleDTO;
import zw.co.esolutions.zpas.dto.profile.UserRolePrivilegeDTO;
import zw.co.esolutions.zpas.dto.profile.UserRolePrivilegeRequestDTO;
import zw.co.esolutions.zpas.security.model.Privilege;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.security.services.iface.privilege.PrivilegeService;
import zw.co.esolutions.zpas.security.services.iface.userrole.UserRoleService;
import zw.co.esolutions.zpas.utilities.enums.RoleLevel;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mabuza on 11 Mar 2019
 */
@Slf4j
@Controller
public class UserRoleController {

    @Autowired
    private PrivilegeService privilegeService;

    @Autowired
    private UserRoleService userRoleService;

    @GetMapping("/userrole/create-page")
    public String createUserRolePage(Model model) {
        final List<RoleLevel> roleLevels = Arrays.stream(RoleLevel.values())
                .sorted(Comparator.comparingInt(RoleLevel::getWeight)).collect(Collectors.toList());
        model.addAttribute("privileges", getPrivilegesMap());
        model.addAttribute("roleLevels", roleLevels);
        return "userrole/create_user_role";
    }

    @PostMapping("/userrole")
    public String createUserRole(@Valid UserRoleDTO userRoleDTO, BindingResult bindingResult, Model model) {
        model.addAttribute("title", "User Role");

        if (bindingResult.hasErrors()) {
            return "userrole/create-page";
        }

        final UserRole createdUserRole = userRoleService.createUserRole(userRoleDTO);

        log.info("User Role created successfully -> {}", createdUserRole.getName());

        return "redirect:userrole/details/" + createdUserRole.getId();
    }

    @ResponseBody
    @PostMapping("/userrole/add/privilege")
    public UserRole addPrivilegeToUserRole(@RequestBody UserRolePrivilegeRequestDTO userRolePrivilegeRequestDTO) {
        return userRoleService.addPrivilegeToUserRole(userRolePrivilegeRequestDTO);
    }

    @ResponseBody
    @PostMapping("/userrole/remove/privilege")
    public UserRole removePrivilegeFromUserRole(@RequestBody UserRolePrivilegeRequestDTO userRolePrivilegeRequestDTO) {
        return userRoleService.removePrivilegeFromUserRole(userRolePrivilegeRequestDTO);
    }

    @GetMapping("/userrole/details/{id}")
    public String viewUserRole(Model model, @PathVariable(name = "id") String id) {
        return userRoleService.getUserRoleById(id).map(userRole -> {
            final List<UserRolePrivilegeDTO> userRolePrivileges = userRoleService.getUserRolePrivileges(userRole.getId());
            model.addAttribute("title", "View User Role");
            model.addAttribute("userRolePrivileges", userRolePrivileges);
            model.addAttribute("userrole", userRole);
            return "userrole/view_user_role";
        }).orElseGet(() -> {
            model.addAttribute("userRole", null);
            return "userrole/view_user_role";
        });
    }

    private List<Privilege> getPrivilegesMap() {

        return privilegeService.getAllPrivileges();
    }

    @GetMapping("/userrole/manage")
    public String manageUserRoles(Model model) {
        List<UserRole> userRoles = userRoleService.getAllUserRoles();
        model.addAttribute("userRoles", userRoles);
        return "userrole/manage_user_role";
    }
    @GetMapping("/userrole/privilege/manage")
    public String manageUserRolePrivileges(Model model) {
        List<UserRole> userRoles = userRoleService.getAllUserRoles();
        model.addAttribute("userRoles", userRoles);
        return "userrole/view_user_role_privilege";
    }

    @GetMapping("/userrole/privilege/details/{id}")
    public String viewUserRolePrivileges(Model model, @PathVariable(name = "id") String id) {
        return userRoleService.getUserRoleById(id).map(userRole -> {
            final List<UserRolePrivilegeDTO> userRolePrivileges = userRoleService.getUserRolePrivileges(userRole.getId());
            model.addAttribute("title", "View User Role Privilege");
            model.addAttribute("userRolePrivileges", userRolePrivileges);
            model.addAttribute("userrole", userRole);
            return "userrole/view_user_role_privilege";
        }).orElseGet(() -> {
            model.addAttribute("userRole", null);
            return "userrole/view_user_role_privilege";
        });
    }
}
