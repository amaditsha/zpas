package zw.co.esolutions.zpas.controller.signatory;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.account.AccountPartyInfo;
import zw.co.esolutions.zpas.dto.agreement.ApprovalRequestDTO;
import zw.co.esolutions.zpas.dto.agreement.ApprovalResponseDTO;
import zw.co.esolutions.zpas.dto.agreement.MandateHolderInfo;
import zw.co.esolutions.zpas.dto.agreement.MandateRegistrationDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchResponseDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.MandatePartyRoleRepository;
import zw.co.esolutions.zpas.repository.PersonRepository;
import zw.co.esolutions.zpas.repository.RolePlayerRepository;
import zw.co.esolutions.zpas.repository.SigningDetailRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountContractService;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountMandateService;
import zw.co.esolutions.zpas.services.iface.agreement.MandateHolderService;
import zw.co.esolutions.zpas.services.iface.agreement.SignatureConditionService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.restapi.RestApiService;
import zw.co.esolutions.zpas.services.iface.signature.ApprovalProcessorService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityType;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping(path = "/cash-account-mandate")
public class CashAccountMandateController {
    @Autowired
    private CashAccountMandateService cashAccountMandateService;
    @Autowired
    private SignatureConditionService signatureConditionService;

    @Autowired
    private CashAccountContractService cashAccountContractService;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private MandatePartyRoleRepository mandatePartyRoleRepository;

    @Autowired
    private ApprovalProcessorService approvalProcessorService;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RestApiService jsonApiService;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private MandateHolderService mandateHolderService;

    @Autowired
    private RolePlayerRepository rolePlayerRepository;

    @Autowired
    private SigningDetailRepository signingDetailRepository;


    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");


    @GetMapping(value = "/home/{cashAccountId}")
    public String renderHome(@PathVariable String cashAccountId, Model model) {
        log.info("Cash account mandate home page GET request.");
        model.addAttribute("cashAccountId", cashAccountId);
        return "mandate/home_cashaccountmandate";
    }

    private Boolean isMandateActive(EntityStatus status) {
        if (status.equals(EntityStatus.DELETED) || status.equals(EntityStatus.DISAPPROVED) || status.equals(EntityStatus.INACTIVE)) {
            return false;
        }
        return true;
    }


    @GetMapping("/all/{cashAccountId}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_CASH_ACCOUNT_MANDATE')")
    public String viewCashAccountMandates(@PathVariable String cashAccountId, Model model) {
        List<CashAccountMandate> cashAccountMandates = cashAccountMandateService.getCashAccountMandateByCashAccountId(cashAccountId);
        model.addAttribute("cashAccountMandates", cashAccountMandates);
        Notification notification = new Notification();
        model.addAttribute("isActive", false);
        if (cashAccountMandates.size() == 0) {
            notification.setType("error");
            notification.setMessage("The mandate(s) were not found.");
        } else {
            Optional<CashAccountMandate> optionalCashAccountMandate = cashAccountMandates.stream()
                    .filter(cashAccountMandate -> isMandateActive(cashAccountMandate.getEntityStatus()))
                    .findFirst();
            optionalCashAccountMandate.ifPresent(cashAccountMandate -> {
                model.addAttribute("isActive", true);
            });
        }

        CashAccount cashAccount = this.accountService.findAccountById(cashAccountId);

        model.addAttribute("cashAccount", cashAccount);

        model.addAttribute("cashAccountId", cashAccountId);
//        model.addAttribute("notification", notification);
        model.addAttribute("formatter", formatter);
        model.addAttribute("moneyUtil", new MoneyUtil());
        return "mandate/view_cashaccountmandates";
    }


    @GetMapping("/create/{cashAccountId}")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CASH_ACCOUNT_MANDATE')")
    public String renderCashAccountMandate(@PathVariable("cashAccountId") String cashAccountId, Model model, RedirectAttributes redirectAttributes) {
        log.info("Cash account mandate page GET request.");

        return cashAccountMandateService.checkIfCashAccountHaveValidMandate(cashAccountId).map(cashAccountMandate -> {
            Notification notification = new Notification();
            notification.setMessage("Oops! This account have valid mandate. Only one active mandate per account is required.");
            notification.setType("warning");

            redirectAttributes.addFlashAttribute("notification", notification);
            return "redirect:/cash-account-mandate/all/" + cashAccountId;
        }).orElseGet(() -> {
            CashAccount cashAccount = this.accountService.findAccountById(cashAccountId);
            Optional<RolePlayer> optionalRolePlayer = cashAccount.getPartyRole().stream().filter(accountPartyRole -> accountPartyRole instanceof AccountOwnerRole)
                    .map(accountPartyRole -> accountPartyRole.getPlayer())
                    .flatMap(Collection::stream)
                    .findFirst();
            optionalRolePlayer.ifPresent(rolePlayer -> {
                model.addAttribute("accountOwnerPersonId", rolePlayer.getId());
                log.info("The account owner person id is {}", rolePlayer.getId());
            });

            model.addAttribute("mandateIdentification", GenerateKey.generateRef());
            model.addAttribute("cashAccount", cashAccount);
            model.addAttribute("cashAccountMandate", new MandateRegistrationDTO());
            model.addAttribute("entityTypes", EntityType.values());
            model.addAttribute("instructionProcessingStatuses", InstructionProcessingStatusCode.values());
            model.addAttribute("settlementStatuses", SecuritiesSettlementStatusCode.values());
            model.addAttribute("cancellationProcessingStatuses", CancellationProcessingStatusCode.values());
            model.addAttribute("transactionProcessingStatuses", InstructionProcessingStatusCode.values());
            model.addAttribute("mandateHolderTypes", MandateHolderType.values());
            model.addAttribute("transactionChannelCode", TransactionChannelCode.ElectronicCommerce.name());
            model.addAttribute("cashAccountId", cashAccountId);
            List<SignatureCondition> signatureConditions = signatureConditionService.getSignatureConditions();
            model.addAttribute("signatureConditions", signatureConditions);
            return "mandate/create_cashaccountmandate";

        });
    }

    @PostMapping
    public String saveCashAccountMandate(@Valid @ModelAttribute("cashAccountMandate") MandateRegistrationDTO cashAccountMandate, Errors errors, Model model) {
        log.info("Cash account mandate page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "mandate/create_signaturecondition";
        }
        // Save the mandate condition design...

        log.info("Cash account mandate : {}", cashAccountMandate);
        CashAccountMandate persistedCashAccountMandate = cashAccountMandateService.createCashAccountMandate(cashAccountMandate);
        return "redirect:/cash-account-mandate/" + persistedCashAccountMandate.getId() + "/account/" + cashAccountMandate.getCashAccountId();
    }

    @GetMapping("/{id}/account/{accountId}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_CASH_ACCOUNT_MANDATE')")
    public String getCashAccountMandateById(@PathVariable("id") String id, @PathVariable("accountId") String accountId, Model model) {
        Optional<CashAccountMandate> cashAccountMandateOptional = cashAccountMandateService.getCashAccountMandateById(id);
        model.addAttribute("title", "View Signature Condition");
        return cashAccountMandateOptional.map(cashAccountMandate -> {
            CashAccount cashAccount = this.accountService.findAccountById(accountId);

            model.addAttribute("cashAccount", cashAccount);
            model.addAttribute(cashAccountMandate);
            model.addAttribute("formatter", formatter);
            model.addAttribute("moneyUtil", new MoneyUtil());
            model.addAttribute("cashAccountContract", new CashAccountContract());
            model.addAttribute("accountId", "");

            return "mandate/view_cashaccountmandate";
        }).orElseGet(() -> {
            Notification notification = new Notification();
            notification.setType("error");
            notification.setMessage("The mandate condition was not found.");
            model.addAttribute("notification", notification);
            return "mandate/view_cashaccountmandate";
        });
    }

    @GetMapping("edit/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CASH_ACCOUNT_MANDATE')")
    public String renderUpdateRule(@PathVariable("id") String id, Model model) {
        log.info("Update mandate condition page request.");
        model.addAttribute("title", "Create mandate condition");
        Optional<CashAccountMandate> cashAccountMandateOptional = cashAccountMandateService.getCashAccountMandateById(id);
        return cashAccountMandateOptional.map(cashAccountMandate -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            model.addAttribute("formatter", formatter);
            model.addAttribute("cashAccountMandate", cashAccountMandate);
            model.addAttribute("cashAccountContract", new CashAccountContract());
            model.addAttribute("moneyUtil", new MoneyUtil());
            model.addAttribute("entityTypes", EntityType.values());
            return "mandate/edit_cashaccountmandate";
        }).orElseGet(() -> {
            return "mandate/home";
        });
    }

    @PostMapping("/edit")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CASH_ACCOUNT_MANDATE')")
    public String updateCashAccountMandate(@Valid @ModelAttribute("cashAccountMandate") MandateRegistrationDTO cashAccountMandate, Errors errors, Model model) {
        log.info("Cash account mandate page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "mandate/home";
        }
        log.info("Updating the mandate condition | {}", cashAccountMandate);
//        CashAccountMandate updatedCashAccountMandate = cashAccountMandateService.updateCashAccountMandate(cashAccountMandate);
        return "redirect:/cash-account-mandate/" + cashAccountMandate.getId();
    }

    @GetMapping("/approve/{id}/account/{cashAccountId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CASH_ACCOUNT_MANDATE')")
    public String approveCashAccountMandate(@PathVariable("id") String id, @PathVariable("cashAccountId") String cashAccountId, RedirectAttributes redirectAttributes) {
        log.info("Approving the cash account mandate");
        Notification notification = new Notification();

        CashAccountMandate cashAccountMandate = cashAccountMandateService.approveCashAccountMandate(id);
        if (cashAccountMandate.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            notification.setMessage("Successfully approved.");
            notification.setType("success");
        } else {
            notification.setMessage("Failed to approve.");
            notification.setType("error");
        }
        redirectAttributes.addFlashAttribute(notification);

        return "redirect:/cash-account-mandate/" + cashAccountMandate.getId() + "/account/" + cashAccountId;
    }

    @GetMapping("/reject/{id}/account/{cashAccountId}/{description}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_CASH_ACCOUNT_MANDATE')")
    public String rejectCashAccountMandate(@PathVariable("id") String id, @PathVariable("cashAccountId") String cashAccountId, @PathVariable("description") String statusDescription, RedirectAttributes redirectAttributes) {
        log.info("Reject the cash account mandate");
        CashAccountMandate cashAccountMandate = cashAccountMandateService.rejectCashAccountMandate(id, statusDescription);
        Notification notification = new Notification();

        if (cashAccountMandate.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            notification.setMessage("Successfully disapproved.");
            notification.setType("success");
        } else {
            notification.setMessage("Failed to disapprove.");
            notification.setType("error");
        }
        redirectAttributes.addFlashAttribute(notification);
        return "redirect:/cash-account-mandate/" + cashAccountMandate.getId() + "/account/" + cashAccountId;
    }

    @GetMapping("/delete/{id}/account/{cashAccountId}/{description}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_CASH_ACCOUNT_MANDATE')")
    public String deleteCashAccountMandate(@PathVariable("id") String id, @PathVariable("cashAccountId") String cashAccountId, @PathVariable("description") String description, RedirectAttributes redirectAttributes) {
        log.info("Delete the cash account mandate");
        CashAccountMandate cashAccountMandate = cashAccountMandateService.deleteCashAccountMandate(id, description);
        Notification notification = new Notification();

        if (cashAccountMandate.getEntityStatus().equals(EntityStatus.DELETED)) {
            notification.setMessage("Successfully deleted.");
            notification.setType("success");
        } else {
            notification.setMessage("Failed to delete.");
            notification.setType("error");
        }
        redirectAttributes.addFlashAttribute(notification);
        return "redirect:/cash-account-mandate/" + cashAccountMandate.getId() + "/account/" + cashAccountId;
    }

    private Boolean canMandateAuthorise(MandateHolderType holderType) {
        if (holderType.equals(MandateHolderType.AUTHORISE) || holderType.equals(MandateHolderType.BOTH)) {
            return true;
        }
        return false;
    }

    @GetMapping("/get/mandate/holder/{mandateHolderId}")
    public ResponseEntity<?> getMandateHolderViaAjax(@PathVariable("mandateHolderId") String mandateHolderId) {
        log.info("Get mandate holder by id {} ", mandateHolderId);
        AjaxResponseBody result = new AjaxResponseBody();

        Optional<MandatePartyRole> optionalMandatePartyRole = mandatePartyRoleRepository.findById(mandateHolderId);

        return optionalMandatePartyRole.map(mandatePartyRole -> {
            try {
                mandatePartyRole.getPlayer().forEach(rolePlayer -> {
                    if(rolePlayer instanceof Person) {
                        Person person = (Person) rolePlayer;
                        StringBuilder personNameSb = new StringBuilder();

                        person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                            personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                                final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                                personNameSb.append(personFullName);
                            });
                        });

                        result.setResponseCode("00");
                        result.setObject(personNameSb.toString());
                        result.setNarrative("Successful");
                    } else {
                        log.info("The role player is not a person.");
                    }
                });
            } catch (Exception e) {
                result.setResponseCode("04");
                result.setNarrative("Failed to get the person name");
                e.printStackTrace();
            }
            return ResponseEntity.ok(result);
        }).orElseGet(() -> {
            result.setResponseCode("04");
            result.setNarrative("Failed to get the person name");
            return ResponseEntity.ok(result);
        });

    }

    @ResponseBody
    @GetMapping("/get/mandate/holders/{cashAccountId}")
    public List<PersonInfo> getAccountMandateHoldersViaAjax(@PathVariable("cashAccountId") String cashAccountId) {
        log.info("Getting the account mandate holders");
        List<MandateHolder> mandateHolders = cashAccountMandateService.getMandateHolders(cashAccountId);
        List<PersonInfo> personInfos = new ArrayList<>();
        mandateHolders.forEach(mandateHolder -> {
            String extra = mandateHolder.getHolderType().name();
            List<RolePlayer> rolePlayers = new ArrayList<>(mandateHolder.getPlayer());
            rolePlayers.forEach(rolePlayer -> {
                Person person = (Person) rolePlayer;
                StringBuilder personNameSb = new StringBuilder();
                person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                    personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                        final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                        personNameSb.append(personFullName);
                    });
                });
                PersonInfo personInfo = new PersonInfo();
                personInfo.setId(person.getId());
                personInfo.setExtra(extra);
                personInfo.setName(personNameSb.toString());
                personInfos.add(personInfo);
            });
        });
        return personInfos;
    }


    @ResponseBody
    @GetMapping("/get/mandate/all/holders/{cashAccountId}")
    public List<PersonInfo> getAllMandateHoldersViaAjax(@PathVariable("cashAccountId") String cashAccountId) {
        log.info("Getting all the account mandate holders for id {}", cashAccountId);
        List<MandateHolder> mandateHolders = cashAccountMandateService.getAllMandateHolders(cashAccountId);
        List<PersonInfo> personInfos = new ArrayList<>();
        mandateHolders.forEach(mandateHolder -> {
            String extra = mandateHolder.getHolderType().name();
            List<RolePlayer> rolePlayers = new ArrayList<>(mandateHolder.getPlayer());
            rolePlayers.forEach(rolePlayer -> {
                Person person = (Person) rolePlayer;
                StringBuilder personNameSb = new StringBuilder();
                person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                    personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                        final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                        personNameSb.append(personFullName);
                    });
                });
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");
                PersonInfo personInfo = new PersonInfo();
                personInfo.setId(mandateHolder.getId());
                personInfo.setExtra(extra);
                personInfo.setName(personNameSb.toString());
                personInfo.setDate(formatter.format(mandateHolder.getDateCreated()));
                personInfos.add(personInfo);
            });
        });
        return personInfos;
    }


    @ResponseBody
    @GetMapping("/get/initiators/{cashAccountId}")
    public List<PersonInfo> getAccountInitiatorsViaAjax(@PathVariable("cashAccountId") String cashAccountId) {
        log.info("Getting the account initiators");
        List<MandatePartyRole> mandateHolders = cashAccountMandateService.getInitiators(cashAccountId);
        List<PersonInfo> personInfos = new ArrayList<>();
        mandateHolders.forEach(mandatePartyRole -> {
            if (mandatePartyRole instanceof MandateHolder) {
                MandateHolder mandateHolder = (MandateHolder) mandatePartyRole;
                String extra = mandateHolder.getHolderType().name();
                List<RolePlayer> rolePlayers = new ArrayList<>(mandateHolder.getPlayer());
                rolePlayers.forEach(rolePlayer -> {
                    Person person = (Person) rolePlayer;
                    StringBuilder personNameSb = new StringBuilder();
                    person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                        personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                            final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                            personNameSb.append(personFullName);
                        });
                    });
                    PersonInfo personInfo = new PersonInfo();
                    personInfo.setId(person.getId());
                    personInfo.setExtra(extra);
                    personInfo.setName(personNameSb.toString());
                    personInfos.add(personInfo);
                });
            } else if (mandatePartyRole instanceof MandateIssuer) {
                MandateIssuer mandateIssuer = (MandateIssuer) mandatePartyRole;
                String extra = "ACCOUNT OWNER";
                List<RolePlayer> rolePlayers = new ArrayList<>(mandateIssuer.getPlayer());
                rolePlayers.forEach(rolePlayer -> {
                    if(rolePlayer instanceof Person) {
                        Person person = (Person) rolePlayer;
                        StringBuilder personNameSb = new StringBuilder();
                        person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                            personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                                final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                                personNameSb.append(personFullName);
                            });
                        });
                        PersonInfo personInfo = new PersonInfo();
                        personInfo.setId(person.getId());
                        personInfo.setExtra(extra);
                        personInfo.setName(personNameSb.toString());
                        personInfos.add(personInfo);
                    } else if(rolePlayer instanceof Organisation){
                        Organisation organisation = (Organisation) rolePlayer;
                        StringBuilder organisationNameSb = new StringBuilder();
                        organisation.getOrganisationIdentification().stream().findFirst().ifPresent(organisationIdentification -> {
                            organisationIdentification.getOrganisationName().stream().findFirst().ifPresent(organisationName -> {
                                final String personFullName = organisationName.getLegalName() + " (" + organisationName.getShortName() + ") ";
                                organisationNameSb.append(personFullName);
                            });
                        });
                        PersonInfo personInfo = new PersonInfo();
                        personInfo.setId(organisation.getId());
                        personInfo.setExtra(extra);
                        personInfo.setName(organisationNameSb.toString());
                        personInfos.add(personInfo);
                        log.info("The role player instance of organisation");
                    }
                });
            }
        });

        return personInfos;
    }

    @ResponseBody
    @GetMapping("/get/mandate/signatures/{paymentId}")
    public List<PersonInfo> getPaymentSignatureViaAjax(@PathVariable("paymentId") String paymentId) {
        log.info("Getting the payment signature");
        List<RolePlayer> rolePlayers = cashAccountMandateService.getSignatureForPayment(paymentId);

        HashMap<String, String> signingDetailMap = new HashMap<>();
        List<SigningDetail> signingDetails = signingDetailRepository.findByPaymentIdAndDone(paymentId, true);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");
        signingDetails.forEach(signingDetail -> {
            signingDetailMap.put(signingDetail.getPartyId(), formatter.format(signingDetail.getDateApproved()));
        });

        List<PersonInfo> personInfos = new ArrayList<>();
        List<String> signedIds = new ArrayList<>();
        rolePlayers.forEach(rolePlayer -> {
            Person person = (Person) rolePlayer;
            signedIds.add(person.getId());
            StringBuilder personNameSb = new StringBuilder();
            person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                    final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                    personNameSb.append(personFullName);
                });
            });
            PersonInfo personInfo = new PersonInfo();
            personInfo.setId(person.getId());
            personInfo.setName(personNameSb.toString());
            personInfo.setTitle("Signed");
            personInfo.setDate(signingDetailMap.get(person.getId()));
            personInfos.add(personInfo);
        });
        Optional<Payment> optionalPayment = paymentsService.getPaymentById(paymentId);
        if (optionalPayment.isPresent()) {
            log.info("Get the payment pending signatures ...");
            String cashAccountId = Optional.ofNullable(optionalPayment.get().getAccount()).map(CashAccount::getId).orElse("");
            List<MandatePartyRole> mandateParties = cashAccountMandateService.getAuthoriseMandateParties(cashAccountId);
            mandateParties.forEach(partyRole -> {
                List<RolePlayer> rolePlayers1 = partyRole.getPlayer().stream()
                        .filter(rolePlayer -> !signedIds.contains(rolePlayer.getId())).collect(Collectors.toList());
                rolePlayers1.forEach(rolePlayer -> {
                    if (rolePlayer instanceof Person) {
                        Person person = (Person) rolePlayer;
                        StringBuilder personNameSb = new StringBuilder();
                        person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                            personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                                final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                                personNameSb.append(personFullName);
                            });
                        });
                        PersonInfo personInfo = new PersonInfo();
                        personInfo.setId(person.getId());
                        personInfo.setName(personNameSb.toString());
                        personInfo.setTitle("Pending");
                        personInfos.add(personInfo);
                    } else if (rolePlayer instanceof Organisation) {
                        log.info("The party is organisation. Can't save the name.");
                    }
                });
            });
        }
        return personInfos;
    }

    private List<Personalize> getPersons(String mandateId) {
        List<Personalize> personalizes = new ArrayList<>();
        Optional<MandatePartyRole> optionalMandatePartyRole = mandatePartyRoleRepository.findById(mandateId);
        return optionalMandatePartyRole.map(mandatePartyRole -> {
            String title;
            if (mandatePartyRole instanceof MandateIssuer) {
                title = "Issuer";
                RolePlayer rolePlayer = mandatePartyRole.getPlayer().stream().findFirst().orElseGet(null);
                if (rolePlayer instanceof Person) {
                    Person person = (Person) rolePlayer;
                    Personalize personalize = null;
                    personalize = new Personalize();
                    personalize.setPerson(person);
                    personalize.setOrganisation(null);
                    personalize.setTitle(title);
                    personalize.setExtra("Issuer");

                    personalizes.add(personalize);
                } else if(rolePlayer instanceof Organisation){
                    log.info("The mandate issuer is the organisation.");
                    Organisation organisation = (Organisation) rolePlayer;
                    Personalize personalize = null;
                    personalize = new Personalize();
                    personalize.setOrganisation(organisation);
                    personalize.setPerson(null);
                    personalize.setTitle(title);
                    personalize.setExtra("Issuer");

                    personalizes.add(personalize);
                }

            } else if (mandatePartyRole instanceof MandateHolder) {
                title = "Holder";
                mandatePartyRole.getPlayer().forEach(rolePlayer -> {
                    Person person = (Person) rolePlayer;
                    Personalize personalize = null;
                    if (person != null) {
                        personalize = new Personalize();
                        personalize.setPerson(person);
                        personalize.setTitle(title);
                        personalize.setExtra(((MandateHolder) mandatePartyRole).getHolderType().name());
                    }
                    personalizes.add(personalize);
                });
            } else {
                title = "Not Applicable";
                Personalize personalize = new Personalize();
                personalize.setTitle(title);
                personalizes.add(personalize);
            }
            return personalizes;
        }).orElseGet(() -> null);
    }

    @ResponseBody
    @PostMapping("/get/mandate/holder")
    public List<PersonInfo> getPersonNamesInfo(Model model, @RequestBody List<String> mandateIds) {
        List<PersonInfo> persons = new ArrayList<>();
        mandateIds.forEach(mandateId -> {
            List<Personalize> personalizeOptional = getPersons(mandateId);
            personalizeOptional.forEach(personalize -> {
                StringBuilder personNameSb = new StringBuilder();

                if(personalize.getPerson() != null) {
                    personalize.getPerson().getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                        personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                            final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                            personNameSb.append(personFullName);
                        });
                    });
                } else if(personalize.getOrganisation() != null) {
                    Organisation organisation = personalize.getOrganisation();
                    organisation.getOrganisationIdentification().stream().findFirst().ifPresent(organisationIdentification -> {
                        organisationIdentification.getOrganisationName().stream().findFirst().ifPresent(organisationName -> {
                            final String organisationFullName = organisationName.getLegalName() + " (" + organisationName.getShortName() + ") ";
                            personNameSb.append(organisationFullName);
                        });
                    });
                }
                PersonInfo personInfo = new PersonInfo();
                personInfo.setId(mandateId);
                personInfo.setName(personNameSb.toString());
                personInfo.setTitle(personalize.getTitle());
                personInfo.setExtra(personalize.getExtra());
                persons.add(personInfo);
            });
        });
        return persons;
    }

    @ResponseBody
    @PostMapping("/get/mandate/person")
    public List<PersonInfo> getPersonNamesInfos(Model model, @RequestBody List<String> personIds) {
        List<PersonInfo> persons = new ArrayList<>();
        personIds.forEach(personId -> {
            Optional<Person> personOptional = personRepository.findById(personId);
            personOptional.ifPresent(person -> {
                StringBuilder personNameSb = new StringBuilder();

                person.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                    personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                        final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                        personNameSb.append(personFullName);
                    });
                });
                PersonInfo personInfo = new PersonInfo();
                personInfo.setId(personId);
                personInfo.setName(personNameSb.toString());
                persons.add(personInfo);
            });
        });
        return persons;
    }

    @PostMapping("/edit/generaldetails")
    public ResponseEntity<?> editGeneralDetails(@ModelAttribute("cashAccountMandate") CashAccountMandate formCashAccountMandate, Errors errors) {
        log.info("Edit the cash account mandate details");

        Optional<CashAccountMandate> cashAccountMandateOptional = cashAccountMandateService.getCashAccountMandateById(formCashAccountMandate.getId());
        AjaxResponseBody result = new AjaxResponseBody();

        cashAccountMandateOptional.ifPresent(cashAccountMandate -> {

            /**
             * I will add the edits info to the cash account mandate
             * */

            cashAccountMandate.setDescription(formCashAccountMandate.getDescription());
            cashAccountMandate.setVersion(formCashAccountMandate.getVersion());
            cashAccountMandate.setMandateIdentification(formCashAccountMandate.getMandateIdentification());
            cashAccountMandate.setTrackingIndicator(formCashAccountMandate.getTrackingIndicator());
            cashAccountMandate.setTrackingDays(formCashAccountMandate.getTrackingDays());
            cashAccountMandate.setRate(formCashAccountMandate.getRate());
            CashAccountMandate updatedCashAccountMandate = cashAccountMandateService.updateCashAccountMandate(cashAccountMandate);

            if (updatedCashAccountMandate != null && updatedCashAccountMandate.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");
                result.setObject(cashAccountMandate);
            } else {
                result.setNarrative("The cash account mandate can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }


    @PostMapping("/edit/accountcontract")
    public ResponseEntity<?> editAccountContract(@ModelAttribute("accountContract") CashAccountContract formCashAccountContract, Errors errors) {
        log.info("Edit the cash account contract details");

        Optional<CashAccountContract> optionalCashAccountContract = cashAccountContractService.getCashAccountContractById(formCashAccountContract.getId());
        AjaxResponseBody result = new AjaxResponseBody();

        optionalCashAccountContract.ifPresent(cashAccountContract -> {

            /**
             * I will add the edits info to the cash account contract
             * */

            cashAccountContract.setDescription(formCashAccountContract.getDescription());
            cashAccountContract.setVersion(formCashAccountContract.getVersion());
            cashAccountContract.setUrgencyFlag(formCashAccountContract.getUrgencyFlag());
            cashAccountContract.setRemovalIndicator(formCashAccountContract.getRemovalIndicator());
            CashAccountContract updatedCashAccountContract = cashAccountContractService.updateCashAccountContract(cashAccountContract);

            if (updatedCashAccountContract != null && updatedCashAccountContract.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");
                result.setObject(cashAccountContract);
            } else {
                result.setNarrative("The cash account contract can't be edited");
                result.setResponseCode("04");
            }
        });

        return ResponseEntity.ok(result);
    }

    private Boolean getBooleanFormValue(String booleanStringValue) {
        booleanStringValue = booleanStringValue == null ? "false" : booleanStringValue.equalsIgnoreCase("on") ?
                "true" : booleanStringValue.equalsIgnoreCase("true") ? "true" : "false";
        return Boolean.valueOf(booleanStringValue);
    }

    @ResponseBody
    @GetMapping("/approval/test")
    public ApprovalResponseDTO getApprovalTest() {//9VNF1552991961879728/ F0ES1552990705695225
        ApprovalRequestDTO requestDTO = ApprovalRequestDTO.builder().cashAccountId("H18I1552990809131661").personId("F0ES1552990705695225").paymentId("F0ES1552990056225").build();
        log.info("The approval request test | " + requestDTO);

        return approvalProcessorService.approveReq(requestDTO);
    }


    @ResponseBody
    @GetMapping("/search/persons/")//cash-account-mandate/persons/search/uio/H18I1552990809131661
    public List<PersonSearchResponseDTO> getPersonsViaAjax(@RequestParam("search") String search,
                                                           @RequestParam("cashAccountId") String cashAccountId) {
        log.info("Get persons by search name {}", search);

        AccountPartyInfo accountPartyInfo = cashAccountMandateService.getAccountPartyInfo(cashAccountId);

        PersonSearchDTO personSearchDTO = PersonSearchDTO.builder()
                .search(search)
                .partyType(accountPartyInfo.getPartyType())
                .partyRoleId(accountPartyInfo.getPartyRoleId())
                .build();
        List<PersonSearchResponseDTO> searchResponseDTOS = jsonApiService.searchPersonsForCashAccountMandate(personSearchDTO);
        searchResponseDTOS = searchResponseDTOS.stream()
                .filter(this::filterEmptyResponse)
                .collect(Collectors.toList());
        return searchResponseDTOS;
    }

    private boolean filterEmptyResponse(PersonSearchResponseDTO personSearchResponseDTO) {
        if(personSearchResponseDTO.getPersonId() == null
                || personSearchResponseDTO.getPersonId().equals("null")) {
            return false;
        }
        return true;
    }

    @ResponseBody
    @GetMapping("/remove/holder/{mandateHolderId}/{cashAccountMandateId}/{cashAccountId}")
    public Response removeMandateHolder(@PathVariable("mandateHolderId") String mandateHolderId,
                                        @PathVariable("cashAccountMandateId") String cashAccountMandateId,
                                        @PathVariable("cashAccountId") String cashAccountId) {
        log.info("Initiating removal of mandate holder with id {}", mandateHolderId);
        mandateHolderService.deleteMandateHolder(mandateHolderId);
        Response response = new Response();
        response.setMessage("Removed mandate holder successfully. You can refresh your page.");
        return response;
    }

    @PostMapping("/add/holder")
    public String addMandateHolder(@ModelAttribute MandateHolderInfo mandateHolderInfo, RedirectAttributes redirectAttributes) {
        log.info("Creating the new mandate holder | {}", mandateHolderInfo);
        Optional<CashAccountMandate> cashAccountMandateOptional = cashAccountMandateService.getCashAccountMandateById(mandateHolderInfo.getCashAccountMandateId());
        if (cashAccountMandateOptional.isPresent()) {
            CashAccountMandate cashAccountMandate = cashAccountMandateOptional.get();
            List<RolePlayer> rolePlayers = cashAccountMandate.getMandatePartyRole().stream().filter(mandatePartyRole1 -> mandatePartyRole1 instanceof MandateHolder)
                    .map(mandatePartyRole1 -> (MandateHolder) mandatePartyRole1)
                    .map(Role::getPlayer)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            boolean isRolePlayedAlreadyAMandate = rolePlayers.stream().anyMatch(rolePlayer -> rolePlayer.getId().equals(mandateHolderInfo.getRolePlayerId()));
            if (isRolePlayedAlreadyAMandate) {
                redirectAttributes.addFlashAttribute("message", "The mandate holder already exists.");
                return "redirect:/cash-account-mandate/" + mandateHolderInfo.getCashAccountMandateId() + "/account/" + mandateHolderInfo.getCashAccountId();
            }
        }
        mandateHolderService.saveMandateHolder(mandateHolderInfo);
        redirectAttributes.addFlashAttribute("message", "New mandate holder created.");
        return "redirect:/cash-account-mandate/" + mandateHolderInfo.getCashAccountMandateId() + "/account/" + mandateHolderInfo.getCashAccountId();
    }

    @Data
    class Response {
        protected String message;
    }

    @Data
    class Notification {
        protected String message;
        protected String type;
    }

    @Data
    class PersonInfo {
        protected String id;
        protected String name;
        protected String title;
        protected String extra;
        protected String date;
    }

    @Data
    class Personalize {
        private Organisation organisation;
        private Person person;
        private String title;
        private String extra;
    }

}
