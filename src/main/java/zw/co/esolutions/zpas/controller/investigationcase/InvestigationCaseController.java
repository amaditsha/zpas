package zw.co.esolutions.zpas.controller.investigationcase;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.investigationcase.PaymentInvestigationCaseDTO;
import zw.co.esolutions.zpas.enums.CancellationReasonCode;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.services.iface.investigationcase.PaymentInvestigationCaseService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.impl.process.pojo.DebitAuthorisationDTO;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
@RequestMapping(path = "/investigation-case")
public class InvestigationCaseController {

    @Autowired
    private PaymentInvestigationCaseService paymentInvestigationCaseService;

    @Autowired
    private PaymentsService paymentsService;


    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");


    @GetMapping("")
    public String paymentInvestigationCasePage(@PathVariable("paymentId") String paymentId, Model model) {
        log.info("paymentId in controller: " + paymentId);
        Optional<Payment> bulkPaymentOptional = paymentsService.getPaymentById(paymentId);
        model.addAttribute("title", "Payment Investigation Case");
        PaymentInvestigationCase paymentInvestigationCase = new PaymentInvestigationCase();
        bulkPaymentOptional.map(payment -> {
            model.addAttribute("paymentReference", payment.getId());
            model.addAttribute("paymentTypeCode", payment.getType());
            model.addAttribute("amount", payment.getAmount().getCurrency().getCodeName() + " " + payment.getAmount().getAmount());
            return "investigationcase/investigation-case";
        }).orElseGet(() -> {
            model.addAttribute("paymentInvestigationCase", null);
            return "investigationcase/investigation-case";
        });
        model.addAttribute("paymentInvestigationCase", paymentInvestigationCase);

        model.addAttribute("cancellationReasons", CancellationReasonCode.values());
        return "investigationcase/create_investigation_case";
    }

    @GetMapping("/view")
    public String viewPaymentInvestigationCase(Model model) {
        model.addAttribute("title", "View Payment Investigation Case");
        return "investigationcase/manage_payment_investigation_case";
    }

    @PostMapping("/register")
    public String createPaymentInvestigationCase(@Valid PaymentInvestigationCaseDTO paymentInvestigationCaseDTO,
                                                 Model model,HttpSession session) {

        String clientId = (String)session.getAttribute("clientId");
        log.info("clientId in PaymentInvestigation from session: " + clientId);

        model.addAttribute("title", "Payment Investigation Case");
        PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.createPaymentInvestigationCase(paymentInvestigationCaseDTO, clientId);

        return "redirect:/investigation-case/view/" + paymentInvestigationCase.getId();

    }

    @GetMapping("/view/{id}")
    public String viewPaymentInvestigationCase(Model model, @PathVariable(name = "id") String id) {
        final PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.findPaymentInvestigationCaseById(id);
        model.addAttribute("title", "View Payment Investigation Case");
        model.addAttribute("formatter", formatter);
        model.addAttribute("moneyUtil", new MoneyUtil());
        model.addAttribute("paymentInvestigationCase", paymentInvestigationCase);

        return "investigationcase/view_investigation_case";
    }


    @GetMapping("/approve/{id}")
    public ResponseEntity<?> approveInvestigationCaseViaAjax(@PathVariable("id") String id){
        PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.approvePaymentInvestigationCase(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(paymentInvestigationCase.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(paymentInvestigationCase.getEntityStatus().name());
        } else {
            result.setNarrative("The Investigation Case  condition can't be approved");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/reject/{id}")
    public ResponseEntity<?> rejectInvestigationCaseViaAjax(@PathVariable("id") String id){
        PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.rejectPaymentInvestigationCase(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(paymentInvestigationCase.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            result.setNarrative("Successfully rejected.");
            result.setResponseCode("00");
            result.setObject(paymentInvestigationCase.getEntityStatus().name());
        } else {
            result.setNarrative("The Investigation Case can't be rejected");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteInvestigationCaseViaAjax(@PathVariable("id") String id){
        PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.deletePaymentInvestigationCase(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(paymentInvestigationCase.getEntityStatus().equals(EntityStatus.DELETED)) {
            result.setNarrative("Successfully deleted.");
            result.setResponseCode("00");
            result.setObject(paymentInvestigationCase.getEntityStatus().name());
        } else {
            result.setNarrative("The Investigation Case  can't be deleted");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchInvestigationCase(@RequestParam(value = "search", required = false) String q, Model model) {
        List<PaymentInvestigationCase> searchResults = null;
        log.info("Search string: " + q);
        try {
            searchResults = paymentInvestigationCaseService.searchInvestigationCase(q);
            searchResults.stream().forEach(paymentInvestigationCase -> {
                try {
                    log.info("search results: {}", paymentInvestigationCase.getUnderlyingPayment().get(0).getId());
                }catch (Exception e) {
                    e.printStackTrace();
                }
            });

        } catch (Exception ex) {
            // here you should handle unexpected errors
            // ...
            // throw ex;
            log.info("catch block");
            searchResults = new ArrayList<>();
        }
        log.info("Search results are: {}", searchResults);
        paymentInvestigationCaseService.refreshPaymentInvestigationCaseSearchIndex();
        model.addAttribute("investigationCases", searchResults);
        return "investigationcase/manage_payment_investigation_case";

    }

    @PostMapping("/debit-authorisation")
    public String debitAuthorisationResponse(@ModelAttribute("authorisationDTO") DebitAuthorisationDTO authorisationDTO
            , RedirectAttributes redirectAttributes)
    {
        OffsetDateTime valueDateToDebit = null;
        if(StringUtils.isNoneBlank(authorisationDTO.getValueDateToDebitString())){
            valueDateToDebit = OffsetDateTime.parse(authorisationDTO.getValueDateToDebitString() + "T" +
                    OffsetTime.now(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        }

        authorisationDTO.setValueDateToDebit(valueDateToDebit);

        log.info("Authorisation String: " + authorisationDTO.getAuthorisationString());

        if(StringUtils.isNoneBlank(authorisationDTO.getAuthorisationString())){
            if(authorisationDTO.getAuthorisationString().equals("Yes")){
                authorisationDTO.setAuthorisation(Boolean.TRUE);
            }else{
                authorisationDTO.setAuthorisation(Boolean.FALSE);
            }
        }else {
            authorisationDTO.setAuthorisation(Boolean.FALSE);
        }

        log.info("The authorisation DTO is: {}", authorisationDTO);

        PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.debitAuthorisationResponseInitiation(authorisationDTO);

        log.info("Responded to investigation case {}", paymentInvestigationCase);
        redirectAttributes.addFlashAttribute("message", "Debit authorisation response submitted successfully.");
        return "redirect:/resolution/center/view/" + paymentInvestigationCase.getId();
    }

}
