package zw.co.esolutions.zpas.controller.hubconfig;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.model.HubConfiguration;
import zw.co.esolutions.zpas.services.iface.hubconfig.HubConfigurationService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
@RequestMapping("/hubConfigs")
public class HubConfigurationController {

    @Autowired
    HubConfigurationService hubConfigurationService;

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_HUB_CONFIGURATION')")
    public String createHubConfigurationPage(Model model, HttpSession session) {
        model.addAttribute("title", "Create Hub Configuration");
        //model.addAttribute("hubConfig", new HubConfiguration("uri"));
        return "hubconfig/create_hub_config";
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_HUB_CONFIGURATION')")
    public String createHubConfiguration(@Valid HubConfiguration configuration, Model model, BindingResult bindingResult, Errors errors, HttpSession session) {
        log.info("Entering Hub Config Controller for Create");

        model.addAttribute("title", "Create Hub Configuration");
        if (bindingResult.hasErrors()) {
            return "hubconfig/create_hub_config";
        }

        log.info("About to create hub config");
        final HubConfiguration hubConfigurationCreated = hubConfigurationService.createConfig(configuration);
        log.info("Hub Configuration ID is {}", hubConfigurationCreated.getId());

       // hubConfigurationCreated.getClientCertPath();

        return "redirect:view/" + hubConfigurationCreated.getId();
    }

    @PostMapping("/edit")
    @PreAuthorize("hasAuthority('ROLE_EDIT_HUB_CONFIGURATION')")
    public ResponseEntity<?> editHubConfiguration(@ModelAttribute("hubConfiguration") HubConfiguration formConfiguration, Model model, Errors errors, HttpSession session) {

        log.info("Inside Hub Config Edit Controller : {}", formConfiguration.getBIC());
        final Optional<HubConfiguration> configurationOptional = hubConfigurationService.getConfig(formConfiguration.getId());

        log.info("Found Config? : {} ", configurationOptional.isPresent());

        AjaxResponseBody result = new AjaxResponseBody();

        configurationOptional.ifPresent(configuration -> {

            //add the details to be edited
            configuration.setBIC(formConfiguration.getBIC());
            configuration.setUri(formConfiguration.getUri());
            configuration.setClientCertPath(formConfiguration.getClientCertPath());
            configuration.setClientKeyPath(formConfiguration.getClientKeyPath());

            log.info("BIC before updating: {} " , configuration.getBIC());

            HubConfiguration updatedHubConfiguration = hubConfigurationService.updateConfig(configuration);
            log.info("BIC after updating: {} " , updatedHubConfiguration.getBIC());

            if(updatedHubConfiguration.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                HubConfigurationTemp hubConfigurationTemp = new HubConfigurationTemp();
                hubConfigurationTemp.setId(updatedHubConfiguration.getId());
                hubConfigurationTemp.setBIC(updatedHubConfiguration.getBIC());
                hubConfigurationTemp.setUri(updatedHubConfiguration.getUri());
                hubConfigurationTemp.setClientCertPath(updatedHubConfiguration.getClientCertPath());
                hubConfigurationTemp.setClientKeyPath(updatedHubConfiguration.getClientKeyPath());
                hubConfigurationTemp.setEntityStatus(updatedHubConfiguration.getEntityStatus());

                result.setObject(hubConfigurationTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The hub config can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);

    }

    @GetMapping("/view/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_HUB_CONFIGURATION')")
    public String viewHubConfiguration(Model model, @PathVariable(name = "id") String id) {
        final Optional<HubConfiguration> configurationOptional = hubConfigurationService.getConfig(id);

        model.addAttribute("title", "View Hub Configuration");
        model.addAttribute("hubConfiguration", configurationOptional.get());

        return "hubconfig/view_hub_config";
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_HUB_CONFIGURATION')")
    public ResponseEntity<?> deleteHubConfigurationViaAjax(@PathVariable("id") String id){
        HubConfiguration hubConfiguration = hubConfigurationService.deleteHubConfig(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(hubConfiguration.getEntityStatus().equals(EntityStatus.DELETED)) {
            result.setNarrative("Successfully deleted.");
            result.setResponseCode("00");
            result.setObject(hubConfiguration.getEntityStatus().name());
        } else {
            result.setNarrative("The hub configuration can't be deleted");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/reject/{id}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_HUB_CONFIGURATION')")
    public ResponseEntity<?> rejectHubConfigurationViaAjax(@PathVariable("id") String id){
        HubConfiguration hubConfiguration = hubConfigurationService.rejectHubConfig(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(hubConfiguration.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            result.setNarrative("Successfully rejected.");
            result.setResponseCode("00");
            result.setObject(hubConfiguration.getEntityStatus().name());
        } else {
            result.setNarrative("The hub config can't be rejected");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/approve/{id}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_HUB_CONFIGURATION')")
    public ResponseEntity<?> approveHubConfigurationViaAjax(@PathVariable("id") String id){
        HubConfiguration hubConfiguration = hubConfigurationService.approveHubConfig(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(hubConfiguration.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(hubConfiguration.getEntityStatus().name());
        } else {
            result.setNarrative("The hub config can't be approved");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/manage")
    @PreAuthorize("hasAuthority('ROLE_VIEW_HUB_CONFIGURATION')")
    public String manageHubConfigs(Model model) {
        List<HubConfiguration> hubConfigurationList = hubConfigurationService.getConfigs();
        model.addAttribute("hubConfigs", hubConfigurationList);
        return "hubconfig/manage_hub_configs";
    }
    @Data
    public class HubConfigurationTemp{
        protected String id;
        protected String BIC;
        protected String uri;
        protected String clientCertPath;
        protected String clientKeyPath;
        protected EntityStatus entityStatus;

    }
}
