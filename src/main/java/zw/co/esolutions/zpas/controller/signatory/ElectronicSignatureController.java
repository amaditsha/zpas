package zw.co.esolutions.zpas.controller.signatory;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.dto.agreement.SignatureCreationDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.ElectronicSignature;
import zw.co.esolutions.zpas.model.PostalAddress;
import zw.co.esolutions.zpas.services.iface.signature.ElectronicSignatureService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityType;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping(path = "/electronicsignature")
public class ElectronicSignatureController {
    @Autowired
    private ElectronicSignatureService electronicSignatureService;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");


    @GetMapping(value = "/home")
    public String renderHome(Model model) {
        log.info("Electronic Signature home page GET request.");
//        model.addAttribute("entityTypes", EntityType.values());
        List<ElectronicSignature> electronicSignatures = electronicSignatureService.getElectronicSignatures()
                .stream().filter(electronicSignature -> !electronicSignature.getEntityStatus().equals(EntityStatus.DELETED))
                .collect(Collectors.toList());
        model.addAttribute("electronicSignatures", electronicSignatures);
        model.addAttribute("electronicSignature", new ElectronicSignature());
        model.addAttribute("copyDuplicateCodes", CopyDuplicateCode.values());
        model.addAttribute("documentTypeCodes", DocumentTypeCode.values());
        model.addAttribute("dataSetTypeCodes", DataSetTypeCode.values());
        model.addAttribute("addressTypes", AddressTypeCode.values());
        model.addAttribute("languageCodes", LanguageCode.values());
        if (electronicSignatures.size() == 0) {
            Notification notification = new Notification();
            notification.setType("info");
            notification.setMessage("No electronic signatures found.");
            model.addAttribute("notification", notification);
        }
        model.addAttribute("formatter", formatter);
        return "electronicsignature/home_electronicsignature";
    }


    @GetMapping
    public String renderElectronicSignature(Model model) {
        log.info("Electronic Signature page GET request.");
        model.addAttribute("electronicSignature", new ElectronicSignature());
        model.addAttribute("entityTypes", EntityType.values());
        return "electronicsignature/create_electronicsignature";
    }

    @PostMapping
    public String saveElectronicSignature(@Valid @ModelAttribute("electronicSignature") SignatureCreationDTO electronicSignature, Errors errors, Model model) {
        log.info("Electronic Signature page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "electronicsignature/home";
        }
        // Save the electronicsignature design...

        log.info("Electronic Signature : {}", electronicSignature);
        ElectronicSignature persistedElectronicSignature = electronicSignatureService.createElectronicSignature(electronicSignature);
        return "redirect:/electronicsignature/home";
    }

    @GetMapping("/{id}")
    public String getElectronicSignatureById(@PathVariable("id") String id, Model model) {
        Optional<ElectronicSignature> electronicSignatureOptional = electronicSignatureService.getElectronicSignatureById(id);
        return electronicSignatureOptional.map(electronicSignature -> {
            model.addAttribute(electronicSignature);


            return "home_electronicsignature";
        }).orElseGet(() -> {
            Notification notification = new Notification();
            notification.setType("error");
            notification.setMessage("The electronic signature was not found.");
            model.addAttribute("notification", notification);
            return "home_electronicsignature";
        });
    }

    @PostMapping("/edit")
    public ResponseEntity<?> updateElectronicSignatureViaAjax(@Valid @ModelAttribute("electronicSignature") ElectronicSignature electronicSignature, Errors errors) {
        log.info("Edit the electronic signature");
        log.info("Edit profile group | {}", electronicSignature);
        AjaxResponseBody result = new AjaxResponseBody();

        Optional<ElectronicSignature> oldElectronicSignatureOptional = electronicSignatureService.getElectronicSignatureById(electronicSignature.getId());
        return oldElectronicSignatureOptional.map(newElectronicSignature -> {
//            newElectronicSignature.setName(electronicSignature.getName());

            ElectronicSignature updatedElectronicSignature = electronicSignatureService.updateElectronicSignature(newElectronicSignature);
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(updatedElectronicSignature);

            return ResponseEntity.ok(result);
        }).orElseGet(() -> {
            result.setNarrative("The group can't be edited");
            result.setResponseCode("04");
            return ResponseEntity.ok(result);
        });
    }

    @GetMapping("/ajaxRequest/{id}")
    public ResponseEntity<?> getElectronicSignatureAjaxRequest(@PathVariable("id") String id) {
        log.info("Get ajax request the electronic signature | " + id);
        Optional<ElectronicSignature> electronicSignatureOptional = electronicSignatureService.getElectronicSignatureById(id);
        AjaxResponseBody result = new AjaxResponseBody();
        return electronicSignatureOptional.map(electronicSignature -> {
            result.setNarrative("Successful.");
            result.setResponseCode("00");
            result.setObject(rePopulateTemp(electronicSignature));
            return ResponseEntity.ok(result);
        }).orElseGet(() -> {
            result.setNarrative("The electronic signature can't be approved");
            result.setResponseCode("04");
            return ResponseEntity.ok(result);
        });
    }

    @GetMapping("/approve/{id}")
    public ResponseEntity<?> approveElectronicSignatureViaAjax(@PathVariable("id") String id) {
        log.info("Approving the electronic signature");
        ElectronicSignature electronicSignature = electronicSignatureService.approveElectronicSignature(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (electronicSignature.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(electronicSignature.getEntityStatus().name());
        } else {
            result.setNarrative("The group can't be approved");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/reject/{id}")
    public ResponseEntity<?> rejectElectronicSignatureViaAjax(@PathVariable("id") String id) {
        log.info("Reject the electronic signature");
        ElectronicSignature electronicSignature = electronicSignatureService.rejectElectronicSignature(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (electronicSignature.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            result.setNarrative("Successfully rejected.");
            result.setResponseCode("00");
            result.setObject(electronicSignature.getEntityStatus().name());
        } else {
            result.setNarrative("The group can't be rejected");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteElectronicSignatureViaAjax(@PathVariable("id") String id) {
        log.info("Delete the electronic signature");
        ElectronicSignature electronicSignature = electronicSignatureService.deleteElectronicSignature(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (electronicSignature.getEntityStatus().equals(EntityStatus.DELETED)) {
            result.setNarrative("Successfully deleted.");
            result.setResponseCode("00");
            result.setObject(electronicSignature.getEntityStatus().name());
        } else {
            result.setNarrative("The group can't be deleted");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @Data
    class SignatureTemp {
        protected String id;
        protected String documentPurpose;
        protected String copyDuplicateCode;
        protected String documentTypeCode;
        protected Boolean signedIndicator;
        protected String languageCode;
        protected String dataSetTypeCode;
        protected Number documentVersion;
        protected String addressType;
        protected String streetName;
        protected String streetBuildingIdentification;
        protected String townName;
        protected String state;
        protected String buildingName;
        protected String floor;
        protected String districtName;
        protected String province;
        protected String countryName;
    }

    public SignatureTemp rePopulateTemp(ElectronicSignature electronicSignature) {
        SignatureTemp signatureTemp = new SignatureTemp();
        try {
            signatureTemp.setId(electronicSignature.getId());
            signatureTemp.setDocumentPurpose(electronicSignature.getRelatedDocument().getPurpose());
            signatureTemp.setCopyDuplicateCode(electronicSignature.getRelatedDocument().getCopyDuplicate().name());
            signatureTemp.setDocumentTypeCode(electronicSignature.getRelatedDocument().getType().name());
            signatureTemp.setSignedIndicator(electronicSignature.getRelatedDocument().getSignedIndicator());
            signatureTemp.setLanguageCode(electronicSignature.getRelatedDocument().getLanguage().name());
            signatureTemp.setDataSetTypeCode(electronicSignature.getRelatedDocument().getDataSetType().name());
            signatureTemp.setDocumentVersion(electronicSignature.getRelatedDocument().getDocumentVersion());
            signatureTemp.setAddressType(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getAddressType().name());
            signatureTemp.setStreetName(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getStreetName());
            signatureTemp.setStreetBuildingIdentification(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getStreetBuildingIdentification());
            signatureTemp.setTownName(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getTownName());
            signatureTemp.setState(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getState());
            signatureTemp.setBuildingName(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getBuildingName());
            signatureTemp.setFloor(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getFloor());
            signatureTemp.setDistrictName(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getDistrictName());
            signatureTemp.setProvince(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getProvince());
            signatureTemp.setCountryName(((PostalAddress) electronicSignature.getRelatedDocument().getPlaceOfStorage().get(0)).getCountry().getName());
        }catch (Exception e) {
            e.printStackTrace();
            log.info("Failed to repopulate temp class");
        }
        return signatureTemp;
    }


    @Data
    class Notification {
        protected String message;
        protected String type;
    }

}
