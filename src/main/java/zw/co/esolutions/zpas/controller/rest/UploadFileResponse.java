package zw.co.esolutions.zpas.controller.rest;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 02 Apr 2019
 */
@Data
@Builder
public class UploadFileResponse {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
}
