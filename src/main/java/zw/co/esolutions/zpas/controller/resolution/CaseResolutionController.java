package zw.co.esolutions.zpas.controller.resolution;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.controller.main.MainController;
import zw.co.esolutions.zpas.dto.investigationcase.InvestigationCaseStatusDTO;
import zw.co.esolutions.zpas.dto.investigationcase.InvestigationStatusDTO;
import zw.co.esolutions.zpas.dto.payments.PaymentsStatusDTO;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.enums.RejectionReasonV2Code;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.enums.CaseStatusCode;
import zw.co.esolutions.zpas.model.InvestigationCaseStatus;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.model.PaymentInvestigationCaseResolution;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.agreement.MandateHolderService;
import zw.co.esolutions.zpas.services.iface.investigationcase.PaymentInvestigationCaseService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/resolution/center")
public class CaseResolutionController {

    public static final String CLIENT_ID = "ZS7M1553671747896995";

    @Autowired
    PaymentInvestigationCaseService paymentInvestigationCaseService;

    @Autowired
    PaymentsService paymentsService;

    @Autowired
    AccountsService accountsService;

    @Autowired
    InvestigationCaseStatusRepository investigationCaseStatusRepository;
    @Autowired
    PaymentInvestigationCaseResolutionRepository paymentInvestigationCaseResolutionRepository;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired MandateHolderService mandateHolderService;

    @Autowired
    PaymentIdentificationRepository paymentIdentificationRepository;

    @Autowired
    PaymentInvestigationCaseRepository paymentInvestigationCaseRepository;

    private String getDateRange() {
        //        01/01/2018 - 01/15/2018
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate toDate = LocalDate.now();
        LocalDate fromdate = toDate.minusMonths(1);
        String range = fromdate.format(formatter) + " - " + toDate.format(formatter);
        return range;
    }

    @GetMapping
    public String renderHome(Model model) {
        model.addAttribute("formatter", formatter);
        model.addAttribute("caseFilterRange", getDateRange());
        return "resolution/home";
    }

    @GetMapping("/view/{id}")
    public String viewInvestigationCase(@PathVariable String id, Model model, HttpSession session) {
        PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.findPaymentInvestigationCaseById(id);
        List<PaymentInvestigationCaseResolution> paymentInvestigationCaseResolutions = paymentInvestigationCaseResolutionRepository.findAllByInvestigationCase_id(id);
        String caseCreatorName = "";
        try {
            caseCreatorName = getCaseCreatorName(paymentInvestigationCase);
        } catch (Exception e){
            log.error("Error getting case creator name | {}", e.getMessage());
        }

        String clientId = (String) session.getAttribute("loginAsId");
        BigDecimal amountToBeDebited = BigDecimal.ZERO;
        try {
            amountToBeDebited = getAmountToBeDebited(paymentInvestigationCase.getUnderlyingPayment());
        } catch (Exception e) {
            log.error("Error getting case amount to debit | {}", e.getMessage());
        }

        log.info("Case Creator Name: " + caseCreatorName);
        log.info("Amount To Be Debited: " + amountToBeDebited);
        List<RejectionReasonV2Code> rejectionReasonV2Codes = Arrays.asList(RejectionReasonV2Code.values());

        boolean isDebitAuthorisationReq = paymentInvestigationCase.getUnderlyingPayment().stream().findFirst()
                .map(payment -> {
                    Optional<PaymentStatus> paymentStatusOptional = payment.getLatestPaymentStatus();
                    if (paymentStatusOptional.isPresent()) {
                        log.info("Payment Status Present..." + paymentStatusOptional.get().getStatus());
                        /*if (paymentStatusOptional.get().getStatus().equals(PaymentStatusCode.Accepted)
                                || paymentStatusOptional.get().getStatus().equals(PaymentStatusCode.AcceptedSettlementCompleted)) {*/
                        if (paymentStatusOptional.get().getStatus().equals(PaymentStatusCode.PendingCancellationRequest)) {

                            //There is no creditor role in AQS, there's only custom party. Get custom party
                            //to get account number and get account then account owner
                            Optional<PaymentPartyRole> optionalCreditorRole = payment.getPartyRole().stream()
                                    .filter(paymentPartyRole -> paymentPartyRole instanceof CreditorRole)
                                    .findFirst();

                            Optional<RolePlayer> accountOwnerOptional = Optional.empty();
                            if(optionalCreditorRole.isPresent()){
                                String accountNumber = optionalCreditorRole.get().getCustomParty().getAccountNumber();
                                Optional<CashAccount> optionalAccount = accountsService.findAccountByNumber(accountNumber);
                                if(optionalAccount.isPresent()){
                                    accountOwnerOptional = optionalAccount.get().getPartyRole().stream().filter(partyRole -> partyRole instanceof AccountOwnerRole)
                                            .flatMap(accountPartyRole -> accountPartyRole.getPlayer().stream())
                                            .findFirst();
                                }
                            }

                            /*Optional<RolePlayer> optionalCreditor = payment.getPartyRole().stream()
                                    .filter(paymentPartyRole -> paymentPartyRole instanceof CreditorRole)
                                    .flatMap(paymentPartyRole -> paymentPartyRole.getPlayer().stream())
                                    .findFirst();

                            if(optionalCreditor.isPresent() && optionalCreditor.get() instanceof Organisation){
                                List<RolePlayer> mandateHolderRoles = mandateHolderService.getMandateHolderRoles(optionalCreditor.get().getId());
                                return mandateHolderRoles.stream().anyMatch(rolePlayer -> rolePlayer.getId().equals(clientId));
                            }
                            //log.info("after if block of mandates...");
                            //log.info("creditor id: " +  optionalCreditor.get().getId() + " clientId: " + clientId);
                            return optionalCreditor.isPresent() && optionalCreditor.get().getId().equals(clientId);*/

                            if(accountOwnerOptional.isPresent() && accountOwnerOptional.get() instanceof Organisation){
                                List<RolePlayer> mandateHolderRoles = mandateHolderService.getMandateHolderRoles(accountOwnerOptional.get().getId());
                                return mandateHolderRoles.stream().anyMatch(rolePlayer -> rolePlayer.getId().equals(clientId));
                            }

                            if (!accountOwnerOptional.isPresent()){
                                log.error("Account owner not present for account number.");
                                return false;
                            }
                            log.info("after if block of mandates...");
                            log.info("creditor id: " +  accountOwnerOptional.get().getId() + " clientId: " + clientId);
                            return accountOwnerOptional.get().getId().equals(clientId);
                        }
                    }
                    return false;
                }).orElse(false);
        log.info("isDebitAuthReq: " + isDebitAuthorisationReq);
        /**
         * Put true for now as you test, remove the true when you have fixed the CreditorRoleBug.
         */
        //isDebitAuthorisationReq = true;

        model.addAttribute("paymentInvestigationCaseResolutions", paymentInvestigationCaseResolutions);
        model.addAttribute("rejectionReasonV2Codes", rejectionReasonV2Codes);

        model.addAttribute("investigationCase", paymentInvestigationCase);
        model.addAttribute("caseCreatorName", caseCreatorName);
        model.addAttribute("amountToBeDebited", amountToBeDebited);
        model.addAttribute("isDebitAuthorisationReq", isDebitAuthorisationReq);
        return "resolution/view_investigation_case";
    }

    @ResponseBody
    @GetMapping("/view/custom/{id}")
    public PaymentInvestigationCase viewCustomCase(@PathVariable String id) {
        return paymentInvestigationCaseRepository.findById(id).orElse(null);
    }

    @PostMapping("/case/update")
    public String rejectCaseStatus(@ModelAttribute("investigationCaseStatusDTO") InvestigationCaseStatusDTO investigationCaseStatusDTO, RedirectAttributes redirectAttributes) {

        PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseService.updateInvestigationCaseStatus(investigationCaseStatusDTO);

        redirectAttributes.addFlashAttribute("message",
                paymentInvestigationCase == null? "Failed to reject the investigation case" : "Reject successful. Updated the investigation case status.");
        return "redirect:/resolution/center/view/" + investigationCaseStatusDTO.getPaymentInvestigationCaseId();
    }

    @GetMapping("/view/all/{status}")
    public String viewInvestigationCases(@PathVariable String status, HttpSession session, Model model) {
        log.info("View all by status {}", status);
        CaseStatusCode caseStatusCode;

        List<CaseStatusCode> caseStatusCodes = new ArrayList<>();
        try {
            caseStatusCode = CaseStatusCode.valueOf(status);
            if(!caseStatusCode.equals(CaseStatusCode.Closed)) {
                caseStatusCodes.add(CaseStatusCode.Overdue);
                caseStatusCodes.add(CaseStatusCode.UnderInvestigation);
                caseStatusCodes.add(CaseStatusCode.Unknown);
                caseStatusCodes.add(CaseStatusCode.Assigned);
            }else {
                caseStatusCode = CaseStatusCode.Closed;
                caseStatusCodes.add(caseStatusCode);
            }

        } catch (Exception e) {
            log.error("The case status code : {} is not defined. Setting closed as default.", status);
            caseStatusCode = CaseStatusCode.Closed;
            caseStatusCodes.add(caseStatusCode);

        }

//        final CaseStatusCode finalCaseStatusCode = caseStatusCode;
        String clientId = (String) session.getAttribute("loginAsId");
        List<Payment> payments = paymentsService.getPaymentsByClientId(clientId);
        List<PaymentInvestigationCase> paymentInvestigationCases = payments.stream().map(Payment::getRelatedInvestigationCase)
                .filter(paymentInvestigationCase -> {

                    log.info("The investigation case is : {}", paymentInvestigationCase);

                    Optional<InvestigationCaseStatus> optionalLatestInvestigationCaseStatus = null;
                    if(paymentInvestigationCase == null || paymentInvestigationCase.getStatus() == null) {
                        return false;
                    }
                    optionalLatestInvestigationCaseStatus = paymentInvestigationCase.getStatus().stream()
                            .sorted(Comparator.comparing(InvestigationCaseStatus::getDateCreated).reversed())
                            .findFirst();

                    return optionalLatestInvestigationCaseStatus
                            .map(investigationCaseStatus -> caseStatusCodes.contains(investigationCaseStatus.getCaseStatus()))
                            .orElse(false);

                })
                .collect(Collectors.toList());
        if(paymentInvestigationCases.isEmpty()) {
            model.addAttribute("message", "No payment investigation case with status " + status + " found.");
        } else {
            model.addAttribute("message", "Payment investigation case with status " + status + " found.");
        }
        model.addAttribute("formatter", formatter);
        model.addAttribute("paymentInvestigationCases", paymentInvestigationCases);
        return "resolution/view_all_investigation_cases";
    }

    @PostMapping
    public String searchPaymentCases(@ModelAttribute("searchTemp") SearchTemp searchTemp, Model model) {
        log.info("The search request is | {}", searchTemp);
        List<PaymentInvestigationCase> paymentInvestigationCaseList = new ArrayList<>();
        Optional<PaymentIdentification> optionalPaymentIdentification;
        switch (searchTemp.getCaseFilter()) {
            case "CASE_ID":
                PaymentInvestigationCase investigationCase = paymentInvestigationCaseRepository.findByIdentification(searchTemp.getCaseInput());
                if(investigationCase != null) {
                    paymentInvestigationCaseList.add(investigationCase);
                }
                break;
            case "END_TO_END_ID":
                optionalPaymentIdentification = paymentIdentificationRepository.findByEndToEndIdentification(searchTemp.getCaseInput());
                if(optionalPaymentIdentification.isPresent()) {
                    PaymentInvestigationCase relatedInvestigationCase = optionalPaymentIdentification.get().getPayment().getRelatedInvestigationCase();
                    if(relatedInvestigationCase != null) {
                        paymentInvestigationCaseList.add(relatedInvestigationCase);
                    }
                }
                break;
            case "INSTRUCTION_ID":
                optionalPaymentIdentification = paymentIdentificationRepository.findByInstructionIdentification(searchTemp.getCaseInput());
                if(optionalPaymentIdentification.isPresent()) {
                    PaymentInvestigationCase relatedInvestigationCase = optionalPaymentIdentification.get().getPayment().getRelatedInvestigationCase();
                    if(relatedInvestigationCase != null) {
                        paymentInvestigationCaseList.add(relatedInvestigationCase);
                    }
                }
                break;
            case "EXECUTION_ID":
                optionalPaymentIdentification = paymentIdentificationRepository.findByExecutionIdentification(searchTemp.getCaseInput());
                if(optionalPaymentIdentification.isPresent()) {
                    PaymentInvestigationCase relatedInvestigationCase = optionalPaymentIdentification.get().getPayment().getRelatedInvestigationCase();
                    if(relatedInvestigationCase != null) {
                        paymentInvestigationCaseList.add(relatedInvestigationCase);
                    }
                }
                break;
            case "CASEID":
                paymentInvestigationCaseList = paymentInvestigationCaseService
                        .getAllByCaseId(searchTemp.getCaseInput());
                break;
            case "TRANSACTIONID":
                List<Payment> payments = paymentRepository.searchPayments(searchTemp.getCaseInput());
                paymentInvestigationCaseList = payments.stream().map(Payment::getRelatedInvestigationCase)
                        .collect(Collectors.toList());
                break;
        }
        if (paymentInvestigationCaseList.isEmpty()) {
            model.addAttribute("message", "There are no cases in your queue currently.");
        }
        if(Boolean.valueOf(searchTemp.getOpenedOn())) {
            List<String> dateList = Arrays.asList(searchTemp.getCaseRange().split("-"));
            String startDateString = dateList.get(0).trim() + " 00:00";
            String endDateString = dateList.get(1).trim() + " 23:59";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            LocalDateTime startDate = LocalDateTime.parse(startDateString, formatter);

            OffsetDateTime startDateOffset = OffsetDateTime.of(startDate, OffsetDateTime.now().getOffset());
            LocalDateTime endDate = LocalDateTime.parse(endDateString, formatter);
            OffsetDateTime endDateOffset = OffsetDateTime.of(endDate, OffsetDateTime.now().getOffset());

            paymentInvestigationCaseList = paymentInvestigationCaseList.stream()
                    .filter(investigationCase -> investigationCase.getDateCreated().isBefore(endDateOffset) && investigationCase.getDateCreated().isAfter(startDateOffset))
                    .collect(Collectors.toList());
        }

        log.info("The result payment cases search size is {}", paymentInvestigationCaseList.size());
        if(paymentInvestigationCaseList.size() > 0) {
            model.addAttribute("message", "Found the investigation cases");
        }
        model.addAttribute("paymentInvestigationCases", paymentInvestigationCaseList);

        model.addAttribute("caseFilterRange", getDateRange());
        model.addAttribute("formatter", formatter);
        return "resolution/home";
    }

    @GetMapping("/status/dashboard/fetch")
    @ResponseBody
    public InvestigationStatusDTO fetchInvestigationsStatus(HttpSession session) {
        String clientId = (String) session.getAttribute("loginAsId");
        if (StringUtils.isEmpty(clientId)) {
            clientId = CLIENT_ID;
        }
        return paymentInvestigationCaseService.getInvestigationStatusesByClientId(clientId).orElseGet(() -> InvestigationStatusDTO.builder().build());
    }
    @ResponseBody
    @GetMapping("/case/status/{investigationCaseId}")
    public List<CaseStatusTemp> getInvestigationCaseStatusViaAjax(@PathVariable String investigationCaseId) {
        log.info("The case id {} for searching statuses", investigationCaseId);
        List<InvestigationCaseStatus> investigationCaseStatuses = investigationCaseStatusRepository
                .findAllByInvestigationCase_id(investigationCaseId);

        return investigationCaseStatuses.stream()
                .map(this::mapperToStatus)
                .collect(Collectors.toList());
    }
    @ResponseBody
    @GetMapping("/case/resolution/{investigationCaseId}")
    public List<ResolutionTemp> getInvestigationCaseResolutionViaAjax(@PathVariable String investigationCaseId) {
        log.info("The case id {} for searching statuses", investigationCaseId);
        List<PaymentInvestigationCaseResolution> investigationCaseResolutions = paymentInvestigationCaseResolutionRepository
                .findAllByInvestigationCase_id(investigationCaseId);

        return investigationCaseResolutions.stream()
                .map(this::mapperToResolution)
                .collect(Collectors.toList());
    }

    private CaseStatusTemp mapperToStatus(InvestigationCaseStatus investigationCaseStatus) {
        CaseStatusTemp caseStatusTemp = new CaseStatusTemp();
        caseStatusTemp.setId(investigationCaseStatus.getId());
        caseStatusTemp.setCaseStatusCode(investigationCaseStatus.getCaseStatus().name());
        caseStatusTemp.setStatusDescription(investigationCaseStatus.getStatusDescription());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        caseStatusTemp.setDate(investigationCaseStatus.getDateCreated().format(formatter));

        return caseStatusTemp;
    }

    private ResolutionTemp mapperToResolution(PaymentInvestigationCaseResolution investigationCaseResolution) {
        ResolutionTemp resolutionTemp = new ResolutionTemp();
        resolutionTemp.setId(investigationCaseResolution.getId());
        resolutionTemp.setInvestigationStatus(investigationCaseResolution.getInvestigationStatus().name());
        resolutionTemp.setInvestigationCaseReference(investigationCaseResolution.getInvestigationCaseReference());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        resolutionTemp.setDate(investigationCaseResolution.getDateCreated().format(formatter));

        return resolutionTemp;
    }

    private String getCaseCreatorName(PaymentInvestigationCase paymentInvestigationCase){
        InvestigationPartyRole investigationPartyRole = null;
        Optional<InvestigationPartyRole> investigationPartyRoleOptional = paymentInvestigationCase.getInvestigationPartyRole()
                .stream().filter(investigationPartyRole1 -> investigationPartyRole1 instanceof CaseCreator).findFirst();
        if(investigationPartyRoleOptional.isPresent()){
            investigationPartyRole = investigationPartyRoleOptional.get();
        }

        Optional<RolePlayer> rolePlayerOptional = investigationPartyRole.getPlayer().stream().findFirst();
        String caseCreatorName = "";
        if(rolePlayerOptional.isPresent()){
            RolePlayer rolePlayer = rolePlayerOptional.get();
            if(rolePlayer instanceof Person){
                Person person = (Person) rolePlayer;
                Optional<PersonName> personNameOptional = person.getPersonIdentification().stream().flatMap(personIdentification ->
                        personIdentification.getPersonName().stream()).findFirst();
                if(personNameOptional.isPresent()){
                    caseCreatorName = personNameOptional.get().getBirthName() + " "
                            + personNameOptional.get().getMiddleName() + " " + personNameOptional.get().getGivenName();
                }
            }else {
                Organisation organisation = (Organisation) rolePlayer;
                Optional<OrganisationName> organisationNameOptional = organisation.getOrganisationIdentification().stream()
                        .flatMap(organisationIdentification -> organisationIdentification.getOrganisationName().stream()).findFirst();
                if(organisationNameOptional.isPresent()){
                    caseCreatorName = organisationNameOptional.get().getTradingName();
                }
            }
        }
        return caseCreatorName;
    }

    private BigDecimal getAmountToBeDebited(List<Payment> underlyingPayment){
        Optional<Payment> paymentOptional = underlyingPayment.stream().findFirst();
        BigDecimal amountToBeDebited = new BigDecimal(0);
        if(paymentOptional.isPresent()){
            amountToBeDebited = paymentOptional.get().getAmount().getAmount();
        }
        return amountToBeDebited;
    }

    @Data
    class CaseStatusTemp {
        String id;
        String caseStatusCode;
        String statusDescription;
        String date;
    }
    @Data
    class ResolutionTemp {
        String id;
        String investigationStatus;
        String investigationCaseReference;
        String date;
    }
    @Data
    class SearchTemp {
        String caseInput;
        String caseFilter;
        String caseRange;
        String openedOn;
    }

}
