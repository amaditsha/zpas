package zw.co.esolutions.zpas.controller.signatory;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.model.ProfileGroup;
import zw.co.esolutions.zpas.services.iface.signature.ProfileGroupService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityType;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping(path = "/college")
public class CollegeController {
    @Autowired
    private ProfileGroupService profileGroupService;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");



    @GetMapping(value = "/home")
    public String renderHome(Model model) {
        log.info("College home page GET request.");
//        model.addAttribute("entityTypes", EntityType.values());
        List<ProfileGroup> profileGroups = profileGroupService.getProfileGroups()
                .stream().filter(profileGroup -> !profileGroup.getEntityStatus().equals(EntityStatus.DELETED))
                .collect(Collectors.toList());
        model.addAttribute("profileGroups", profileGroups);
        model.addAttribute("profileGroup", new ProfileGroup());
        if(profileGroups.size() == 0) {
            Notification notification = new Notification();
            notification.setType("info");
            notification.setMessage("No colleges found.");
            model.addAttribute("notification", notification);
        }
        model.addAttribute("formatter", formatter);
        return "college/home_college";
    }



    @GetMapping
    public String renderProfileGroup(Model model) {
        log.info("College page GET request.");
        model.addAttribute("profileGroup", new ProfileGroup());
        model.addAttribute("entityTypes", EntityType.values());
        return "college/create_college";
    }

    @PostMapping
    public String saveProfileGroup(@Valid @ModelAttribute("profileGroup") ProfileGroup profileGroup, Errors errors, Model model) {
        log.info("College page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "college/home";
        }
        // Save the college design...

        log.info("College : {}", profileGroup);
        ProfileGroup persistedProfileGroup = profileGroupService.createProfileGroup(profileGroup);
        return "redirect:/college/home";
    }

    @GetMapping("/{id}")
    public String getProfileGroupById(@PathVariable("id") String id, Model model) {
        Optional<ProfileGroup> profileGroupOptional = profileGroupService.getProfileGroupById(id);
        return profileGroupOptional.map(profileGroup -> {
            model.addAttribute(profileGroup);


            return "home_college";
        }).orElseGet(() -> {
            Notification notification = new Notification();
                    notification.setType("error");
                    notification.setMessage("The college was not found.");
            model.addAttribute("notification", notification);
            return "home_college";
        });
    }

    @PostMapping("/edit")
    public ResponseEntity<?> updateProfileGroupViaAjax(@Valid @ModelAttribute("profileGroup") ProfileGroup profileGroup, Errors errors){
        log.info("Edit the college");
        log.info("Edit profile group | {}", profileGroup);
        AjaxResponseBody result = new AjaxResponseBody();

        Optional<ProfileGroup> oldProfileGroupOptional = profileGroupService.getProfileGroupById(profileGroup.getId());
        return oldProfileGroupOptional.map(newProfileGroup -> {
            newProfileGroup.setName(profileGroup.getName());

            ProfileGroup updatedProfileGroup = profileGroupService.updateProfileGroup(newProfileGroup);
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(updatedProfileGroup);

            return ResponseEntity.ok(result);
        }).orElseGet(() -> {
            result.setNarrative("The group can't be edited");
            result.setResponseCode("04");
            return ResponseEntity.ok(result);
        });
    }

    @GetMapping("/approve/{id}")
    public ResponseEntity<?> approveProfileGroupViaAjax(@PathVariable("id") String id){
        log.info("Approving the college");
        ProfileGroup profileGroup = profileGroupService.approveProfileGroup(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(profileGroup.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(profileGroup.getEntityStatus().name());
        } else {
            result.setNarrative("The group can't be approved");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
        }

    @GetMapping("/reject/{id}")
    public ResponseEntity<?> rejectProfileGroupViaAjax(@PathVariable("id") String id){
        log.info("Reject the college");
        ProfileGroup profileGroup = profileGroupService.rejectProfileGroup(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(profileGroup.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            result.setNarrative("Successfully rejected.");
            result.setResponseCode("00");
            result.setObject(profileGroup.getEntityStatus().name());
        } else {
            result.setNarrative("The group can't be rejected");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
        }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteProfileGroupViaAjax(@PathVariable("id") String id){
        log.info("Delete the college");
        ProfileGroup profileGroup = profileGroupService.deleteProfileGroup(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(profileGroup.getEntityStatus().equals(EntityStatus.DELETED)) {
            result.setNarrative("Successfully deleted.");
            result.setResponseCode("00");
            result.setObject(profileGroup.getEntityStatus().name());
        } else {
            result.setNarrative("The group can't be deleted");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }


    @Data
    class Notification {
        protected String message;
        protected String type;
    }

}
