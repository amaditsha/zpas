package zw.co.esolutions.zpas.controller.signatory;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.dto.agreement.SignatureConditionDTO;
import zw.co.esolutions.zpas.model.ProfileGroup;
import zw.co.esolutions.zpas.model.SignatureCondition;
import zw.co.esolutions.zpas.services.iface.agreement.SignatureConditionService;
import zw.co.esolutions.zpas.services.iface.signature.ProfileGroupService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityType;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping(path = "/signature-condition")
public class SignatureConditionController {
    @Autowired
    private SignatureConditionService signatureConditionService;
    @Autowired
    private ProfileGroupService profileGroupService;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");


    @GetMapping(value = "/home")
    public String renderHome(Model model) {
        log.info("Signature condition home page GET request.");
//        model.addAttribute("entityTypes", EntityType.values());
        return "signature/home_signaturecondition";
    }


    @GetMapping
    public String renderSignatureCondition(Model model) {
        log.info("Signature condition page GET request.");
        model.addAttribute("signatureCondition", new SignatureConditionDTO());
        model.addAttribute("entityTypes", EntityType.values());
        return "signature/create_signaturecondition";
    }

    @PostMapping
    public String saveSignatureCondition(@Valid @ModelAttribute("signatureCondition") SignatureConditionDTO signatureCondition, Errors errors, Model model) {
        log.info("Signature condition page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "signature/create_signaturecondition";
        }
        // Save the signature condition design...

        log.info("Signature condition : {}", signatureCondition);
        SignatureCondition persistedSignatureCondition = signatureConditionService.createSignatureCondition(signatureCondition);
        return "redirect:/signature-condition/" + persistedSignatureCondition.getId();
    }

    @GetMapping("/{id}")
    public String getSignatureConditionById(@PathVariable("id") String id, Model model) {
        Optional<SignatureCondition> signatureConditionOptional = signatureConditionService.getSignatureConditionById(id);
        model.addAttribute("title", "View Signature Condition");
        return signatureConditionOptional.map(signatureCondition -> {
            model.addAttribute(signatureCondition);
            model.addAttribute("formatter", formatter);
            model.addAttribute("moneyUtil", new MoneyUtil());


            List<ProfileGroup> profileGroups = profileGroupService.getProfileGroups();
            model.addAttribute("profileGroups", profileGroups);
            HashMap<String, ProfileGroup> profileGroupHashMap = new HashMap<>();
            profileGroups.forEach(profileGroup -> {
                profileGroupHashMap.put(profileGroup.getId(), profileGroup);
            });

            model.addAttribute("profileGroupMap", profileGroupHashMap);


            return "signature/view_signaturecondition";
        }).orElseGet(() -> {
            Notification notification = new Notification();
            notification.setType("error");
            notification.setMessage("The signature condition was not found.");
            model.addAttribute("notification", notification);
            return "signature/view_signaturecondition";
        });
    }

    @GetMapping("edit/{id}")
    public String renderUpdateRule(@PathVariable("id") String id, Model model) {
        log.info("Update signature condition page request.");
        model.addAttribute("title", "Create signature condition");
        Optional<SignatureCondition> signatureConditionOptional = signatureConditionService.getSignatureConditionById(id);
        return signatureConditionOptional.map(signatureCondition -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            model.addAttribute("formatter", formatter);
            model.addAttribute("signatureCondition", signatureCondition);
            model.addAttribute("moneyUtil", new MoneyUtil());
            model.addAttribute("entityTypes", EntityType.values());
            return "signature/edit_signaturecondition";
        }).orElseGet(() -> {
            return "signature/home";
        });
    }

    @PostMapping("/edit")
    public String updateSignatureCondition(@Valid @ModelAttribute("signatureCondition") SignatureConditionDTO signatureCondition, Errors errors, Model model) {
        log.info("Signature condition page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the rule.");
            return "signature/home";
        }
        log.info("Updating the signature condition | {}", signatureCondition);
        SignatureCondition updatedSignatureCondition = signatureConditionService.updateSignatureCondition(signatureCondition);
        return "redirect:/signature-condition/" + updatedSignatureCondition.getId();
    }

    @PostMapping("/edit/ajax")
    public ResponseEntity<?> updateSignatureConditionViaAjax(@Valid @ModelAttribute("signatureCondition") SignatureConditionDTO formSignatureCondition, Errors errors, Model model) {
        log.info("Signature condition page POST request. \n {}", formSignatureCondition);

        AjaxResponseBody result = new AjaxResponseBody();

        SignatureCondition updatedSignatureCondition = signatureConditionService.updateSignatureCondition(formSignatureCondition);

        if (updatedSignatureCondition != null && updatedSignatureCondition.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
            result.setNarrative("Successfully updated.");
            result.setResponseCode("00");
            SignatureConditionTemp signatureConditionTemp = new SignatureConditionTemp();
            signatureConditionTemp.setRequiredSignatureNumber(updatedSignatureCondition.getRequiredSignatureNumber().intValue());
            signatureConditionTemp.setSignatoryRightIndicator(updatedSignatureCondition.getSignatoryRightIndicator());
            signatureConditionTemp.setSignatureOrderIndicator(updatedSignatureCondition.getSignatureOrderIndicator());
            signatureConditionTemp.setSignatureOrder(updatedSignatureCondition.getSignatureOrder());

            result.setObject(signatureConditionTemp);
        } else {
            result.setNarrative("The signature condition can't be edited");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);

    }
    @Data
    class SignatureConditionTemp {
        protected Integer requiredSignatureNumber;
        protected Boolean signatoryRightIndicator;
        protected Boolean signatureOrderIndicator;
        protected String signatureOrder;
    }

    @GetMapping("/approve/{id}")
    public ResponseEntity<?> approveSignatureConditionViaAjax(@PathVariable("id") String id) {
        log.info("Approving the college");
        SignatureCondition signatureCondition = signatureConditionService.approveSignatureCondition(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (signatureCondition.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(signatureCondition.getEntityStatus().name());
        } else {
            result.setNarrative("The signature condition can't be approved");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/reject/{id}")
    public ResponseEntity<?> rejectSignatureConditionViaAjax(@PathVariable("id") String id) {
        log.info("Reject the college");
        SignatureCondition signatureCondition = signatureConditionService.rejectSignatureCondition(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (signatureCondition.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            result.setNarrative("Successfully rejected.");
            result.setResponseCode("00");
            result.setObject(signatureCondition.getEntityStatus().name());
        } else {
            result.setNarrative("The signature condition can't be rejected");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteSignatureConditionViaAjax(@PathVariable("id") String id) {
        log.info("Delete the college");
        SignatureCondition signatureCondition = signatureConditionService.deleteSignatureCondition(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if (signatureCondition.getEntityStatus().equals(EntityStatus.DELETED)) {
            result.setNarrative("Successfully deleted.");
            result.setResponseCode("00");
            result.setObject(signatureCondition.getEntityStatus().name());
        } else {
            result.setNarrative("The signature condition can't be deleted");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }


    @GetMapping("/all")
    public String viewSignatureConditions(Model model) {
        List<SignatureCondition> signatureConditions = signatureConditionService.getSignatureConditions();
        model.addAttribute("signatureConditions", signatureConditions);
        Notification notification = new Notification();
        if (signatureConditions == null || signatureConditions.size() == 0) {
            notification.setType("error");
            notification.setMessage("The signature conditions was not found.");
        }
        model.addAttribute("notification", notification);
        model.addAttribute("formatter", formatter);
        model.addAttribute("moneyUtil", new MoneyUtil());
        return "signature/view_signatureconditions";
    }


/*    @PostMapping("/ruleitem/create")
    public ResponseEntity<?> createRuleItemViaAjax(@Valid @ModelAttribute("ruleItem") RuleItem ruleItem, Errors errors){
        log.info("Create the rule item");
        log.info("create rule item | {}", ruleItem);
        AjaxResponseBody result = new AjaxResponseBody();

        Optional<SignatureCondition> signatureConditionOptional = signatureConditionService.getSignatureConditionById(ruleItem.getSignatureConditionId());
        return signatureConditionOptional.map(signatureCondition -> {
            ruleItem.setSignatureCondition(signatureCondition);
            RuleItem item = ruleItemService.createRuleItem(ruleItem);

            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(item);
            return ResponseEntity.ok(result);
        }).orElseGet(() -> {
            result.setNarrative("Failed to create rule item.");
            result.setResponseCode("04");
            return ResponseEntity.ok(result);
        });

    }*/


/*    @GetMapping("/ruleitem/delete/{id}")
    public ResponseEntity<?> deleteRuleItemViaAjax(@PathVariable("id") String id){
        log.info("Delete the rule item");
        RuleItem ruleItem = ruleItemService.deleteRuleItem(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(ruleItem.getEntityStatus().equals(EntityStatus.DELETED)) {
            result.setNarrative("Successfully deleted.");
            result.setResponseCode("00");
            result.setObject(ruleItem);
        } else {
            result.setNarrative("The rule item can't be deleted");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }*/


    @Data
    class Notification {
        protected String message;
        protected String type;
    }

}
