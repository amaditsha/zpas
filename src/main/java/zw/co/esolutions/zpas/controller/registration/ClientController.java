package zw.co.esolutions.zpas.controller.registration;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchResponseDTO;
import zw.co.esolutions.zpas.dto.profile.ProfileDTO;
import zw.co.esolutions.zpas.dto.registration.ClientCreationDTO;
import zw.co.esolutions.zpas.dto.registration.EmployerDetailsDTO;
import zw.co.esolutions.zpas.dto.registration.EmployingPartyRolePersonRequestDTO;
import zw.co.esolutions.zpas.dto.registration.OrganisationDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.model.PersonName;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.security.services.iface.userrole.UserRoleService;
import zw.co.esolutions.zpas.services.PersonSearchService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.services.iface.country.CountryService;
import zw.co.esolutions.zpas.services.iface.organisations.OrganisationService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.iface.restapi.RestApiService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.RoleLevel;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.management.relation.Role;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class ClientController {
    @Autowired
    private RestApiService jsonApiService;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private PersonSearchService personSearchService;

    @Autowired
    private OrganisationService organisationService;

    @Autowired
    private PersonNameRepository personNameRepository;

    @Autowired
    private PersonIdentificationRepository personIdentificationRepository;

    @Autowired
    private PersonProfileRepository personProfileRepository;

    @Autowired
    private PostalAddressRepository postalAddressRepository;

    @Autowired
    CountryService countryService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    NotificationConfigService notificationConfigService;

    @Autowired
    EmployingPartyRoleRepository employingPartyRoleRepository;

    @GetMapping("/clients/create-page")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CLIENT')")
    public String createClientPage(ClientCreationDTO clientCreationDTO, Model model){
        log.info("Client page GET request.");

        final List<Organisation> organisations = organisationService.getAllOrganisations();
        final List<Country> countries = countryService.getAllCountries();

        model.addAttribute("namePrefixes", NamePrefix1Code.values());
        model.addAttribute("addressTypes", AddressTypeCode.values());
        model.addAttribute("organisations", organisations);
        model.addAttribute("genders", GenderCode.values());
        model.addAttribute("roles", getRoles(RoleLevel.CLIENT));
        model.addAttribute("languages", LanguageCode.values());
        model.addAttribute("residentialStatuses", ResidentialStatusCode.values());
        model.addAttribute("civilStatuses", CivilStatusCode.values());
        model.addAttribute("namePrefixes", NamePrefix1Code.values());
        model.addAttribute("partyType", PartyTypeCode.Client.name());
        model.addAttribute("typeOfIdentifications", TypeOfIdentificationCode.values());
        model.addAttribute("countries", countries);
        return "registration/create_client";
    }

    private List<UserRole> getRoles(RoleLevel roleLevel) {
        return userRoleService.getAllUserRoles().stream().filter(userRole -> {
            return userRole.getRoleLevel().getWeight() <= roleLevel.getWeight();
        }).collect(Collectors.toList());
    }

    @GetMapping("/clients/details/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_CLIENT')")
    public String viewPersonById(Model model, @PathVariable(name = "id") String id) {
        final Optional<Person> personOptional = clientService.getPersonById(id);
        return personOptional.map(person -> {
            model.addAttribute("person", person);
            model.addAttribute("personName", new PersonName());
            model.addAttribute("personIdentification", new PersonIdentification());
            model.addAttribute("personProfile", new PersonProfile());
            model.addAttribute("title", "View Client");
            model.addAttribute("genders", GenderCode.values());

            return "registration/view_client";
        }).orElseGet(() -> {
            model.addAttribute("person", null);
            return "registration/view_client";
        });
    }

    @PostMapping("/clients")
    @PreAuthorize("hasAuthority('ROLE_CREATE_CLIENT')")
    public String createClient(@Valid ClientCreationDTO clientCreationDTO, BindingResult bindingResult, Model model, HttpSession session){

        model.addAttribute("title", "Clients");

        String username = (String) session.getAttribute("username");
        String financialInstitutionId = (String) session.getAttribute("financialInstitutionId");
        log.info("Client name found: " + username);
        clientCreationDTO.setAuditUsername(username);
        clientCreationDTO.setRegistrationFI(financialInstitutionId);

        if (bindingResult.hasErrors()) {
            return "registration/create_client";
        }

        final Person personCreated = clientService.createPerson(clientCreationDTO);

//        log.info("Generate notification configs for user with party id {}", personCreated.getId());
//        notificationConfigService.findAllByPartyId(personCreated.getId());

        return "redirect:clients/details/" + personCreated.getId();


    }

    @PostMapping("/clients/update")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CLIENT')")
    public String updateClient(@Valid ClientCreationDTO clientUpdateDTO, BindingResult bindingResult, Model model){
        log.info("In update client controller..");
        if (bindingResult.hasErrors()) {
            return "registration/update_client";
        }

        log.info("The person object is: " + clientUpdateDTO);

        String id = clientUpdateDTO.getId();
        log.info("The person id is: " + id);

       // Person person = new Person();

        final Person personUpdated = clientService.updatePerson(clientUpdateDTO);
        log.info("Done updating about to return to view");
        log.info("The person id is {}" + personUpdated.getId());

        return "redirect:/clients/details/" + personUpdated.getId();


    }



    @GetMapping("/clients/update/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CLIENT')")
    public String showClientEditPage(@PathVariable(name = "id") String id, Model model) {
        log.info("Update client page request.");

        final Optional<Person> personOptional = clientService.getPersonById(id);


        return personOptional.map(person -> {
            ClientCreationDTO clientCreationDTO = generateClientCreationDTOFromPerson(person);
            log.info("The person object is:" + person);
            log.info("The name prefix is:" + clientCreationDTO.getNamePrefix());
            model.addAttribute("clientCreationDTO", clientCreationDTO);
            model.addAttribute("title", "Update Client");
            model.addAttribute("addressTypes", AddressTypeCode.values());
            model.addAttribute("genders", GenderCode.values());
            model.addAttribute("languages", LanguageCode.values());
            model.addAttribute("civilStatuses", CivilStatusCode.values());
            model.addAttribute("namePrefixes", NamePrefix1Code.values());
            return "registration/update_client";
        }).orElseGet(() -> {
            model.addAttribute("person", null);
            return "registration/view_client";
        });
    }

    public ClientCreationDTO generateClientCreationDTOFromPerson(Person person) {
        ClientCreationDTO clientCreationDTO = new ClientCreationDTO();

        clientCreationDTO.setId(person.getId());

        clientCreationDTO.setGender(person.getGender().name());

        clientCreationDTO.setLanguage(person.getLanguage().getName());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        clientCreationDTO.setBirthDate(person.getBirthDate().format(formatter));

        clientCreationDTO.setProfession(person.getProfession());

        clientCreationDTO.setNationality(person.getNationality().toString());

        clientCreationDTO.setBusinessFunctionTitle(person.getBusinessFunctionTitle());

        clientCreationDTO.setEmployingParty(person.getEmployingParty().toString());

        clientCreationDTO.setContactPersonRole(person.getContactPersonRole() == null? "": person.getContactPersonRole().getId());

        clientCreationDTO.setCivilStatus(person.getCivilStatus() == null? "": person.getCivilStatus().name());

        clientCreationDTO.setCitizenshipStartDate(person.getCitizenshipStartDate().format(formatter));
        clientCreationDTO.setCitizenshipEndDate(person.getCitizenshipEndDate().format(formatter));

        clientCreationDTO.setStreetName(person.getPlaceOfBirth().getAddress().get(0).getStreetName());
        clientCreationDTO.setStreetBuildingIdentification(person.getPlaceOfBirth().getAddress().get(0).getStreetBuildingIdentification());
        clientCreationDTO.setPostCodeIdentification(person.getPlaceOfBirth().getAddress().get(0).getPostCodeIdentification());
        clientCreationDTO.setTownName(person.getPlaceOfBirth().getAddress().get(0).getTownName());
        clientCreationDTO.setState(person.getPlaceOfBirth().getAddress().get(0).getState());
        clientCreationDTO.setBuildingName(person.getPlaceOfBirth().getAddress().get(0).getBuildingName());
        clientCreationDTO.setFloor(person.getPlaceOfBirth().getAddress().get(0).getFloor());
        clientCreationDTO.setDistrictName(person.getPlaceOfBirth().getAddress().get(0).getDistrictName());
        clientCreationDTO.setRegionIdentification(person.getPlaceOfBirth().getAddress().get(0).getRegionIdentification());
        clientCreationDTO.setCountryIdentification(person.getPlaceOfBirth().getAddress().get(0).getCountyIdentification());
        clientCreationDTO.setPostOfficeBox(person.getPlaceOfBirth().getAddress().get(0).getPostOfficeBox());
        clientCreationDTO.setProvince(person.getPlaceOfBirth().getAddress().get(0).getProvince());
        clientCreationDTO.setDepartment(person.getPlaceOfBirth().getAddress().get(0).getDepartment());
        clientCreationDTO.setSubDepartment(person.getPlaceOfBirth().getAddress().get(0).getSubDepartment());
        clientCreationDTO.setSuiteIdentification(person.getPlaceOfBirth().getAddress().get(0).getSuiteIdentification());
        clientCreationDTO.setBuildingIdentification(person.getPlaceOfBirth().getAddress().get(0).getBuildingIdentification());
        clientCreationDTO.setMailDeliverySubLocation(person.getPlaceOfBirth().getAddress().get(0).getMailDeliverySubLocation());
        clientCreationDTO.setBlock(person.getPlaceOfBirth().getAddress().get(0).getBlock());
        clientCreationDTO.setLot(person.getPlaceOfBirth().getAddress().get(0).getLot());
        clientCreationDTO.setDistrictSubDivisionIdentification(person.getPlaceOfBirth().getAddress().get(0).getDistrictSubDivisionIdentification());

        clientCreationDTO.setSourceOfWealth(person.getPersonProfile().getSourceOfWealth());
        clientCreationDTO.setSalaryRange(person.getPersonProfile().getSalaryRange());
        clientCreationDTO.setEmployeeTerminationIndicator(String.valueOf(person.getPersonProfile().getEmployeeTerminationIndicator()));

        clientCreationDTO.setSocialSecurityNumber(person.getPersonIdentification().get(0).getSocialSecurityNumber());
        clientCreationDTO.setDriversLicenseNumber(person.getPersonIdentification().get(0).getDriversLicenseNumber());
        clientCreationDTO.setAlienRegistrationNumber(person.getPersonIdentification().get(0).getAlienRegistrationNumber());
        clientCreationDTO.setPassportNumber(person.getPersonIdentification().get(0).getPassportNumber());
        clientCreationDTO.setIdentityCardNumber(person.getPersonIdentification().get(0).getIdentityCardNumber());
        clientCreationDTO.setEmployerIdentificationNumber(person.getPersonIdentification().get(0).getEmployerIdentificationNumber());

        clientCreationDTO.setBirthName(person.getPersonIdentification().get(0).getPersonName().get(0).getBirthName());

        clientCreationDTO.setNamePrefix(person.getPersonIdentification().get(0).getPersonName().get(0).getNamePrefix().name());
        clientCreationDTO.setGivenName(person.getPersonIdentification().get(0).getPersonName().get(0).getGivenName());
        clientCreationDTO.setMiddleName(person.getPersonIdentification().get(0).getPersonName().get(0).getMiddleName());
        clientCreationDTO.setNameSuffix(person.getPersonIdentification().get(0).getPersonName().get(0).getNameSuffix());



        return clientCreationDTO;
    }

    @RequestMapping(value = "/clients/search", method = RequestMethod.GET)
    public String searchPerson(@RequestParam(value = "search", required = false) String q, Model model, String fiId) {
        List<Person> searchResults = null;
        log.info("Search string: " + q);
        try {
//            clientService.createPerson(clientCreationDTO);
            searchResults = personSearchService.searchPerson(q, fiId);

        } catch (Exception ex) {
            // here you should handle unexpected errors
            // ...
            // throw ex;
            searchResults = new ArrayList<>();
        }
        log.info("Search results are: {}", searchResults);
        clientService.refreshPersonIndex();
        model.addAttribute("search", searchResults);
        return "registration/search_client";

    }

    @ResponseBody
    @GetMapping("/clients/search/persons/")
    public List<PersonSearchResponseDTO> getPersonsViaAjax(@RequestParam("search") String search) {
        log.info("Get persons by search name {}", search);

        PersonSearchDTO personSearchDTO = PersonSearchDTO.builder()
                .search(search)
                .build();
        List<PersonSearchResponseDTO> searchResponseDTOS = jsonApiService.searchAllPersons(personSearchDTO);
        return searchResponseDTOS;
    }


    @RequestMapping(value = "/clients/view/all", method = RequestMethod.GET)
    public String viewAllPersons(Model model){
        List<Person> persons = clientService.getPersons();
        model.addAttribute("persons", persons);
        return "registration/view_clients";
    }

    @GetMapping("/clients/approve/{id}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_CLIENT')")
    public String approvePerson(Model model, @PathVariable(name = "id") String id) {
        log.info("Approving person: " + id);
        final Person person = clientService.approvePerson(id);
        model.addAttribute("person", person);
        return "redirect:/clients/details/" + person.getId();
    }

    @GetMapping("/clients/reject/{id}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_CLIENT')")
    public String rejectPerson(Model model, @PathVariable(name = "id") String id) {
        log.info("Rejecting person: " + id);
        final Person person = clientService.rejectPerson(id);
        model.addAttribute("person", person);
        return "redirect:/clients/details/" + person.getId();
    }

    @GetMapping("/clients/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_CLIENT')")
    public String deletePerson(Model model, @PathVariable(name = "id") String id) {
        log.info("Deleting person: " + id);
        final Person person = clientService.deletePerson(id);
        model.addAttribute("person", person);
        return "redirect:/clients/details/" + person.getId();
    }

    /*@GetMapping("/my-accounts")
    public String getMyAccounts(Model model, HttpSession session){
        String clientId = (String) session.getAttribute("clientId");
        log.info("clientId: " + clientId);

        List<Account> accounts = accountService.getAccountsByClientId(clientId);
        List<AccountTemp> accountTemps = new ArrayList<>();

        if(getAccountTemps(accounts) != null && !getAccountTemps(accounts).isEmpty()){
            accountTemps = getAccountTemps(accounts);
        }

        model.addAttribute("accounts", accountTemps);

        return "account/my_accounts";
    }*/

    @GetMapping("/clients/profile")
    public String getPersonProfile(Model model, HttpSession session){
        String clientId = (String) session.getAttribute("clientId");
        log.info("clientId: " + clientId);
        final Person person = clientService.findPersonById(clientId);
        model.addAttribute("persons", person);
        return "registration/person_profile";
    }


    @PostMapping("/clients/edit/person/name/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CLIENT')")
    public ResponseEntity<?> editPersonNameDetails(@ModelAttribute("personName") PersonName formPersonName, Errors errors){
        log.info("Edit the person name details in controller | " + formPersonName.getId() + " name " + formPersonName.getBirthName());

        Optional<PersonName> personNameOptional = personNameRepository.findById(formPersonName.getId());
        log.info("Person Name details found? : {} " + personNameOptional);

        AjaxResponseBody result = new AjaxResponseBody();

        personNameOptional.ifPresent(personName -> {

            /**
             * add the details to be edited to the person name
             * */

            personName.setBirthName(formPersonName.getBirthName());
            personName.setGivenName(formPersonName.getGivenName());
            personName.setMiddleName(formPersonName.getMiddleName());
            //personName.setNamePrefix(personName.getNamePrefix());
            personName.setNameSuffix(formPersonName.getNameSuffix());
            log.info("Birth Name before updating: {} " + personName.getBirthName());

            PersonName updatedPersonName = clientService.updatePersonName(personName);
            log.info("Birth Name after updating: {} " + updatedPersonName.getBirthName());

            if(updatedPersonName.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                PersonNameTemp personNameTemp = new PersonNameTemp();
                personNameTemp.setBirthName(updatedPersonName.getBirthName());
                personNameTemp.setId(updatedPersonName.getId());
                personNameTemp.setGivenName(updatedPersonName.getGivenName());
                personNameTemp.setMiddleName(updatedPersonName.getMiddleName());
                personNameTemp.setNameSuffix(updatedPersonName.getNameSuffix());

                result.setObject(personNameTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The person name can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }

    @PostMapping("/clients/edit/person/identification/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CLIENT')")
    public ResponseEntity<?> editPersonIdentificationDetails(@ModelAttribute("personIdentification") PersonIdentification formPersonIdentification, Errors errors){
        log.info("Edit the person identification details in controller | " + formPersonIdentification.getId());

        Optional<PersonIdentification> personIdentificationOptional = personIdentificationRepository.findById(formPersonIdentification.getId());
        log.info("Person Identification details found? : {} ", personIdentificationOptional);

        AjaxResponseBody result = new AjaxResponseBody();

        personIdentificationOptional.ifPresent(personIdentification -> {
            log.info("Setting old value {} " + personIdentification.getIdentityCardNumber() + " to -> new value {} " + formPersonIdentification.getIdentityCardNumber());
            personIdentification.setIdentityCardNumber(formPersonIdentification.getIdentityCardNumber());
            personIdentification.setPassportNumber(formPersonIdentification.getPassportNumber());
            personIdentification.setAlienRegistrationNumber(formPersonIdentification.getAlienRegistrationNumber());
            personIdentification.setDriversLicenseNumber(formPersonIdentification.getDriversLicenseNumber());
            personIdentification.setEmployerIdentificationNumber(formPersonIdentification.getEmployerIdentificationNumber());
            personIdentification.setSocialSecurityNumber(formPersonIdentification.getSocialSecurityNumber());

            PersonIdentification updatedPersonIdentification = clientService.updatePersonIdentification(personIdentification);
            log.info("National ID after updating: {} " + updatedPersonIdentification.getIdentityCardNumber());

            if(updatedPersonIdentification.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                PersonIdentificationTemp personIdentificationTemp = new PersonIdentificationTemp();
                personIdentificationTemp.setId(updatedPersonIdentification.getId());
                personIdentificationTemp.setIdentityCardNumber(updatedPersonIdentification.getIdentityCardNumber());
                personIdentificationTemp.setAlienRegistrationNumber(updatedPersonIdentification.getAlienRegistrationNumber());
                personIdentificationTemp.setDriversLicenseNumber(updatedPersonIdentification.getDriversLicenseNumber());
                personIdentificationTemp.setPassportNumber(updatedPersonIdentification.getPassportNumber());
                personIdentificationTemp.setEmployerIdentificationNumber(updatedPersonIdentification.getEmployerIdentificationNumber());
                personIdentificationTemp.setSocialSecurityNumber(updatedPersonIdentification.getSocialSecurityNumber());

                result.setObject(personIdentificationTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The person identification can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }

    @PostMapping("/clients/edit/person/profile/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CLIENT')")
    public ResponseEntity<?> editPersonProfileDetails(@ModelAttribute("personProfile") PersonProfile formPersonProfile){
        log.info("Editing person profile details in controller | " + formPersonProfile.getId());

        Optional<PersonProfile> personProfileOptional = personProfileRepository.findById(formPersonProfile.getId());
        log.info("Person profile details found? : {} ", personProfileOptional.isPresent());

        AjaxResponseBody result = new AjaxResponseBody();

        personProfileOptional.ifPresent(personProfile -> {
            log.info("Setting old value {} " + personProfile.getSourceOfWealth() + " to -> new value {} " + formPersonProfile.getSourceOfWealth());
            personProfile.setSourceOfWealth(formPersonProfile.getSourceOfWealth());
            personProfile.setSalaryRange(formPersonProfile.getSalaryRange());
            personProfile.setEmployeeTerminationIndicator(formPersonProfile.getEmployeeTerminationIndicator());

            PersonProfile updatedPersonProfile = clientService.updatePersonProfile(personProfile);
            log.info("Source of wealth after updating: {} " + updatedPersonProfile.getSourceOfWealth());

            if(updatedPersonProfile.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                PersonProfileTemp personProfileTemp = new PersonProfileTemp();
                personProfileTemp.setId(updatedPersonProfile.getId());
                personProfileTemp.setSourceOfWealth(updatedPersonProfile.getSourceOfWealth());
                personProfileTemp.setSalaryRange(updatedPersonProfile.getSalaryRange());
                personProfileTemp.setEmployeeTerminationIndicator(updatedPersonProfile.getEmployeeTerminationIndicator());

                result.setObject(personProfileTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The person profile can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }

    @PostMapping("/clients/edit/place/of/birth/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CLIENT')")
    public ResponseEntity<?> editPlaceOfBirthDetails(@ModelAttribute("placeOfBirth") PostalAddress formPlaceOfBirth, Errors errors){
        log.info("Editing place of birth details in controller | " + formPlaceOfBirth.getId());

        Optional<PostalAddress> postalAddressOptional = postalAddressRepository.findById(formPlaceOfBirth.getId());
        log.info("Person place of birth details found? : {} ", postalAddressOptional);

        AjaxResponseBody result = new AjaxResponseBody();

        postalAddressOptional.ifPresent(postalAddress -> {

            log.info("Setting old value {} " + postalAddress.getBuildingName() + " to -> new value {} " + formPlaceOfBirth.getBuildingName());
            postalAddress.setBuildingName(formPlaceOfBirth.getBuildingName());
            postalAddress.setStreetName(formPlaceOfBirth.getStreetName());
            postalAddress.setTownName(formPlaceOfBirth.getTownName());
            postalAddress.setDistrictName(formPlaceOfBirth.getDistrictName());
            postalAddress.setRegionIdentification(formPlaceOfBirth.getRegionIdentification());
            postalAddress.setProvince(formPlaceOfBirth.getProvince());
            postalAddress.setState(formPlaceOfBirth.getState());
            postalAddress.setPostOfficeBox(formPlaceOfBirth.getPostOfficeBox());


            PostalAddress updatedPostalAddress = clientService.updatePlaceOfBirth(postalAddress);
            log.info("Building name after updating: {} " + updatedPostalAddress.getBuildingName());

            if(updatedPostalAddress.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                PlaceOfBirthTemp placeOfBirthTemp = new PlaceOfBirthTemp();
                placeOfBirthTemp.setId(updatedPostalAddress.getId());
                placeOfBirthTemp.setBuildingName(updatedPostalAddress.getBuildingName());
                placeOfBirthTemp.setStreetName(updatedPostalAddress.getStreetName());
                placeOfBirthTemp.setTownName(updatedPostalAddress.getTownName());
                placeOfBirthTemp.setDistrictName(updatedPostalAddress.getDistrictName());
                placeOfBirthTemp.setRegionIdentification(updatedPostalAddress.getRegionIdentification());
                placeOfBirthTemp.setProvince(updatedPostalAddress.getProvince());
                placeOfBirthTemp.setState(updatedPostalAddress.getState());
                placeOfBirthTemp.setPostOfficeBox(updatedPostalAddress.getPostOfficeBox());

                result.setObject(placeOfBirthTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The place of birth can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }

    @PostMapping("/clients/edit/general/information/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_CLIENT')")
    public ResponseEntity<?> editGeneralInformationDetails(@ModelAttribute("person") Person formPerson, Errors errors){
        log.info("Editing person general details in controller | " + formPerson.getId());

        Optional<Person> personOptional = personRepository.findById(formPerson.getId());
        log.info("Person general details found? : {} " + personOptional);

        AjaxResponseBody result = new AjaxResponseBody();

        personOptional.ifPresent(person -> {

            log.info("Setting old value {} " + person.getGender() + " to -> new value {} " + formPerson.getGender());
            person.setGender(formPerson.getGender());
            person.setLanguage(formPerson.getLanguage());
            person.setBirthDate(formPerson.getBirthDate());
            person.setProfession(formPerson.getProfession());
            person.setBusinessFunctionTitle(formPerson.getBusinessFunctionTitle());
            person.setEmployingParty(formPerson.getEmployingParty());
            person.setCivilStatus(formPerson.getCivilStatus());
            person.setCitizenshipStartDate(formPerson.getCitizenshipStartDate());
            person.setCitizenshipEndDate(formPerson.getCitizenshipEndDate());

            Person updatedPerson = clientService.updateGeneralInformation(person);
            log.info("Gender after updating: {} " + updatedPerson.getGender());

            if(updatedPerson.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                GeneralnformationTemp generalnformationTemp = new GeneralnformationTemp();
                generalnformationTemp.setId(updatedPerson.getId());
                generalnformationTemp.setGender(updatedPerson.getGender().name());
                generalnformationTemp.setLanguage(updatedPerson.getLanguage().name());
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
                generalnformationTemp.setBirthDate(updatedPerson.getBirthDate().format(formatter));
                generalnformationTemp.setProfession(updatedPerson.getProfession());
                generalnformationTemp.setBusinessFunctionTitle(updatedPerson.getBusinessFunctionTitle());
                generalnformationTemp.setEmployingParty(updatedPerson.getEmployingParty().getEmployee().get(0).toString());
                generalnformationTemp.setCivilStatus(updatedPerson.getCivilStatus().name());
                generalnformationTemp.setCitizenshipStartDate(updatedPerson.getCitizenshipStartDate().format(formatter));
                generalnformationTemp.setCitizenshipEndDate(updatedPerson.getCitizenshipEndDate().format(formatter));

                result.setObject(generalnformationTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The general information can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }

    @Data
    public class PersonNameTemp{
        protected String id;
        protected String birthName;
        protected String givenName;
        protected String middleName;
        protected String nameSuffix;
    }

    @Data
    public class PersonIdentificationTemp{
        protected String id;
        protected String identityCardNumber;
        protected String socialSecurityNumber;
        protected String driversLicenseNumber;
        protected String alienRegistrationNumber;
        protected String passportNumber;
        protected String employerIdentificationNumber;
    }

    @Data
    public class PersonProfileTemp{
        protected String id;
        protected String sourceOfWealth;
        protected String salaryRange;
        protected Boolean employeeTerminationIndicator;

    }

    @Data
    public class PlaceOfBirthTemp{
        protected String id;
        protected String addressType;
        protected String streetName;
        protected String streetBuildingIdentification;
        protected String postCodeIdentification;
        protected String townName;
        protected String state;
        protected String buildingName;
        protected String floor;
        protected String districtName;
        protected String regionIdentification;
        protected String countryIdentification;
        protected String postOfficeBox;
        protected String province;
        protected String department;
        protected String subDepartment;
        protected String suiteIdentification;
        protected String buildingIdentification;
        protected String mailDeliverySubLocation;
        protected String block;
        protected String districtSubDivisionIdentification;
        protected String lot;
        protected String country;
    }

    @Data
    public class GeneralnformationTemp{
        protected String id;
        protected String gender;
        protected String language;
        protected String birthDate;
        protected String profession;
        protected String nationality;
        protected String businessFunctionTitle;
        protected String employingParty;
        protected String contactPersonRole;
        protected String civilStatus;
        protected String deathDate;
        protected String citizenshipEndDate;
        protected String citizenshipStartDate;

    }

    @ResponseBody
    @GetMapping("/clients/search/employers/")
    public List<OrganisationDTO> getOrganisationViaAjax(@RequestParam("search") String search) {
        log.info("Get employer by search name {}", search);
        final EmployerDetailsDTO employerDetailsDTO = EmployerDetailsDTO.builder().search(search).build();
        List<OrganisationDTO> organisationDTOS = organisationService.searchOrganisation(employerDetailsDTO);
        log.info("The list of organisations, {}", organisationDTOS);
        return organisationDTOS;
    }
    @Data
    public class ValidateIdentityCardNumberResponse {
        protected String identityCardNumber;
        protected String message;
        protected String code;

        protected String firstName;
        protected String givenName;
        protected String fullName;
        protected String personId;
    }


    @ResponseBody
    @GetMapping("/clients/validate/{identityCardNumber}")
    public ValidateIdentityCardNumberResponse validateIdentityCardNumber(Model model, @PathVariable("identityCardNumber") String identityCardNumber) {
        final boolean identityCardNumberAlreadyInUse = clientService.isIdentityCardNumberAlreadyInUse(identityCardNumber);
        ValidateIdentityCardNumberResponse validateIdentityCardNumberResponse = new ValidateIdentityCardNumberResponse();
        validateIdentityCardNumberResponse.setIdentityCardNumber(identityCardNumber);
        final PersonIdentification personIdentificationByIdentityCardNumber = personIdentificationRepository.findPersonIdentificationByIdentityCardNumber(identityCardNumber);
        try{
            /*validateIdentityCardNumberResponse.setFullName(personIdentificationByIdentityCardNumber.getPersonName().get(0).getName());*/
            validateIdentityCardNumberResponse.setFirstName(personIdentificationByIdentityCardNumber.getPersonName().get(0).getBirthName());
            validateIdentityCardNumberResponse.setGivenName(personIdentificationByIdentityCardNumber.getPersonName().get(0).getGivenName());
            validateIdentityCardNumberResponse.setPersonId(personIdentificationByIdentityCardNumber.getPerson().getId());
            //model.addAttribute("personId", personIdentificationByIdentityCardNumber.getPerson().getId());
            log.info("the person id is {}", personIdentificationByIdentityCardNumber.getPerson().getId());
        }catch (Exception e){
            log.info("name is null");
            validateIdentityCardNumberResponse.setIdentityCardNumber(null);

        }
        if(identityCardNumberAlreadyInUse) {
            validateIdentityCardNumberResponse.setMessage("There is already a person with this identity card number!");
            validateIdentityCardNumberResponse.setCode("05");
        } else {
            validateIdentityCardNumberResponse.setCode("00");
        }
        return validateIdentityCardNumberResponse;
    }

    @RequestMapping(value = "/clients/view/employees/{employerId}", method = RequestMethod.GET)
    public String viewPersonsByEmployingPartyRole(Model model, @PathVariable("employerId") String employerId){
        final List<Person> persons = clientService.getPersonsByEmployerId(employerId);
        clientService.getEmployingPartyRoleByEmployerId(employerId).stream().findFirst().ifPresent(employingPartyRole -> {
            log.info("The employingPartyRoleId is {}", employingPartyRole.getId());
            model.addAttribute("employingPartyRoleId", employingPartyRole.getId());
        });
        model.addAttribute("persons", persons);
        log.info("the employees are {}", persons);
        return "registration/view_employees";
    }

    @Data
    public class EmployingPartyRoleResult{
        protected String msg;
        protected String code;
    }

    @PostMapping("/clients/add/employee")
    public String addEmployee(@ModelAttribute EmployingPartyRolePersonRequestDTO employingPartyRolePersonRequestDTO, RedirectAttributes redirectAttributes){
        log.info("Creating the new employee | {}", employingPartyRolePersonRequestDTO);
        /*Optional<Person> personOptional = clientService.getPersonById(employingPartyRolePersonRequestDTO.getPersonId());
        if(personOptional != null){
            Person person = personOptional.get();*/
            /*List<Person> persons = person.getEmployingParty().getEmployee().stream().filter(person1 -> person1 instanceof Person)
                    .map(person1 -> (Person) person1)
                    .collect(Collectors.toList());*/
            /*boolean isPersonAlreadyAnEmployee = person.getId().equals(employingPartyRolePersonRequestDTO.getPersonId());
            if (isPersonAlreadyAnEmployee) {
                redirectAttributes.addAttribute("message", "The employee already exists.");
                log.info("employee already exists");
                return "redirect:/clients/view/employees" + employingPartyRolePersonRequestDTO.getEmployingPartyRoleId();
            }*/
        /*}*/
        clientService.addPersonToEmployingPartyRole(employingPartyRolePersonRequestDTO);
        log.info("new employee has been created");
        redirectAttributes.addFlashAttribute("message", "New employee has been created");
        StringBuilder rolePlayerId = new StringBuilder();

        employingPartyRoleRepository.findById(employingPartyRolePersonRequestDTO.getEmployingPartyRoleId()).ifPresent(employingPartyRole -> {
            final Optional<RolePlayer> rolePlayerOptional = employingPartyRole.getPlayer().stream().findFirst();

            if(rolePlayerOptional.isPresent()) {
                final RolePlayer rolePlayer = rolePlayerOptional.get();
                rolePlayerId.append(rolePlayer.getId());
            }
        });
        return "redirect:/clients/view/employees/" + rolePlayerId.toString();
    }

    @ResponseBody
    @PostMapping("/clients/employer/add/person")
    public EmployingPartyRoleResult addPersonToEmployingPartyRole(@RequestBody EmployingPartyRolePersonRequestDTO employingPartyRolePersonRequestDTO){
        final EmployingPartyRole employingPartyRole = clientService.addPersonToEmployingPartyRole(employingPartyRolePersonRequestDTO);
        EmployingPartyRoleResult employingPartyRoleMessage = new EmployingPartyRoleResult();
        if(employingPartyRole == null) {
            employingPartyRoleMessage.setCode("05");
        } else {
            employingPartyRoleMessage.setMsg("The employee has been added");
            log.info("the message is {}", employingPartyRoleMessage.getMsg());
            employingPartyRoleMessage.setCode("00");
        }

        return employingPartyRoleMessage;

    }

    @ResponseBody
    @PostMapping("/clients/employer/remove/person")
    public EmployingPartyRoleResult removePersonFromEmployingPartyRole(@RequestBody EmployingPartyRolePersonRequestDTO employingPartyRolePersonRequestDTO){
        final EmployingPartyRole employingPartyRole = clientService.removePersonFromEmployingPartyRole(employingPartyRolePersonRequestDTO);
        log.info("employee has been removed {}", employingPartyRole);
        EmployingPartyRoleResult employingPartyRoleMessage = new EmployingPartyRoleResult();
        if(employingPartyRole == null) {
            employingPartyRoleMessage.setCode("05");
        } else {
            employingPartyRoleMessage.setMsg("The employee has been removed");
            log.info("the message is {}", employingPartyRoleMessage.getMsg());
            employingPartyRoleMessage.setCode("00");
        }

        return employingPartyRoleMessage;
    }

    @GetMapping("/clients/get/employee/{personId}")
    public ResponseEntity<?> getEmployeeViaAjax(@PathVariable ("personId") String personId){
        log.info("Get employee by id {}", personId);
        AjaxResponseBody result = new AjaxResponseBody();

        Optional<Person> optionalPerson = personRepository.findById(personId);
        return  optionalPerson.map(person -> {
            try {
                Person person1 = new Person();
                StringBuilder personNameSb = new StringBuilder();
                person1.getPersonIdentification().stream().findFirst().ifPresent(personIdentification -> {
                    personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                        final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                        personNameSb.append(personFullName);
                    });
                });
                result.setResponseCode("00");
                result.setObject(personNameSb.toString());
                result.setNarrative("Successful");
            }catch (Exception e){
                result.setResponseCode("04");
                result.setNarrative("Failed to get the person name");
                e.printStackTrace();
            }
            return ResponseEntity.ok(result);
        }).orElseGet(() -> {
            result.setResponseCode("04");
            result.setNarrative("Failed to get the person name");
            return ResponseEntity.ok(result);
        });
    }


}
