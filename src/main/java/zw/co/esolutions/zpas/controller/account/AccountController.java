package zw.co.esolutions.zpas.controller.account;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.dto.account.AccountUploadDTO;
import zw.co.esolutions.zpas.dto.account.CashAccountDTO;
import zw.co.esolutions.zpas.dto.payments.RequestInfo;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.account.UploadAccountsService;
import zw.co.esolutions.zpas.services.iface.currency.CurrencyService;
import zw.co.esolutions.zpas.utilities.enums.AccountInfoRequestType;
import zw.co.esolutions.zpas.utilities.storage.StorageService;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;
import zw.co.esolutions.zpas.utilities.util.StringUtil;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Slf4j
@Controller
@RequestMapping("/accounts")
public class AccountController {

    @Autowired private AccountsService accountService;

    private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @Autowired private UploadAccountsService uploadAccountsService;
    @Autowired private StorageService storageService;
    @Autowired private CurrencyService currencyService;
    @Autowired private PartyRepository partyRepository;

    @GetMapping("/register/{rolePlayerId}")
    @PreAuthorize("hasAuthority('ROLE_CREATE_ACCOUNT')")
    public String createCashAccountPage(Model model, @PathVariable("rolePlayerId") String rolePlayerId, HttpSession session) {
        log.info("rolePlayerId in createCashAccountPage: " +  rolePlayerId);
        log.info("fiId in createCashAccountPage: " +  session.getAttribute("financialInstitutionId"));

        String accOwnerName = "";
        Optional<Party> partyOptional = partyRepository.findById(rolePlayerId);
        if(partyOptional.isPresent()){
            accOwnerName = getAccountOwnerName(partyOptional.get());
        }

        model.addAttribute("cashAccountTypes", CashAccountTypeCode.values());
        model.addAttribute("accountLevels", AccountLevelCode.values());
        model.addAttribute("accountStatusCodes", AccountStatusCode.values());
        model.addAttribute("accountManagementStatusCodes", AccountManagementStatusCode.values());
        model.addAttribute("entryStatusCodes", EntryStatusCode.values());
        model.addAttribute("balanceStatusCodes", BalanceStatusCode.values());
        model.addAttribute("reasonBlockedCodes", ReasonBlockedCode.values());
        model.addAttribute("rolePlayerId", rolePlayerId);
        model.addAttribute("accountOwnerName", accOwnerName);
        model.addAttribute("currencies", currencyService.getAllCurrencies());
        return "account/accountnew/create_account";
    }

    @PostMapping("/register/{rolePlayerId}")
    @PreAuthorize("hasAuthority('ROLE_CREATE_ACCOUNT')")
    public String createCashAccount(@Valid CashAccountDTO cashAccount, @PathVariable("rolePlayerId") String rolePlayerId,
                                    Model model, Errors errors, HttpSession session) {

        log.info("rolePlayerId in post method AccountController: " + rolePlayerId);

        String fiId = (String) session.getAttribute("financialInstitutionId");
        if(StringUtils.isEmpty(fiId)) {
            fiId = "NNTM1553862938756676";
        }
        String username = (String) session.getAttribute("clientName");
        log.info("Client name found: " + username);
        cashAccount.setUsername(username);

        log.info("fiId in AccountController from session: " + fiId);

        CashAccount cashAccount1 = this.accountService.createCashAccount(cashAccount, rolePlayerId, fiId);
        model.addAttribute("cashAccount", cashAccount1);
        return "redirect:/accounts/view/" + cashAccount1.getId();
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_ACCOUNT')")
    public String editCashAccountPage(Model model, @PathVariable("id") String id) {

        CashAccount cashAccount = this.accountService.findAccountById(id);
        Party accountOwner = getAccountOwner(cashAccount);

        model.addAttribute("cashAccount", cashAccount);
        model.addAttribute("accountOwnerId", accountOwner.getId());
        model.addAttribute("cashAccountTypeCodes", CashAccountTypeCode.values());
        model.addAttribute("accountLevelCodes", AccountLevelCode.values());
        model.addAttribute("accountStatusCodes", AccountStatusCode.values());
        model.addAttribute("accountManagementStatusCodes", AccountManagementStatusCode.values());
        model.addAttribute("entryStatusCodes", EntryStatusCode.values());
        model.addAttribute("balanceStatusCodes", BalanceStatusCode.values());
        model.addAttribute("reasonBlockedCodes", ReasonBlockedCode.values());
        model.addAttribute("currencies", currencyService.getAllCurrencies());
        return "account/accountnew/edit_account";
    }

    @PostMapping("/update/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_ACCOUNT')")
    public String editCashAccount(@Valid CashAccountDTO cashAccountDTO, @PathVariable("id") String id,
                                  Model model, Errors errors, HttpSession session) {
        log.info("cashAccountId: " + cashAccountDTO.getId());

        String username = (String) session.getAttribute("clientName");
        log.info("Client name found: " + username);
        cashAccountDTO.setUsername(username);

        CashAccount cashAccount = this.accountService.updateCashAccount(cashAccountDTO);
        model.addAttribute("cashAccount", cashAccount);
        return "redirect:/accounts/view/" + cashAccount.getId();
    }

    @GetMapping("/view/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_ACCOUNT')")
    public String viewAccount(Model model, @PathVariable(name = "id") String id) {
        final CashAccount cashAccount = accountService.findAccountById(id);
        Party accountOwner = getAccountOwner(cashAccount);
        String accountOwnerName = getAccountOwnerName(accountOwner);

        model.addAttribute("cashAccount", cashAccount);
        model.addAttribute("accountOwnerId", accountOwner.getId());
        model.addAttribute("accountOwnerName", accountOwnerName);
        model.addAttribute("fiName", getFiName(cashAccount));
        model.addAttribute("formatter", dateFormatter);
        model.addAttribute("dateTimeFormatter", dateTimeFormatter);
        model.addAttribute("requestTypes", AccountInfoRequestType.values());

        return "account/accountnew/view_account";
    }

    @GetMapping("/approve/{id}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_ACCOUNT')")
    public String approveAccount(Model model, @PathVariable(name = "id") String id) {
        log.info("Approving account: " + id);
        final CashAccount cashAccount = accountService.approveCashAccount(id);
        model.addAttribute("cashAccount", cashAccount);
        return "redirect:/accounts/view/" + cashAccount.getId();
    }

    @GetMapping("/reject/{id}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_ACCOUNT')")
    public String rejectAccount(Model model, @PathVariable(name = "id") String id) {
        log.info("Rejecting account: " + id);
        final CashAccount cashAccount = accountService.rejectCashAccount(id);
        model.addAttribute("cashAccount", cashAccount);
        return "redirect:/accounts/view/" + cashAccount.getId();
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_ACCOUNT')")
    public String deleteAccount(Model model, @PathVariable(name = "id") String id) {
        log.info("Deleting account: " + id);
        final CashAccount cashAccount = accountService.deleteCashAccount(id);
        model.addAttribute("cashAccount", cashAccount);
        return "redirect:/accounts/view/" + cashAccount.getId();
    }

    @GetMapping("/search")
    @PreAuthorize("hasAuthority('ROLE_SEARCH_ACCOUNTS')")
    public String searchAccountsPage(Model model){
        List<CashAccount> accounts = new ArrayList<>();

        model.addAttribute("accounts", accounts);
        return "account/search_accounts";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROLE_SEARCH_ACCOUNTS')")
    public String searchAccounts(@RequestParam(value = "search", required = false) String q, Model model, HttpSession session) {
        List<CashAccount> searchResults;
        String fiId = (String)session.getAttribute("financialInstitutionId");
        log.info("fiId in session: " + fiId);

        log.info("Search string: " + q);
        try {
            log.info("Inside try...");
            searchResults = accountService.searchAccounts(q, fiId);

        } catch (Exception ex) {
            searchResults = new ArrayList<>();
        }
        log.info("Search results are: {}", searchResults);
        model.addAttribute("accounts", searchResults);
        return "account/search_accounts";
    }

    @GetMapping("/my-accounts")
    public String getMyAccounts(Model model, HttpSession session){
        //String clientId = (String) session.getAttribute("clientId");
        String clientId = (String) session.getAttribute("loginAsId");
        log.info("logInAsId: " + clientId);

        List<CashAccount> accounts = accountService.getCashAccountsByClientId(clientId);
        List<AccountTemp> accountTemps = getAccountTemps(accounts);

        accountService.accountBalanceRequest(clientId);

        log.info("Account temps: {}", accountTemps);

        model.addAttribute("accounts", accountTemps);
        return "account/my_accounts";
    }

    @GetMapping("/clients/fetch/{clientId}")
    public ResponseEntity<?> fetchAccountsForClient(@PathVariable("clientId") String clientId){
        log.info("Fetching accounts...Client id is: " + clientId);

        AjaxResponseBody result = new AjaxResponseBody();
        List<CashAccount> accounts = accountService.getCashAccountsByClientId(clientId);
        List<AccountTemp> accountTemps = new ArrayList<>();

        if(getAccountTemps(accounts) != null && !getAccountTemps(accounts).isEmpty()){
            accountTemps = getAccountTemps(accounts);
        }

        result.setResponseCode("00");
        result.setNarrative("Successfully fetched accounts.");
        result.setObject(accountTemps);

        return ResponseEntity.ok(result);
    }

    @PostMapping("/info-request")
    public String accountInfoRequest(@ModelAttribute("requestInfo") RequestInfo requestInfo, RedirectAttributes redirectAttributes) {
        log.info("The request info DTO is: {}", requestInfo);

        CashAccount cashAccount = accountService.aggregateAccountInfoRequest(requestInfo);

        log.info("Requested info for account {}", cashAccount);
        redirectAttributes.addFlashAttribute("message", "Account info request submitted successfully.");
        return "redirect:/accounts/view/" + requestInfo.getId();
    }

    @GetMapping("/upload")
    @PreAuthorize("hasAuthority('ROLE_UPLOAD_ACCOUNTS')")
    public String uploadAccountPage(Model model, HttpSession session) {
        model.addAttribute("title", "Upload Payment");
        return "account/upload_accounts";
    }

    @PostMapping("/upload")
    @PreAuthorize("hasAuthority('ROLE_UPLOAD_ACCOUNTS')")
    public String uploadAccounts(@RequestParam("accountsFile") MultipartFile accountsFile,
                                    @RequestParam("purpose") String purpose,
                                    RedirectAttributes redirectAttributes, HttpSession session) {

        String clientId = (String) session.getAttribute("clientId");
        String financialInstitutionId = (String) session.getAttribute("financialInstitutionId");
        String username = (String) session.getAttribute("username");


        storageService.store(accountsFile);
        final AccountUploadDTO paymentUploadDTO = AccountUploadDTO.builder()
                .clientId(clientId)
                .fileName(accountsFile.getOriginalFilename())
                .financialInstitutionId(financialInstitutionId)
                .username(username)
                .purpose(purpose)
                .build();
        List<CashAccount> cashAccounts = uploadAccountsService.uploadAccounts(paymentUploadDTO);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + accountsFile.getOriginalFilename());
        return "redirect:view/upload/" + cashAccounts.get(0).getUploadAccountsId();
    }


    @GetMapping("/view/upload/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_ACCOUNTS')")
    public String viewUploadAccount(Model model, @PathVariable(name = "id") String id) {
        Notification notification = new Notification();
        Optional<UploadAccounts> uploadAccounts = uploadAccountsService.getUploadAccounts(id);
        return uploadAccounts.map(uploadAccounts1 -> {

            model.addAttribute("uploadAccounts", uploadAccounts1);
            model.addAttribute("formatter", dateFormatter);
            model.addAttribute("dateTimeFormatter", dateTimeFormatter);
            model.addAttribute("notification", notification);
            return "accounts/view_accountupload";
        }).orElseGet(() -> {
            return "accounts/upload_accounts";
        });
    }

    private List<AccountTemp> getAccountTemps(List<CashAccount> accounts){
        List<AccountTemp> accountTemps = new ArrayList<>();

        accounts.forEach(account -> {
            AccountTemp accountTemp = new AccountTemp();
            accountTemp.setId(account.getId());
            accountTemp.setAccNo(account.getIdentification().getNumber());
            accountTemp.setAccName(account.getIdentification().getName());
            accountTemp.setBBan(account.getIdentification().getBBAN());
            accountTemp.setIBan(account.getIdentification().getIBAN());
            accountTemp.setFiName(account.getFinancialInstitutionName());

            for(CashBalance cashBalance : account.getCashBalance()){
                if(cashBalance.getType().getCodeName().equals("ITAV")){
                    accountTemp.setBalance(cashBalance.getAmount().getAmount());
                    accountTemp.setCurrency(cashBalance.getAmount().getCurrency().getCodeName());
                }
            }

            /**
             * Add account temp to the list of account temps
             */
            accountTemps.add(accountTemp);
        });

        return accountTemps;
    }

    private Party getAccountOwner(CashAccount cashAccount){
        List<AccountPartyRole> accountPartyRoles = cashAccount.getPartyRole();
        Optional<AccountPartyRole> accountPartyRoleOptional = accountPartyRoles.stream()
                .filter(role -> role instanceof AccountOwnerRole).findFirst();
        Party accountOwner = null;

        if(accountPartyRoleOptional.isPresent()){
            AccountPartyRole accountOwnerRole = accountPartyRoleOptional.get();
            List<RolePlayer> accountOwners = accountOwnerRole.getPlayer();
            Optional<RolePlayer> accountOwnerOptional = accountOwners.stream().findFirst();
            if(accountOwnerOptional.isPresent()){
                accountOwner = (Party) accountOwnerOptional.get();
            }
        }
        return accountOwner;
    }

    private String getAccountOwnerName(Party party) {

        if (party instanceof Organisation) {
            Organisation organisation = (Organisation) party;
            final Stream<OrganisationName> organisationNameStream = organisation.getOrganisationIdentification().stream()
                    .map(OrganisationIdentification::getOrganisationName)
                    .flatMap(organisationNames -> organisationNames.stream());

            return organisationNameStream.map(organisationName ->
                organisationName == null ? "" : organisationName.getLegalName()
            ).findFirst().orElse("");
        } else {
            Person person = (Person) party;
            final Stream<PersonName> personNameStream = person.getPersonIdentification().stream()
                    .map(PersonIdentification::getPersonName)
                    .flatMap(personNames -> personNames.stream());

            return personNameStream.map(personName ->
                personName == null ?
                        "" : personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName()
            ).findFirst().orElse("");
        }
    }

    private String getFiName(CashAccount cashAccount){
        List<AccountPartyRole> accountPartyRoles = cashAccount.getPartyRole();
        Optional<AccountPartyRole> accountPartyRoleOptional = accountPartyRoles.stream()
                .filter(role -> role instanceof AccountServicerRole).findFirst();
        String fiName = "";

        if(accountPartyRoleOptional.isPresent()){
            AccountPartyRole accountServicerRole = accountPartyRoleOptional.get();
            List<RolePlayer> accountServicers = accountServicerRole.getPlayer();
            Optional<RolePlayer> accountServicerOptional = accountServicers.stream().findFirst();
            if(accountServicerOptional.isPresent()){
                Organisation accountServicer = (Organisation) accountServicerOptional.get();
                Optional<OrganisationIdentification> identificationOptional = accountServicer
                        .getOrganisationIdentification().stream().findFirst();
                if(identificationOptional.isPresent()){
                    List<OrganisationName> names = identificationOptional.get().getOrganisationName();
                    Optional<OrganisationName> nameOptional = names.stream().findFirst();
                    fiName = nameOptional.map(OrganisationName::getLegalName).orElse("");
                }
            }
        }
        return fiName;
    }

    @Data
    public class AccountTemp {
        protected String id;
        protected String fiName;
        protected String accName;
        protected String accNo;
        protected String bBan;
        protected String iBan;
        protected String currency;
        protected BigDecimal balance;
        //protected Account account;
        //protected String uPic;
    }

}
