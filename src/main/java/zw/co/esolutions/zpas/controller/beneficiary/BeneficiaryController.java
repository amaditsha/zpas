package zw.co.esolutions.zpas.controller.beneficiary;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.model.Beneficiary;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.model.Person;
import zw.co.esolutions.zpas.services.iface.beneficiary.BeneficiaryService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Created by jnkomo on 29 Mar 2019
 */

@Controller
@Slf4j
@RequestMapping("/beneficiaries")
public class BeneficiaryController {

    @Autowired
    BeneficiaryService beneficiaryService;

    @Autowired
    ClientService clientService;

    @Autowired
    FinancialInstitutionService financialInstitutionService;


    @GetMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_BENEFICIARY')")
    public String createBeneficiaryPage(Model model, HttpSession session) {
        model.addAttribute("title", "Create Beneficiary");
        String clientId = (String) session.getAttribute("clientId");

        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        model.addAttribute("financialInstitutions", financialInstitutions);


        return "beneficiary/create_beneficiary";
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_BENEFICIARY')")
    public String createBeneficiary(@Valid Beneficiary beneficiary, Model model, BindingResult bindingResult, Errors errors, HttpSession session) {
        log.info("Entering Beneficiary Controller for Create");

        model.addAttribute("title", "Create Beneficiary");
        if (bindingResult.hasErrors()) {
            return "beneficiary/create_beneficiary";
        }

        String clientId = (String) session.getAttribute("clientId");
        Optional<Person> optionalPerson = clientService.getPersonById(clientId);
        optionalPerson.ifPresent(person -> {
            beneficiary.setLinkedBeneficiaryId(person);
        });

        log.info("About to create beneficiary");
        final Beneficiary beneficiaryCreated = beneficiaryService.createBeneficiary(beneficiary);
        log.info("Beneficiary ID is {}", beneficiaryCreated.getId());

        // beneficiaryCreated.getClientCertPath();

        return "redirect:view/" + beneficiaryCreated.getId();
    }

    @GetMapping("/view/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_BENEFICIARY')")
    public String viewBeneficiary(Model model, @PathVariable(name = "id") String id) {
        final Optional<Beneficiary> beneficiaryOptional = beneficiaryService.getBeneficiary(id);

        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        model.addAttribute("financialInstitutions", financialInstitutions);

        Optional<FinancialInstitution> financialInstitution = financialInstitutionService.getFinancialInstitutionById(beneficiaryOptional.get().getBank());


        financialInstitution.ifPresent(
                financialInstitution1 -> {
                    final String bankCopy = financialInstitution1.getOrganisationIdentification().get(0).getOrganisationName().get(0).getTradingName();
                    model.addAttribute("bankCopy", bankCopy);
                }
        );

        model.addAttribute("title", "View Beneficiary");
        model.addAttribute("beneficiary", beneficiaryOptional.get());
        model.addAttribute("financialInstitution", financialInstitution);


        return "beneficiary/view_beneficiary";
    }

    @GetMapping("/manage")
    @PreAuthorize("hasAuthority('ROLE_VIEW_BENEFICIARY')")
    public String manageBeneficiaries(Model model) {
        List<Beneficiary> beneficiaryList = beneficiaryService.getBeneficiaries();
        model.addAttribute("beneficiaries", beneficiaryList);
        return "beneficiary/manage_beneficiaries";
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_BENEFICIARY')")
    public ResponseEntity<?> deleteBeneficiaryViaAjax(@PathVariable("id") String id){
        Beneficiary beneficiary = beneficiaryService.deleteBeneficiary(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(beneficiary.getEntityStatus().equals(EntityStatus.DELETED)) {
            result.setNarrative("Successfully deleted.");
            result.setResponseCode("00");
            result.setObject(beneficiary.getEntityStatus().name());
        } else {
            result.setNarrative("The beneficiary can't be deleted");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/reject/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_BENEFICIARY')")
    public ResponseEntity<?> rejectBeneficiaryViaAjax(@PathVariable("id") String id){
        Beneficiary beneficiary = beneficiaryService.rejectBeneficiary(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(beneficiary.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            result.setNarrative("Successfully rejected.");
            result.setResponseCode("00");
            result.setObject(beneficiary.getEntityStatus().name());
        } else {
            result.setNarrative("The beneficiary can't be rejected");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/approve/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_BENEFICIARY')")
    public ResponseEntity<?> approveBeneficiaryViaAjax(@PathVariable("id") String id){
        Beneficiary beneficiary = beneficiaryService.approveBeneficiary(id);
        AjaxResponseBody result = new AjaxResponseBody();
        if(beneficiary.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            result.setNarrative("Successfully approved.");
            result.setResponseCode("00");
            result.setObject(beneficiary.getEntityStatus().name());
        } else {
            result.setNarrative("The beneficiary can't be approved");
            result.setResponseCode("04");
        }

        return ResponseEntity.ok(result);
    }

    @PostMapping("/edit")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_BENEFICIARY')")
    public ResponseEntity<?> editBeneficiary(@ModelAttribute("beneficiary") Beneficiary formBeneficiary, Model model, Errors errors, HttpSession session) {

        log.info("Inside Beneficiary Edit Controller : {}", formBeneficiary.getId());
        final Optional<Beneficiary> beneficiaryOptional = beneficiaryService.getBeneficiary(formBeneficiary.getId());

        log.info("Found Beneficiary? : {} ", beneficiaryOptional.isPresent());

        AjaxResponseBody result = new AjaxResponseBody();

        beneficiaryOptional.ifPresent(beneficiary -> {

            //add the details to be edited
            beneficiary.setName(formBeneficiary.getName());
            beneficiary.setAccountNumber(formBeneficiary.getAccountNumber());
            beneficiary.setBank(formBeneficiary.getBank());
            beneficiary.setNationalId(formBeneficiary.getNationalId());

            log.info("Account Number before updating: {} " , beneficiary.getAccountNumber());

            Beneficiary updatedBeneficiary = beneficiaryService.updateBeneficiary(beneficiary);
            log.info("Account Number after updating: {} " , updatedBeneficiary.getAccountNumber());

            if(updatedBeneficiary.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                BeneficiaryTemp beneficiaryTemp = new BeneficiaryTemp();
                beneficiaryTemp.setId(updatedBeneficiary.getId());
                beneficiaryTemp.setName(updatedBeneficiary.getName());
                beneficiaryTemp.setBank(updatedBeneficiary.getBank());
                beneficiaryTemp.setAccountNumber(updatedBeneficiary.getAccountNumber());
                beneficiaryTemp.setNationalId(updatedBeneficiary.getNationalId());
                beneficiaryTemp.setEntityStatus(updatedBeneficiary.getEntityStatus());

                result.setObject(beneficiaryTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The beneficiary can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);

    }

    @Data
    public class BeneficiaryTemp{
        protected String id;
        protected String name;
        protected String bank;
        protected String accountNumber;
        protected String nationalId;
        protected EntityStatus entityStatus;

    }

}
