package zw.co.esolutions.zpas.controller.merchant;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchDTO;
import zw.co.esolutions.zpas.dto.jsonapi.PersonSearchResponseDTO;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PartyRepository;
import zw.co.esolutions.zpas.services.iface.merchant.MerchantService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.iface.restapi.RestApiService;
import zw.co.esolutions.zpas.services.impl.merchant.MerchantSearchService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityType;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping(path = "/merchant")
public class MerchantController {
    @Autowired
    MerchantService merchantService;

    @Autowired
    private RestApiService jsonApiService;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private MerchantSearchService merchantSearchService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @GetMapping
    public String renderMerchant(Model model) {
        log.info("Merchant page GET request.");
        model.addAttribute("entityTypes", EntityType.values());
        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        model.addAttribute("financialInstitutions", financialInstitutions);
        return "merchant/create_merchant";
    }

    @PostMapping
    public String saveMerchant(@Valid @ModelAttribute("merchant") Merchant merchant, Errors errors, Model model) {
        log.info("Merchant page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the merchant.");
            return "merchant/create_merchant";
        }

        log.info("Merchant: {}", merchant);
        Merchant persistedMerchant = merchantService.createMerchant(merchant);
        return "redirect:/merchant/" + persistedMerchant.getId();
    }

    @GetMapping("/{id}")
    public String getMerchantById(@PathVariable("id") String id, Model model) {
        Optional<Merchant> merchantOptional = merchantService.getMerchant(id);
        return merchantOptional.map(merchant -> {
            model.addAttribute("formatter", formatter);
            model.addAttribute("merchant", merchant);
            model.addAttribute("title", "View Merchant");

            Optional<FinancialInstitution> financialInstitution = financialInstitutionService.getFinancialInstitutionById(merchantOptional.get().getBank());
            financialInstitution.ifPresent(
                    financialInstitution1 -> {
                        final String bankCopy = financialInstitution1.getOrganisationIdentification().get(0).getOrganisationName().get(0).getTradingName();
                        model.addAttribute("bankCopy", bankCopy);
                    }
            );
            return "merchant/view_merchant";
        }).orElseGet(() -> {
            return "merchant/create_merchant";
        });
    }

    @GetMapping("edit/{id}")
    public String renderUpdateMerchant(@PathVariable("id") String id, Model model) {
        log.info("Update merchant page request.");
        model.addAttribute("title", "Edit merchant");
        Optional<Merchant> merchantOptional = merchantService.getMerchant(id);
        return merchantOptional.map(merchant -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            model.addAttribute("formatter", formatter);
            model.addAttribute("merchant", merchant);
            model.addAttribute("moneyUtil", new MoneyUtil());
            model.addAttribute("entityTypes", EntityType.values());
            return "merchant/edit_merchant";
        }).orElseGet(() -> {
            return "merchant/create_merchant";
        });
    }

    @PostMapping("/edit")
    public String updateMerchant(@Valid @ModelAttribute("merchant") Merchant merchant, Errors errors, Model model) {
        log.info("Merchant edit page POST request.");
        if (errors.hasErrors()) {
            log.error("Error encountered while processing the POST request");
//            errors.getAllErrors().
            model.addAttribute("error", "Failed to save the merchant.");
            return "merchant/home";
        }
        log.info("Updating the merchant | {}", merchant);
        Merchant updatedMerchant = merchantService.updateMerchant(merchant);
        return "redirect:/merchant/" + updatedMerchant.getId();
    }



    @GetMapping("/approve/{id}")
    public String approveMerchant(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        log.info("Approving the merchant");
        Merchant merchant = merchantService.approveMerchant(id);

        Notification notification = new Notification();
        if(merchant.getEntityStatus().equals(EntityStatus.ACTIVE)) {
            notification.setType("success");
            notification.setMessage("Successfully approved.");
        } else {
            notification.setType("error");
            notification.setMessage("Failed to approve merchant.");
        }
        redirectAttributes.addFlashAttribute("notification", notification);
        return "redirect:/merchant/" + merchant.getId();
    }

    @GetMapping("/reject/{id}")
    public String rejectMerchant(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        log.info("Rejecting the merchant");
        Merchant merchant = merchantService.rejectMerchant(id);
        Notification notification = new Notification();
        if(merchant.getEntityStatus().equals(EntityStatus.DISAPPROVED)) {
            notification.setType("success");
            notification.setMessage("Successfully dissaproved.");
        } else {
            notification.setType("error");
            notification.setMessage("Failed to disapprove the merchant.");
        }
        redirectAttributes.addFlashAttribute("notification", notification);
        return "redirect:/merchant/" + merchant.getId();
    }

    @GetMapping("/delete/{id}")
    public String deleteMerchant(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
        log.info("Delete the college");
        Merchant merchant = merchantService.deleteMerchant(id);
        Notification notification = new Notification();
        if (merchant.getEntityStatus().equals(EntityStatus.DELETED)) {
            notification.setMessage("Successfully deleted.");
            notification.setType("success");
        } else {
            notification.setMessage("Failed to delete.");
            notification.setType("error");
        }

        return "redirect:/merchant/" + merchant.getId();
    }

    @ResponseBody
    @GetMapping("/search/persons/")
    public List<PersonSearchResponseDTO> getPersonsViaAjax(@RequestParam("search") String search) {
        log.info("Get persons by search name {}", search);

        PersonSearchDTO personSearchDTO = PersonSearchDTO.builder().search(search).build();
        List<PersonSearchResponseDTO> searchResponseDTOS = jsonApiService.searchAllPersons(personSearchDTO);
        return searchResponseDTOS;
    }

    @Data
    class PersonInfo {
        private String name;
        private String title;
    }

    @ResponseBody
    @GetMapping("/get/partyId/{partyId}")
    public PersonInfo getPersonNamesInfo(@PathVariable("partyId") String partyId) {
        log.info("The party id is {}", partyId);
        Optional<Party> optionalParty = partyRepository.findById(partyId);
        PersonInfo personInfo = new PersonInfo();
        StringBuilder personNameSb = new StringBuilder();
        log.info("The party is : {}", optionalParty.map(Party::getId).orElse("Oops..No party found."));
        optionalParty.map(party -> {
            if(party instanceof NonFinancialInstitution) {
               /* List<PersonIdentification> personIdentifications = ((Person) party).getPersonIdentification();
                log.info("The Person Identification is: {}", personIdentifications);
                personIdentifications.stream().findFirst().ifPresent(personIdentification -> {
                    personIdentification.getPersonName().stream().findFirst().ifPresent(personName -> {
                        log.info("");
                        final String personFullName = personName.getBirthName() + " " + personName.getMiddleName() + " " + personName.getGivenName();
                        personNameSb.append(personFullName);
                    });
                });
                personInfo.setName(personNameSb.toString());*/
                List<OrganisationIdentification> organisationIdentifications = ((NonFinancialInstitution) party).getOrganisationIdentification();
                log.info("Organisation Identifications : {}", organisationIdentifications);
                organisationIdentifications.stream().findFirst().ifPresent(organisationIdentification -> {
                    organisationIdentification.getOrganisationName().stream().findFirst().ifPresent(organisationName -> {
                        final String legalName = organisationName.getLegalName();
                        personNameSb.append(legalName);
                    });
                });
                personInfo.setName(personNameSb.toString());

            } else if(party instanceof Organisation) {
                List<OrganisationIdentification> organisationIdentifications = ((Organisation) party).getOrganisationIdentification();
                log.info("Organisation Identifications : {}", organisationIdentifications);
                organisationIdentifications.stream().findFirst().ifPresent(organisationIdentification -> {
                    organisationIdentification.getOrganisationName().stream().findFirst().ifPresent(organisationName -> {
                        final String legalName = organisationName.getLegalName();
                        personNameSb.append(legalName);
                    });
                });
                personInfo.setName(personNameSb.toString());
            }
            return null;
        }).orElseGet(() -> null);
        log.info("The identification name is  :{}", personInfo);
        return personInfo;
    }


    @GetMapping("/all")
    public String viewMerchants(Model model) {
        List<Merchant> merchants = merchantService.getMerchants();
        model.addAttribute("merchants", merchants);
        Notification notification = new Notification();
        if (merchants == null || merchants.size() == 0) {
            notification.setType("error");
            notification.setMessage("The merchants were not found.");
        }
        model.addAttribute("notification", notification);
        model.addAttribute("formatter", formatter);
        model.addAttribute("moneyUtil", new MoneyUtil());

        return "merchant/manage_merchants";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchMerchant(@RequestParam(value = "search", required = false) String q, Model model) {
        List<Merchant> searchResults = null;
        log.info("Search string: " + q);
        try {
            searchResults = merchantSearchService.searchMerchant(q);
            log.info("the search results in try statement are: {}", searchResults);

        } catch (Exception ex) {
            // here you should handle unexpected errors
            // ...
            // throw ex;
            searchResults = new ArrayList<>();
        }
        log.info("Search results are: {}", searchResults);
        merchantService.refreshMerchantSearchIndex();
        model.addAttribute("merchants", searchResults);
        return "merchant/search_merchant";

    }


    @Data
    class Notification {
        protected String message;
        protected String type;
    }

}
