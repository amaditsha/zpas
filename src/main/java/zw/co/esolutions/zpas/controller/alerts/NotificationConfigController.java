package zw.co.esolutions.zpas.controller.alerts;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.model.NotificationConfig;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
@RequestMapping("/notificationconfiguration")
public class NotificationConfigController {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @Autowired
    NotificationConfigService notificationConfigService;
    
    @GetMapping("/all/{partyId}")
    public String viewAll(@PathVariable String partyId, Model model) {
        List<NotificationConfig> notificationConfigs = notificationConfigService.findAllByPartyId(partyId);
        log.info("The size of returned configs are {}", notificationConfigs.size());
        model.addAttribute("notificationConfigs", notificationConfigs);
        model.addAttribute("notificationConfiguration", new NotificationConfig());
        model.addAttribute("formatter", formatter);
        
        return "notificationconfig/home_notificationconfig";
    }

    @PostMapping("/edit")
    public ResponseEntity<?> updateNotificationConfigViaAjax(@Valid @ModelAttribute("notificationType") NotificationConfig notificationType, Errors errors){
        log.info("Edit config | {}", notificationType);
        AjaxResponseBody result = new AjaxResponseBody();

        Optional<NotificationConfig> oldNotificationConfigOptional = notificationConfigService.findById(notificationType.getId());
        return oldNotificationConfigOptional.map(newNotificationConfig -> {
            newNotificationConfig.setBell(checkBooleanValue(notificationType.getBell()));
            newNotificationConfig.setEmail(checkBooleanValue(notificationType.getEmail()));
            newNotificationConfig.setSms(checkBooleanValue(notificationType.getSms()));

            NotificationConfig updatedNotificationConfig = notificationConfigService.update(newNotificationConfig);
            result.setNarrative("Successfully updated.");
            result.setResponseCode("00");
            result.setObject(updatedNotificationConfig);

            return ResponseEntity.ok(result);
        }).orElseGet(() -> {
            result.setNarrative("The configuration can't be edited");
            result.setResponseCode("04");
            return ResponseEntity.ok(result);
        });
    }

    private boolean checkBooleanValue(Boolean rawValue){
        if(rawValue != null && rawValue) {
            return rawValue;
        }
        return false;
    }

    @Data
    class Notification {
        protected String message;
        protected String type;
    }
}
