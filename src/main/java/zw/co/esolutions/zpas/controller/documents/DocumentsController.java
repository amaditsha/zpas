package zw.co.esolutions.zpas.controller.documents;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import zw.co.esolutions.zpas.model.Document;
import zw.co.esolutions.zpas.services.iface.documents.DocumentsService;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by alfred on 11 Mar 2019
 */
@Slf4j
@Controller
@Scope("session")
@RequestMapping("/documents")
public class DocumentsController {
    @Autowired
    private DocumentsService documentsService;

    @GetMapping("/{documentId}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_DOCUMENTS')")
    public String getUserDocuments(Model model, @PathVariable("documentId") String documentId) {
        return documentsService.getDocumentById(documentId).map(document -> {
            model.addAttribute("document", document);
            return "documents/view_document";
        }).orElseGet(() -> {
            model.addAttribute("message", "Document not found.");
            return "documents/view_document";
        });
    }

    @GetMapping("/")
    @PreAuthorize("hasAuthority('ROLE_VIEW_DOCUMENTS')")
    public String getUserDocuments(Model model, HttpSession session) {
        String clientId = (String) session.getAttribute("clientId");
        final List<Document> clientDocuments = documentsService.getDocumentsByClientId(clientId);
        model.addAttribute("documents", clientDocuments);
        return "documents/view_documents";
    }
}
