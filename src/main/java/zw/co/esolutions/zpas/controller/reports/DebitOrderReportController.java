package zw.co.esolutions.zpas.controller.reports;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import zw.co.esolutions.zpas.dto.debitorder.DebitOrderReportDTO;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/debitorderreport")
@Slf4j
public class DebitOrderReportController {

    private static final String OUTPUT_FOLDER = StorageProperties.BASE_LOCATION;
    @Autowired
    private DataSource dataSource;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap modelMap) {
        ModelAndView model = new ModelAndView();
        return "reports/debit_order_report";
    }

    @GetMapping("/report")
    public void batchReport(@Valid DebitOrderReportDTO debitOrderReportDTO, HttpServletResponse response, HttpSession session) {
        log.info("inside batch report method...");
        try {
            Map<String,Object> reportParameters = new HashMap<>();

            String bicFi = (String) session.getAttribute("bicFi");
            log.info("BICFI inside method...." + bicFi);
            log.info("start Processiing Date inside method...." + debitOrderReportDTO.getStartProcessingDate());
            reportParameters.put("receiverid",bicFi);

            if(StringUtils.isNoneBlank(debitOrderReportDTO.getStartProcessingDate())){
                reportParameters.put("startProcessingDate", Timestamp.valueOf(LocalDateTime.parse(debitOrderReportDTO.getStartProcessingDate())));
            }
            if(StringUtils.isNoneBlank(debitOrderReportDTO.getEndProcessingDate())){
                reportParameters.put("endProcessingDate", Timestamp.valueOf(LocalDateTime.parse(debitOrderReportDTO.getEndProcessingDate())));
            }
            if(StringUtils.isNoneBlank(debitOrderReportDTO.getStartDate())){
                reportParameters.put("startDate", Timestamp.valueOf(LocalDateTime.parse(debitOrderReportDTO.getStartDate())));
            }
            if(StringUtils.isNoneBlank(debitOrderReportDTO.getEndDate())){
                reportParameters.put("endDate", Timestamp.valueOf(LocalDateTime.parse(debitOrderReportDTO.getEndDate())));
            }

            //   JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(transactionService.getSummaryReport());
            InputStream inputStream = this.getClass().getResourceAsStream("/reports/debitorderreports/DebitOrderBatchReport.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
            log.info("report params and data src---- " + reportParameters + "--" + dataSource.getConnection());
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameters, dataSource.getConnection());

            //exportToPdf(jasperPrint, "test");

           /*HtmlExporter exporter = new HtmlExporter(DefaultJasperReportsContext.getInstance());
           exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
           exporter.setExporterOutput(new SimpleHtmlExporterOutput(response.getWriter()));
           exporter.exportReport();*/

            response.setContentType("application/pdf");

            java.time.format.DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            String file = "DebitOrderBatchReport"+ LocalDateTime.now().format(formatter);

            /**
             * Set the content disposition to "attachment" to force browser to download
             * and "inline" to view in browser
             **/
            response.setHeader("Content-disposition", "inline; filename="+file+".pdf");
            final OutputStream outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);


        }catch (Exception e){
            System.out.println("An error occured" + e.getMessage());
        }
    }

    @GetMapping("/{batchId}")
    public void report(@PathVariable(name = "batchId") String batchId, HttpServletResponse response) {
        log.info("inside report method...");
        try {
            Map<String,Object> reportParameters = new HashMap<>();

            reportParameters.put("batchId",batchId);

            //   JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(transactionService.getSummaryReport());
            InputStream inputStream = this.getClass().getResourceAsStream("/reports/debitorderreports/debit_order_report.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
            log.info("report params and data src---- " + reportParameters + "--" + dataSource.getConnection());

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameters, dataSource.getConnection());
            /*HtmlExporter exporter = new HtmlExporter(DefaultJasperReportsContext.getInstance());
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleHtmlExporterOutput(response.getWriter()));
            exporter.exportReport();
            exportToPdf(jasperPrint,"debitorders");*/
            /*exportToCsv(jasperPrint, "debitorders");
            exportToXls(jasperPrint, "debitorders");*/

            response.setContentType("application/pdf");

            //String pdfFileName = "/Debit_Order_Report"  + LocalDateTime.now() + ".pdf";
            String pdfFileName = "Debit_Order_Report"  + LocalDateTime.now() + ".pdf";
            String filePath = OUTPUT_FOLDER + pdfFileName;

            response.setHeader("Content-Disposition", "inline: filename" + pdfFileName);
            final ServletOutputStream outputStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

        }catch (Exception e){
            System.out.println("An error occured" + e.getMessage());
        }
    }

    private File exportToPdf(JasperPrint jasperPrint, String userFullname) {
        log.info("inside export to pdf...");
        String pdfFileName = "/Debit_Order_Report" + userFullname + "_" + LocalDateTime.now() + ".pdf";
        String filePath = OUTPUT_FOLDER + pdfFileName;
        log.info("filePath: " + filePath);

        JRPdfExporter exporter = new JRPdfExporter();

        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(filePath));

        SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
        reportConfig.setSizePageToContent(true);
        reportConfig.setForceLineBreakPolicy(false);

        SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
        exportConfig.setMetadataAuthor(userFullname);
        exportConfig.setEncrypted(true);
        exportConfig.setAllowedPermissionsHint("PRINTING");

        exporter.setConfiguration(reportConfig);
        exporter.setConfiguration(exportConfig);

        try {
            exporter.exportReport();
        } catch (JRException e) {
            e.printStackTrace();
        }

        return new File(filePath);
    }

    private File exportToXls(JasperPrint jasperPrint, String userFullname) {
        String xlsFileName = "Transaction_report_" + userFullname + "_" + LocalDateTime.now() + ".xls";
        String filePath =OUTPUT_FOLDER+ xlsFileName;

        JRXlsxExporter exporter = new JRXlsxExporter();

        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(filePath));

        SimpleXlsxReportConfiguration reportConfig
                = new SimpleXlsxReportConfiguration();
        reportConfig.setSheetNames(new String[]{});

        exporter.setConfiguration(reportConfig);
        try {
            exporter.exportReport();
        } catch (JRException e) {
            e.printStackTrace();
        }
        return new File(filePath);
    }

    private File exportToCsv(JasperPrint jasperPrint, String userFullname) {
        String csvFileName = "Transaction_report_" + userFullname + "_" + LocalDateTime.now() + ".csv";
        String filePath = OUTPUT_FOLDER + csvFileName;

        JRCsvExporter exporter = new JRCsvExporter();

        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleWriterExporterOutput(filePath));

        try {
            exporter.exportReport();
        } catch (JRException e) {
            e.printStackTrace();
        }
        return new File(filePath);
    }

    public static void main(String[] args) {

    }

    private Optional<InputStream> getFileInputStream(String fileName) {

        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            final Path path = Paths.get(classLoader.getResource(fileName).getFile());
            final InputStream inputStream = Files.newInputStream(path);
            return Optional.ofNullable(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

}
