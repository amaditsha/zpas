package zw.co.esolutions.zpas.controller.proofofpayments;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import zw.co.esolutions.zpas.dto.messaging.ProofOfPaymentDTO;
import zw.co.esolutions.zpas.dto.messaging.ProofOfPaymentInfo;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.services.iface.statements.StatementsService;

/**
 * Created by alfred on 14 Mar 2019
 */
@Slf4j
@Controller
@Scope("session")
@RequestMapping("/messaging")
public class ProofOfPaymentController {

    @Autowired
    private StatementsService statementsService;

    @ResponseBody
    @PostMapping("/proof-of-payment")
    public ProofOfPaymentDTO getUserDocuments(Model model, @RequestBody ProofOfPaymentDTO proofOfPaymentDTO) {
        final ProofOfPaymentInfo proofOfPaymentInfo = statementsService.sendProofOfPaymentInfo(proofOfPaymentDTO);
        final Payment payment = proofOfPaymentInfo.getPayment();

        if(payment != null) {
            return proofOfPaymentDTO.toBuilder().status("success").message("Proof of payment sent!").build();
        } else {
            return proofOfPaymentDTO.toBuilder()
                    .status("fail")
                    .message("Failed to send proof of payment. Payment reference " + proofOfPaymentDTO.getPaymentId()
                            + " was not found.")
                    .build();
        }
    }
}
