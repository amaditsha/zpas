package zw.co.esolutions.zpas.controller.contactpoints;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.expression.Lists;
import zw.co.esolutions.zpas.model.Party;
import java.time.OffsetDateTime;
import zw.co.esolutions.zpas.model.Location;
import zw.co.esolutions.zpas.model.Country;
import zw.co.esolutions.zpas.enums.AddressTypeCode;
import zw.co.esolutions.zpas.model.PaymentObligation;
import zw.co.esolutions.zpas.model.PaymentProcessing;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.model.GenericIdentification;

import org.springframework.beans.factory.annotation.Autowired;
import zw.co.esolutions.zpas.model.ContactPoint;
import zw.co.esolutions.zpas.model.PostalAddress;
import zw.co.esolutions.zpas.repository.ContactPointRepository;
import zw.co.esolutions.zpas.repository.PostalAddressRepository;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by jnkomo on 05 Mar 2019
 */
@RestController("/api/contact-points")
@RequestMapping("/api/contact-points")
@Slf4j
public class ContactPointsRestController {
    @Autowired
    private PostalAddressRepository postalAddressRepository;

    @GetMapping("/{contactPointId}")
    public PostalAddress getPostalAddress(@RequestParam("contactPointId") String contactPointId) {
        log.info("The id is: {}" + contactPointId);
        Optional<PostalAddress> postalAddressOptional = postalAddressRepository.findById(contactPointId);
        return postalAddressOptional.map(postalAddress -> {
            log.info("Found address : {} " + postalAddress);
            return postalAddress;
        }).orElseGet(() -> {
            log.info("Getting default postal address");
            PostalAddress postalAddress = new PostalAddress();
            postalAddress.setAddressType(AddressTypeCode.Residential);
            postalAddress.setStreetName("What");
            postalAddress.setStreetBuildingIdentification("Not");
            postalAddress.setPostCodeIdentification("Is ");
            postalAddress.setTownName("sdfsd");
            postalAddress.setState("sdfs");
            postalAddress.setBuildingName("sfdfsf");
            postalAddress.setFloor("sfsfd");
            postalAddress.setDistrictName("sdfrf");
            postalAddress.setRegionIdentification("rfrfr");
            postalAddress.setCountyIdentification("");
            postalAddress.setPostOfficeBox("");
            postalAddress.setProvince("");
            postalAddress.setDepartment("");
            postalAddress.setSubDepartment("");
            postalAddress.setLocation(null);
            postalAddress.setCountry(null);
            postalAddress.setSuiteIdentification("");
            postalAddress.setBuildingIdentification("");
            postalAddress.setMailDeliverySubLocation("");
            postalAddress.setBlock("");
            postalAddress.setLot("");
            postalAddress.setDistrictSubDivisionIdentification("");
            postalAddress.setId("");
            postalAddress.setDateCreated(OffsetDateTime.now());
            postalAddress.setLastUpdated(OffsetDateTime.now());
            postalAddress.setEntityStatus(EntityStatus.ACTIVE);
            postalAddress.setEntityVersion(0L);
            postalAddress.setIdentification(null);
            postalAddress.setRelatedParty(null);
            postalAddress.setBICAddress("");
            postalAddress.setStoredDocument(null);
            postalAddress.setRemittanceRelatedPayment(null);
            postalAddress.setMainContact(null);
            postalAddress.setReturnAddress(null);
            postalAddress.setRelatedPayment(null);
            postalAddress.setTemporaryIndicator(false);
            return postalAddress;
        });
    }

    @GetMapping("/hello/weed")
    public String hello(){
        return "The weed you are looking for was not found!!!!!!";
    }

    @PostMapping("/")
    public PostalAddress getPostalAddress(@RequestParam("contactPointId") PostalAddress postalAddress) {
        log.info("About to save object: {}" + postalAddress);
        PostalAddress save = postalAddressRepository.save(postalAddress);
        return save;
    }
}
