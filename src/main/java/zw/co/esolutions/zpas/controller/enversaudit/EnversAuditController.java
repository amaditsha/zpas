package zw.co.esolutions.zpas.controller.enversaudit;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.model.enversaudit.AccountAuditResult;
import zw.co.esolutions.zpas.model.enversaudit.AuditedRevisionEntity;
import zw.co.esolutions.zpas.model.enversaudit.UserRoleAuditResult;
import zw.co.esolutions.zpas.security.model.*;
import zw.co.esolutions.zpas.services.iface.enversaudit.AuditAccountService;
import zw.co.esolutions.zpas.services.iface.enversaudit.AuditReportService;
import zw.co.esolutions.zpas.services.iface.enversaudit.AuditUserRoleService;
import zw.co.esolutions.zpas.utilities.audit.Auditable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/audit-trails")
public class EnversAuditController {
    @Autowired
    private AuditUserRoleService auditUserRoleService;

    @Autowired
    private AuditAccountService auditAccountService;

    @Autowired
    private AuditReportService auditReportService;

    private final List<Class<? extends Auditable>> classes;

    public EnversAuditController() {
        this.classes = new ArrayList<>();
        classes.addAll(Arrays.asList(UserRole.class, Profile.class, Account.class, FinancialInstitution.class, Person.class, NonFinancialInstitution.class, PersonIdentification.class, OrganisationIdentification.class, ContactPoint.class));
    }

    @ResponseBody
    @GetMapping("/userrole/{id}")
    public List<UserRoleAuditResult> auditUserRole(@PathVariable(name = "id") String id){
        List<UserRoleAuditResult> userRoleAuditResults = auditUserRoleService.auditUserRoleRevisions(id);
        log.info("Audit results: {}", userRoleAuditResults);
        return userRoleAuditResults;
    }

    @ResponseBody
    @GetMapping("/account/{id}")
    public List<AccountAuditResult> auditAccount(@PathVariable(name = "id") String id){
        List<AccountAuditResult> accountAuditResults = auditAccountService.auditAccount(id);
        log.info("Audit results: {}", accountAuditResults);
        return accountAuditResults;
    }

    @ResponseBody
    @GetMapping("/revisions/{id}")
    public List<AuditedRevisionEntity> auditRevisions(@PathVariable(name = "id") String id){
        List<AuditedRevisionEntity> auditedRevisionEntities = auditUserRoleService.auditRevisions(id);
        return auditedRevisionEntities;
    }

    @ResponseBody
    @GetMapping("/revisions/{username}/{entityName}")
    public List auditTrails(@PathVariable("username") String username, @PathVariable("entityName") String entityName){
        return getAudits(username, entityName);
    }

    @GetMapping("/")
    public String auditTrailPage(Model model, @RequestParam(name = "username", required = false) String username, @RequestParam(name = "entityName", required = false) String entityName){
        model.addAttribute("classes", classes);
        if(StringUtils.isEmpty(username) && StringUtils.isEmpty(entityName)) {
            log.info("No parameters provided.");
            return "enversaudit/audit_report";
        } else {
            final List audits = getAudits(username, entityName);
            log.info("Parameters were provided for this request: Username {}, Entity {}", username, entityName);
            model.addAttribute("audits", audits);
            model.addAttribute("username", username);
            return "enversaudit/audit_report";

        }
    }

    private List getAudits(String username, String entityName) {
        return classes.stream().filter(cl -> cl.getSimpleName().equals(entityName))
                .findAny().map(aClass -> auditReportService.getAuditEntries(username, aClass))
                .orElse(new ArrayList());
    }


}
