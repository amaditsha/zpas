package zw.co.esolutions.zpas.controller.profile;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.profile.ProfileDTO;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.Person;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.security.services.iface.profile.ProfileService;
import zw.co.esolutions.zpas.security.services.iface.userrole.UserRoleService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.ProfileType;
import zw.co.esolutions.zpas.utilities.enums.RoleLevel;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by mabuza on 06 Mar 2019
 */
@Slf4j
@Controller
public class UserProfileController {
    @Autowired
    private ProfileService profileService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private ClientService clientService;

    @GetMapping("/profile/create-page")
    @PreAuthorize("hasAuthority('ROLE_CREATE_PROFILE')")
    public String createProfilePage(Model model, HttpSession session) {
        final String highestUserRoleLevel = (String) session.getAttribute("highestUserRoleLevel");
        model.addAttribute("title", "Create Profile");
        RoleLevel roleLevel = null;
        try {
            roleLevel = RoleLevel.valueOf(highestUserRoleLevel);
        } catch (RuntimeException re) {
            model.addAttribute("message", "Could not determine user role level.");
            return "profile/create_profile";
        }
        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        final List<RoleLevel> profileLevels = getProfileLevelsCreatableByRoleLevel(roleLevel);
        model.addAttribute("financialInstitutions", financialInstitutions);
        model.addAttribute("roles", getRoles(roleLevel));
        model.addAttribute("profileLevels", profileLevels);
        return "profile/create_profile";
    }

    private List<RoleLevel> getProfileLevelsCreatableByRoleLevel(RoleLevel roleLevel) {
        switch (roleLevel) {
            case FINANCIAL_INSTITUTION:
                return Arrays.stream(RoleLevel.values()).filter(rl -> rl != RoleLevel.SYSTEM).collect(Collectors.toList());
            case CLIENT:
                return Arrays.stream(RoleLevel.values()).filter(rl -> rl == RoleLevel.CLIENT).collect(Collectors.toList());
            case SYSTEM:
                return Arrays.stream(RoleLevel.values()).filter(rl -> rl != RoleLevel.CLIENT).collect(Collectors.toList());
            default:
                return new ArrayList<>();
        }
    }

    @PostMapping("/profile")
    @PreAuthorize("hasAuthority('ROLE_CREATE_PROFILE')")
    public String createProfile(@Valid ProfileDTO profileDTO, BindingResult bindingResult, Model model) {
        model.addAttribute("title", "Profile");

        if (bindingResult.hasErrors()) {
            return "redirect:/profile/create-page";
        }

        final Optional<Profile> createdProfileOptional = profileService.createProfile(profileDTO);
        return createdProfileOptional.map(profile -> {
            log.info("Profile created successfully -> {}", profile.getFirstNames());
            return "redirect:profile/details/" + profile.getId();
        }).orElse("redirect:/profile/create-page");
    }


    @GetMapping("/profile/details/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PROFILE')")
    public String viewUserProfile(Model model, @PathVariable(name = "id") String id) {
        final Optional<Profile> profileOptional = profileService.getProfileById(id);
        return profileOptional.map(profile -> {
            final List<Organisation> employers = new ArrayList<>();
            if(ProfileType.FINANCIAL_INSTITUTION.equals(profile.getProfileType())) {
                final String fiId = profile.getFinancialInstitutionId();
                if(!StringUtils.isEmpty(fiId)){
                    final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(fiId);
                    financialInstitutionOptional.ifPresent(financialInstitution -> {
                        employers.add(financialInstitution);
                    });
                }
            }
            if(!StringUtils.isEmpty(profile.getPersonId())) {
                final Person person = clientService.findPersonById(profile.getPersonId());
                if(person != null) {
                    employers.addAll(person.getEmployers());
                }
            }
            model.addAttribute("employers", employers);
            model.addAttribute("title", "View Profile");
            model.addAttribute("profile", profile);
            return "profile/view_profile";
        }).orElseGet(() -> {
            model.addAttribute("profile", null);
            return "profile/view_profile";
        });
    }

    @GetMapping("/profile/unlock/{profileId}")
    @PreAuthorize("hasAuthority('ROLE_LOCK_USER_PROFILE')")
    public String lockUserProfile(@PathVariable(name = "profileId") String profileId, RedirectAttributes redirectAttributes) {
        return profileService.unlockProfile(profileId).map(profile -> {
            redirectAttributes.addFlashAttribute("message", "Profile disabled successfully.");
            return "redirect:/profile/details/" + profileId;
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Profile disabling failed.");
            return "redirect:/profile/details/" + profileId;
        });
    }

    @GetMapping("/profile/disable/{profileId}")
    @PreAuthorize("hasAuthority('ROLE_DISABLE_USER_PROFILE')")
    public String disableUserProfile(@PathVariable(name = "profileId") String profileId, RedirectAttributes redirectAttributes) {
        return profileService.disableProfile(profileId).map(profile -> {
            redirectAttributes.addFlashAttribute("message", "Profile disabled successfully.");
            return "redirect:/profile/details/" + profileId;
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Profile disabling failed.");
            return "redirect:/profile/details/" + profileId;
        });
    }

    @GetMapping("/profile/enable/{profileId}")
    @PreAuthorize("hasAuthority('ROLE_ENABLE_USER_PROFILE')")
    public String enableUserProfile(@PathVariable(name = "profileId") String profileId, RedirectAttributes redirectAttributes) {
        return profileService.enableProfile(profileId).map(profile -> {
            redirectAttributes.addFlashAttribute("message", "Profile enabled successfully.");
            return "redirect:/profile/details/" + profileId;
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Profile enabling failed.");
            return "redirect:/profile/details/" + profileId;
        });
    }

    @GetMapping("/profile/approve/{id}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_USER_PROFILE')")
    public String approveUserProfile(Model model, @PathVariable(name = "id") String id) {
        final Optional<Profile> profileOptional = profileService.approveProfile(id);
        return profileOptional.map(profile -> {
            model.addAttribute("title", "View Profile");
            model.addAttribute("profile", profile);
            model.addAttribute("message", "Profile for " + profile.getFullName() + " approved.");
            return "redirect:/profile/details/" + id;
        }).orElse("redirect:/profile/details/" + id);
    }

    @GetMapping("/profile/reject/{id}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_USER_PROFILE')")
    public String rejectUserProfile(Model model, @PathVariable(name = "id") String id) {
        final Optional<Profile> profileOptional = profileService.rejectProfile(id);
        return profileOptional.map(profile -> {
            model.addAttribute("title", "View Profile");
            model.addAttribute("profile", profile);
            model.addAttribute("message", "Profile for " + profile.getFullName() + " rejected.");
            return "redirect:/profile/details/" + id;
        }).orElse("redirect:/profile/details/" + id);
    }

    @ResponseBody
    @GetMapping("/profile/applicable/roles/{roleLevel}")
    public List<UserRole> getApplicableRoles(Model model, @PathVariable(name = "roleLevel") String roleLevelString) {
        RoleLevel roleLevel = null;
        try {
            roleLevel = RoleLevel.valueOf(roleLevelString);
        } catch (RuntimeException re) {
            return new ArrayList<>();
        }
        return getRoles(roleLevel);
    }

    private List<UserRole> getRoles(RoleLevel roleLevel) {
        return userRoleService.getAllUserRoles().stream().filter(userRole -> {
            return userRole.getRoleLevel().getWeight() <= roleLevel.getWeight();
        }).collect(Collectors.toList());
    }

    @GetMapping("/profile/manage")
    @PreAuthorize("hasAuthority('ROLE_VIEW_PROFILE')")
    public String manageProfiles(Model model) {
        List<Profile> profiles = profileService.getAllProfiles();
        model.addAttribute("profiles", profiles);
        return "profile/manage_profiles";
    }

    @PostMapping("/profile/edit")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_PROFILE')")
    public ResponseEntity<?> editUserProfileDetails(@ModelAttribute("profile") Profile formProfile, Errors errors) {
        log.info("Edit the profile details in controller | " + formProfile.getId() + " name " + formProfile.getFirstNames());

        Optional<Profile> profileOptional = profileService.getProfileById(formProfile.getId());

        AjaxResponseBody result = new AjaxResponseBody();

        profileOptional.ifPresent(profile -> {

            /**
             * add the details to be edited to the person name
             * */

            profile.setFirstNames(formProfile.getFirstNames());
            profile.setLastName(formProfile.getLastName());
            profile.setEmail(formProfile.getEmail());
            profile.setMobileNumber(formProfile.getMobileNumber());
            profile.setPhoneNumber(formProfile.getPhoneNumber());
            profile.setUsername(formProfile.getUsername());
            profile.setUserRoles(formProfile.getUserRoles());

            Optional<Profile> updateProfileOptional = profileService.updateProfile(profile);

            updateProfileOptional.ifPresent(updatedProfile -> {
                if (updatedProfile.getStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                    result.setNarrative("Successfully updated.");
                    result.setResponseCode("00");

                    result.setObject(updatedProfile);
                } else {
                    result.setNarrative("The person name can't be edited");
                    result.setResponseCode("04");
                }
            });
        });

        return ResponseEntity.ok(result);
    }
}
