package zw.co.esolutions.zpas.controller.rest;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.account.AccountSearchResult;
import zw.co.esolutions.zpas.dto.account.CashAccountDTO;
import zw.co.esolutions.zpas.dto.agreement.ApprovalRequestDTO;
import zw.co.esolutions.zpas.dto.agreement.ApprovalResponseDTO;
import zw.co.esolutions.zpas.dto.currency.CurrencyCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.jsonapi.*;
import zw.co.esolutions.zpas.dto.party.VirtualCodeAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.party.VirtualIdAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.party.VirtualSuffixAvailabilityCheckDTO;
import zw.co.esolutions.zpas.dto.payments.MakePaymentDTO;
import zw.co.esolutions.zpas.dto.profile.UsernameCheckDTO;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.OrganisationRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.agreement.CashAccountMandateService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.restapi.RestApiService;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.services.impl.payments.zimra.ZimraProcessor;
import zw.co.esolutions.zpas.services.impl.payments.zimra.response.AssessmentValidationGetResponse;
import zw.co.esolutions.zpas.services.impl.payments.zimra.response.AssessmentValidationResponse;
import zw.co.esolutions.zpas.utilities.enums.SpecialPayee;
import zw.co.esolutions.zpas.utilities.util.StringUtil;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by alfred on 20 Mar 2019
 */
@Slf4j
@RestController
@Scope("session")
@RequestMapping("/api/json")
public class JsonApiController {

    @Autowired
    private ZimraProcessor zimraProcessor;

    @Autowired
    private RestApiService jsonApiService;

    @Autowired
    private AccountsService accountsService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private OrganisationRepository organisationRepository;

    @ResponseBody
    @GetMapping("/clients/search")
    public List<PersonSearchResponseDTO> searchPerson(@RequestParam("search") String search,
                                                      @RequestParam("personType") String personType,
                                                      @RequestParam("organisationId") String organisationId,
                                                      HttpSession httpSession) {
        if (StringUtils.isEmpty(organisationId)) {
            organisationId = (String) httpSession.getAttribute("financialInstitutionId");
        }
        PersonSearchDTO personSearchDTO = PersonSearchDTO.builder()
                .organisationId(organisationId)
                .personType(personType)
                .search(search)
                .build();
        log.info("Search object: {}", personSearchDTO);
        final List<PersonSearchResponseDTO> personSearchResponseDTOS = jsonApiService.searchPersons(personSearchDTO);
        log.info("Search results are: {}", personSearchResponseDTOS);
        return personSearchResponseDTOS;
    }

    @ResponseBody
    @GetMapping("/validation/national-id")
    public ValidationResponse validateNationalId(@RequestParam("nationalId") String nationalId) {
        return ValidationResponse.builder()
                .value(nationalId)
                .valid(StringUtil.validateNationalID(nationalId))
                .build();
    }

    @ResponseBody
    @GetMapping("/validation/email")
    public ValidationResponse validateEmail(@RequestParam("email") String email) {
        return ValidationResponse.builder()
                .value(email)
                .valid(StringUtil.validateEmail(email))
                .build();
    }

    @ResponseBody
    @GetMapping("/validation/bic")
    public ValidationResponse validateBIC(@RequestParam("bic") String bic) {
        return ValidationResponse.builder()
                .value(bic)
                .valid(StringUtil.validateBIC(bic))
                .build();
    }

    @ResponseBody
    @PostMapping("/clients/find/single")
    public PersonSearchResponseDTO getPersonById(@RequestBody PersonSearchDTO personSearchDTO) {
        return jsonApiService.getPersonInfoById(personSearchDTO.getPersonId()).orElse(null);
    }

    @ResponseBody
    @GetMapping("/virtual-ids/check/{virtualId}")
    public VirtualIdAvailabilityCheckDTO checkVirtualIdAvailability(@PathVariable("virtualId") String virtualId) {
        return jsonApiService.checkVirtualIdAvailability(virtualId)
                .orElse(VirtualIdAvailabilityCheckDTO.builder()
                        .partyName("")
                        .virtualId(virtualId)
                        .partyId("")
                        .available(false)
                        .build());
    }

    @ResponseBody
    @GetMapping("/virtual-suffices/check/{virtualSuffix}")
    public VirtualSuffixAvailabilityCheckDTO checkVirtualSuffixAvailability(@PathVariable("virtualSuffix") String virtualSuffix) {
        return jsonApiService.checkVirtualSuffixAvailability(virtualSuffix)
                .orElse(VirtualSuffixAvailabilityCheckDTO.builder()
                        .available(false)
                        .financialInstitutionId("")
                        .financialInstitutionName("")
                        .virtualSuffix(virtualSuffix)
                        .build());
    }

    @ResponseBody
    @GetMapping("/virtual-codes/check/{virtualCode}")
    public VirtualCodeAvailabilityCheckDTO checkVirtualCodeAvailability(@PathVariable("virtualCode") String virtualCode) {
        return jsonApiService.checkVirtualCodeAvailability(virtualCode)
                .orElse(VirtualCodeAvailabilityCheckDTO.builder()
                        .virtualCode(virtualCode)
                        .available(false)
                        .accountName("")
                        .accountId("")
                        .build());
    }

    @ResponseBody
    @GetMapping("/usernames/check")
    public UsernameCheckDTO checkUsernameAvailability(@RequestParam("username") String username) {
        return jsonApiService.checkUsernameAvailability(username)
                .orElse(UsernameCheckDTO.builder()
                        .username(username)
                        .available(false)
                        .extraInformation("An error occurred.")
                        .build());
    }

    @ResponseBody
    @GetMapping("/currency-codes/check/{currencyCode}")
    public CurrencyCodeAvailabilityCheckDTO checkCurrencyCodeAvailability(@PathVariable("currencyCode") String currencyCode) {
        return jsonApiService.checkCurrencyCodeAvailability(currencyCode)
                .orElse(CurrencyCodeAvailabilityCheckDTO.builder()
                        .currencyCode(currencyCode)
                        .available(false)
                        .build());
    }

    @ResponseBody
    @GetMapping(value = "/accounts/by/acc-number/{accountNumber}", produces = "application/json; charset=UTF-8")
    public CashAccountDTO getAccountByNumber(@PathVariable("accountNumber") String accountNumber) {
        return accountsService.findAccountByNumber(accountNumber).map(cashAccount -> {
            CashAccountDTO cashAccountDTO = new CashAccountDTO();
            cashAccountDTO.setName(cashAccount.getAccountName());
            cashAccountDTO.setNumber(cashAccount.getAccountNumber());
            return cashAccountDTO;
        }).orElse(null);
    }

    @ResponseBody
    @GetMapping("/accounts/search")
    public List<AccountSearchResult> searchAccount(@RequestParam("search") String search) {
        return jsonApiService.searchAccountByVirtualId(search)
                .map(accountSearchResult -> Arrays.asList(accountSearchResult))
                .orElse(Arrays.asList(AccountSearchResult.builder()
                        .virtualId(search)
                        .accountNumber("")
                        .accountName("")
                        .bpn("")
                        .accountId("")
                        .accountOwnerName("")
                        .accountOwnerId("")
                        .narrative("Something went wrong with backend service")
                        .build()));
    }

    @ResponseBody
    @GetMapping("/accounts/account-info")
    public AccountSearchResult getAccountInfo(@RequestParam("accountNumber") String accountNumber,
                                              @RequestParam("financialInstitutionId") String financialInstitutionId) {
        return jsonApiService.getAccountInfo(accountNumber, financialInstitutionId)
                .orElse(AccountSearchResult.builder()
                        .financialInstitutionId(financialInstitutionId)
                        .financialInstitutionId("")
                        .virtualId("")
                        .accountNumber(accountNumber)
                        .accountName("")
                        .bpn("")
                        .accountId("")
                        .accountOwnerName("")
                        .accountOwnerId("")
                        .narrative("Something went wrong with backend service")
                        .build());
    }

    @ResponseBody
    @PostMapping("/make-payment")
    public PaymentResponseDTO makePayment(@RequestBody MakePaymentApiRequestDTO makePaymentApiRequestDTO) {
        log.info("The DTO is: {}", makePaymentApiRequestDTO);
        Optional<IndividualPayment> individualPaymentOptional = null;

        final Optional<Organisation> organisationOptional = organisationRepository.findBySsr(makePaymentApiRequestDTO.getClientSsr());
        final Optional<CashAccount> cashAccountOptional = accountsService.findAccountByNumber(makePaymentApiRequestDTO.getSourceAccountNumber());
        final Optional<CashAccount> destinationCashAccountOptional = accountsService.findAccountByNumber(makePaymentApiRequestDTO.getDestinationAccountNumber());

        MakePaymentDTO makePaymentDTO = MakePaymentDTO.builder()
                .amount(makePaymentApiRequestDTO.getAmount())
                .beneficiaryName(makePaymentApiRequestDTO.getBeneficiaryName())
                .contributionScheduleId(makePaymentApiRequestDTO.getContributionScheduleId())
                .clientName(makePaymentApiRequestDTO.getClientName())
                .clientReference(makePaymentApiRequestDTO.getClientReference())
                .destinationAccountNumber(makePaymentApiRequestDTO.getDestinationAccountNumber())
                .makePaymentRequestType(makePaymentApiRequestDTO.getMakePaymentRequestType())
                .beneficiaryReference(makePaymentApiRequestDTO.getBeneficiaryReference())
                .paymentInstrument(makePaymentApiRequestDTO.getPaymentInstrument())
                .paymentType(makePaymentApiRequestDTO.getPaymentType())
                .priority(makePaymentApiRequestDTO.getPriority())
                .purpose(makePaymentApiRequestDTO.getPurpose())
                .valueDate(makePaymentApiRequestDTO.getValueDate())
                .specialPayee(SpecialPayee.NSSA.name())
                .build();

        organisationOptional.ifPresent(organisation -> {
            log.info("Found client organisation for api payment: {}", organisation.getName());
            makePaymentDTO.setClientId(organisation.getId());
        });
        cashAccountOptional.ifPresent(cashAccount -> {
            log.info("Found source account. {}", cashAccount.getAccountName());
            makePaymentDTO.setSourceAccountId(cashAccount.getId());
            Optional.ofNullable(cashAccount.getFinancialInstitution()).ifPresent(financialInstitution -> {
                log.info("Found financial institution: {}", financialInstitution.getName());
                makePaymentDTO.setSourceFinancialInstitutionId(financialInstitution.getId());
            });
        });

        destinationCashAccountOptional.ifPresent(cashAccount -> {
            log.info("Found cash account for the api call: {}", cashAccount.getAccountName());
            Optional.ofNullable(cashAccount.getFinancialInstitution()).ifPresent(financialInstitution -> {
                log.info("Found destination financial institution for api call to be: {}", financialInstitution.getName());
                makePaymentDTO.setDestinationFinancialInstitutionId(financialInstitution.getId());
            });
        });

        log.info("The final make Payment DTO for the api call is: {}", makePaymentDTO);

        try {
            individualPaymentOptional = paymentsService.makePayment(makePaymentDTO);
            return individualPaymentOptional.map(individualPayment -> PaymentResponseDTO.builder()
                    .paymentId(individualPayment.getId())
                    .responseStatus("SUCCESSFUL")
                    .endToEndId(individualPayment.getEndToEndId())
                    .build()).orElse(PaymentResponseDTO.builder().responseStatus("FAILED").build());
        } catch (PaymentRequestInvalidArgumentException e) {
            e.printStackTrace();
            return PaymentResponseDTO.builder().responseStatus("FAILED").build();
        }
    }

    @ResponseBody
    @GetMapping("/submit/{paymentId}")
    public PaymentResponseDTO submitPayment(@PathVariable("paymentId") String paymentId) {
        final Optional<Payment> paymentOptional = paymentsService.submitPayment(paymentId);
        return handlePaymentOptional(paymentOptional, "", "");
    }

    @ResponseBody
    @GetMapping("/get/client/employees/{clientSsr}")
    public List<EmployerContactPersonDTO> getClientPersons(@PathVariable("clientSsr") String clientSsr) {
        return organisationRepository.findBySsr(clientSsr).map(organisation -> {
            return clientService.getPersonsByEmployerId(organisation.getId()).stream().map(person -> {
                return EmployerContactPersonDTO.builder()
                        .emailAddress(person.getEmailAddress())
                        .name(person.getName())
                        .build();
            }).collect(Collectors.toList());
        }).orElse(new ArrayList<>());
    }

    @ResponseBody
    @GetMapping("/approve/{paymentId}")
    public PaymentResponseDTO approvePayment(@PathVariable("paymentId") String paymentId) {

        final Optional<Payment> paymentOptional = paymentsService.approvePayment(paymentId);
        return handlePaymentOptional(paymentOptional, "", "");
    }

    @ResponseBody
    @GetMapping("/zimra/assessment/{zimraAssessmentNumber}")
    public AssessmentValidationResponse getZimraAssessmentInformation(@PathVariable("zimraAssessmentNumber") String zimraAssessmentNumber) {
        return zimraProcessor.getAssessmentInfo(zimraAssessmentNumber).orElseGet(() -> {
            return AssessmentValidationResponse.builder().build();
        });
    }

    private PaymentResponseDTO handlePaymentOptional(Optional<Payment> optionalPayment, String paymentReference, String additionalInfo) {
        return optionalPayment.map(payment -> {
            return PaymentResponseDTO.builder()
                    .paymentId(payment.getId())
                    .paymentStatus(payment.getLatestPaymentStatus().map(PaymentStatus::getStatus).orElse(PaymentStatusCode.None))
                    .responseStatus("SUCCESSFUL")
                    .endToEndId(payment.getEndToEndId())
                    .build();
        }).orElse(PaymentResponseDTO.builder().endToEndId(paymentReference).paymentStatus(PaymentStatusCode.None).responseStatus("FAILED").build());
    }
}
