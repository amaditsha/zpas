package zw.co.esolutions.zpas.controller.country;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.country.CountryDTO;
import zw.co.esolutions.zpas.model.Country;
import zw.co.esolutions.zpas.services.iface.country.CountryService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RequestMapping("/country")
@Controller
@Slf4j
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_COUNTRY')")
    public String createCountryPage(Model model) {
        model.addAttribute("countryDTO", new CountryDTO());
        return "country/create_country";
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('ROLE_CREATE_COUNTRY')")
    public String createCountry(@Valid CountryDTO country, Errors errors){
        if (errors.hasErrors()) {
            return "country/create_country";
        }
        country.setId(GenerateKey.generateEntityId());
        final Country countryCreated = countryService.createCountry(country);

        return "redirect:/country/details/"+countryCreated.getId();
    }

    @GetMapping("/details/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_COUNTRY')")
    public String viewCountry(Model model, @PathVariable(name = "id") String id) {
        final Optional<Country> countryRetrieved = countryService.findCountryById(id);
        log.debug("Country Retrieved : {}, {}", id, countryRetrieved.isPresent());

        if(countryRetrieved.isPresent()){
            model.addAttribute("country", countryRetrieved.get());
            return "country/view_country";
        }else{
            return "country/view_country";
        }
    }

    @GetMapping("/manage")
    @PreAuthorize("hasAuthority('ROLE_VIEW_COUNTRIES')")
    public String manageCountries(Model model) {
        List<Country> countryList = countryService.getAllCountries();
        model.addAttribute("currencies", countryList);
        return "country/manage_currencies";
    }

    @GetMapping("/edit/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_COUNTRY')")
    public String showCountryEditPage(Model model, @PathVariable(name = "id") String id) {
        final Optional<Country> country = countryService.findCountryById(id);
        model.addAttribute("country", country.get());
        model.addAttribute("statuses", EntityStatus.values());
        return "country/edit_country";
    }

    @PostMapping("/update/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_COUNTRY')")
    public String editCountry(@Valid CountryDTO countryDTO, @PathVariable("id") String id,
                               Model model, Errors errors) {
        log.info("countryId: " + countryDTO.getId());
        Country country = this.countryService.updateCountry(countryDTO);
        model.addAttribute("country", country);
        return "redirect:country/view_country";
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_COUNTRY')")
    public String deleteCountry(@PathVariable("id") String id, RedirectAttributes redirectAttributes){
        log.info("Delete the country");
        Country country = countryService.deleteCountry(id);
        redirectAttributes.addFlashAttribute("message", "Country with code " + country.getCode() + " was deleted.");
        return "redirect:/country/details/" + country.getId();
    }
}