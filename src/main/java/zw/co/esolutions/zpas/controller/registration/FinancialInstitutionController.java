package zw.co.esolutions.zpas.controller.registration;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.registration.ClientCreationDTO;
import zw.co.esolutions.zpas.dto.registration.FinancialInstitutionRegistrationDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.Country;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.model.OrganisationIdentification;
import zw.co.esolutions.zpas.repository.OrganisationIdentificationRepository;
import zw.co.esolutions.zpas.repository.PersonRepository;
import zw.co.esolutions.zpas.services.OrganisationSearchService;
import zw.co.esolutions.zpas.services.iface.country.CountryService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.AjaxResponseBody;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 13 Feb 2019
 */
@Slf4j
@Controller
@RequestMapping("/financial-institutions")
public class FinancialInstitutionController {
    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private OrganisationIdentificationRepository organisationIdentificationRepository;

    @Autowired
    private OrganisationSearchService organisationSearchService;

    @GetMapping("/create-page")
    @PreAuthorize("hasAuthority('ROLE_CREATE_FINANCIAL_INSTITUTION')")
    public String registerFinancialInstitutionPage(Model model) {
        final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        final List<Country> countries = countryService.getAllCountries();

        model.addAttribute("title", "Register Financial Institution");
        model.addAttribute("addressTypes", AddressTypeCode.values());
        model.addAttribute("exemptionReasons", TaxExemptReasonCode.values());
        model.addAttribute("types", TaxTypeCode.values());
        model.addAttribute("countries", countries);
        model.addAttribute("basis", TaxationBasisCode.values());
        model.addAttribute("taxRateTypes", RateTypeCode.values());
        model.addAttribute("withholdingTaxTypes", WithholdingTaxRateTypeCode.values());
        model.addAttribute("taxIdentificationTypes", TaxIdentificationNumberTypeCode.values());
        model.addAttribute("financialInstitutions", financialInstitutions);

        return "registration/financial_institution_registration";
    }

    @GetMapping("/")
    @PreAuthorize("hasAuthority('ROLE_VIEW_FINANCIAL_INSTITUTION')")
    public String viewFinancialInstitutions(Model model) {
        model.addAttribute("title", "Register Financial Institution");
        model.addAttribute("addressTypes", AddressTypeCode.values());
        return "registration/view_financial_institutions";
    }

    @RequestMapping(value = "/view/all", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROLE_VIEW_FINANCIAL_INSTITUTION')")
    public String viewAllFinancialInstitutions(Model model){
        List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();
        model.addAttribute("financialInstitutions", financialInstitutions);
        //model.addAttribute("processingModes", ProcessingMode.values());
        return "registration/view_financial_institutions";
    }

    @GetMapping("/approve/{financialInstitutionId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_FINANCIAL_INSTITUTION')")
    public String approveFinancialInstitution(@PathVariable("financialInstitutionId") String financialInstitutionId, RedirectAttributes redirectAttributes, HttpSession session) {
        return financialInstitutionService.approveFinancialInstitution(financialInstitutionId).map(financialInstitution -> {
            return "redirect:/financial-institutions/details/" + financialInstitution.getId();
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Approval for Financial Institution failed. Can't be found.");
            return "redirect:/financial-institutions/details/" + financialInstitutionId;
        });
    }

    @GetMapping("/reject/{financialInstitutionId}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_FINANCIAL_INSTITUTION')")
    public String rejectFinancialInstitution(@PathVariable("financialInstitutionId") String financialInstitutionId, RedirectAttributes redirectAttributes) {
        return financialInstitutionService.rejectFinancialInstitution(financialInstitutionId).map(financialInstitution -> {
            redirectAttributes.addFlashAttribute("message", "Financial Institution  rejected.");
            return "redirect:/financial-institutions/details/" + financialInstitution.getId();
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Financial Institution rejection failed.");
            return "redirect:/financial-institutions/details/" + financialInstitutionId;
        });
    }

    @GetMapping("/submit/{financialInstitutionId}")
    public String submitFinancialInstitution(@PathVariable("financialInstitutionId") String financialInstitutionId, RedirectAttributes redirectAttributes) {
        return financialInstitutionService.submitFinancialInstitution(financialInstitutionId).map(financialInstitution -> {
            redirectAttributes.addFlashAttribute("message", "You have successfully submitted Financial Institution.");
            return "redirect:/financial-institutions/details/" + financialInstitution.getId();
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Financial Institution submission failed.");
            return "redirect:/financial-institutions/details/" + financialInstitutionId;
        });
    }

    @GetMapping("/delete/{financialInstitutionId}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_FINANCIAL_INSTITUTION')")
    public String deleteFinancialInstitution(@PathVariable("financialInstitutionId") String financialInstitutionId, RedirectAttributes redirectAttributes) {
        return financialInstitutionService.deleteFinancialInstitution(financialInstitutionId).map(financialInstitution -> {
            redirectAttributes.addFlashAttribute("message", "Financial Institution deleted.");
            return "redirect:/financial-institutions/";
        }).orElseGet(() -> {
            redirectAttributes.addFlashAttribute("message", "Financial Institution deletion failed.");
            return "redirect:/financial-institutions/details/" + financialInstitutionId;
        });
    }

    @GetMapping("/details/{id}")
    @PreAuthorize("hasAuthority('ROLE_VIEW_FINANCIAL_INSTITUTION')")
    public String viewFinancialInstitutionById(Model model, @PathVariable(name = "id") String id) {
        final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(id);
        return financialInstitutionOptional.map(financialInstitution -> {
            log.info("FI is {}", financialInstitution);
            model.addAttribute("financialInstitution", financialInstitution);
            model.addAttribute("title", "View Financial Institution");
            model.addAttribute("identification", new OrganisationIdentification());
            model.addAttribute("genders", GenderCode.values());
            model.addAttribute("languages", LanguageCode.values());
            model.addAttribute("civilStatuses", CivilStatusCode.values());
            model.addAttribute("namePrefixes", NamePrefix1Code.values());
            log.info("Other BICS: {}", financialInstitution.getOrganisationIdentification().get(0).getOtherBIC());
            model.addAttribute("otherBICList", financialInstitution.getOrganisationIdentification().get(0).getOtherBIC());

            log.info("<<<< sort code returned is >>>> {} ", financialInstitution.getSortCodes());
            return "registration/view_financial_institution";
        }).orElseGet(() -> {
            model.addAttribute("financialInstitution", null);
            return "registration/view_financial_institution";
        });
    }

    @PostMapping("/")
    @PreAuthorize("hasAuthority('ROLE_CREATE_FINANCIAL_INSTITUTION')")
    public String registerFinancialInstitution(@Valid FinancialInstitutionRegistrationDTO financialInstitutionRegistrationDTO,
                                               BindingResult bindingResult, Model model, HttpSession session) {
        model.addAttribute("title", "Financial Institutions");
        if (bindingResult.hasErrors()) {
            return "registration/financial_institution_registration";
        }

        log.info("other BIC {} ", financialInstitutionRegistrationDTO.getOtherBIC());

        final FinancialInstitution financialInstitutionCreated =
                financialInstitutionService.registerFinancialInstitution(financialInstitutionRegistrationDTO);

        session.setAttribute("fiId", financialInstitutionCreated.getId());
        log.info("fiId in session: " + session.getAttribute("fiId"));

        return "redirect:/financial-institutions/details/" + financialInstitutionCreated.getId();
    }

    @GetMapping("/update/{id}")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_FINANCIAL_INSTITUTION')")
    public String showFinancialInstitutionEditPage(@PathVariable(name = "id") String id, Model model) {
        final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(id);

        return financialInstitutionOptional.map(financialInstitution -> {
            log.info("FI is {}", financialInstitution);
            FinancialInstitutionRegistrationDTO institutionRegistrationDTO = generateFinancialInstitutionRegistrationDTOFromFI(financialInstitution);
            model.addAttribute("financialInstitution", financialInstitution);
            model.addAttribute("title", "Update Financial Institution");
            model.addAttribute("institutionRegistrationDTO", institutionRegistrationDTO);
            model.addAttribute("addressTypes", AddressTypeCode.values());
            return "registration/update_financial_institution";
        }).orElseGet(() -> {
            model.addAttribute("financialInstitution", null);
            return "registration/view_financial_institution";
        });
    }


    @PutMapping("/update")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_FINANCIAL_INSTITUTION')")
    public String updateFinancialInstitution(@Valid FinancialInstitutionRegistrationDTO updateInstitutionRegistrationDTO, BindingResult bindingResult, Model model, HttpSession session){

        if (bindingResult.hasErrors()) {
            return "registration/update_financial_institution";
        }

        final FinancialInstitution financialInstitutionUpdated =
                financialInstitutionService.updateFinancialInstitution(updateInstitutionRegistrationDTO);

        session.setAttribute("fiId", financialInstitutionUpdated.getId());
        log.info("fiId in session: " + session.getAttribute("fiId"));

        return "redirect:/financial-institutions/details/" + financialInstitutionUpdated.getId();

    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROLE_SEARCH_FINANCIAL_INSTITUTIONS')")
    public String searchOrganisation(@RequestParam(value = "search", required = false) String q, Model model) {
        List<Organisation> searchResults = null;
        log.info("Search string: " + q);
        try {
            searchResults = organisationSearchService.searchOrganisation(q);

        } catch (Exception ex) {
            // here you should handle unexpected errors
            // ...
            //throw ex;
            searchResults = new ArrayList<>();
        }
        log.info("Search results are: {}", searchResults);
        financialInstitutionService.refreshOrganisationIndex();
        model.addAttribute("search", searchResults);
        return "registration/search_organisation";

    }

    public FinancialInstitutionRegistrationDTO generateFinancialInstitutionRegistrationDTOFromFI(FinancialInstitution financialInstitution){
        FinancialInstitutionRegistrationDTO dto = new FinancialInstitutionRegistrationDTO();
        dto.setId(financialInstitution.getId());

        dto.setPurpose(financialInstitution.getPurpose() == null ? "" :financialInstitution.getPurpose());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        dto.setRegistrationDay(financialInstitution.getRegistrationDate().format(formatter));

        dto.setParentOrganisationId(financialInstitution.getParentOrganisation() == null ? "" : financialInstitution.getParentOrganisation().getId());
        dto.setDescription(financialInstitution.getDescription() == null ? "" : financialInstitution.getDescription());
        dto.setEstablishmentDate(financialInstitution.getEstablishmentDate().format(formatter));

        dto.setRoles(financialInstitution.getRole() == null ? "": financialInstitution.getRole().toString());

        dto.setBicFIIdentifier(financialInstitution.getOrganisationIdentification().get(0).getBICFI());
        dto.setAnyBICIdentifier(financialInstitution.getOrganisationIdentification().get(0).getAnyBIC());

        if(financialInstitution.getOrganisationIdentification().size() > 0) {
            dto.setLegalName(financialInstitution.getOrganisationIdentification().get(0).getOrganisationName().get(0).getLegalName());
            dto.setTradingName(financialInstitution.getOrganisationIdentification().get(0).getOrganisationName().get(0).getTradingName());
            dto.setShortName(financialInstitution.getOrganisationIdentification().get(0).getOrganisationName().get(0).getShortName());
        }

        /*    private String phonesJson;
        private String addressesJson;
        private String postalAddressesJson;*/

        dto.setPlcOfRegAddressType(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getAddressType().name());

        //Postal Address Fields
        dto.setStreetName(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getStreetName());
        dto.setStreetBuildingIdentification(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getStreetBuildingIdentification());
        dto.setPostCodeIdentification(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getPostCodeIdentification());
        dto.setTownName(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getTownName());
        dto.setState(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getState());
        dto.setBuildingName(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getBuildingName());
        dto.setFloor(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getFloor());
        dto.setDistrictName(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getDistrictName());
        dto.setPostOfficeBox(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getPostOfficeBox());
        dto.setProvince(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getProvince());
        dto.setDepartment(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getDepartment());
        dto.setSubDepartment(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getSubDepartment());
        dto.setSuiteIdentification(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getSuiteIdentification());
        dto.setBuildingIdentification(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getBuildingIdentification());
        dto.setMailDeliverySubLocation(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getMailDeliverySubLocation());
        dto.setBlock(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getBlock());
        dto.setDistrictSubDivisionIdentification(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getDistrictSubDivisionIdentification());
        dto.setLot(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getLot());

        dto.setPlcOfRegCountryId(financialInstitution.getPlaceOfRegistration().getAddress().get(0).getCountyIdentification());

        dto.setTaxConditionsId(financialInstitution.getTaxationConditions().getId());
        dto.setValidityPeriodStartDate(financialInstitution.getValidityPeriod().getFromDateTime().format(formatter));
        dto.setValidityPeriodEndDate(financialInstitution.getValidityPeriod().getToDateTime().format(formatter));

        return dto;
    }

    @PostMapping("/edit/general/information/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_FINANCIAL_INSTITUTION')")
    public ResponseEntity<?> editGeneralInformationDetails(@ModelAttribute("financialInstitution") FinancialInstitution formFI, Errors errors){
        log.info("Editing financial institution details in controller | " + formFI.getId());

        Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(formFI.getId());
        log.info("FI general details found? : {} " + financialInstitutionOptional);

        AjaxResponseBody result = new AjaxResponseBody();

        financialInstitutionOptional.ifPresent(financialInstitution -> {

            log.info("Setting old value {} " + financialInstitution.getPurpose() + " to -> new value {} " + formFI.getPurpose());
            financialInstitution.setPurpose(formFI.getPurpose());
            if(financialInstitution.getTaxationConditions() != null && financialInstitution.getTaxationConditions().getId() == null) {
                financialInstitution.setTaxationConditions(null);
            }


            FinancialInstitution updatedFinancialInstitution = financialInstitutionService.updateFinancialInstitutionGeneralInformation(financialInstitution);
            log.info("FI Purpose after updating: {} " + updatedFinancialInstitution.getPurpose());

            if(updatedFinancialInstitution.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                GeneralnformationTemp generalnformationTemp = new GeneralnformationTemp();
                generalnformationTemp.setId(updatedFinancialInstitution.getId());
                generalnformationTemp.setPurpose(updatedFinancialInstitution.getPurpose());

                result.setObject(generalnformationTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The general information can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }

    @PostMapping("/edit/organisation/identification/details")
    @PreAuthorize("hasAuthority('ROLE_UPDATE_FINANCIAL_INSTITUTION')")
    public ResponseEntity<?> editOrganisationIdentificationDetails(@ModelAttribute("organisationIdentification") OrganisationIdentification formOrganisationIdentification, Errors errors){
        log.info("Edit the organisation identification details in controller | " + formOrganisationIdentification.getId());

        Optional<OrganisationIdentification> organisationIdentificationOptional = organisationIdentificationRepository.findById(formOrganisationIdentification.getId());
        log.info("Organisation Identification details found : {} ", organisationIdentificationOptional.isPresent());

        AjaxResponseBody result = new AjaxResponseBody();

        organisationIdentificationOptional.ifPresent(organisationIdentification -> {
            log.info("Setting old value {} " + organisationIdentification.getBICFI() + " to -> new value {} " + formOrganisationIdentification.getBICFI());
            organisationIdentification.setBICFI(formOrganisationIdentification.getBICFI());
            organisationIdentification.setAnyBIC(formOrganisationIdentification.getAnyBIC());
            //formOrganisationIdentification.getOrganisationName().get(0).getLegalName());

            OrganisationIdentification updatedOrganisationIdentification = financialInstitutionService.updateFinancialInstitutionOrganisatinIdentification(organisationIdentification);
            log.info("BIC after updating: {} " + updatedOrganisationIdentification.getBICFI());

            if(updatedOrganisationIdentification.getEntityStatus().equals(EntityStatus.PENDING_EDIT_APPROVAL)) {
                result.setNarrative("Successfully updated.");
                result.setResponseCode("00");

                OrganisationIdentificationTemp organisationIdentificationTemp = new OrganisationIdentificationTemp();
                organisationIdentificationTemp.setId(updatedOrganisationIdentification.getId());
                organisationIdentificationTemp.setBICFI(updatedOrganisationIdentification.getBICFI());
                organisationIdentificationTemp.setAnyBICIdentifier(updatedOrganisationIdentification.getAnyBIC());
                organisationIdentificationTemp.setLegalName(updatedOrganisationIdentification.getOrganisationName().get(0).getLegalName());
                organisationIdentificationTemp.setShortName(updatedOrganisationIdentification.getOrganisationName().get(0).getShortName());
                organisationIdentificationTemp.setTradingName(updatedOrganisationIdentification.getOrganisationName().get(0).getTradingName());
                result.setObject(organisationIdentificationTemp);
                log.info("Result object is {} : " + result.getObject().toString());
            } else {
                result.setNarrative("The organisation identification can't be edited");
                result.setResponseCode("04");
            }
        });


        return ResponseEntity.ok(result);
    }


    @Data
    public class GeneralnformationTemp{
        protected String id;
        protected String purpose;
    }

    @Data
    public class OrganisationIdentificationTemp{
        protected String id;
        protected String bICFI;
        protected String anyBICIdentifier;
        protected String legalName;
        protected String tradingName;
        protected String shortName;
    }

}
