package zw.co.esolutions.zpas.controller.debitorder;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import zw.co.esolutions.zpas.dto.debitorder.DebitOrderBatchDTO;
import zw.co.esolutions.zpas.dto.debitorder.DebitOrderReportDTO;
import zw.co.esolutions.zpas.enums.PaymentInstrumentCode;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.RolePlayerRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.currency.CurrencyService;
import zw.co.esolutions.zpas.services.iface.debitorder.DebitOrderService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.impl.debitorder.DebitOrderBatchUploadsProcessor;
import zw.co.esolutions.zpas.services.impl.payments.exceptions.PaymentRequestInvalidArgumentException;
import zw.co.esolutions.zpas.utilities.storage.StorageService;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
@Scope("session")
@RequestMapping("/debit-orders")
public class DebitOrdersController {
    @Autowired
    private FinancialInstitutionService financialInstitutionService;
    @Autowired
    private RolePlayerRepository rolePlayerRepository;

    @Autowired
    private DebitOrderService debitOrderService;

    @Autowired
    private AccountsService accountService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private DebitOrderBatchUploadsProcessor batchUploadsProcessor;


    @GetMapping("/upload")
    @PreAuthorize("hasAuthority('ROLE_UPLOAD_DEBIT_ORDER_BATCH')")
    public String uploadDebitOrderBatchPage(Model model, HttpSession session,
                                        @RequestParam(value = "accountId", required = false) String accountId) {
        String clientId = (String) session.getAttribute("loginAsId");

        final List<Account> clientAccounts = accountService.getAccountsByClientId(clientId);
        final List<PaymentInstrumentCode> paymentInstrumentCodes = Arrays.asList(PaymentInstrumentCode.CustomerCreditTransfer);

        if (!StringUtils.isEmpty(accountId)) {
            Optional.ofNullable(accountService.findAccountById(accountId)).ifPresent(account -> {
                model.addAttribute("account", account);
            });
        }

        model.addAttribute("title", "Upload Debit Order Batch");
        model.addAttribute("clientAccounts", clientAccounts);
        model.addAttribute("currencies", currencyService.getAllCurrencies());
        return "debitorder/upload_debit_order_batch";
    }


    @PostMapping("/upload")
    @PreAuthorize("hasAuthority('ROLE_UPLOAD_DEBIT_ORDER_BATCH')")
    public String uploadDebitOrderBatch(@RequestParam("debitOrdersFile") MultipartFile debitOrdersFile,
                                    @RequestParam("description") String description,
                                    @RequestParam("processingDate") String processingDate,
                                    @RequestParam("targetAccount") String targetAccount,
                                    @RequestParam("currencyCode") String currencyCode,
                                    RedirectAttributes redirectAttributes, HttpSession session) {
        if(debitOrdersFile == null) {
            redirectAttributes.addFlashAttribute("error", "Tried to upload an empty file, not allowed.");
            return "redirect:/debit-orders/upload";
        }

        String clientId = (String) session.getAttribute("loginAsId");
        Optional<RolePlayer> rolePlayerOptional = rolePlayerRepository.findById(clientId);
        if(rolePlayerOptional.isPresent()) {
            log.info("Role player is available ...");
            RolePlayer rolePlayer = rolePlayerOptional.get();
            if (rolePlayer instanceof NonFinancialInstitution) {
                log.info("The role player is a non FI");
                NonFinancialInstitution nonFinancialInstitution = (NonFinancialInstitution) rolePlayer;
                if(nonFinancialInstitution.getEnableDebitOrder() != null && nonFinancialInstitution.getEnableDebitOrder()) {
                    log.info("The role player can send the debit order ");
                } else {
                    log.info("The non FI is not enabled to send debit order.");
                    redirectAttributes.addFlashAttribute("error", "Client is not enabled to send debit orders.");
                    return "redirect:/debit-orders/upload";
                }
            } else {
                log.info("The role player  is | {}", rolePlayer.getClass().getName());
                log.info("The client is not enabled to send debit orders");
                redirectAttributes.addFlashAttribute("error", "Client is not enabled to send debit orders.");
                return "redirect:/debit-orders/upload";
            }
        } else {
            log.info("The client is not a role player.");
            redirectAttributes.addFlashAttribute("error", "Client is not enabled to send debit orders.");
            return "redirect:/debit-orders/upload";
        }

        try {
            storageService.store(debitOrdersFile);
        } catch (RuntimeException re) {
            redirectAttributes.addFlashAttribute("error", "An error occurred trying to save file. " + re.getMessage());
            return "redirect:/debit-orders/upload";
        }

//        String clientName = (String) session.getAttribute("clientName");

        DateTimeFormatter dateTime_localFomatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        LocalDateTime localDate = LocalDateTime.parse(processingDate.replace("T", " "), dateTime_localFomatter);
        OffsetDateTime processingDate_1 = localDate.atOffset(OffsetDateTime.now().getOffset());

        final DebitOrderBatchDTO debitOrderBatchDTO = new DebitOrderBatchDTO();
        debitOrderBatchDTO.setFileName(debitOrdersFile.getOriginalFilename());
        debitOrderBatchDTO.setTargetAccount(targetAccount);
        debitOrderBatchDTO.setDescription(description);
        debitOrderBatchDTO.setProcessingDate(processingDate_1);
        debitOrderBatchDTO.setClientId(clientId);
        debitOrderBatchDTO.setCurrencyCode(currencyCode);
//        debitOrderBatchDTO.setClientName(clientName);
        log.info("Batch Upload DTO is: {}", debitOrderBatchDTO);
        try {
            return batchUploadsProcessor.uploadDebitOrderBatch(debitOrderBatchDTO).map(debitOrderBatch -> {
                redirectAttributes.addFlashAttribute("message",
                        "You successfully uploaded " + debitOrdersFile.getOriginalFilename());
                return "redirect:view/" + debitOrderBatch.getId();
            }).orElseGet(() -> {
                throw new RuntimeException("No debit order returned.");
            });
        } catch (PaymentRequestInvalidArgumentException e) {
            redirectAttributes.addFlashAttribute("error", "An error occurred. " + e.getMessage());
            return "redirect:/debit-orders/upload";
        }
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable String id, Model model) {

        log.info("In the view debit order page with id : {}", id);
        Optional<DebitOrderBatch> debitOrderBatchOptional = debitOrderService.getDebitOrderBatchById(id);

        debitOrderBatchOptional.map(debitOrderBatch -> {
            model.addAttribute("debitOrderBatch", debitOrderBatch);
            model.addAttribute("moneyUtil", new MoneyUtil());
            return "";
        }).orElseGet(() -> {
            log.error("Failed to get debit order.");
            model.addAttribute("message", "Debit order is not found.");
            return "";
        });
        return "debitorder/view_debit_order_batch";
    }

    @GetMapping("/manage")
    @PreAuthorize("hasAuthority('ROLE_VIEW_DEBIT_ORDER_BATCHES')")
    public String viewDebitOrderBatches(Model model, HttpSession session) {
        log.info("Inside manage debit orders...");
        final List<DebitOrderBatch> debitOrderBatches;

        String clientId = (String) session.getAttribute("loginAsId");
        Optional<RolePlayer> rolePlayerOptional = rolePlayerRepository.findById(clientId);
        if(rolePlayerOptional.isPresent()) {
            log.info("Role player is available ...");
            RolePlayer rolePlayer = rolePlayerOptional.get();
            if(rolePlayer instanceof Person) {
                log.info("The role player is a person");
                debitOrderBatches = debitOrderService.getDebitOrderBatchByClientId(clientId);
            } else if (rolePlayer instanceof Organisation) {
                log.info("The role player is a organisation");
                Organisation organisation = (Organisation) rolePlayer;
                String organisationBIC = organisation.getBIC();
                log.info("The organisation BIC is {}", organisationBIC);
                debitOrderBatches = debitOrderService.getDebitOrderBatchBySenderId(organisationBIC);
            } else {
                log.info("The role player class not found | {}", rolePlayer.getClass().getName());
                debitOrderBatches = new ArrayList<>();
            }
        } else {
            log.info("The client is not a role player ");
            debitOrderBatches = new ArrayList<>();
        }

        model.addAttribute("title", "Debit Order Batches");
        model.addAttribute("debitOrderBatches", debitOrderBatches);
        return "debitorder/view_debit_order_batches";
    }

    @GetMapping("/approve/{debitOrderBatchId}")
    @PreAuthorize("hasAuthority('ROLE_APPROVE_DEBIT_ORDER_BATCH')")
    public String approveDebitOrderBatch(@PathVariable("debitOrderBatchId") String debitOrderBatchId, Model model, RedirectAttributes redirectAttributes, HttpSession session) {
        Optional<DebitOrderBatch> debitOrderBatchOptional = debitOrderService.getDebitOrderBatchById(debitOrderBatchId);

        if (debitOrderBatchOptional.isPresent()) {
            debitOrderService.approveDebitOrderBatch(debitOrderBatchId) ;
            redirectAttributes.addFlashAttribute("message", "Debit order batch with reference " + debitOrderBatchOptional.get().getBatchNumber() + " approved.");
            return "redirect:/debit-orders/view/" + debitOrderBatchId;
        } else {
            redirectAttributes.addFlashAttribute("message", "Debit order batch could not be approved");
            return "redirect:/debit-orders/view/" + debitOrderBatchId;
        }
    }

    @GetMapping("/initiate/{debitOrderBatchId}")
    @PreAuthorize("hasAuthority('ROLE_INITIATE_DEBIT_ORDER_BATCH')")
    public String initiateDebitOrderBatch(@PathVariable("debitOrderBatchId") String debitOrderBatchId, Model model, RedirectAttributes redirectAttributes, HttpSession session) {
        Optional<DebitOrderBatch> debitOrderBatchOptional = debitOrderService.getDebitOrderBatchById(debitOrderBatchId);

        if (debitOrderBatchOptional.isPresent()) {
            debitOrderService.initiateDebitOrderBatch(debitOrderBatchId) ;
            redirectAttributes.addFlashAttribute("message", "Debit order batch with reference " + debitOrderBatchOptional.get().getBatchNumber() + " initiated.");
            return "redirect:/debit-orders/view/" + debitOrderBatchId;
        } else {
            redirectAttributes.addFlashAttribute("message", "Debit order batch could not be initiated");
            return "redirect:/debit-orders/view/" + debitOrderBatchId;
        }
    }

    @GetMapping("/reject/{debitOrderBatchId}")
    @PreAuthorize("hasAuthority('ROLE_REJECT_DEBIT_ORDER_BATCH')")
    public String rejectDebitOrderBatch(@PathVariable("debitOrderBatchId") String debitOrderBatchId, Model model, RedirectAttributes redirectAttributes) {
        debitOrderService.rejectDebitOrderBatch(debitOrderBatchId);
        redirectAttributes.addFlashAttribute("message", "Debit Order Batch with reference " + debitOrderBatchId + " rejected.");
        return "redirect:/debit-orders/view/" + debitOrderBatchId;
    }

    @GetMapping("/submit/{debitOrderBatchId}")
    public String submitDebitOrderBatch(@PathVariable("debitOrderBatchId") String debitOrderBatchId, Model model, RedirectAttributes redirectAttributes, HttpSession httpSession) {
        Optional<DebitOrderBatch> optionalDebitOrderBatch = debitOrderService.getDebitOrderBatchById(debitOrderBatchId);

            if (optionalDebitOrderBatch.isPresent()) {
                DebitOrderBatch debitOrderBatch = optionalDebitOrderBatch.get();
                String receiverID = debitOrderBatch.getReceiverID();


                boolean anyMatch = debitOrderBatch.getDebitOrderBatchItems().stream()
                        .anyMatch(debitOrderBatchItem -> !debitOrderBatchItem.getSourceBICFI().equalsIgnoreCase(receiverID));

                if(anyMatch) {
                    log.error("The source BIC & destination BIC are different. Can't submit the form.");
                    redirectAttributes.addFlashAttribute("error", "Can not be processed. Found Source & destination BIC different for D.O with reference " + debitOrderBatch.getBatchNumber());
                    return "redirect:/debit-orders/view/" + debitOrderBatchId;
                }

                debitOrderService.submitDebitOrderBatch(debitOrderBatchId);
                redirectAttributes.addFlashAttribute("message", "You successfully submitted debit order batch with reference " + debitOrderBatch.getBatchNumber());
            } else {
                redirectAttributes.addFlashAttribute("error", "Failed to submit debit order bacth.");
            }
            return "redirect:/debit-orders/view/" + debitOrderBatchId;

    }


    @GetMapping("/delete/{debitOrderBatchId}")
    @PreAuthorize("hasAuthority('ROLE_DELETE_DEBIT_ORDER_BATCH')")
    public String deleteDebitOrderBatch(@PathVariable("debitOrderBatchId") String debitOrderBatchId, Model model, RedirectAttributes redirectAttributes) {
        debitOrderService.deleteDebitOrderBatch(debitOrderBatchId);
        redirectAttributes.addFlashAttribute("error", "Debit Order Batch with reference " + debitOrderBatchId + " deleted.");
        return "redirect:/debit-orders/view/" + debitOrderBatchId;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String searchDebitOrders(@RequestParam(value = "searchTerm", required = false) String searchTerm, Model model){

        List<DebitOrderBatch> debitOrderBatches = new ArrayList<>();
        try {
            debitOrderBatches = debitOrderService.searchDebitOrderBatches(searchTerm);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        log.info("Search results are: {}", debitOrderBatches);
        debitOrderService.refreshDebitOrderBatchIndex();
        model.addAttribute("debitOrderBatches", debitOrderBatches);
        return "debitorder/view_debit_order_batches";
    }

    @GetMapping("/search")
    public String searchDebitOrdersByDate(@Valid DebitOrderReportDTO debitOrderReportDTO, Model model){
        List<DebitOrderBatch> debitOrderBatches = new ArrayList<>();
        debitOrderBatches = debitOrderService.searchByDate(debitOrderReportDTO);

        model.addAttribute("debitOrderBatches", debitOrderBatches);
        return "debitorder/view_debit_order_batches";

    }
    @GetMapping("/recent")
    public String viewRecentDebitOrders(Model model, HttpSession httpSession){
        List<DebitOrderBatch> debitOrderBatches = new ArrayList<>();
        String receiverID = (String) httpSession.getAttribute("bicFi");
        debitOrderBatches = debitOrderService.findTop50ByReceiverID(receiverID);

        model.addAttribute("debitOrderBatches", debitOrderBatches);
        return "debitorder/view_debit_order_batches";

    }


}
