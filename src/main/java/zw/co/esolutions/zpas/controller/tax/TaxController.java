package zw.co.esolutions.zpas.controller.tax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import zw.co.esolutions.zpas.model.Tax;
import zw.co.esolutions.zpas.services.iface.tax.TaxService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class TaxController {
    @Autowired
    TaxService taxService;

    @GetMapping("/create-tax")
    public String createTaxForm(Tax tax) {
        return "tax/create_tax";
    }

    @PostMapping("/create-tax")
    public String createTax(@Valid Tax tax, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "create-tax";
        }
        taxService.createTax(tax);
        model.addAttribute("taxes", taxService.createTax(tax));
        return "index";
    }

    //  to pass id in the route
    @GetMapping("/view-tax/details")
    public String viewTaxByIdForm(Model model) {
        return "tax/view_tax";
    }

    @GetMapping("/view")
    public String viewTaxById(Model model) {
        return "tax/view";
    }
}