package zw.co.esolutions.zpas.integration.camel.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.hibernate.engine.jdbc.batch.spi.Batch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.integration.camel.intermediate.PaymentIdentificationVerificationTimeoutProcessor;

import javax.transaction.Transactional;

/**
 * Created by mabuza on 09 May 2019
 */

@Component
@Slf4j
@Transactional
public class CronJobRoutes extends RouteBuilder {
    @Autowired
    private PaymentIdentificationVerificationTimeoutProcessor paymentIdentificationVerificationTimeoutProcessor;

    @Override
    public void configure() throws Exception {

        from("timer://paymentIdentificationVerificationTimeoutProcessor?fixedRate=true&period=120s")
                .log("Running payment verification timeout processor...")
                .process(paymentIdentificationVerificationTimeoutProcessor)
                .log("Payment verification timeout processor ended.")
        .end();
    }
}
