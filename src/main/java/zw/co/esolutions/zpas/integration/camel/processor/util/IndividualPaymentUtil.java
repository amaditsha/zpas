package zw.co.esolutions.zpas.integration.camel.processor.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.payments.MakePaymentDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.MakePaymentRequestType;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
@Transactional
public class IndividualPaymentUtil {
    private final AccountsService accountService;
    private final FinancialInstitutionService financialInstitutionService;
    private final CustomPartyRepository customPartyRepository;
    private final PaymentPartyRoleRepository paymentPartyRoleRepository;
    private final SchemeRepository schemeRepository;
    private final IndividualPaymentRepository individualPaymentRepository;
    private final PaymentStatusRepository paymentStatusRepository;

    @Autowired
    public IndividualPaymentUtil(
            AccountsService accountService,
            FinancialInstitutionService financialInstitutionService,
            CustomPartyRepository customPartyRepository,
            PaymentPartyRoleRepository paymentPartyRoleRepository,
            SchemeRepository schemeRepository,
            IndividualPaymentRepository individualPaymentRepository,
            PaymentStatusRepository paymentStatusRepository
    ) {
        this.individualPaymentRepository = individualPaymentRepository;
        this.customPartyRepository = customPartyRepository;
        this.accountService = accountService;
        this.financialInstitutionService = financialInstitutionService;
        this.paymentPartyRoleRepository = paymentPartyRoleRepository;
        this.schemeRepository = schemeRepository;
        this.paymentStatusRepository = paymentStatusRepository;
    }

    public Optional<IndividualPayment> createIndividualPayment(MakePaymentDTO makePaymentDTO) {
        final IndividualPayment individualPayment = createIndividualPaymentHelper(makePaymentDTO);
        final List<PaymentPartyRole> paymentPartyRoles = individualPayment.getPartyRole();
        final List<PaymentStatus> paymentStatuses = individualPayment.getPaymentStatus();
        individualPayment.setPartyRole(new ArrayList<>());
        individualPayment.setPaymentStatus(new ArrayList<>());
        final IndividualPayment savedIndividualPayment = individualPaymentRepository.save(individualPayment);
        paymentPartyRoleRepository.saveAll(paymentPartyRoles);
        paymentStatusRepository.saveAll(paymentStatuses);
        return Optional.ofNullable(savedIndividualPayment);
    }

    private IndividualPayment createIndividualPaymentHelper(MakePaymentDTO makePaymentDTO) {
        PaymentTypeCode paymentTypeCode = null;
        OffsetDateTime valueDate = null;

        try {
            paymentTypeCode = PaymentTypeCode.valueOf(makePaymentDTO.getPaymentType());
            log.info("Value date is -->: {}", makePaymentDTO.getValueDate());
            final LocalDate localDate = LocalDate.parse(makePaymentDTO.getValueDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            valueDate = LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
        } catch (RuntimeException re) {
            re.printStackTrace();
        }
        final CashAccount sourceAccount = accountService.findAccountById(makePaymentDTO.getSourceAccountId());

        IndividualPayment individualPayment = new CreditTransfer();
        individualPayment.setPaymentObligation(new ArrayList<>());
        individualPayment.setCurrencyOfTransfer(sourceAccount.getBaseCurrency());
        individualPayment.setCreditMethod(new ArrayList<>());
        individualPayment.setType(paymentTypeCode);

        log.info("Configuring payment information.");
        configurePaymentInformationForRequestType(individualPayment, makePaymentDTO);

        final CurrencyAndAmount instructedCurrencyAndAmount = new CurrencyAndAmount();
        instructedCurrencyAndAmount.setAmount(new BigDecimal(makePaymentDTO.getAmount()));
        instructedCurrencyAndAmount.setCurrency(sourceAccount.getBaseCurrency());
        individualPayment.setInstructedAmount(instructedCurrencyAndAmount);
        individualPayment.setPriority(PriorityCode.valueOf(makePaymentDTO.getPriority()));
        individualPayment.setValueDate(Optional.ofNullable(valueDate).orElse(OffsetDateTime.now()));
        final PaymentStatus paymentStatus = new PaymentStatus();
        paymentStatus.setStatus(PaymentStatusCode.PendingCancellationRequest);
        paymentStatus.setPayment(individualPayment);
        paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
        paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
        paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
        paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.None);
        paymentStatus.setTransactionRejectionReason(TransactionReasonCode.None);
        paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
        paymentStatus.setTransactionStatus(TransactionStatusCode.None);
        paymentStatus.setCashPaymentStatus(CashPaymentStatusCode.None);
        paymentStatus.setCancellationReason(CancellationReasonCode.None);
        paymentStatus.setMandateRejectionReason(MandateReasonCode.None);
        paymentStatus.setPendingFailingSettlement(PendingFailingSettlementCode.None);
        paymentStatus.setId(GenerateKey.generateEntityId());
        paymentStatus.setEntityStatus(EntityStatus.PENDING_APPROVAL);
        paymentStatus.setDateCreated(OffsetDateTime.now());
        paymentStatus.setLastUpdated(OffsetDateTime.now());
        paymentStatus.setEntityVersion(0L);
        paymentStatus.setStatusDateTime(OffsetDateTime.now());
        paymentStatus.setStatusDescription("Initial Status");
        paymentStatus.setSettlementStatus(SecuritiesSettlementStatusCode.None);
        paymentStatus.setCancellationProcessingStatus(CancellationProcessingStatusCode.None);
        paymentStatus.setTransactionProcessingStatus(InstructionProcessingStatusCode.None);
        paymentStatus.setModificationProcessingStatus(ModificationProcessingStatusCode.None);

        final List<PaymentStatus> paymentStatuses = Arrays.asList(paymentStatus);

        individualPayment.setPaymentStatus(paymentStatuses);
        individualPayment.setPoolingAdjustmentDate(OffsetDateTime.now());
        individualPayment.setEquivalentAmount(instructedCurrencyAndAmount.getAmount());
        individualPayment.setInstructionForCreditorAgent(InstructionCode.HoldCashForCreditor);
        individualPayment.setInstructionForDebtorAgent(InstructionCode.PayTheBeneficiary);
        individualPayment.setAmount(instructedCurrencyAndAmount);
        individualPayment.setPaymentInstrument(PaymentInstrumentCode.valueOf(makePaymentDTO.getPaymentInstrument()));
        individualPayment.setAccount(sourceAccount);
        individualPayment.setId(UUID.randomUUID().toString());
        individualPayment.setDateCreated(OffsetDateTime.now());
        individualPayment.setLastUpdated(OffsetDateTime.now());
        individualPayment.setEntityStatus(EntityStatus.DRAFT);
        individualPayment.setEntityVersion(0L);
        addIndividualPaymentPartyRoles(individualPayment, makePaymentDTO);
        return individualPayment;
    }

    private void configurePaymentInformationForRequestType(IndividualPayment individualPayment, MakePaymentDTO makePaymentDTO) {
        final Optional<MakePaymentRequestType> paymentRequestTypeOptional = getPaymentRequestType(makePaymentDTO.getMakePaymentRequestType());
        paymentRequestTypeOptional.ifPresent(makePaymentRequestType -> {
            log.info("Handling Credit Transfer payment information.");
            individualPayment.setType(PaymentTypeCode.Other);
        });
    }

    private static Optional<MakePaymentRequestType> getPaymentRequestType(String paymentRequest) {
        try {
            return Optional.ofNullable(MakePaymentRequestType.valueOf(paymentRequest));
        } catch (RuntimeException re) {
            return Optional.empty();
        }
    }

    private void addIndividualPaymentPartyRoles(final IndividualPayment individualPayment, MakePaymentDTO makePaymentDTO) {

        /**
         * Configure Individual Payment Roles
         */
        //The Payment
        final List<Payment> individualPayments = Arrays.asList(individualPayment);
        //The affected Bulk Payment accounts
        final List<CashAccount> cashAccounts = Arrays.asList(individualPayment.getAccount());

        final List<AccountPartyRole> accountPartyRoles = individualPayment.getAccount().getPartyRole();

        //account servicing roles
        final List<AccountPartyRole> accountServicerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountServicerRole);
        }).collect(Collectors.toList());

        //The Role Players servicing this payment
        final List<RolePlayer> accountServicerRolePlayers = accountServicerRoles.stream()
                .flatMap(accountServicerRole -> accountServicerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //account owning roles
        final List<AccountPartyRole> accountOwnerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountOwnerRole);
        }).collect(Collectors.toList());

        //The Role Players owning this payment
        final List<RolePlayer> accountOwnerRolePlayers = accountOwnerRoles.stream()
                .flatMap(accountOwnerRole -> accountOwnerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //Debtor Role
        DebtorRole debtorRole = new DebtorRole();
        debtorRole.setPayment(individualPayments);
        debtorRole.setCashAccount(cashAccounts);
        debtorRole.setId(GenerateKey.generateEntityId());
        debtorRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorRole.setDateCreated(OffsetDateTime.now());
        debtorRole.setLastUpdated(OffsetDateTime.now());
        debtorRole.setEntityVersion(0L);
        debtorRole.setPlayer(accountOwnerRolePlayers);
        debtorRole.setEntry(null);
        debtorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        //Debtor Agent Role
        DebtorAgentRole debtorAgentRole = new DebtorAgentRole();
        debtorAgentRole.setPayment(individualPayments);
        debtorAgentRole.setCashAccount(cashAccounts);
        debtorAgentRole.setId(GenerateKey.generateEntityId());
        debtorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorAgentRole.setDateCreated(OffsetDateTime.now());
        debtorAgentRole.setLastUpdated(OffsetDateTime.now());
        debtorAgentRole.setEntityVersion(0L);
        debtorAgentRole.setPlayer(accountServicerRolePlayers);
        debtorAgentRole.setEntry(null);
        debtorAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Initiating Party Role
        InitiatingPartyRole initiatingPartyRole = new InitiatingPartyRole();
        initiatingPartyRole.setPayment(individualPayments);
        initiatingPartyRole.setCashAccount(cashAccounts);
        initiatingPartyRole.setId(GenerateKey.generateEntityId());
        initiatingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
        initiatingPartyRole.setDateCreated(OffsetDateTime.now());
        initiatingPartyRole.setLastUpdated(OffsetDateTime.now());
        initiatingPartyRole.setEntityVersion(0L);
        initiatingPartyRole.setPlayer(accountOwnerRolePlayers);
        initiatingPartyRole.setEntry(null);
        initiatingPartyRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Forwarding Party Role
        ForwardingAgentRole forwardingAgentRole = new ForwardingAgentRole();
        forwardingAgentRole.setPayment(individualPayments);
        forwardingAgentRole.setCashAccount(cashAccounts);
        forwardingAgentRole.setId(GenerateKey.generateEntityId());
        forwardingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        forwardingAgentRole.setDateCreated(OffsetDateTime.now());
        forwardingAgentRole.setLastUpdated(OffsetDateTime.now());
        forwardingAgentRole.setEntityVersion(0L);
        forwardingAgentRole.setPlayer(accountServicerRolePlayers);
        forwardingAgentRole.setEntry(null);
        forwardingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Instructing Agent Role
        InstructingAgentRole instructingAgentRole = new InstructingAgentRole();
        instructingAgentRole.setPayment(individualPayments);
        instructingAgentRole.setCashAccount(cashAccounts);
        instructingAgentRole.setId(GenerateKey.generateEntityId());
        instructingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        instructingAgentRole.setDateCreated(OffsetDateTime.now());
        instructingAgentRole.setLastUpdated(OffsetDateTime.now());
        instructingAgentRole.setEntityVersion(0L);
        instructingAgentRole.setPlayer(accountServicerRolePlayers);
        instructingAgentRole.setEntry(null);
        instructingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        List<PaymentPartyRole> individualPaymentPartyRoles = new ArrayList<>();
        individualPaymentPartyRoles.add(debtorRole);
        individualPaymentPartyRoles.add(debtorAgentRole);
        individualPaymentPartyRoles.add(initiatingPartyRole);
        individualPaymentPartyRoles.add(forwardingAgentRole);
        individualPaymentPartyRoles.add(instructingAgentRole);

        /**
         * Configure Payment Parties for individual payment
         */
        final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionById(makePaymentDTO.getDestinationFinancialInstitutionId());
        configureCreditorRoles(individualPaymentPartyRoles, individualPayments, makePaymentDTO, financialInstitutionOptional);

        individualPayment.setPartyRole(individualPaymentPartyRoles);
    }

    private void configureCreditorRoles(List<PaymentPartyRole> individualPaymentPartyRoles, List<Payment> individualPayments,
                                        MakePaymentDTO makePaymentDTO, Optional<FinancialInstitution> destinationFinancialInstitutionOptional) {
        //Creditor Agent Role
        CreditorAgentRole creditorAgentRole = new CreditorAgentRole();
        creditorAgentRole.setPayment(individualPayments);
        destinationFinancialInstitutionOptional.ifPresent(financialInstitution -> {
            creditorAgentRole.setPlayer(Arrays.asList(financialInstitution));
        });
        creditorAgentRole.setId(GenerateKey.generateEntityId());
        creditorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorAgentRole.setDateCreated(OffsetDateTime.now());
        creditorAgentRole.setLastUpdated(OffsetDateTime.now());
        creditorAgentRole.setEntityVersion(0L);
        creditorAgentRole.setEntry(null);
        creditorAgentRole.setPartyRoleCode(PartyRoleCode.FinalAgent);

        CustomParty customParty = new CustomParty();
        customParty.setId(GenerateKey.generateEntityId());
        customParty.setDateCreated(OffsetDateTime.now());
        customParty.setLastUpdated(OffsetDateTime.now());
        customParty.setEntityStatus(EntityStatus.ACTIVE);
        customParty.setEntityVersion(0L);
        customParty.setName(makePaymentDTO.getBeneficiaryName());
        customParty.setNationalId(makePaymentDTO.getNationalId());
        customParty.setPassportNumber(makePaymentDTO.getPassportNumber());
        customParty.setAccountNumber(makePaymentDTO.getDestinationAccountNumber());
        customParty.setFinancialInstitutionIdentification(destinationFinancialInstitutionOptional.map(FinancialInstitution::getId).orElse(""));

        final CustomParty savedCustomParty = customPartyRepository.save(customParty);

        //Scheme
//        final Scheme scheme = new Scheme();
//        scheme.setId(GenerateKey.generateEntityId());
//        scheme.setEntityStatus(EntityStatus.ACTIVE);
//        scheme.setDateCreated(OffsetDateTime.now());
//        scheme.setLastUpdated(OffsetDateTime.now());
//        scheme.setNameShort("Default");
//        scheme.setCode("");
//        scheme.setIdentification(new ArrayList<>());
//        scheme.setVersion("0");
//        scheme.setNameLong("Default Scheme");
//        scheme.setDescription("This is the default scheme for the creditor role.");
//        scheme.setDomainValueCode("Default");
//        scheme.setDomainValueName("Default");
//        schemeRepository.save(scheme);

        //Creditor Role
        CreditorRole creditorRole = new CreditorRole();
//        scheme.setCreditorRole(creditorRole);
//        creditorRole.setScheme(scheme);
        creditorRole.setPayment(individualPayments);
        creditorRole.setCustomParty(savedCustomParty);
        creditorRole.setHasCustomParty(true);
        creditorRole.setId(GenerateKey.generateEntityId());
        creditorRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorRole.setDateCreated(OffsetDateTime.now());
        creditorRole.setLastUpdated(OffsetDateTime.now());
        creditorRole.setEntityVersion(0L);
        creditorRole.setEntry(null);
        creditorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        individualPaymentPartyRoles.add(creditorAgentRole);
        individualPaymentPartyRoles.add(creditorRole);
    }
}
