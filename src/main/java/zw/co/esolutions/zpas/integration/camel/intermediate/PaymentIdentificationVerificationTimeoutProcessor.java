package zw.co.esolutions.zpas.integration.camel.intermediate;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.model.BulkPayment;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.repository.BulkPaymentRepository;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.services.impl.payments.cache.PaymentsCacheProcessor;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.domain.PaymentStatusInfo;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by alfred on 23 Jan 2019
 */
@Component
@Slf4j
@Transactional
public class PaymentIdentificationVerificationTimeoutProcessor implements Processor {

    @Autowired
    private PaymentsCacheProcessor cacheProcessor;

    @Autowired
    private BulkPaymentRepository bulkPaymentRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        log.info("Payment verification timeout processor invoked...");

        final List<BulkPayment> bulkPayments = bulkPaymentRepository.findAllByEntityStatus(EntityStatus.PENDING_VERIFICATION);
        log.info("Found {} bulk bulkPayments in status: {}", bulkPayments.size(), EntityStatus.PENDING_VERIFICATION.name());

        for (BulkPayment bulkPayment : bulkPayments) {
            log.info("Clearing map for timed out payment account info verificication for payment {}", bulkPayment.getId());
            cacheProcessor.getPaymentStatusInfoMap().computeIfPresent(bulkPayment.getId(), (key, value) -> {
                PaymentStatusInfo.PaymentStatusInfoBuilder paymentStatusInfoBuilder = PaymentStatusInfo.builder();
                paymentStatusInfoBuilder.requestId(bulkPayment.getId());
                final String status = "TIMED_OUT";
                paymentStatusInfoBuilder.status(status);
                    final String info = "Verification request timed out";
                paymentStatusInfoBuilder.info(info);

                final List<PaymentStatusInfo> paymentStatusInfos = value.getSseMessages().stream().map(paymentStatusInfo -> {
                    paymentStatusInfo.setInfo(info);
                    paymentStatusInfo.setStatus(status);
                    return paymentStatusInfo;
                }).collect(Collectors.toList());
                paymentStatusInfoBuilder.sseMessages(paymentStatusInfos);
                return paymentStatusInfoBuilder.build();
            });

            log.info("Checking paymentUploadDTO with reference: {}", bulkPayment.getEndToEndId());

            log.info("Number of individual payments in paymentUploadDTO: {}", bulkPayment.getGroups().size());
            if (bulkPayment.getVerificationStartTime().isBefore(OffsetDateTime.now().minusMinutes(2))) {
                bulkPayment.setEntityStatus(EntityStatus.PENDING_APPROVAL);
                bulkPayment.setVerificationNarrative("Verification timed out");
                bulkPaymentRepository.save(bulkPayment);
                bulkPayment.getGroups().forEach(individualPayment -> {
                    individualPayment.setEntityStatus(EntityStatus.PENDING_APPROVAL);
                    individualPayment.setVerificationNarrative("Verification timed out");
                    paymentRepository.save(individualPayment);
                });
            }
        }
    }
}

