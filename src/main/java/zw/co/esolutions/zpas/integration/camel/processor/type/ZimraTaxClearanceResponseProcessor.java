package zw.co.esolutions.zpas.integration.camel.processor.type;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.model.taxclearance.TaxClearance;
import zw.co.esolutions.zpas.services.iface.taxclearance.TaxClearanceService;
import zw.co.esolutions.zpas.services.impl.taxclearance.TaxClearanceJsonResponse;

import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by alfred on 23 May 2019
 */
@Slf4j
@Component
public class ZimraTaxClearanceResponseProcessor implements Processor {
    @Autowired
    private TaxClearanceService taxClearanceService;

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Response: \n{}\n", message);
        List<TaxClearanceJsonResponse> taxClearanceJsons = readTaxClearancesFromJsonArray(message);
        List<TaxClearance> taxClearances = taxClearanceService.refreshTaxClearances(taxClearanceJsons);
        log.info("Processed {} Tax Clearance updates.", taxClearances.size());
    }

    public List<TaxClearanceJsonResponse> readTaxClearancesFromJsonArray(String json) {
        ObjectMapper mapper = new ObjectMapper();

        /**
         * Read object from file
         */
        List<TaxClearanceJsonResponse> taxClearanceJsonObjects = null;
        try {
            taxClearanceJsonObjects = Arrays.asList(mapper.readValue(json, TaxClearanceJsonResponse[].class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<TaxClearanceJsonResponse> taxClearanceJsonResponses = taxClearanceJsonObjects.stream().map(taxClearanceJsonResponse -> {
            return taxClearanceJsonResponse.toBuilder().verificationCompleted(true).build();
        }).collect(Collectors.toList());
        log.info("Processed tax renewal response: {}", taxClearanceJsonResponses);
        return Optional.ofNullable(taxClearanceJsonResponses).orElse(new ArrayList<>());
    }

    public static void main(String args[]) {

        List<TaxClearanceJsonResponse> taxClearanceJsons = IntStream.range(1, 20).mapToObj(i -> {
            TaxClearanceJsonResponse taxClearanceJson = TaxClearanceJsonResponse.builder()
                    .bpn("BPN" + i)
                    .companyName("CompanyName" + i)
                    .startDate(OffsetDateTime.now().format(TaxClearanceService.DATE_TIME_FORMATTER))
                    .expiryDate(OffsetDateTime.now().plusYears(1).format(TaxClearanceService.DATE_TIME_FORMATTER))
                    .build();
            return taxClearanceJson;
        }).collect(Collectors.toList());

        ObjectMapper mapper = new ObjectMapper();

        /**
         * Write object to file
         */
        StringWriter writer = new StringWriter();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(writer, taxClearanceJsons);//Prettified JSON
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(writer.toString());
    }
}
