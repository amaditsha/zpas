package zw.co.esolutions.zpas.integration.camel.processor;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Handler for unrecognized or erroneous messages
 */
@Slf4j
public class MessageErrorProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Unrecognized message: \n{}", message);
        //a better implementation could be to dump the message in an error msg queue.
        log.info("Message DISCARDED");
    }
}
