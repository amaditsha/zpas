package zw.co.esolutions.zpas.integration.camel.processor.type;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.dto.message.MessageResponseDTO;
import zw.co.esolutions.zpas.dto.message.NotificationRequestDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.CreditorPaymentActivationRequestStatusReportV07;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.Document;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentObligationRepository;
import zw.co.esolutions.zpas.repository.PaymentStatusRepository;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.GroupStatus;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import javax.xml.bind.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringReader;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Created by alfred on 24 Apr 2019
 */
@Slf4j
@Component
@Transactional
public class Pain014CreditorPaymentActivationRequestStatusReportProcessor implements Processor {
    @Autowired
    private PaymentObligationRepository paymentObligationRepository;

    @Autowired
    private NotificationConfigService notificationConfigService;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.pain014_001_07");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Creditor Payment Activation Request Status Report : \n{}", message);

        final CreditorPaymentActivationRequestStatusReportV07 statusReportV07 = parseXml(message);

        log.info("Got message with MsgID : {}", statusReportV07.getGrpHdr().getMsgId());

        String originalMsgId = statusReportV07.getOrgnlGrpInfAndSts().getOrgnlMsgId();
        final String grpSts = statusReportV07.getOrgnlGrpInfAndSts().getGrpSts();
        GroupStatus groupStatus = GroupStatus.FAIL;
        try {
            groupStatus = GroupStatus.valueOf(grpSts);
        } catch (RuntimeException re) {
            log.error("Unknown group status: {}", groupStatus);
        }

        final GroupStatus finalGroupStatus = groupStatus;
        log.info("Orgnl Msg ID is : {}", originalMsgId);

        if (StringUtils.isEmpty(originalMsgId)) {
            log.info("Message could not be processed.");
        } else {
            paymentObligationRepository.findById(originalMsgId).map(paymentObligation -> {
                updateObligationStatuses(paymentObligation, finalGroupStatus);
                paymentObligation.getPartyRole().stream().filter(role -> role instanceof UltimateCreditorRole)
                        .map(role -> (UltimateCreditorRole) role)
                        .findFirst().ifPresent(ultimateCreditorRole -> {
                    log.info("Found Creditor role on Creditor Payment Activation Request Status Report");
                    ultimateCreditorRole.getPlayer().stream().findFirst().ifPresent(rolePlayer -> {
                        final Party party = (Party) rolePlayer;
                        sendMailHelper(party, statusReportV07, finalGroupStatus);
                    });
                });
                return paymentObligation;
            }).orElseGet(() -> {
                return null;
            });
        }

    }

    private void updateObligationStatuses(PaymentObligation paymentObligation, GroupStatus groupStatus) {
        switch (groupStatus) {
            case SUCCESS:
                break;
            case FAIL:
                paymentObligation.setEntityStatus(EntityStatus.FAILED);
                paymentObligation.getPaymentOffset().forEach(payment -> {
                    payment.setEntityStatus(EntityStatus.FAILED);
                    final PaymentStatus paymentStatus = new PaymentStatus();
                    paymentStatus.setStatus(PaymentStatusCode.Rejected);
                    paymentStatus.setPayment(payment);
                    paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                    paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                    paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                    paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.Authorised);
                    paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                    paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                    paymentStatus.setTransactionStatus(TransactionStatusCode.Cancelled);
                    paymentStatus.setId(GenerateKey.generateEntityId());
                    paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                    paymentStatus.setDateCreated(OffsetDateTime.now());
                    paymentStatus.setLastUpdated(OffsetDateTime.now());
                    paymentStatus.setStatusDateTime(OffsetDateTime.now());
                    paymentStatus.setStatusDescription("Initialising Payment Activation Request");
                    paymentStatus.setInstructionProcessingStatus(StatusCode.Rejected);

                    final PaymentStatus savedPaymentStatus = paymentStatusRepository.save(paymentStatus);

                    payment.getPaymentStatus().add(savedPaymentStatus);
                });
                break;
            case APPROVED:
                paymentObligation.setEntityStatus(EntityStatus.ACTIVE);
                paymentObligation.getPaymentOffset().stream().forEach(payment -> {

                    final PaymentStatus paymentStatus = new PaymentStatus();
                    paymentStatus.setStatus(PaymentStatusCode.Pending);
                    paymentStatus.setPayment(payment);
                    paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                    paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                    paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                    paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.Authorised);
                    paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                    paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                    paymentStatus.setTransactionStatus(TransactionStatusCode.None);
                    paymentStatus.setId(GenerateKey.generateEntityId());
                    paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                    paymentStatus.setDateCreated(OffsetDateTime.now());
                    paymentStatus.setLastUpdated(OffsetDateTime.now());
                    paymentStatus.setStatusDateTime(OffsetDateTime.now());
                    paymentStatus.setStatusDescription("Initialising Payment Activation Request");
                    paymentStatus.setInstructionProcessingStatus(StatusCode.Pending);

                    final PaymentStatus savedPaymentStatus = paymentStatusRepository.save(paymentStatus);

                    payment.getPaymentStatus().add(savedPaymentStatus);
                });
                break;
            case REJECTED:
                paymentObligation.setEntityStatus(EntityStatus.DISAPPROVED);
                paymentObligation.getPaymentOffset().stream().forEach(payment -> {
                    final PaymentStatus paymentStatus = new PaymentStatus();
                    paymentStatus.setStatus(PaymentStatusCode.Rejected);
                    paymentStatus.setPayment(payment);
                    paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                    paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                    paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                    paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.Rejected);
                    paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                    paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                    paymentStatus.setTransactionStatus(TransactionStatusCode.Cancelled);
                    paymentStatus.setId(GenerateKey.generateEntityId());
                    paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                    paymentStatus.setDateCreated(OffsetDateTime.now());
                    paymentStatus.setLastUpdated(OffsetDateTime.now());
                    paymentStatus.setStatusDateTime(OffsetDateTime.now());
                    paymentStatus.setStatusDescription("Rejected Activation Request");
                    paymentStatus.setInstructionProcessingStatus(StatusCode.Rejected);

                    final PaymentStatus savedPaymentStatus = paymentStatusRepository.save(paymentStatus);

                    payment.getPaymentStatus().add(savedPaymentStatus);
                });
                break;
            case RECEIVED:
                break;
            default:
        }
    }

    private void sendMailHelper(Party party, CreditorPaymentActivationRequestStatusReportV07 statusReportV07, GroupStatus groupStatus) {
        final String partyName = party.getName();
        final String partyEmail = getPartyEmail(party);
        final XMLGregorianCalendar creDtTm = statusReportV07.getGrpHdr().getCreDtTm();
        final OffsetDateTime creditOffsetDateTime = XmlDateUtil.getOffsetDateTime(creDtTm);
        final String creditDateTime = creditOffsetDateTime.format(DateTimeFormatter.ofPattern("dd MMMM yyyy"));

        String messageText = "";
        switch (groupStatus) {
            case SUCCESS:
                messageText = "The payment request you made on " + creditDateTime
                        + " has been successfully received. We will inform you of subsequent processing.";
                break;
            case FAIL:
                messageText = "The payment pequest you made on " + creditDateTime
                        + " has failed validation. Please provided all required information and retry, or consult the bank.";
                break;
            case APPROVED:
                messageText = "The payment request you made on " + creditDateTime
                        + " has been approved. The payment is now being processed.";
                break;
            case REJECTED:
                messageText = "The payment request you made on " + creditDateTime
                        + " has been rejected. For any queries, contact the debtor/recipient of your request.";
                break;
            case RECEIVED:
                messageText = "The payment request you made on " + creditDateTime
                        + " has been received. We will inform you of subsequent processing.";
                break;
            default:
                messageText = "The payment request you made on " + creditDateTime
                        + " could not be processed. Please consult with the bank.";
        }

        final MessageResponseDTO messageResponseDTO = notificationConfigService.processAndSendNotifications(
                NotificationRequestDTO.builder()
                        .attachmentFilePaths(new ArrayList<>())
                        .clientId(party.getId())
                        .destination(partyEmail)
                        .messageChannel(MessageChannel.EMAIL)
                        .messageDate(OffsetDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
                        .messageReference(RefGen.getReference("N"))
                        .messageText(messageText)
                        .notificationType(NotificationType.NONE)
                        .originator("")
                        .recipientName(partyName)
                        .subject("Status Report for Creditor Payment Activation Request")
                        .build());

        log.info("Creditor Payment Activation Request Status Report Message Sent.");
    }

    private CreditorPaymentActivationRequestStatusReportV07 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
        final CreditorPaymentActivationRequestStatusReportV07 cdtrPmtActvtnReqStsRpt = document.getCdtrPmtActvtnReqStsRpt();

        return cdtrPmtActvtnReqStsRpt;
    }

    private String getPartyEmail(Party party) {
        return party.getContactPoint().stream().filter(c -> c instanceof ElectronicAddress)
                .map(c -> (ElectronicAddress) c)
                .findFirst().map(ElectronicAddress::getEmailAddress)
                .orElse("");
    }
}
