package zw.co.esolutions.zpas.integration.camel.config;

import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.integration.camel.config.util.Konstants;
import zw.co.esolutions.zpas.integration.camel.processor.MessageErrorProcessor;
import zw.co.esolutions.zpas.integration.camel.processor.type.*;
import zw.co.esolutions.zpas.model.HubConfiguration;
import zw.co.esolutions.zpas.repository.HubConfigurationRepository;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;
import zw.co.esolutions.zpas.utilities.util.DateUtil;
import zw.co.esolutions.zpas.utilities.util.XMLUtil;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

@Component
public class AppRoutes extends RouteBuilder {

    @Value("${XSD.BASE.PATH}")
    String xsdBasePath;

    @Value("${hub.reply.queue.url}")
    private String hubReplyQueue;

    @Value("${hub.reply.queue.other.uri}")
    private String otherHubReplyQueue;

    @Value("${hub.req.queue.url}")
    public String hubRequestQueue;

    @Value("${zimra.req.queue.url}")
    public String zimraRequestQueue;

    @Value("${zimra.reply.queue.url}")
    private String zimraReplyQueue;

    @Value("${nssa.req.queue.url}")
    public String nssaRequestQueue;

    @Value("${nssa.reply.queue.url}")
    private String nssaReplyQueue;

    @Value("${aqs.in.system.queue.url}")
    public String aqsInSystemQueue;

    @Value("${hub.req.http.url}")
    public String hubRequestHttpEndpoint;

    @Autowired Pain002CustomerPaymentStatusReportProcessor pain002CustomerPaymentStatusReportProcessor;
    @Autowired Acmt024IdentificationVerificationReportProcessor acmt024IdentificationVerificationReportProcessor;
    @Autowired Pain013CreditorPaymentActivationRequestProcessor pain013CreditorPaymentActivationRequestProcessor;
    @Autowired Pain014CreditorPaymentActivationRequestStatusReportProcessor pain014CreditorPaymentActivationRequestStatusReportProcessor;
    @Autowired Camt026UnableToApplyProcessor camt026UnableToApplyProcessor;
    @Autowired Camt029ResolutionOfInvestigationProcessor camt029ResolutionOfInvestigationProcessor;
    @Autowired Camt031RejectInvestigationProcessor camt031RejectInvestigationProcessor;
    @Autowired Camt030NotificationOfCaseAssignmentProcessor camt030NotificationOfCaseAssignmentProcessor;
    @Autowired Camt052BankToCustomerReportProcessor camt052BankToCustomerReportProcessor;
    @Autowired Camt053BankToCustomerStatementProcessor camt053BankToCustomerStatementProcessor;
    @Autowired Camt054BankToCustomerDebitCreditNotificationProcessor camt054BankToCustomerDebitCreditNotificationProcessor;
    @Autowired Camt037DebitAuthorisationRequestProcessor camt037DebitAuthorisationRequestProcessor;

    @Autowired ZimraTaxClearanceResponseProcessor zimraTaxClearanceResponseProcessor;
    @Autowired ZimraTaxClearanceResponseSimulator zimraTaxClearanceResponseSimulator;

    @Autowired
    HubConfigurationRepository hubConfigurationRepository;

    @Override
    public void configure() throws Exception {

        from(hubReplyQueue)
                .convertBodyTo(String.class)
                .to("seda:iso.xsd.validator")
                .to("direct:extract.msg.metadata")
                .wireTap("direct:archive.incoming.iso")
                .choice()
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:pain.002"))
                .process(pain002CustomerPaymentStatusReportProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:acmt.024"))
                .process(acmt024IdentificationVerificationReportProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.026"))
                .process(camt026UnableToApplyProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.029"))
                .process(camt029ResolutionOfInvestigationProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.030"))
                .process(camt030NotificationOfCaseAssignmentProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.031"))
                .process(camt031RejectInvestigationProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.052"))
                .process(camt052BankToCustomerReportProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.053"))
                .process(camt053BankToCustomerStatementProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.054"))
                .process(camt054BankToCustomerDebitCreditNotificationProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:pain.013"))
                .process(pain013CreditorPaymentActivationRequestProcessor)
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.037"))
                .process(camt037DebitAuthorisationRequestProcessor)
                .otherwise()
                .process(new MessageErrorProcessor())
                .end();

//            from(hubRequestQueue)
//                    .to("seda:iso.xsd.validator")
//                    .to(hubRequestHttpEndpoint)
//                    .log("HUB RESPONSE -> " + body());

        from(hubRequestQueue)
                .process(exchange -> {
                    log.info("BIC in the flow is ----> " + exchange.getIn().getHeader("BIC", String.class));
                })
                .to("seda:iso.xsd.validator")
                .to("direct:extract.msg.metadata")
                .wireTap("direct:archive.outgoing.iso")
                .process(exchange -> {
                    String bic = exchange.getIn().getHeader("BIC", String.class);
                    final Optional<HubConfiguration> hubConfigurationOptional = hubConfigurationRepository.findById(bic);

                    String URI;
                    if (hubConfigurationOptional.isPresent()) {
                        URI = hubConfigurationOptional.get().getUri();
                    } else {
                        URI = "NO_CONFIGURED_URL";
                    }
                    log.info("HUB URI -> {}", URI);
                    exchange.getIn().setHeader("URI", URI);
                })
                .choice()
                .when(header("URI").isEqualTo("NO_CONFIGURED_URL"))
                    .log("Not forwarding request : ${header.URI}")
                .otherwise()
                    .doTry()
                        .toD("${header.URI}")
                        .log("HUB RESPONSE -> " + body())
                    .doCatch(Exception.class)
                        .log("Exception - Failed to send request")
                .endChoice()
                .end();

        from("seda:iso.xsd.validator")
                .choice()
//                        .when(body().convertTo(String.class).contains("urn:iso:std:iso:20022:tech:xsd:acmt.023"))
//                            .to("validator:file:" + xsdBasePath + "/xsd/acmt/acmt.023.001.02.xsd")
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.060"))
                .to("validator:file:" + xsdBasePath + "/xsd/camt/camt.060.001.04.xsd")
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.052"))
                .to("validator:file:" + xsdBasePath + "/xsd/camt/camt.052.001.07.xsd")
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.053"))
                .to("validator:file:" + xsdBasePath + "/xsd/camt/camt.053.001.07.xsd")
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.037"))
                .to("validator:file:" + xsdBasePath + "/xsd/camt/camt.037.001.07.xsd")
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.036"))
                .to("validator:file:" + xsdBasePath + "/xsd/camt/camt.036.001.05.xsd")
                .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:pain.002"))
                .to("validator:file:" + xsdBasePath + "/xsd/pain/pain.002.001.09.xsd")
                .end();


//        from(otherHubReplyQueue)
//                .convertBodyTo(String.class)
//                .setHeader("ns", xpath("/*/namespace::*[name()='']"))
//                .setHeader("msgId", xpath("//*[local-name()='MsgId']/text()"))
//                .log("NS from XPATH : ${header.ns}")
//                .log("MSG ID from XPATH : ${header.msgId}")
//                .wireTap("seda:archive.outgoing.iso")
//                .choice()
//                    .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.053"))
//                    .process(camt053BankToCustomerStatementProcessor)
//                    .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:camt.054"))
//                    .process(camt054BankToCustomerDebitCreditNotificationProcessor)
//                    .otherwise()
//                    .process(new MessageErrorProcessor())
//                    .end();

        from(aqsInSystemQueue)
                .convertBodyTo(String.class)
                .choice()
                    .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:pain.014"))
                        .process(pain014CreditorPaymentActivationRequestStatusReportProcessor)
                    .when(header("ns").convertToString().contains("urn:iso:std:iso:20022:tech:xsd:pain.013"))
                        .process(pain013CreditorPaymentActivationRequestProcessor)
                .otherwise()
                    .process(new MessageErrorProcessor())
                .end();

        from(zimraRequestQueue)
                .process(zimraTaxClearanceResponseSimulator)
                .end();

        from(zimraReplyQueue)
                .process(zimraTaxClearanceResponseProcessor)
                .end();

        from("direct:extract.msg.metadata")
                .log("Extracting default Namespace & relevant Msg/Assgnmt Id..")
                .choice()
                    .when(body().isNull())
                        .log("Body is NULL, not setting xpath headers")
                    .otherwise()
                        .setHeader(Konstants.NAMESPACE, xpath("/*/namespace::*[name()='']"))
                        .setHeader(Konstants.MSG_ID, xpath("//*[local-name()='MsgId']/text()"))
                        .setHeader(Konstants.ASSGNMT_ID, xpath("/*/*/*[local-name()='Assgnmt']/*[local-name()='Id']/text()"))
                        .log("NS from XPATH : ${header.ns}")
                        .log("MSG ID from XPATH : ${header.msgId}")
                        .log("ASSIGN ID from XPATH : ${header.assgnmtId}")
                        .convertBodyTo(String.class)
                .end();

        from("direct:archive.incoming.iso")
                .setHeader("basePath", simple(StorageProperties.INCOMING_ISO_ARCHIVE_FOLDER))
                .to("direct:archive.iso.file")
                .end();

        from("direct:archive.outgoing.iso")
                .setHeader("basePath", simple(StorageProperties.OUTGOING_ISO_ARCHIVE_FOLDER))
                .to("direct:archive.iso.file")
                .end();

        from("direct:archive.iso.file")
                .process(exchange -> {
                    String namespace = exchange.getIn().getHeader(Konstants.NAMESPACE, String.class);
                    String msgId = exchange.getIn().getHeader(Konstants.MSG_ID, String.class);
                    String assgnmtId = exchange.getIn().getHeader(Konstants.ASSGNMT_ID, String.class);

                    String fileType;

                    if (StringUtils.isNotBlank(namespace)) {
                        String[] namespaceTokens = namespace.split(":");
                        fileType = namespaceTokens[namespaceTokens.length - 1];
                    } else {
                        //Not Namespaced
                        fileType = "NONS.999";
                    }

                    String prefix = fileType.substring(0, 8).toUpperCase();

                    String postfix = (StringUtils.isNotBlank(msgId) ? msgId : assgnmtId);

                    if (StringUtils.isNotBlank(postfix)) {
                        postfix = postfix.replace("/", ".");
                    } else {
                        postfix = UUID.randomUUID().toString();
                    }

                    String filename = prefix + "." + postfix;

                    String basePath = exchange.getIn().getHeader("basePath", String.class);

                    String directory = DateUtil.convertDateToYYYYMMDDFormat(OffsetDateTime.now());

                    final String prettyXML = XMLUtil.getPrettyString(exchange.getIn().getBody(String.class), 4);

                    exchange.getIn().setBody(prettyXML);

                    exchange.getIn().setHeader("path", basePath + directory);
                    exchange.getIn().setHeader("filename", filename);
                })
                .toD("file:${header.path}?fileName=${header.filename}")
                .log("ISO FILE ARCHIVED to : ${header.path}/${header.filename}")
                .end();
    }
}
