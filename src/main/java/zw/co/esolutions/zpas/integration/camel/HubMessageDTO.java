package zw.co.esolutions.zpas.integration.camel;

import lombok.Builder;
import lombok.Data;

/**
 * Created by mabuza on 10 Jul 2019
 */
@Data
@Builder
public class HubMessageDTO {
    private String bic;
    private String message;
}
