package zw.co.esolutions.zpas.integration.camel.processor.type;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.dto.message.NotificationRequestDTO;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.iso.msg.camt054_001_07.Document;
import zw.co.esolutions.zpas.iso.msg.camt054_001_07.*;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PersonIdentificationRepository;
import zw.co.esolutions.zpas.repository.PersonRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.xml.bind.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringReader;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by alfred on 09 Apr 2019
 */
@Component
@Slf4j
public class Camt054BankToCustomerDebitCreditNotificationProcessor implements Processor {
    @Autowired
    private AccountsService accountService;

    @Autowired
    private NotificationConfigService messageService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private PersonIdentificationRepository personIdentificationRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private PersonRepository personRepository;

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt054_001_07");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Bank to Customer Debit Credit notification : \n{}", message);

        BankToCustomerDebitCreditNotificationV07 bankToCustomerDebitCreditNotificationV07 = parseXml(message);

        final GroupHeader73 grpHdr = bankToCustomerDebitCreditNotificationV07.getGrpHdr();

        final List<AccountNotification15> ntfctn = bankToCustomerDebitCreditNotificationV07.getNtfctn();
        ntfctn.forEach(accountNotification15 -> processAccountNotification(accountNotification15, grpHdr));
    }

    private void processAccountNotification(AccountNotification15 accountNotification15, GroupHeader73 grpHdr) {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        final CashAccount36 cashAccount36 = accountNotification15.getAcct();

        log.info("Fetching account owner.");
        final AccountIdentification4Choice cashAccount36Id = cashAccount36.getId();
        final String accountNumber = cashAccount36Id.getOthr().getId();
        Optional<Person> optionalPerson = getPerson(grpHdr, accountNumber);

        optionalPerson.ifPresent(person -> {
            final String personEmail = person.getEmailAddress();
            final String personName = person.getName();
            if (!StringUtils.isEmpty(personEmail)) {
                NotificationRequestDTO.NotificationRequestDTOBuilder notificationRequestDTOBuilder = NotificationRequestDTO.builder();
                notificationRequestDTOBuilder.recipientName(personName);
                notificationRequestDTOBuilder.destination(personEmail);
                notificationRequestDTOBuilder.messageReference(Optional.ofNullable(grpHdr).map(groupHeader73 -> groupHeader73.getMsgId()).orElse(GenerateKey.generateRef()));
                notificationRequestDTOBuilder.originator("noreply@esolutions.co.zw");
                notificationRequestDTOBuilder.subject("Account Notification");
                notificationRequestDTOBuilder.messageChannel(MessageChannel.EMAIL);
                notificationRequestDTOBuilder.messageDate(OffsetDateTime.now().format(dateTimeFormatter));

                StringBuilder emailMessageBuilder = new StringBuilder();

                final String cashAccount36Currency = cashAccount36.getCcy();
                final String cashAccount36Number = cashAccount36.getNm();
                final PartyIdentification125 cashAccount36Owner = cashAccount36.getOwnr();
                final BranchAndFinancialInstitutionIdentification5 cashAccount36Servicer = cashAccount36.getSvcr();
                final CashAccountType2Choice cashAccount36Type = cashAccount36.getTp();

                final String additionalNotificationInfo = accountNotification15.getAddtlNtfctnInf();
                final CopyDuplicate1Code cpyDplctInd = accountNotification15.getCpyDplctInd();
                final XMLGregorianCalendar creditDateTime = accountNotification15.getCreDtTm();
                final BigDecimal electronicSequenceNumber = accountNotification15.getElctrncSeqNb();
                final DateTimePeriod1 fromToDate = accountNotification15.getFrToDt();
                final String accountNotification15Id = accountNotification15.getId();
                final BigDecimal legalSequenceNumber = accountNotification15.getLglSeqNb();
                final Pagination1 notificationPagination = accountNotification15.getNtfctnPgntn();
                final CashAccount24 relatedAccount = accountNotification15.getRltdAcct();
                final SequenceRange1Choice reportingSequence = accountNotification15.getRptgSeq();
                final ReportingSource1Choice reportingSource = accountNotification15.getRptgSrc();
                final TotalTransactions6 transactionSummaries = accountNotification15.getTxsSummry();

                final List<AccountInterest4> accountInterests = accountNotification15.getIntrst();

                final List<ReportEntry9> notificationEntries = accountNotification15.getNtry();

                log.info("Getting financial institution at which the transaction(s) were processed.");
                final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionByAnyBIC(cashAccount36Servicer.getFinInstnId().getOthr().getId());

                emailMessageBuilder.append("<br>Please be informed that your account ")
                        .append(Optional.ofNullable(accountNumber/*cashAccount36Number*/).orElseGet(() -> getAccountNumber(accountNumber))).append(" with ");

                financialInstitutionOptional.ifPresent(financialInstitution -> {
                    final String financialInstitutionName = financialInstitution.getName();
                    emailMessageBuilder.append(financialInstitutionName);
                });

                emailMessageBuilder.append(" has processed the following ");

                if (notificationEntries.size() == 1) {
                    emailMessageBuilder.append("transaction:");
                } else if (notificationEntries.size() > 1) {
                    emailMessageBuilder.append("transactions:");
                } else {
                    log.error("No transactions to notify.<br><br>");
                    return;
                }

                emailMessageBuilder.append("<br><br><br>");

                if (transactionSummaries != null) {
                    emailMessageBuilder.append("<table>\n" +
                            "    <thead>\n" +
                            "        <tr>\n" +
                            "            <th>Total Number of Entries</th>\n" +
                            "            <th>Total Debit Entries</th>\n" +
                            "            <th>Total Credit Entries</th>\n" +
                            "        </tr>\n" +
                            "    </thead>\n" +
                            "    <tbody>\n" +
                            "        <tr>\n" +
                            "            <td>").append(Optional.ofNullable(transactionSummaries.getTtlNtries()).map(numberAndSumOfTransactions4 -> numberAndSumOfTransactions4.getNbOfNtries()).orElse("")).append("</td>\n" +
                            "            <td>").append(Optional.ofNullable(transactionSummaries.getTtlDbtNtries()).map(numberAndSumOfTransactions1 -> numberAndSumOfTransactions1.getNbOfNtries()).orElse("")).append("</td>\n" +
                            "            <td>").append(Optional.ofNullable(transactionSummaries.getTtlCdtNtries()).map(numberAndSumOfTransactions1 -> numberAndSumOfTransactions1.getNbOfNtries()).orElse("")).append("</td>\n" +
                            "        </tr>\n" +
                            "    </tbody>\n" +
                            "</table><br><br>");
                }

                emailMessageBuilder.append("<table border=\"1\">\n" +
                        "    <thead>\n" +
                        "        <tr>\n" +
                        "            <th>Booking Date</th>\n" +
                        "            <th>Value Date</th>\n" +
                        "            <th>Bank Reference</th>\n" +
                        "            <th>Amount</th>\n" +
                        "            <th>Credit/Debit</th>\n" +
                        "        </tr>\n" +
                        "    </thead>\n" +
                        "    <tbody>\n");

                notificationEntries.forEach(reportEntry9 -> {
                    final String acctSvcrRef = reportEntry9.getAcctSvcrRef();
                    final MessageIdentification2 addtlInfInd = reportEntry9.getAddtlInfInd();
                    final String addtlNtryInf = reportEntry9.getAddtlNtryInf();
                    final ActiveOrHistoricCurrencyAndAmount amt = reportEntry9.getAmt();
                    final AmountAndCurrencyExchange3 amtDtls = reportEntry9.getAmtDtls();
                    final List<CashAvailability1> avlbty = reportEntry9.getAvlbty();
                    final BankTransactionCodeStructure4 bookingTransactionCode = reportEntry9.getBkTxCd();
                    final DateAndDateTime2Choice bookgDate = reportEntry9.getBookgDt();
                    final CardEntry3 cardTransaction = reportEntry9.getCardTx();
                    final CreditDebitCode creditDebitIndicator = reportEntry9.getCdtDbtInd();
                    final Charges4 charges = reportEntry9.getChrgs();
                    final List<EntryDetails8> notificationDetails = reportEntry9.getNtryDtls();
                    final String ntryRef = reportEntry9.getNtryRef();
                    final EntryStatus1Choice sts = reportEntry9.getSts();
                    final TechnicalInputChannel1Choice techInptChanl = reportEntry9.getTechInptChanl();
                    final DateAndDateTime2Choice valDt = reportEntry9.getValDt();

                    emailMessageBuilder.append("<tr><td>").append(dateTimeFormatter.format(XmlDateUtil.getOffsetDateTime(bookgDate.getDtTm()))).append("</td>");
                    if (valDt != null && valDt.getDtTm() != null) {
                        emailMessageBuilder.append("<td>").append(dateTimeFormatter.format(XmlDateUtil.getOffsetDateTime(valDt.getDtTm()))).append("</td>\n");
                    } else {
                        emailMessageBuilder.append("<td></td>\n");
                    }
                    emailMessageBuilder.append("<td>").append(acctSvcrRef).append("</td>")
                            .append("<td>").append(amt.getCcy()).append(" ").append(MoneyUtil.convertDollarsToPattern(amt.getValue().doubleValue())).append("</td>")
                            .append("<td>").append(creditDebitIndicator.value()).append("</td></tr>");
                });

                emailMessageBuilder.append("</tbody></table>");
                notificationRequestDTOBuilder.attachmentFilePaths(new ArrayList<>());
                notificationRequestDTOBuilder.messageText(emailMessageBuilder.toString());
                notificationRequestDTOBuilder.notificationType(NotificationType.DEBIT_CREDIT);
                notificationRequestDTOBuilder.clientId(person.getId());
                messageService.processAndSendNotifications(notificationRequestDTOBuilder.build());
            } else {
                log.error("Email not found. Could not notify.");
            }
        });
    }

    private Optional<Person> getPerson(GroupHeader73 grpHdr, String accountNumber) {
        if (grpHdr != null) {
            try {
                final PartyIdentification125 msgRcpt = grpHdr.getMsgRcpt();
                final Party34Choice id = msgRcpt.getId();
                final Optional<PersonIdentification> personIdentificationOptional
                        = personIdentificationRepository.getPersonIdentificationByUniqueField(id.getPrvtId().getOthr().get(0).getId());
                return personIdentificationOptional.map(personIdentification -> {
                    return personRepository.findPersonByIdentification(personIdentification);
                }).orElseGet(() -> {
                    return Optional.empty();
                });
            } catch (RuntimeException re) {
                return getPersonByAccountNumber(accountNumber);
            }
        } else {
            return getPersonByAccountNumber(accountNumber);
        }
    }

    private Optional<Person> getPersonByAccountNumber(String accountNumber) {
        final Optional<CashAccount> accountOptional = accountService.findAccountByIban(accountNumber);
        if(accountOptional.isPresent()) {
            final CashAccount cashAccount = accountOptional.get();
            return clientService.getPersonByAccountId(cashAccount.getId());
        } else {
            log.error("Account not found for acc number {}", accountNumber);
            return Optional.empty();
        }
    }

    private String getAccountNumber(String accountNumber) {
        final Optional<CashAccount> accountOptional = accountService.findAccountByIban(accountNumber);
        return accountOptional.map(cashAccount -> {
            return Optional.ofNullable(cashAccount.getIdentification())
                    .map(AccountIdentification::getNumber).orElse("");
        }).orElse("");
    }

    private BankToCustomerDebitCreditNotificationV07 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));

        return document.getBkToCstmrDbtCdtNtfctn();
    }
}
