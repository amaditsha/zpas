package zw.co.esolutions.zpas.integration.camel.config.util;

public class Konstants {
    public static final String NAMESPACE = "ns";
    public static final String MSG_ID = "msgId";
    public static final String ASSGNMT_ID = "assgnmtId";

    public static final String FILENAME = "filename";

    public static final String XML_ENCODING_UTF_8_ = "UTF-8";
    public static final String XML_ENCODING_US_ASCII = "us-ascii";

    public static final String AUTHENTICATED = "AUTHENTICATED";
    public static final String OTP_AUTHENTICATED = "OTP_AUTHENTICATED";
}
