package zw.co.esolutions.zpas.integration.camel.processor.type;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.enums.CaseStatusCode;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.iso.msg.camt030_001_04.NotificationOfCaseAssignmentV04;
import zw.co.esolutions.zpas.iso.msg.camt030_001_04.Document;
import zw.co.esolutions.zpas.iso.msg.camt030_001_04.Party12Choice;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentInvestigationCaseRepository;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationCaseStatusUtil;
import zw.co.esolutions.zpas.services.impl.process.util.StatusReasonUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.xml.bind.*;
import java.io.StringReader;
import java.lang.System;
import java.time.OffsetDateTime;
import java.util.Optional;


/**
 * Handler for CAMT.030
 */

@Component
@Slf4j
public class Camt030NotificationOfCaseAssignmentProcessor implements Processor {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt030_001_04");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    PaymentInvestigationCaseRepository paymentInvestigationCaseRepository;

    @Autowired
    InvestigationCaseStatusUtil investigationCaseStatusUtil;

    @Autowired
    StatusReasonUtil statusReasonUtil;

    @Autowired
    NotificationService notificationService;



    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Notification Of Case Assignment : \n{}", message);
        NotificationOfCaseAssignmentV04 notificationOfCaseAssignmentV04 = parseXml(message);

        log.info("Got message with assignment ID : {}", notificationOfCaseAssignmentV04.getAssgnmt().getId());
        String caseAssignmentId = notificationOfCaseAssignmentV04.getAssgnmt().getId();

        String caseId = notificationOfCaseAssignmentV04.getCase().getId();
        log.info("caseID is : {}", caseId);
//        String assignerID = getAssignerIDOrAssigneeID(notificationOfCaseAssignmentV04, Assigner.class);
//        log.info("assignerID is : {}", assignerID);
//        String assigneeID = getAssignerIDOrAssigneeID(notificationOfCaseAssignmentV04, Assignee.class);
//        log.info("assigneeID is : {}", assigneeID);

        String cretrId = null;
        /*try {
            Party12Choice cretr = notificationOfCaseAssignmentV04.getCase().getCretr();
            cretrId = getPartyId(cretr);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        //log.info("creator ID is : {}", cretrId);

        Optional<PaymentInvestigationCase> paymentInvestigationCaseOptional = Optional.ofNullable(paymentInvestigationCaseRepository.findByIdentification(caseId));

        if (paymentInvestigationCaseOptional.isPresent()){
            PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseOptional.get();
            cretrId = paymentInvestigationCase.getInvestigationPartyRole().stream().filter(investigationPartyRole -> investigationPartyRole instanceof CaseCreator)
                    .flatMap(investigationPartyRole -> investigationPartyRole.getPlayer().stream())
                    .map(player -> player.getId())
                    .findFirst()
                    .orElse("");
            log.info("creator ID is : {}", cretrId);
            log.info("Found Payment Investigation Case linked to this caseAssignmentId : {}", paymentInvestigationCase.getId());

            String ntfcnMsg = "";
            switch (notificationOfCaseAssignmentV04.getNtfctn().getJustfn()) {

                case FTHI:
//                    Case has been forwarded to the next party for further investigation.
                    ntfcnMsg = "Case has been forwarded to the next party for further investigation.";
                    log.info(ntfcnMsg);
                    forwardedCaseForFurtherInvestigation(paymentInvestigationCase);
                    break;
                case CANC:
//                     Case has been forwarded to the next party for cancellation.
                    ntfcnMsg = "Case has been forwarded to the next party for cancellation.";
                    log.info(ntfcnMsg);
                    forwardedCaseForCancellation(paymentInvestigationCase);
                    break;
                case MODI:
//                    Case has been forwarded to the next party for modification.
                    ntfcnMsg = "Case has been forwarded to the next party for modification.";
                    log.info(ntfcnMsg);
                    forwardedCaseForModification(paymentInvestigationCase);
                    break;
                case DTAU:
//                    Case has been forwarded to obtain authorisation to debit.
                    ntfcnMsg = "Case has been forwarded to obtain authorisation to debit.";
                    forwardedCaseForAuthorisation(paymentInvestigationCase);
                    log.info(ntfcnMsg);
                    break;
                case SAIN:
//                     Additional information has been forwarded to the creditor.
                    ntfcnMsg = "Additional information has been forwarded to the creditor.";
                    forwardedCaseToCreditor(paymentInvestigationCase);
                    log.info(ntfcnMsg);
                    break;
                case MINE:
//                     Case is been handled by the assigned party.
                    ntfcnMsg = "Case is been handled by the assigned party.";
                    handledByAssignedParty(paymentInvestigationCase);
                    log.info(ntfcnMsg);
                    break;
            }
            log.info("Notify User {} with message {}", cretrId, ntfcnMsg);
            if(cretrId != null) {
                notifyUser(ntfcnMsg, cretrId);
            }
        }
    }

    private String getAssignerIDOrAssigneeID(NotificationOfCaseAssignmentV04 notificationOfCaseAssignmentV05, Class<?> investigationPartyRoleSubclass) {
        if(investigationPartyRoleSubclass.isInstance(new Assigner())) {
            Party12Choice party40Choice = notificationOfCaseAssignmentV05.getAssgnmt().getAssgnr();
            return getPartyId(party40Choice);
        } else {
            Party12Choice party40Choice = notificationOfCaseAssignmentV05.getAssgnmt().getAssgne();
            return getPartyId(party40Choice);
        }
    }

    private String getPartyId(Party12Choice party12Choice) {
        String assignerID;
        if (party12Choice.getPty() != null) {
            if (party12Choice.getPty().getId().getOrgId() != null) {
                assignerID = party12Choice.getPty().getId().getOrgId().getAnyBIC();
            } else {
                assignerID = party12Choice.getPty().getId().getPrvtId().getOthr().get(0).getId();
            }
        } else {
            if (party12Choice.getAgt().getBrnchId() != null) {
                assignerID = party12Choice.getAgt().getBrnchId().getId();
            } else {
                assignerID = party12Choice.getAgt().getFinInstnId().getBICFI();
            }
        }
        return assignerID;
    }

    private void forwardedCaseForCancellation(PaymentInvestigationCase investigationCase) {
        investigationCaseStatusUtil.saveCaseStatus(investigationCase, CaseStatusCode.Assigned, "Case has been forwarded to the next party for cancellation.");
    }

    private void forwardedCaseForFurtherInvestigation(PaymentInvestigationCase investigationCase) {
        investigationCaseStatusUtil.saveCaseStatus(investigationCase, CaseStatusCode.Assigned, "Case has been forwarded to the next party for further investigation.");
    }

    private void forwardedCaseForModification(PaymentInvestigationCase investigationCase) {
        investigationCaseStatusUtil.saveCaseStatus(investigationCase, CaseStatusCode.UnderInvestigation, "Case has been forwarded to the next party for modification.");
    }

    private void forwardedCaseForAuthorisation(PaymentInvestigationCase investigationCase) {
        investigationCaseStatusUtil.saveCaseStatus(investigationCase, CaseStatusCode.Assigned, "Case has been forwarded to obtain authorisation to debit.");
    }

    private void forwardedCaseToCreditor(PaymentInvestigationCase investigationCase) {
        investigationCaseStatusUtil.saveCaseStatus(investigationCase, CaseStatusCode.Assigned, "Additional information has been forwarded to the creditor.");
    }

    private void handledByAssignedParty(PaymentInvestigationCase investigationCase) {
        investigationCaseStatusUtil.saveCaseStatus(investigationCase, CaseStatusCode.UnderInvestigation, "Case is been handled by the assigned party.");
    }

    private NotificationOfCaseAssignmentV04 parseXml(String message) throws JAXBException{
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
        System.out.println(document);

        NotificationOfCaseAssignmentV04 ntfctnOfCaseAssgnmt = document.getNtfctnOfCaseAssgnmt();
        return ntfctnOfCaseAssgnmt;
    }

    private void notifyUser(String msg, String userId) {
        Notification notification = new Notification();
        notification.setId(GenerateKey.generateEntityId());
        notification.setDateCreated(OffsetDateTime.now());
        notification.setEntityStatus(EntityStatus.ACTIVE);
        notification.setMessage(msg);
        notification.setRead(false);
        notification.setEntityId("");
        notification.setClientId(userId);
        notification.setNotificationType(NotificationType.CASE_ASSIGNMENT);
        notification.setGroupId("");
        notification.setVersion(0L);

        notificationService.createNotification(notification);

    }
}
