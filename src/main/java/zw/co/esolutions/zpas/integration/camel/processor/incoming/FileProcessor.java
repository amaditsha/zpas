package zw.co.esolutions.zpas.integration.camel.processor.incoming;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.File;
import java.nio.file.Files;
import java.util.stream.Collectors;

@Component
@Transactional
public class FileProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(FileProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {

        File file = exchange.getIn().getBody(File.class);

        log.info("Incoming message => " + file.getAbsolutePath());

        final String xml = Files.lines(file.toPath()).collect(Collectors.reducing("", (s1, s2) -> s1 + "\n" + s2));

        exchange.getOut().setBody(xml);
    }
}
