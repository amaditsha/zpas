package zw.co.esolutions.zpas.integration.camel.processor.type;
import zw.co.esolutions.zpas.enums.MandateReasonCode;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.enums.PendingFailingSettlementCode;
import zw.co.esolutions.zpas.model.PaymentInvestigationCase;
import zw.co.esolutions.zpas.enums.PendingSettlementStatusReasonCode;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.enums.NotificationToReceiveStatusCode;
import zw.co.esolutions.zpas.enums.UnmatchedStatusReasonCode;
import zw.co.esolutions.zpas.enums.TransactionReasonCode;
import zw.co.esolutions.zpas.enums.SuspendedStatusReasonCode;
import zw.co.esolutions.zpas.enums.PaymentInstructionStatusCode;
import zw.co.esolutions.zpas.enums.CashPaymentStatusCode;
import zw.co.esolutions.zpas.enums.TransactionStatusCode;
import zw.co.esolutions.zpas.codeset.ExternalStatusReason1Code;
import zw.co.esolutions.zpas.model.InvestigationPartyRole;
import zw.co.esolutions.zpas.model.InvestigationCase;
import zw.co.esolutions.zpas.enums.CancellationProcessingStatusCode;
import zw.co.esolutions.zpas.model.DateTimePeriod;
import zw.co.esolutions.zpas.enums.StatusCode;
import zw.co.esolutions.zpas.enums.SecuritiesSettlementStatusCode;
import zw.co.esolutions.zpas.enums.InstructionProcessingStatusCode;
import zw.co.esolutions.zpas.enums.CaseStatusCode;
import zw.co.esolutions.zpas.enums.ModificationProcessingStatusCode;
import zw.co.esolutions.zpas.iso.msg.camt026_001_06.Document;
import zw.co.esolutions.zpas.model.*;
import java.time.OffsetDateTime;

import zw.co.esolutions.zpas.enums.CancellationReasonCode;
import zw.co.esolutions.zpas.enums.UnableToApplyMissingInformationV2Code;
import zw.co.esolutions.zpas.enums.UnableToApplyIncorrectInfoCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt026_001_06.*;
import zw.co.esolutions.zpas.repository.PaymentExecutionRepository;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.repository.PaymentStatusRepository;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentStatusUtil;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.*;

/**
 * Handler for CAMT.026
 */

@Component
@Slf4j
public class Camt026UnableToApplyProcessor implements Processor {

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt026_001_06");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    PaymentExecutionRepository paymentExecutionRepository;

    @Autowired
    PaymentStatusRepository paymentStatusRepository;

    @Autowired
    PaymentStatusUtil paymentStatusUtil;

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Unable To Apply response : \n{}", message);

        UnableToApplyV06 unableToApplyV06 = parseXml(message);

        log.info("Got message with ID : {}", unableToApplyV06.getAssgnmt().getId());

        final UnderlyingPaymentInstruction4 underlyingPaymentInstruction4 = unableToApplyV06.getUndrlyg().getInitn();

        final UnderlyingGroupInformation1 orgnlGrpInf = underlyingPaymentInstruction4.getOrgnlGrpInf();

        String originalMsgID = orgnlGrpInf.getOrgnlMsgId();
        log.info("Original MsgID is : {}", originalMsgID);

        String originalMsgNameID = orgnlGrpInf.getOrgnlMsgNmId();
        log.info("Original MsgNameID is : {}", originalMsgNameID);


        String assignmentId = unableToApplyV06.getAssgnmt().getId();
        log.info("Assignment ID : {}", assignmentId);

        String caseId = unableToApplyV06.getCase().getId();
        log.info("Case ID : {}", caseId);

        Optional<PaymentExecution> paymentExecutionOptional = paymentExecutionRepository.findById(originalMsgID);

        Optional<Payment> originalPaymentOptional = paymentRepository.findById(underlyingPaymentInstruction4.getOrgnlInstrId());

        if (originalPaymentOptional.isPresent()) {

            Payment originalPayment = originalPaymentOptional.get();

            final boolean possibleDuplicate = unableToApplyV06.getJustfn().isPssblDplctInstr();

            log.info("This is a possible duplicate : {}", possibleDuplicate);

            final MissingOrIncorrectInformation3 mssngOrIncrrctInf = unableToApplyV06.getJustfn().getMssngOrIncrrctInf();

            final List<UnableToApplyIncorrect1> incrrctInf = mssngOrIncrrctInf.getIncrrctInf();

            final List<UnableToApplyMissing1> mssngInf = mssngOrIncrrctInf.getMssngInf();

            final boolean amlRequest = mssngOrIncrrctInf.isAMLReq();

            log.info("This is an AML Request : {}", amlRequest);

            PaymentInvestigationCase paymentInvestigationCase = new PaymentInvestigationCase();
            paymentInvestigationCase.setUnderlyingPayment(Arrays.asList(originalPayment));
            paymentInvestigationCase.setUnderlyingInstruction(paymentExecutionOptional.orElse(null));

            incrrctInf.stream().findFirst().ifPresent(unableToApplyIncorrect1 -> {
                paymentInvestigationCase.setIncorrectInformationReason(
                        UnableToApplyIncorrectInfoCode.valueOfCodeName(unableToApplyIncorrect1.getCd().value()));

            });
            mssngInf.stream().findFirst().ifPresent(unableToApplyMissing1 -> {
                paymentInvestigationCase.setMissingInformationReason(
                        UnableToApplyMissingInformationV2Code.valueOf(unableToApplyMissing1.getCd().value()));
            });

            paymentInvestigationCase.setCaseType("");
            paymentInvestigationCase.setId(UUID.randomUUID().toString());
            paymentInvestigationCase.setDateCreated(OffsetDateTime.now());
            paymentInvestigationCase.setLastUpdated(OffsetDateTime.now());
            paymentInvestigationCase.setEntityStatus(EntityStatus.ACTIVE);
            paymentInvestigationCase.setEntityVersion(0L);
            paymentInvestigationCase.setAssignmentIdentification(assignmentId);
            paymentInvestigationCase.setCreationDateTime(OffsetDateTime.now());
            paymentInvestigationCase.setIdentification(caseId);

            InvestigationCaseStatus investigationCaseStatus = new InvestigationCaseStatus();
            investigationCaseStatus.setCaseStatus(CaseStatusCode.Assigned);
            investigationCaseStatus.setInvestigationCase(paymentInvestigationCase);
            investigationCaseStatus.setId(UUID.randomUUID().toString());
            investigationCaseStatus.setEntityStatus(EntityStatus.ACTIVE);
            investigationCaseStatus.setStatusReason(new ArrayList<>());
            investigationCaseStatus.setStatusDateTime(OffsetDateTime.now());
            investigationCaseStatus.setValidityTime(new DateTimePeriod());
            investigationCaseStatus.setStatusDescription("");
            investigationCaseStatus.setPartyRole(new InvestigationPartyRole());

            paymentInvestigationCase.setStatus(Arrays.asList(investigationCaseStatus));


            InvestigationPartyRole investigationPartyRole = new InvestigationPartyRole();

            paymentInvestigationCase.setInvestigationPartyRole(new ArrayList<>());

            if (possibleDuplicate) {
                DuplicateCase duplicateCase = new DuplicateCase();
                duplicateCase.setDuplicatedCase(new ArrayList<>());
                duplicateCase.setRelatedCaseResolution(new PaymentInvestigationCaseResolution());
                duplicateCase.setId(UUID.randomUUID().toString());
                duplicateCase.setDateCreated(OffsetDateTime.now());
                duplicateCase.setLastUpdated(OffsetDateTime.now());
                duplicateCase.setEntityStatus(EntityStatus.ACTIVE);
                duplicateCase.setEntityVersion(0L);
                duplicateCase.setInvestigationCase(paymentInvestigationCase);
                duplicateCase.setInvestigationCaseReference(originalMsgID);

                paymentInvestigationCase.setDuplicateCaseResolution(duplicateCase);
            }
            paymentInvestigationCase.setInvestigationResolution(new ArrayList<>());
            paymentInvestigationCase.setOriginalInvestigationCase(new ArrayList<>());
            paymentInvestigationCase.setLinkedCase(new ArrayList<>());
            paymentInvestigationCase.setReassignment(new Reassignment());

            PaymentStatus paymentStatus = new PaymentStatus();
            paymentStatus.setStatus(PaymentStatusCode.AcceptedWithChange);
            paymentStatus.setPayment(originalPayment);
            paymentStatus.setRelatedInvestigationCase(paymentInvestigationCase);

            paymentStatus.setId(UUID.randomUUID().toString());
            paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
            paymentStatus.setStatusReason(new ArrayList<>());
            paymentStatus.setStatusDateTime(OffsetDateTime.now());
            paymentStatus.setValidityTime(new DateTimePeriod());
            paymentStatus.setStatusDescription("");
            paymentStatus.setPartyRole(new InvestigationPartyRole());

            paymentInvestigationCase.setPaymentStatus(Arrays.asList(paymentStatus));

        } else {
            log.info("Original payment not found");
        }
    }

    private UnableToApplyV06 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));

        UnableToApplyV06 unableToApplyV06 = document.getUblToApply();

        return unableToApplyV06;
    }
}
