package zw.co.esolutions.zpas.integration.camel.processor.type;

import org.springframework.beans.factory.annotation.Value;
import zw.co.esolutions.zpas.enums.CaseAssignmentRejectionCode;
import zw.co.esolutions.zpas.enums.PaymentCancellationRejectionCode;
import zw.co.esolutions.zpas.model.PaymentInvestigationCaseResolution;
import zw.co.esolutions.zpas.enums.PaymentModificationRejectionV2Code;
import zw.co.esolutions.zpas.enums.InvestigationRejectionCode;
import zw.co.esolutions.zpas.model.DebitAuthorisation;
import zw.co.esolutions.zpas.model.InvestigationCase;
import com.google.common.collect.Lists;
import zw.co.esolutions.zpas.enums.InvestigationExecutionConfirmationCode;
import zw.co.esolutions.zpas.model.CashSettlement;
import zw.co.esolutions.zpas.model.PaymentInvestigationCaseRejection;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.enums.CaseStatusCode;
import zw.co.esolutions.zpas.enums.InvestigationCaseType;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.enums.StatusCode;
import zw.co.esolutions.zpas.iso.msg.camt029_001_08.*;
import zw.co.esolutions.zpas.iso.msg.camt029_001_08.Document;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentInvestigationCaseRepository;
import zw.co.esolutions.zpas.repository.PaymentInvestigationCaseResolutionRepository;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.repo.ProfileRepository;
import zw.co.esolutions.zpas.services.impl.process.pojo.StatusReasonDto;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationCaseStatusUtil;
import zw.co.esolutions.zpas.services.impl.process.util.NotificationUtil;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentStatusUtil;
import zw.co.esolutions.zpas.services.impl.process.util.StatusReasonUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;

import javax.xml.bind.*;
import java.io.StringReader;
import java.lang.System;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


/**
 * Handler for CAMT.029
 */

@Component
@Slf4j
public class Camt029ResolutionOfInvestigationProcessor implements Processor {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt029_001_08");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    PaymentInvestigationCaseRepository paymentInvestigationCaseRepository;

    @Autowired
    InvestigationCaseStatusUtil investigationCaseStatusUtil;

    @Autowired
    StatusReasonUtil statusReasonUtil;

    @Autowired
    PaymentStatusUtil paymentStatusUtil;
    @Autowired
    NotificationUtil notificationUtil;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    PaymentInvestigationCaseResolutionRepository paymentInvestigationCaseResolutionRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Resolution of Investigation : \n{}", message);

        ResolutionOfInvestigationV08 resolutionOfInvestigationV08 = parseXml(message);
        log.info("Got message with MsgID : {}", resolutionOfInvestigationV08.getAssgnmt().getId());

        String caseAssignmentId = resolutionOfInvestigationV08.getAssgnmt().getId();
        log.info("caseAssignmentID is : {}", caseAssignmentId);

        String resolvedCaseId = resolutionOfInvestigationV08.getRslvdCase().getId();
        log.info("resolvedCaseID is : {}", resolvedCaseId);

        String assignerID = getAssignerIDOrAssigneeID(resolutionOfInvestigationV08, Assigner.class);
        log.info("assignerID is : {}", assignerID);

        String assigneeID = getAssignerIDOrAssigneeID(resolutionOfInvestigationV08, Assignee.class);
        log.info("assigneeID is : {}", assigneeID);

        Party35Choice cretr = resolutionOfInvestigationV08.getRslvdCase().getCretr();
        String cretrId = null;
        try {
            cretrId = getPartyId(cretr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("creator ID is : {}", cretrId);

        String creDtTm = resolutionOfInvestigationV08.getAssgnmt().getCreDtTm().toXMLFormat();
        log.info("creation Date Time is : {}", creDtTm);

        Optional<PaymentInvestigationCase> paymentInvestigationCaseOptional = Optional.ofNullable(paymentInvestigationCaseRepository.findByIdentification(resolvedCaseId));

        if (paymentInvestigationCaseOptional.isPresent()) {

            PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseOptional.get();

            log.info("Found Payment Investigation Case linked to this caseAssignmentId : {}", paymentInvestigationCase.getId());

            final Optional<Payment> paymentOptional = paymentInvestigationCase.getUnderlyingPayment().stream().findFirst();

            String investigationStatus = resolutionOfInvestigationV08.getSts().getConf();
            log.info("investigation status code: {}", investigationStatus);

            String confMsg = "";
            InvestigationExecutionConfirmationCode investigationExecutionConfirmationCode;
            switch (investigationStatus) {
                case "CNCL":
                    confMsg = "Cancelled as per request.";
                    log.info(confMsg);
                    List<StatusReasonDto> statusReasonDtoList = Arrays.asList(new StatusReasonDto(StatusCode.Cancelled.getCodeName(),
                            Arrays.asList("")));

                    paymentInvestigationCase.getUnderlyingPayment().forEach(payment1 -> {
                        paymentStatusUtil.updatePaymentStatus(payment1, PaymentStatusCode.AcceptedCancellationRequest, statusReasonDtoList);
                    });
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.CancelledAsPerRequest;

                    log.info("Updating the investigation case status for case ID {}", paymentInvestigationCase.getId());
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase,
                            CaseStatusCode.Closed, confMsg);
                    break;
                case "PDCR":
                    confMsg = "Pending Cancellation Request.";
                    log.info(confMsg);
                    List<StatusReasonDto> statusReason = Arrays.asList(new StatusReasonDto(StatusCode.Pending.getCodeName(),
                            Arrays.asList("")));

                    paymentInvestigationCase.getUnderlyingPayment().forEach(payment1 -> {
                        paymentStatusUtil.updatePaymentStatus(payment1, PaymentStatusCode.PendingCancellationRequest, statusReason);
                    });
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.PendingCancellationRequest;

                    log.info("Updating the investigation case status for case ID {}", paymentInvestigationCase.getId());
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase,
                            CaseStatusCode.UnderInvestigation, confMsg);
                    break;
                case "RJCR":
                    confMsg = "Rejected Cancellation Request.";
                    log.info(confMsg);
                    List<StatusReasonDto> statusReasonDtos = Arrays.asList(new StatusReasonDto(StatusCode.Rejected.getCodeName(),
                            Arrays.asList("")));

                    paymentInvestigationCase.getUnderlyingPayment().forEach(payment1 -> {
                        paymentStatusUtil.updatePaymentStatus(payment1, PaymentStatusCode.RejectedCancellationRequest, statusReasonDtos);
                    });
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.RejectedCancellationRequest;

                    log.info("Updating the investigation case status for case ID {}", paymentInvestigationCase.getId());
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase,
                            CaseStatusCode.Closed, confMsg);
                    break;
                case "RCNT":
                    confMsg = "Resolved ClaimNon Receipt.";
                    log.info(confMsg);
                    List<StatusReasonDto> statusReasonDtoLists = Arrays.asList(new StatusReasonDto(StatusCode.Accepted.getCodeName(),
                            Arrays.asList("")));

                    paymentInvestigationCase.getUnderlyingPayment().forEach(payment1 -> {
                        paymentStatusUtil.updatePaymentStatus(payment1, PaymentStatusCode.Accepted, statusReasonDtoLists);
                    });
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.ResolvedNonClaim;

                    log.info("Updating the investigation case status for case ID {}", paymentInvestigationCase.getId());
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase,
                            CaseStatusCode.Closed, confMsg);
                    break;
                case "MODI":
                    confMsg = "Modification as per request.";
                    log.info(confMsg);
                    /*List<StatusReasonDto> statusReasonDtoList1 = Arrays.asList(new StatusReasonDto(StatusCode.Accepted.getCodeName(),
                            Arrays.asList("")));

                    paymentInvestigationCase.getUnderlyingPayment().forEach(payment1 -> {
                        paymentStatusUtil.updatePaymentStatus(payment1, PaymentStatusCode.AcceptedCancellationRequest, statusReasonDtoList1);
                    });*/
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.ModifiedAsPerRequest;

                    log.info("Updating the investigation case status for case ID {}", paymentInvestigationCase.getId());
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase,
                            CaseStatusCode.Closed, confMsg);
                    break;

                default:
                    confMsg = "No case found.";
                    log.info(confMsg);
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.NONE;
                    break;
            }

            log.info("Creating the payment investigation case resolution for case with id : {} and code : {}",
                    paymentInvestigationCase.getId(), investigationExecutionConfirmationCode);
            handleInvestigationCaseResolution(paymentInvestigationCase, investigationExecutionConfirmationCode);

            log.info("Getting the role player from the case creator of investigation party role");
            notifyingCreater(paymentInvestigationCase, confMsg);

        } else {
            log.info("Payment investigation case assignment {} linked to this MsgID not found", caseAssignmentId);
            log.info("Resolved case id : {}", resolvedCaseId);
        }
    }

    private void handleInvestigationCaseResolution(PaymentInvestigationCase paymentInvestigationCase,
                                                   InvestigationExecutionConfirmationCode investigationExecutionConfirmationCode) {

        PaymentInvestigationCaseResolution paymentInvestigationCaseResolution = new PaymentInvestigationCaseResolution();
        paymentInvestigationCaseResolution.setInvestigationStatus(investigationExecutionConfirmationCode);
//        paymentInvestigationCaseResolution.setDebitAuthorisationConfirmation(new DebitAuthorisation());
//        paymentInvestigationCaseResolution.setEntryCorrection(Lists.newArrayList());
        paymentInvestigationCaseResolution.setPaymentCorrection(paymentInvestigationCase.getUnderlyingPayment());

        /**
         * The related payment executions for the case
         * */
        List<PaymentExecution> paymentExecutions = new ArrayList<>();
        paymentExecutions.add(paymentInvestigationCase.getUnderlyingInstruction());
        paymentInvestigationCaseResolution.setPaymentExecutionCorrection(paymentExecutions);

//        paymentInvestigationCaseResolution.setDuplicateCase(Lists.newArrayList());
        paymentInvestigationCaseResolution.setId(GenerateKey.generateEntityId());
        paymentInvestigationCaseResolution.setDateCreated(OffsetDateTime.now());
        paymentInvestigationCaseResolution.setEntityStatus(EntityStatus.NONE);
        paymentInvestigationCaseResolution.setEntityVersion(0L);
        paymentInvestigationCaseResolution.setInvestigationCase(paymentInvestigationCase);
        paymentInvestigationCaseResolution.setInvestigationCaseReference(paymentInvestigationCase.getAssignmentIdentification());

        log.info("Saving the payment investigation case resolution");
        paymentInvestigationCaseResolutionRepository.save(paymentInvestigationCaseResolution);
    }

    private void notifyingCreater(PaymentInvestigationCase paymentInvestigationCase, String confMsg) {
        Optional<RolePlayer> rolePlayer = paymentInvestigationCase.getInvestigationPartyRole().stream()
                .filter(CaseCreator.class::isInstance)
                .map(Role::getPlayer)
                .map(rolePlayers -> rolePlayers.stream().findFirst().orElseGet(null))
                .findFirst();

        if (rolePlayer.isPresent()) {
            log.info("Found the case creator for investigation case {}", paymentInvestigationCase.getCaseType());
            log.info("Send the system notification ...");
            String messageText = confMsg + "For the case type " + paymentInvestigationCase.getCaseType();
            String subject = "Notification for case type " + paymentInvestigationCase.getCaseType();
            String originator = "System";
            notificationUtil.sendNotification(rolePlayer.get().getId(), "", originator, OffsetDateTime.now(), messageText, MessageChannel.EMAIL, subject);

        }
    }


    private String getAssignerIDOrAssigneeID(ResolutionOfInvestigationV08 resolutionOfInvestigationV08, Class<?> investigationPartyRoleSubclass) {
        if (investigationPartyRoleSubclass.isInstance(new Assigner())) {
            Party35Choice party35Choice = resolutionOfInvestigationV08.getAssgnmt().getAssgnr();
            return getPartyId(party35Choice);
        } else {
            Party35Choice party35Choice = resolutionOfInvestigationV08.getAssgnmt().getAssgne();
            return getPartyId(party35Choice);
        }
    }

    private String getPartyId(Party35Choice party35Choice) {
        String assignerID;
        if (party35Choice.getPty() != null) {
            if (party35Choice.getPty().getId().getOrgId() != null) {
                assignerID = party35Choice.getPty().getId().getOrgId().getAnyBIC();
            } else {
                assignerID = party35Choice.getPty().getId().getPrvtId().getOthr().get(0).getId();
            }
        } else {
            if (party35Choice.getAgt().getBrnchId() != null) {
                assignerID = party35Choice.getAgt().getBrnchId().getId();
            } else {
                assignerID = party35Choice.getAgt().getFinInstnId().getBICFI();
            }
        }
        return assignerID;
    }


    private ResolutionOfInvestigationV08 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
        System.out.println(document);

        ResolutionOfInvestigationV08 resolutionOfInvestigationV08 = document.getRsltnOfInvstgtn();

        return resolutionOfInvestigationV08;
    }
}
