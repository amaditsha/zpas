package zw.co.esolutions.zpas.integration.camel.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.model.HubConfiguration;
import zw.co.esolutions.zpas.repository.HubConfigurationRepository;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by mabuza on 09 Jul 2019
 */
@Slf4j
@Singleton
@Component
public class CamelRouteInitializer {

    @Autowired
    HubConfigurationRepository hubConfigurationRepository;

    CamelContext context = new DefaultCamelContext();

    @PostConstruct
    public void init() {
        try {
            context.addRoutes(new GatewayRouteBuilder(this.getHubConfigurations()));
            context.start();
            log.info("Camel context started with endpoints -> {}", context.getRoutes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<HubConfiguration> getHubConfigurations() {
        log.info("Initializing routes from DB HubConfiguration info..");
        List<HubConfiguration> hubConfigurations = hubConfigurationRepository.findAll();
        log.info("Hub Configurations List -> {}", hubConfigurations);
        return hubConfigurations;
    }
}
