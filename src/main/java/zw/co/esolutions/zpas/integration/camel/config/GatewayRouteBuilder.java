package zw.co.esolutions.zpas.integration.camel.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import zw.co.esolutions.zpas.model.HubConfiguration;

import java.util.List;

/**
 * Created by mabuza on 09 Jul 2019
 */

@Slf4j
public class GatewayRouteBuilder extends RouteBuilder {
    public static final String SOURCE_BASE_COMPONENT = "jetty";
    public static final String SOURCE_BASE_URL = "http://127.0.0.1:8099/api/hub/request";
    public static final String SOURCE_CAMEL_BASE_URI = SOURCE_BASE_COMPONENT + ":" + SOURCE_BASE_URL;

    public static final String CALL_SOURCE_URL = "http4://127.0.0.1:8092/api/hub/request";

    private List<HubConfiguration> hubConfigurations;

    public GatewayRouteBuilder(List<HubConfiguration> hubConfigurations) {
        this.hubConfigurations = hubConfigurations;
    }

    /**
     * <b>Called on initialization to build the routes using the fluent builder syntax.</b>
     * <p/>
     * This is a central method for RouteBuilder implementations to implement
     * the routes using the Java fluent builder syntax.
     *
     * @throws Exception can be thrown during configuration
     */
    @Override
    public void configure() throws Exception {
        log.info("Configuring camel endpoints");
        for (HubConfiguration hubConfiguration : hubConfigurations) {
            log.info("Building route for mapping -> {}|{}", hubConfiguration.getBIC(), hubConfiguration.getUri());

            from(SOURCE_CAMEL_BASE_URI + "/" + hubConfiguration.getBIC())
                    .to(hubConfiguration.getUri());
        }
    }
}
