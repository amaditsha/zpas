package zw.co.esolutions.zpas.integration.camel.processor.builder;
import lombok.extern.slf4j.Slf4j;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.PostalAddress24;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.ClearingSystemMemberIdentification2;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.GenericFinancialIdentification1;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.FinancialInstitutionIdentification18;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.BranchData3;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.NamePrefix2Code;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.PreferredContactMethod1Code;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.DateAndPlaceOfBirth1;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.OrganisationIdentification29;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.PersonIdentification13;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.AddressType3Choice;

import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.*;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.PaymentObligation;
import zw.co.esolutions.zpas.utilities.enums.GroupStatus;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Optional;

/**
 * Created by alfred on 25 Apr 2019
 */
@Slf4j
@Component
public class CreditorPaymentActivationRequestStatusReportMessageBuilder {
    public CreditorPaymentActivationRequestStatusReportV07
    buildCreditorPaymentActivationRequestStatusReportMessage(
            zw.co.esolutions.zpas.iso.msg.pain013_001_07.GroupHeader78 groupHeader78, GroupStatus groupStatus, PaymentObligation paymentObligation) {
        CreditorPaymentActivationRequestStatusReportV07 creditorPaymentActivationRequestStatusReportV07 = new CreditorPaymentActivationRequestStatusReportV07();
        final GroupHeader87 groupHeader87 = new GroupHeader87();
        groupHeader87.setMsgId(groupHeader78.getMsgId());
        groupHeader87.setCreDtTm(groupHeader78.getCreDtTm());
        final zw.co.esolutions.zpas.iso.msg.pain013_001_07.PartyIdentification135 initgPty = groupHeader78.getInitgPty();
        final PartyIdentification135 partyIdentification135 = new PartyIdentification135();
        partyIdentification135.setNm(groupHeader78.getInitgPty().getNm());
        final PostalAddress24 postalAddress24 = new PostalAddress24();
        postalAddress24.setAdrTp(new AddressType3Choice());
        Optional.ofNullable(initgPty.getPstlAdr()).ifPresent(postalAddress241 -> {
            postalAddress24.setDept(postalAddress241.getDept());
            postalAddress24.setSubDept(postalAddress241.getSubDept());
            postalAddress24.setStrtNm(postalAddress241.getStrtNm());
            postalAddress24.setBldgNb(postalAddress241.getBldgNb());
            postalAddress24.setBldgNm(postalAddress241.getBldgNm());
            postalAddress24.setFlr(postalAddress241.getFlr());
            postalAddress24.setPstBx(postalAddress241.getPstBx());
            postalAddress24.setRoom(postalAddress241.getRoom());
            postalAddress24.setPstCd(postalAddress241.getPstCd());
            postalAddress24.setTwnNm(postalAddress241.getTwnNm());
            postalAddress24.setTwnLctnNm(postalAddress241.getTwnLctnNm());
            postalAddress24.setDstrctNm(postalAddress241.getDstrctNm());
            postalAddress24.setCtrySubDvsn(postalAddress241.getCtrySubDvsn());
            postalAddress24.setCtry(postalAddress241.getCtry());
        });

        partyIdentification135.setPstlAdr(postalAddress24);
        final Party38Choice party38Choice = new Party38Choice();
        final OrganisationIdentification29 organisationIdentification29 = new OrganisationIdentification29();

        Optional.ofNullable(initgPty.getId()).ifPresent(party38Choice1 -> {
            Optional.ofNullable(party38Choice1.getOrgId()).ifPresent(organisationIdentification291 -> {
                organisationIdentification29.setAnyBIC(organisationIdentification291.getAnyBIC());
                organisationIdentification29.setLEI(organisationIdentification291.getLEI());
            });
        });
        party38Choice.setOrgId(organisationIdentification29);
        final PersonIdentification13 personIdentification13 = new PersonIdentification13();
        final DateAndPlaceOfBirth1 dateAndPlaceOfBirth1 = new DateAndPlaceOfBirth1();
        Optional.ofNullable(initgPty.getId()).ifPresent(party38Choice1 -> {
            Optional.ofNullable(party38Choice1.getPrvtId()).ifPresent(personIdentification131 -> {
                Optional.ofNullable(personIdentification131.getDtAndPlcOfBirth()).ifPresent(dateAndPlaceOfBirth11 -> {
                    dateAndPlaceOfBirth1.setBirthDt(dateAndPlaceOfBirth11.getBirthDt());
                    dateAndPlaceOfBirth1.setPrvcOfBirth(dateAndPlaceOfBirth11.getPrvcOfBirth());
                    dateAndPlaceOfBirth1.setCityOfBirth(dateAndPlaceOfBirth11.getCityOfBirth());
                    dateAndPlaceOfBirth1.setCtryOfBirth(dateAndPlaceOfBirth11.getCtryOfBirth());
                });
            });
        });

        personIdentification13.setDtAndPlcOfBirth(dateAndPlaceOfBirth1);
        party38Choice.setPrvtId(personIdentification13);

        partyIdentification135.setId(party38Choice);
        partyIdentification135.setCtryOfRes(groupHeader78.getInitgPty().getCtryOfRes());
        final Contact4 contact4 = new Contact4();
        Optional.ofNullable(initgPty.getCtctDtls()).ifPresent(contact41 -> {
            contact4.setNm(contact41.getNm());
            contact4.setPhneNb(contact41.getPhneNb());
            contact4.setMobNb(contact41.getMobNb());
            contact4.setFaxNb(contact41.getFaxNb());
            contact4.setEmailAdr(contact41.getEmailAdr());
            contact4.setEmailPurp(contact41.getEmailPurp());
            contact4.setJobTitl(contact41.getJobTitl());
            contact4.setRspnsblty(contact41.getRspnsblty());
            contact4.setDept(contact41.getDept());
            try {
                contact4.setNmPrfx(NamePrefix2Code.valueOf(contact41.getNmPrfx().value()));
                contact4.setPrefrdMtd(PreferredContactMethod1Code.valueOf(contact41.getPrefrdMtd().value()));
            } catch (RuntimeException re) {
                log.error("Could not set preferred method or Name Prefix. Error is: {}", re.getMessage());
            }
            partyIdentification135.setCtctDtls(contact4);
        });

        groupHeader87.setInitgPty(partyIdentification135);
        final BranchAndFinancialInstitutionIdentification6 debtorAgent = new BranchAndFinancialInstitutionIdentification6();
        final FinancialInstitutionIdentification18 debtorAgentFinancialInstitutionIdentification18 = new FinancialInstitutionIdentification18();
        debtorAgentFinancialInstitutionIdentification18.setBICFI("");
        debtorAgentFinancialInstitutionIdentification18.setClrSysMmbId(new ClearingSystemMemberIdentification2());
        debtorAgentFinancialInstitutionIdentification18.setLEI("");
        debtorAgentFinancialInstitutionIdentification18.setNm("");
        debtorAgentFinancialInstitutionIdentification18.setPstlAdr(new PostalAddress24());
        debtorAgentFinancialInstitutionIdentification18.setOthr(new GenericFinancialIdentification1());
        debtorAgent.setFinInstnId(debtorAgentFinancialInstitutionIdentification18);
        final BranchData3 debtorAgentBranchData3 = new BranchData3();
        debtorAgentBranchData3.setId("");
        debtorAgentBranchData3.setLEI("");
        debtorAgentBranchData3.setNm("");
        debtorAgentBranchData3.setPstlAdr(new PostalAddress24());
        debtorAgent.setBrnchId(debtorAgentBranchData3);

        final BranchAndFinancialInstitutionIdentification6 creditorAgent = new BranchAndFinancialInstitutionIdentification6();
        final FinancialInstitutionIdentification18 creditorAgentFinancialInstitutionIdentification18 = new FinancialInstitutionIdentification18();
        creditorAgentFinancialInstitutionIdentification18.setBICFI("");
        creditorAgentFinancialInstitutionIdentification18.setClrSysMmbId(new ClearingSystemMemberIdentification2());
        creditorAgentFinancialInstitutionIdentification18.setLEI("");
        creditorAgentFinancialInstitutionIdentification18.setNm("");
        creditorAgentFinancialInstitutionIdentification18.setPstlAdr(new PostalAddress24());
        creditorAgentFinancialInstitutionIdentification18.setOthr(new GenericFinancialIdentification1());
        creditorAgent.setFinInstnId(creditorAgentFinancialInstitutionIdentification18);
        final BranchData3 creditorAgentBranchData3 = new BranchData3();
        creditorAgentBranchData3.setId("");
        creditorAgentBranchData3.setLEI("");
        creditorAgentBranchData3.setNm("");
        creditorAgentBranchData3.setPstlAdr(new PostalAddress24());
        creditorAgent.setBrnchId(creditorAgentBranchData3);

        groupHeader87.setDbtrAgt(debtorAgent);
        groupHeader87.setCdtrAgt(creditorAgent);

        creditorPaymentActivationRequestStatusReportV07.setGrpHdr(groupHeader87);
        final OriginalGroupInformation30 originalGroupInformation30 = new OriginalGroupInformation30();
        originalGroupInformation30.setOrgnlMsgId(Optional.ofNullable(paymentObligation).map(PaymentObligation::getId).orElse(groupHeader78.getMsgId()));
        originalGroupInformation30.setOrgnlMsgNmId(groupHeader78.getMsgId());
        originalGroupInformation30.setOrgnlCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));
        originalGroupInformation30.setOrgnlNbOfTxs(groupHeader78.getNbOfTxs());
        originalGroupInformation30.setOrgnlCtrlSum(groupHeader78.getCtrlSum());
        originalGroupInformation30.setGrpSts(groupStatus.name());

        creditorPaymentActivationRequestStatusReportV07.setOrgnlGrpInfAndSts(originalGroupInformation30);

        return creditorPaymentActivationRequestStatusReportV07;
    }
}
