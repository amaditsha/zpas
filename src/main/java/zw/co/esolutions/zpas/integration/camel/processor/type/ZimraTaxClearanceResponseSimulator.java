package zw.co.esolutions.zpas.integration.camel.processor.type;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.model.Organisation;
import zw.co.esolutions.zpas.repository.OrganisationIdentificationRepository;
import zw.co.esolutions.zpas.services.iface.taxclearance.TaxClearanceService;
import zw.co.esolutions.zpas.services.impl.taxclearance.TaxClearanceJsonResponse;

import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * Created by alfred on 23 May 2019
 */
@Slf4j
@Component
public class ZimraTaxClearanceResponseSimulator implements Processor {
    @Autowired
    private OrganisationIdentificationRepository organisationIdentificationRepository;

    private String[] companyNames = new String[]{
            "Iron Media",
            "High Tide Media",
            "Smart Arts",
            "Orco",
            "Redustries",
            "Tuliproductions",
            "Mountainetworks",
            "Jetcloud",
            "Rosewares",
            "Apexaid",
            "Prodigy Systems",
            "Lucent Coms",
            "Rose Coms",
            "Glaciarts",
            "Cruxolutions",
            "Daydreamotors",
            "Freacrosystems",
            "Phoenixtime",
            "Mermaidspace",
            "Lagoonpoint",
            "Daydream Co.",
            "Wizard Corporation",
            "Flux Coms",
            "Herbrews",
            "Purplelimited",
            "Oracleutions",
            "Paragonetworks",
            "Wavehive",
            "Jetaid",
            "Typhooncoms"
    };

    @Value("${zimra.reply.queue.url}")
    public String zimraReplyQueue;

    @Autowired
    CamelContext camelContext;

    public boolean routeMessageToZimraReplyQueue(String message) {
        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        producerTemplate.sendBody(zimraReplyQueue, message);
        return true;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        List<TaxClearanceJsonResponse> taxClearanceJsons = mapTaxClearancesFromJsonArray(message);

        ObjectMapper mapper = new ObjectMapper();

        /**
         * Write object to file
         */
        StringWriter writer = new StringWriter();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(writer, taxClearanceJsons);//Prettified JSON
        } catch (Exception e) {
            e.printStackTrace();
        }
        final String zimraResponseMessage = writer.toString();
        routeMessageToZimraReplyQueue(zimraResponseMessage);

    }

    public List<TaxClearanceJsonResponse> mapTaxClearancesFromJsonArray(String json) {
        ObjectMapper mapper = new ObjectMapper();

        /**
         * Read object from file
         */
        List<TaxClearanceJsonResponse> taxClearanceJsonObjects = null;
        try {
            taxClearanceJsonObjects = Arrays.asList(mapper.readValue(json, TaxClearanceJsonResponse[].class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return taxClearanceJsonObjects.stream().map(taxClearanceJson -> {
            String companyName = taxClearanceJson.getCompanyName();
            if (StringUtils.isEmpty(companyName)) {
                companyName = organisationIdentificationRepository.getByBusinessPartnerNumber(taxClearanceJson.getBpn())
                        .map(oi -> {
                            return Optional.ofNullable(oi.getOrganisation()).map(Organisation::getName).orElse("");
                        }).orElse("");
            }
            if(StringUtils.isEmpty(companyName)) {
                companyName = companyNames[ThreadLocalRandom.current().nextInt(0, companyNames.length)];
            }
            return TaxClearanceJsonResponse.builder()
                    .companyName(companyName)
                    .bpn(taxClearanceJson.getBpn())
                    .exists(true)
                    .startDate(OffsetDateTime.now().format(TaxClearanceService.DATE_TIME_FORMATTER))
                    .expiryDate(OffsetDateTime.now().plusMonths(6).format(TaxClearanceService.DATE_TIME_FORMATTER))
                    .build();
        }).collect(Collectors.toList());
    }
}
