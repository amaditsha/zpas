package zw.co.esolutions.zpas.integration.camel.processor.type;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.iso.msg.acmt024_001_02.Document;
import zw.co.esolutions.zpas.iso.msg.acmt024_001_02.*;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.repository.PaymentStatusRepository;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.services.impl.payments.cache.PaymentsCacheProcessor;
import zw.co.esolutions.zpas.services.impl.payments.verification.sse.domain.PaymentStatusInfo;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Handler for ACMT.024
 */

@Slf4j
@Component
@Transactional
public class Acmt024IdentificationVerificationReportProcessor implements Processor {
    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private PaymentsCacheProcessor cacheProcessor;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private PaymentRepository paymentRepository;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.acmt024_001_02");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Identification Verification Report: \n{}", message);

        final IdentificationVerificationReportV02 identificationVerificationReportV02 = parseXml(message);
        final IdentificationAssignment2 identificationAssignment = identificationVerificationReportV02.getAssgnmt();
        final MessageIdentification5 originalAssignmentMessageIdentification = identificationVerificationReportV02.getOrgnlAssgnmt();
        final List<VerificationReport2> verificationReports = identificationVerificationReportV02.getRpt();

        List<PaymentStatusInfo> paymentStatusInfos = new ArrayList<>();
        List<Payment> payments = new ArrayList<>();

        verificationReports.forEach(verificationReport2 -> {
            final String orginalId = verificationReport2.getOrgnlId();
            final boolean verifiedInformationCorrect = verificationReport2.isVrfctn();
            final VerificationReason1Choice verificationReason = verificationReport2.getRsn();
            final IdentificationInformation2 originalPartyAndAccountIdentification = verificationReport2.getOrgnlPtyAndAcctId();
            final IdentificationInformation2 updatedPartyAndAccountIdentification = verificationReport2.getUpdtdPtyAndAcctId();

            List<String> failedVerifications = new ArrayList<>();
            final Optional<Payment> paymentOptional = paymentsService.getPaymentByEndToEndId(orginalId);
            if(paymentOptional.isPresent()) {
                final Payment payment = paymentOptional.get();
                PaymentStatusInfo statusInfo = PaymentStatusInfo.builder()
                        .requestId(payment.getId())
                        .status(verifiedInformationCorrect? "SUCCESS": "FAILED")
                        .info(verifiedInformationCorrect? "Verification completed. Information correct.": "Verification completed. Information incorrect.")
                        .build();
                paymentStatusInfos.add(statusInfo);
                payment.setVerifiedSuccessfully(verifiedInformationCorrect);
                if (verifiedInformationCorrect) {
                    payment.setEntityStatus(EntityStatus.PENDING_APPROVAL);

                    final PaymentStatus paymentStatus = new PaymentStatus();
                    paymentStatus.setStatus(PaymentStatusCode.AcceptedCustomerProfile);
                    paymentStatus.setPayment(payment);
                    paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                    paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                    paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                    paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.Validated);
                    paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                    paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                    paymentStatus.setTransactionStatus(TransactionStatusCode.None);
                    paymentStatus.setId(GenerateKey.generateEntityId());
                    paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                    paymentStatus.setDateCreated(OffsetDateTime.now());
                    paymentStatus.setLastUpdated(OffsetDateTime.now());
                    paymentStatus.setStatusDateTime(OffsetDateTime.now());
                    paymentStatus.setStatusDescription("Account Info successfully validated");
                    paymentStatus.setInstructionProcessingStatus(StatusCode.Pending);
                    final List<StatusReason> statusReasons = new ArrayList<>();
                    final StatusReason statusReason = new StatusReason();
                    statusReason.setId(GenerateKey.generateEntityId());
                    statusReason.setEntityStatus(EntityStatus.ACTIVE);
                    statusReason.setDateCreated(OffsetDateTime.now());
                    statusReason.setLastUpdated(OffsetDateTime.now());
                    statusReason.setStatus(paymentStatus);
                    statusReason.setReason("Account Information validation successful.");
                    paymentStatus.setStatusReason(statusReasons);

                    final PaymentStatus savedPaymentStatus = paymentStatusRepository.save(paymentStatus);

                    payment.getPaymentStatus().add(savedPaymentStatus);
                    paymentRepository.save(payment);
                    setUpdatedPartyAndAccountInformation(payment, updatedPartyAndAccountIdentification);
                    payment.setVerificationNarrative(Optional.ofNullable(verificationReason).map(v -> v.getCd() + ": " + v.getPrtry()).orElse(""));
                    final Payment savedPayment = paymentRepository.save(payment);
                    payments.add(savedPayment);
                    log.info("Payment with reference {} account information verified successfully.", orginalId);
                } else {
                    failedVerifications.add(orginalId);
                    payment.setEntityStatus(EntityStatus.FAILED);

                    final PaymentStatus paymentStatus = new PaymentStatus();
                    paymentStatus.setStatus(PaymentStatusCode.Rejected);
                    paymentStatus.setPayment(payment);
                    paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                    paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                    paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                    paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.Rejected);
                    paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                    paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                    paymentStatus.setTransactionStatus(TransactionStatusCode.None);
                    paymentStatus.setId(GenerateKey.generateEntityId());
                    paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                    paymentStatus.setDateCreated(OffsetDateTime.now());
                    paymentStatus.setLastUpdated(OffsetDateTime.now());
                    paymentStatus.setStatusDateTime(OffsetDateTime.now());
                    paymentStatus.setStatusDescription("Initialising Payment Activation Request");
                    paymentStatus.setInstructionProcessingStatus(StatusCode.Rejected);
                    final List<StatusReason> statusReasons = new ArrayList<>();
                    final StatusReason statusReason = new StatusReason();
                    statusReason.setId(GenerateKey.generateEntityId());
                    statusReason.setEntityStatus(EntityStatus.ACTIVE);
                    statusReason.setDateCreated(OffsetDateTime.now());
                    statusReason.setLastUpdated(OffsetDateTime.now());
                    statusReason.setStatus(paymentStatus);
                    statusReason.setReason("Code: " + verificationReason.getCd() + ". Proprietary: " + verificationReason.getPrtry());
                    statusReason.setRejectedStatusReason(RejectedStatusReasonCode.DataInvalid);
                    statusReason.setRejectionReason(RejectionReasonV2Code.FailedValidation);
                    paymentStatus.setStatusReason(statusReasons);

                    final PaymentStatus savedPaymentStatus = paymentStatusRepository.save(paymentStatus);

                    payment.getPaymentStatus().add(savedPaymentStatus);
                    payment.setVerificationNarrative(Optional.ofNullable(verificationReason).map(v -> v.getCd() + ": " + v.getPrtry()).orElse(""));
                    final Payment savedPayment = paymentRepository.save(payment);
                    payments.add(savedPayment);
                }
            } else {
                log.error("Payment not found for reference {}.", orginalId);
            }
        });

        payments.stream()
                .filter(payment -> payment instanceof IndividualPayment)
                .map(payment -> (IndividualPayment) payment)
                .filter(individualPayment -> individualPayment.getBulkPayment() != null)
                .findFirst()
                .map(individualPayment -> individualPayment.getBulkPayment())
                .ifPresent(bulkPayment -> {
                    List<PaymentStatusInfo> successfulPaymentStatuses = paymentStatusInfos.stream()
                            .peek(paymentStatusInfo -> log.info("Payment Status INFO: -> {}", paymentStatusInfo))
                            .filter(paymentStatusInfo -> paymentStatusInfo.getStatus().equals("SUCCESS"))
                            .collect(Collectors.toList());
                    if(successfulPaymentStatuses.size() > 0) {
                        bulkPayment.setEntityStatus(EntityStatus.PENDING_APPROVAL);
                        bulkPayment.setVerifiedSuccessfully(true);
                        paymentRepository.save(bulkPayment);
                    }
                    updatePaymentStatusInfoCache(paymentStatusInfos, bulkPayment);
                });

        if(payments.size() == 1) {
            updatePaymentStatusInfoCache(new ArrayList<>(), payments.get(0));
        }
    }

    private void setUpdatedPartyAndAccountInformation(Payment payment, IdentificationInformation2 updatedPartyAndAccountIdentification) {
        Optional.ofNullable(updatedPartyAndAccountIdentification.getAgt()).ifPresent(branchAndFIId5 -> {
            final String bicfi = Optional.ofNullable(branchAndFIId5.getFinInstnId()).map(f -> f.getBICFI()).orElse("");
            if(!StringUtils.isEmpty(bicfi)) {
                financialInstitutionService.getFinancialInstitutionByAnyBIC(bicfi).ifPresent(financialInstitution -> {
                    payment.setVerifiedBankName(financialInstitution.getName());
                });
            }
        });
        Optional.ofNullable(updatedPartyAndAccountIdentification.getPty()).ifPresent(partyIdentification43 -> {
            Optional.ofNullable(partyIdentification43.getNm()).ifPresent(s -> {
                payment.setVerifiedBeneficiaryName(s);
            });
        });
        Optional.ofNullable(updatedPartyAndAccountIdentification.getAcct()).ifPresent(accountIdentification -> {
            Optional.ofNullable(accountIdentification.getIBAN()).map(s -> {
                payment.setVerifiedAccountNumber(s);
                return s;
            }).orElseGet(() -> {
                Optional.ofNullable(accountIdentification.getOthr()).ifPresent(genericAccountIdentification1 -> {
                    payment.setVerifiedAccountNumber(genericAccountIdentification1.getId());
                });
                return "";
            });
        });
    }

    private PaymentStatusInfo updatePaymentStatusInfoCache(List<PaymentStatusInfo> paymentStatusInfos, Payment payment) {
        if(payment instanceof BulkPayment) {
            return cacheProcessor.getPaymentStatusInfoMap().computeIfPresent(payment.getId(), (key, value) -> {
                List<PaymentStatusInfo> successfulPaymentStatuses = paymentStatusInfos.stream().filter(paymentStatusInfo -> paymentStatusInfo.getStatus().equals("SUCCESS"))
                        .collect(Collectors.toList());

                PaymentStatusInfo.PaymentStatusInfoBuilder paymentStatusInfoBuilder = PaymentStatusInfo.builder();
                paymentStatusInfoBuilder.requestId(payment.getId());
                paymentStatusInfoBuilder.status(successfulPaymentStatuses.size() > 0 ? "SUCCESS" : "FAILED");
                paymentStatusInfoBuilder.info(successfulPaymentStatuses.size() > 0 ? "Verification completed. Information correct." : "Verification completed. Information incorrect.");

                paymentStatusInfoBuilder.sseMessages(paymentStatusInfos);
                return paymentStatusInfoBuilder.build();
            });
        } else {
            return cacheProcessor.getPaymentStatusInfoMap().computeIfPresent(payment.getId(), (key, value) -> {
                PaymentStatusInfo.PaymentStatusInfoBuilder paymentStatusInfoBuilder = PaymentStatusInfo.builder();
                paymentStatusInfoBuilder.requestId(payment.getId());
                paymentStatusInfoBuilder.status(payment.getEntityStatus() == EntityStatus.PENDING_APPROVAL? "SUCCESS" : "FAILED");
                paymentStatusInfoBuilder.info(payment.getEntityStatus() == EntityStatus.PENDING_APPROVAL? "Verification completed. Information correct." : "Verification completed. Information incorrect.");
                paymentStatusInfoBuilder.sseMessages(paymentStatusInfos);
                return paymentStatusInfoBuilder.build();
            });
        }
    }

    private IdentificationVerificationReportV02 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));

        IdentificationVerificationReportV02 identificationVerificationReportV02 = document.getIdVrfctnRpt();

        return identificationVerificationReportV02;
    }

}
