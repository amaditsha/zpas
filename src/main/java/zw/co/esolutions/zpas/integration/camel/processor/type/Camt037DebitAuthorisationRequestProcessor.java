package zw.co.esolutions.zpas.integration.camel.processor.type;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.dto.investigationcase.PaymentInvestigationCaseDTO;
import zw.co.esolutions.zpas.dto.payments.MakePaymentDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.integration.camel.processor.util.IndividualPaymentUtil;
import zw.co.esolutions.zpas.iso.msg.camt037_001_07.*;
import zw.co.esolutions.zpas.iso.msg.camt037_001_07.Document;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.MessageRepository;
import zw.co.esolutions.zpas.repository.PaymentInvestigationCaseRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import zw.co.esolutions.zpas.services.iface.investigationcase.PaymentInvestigationCaseService;
import zw.co.esolutions.zpas.services.iface.organisations.OrganisationService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.xml.bind.*;
import java.io.StringReader;
import java.lang.System;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

@Component
@Slf4j
public class Camt037DebitAuthorisationRequestProcessor implements Processor {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt037_001_07");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired MessageRepository messageRepository;
    @Autowired ClientService clientService;
    @Autowired NotificationService notificationService;
    @Autowired PaymentInvestigationCaseRepository paymentInvestigationCaseRepository;
    @Autowired OrganisationService organisationService;
    @Autowired AccountsService accountsService;
    @Autowired IndividualPaymentUtil individualPaymentUtil;
    @Autowired PaymentInvestigationCaseService paymentInvestigationCaseService;

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Debit Authorisation Request : \n{}", message);

        DebitAuthorisationRequestV07 debitAuthorisationRequestV07 = parseXml(message);
        log.info("Got message with caseId : {}", debitAuthorisationRequestV07.getCase().getId());

        /*Not Necessary
        Message msg = new Message();
        msg.setId(GenerateKey.generateEntityId());
        msg.setDateCreated(OffsetDateTime.now());
        msg.setUuid(UUID.randomUUID().toString());

        String originalMsgId = "";

        if(debitAuthorisationRequestV07.getUndrlyg().getInitn() != null){
            originalMsgId = debitAuthorisationRequestV07.getUndrlyg().getInitn().getOrgnlGrpInf().getOrgnlMsgId();
        }else if(debitAuthorisationRequestV07.getUndrlyg().getIntrBk() != null){
            originalMsgId = debitAuthorisationRequestV07.getUndrlyg().getIntrBk().getOrgnlGrpInf().getOrgnlMsgId();
        }
        msg.setOriginalMsgId(originalMsgId);
        msg.setPayload(message);

        msg = messageRepository.save(msg);*/

        PaymentInvestigationCase investigationCase = null;
        Payment payment = null;
        String clientId = "";
        /*Optional<PaymentInvestigationCase> optionalInvestigationCase = paymentInvestigationCaseRepository
                .findById(debitAuthorisationRequestV07.getCase().getId());*/

        PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseRepository
                .findByIdentification(debitAuthorisationRequestV07.getCase().getId());
        /**
         * Check if investigation case exists.
         */
        //if(optionalInvestigationCase.isPresent()){
        if(paymentInvestigationCase != null){
            /**
             * If it does, get investigation case to get case type to search with
             */
            investigationCase = paymentInvestigationCase;
            //investigationCase = paymentInvestigationCase.get();
            /**
             * Get underlying payment to get the client (creditor) you are to notify.
             */
            payment = getUnderlyingPayment(investigationCase);
            /**
             * Get the clientId in order to notify the client. TODO: clarify on how to get creditor when you have creditor role.
             */
            clientId = getCreditorId(payment);

        }else{
            /**
             * If Investigation Case doesn't exist, create an investigation case and it's corresponding payment
             */
            /**
             * Creating the payment...
             */
            if(debitAuthorisationRequestV07.getUndrlyg().getInitn() != null){
                /**
                 * This was initiated in the same bank
                 */
                String accountNumber = debitAuthorisationRequestV07.getUndrlyg().getInitn()
                        .getOrgnlTxRef().getCdtrAcct().getId().getOthr().getId();
                clientId = getClientId(accountNumber);
                //clientId = getClientId(debitAuthorisationRequestV07.getUndrlyg().getInitn().getOrgnlTxRef().getCdtr().getPty());

                String clientName = debitAuthorisationRequestV07.getUndrlyg().getInitn().getOrgnlTxRef().getCdtr().getPty().getNm();
                String beneficiaryName = debitAuthorisationRequestV07.getUndrlyg().getInitn().getOrgnlTxRef().getDbtr().getPty().getNm();
                BranchAndFinancialInstitutionIdentification6 sourceFI = debitAuthorisationRequestV07.getUndrlyg()
                        .getInitn().getOrgnlTxRef().getCdtrAgt();
                BranchAndFinancialInstitutionIdentification6 destinationFI = debitAuthorisationRequestV07.getUndrlyg()
                        .getInitn().getOrgnlTxRef().getDbtrAgt();
                CashAccount38 sourceAcc = debitAuthorisationRequestV07.getUndrlyg().getInitn().getOrgnlTxRef().getCdtrAcct();
                String destinationAccNumber = debitAuthorisationRequestV07.getUndrlyg().getInitn().getOrgnlTxRef().getDbtrAcct().getId().getOthr().getId();
                double amount = debitAuthorisationRequestV07.getDtl().getAmtToDbt().getValue().doubleValue();
                String valueDate = debitAuthorisationRequestV07.getDtl().getValDtToDbt().toString();

                MakePaymentDTO makePaymentDTO = MakePaymentDTO.builder()
                        .sourceFinancialInstitutionId(getFinancialInstitution(sourceFI).getId())
                        .sourceAccountId(getAccount(sourceAcc).getId())
                        .clientId(clientId)
                        .clientName(clientName)
                        .destinationFinancialInstitutionId(getFinancialInstitution(destinationFI).getId())
                        .destinationAccountNumber(destinationAccNumber)
                        .beneficiaryName(beneficiaryName)
                        .purpose("Payment Cancellation")
                        .amount(amount)
                        .valueDate(valueDate)
                        .paymentType(PaymentTypeCode.CashTransaction.name())
                        .paymentInstrument(PaymentInstrumentCode.CustomerCreditTransfer.name()) //check on this
                        .priority(PriorityCode.Normal.name())
                        .build();

                Optional<IndividualPayment> individualPaymentOptional = individualPaymentUtil.createIndividualPayment(makePaymentDTO);
                if(individualPaymentOptional.isPresent()){
                    payment = individualPaymentOptional.get();
                }

                /**
                 * Creating the investigation case
                 */
                String cancellationReason = CancellationReasonCode.valueOfCodeName(debitAuthorisationRequestV07.getDtl().getCxlRsn().getCd()).name();
                PaymentInvestigationCaseDTO paymentInvestigationCaseDTO = PaymentInvestigationCaseDTO.builder()
                        .paymentId(payment.getId())
                        .cancellationReason(cancellationReason)
                        .caseType(InvestigationCaseType.PaymentCancellation.getCodeName())
                        .assignmentIdentification(debitAuthorisationRequestV07.getAssgnmt().getId())
                        .creationDateTime("")
                        .identification(debitAuthorisationRequestV07.getCase().getId())
                        .underlyingInstructionId("")
                        .build();

                investigationCase = paymentInvestigationCaseService.createPaymentInvestigationCase(paymentInvestigationCaseDTO, clientId);
                log.info("Finished processing debit authorisation request...");

            }else if(debitAuthorisationRequestV07.getUndrlyg().getIntrBk() != null){
                /**
                 * This is an interbank initiation
                 */
                String accountNumber = debitAuthorisationRequestV07.getUndrlyg().getInitn()
                        .getOrgnlTxRef().getCdtrAcct().getId().getOthr().getId();
                clientId = getClientId(accountNumber);
                //clientId = getClientId(debitAuthorisationRequestV07.getUndrlyg().getIntrBk().getOrgnlTxRef().getCdtr().getPty());

                String clientName = debitAuthorisationRequestV07.getUndrlyg().getIntrBk().getOrgnlTxRef().getCdtr().getPty().getNm();
                String beneficiaryName = debitAuthorisationRequestV07.getUndrlyg().getIntrBk().getOrgnlTxRef().getDbtr().getPty().getNm();
                BranchAndFinancialInstitutionIdentification6 sourceFI = debitAuthorisationRequestV07.getUndrlyg()
                        .getIntrBk().getOrgnlTxRef().getCdtrAgt();
                BranchAndFinancialInstitutionIdentification6 destinationFI = debitAuthorisationRequestV07.getUndrlyg()
                        .getIntrBk().getOrgnlTxRef().getDbtrAgt();
                CashAccount38 sourceAcc = debitAuthorisationRequestV07.getUndrlyg().getIntrBk().getOrgnlTxRef().getCdtrAcct();
                String destinationAccNumber = debitAuthorisationRequestV07.getUndrlyg().getIntrBk().getOrgnlTxRef().getDbtrAcct().getId().getOthr().getId();
                double amount = debitAuthorisationRequestV07.getDtl().getAmtToDbt().getValue().doubleValue();
                String valueDate = debitAuthorisationRequestV07.getDtl().getValDtToDbt().toString();

                MakePaymentDTO makePaymentDTO = MakePaymentDTO.builder()
                        .sourceFinancialInstitutionId(getFinancialInstitution(sourceFI).getId())
                        .sourceAccountId(getAccount(sourceAcc).getId())
                        .clientId(clientId)
                        .clientName(clientName)
                        .destinationFinancialInstitutionId(getFinancialInstitution(destinationFI).getId())
                        .destinationAccountNumber(destinationAccNumber)
                        .beneficiaryName(beneficiaryName)
                        .purpose("Payment Cancellation")
                        .amount(amount)
                        .valueDate(valueDate)
                        .paymentType(PaymentTypeCode.CashTransaction.name())
                        .paymentInstrument(PaymentInstrumentCode.CustomerCreditTransfer.name()) //check on this
                        .priority(PriorityCode.Normal.name())
                        .build();

                Optional<IndividualPayment> individualPaymentOptional = individualPaymentUtil.createIndividualPayment(makePaymentDTO);
                if(individualPaymentOptional.isPresent()){
                    payment = individualPaymentOptional.get();
                }

                /**
                 * Creating the investigation case
                 */
                String cancellationReason = CancellationReasonCode.valueOfCodeName(debitAuthorisationRequestV07.getDtl().getCxlRsn().getCd()).name();
                PaymentInvestigationCaseDTO paymentInvestigationCaseDTO = PaymentInvestigationCaseDTO.builder()
                        .paymentId(payment.getId())
                        .cancellationReason(cancellationReason)
                        .caseType(InvestigationCaseType.PaymentCancellation.getCodeName())
                        .assignmentIdentification(debitAuthorisationRequestV07.getAssgnmt().getId())
                        .creationDateTime("")
                        .identification(debitAuthorisationRequestV07.getCase().getId())
                        .underlyingInstructionId("")
                        .build();

                investigationCase = paymentInvestigationCaseService.createPaymentInvestigationCase(paymentInvestigationCaseDTO, clientId);
            }
        }

        Notification notification = new Notification();
        notification.setId(GenerateKey.generateEntityId());
        notification.setEntityStatus(EntityStatus.NONE);
        notification.setMessage("You have an investigation to review. Search for it by case id: " + investigationCase.getIdentification());
        notification.setRead(false);
        notification.setEntityId("");
        notification.setClientId(clientId);
        notification.setGroupId("");

        notificationService.createNotification(notification);
    }

    private Payment getUnderlyingPayment(PaymentInvestigationCase paymentInvestigationCase){
        Payment payment = null;
        final Optional<Payment> optionalPayment = paymentInvestigationCase.getUnderlyingPayment().stream().findFirst();
        if(optionalPayment.isPresent()){
            payment = optionalPayment.get();
        }

        return payment;
    }

    private String getCreditorId(Payment payment){
        String creditorId = "";
        //RolePlayer creditor = null;
        final Optional<PaymentPartyRole> paymentPartyRoleOptional = payment.getPartyRole().stream()
                .filter(paymentPartyRole -> paymentPartyRole instanceof CreditorRole)
                .findFirst();

        if(paymentPartyRoleOptional.isPresent()){
            String accountNumber = paymentPartyRoleOptional.get().getCustomParty().getAccountNumber();
            creditorId = getClientId(accountNumber);
        }
        /*final Optional<RolePlayer> rolePlayerOptional = payment.getPartyRole().stream()
                .filter(paymentPartyRole -> paymentPartyRole instanceof CreditorRole)
                .flatMap(paymentPartyRole -> paymentPartyRole.getPlayer().stream()).findFirst();

        if(rolePlayerOptional.isPresent()){
            creditor = rolePlayerOptional.get();
        }*/

        return creditorId;
    }

    private String getClientId(/*PartyIdentification135 partyIdentification135*/ String accountNumber){
        String clientId = "";

        Optional<CashAccount> cashAccountOptional = accountsService
                .findAccountByNumber(accountNumber);

        if(cashAccountOptional.isPresent()){
            CashAccount cashAccount = cashAccountOptional.get();
            final Optional<RolePlayer> accountOwnerOptional = cashAccount.getPartyRole().stream().filter(partyRole -> partyRole instanceof AccountOwnerRole)
                    .flatMap(accountPartyRole -> accountPartyRole.getPlayer().stream())
                    .findFirst();

            if(accountOwnerOptional.isPresent()){
                clientId = accountOwnerOptional.get().getId();
            }
        }

        /*if(partyIdentification135.getId().getOrgId() != null){
            String anyBIC = partyIdentification135.getId().getOrgId().getAnyBIC();
            Optional<OrganisationIdentification> organisationIdentificationOptional = organisationService
                    .getOrganisationIdentificationByAnyBIC(anyBIC);
            if(organisationIdentificationOptional.isPresent()){
                Organisation organisation = organisationIdentificationOptional.get().getOrganisation();
                clientId = organisation.getId();
            }
        }else {
            Optional<GenericPersonIdentification1> optionalGenId1 = partyIdentification135.getId().getPrvtId()
                    .getOthr().stream().findFirst();
            if(optionalGenId1.isPresent()){
                String idNumber = optionalGenId1.get().getId();
                Optional<PersonIdentification> personIdentificationOptional = clientService.findPersonIdentificationByIdNumber(idNumber);
                if(personIdentificationOptional.isPresent()){
                    Person person = personIdentificationOptional.get().getPerson();
                    clientId = person.getId();
                }
            }
        }*/
        return clientId;
    }

    private Organisation getFinancialInstitution(BranchAndFinancialInstitutionIdentification6 financialInstitutionIdentification6){
        Organisation organisation = null;
        FinancialInstitutionIdentification18 financialInstitutionIdentification18 = financialInstitutionIdentification6.getFinInstnId();
        Optional<OrganisationIdentification> organisationIdentificationOptional = organisationService
                .getOrganisationIdentificationByBICFI(financialInstitutionIdentification18.getBICFI());
        if(organisationIdentificationOptional.isPresent()){
            organisation = organisationIdentificationOptional.get().getOrganisation();
        }
        return organisation;
    }

    private CashAccount getAccount(CashAccount38 cashAccount38){
        CashAccount cashAccount = null;
        //Use Othr instead of IBAN
        //String iBAN = cashAccount38.getId().getIBAN();
        String accNumber = cashAccount38.getId().getOthr().getId();
        Optional<CashAccount> cashAccountOptional = accountsService.findAccountByNumber(accNumber);
        if(cashAccountOptional.isPresent()){
            cashAccount = cashAccountOptional.get();
        }
        return cashAccount;
    }

    private DebitAuthorisationRequestV07 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
        System.out.println(document);

        DebitAuthorisationRequestV07 debitAuthorisationRequestV07 = document.getDbtAuthstnReq();

        return debitAuthorisationRequestV07;
    }
}
