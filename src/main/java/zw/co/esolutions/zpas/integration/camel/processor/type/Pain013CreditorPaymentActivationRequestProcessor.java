package zw.co.esolutions.zpas.integration.camel.processor.type;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.codeset.CountryCode;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.dto.message.MessageResponseDTO;
import zw.co.esolutions.zpas.dto.message.NotificationRequestDTO;
import zw.co.esolutions.zpas.enums.*;
import zw.co.esolutions.zpas.enums.proprietarycodes.ProprietaryPurposeCode;
import zw.co.esolutions.zpas.integration.camel.processor.builder.CreditorPaymentActivationRequestStatusReportMessageBuilder;
import zw.co.esolutions.zpas.integration.camel.producer.MessageProducer;
import zw.co.esolutions.zpas.iso.msg.pain013_001_07.Document;
import zw.co.esolutions.zpas.iso.msg.pain013_001_07.*;
import zw.co.esolutions.zpas.iso.msg.pain014_001_07.CreditorPaymentActivationRequestStatusReportV07;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationConfigService;
import zw.co.esolutions.zpas.services.iface.party.PartyService;
import zw.co.esolutions.zpas.services.iface.payments.PaymentObligationsService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.GroupStatus;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import javax.xml.bind.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by alfred on 24 Apr 2019
 */
@Slf4j
@Component
public class Pain013CreditorPaymentActivationRequestProcessor implements Processor {

    @Autowired
    private MessageProducer messageProducer;

    @Autowired
    private ClientService clientService;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private PartyService partyService;

    @Autowired
    private AccountsService accountsService;

    @Autowired
    private PaymentObligationsService paymentObligationsService;

    @Autowired
    private NotificationConfigService notificationConfigService;

    @Autowired
    private CreditorPaymentActivationRequestStatusReportMessageBuilder creditorPaymentActivationRequestStatusReportMessageBuilder;

    static JAXBContext jaxbContextForPain013;
    static JAXBContext jaxbContextForPain014;

    static {
        try {
            jaxbContextForPain013 = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.pain013_001_07");
            jaxbContextForPain014 = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.pain014_001_07");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Creditor Payment Activation Request Report: \n{}", message);

        final CreditorPaymentActivationRequestV07 creditorPaymentActivationRequestV07 = parseXml(message);
        final GroupHeader78 grpHdr = creditorPaymentActivationRequestV07.getGrpHdr();
        final List<PaymentInstruction31> paymentInstruction31s = creditorPaymentActivationRequestV07.getPmtInf();

        BigDecimal ctrlSum = grpHdr.getCtrlSum();
        XMLGregorianCalendar creDtTm;
        PartyIdentification135 initgPty;
        String msgId = grpHdr.getMsgId();
        String nbOfTxs = grpHdr.getNbOfTxs();

        try {
            Preconditions.checkNotNull(grpHdr);
            Preconditions.checkNotNull(paymentInstruction31s);

            ctrlSum = grpHdr.getCtrlSum();
            creDtTm = grpHdr.getCreDtTm();
            initgPty = grpHdr.getInitgPty();
            nbOfTxs = grpHdr.getNbOfTxs();

            Preconditions.checkNotNull(initgPty, "Initiating party is missing.");
            Preconditions.checkNotNull(msgId, "Message ID is missing.");

            List<PaymentObligation> paymentObligations = new ArrayList<>();

            for (PaymentInstruction31 paymentInstruction31 : paymentInstruction31s) {
                final List<CreditTransferTransaction35> cdtTrfTxs = paymentInstruction31.getCdtTrfTx();
                final ChargeBearerType1Code chrgBr = paymentInstruction31.getChrgBr();
                final PartyIdentification135 dbtr = paymentInstruction31.getDbtr();
                final CashAccount38 dbtrAcct = paymentInstruction31.getDbtrAcct();
                final BranchAndFinancialInstitutionIdentification6 dbtrAgt = paymentInstruction31.getDbtrAgt();
                final PaymentCondition1 pmtCond = paymentInstruction31.getPmtCond();
                final String pmtInfId = paymentInstruction31.getPmtInfId();
                final PaymentMethod7Code pmtMtd = paymentInstruction31.getPmtMtd();
                final PaymentTypeInformation26 pmtTpInf = paymentInstruction31.getPmtTpInf();
                final DateAndDateTime2Choice reqdExctnDt = paymentInstruction31.getReqdExctnDt();
                final PartyIdentification135 ultmtDbtr = paymentInstruction31.getUltmtDbtr();
                final DateAndDateTime2Choice xpryDt = paymentInstruction31.getXpryDt();

                Preconditions.checkNotNull(pmtInfId, "Payment info ID is missing");
                Preconditions.checkNotNull(cdtTrfTxs, "Credit Transfer transaction is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(dbtr, "Debtor is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(dbtrAcct, "Debtor Account is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(dbtrAgt, "Debtor Agent is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(pmtCond, "Payment Condition is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(pmtMtd, "Payment method is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(pmtTpInf, "Payment Type Information is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(reqdExctnDt, "Requested Execution date is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(ultmtDbtr, "Ultimate Debtor is missing for Payment info ID \"" + pmtInfId + "\"");
                Preconditions.checkNotNull(xpryDt, "Expiry Date is missing for Payment info ID \"" + pmtInfId + "\"");

                PaymentObligation paymentObligation = new PaymentObligation();
                paymentObligation.setId(RefGen.getReference("P"));
                paymentObligation.setDateCreated(OffsetDateTime.now());
                paymentObligation.setLastUpdated(OffsetDateTime.now());
                paymentObligation.setEntityStatus(EntityStatus.ACTIVE);
                paymentObligation.setEntityVersion(0L);
                paymentObligation.setPaymentOffset(Lists.newArrayList());
                paymentObligation.setRemittanceDeliveryMethod(RemittanceLocationMethodCode.UniformResourceIdentifier);
                paymentObligation.setRemittanceLocation(Lists.newArrayList());
                paymentObligation.setAssociatedDocument(Lists.newArrayList());
                paymentObligation.setPercentage(new BigDecimal("0"));
                paymentObligation.setExpiryDate(XmlDateUtil.getOffsetDateTime(xpryDt.getDtTm()));
                paymentObligation.setApplicableLaw(new CountryCode());
                paymentObligation.setOriginalObligationId(msgId);

                final List<PaymentObligationPartyRole> paymentObligationPartyRoles = new ArrayList<>();
                Optional<RolePlayer> debtorRolePlayerOptional = Optional.empty();
                Optional<RolePlayer> creditorRolePlayerOptional = Optional.empty();

                UltimateDebtorRole ultimateDebtorRole = new UltimateDebtorRole();
                ultimateDebtorRole.setId(GenerateKey.generateEntityId());
                ultimateDebtorRole.setEntityStatus(EntityStatus.ACTIVE);
                ultimateDebtorRole.setDateCreated(OffsetDateTime.now());
                ultimateDebtorRole.setLastUpdated(OffsetDateTime.now());
                ultimateDebtorRole.setPaymentObligation(Arrays.asList(paymentObligation));
                ultimateDebtorRole.setPlayer(new ArrayList<>());
                ultimateDebtorRole.setHasCustomParty(false);

                Preconditions.checkNotNull(ultmtDbtr.getId(), "Id for Ultimate Debtor is missing");
                if (ultmtDbtr.getId().getPrvtId() != null) {
                    log.info("Private ID present, fetching person.");
                    debtorRolePlayerOptional = setAndGetPersonRolePlayer(ultmtDbtr.getId().getPrvtId().getOthr(), ultimateDebtorRole);
                } else if (ultmtDbtr.getId().getOrgId() != null) {
                    log.info("Organisation ID present, fetching organisation.");
                    debtorRolePlayerOptional = setAndGetOrganisationRolePlayer(ultmtDbtr.getId().getOrgId().getOthr(), ultimateDebtorRole);
                } else {
                    throw new RuntimeException("Failed to get Ultimate Creditor from the message");
                }

                UltimateCreditorRole ultimateCreditorRole = new UltimateCreditorRole();
                ultimateCreditorRole.setId(GenerateKey.generateEntityId());
                ultimateCreditorRole.setEntityStatus(EntityStatus.ACTIVE);
                ultimateCreditorRole.setDateCreated(OffsetDateTime.now());
                ultimateCreditorRole.setLastUpdated(OffsetDateTime.now());
                ultimateCreditorRole.setPaymentObligation(Arrays.asList(paymentObligation));
                ultimateCreditorRole.setPlayer(new ArrayList());
                ultimateCreditorRole.setHasCustomParty(false);

                Preconditions.checkNotNull(initgPty.getId(), "Id for Ultimate Creditor is missing");
                if (initgPty.getId().getPrvtId() != null) {
                    log.info("Private ID for Ultimate Creditor present, fetching person.");
                    creditorRolePlayerOptional = setAndGetPersonRolePlayer(initgPty.getId().getPrvtId().getOthr(), ultimateCreditorRole);
                } else if (initgPty.getId().getOrgId() != null) {
                    log.info("Organisation ID for Ultimate Creditor present, fetching person.");
                    creditorRolePlayerOptional = setAndGetOrganisationRolePlayer(initgPty.getId().getOrgId().getOthr(), ultimateCreditorRole);
                } else {
                    throw new RuntimeException("Failed to get the Ultimate Creditor from the message");
                }

                paymentObligationPartyRoles.add(ultimateDebtorRole);
                paymentObligationPartyRoles.add(ultimateCreditorRole);
                paymentObligation.setPartyRole(paymentObligationPartyRoles);
                paymentObligation.setPaymentDueDate(XmlDateUtil.getOffsetDateTime(xpryDt.getDtTm()));

                final List<Payment> creditTransfers = new ArrayList<>();

                for (CreditTransferTransaction35 cdtTrfTx35 : cdtTrfTxs) {
                    final AmountType4Choice amt = cdtTrfTx35.getAmt();
                    final PartyIdentification135 cdtr = cdtTrfTx35.getCdtr();
                    final CashAccount38 cdtrAcct = cdtTrfTx35.getCdtrAcct();
                    final BranchAndFinancialInstitutionIdentification6 cdtrAgt = cdtTrfTx35.getCdtrAgt();
                    final Cheque11 chqInstr = cdtTrfTx35.getChqInstr();
                    final ChargeBearerType1Code cdtTrfTx35ChrgBr = cdtTrfTx35.getChrgBr();
                    final List<InstructionForCreditorAgent1> instrForCdtrAgt = cdtTrfTx35.getInstrForCdtrAgt();
                    final BranchAndFinancialInstitutionIdentification6 intrmyAgt1 = cdtTrfTx35.getIntrmyAgt1();
                    final BranchAndFinancialInstitutionIdentification6 intrmyAgt2 = cdtTrfTx35.getIntrmyAgt2();
                    final BranchAndFinancialInstitutionIdentification6 intrmyAgt3 = cdtTrfTx35.getIntrmyAgt3();
                    final List<Document12> nclsdFile = cdtTrfTx35.getNclsdFile();
                    final PaymentCondition1 cdtTrfTx35PmtCond = cdtTrfTx35.getPmtCond();
                    final PaymentIdentification6 pmtId = cdtTrfTx35.getPmtId();
                    final PaymentTypeInformation26 cdtTrfTx35PmtTpInf = cdtTrfTx35.getPmtTpInf();
                    final Purpose2Choice purp = cdtTrfTx35.getPurp();
                    final List<RegulatoryReporting3> rgltryRptg = cdtTrfTx35.getRgltryRptg();
                    final List<RemittanceLocation7> rltdRmtInf = cdtTrfTx35.getRltdRmtInf();
                    final RemittanceInformation16 rmtInf = cdtTrfTx35.getRmtInf();
                    final List<SupplementaryData1> splmtryData = cdtTrfTx35.getSplmtryData();
                    final TaxInformation8 tax = cdtTrfTx35.getTax();
                    final PartyIdentification135 ultmtCdtr = cdtTrfTx35.getUltmtCdtr();
                    final PartyIdentification135 cdtTrfTx35UltmtDbtr = cdtTrfTx35.getUltmtDbtr();

                    final CreditTransfer creditTransfer = new CreditTransfer();
                    creditTransfer.setId(GenerateKey.generateEntityId());
                    creditTransfer.setDateCreated(OffsetDateTime.now());
                    creditTransfer.setLastUpdated(OffsetDateTime.now());
                    creditTransfer.setEntityStatus(EntityStatus.PENDING_APPROVAL);
                    creditTransfer.setPaymentObligation(new ArrayList<>());

                    creditTransfer.setCreditMethod(Lists.newArrayList());
                    creditTransfer.setType(PaymentTypeCode.valueOfCodeName(cdtTrfTx35PmtTpInf.getCtgyPurp().getCd()));
                    creditTransfer.setPriority(PriorityCode.Normal);
                    creditTransfer.setValueDate(XmlDateUtil.getOffsetDateTime(creDtTm));
                    List<PaymentStatus> paymentStatuses = new ArrayList<>();
                    final PaymentStatus paymentStatus = new PaymentStatus();
                    paymentStatus.setStatus(PaymentStatusCode.Pending);
                    paymentStatus.setPayment(creditTransfer);
                    paymentStatus.setUnmatchedStatusReason(UnmatchedStatusReasonCode.None);
                    paymentStatus.setSuspendedStatusReason(SuspendedStatusReasonCode.None);
                    paymentStatus.setPendingSettlement(PendingSettlementStatusReasonCode.None);
                    paymentStatus.setInstructionStatus(PaymentInstructionStatusCode.None);
                    paymentStatus.setTransactionRejectionReason(TransactionReasonCode.NotSpecifiedReason);
                    paymentStatus.setNotificationStatus(NotificationToReceiveStatusCode.None);
                    paymentStatus.setTransactionStatus(TransactionStatusCode.None);
                    paymentStatus.setId(GenerateKey.generateEntityId());
                    paymentStatus.setEntityStatus(EntityStatus.ACTIVE);
                    paymentStatus.setDateCreated(OffsetDateTime.now());
                    paymentStatus.setLastUpdated(OffsetDateTime.now());
                    paymentStatus.setStatusDateTime(OffsetDateTime.now());
                    paymentStatus.setStatusDescription("Initialising Payment Activation Request");
                    paymentStatus.setInstructionProcessingStatus(StatusCode.Pending);

                    paymentStatuses.add(paymentStatus);
                    creditTransfer.setPaymentStatus(paymentStatuses);
                    creditTransfer.setInstructionForCreditorAgent(InstructionCode.PhoneNextAgent);
                    creditTransfer.setInstructionForDebtorAgent(InstructionCode.PayTheBeneficiary);
                    creditTransfer.setPartyRole(new ArrayList<>());
                    creditTransfer.setPaymentRelatedIdentifications(new ArrayList<>());


                    final CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();
                    final BigDecimal instructedAmountBigDecimal = Optional.ofNullable(amt.getInstdAmt())
                            .map(ActiveOrHistoricCurrencyAndAmount::getValue)
                            .orElse(new BigDecimal(0));
                    final String instructedAmountCcy = Optional.ofNullable(amt.getInstdAmt())
                            .map(ActiveOrHistoricCurrencyAndAmount::getCcy)
                            .orElse("");

                    currencyAndAmount.setAmount(instructedAmountBigDecimal);
                    final CurrencyCode currencyCode = new CurrencyCode();
                    currencyCode.setCodeName(instructedAmountCcy);
                    currencyCode.setName(instructedAmountCcy);
                    currencyAndAmount.setCurrency(currencyCode);
                    creditTransfer.setInstructedAmount(currencyAndAmount);
                    creditTransfer.setAmount(currencyAndAmount);
                    creditTransfer.setCurrencyOfTransfer(currencyCode);
                    creditTransfer.setEquivalentAmount(instructedAmountBigDecimal);

                    creditTransfer.setStandardSettlementInstructions("");
                    creditTransfer.setPaymentInstrument(PaymentInstrumentCode.CustomerCreditTransfer);

                    accountsService.findAccountByIban(cdtrAcct.getId().getOthr().getId()).ifPresent(cashAccount -> {
                        creditTransfer.setAccount(cashAccount);
                    });
                    creditTransfer.setPurpose(ProprietaryPurposeCode.valueOfCodeName(purp.getCd()));
                    creditTransfer.getPaymentObligation().add(paymentObligation);

                    final String fiId = cdtTrfTx35.getCdtrAgt().getFinInstnId().getBICFI();
                    final Optional<FinancialInstitution> financialInstitutionOptional = financialInstitutionService.getFinancialInstitutionByAnyBIC(fiId);
                    final Optional<CashAccount> cashAccountOptional = accountsService.findAccountByIban(dbtrAcct.getId().getOthr().getId());
                    addDebtorPaymentRoles(creditTransfer, cashAccountOptional.orElse(null));
                    addCreditorPaymentPartyRoles(financialInstitutionOptional, creditorRolePlayerOptional.orElse(null), creditTransfer);

                    creditTransfers.add(creditTransfer);
                }

                final CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();

                currencyAndAmount.setAmount(creditTransfers.stream()
                        .map(c -> c.getAmount().getAmount()).reduce(BigDecimal::add).orElse(new BigDecimal(0)));
                final CurrencyCode currencyCode = new CurrencyCode();
                currencyCode.setCodeName("");
                currencyCode.setName("");
                currencyAndAmount.setCurrency(currencyCode);
                paymentObligation.setPaymentOffset(creditTransfers);
                paymentObligation.setAmount(currencyAndAmount);
                paymentObligation.setMaximumAmount(currencyAndAmount);
                paymentObligations.add(paymentObligation);
            }

            if (paymentObligations.size() == 1) {
                final Optional<PaymentObligation> optionalPaymentObligation = paymentObligationsService.getPaymentObligationsById(msgId);
                optionalPaymentObligation.ifPresent(paymentObligation -> {
                    log.info("Obligation already exists. Debtor/Creditor are in the same Acquiring System.");
                    sendMailToDebtors(Arrays.asList(msgId));
                });
            } else {
                paymentObligationsService.saveObligations(paymentObligations);
                sendMailToDebtors(paymentObligations.stream().map(PaymentObligation::getId).collect(Collectors.toList()));
            }

            for (PaymentObligation paymentObligation : paymentObligations) {
                buildAndSendCreditorPaymentActivationRequestStatusReportV07(grpHdr, GroupStatus.SUCCESS, paymentObligation);
            }
        } catch (RuntimeException re) {
            //send the message to System Client
            log.error("An error occurred. {}", re.getMessage());
            re.printStackTrace();
            PaymentObligation paymentObligation = new PaymentObligation();
            paymentObligation.setOriginalObligationId(msgId);
            buildAndSendCreditorPaymentActivationRequestStatusReportV07(grpHdr, GroupStatus.FAIL, paymentObligation);
            return;
        }

    }

    private void sendMailToDebtors(List<String> paymentObligationsIds) {
        log.info("Sending email to {} debtors.", paymentObligationsIds.size());
        log.info("Looking for IDs: {}", paymentObligationsIds.stream().collect(Collectors.joining()));
        paymentObligationsService.getPaymentObligationsForIds(paymentObligationsIds).forEach(paymentObligation -> {
            log.info("Working with obligation reference {}", paymentObligation.getId());
            log.info("Payment Obligation Roles: {}", paymentObligation.getPartyRole().stream().map(pr -> pr.toString()).reduce("", (s1, s2) -> s1 + s2));
            log.info("{} Payment Obligation Party Roles found", paymentObligation.getPartyRole().size());
            paymentObligation.getPartyRole().stream().filter(role -> role instanceof UltimateDebtorRole)
                    .map(role -> (UltimateDebtorRole) role)
                    .forEach(ultimateDebtorRole -> {
                        ultimateDebtorRole.getPlayer().forEach(rolePlayer -> {
                            Party party = (Party) rolePlayer;
                            sendMailHelper(party, paymentObligation);
                        });
                    });
        });
    }

    private void sendMailHelper(Party party, PaymentObligation paymentObligation) {
        final String partyName = party.getName();
        final String partyEmail = getPartyEmail(party);
        String messageText = "You have a pending obligation that needs your attention. Please login to respond. " +
                "The obligation is due on " + paymentObligation.getPaymentDueDate().format(DateTimeFormatter.ofPattern("dd MMMM yyyy"));

        log.info("Sending message:\n{}\n to {}", messageText, partyEmail);

        final MessageResponseDTO messageResponseDTO = notificationConfigService.processAndSendNotifications(
                NotificationRequestDTO.builder()
                        .attachmentFilePaths(new ArrayList<>())
                        .clientId(party.getId())
                        .destination(partyEmail)
                        .messageChannel(MessageChannel.EMAIL)
                        .messageDate(OffsetDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
                        .messageReference(RefGen.getReference("N"))
                        .messageText(messageText)
                        .notificationType(NotificationType.NONE)
                        .originator("")
                        .recipientName(partyName)
                        .subject("Status Report for Creditor Payment Activation Request")
                        .build());
        log.info("Creditor Payment Activation Request notification to client {}, {}.", partyName, partyEmail);
    }

    private String getPartyEmail(Party party) {
        return party.getContactPoint().stream().filter(c -> c instanceof ElectronicAddress)
                .map(c -> (ElectronicAddress) c)
                .findFirst().map(ElectronicAddress::getEmailAddress)
                .orElse("");
    }

    private void buildAndSendCreditorPaymentActivationRequestStatusReportV07(GroupHeader78 grpHdr, GroupStatus groupStatus, PaymentObligation paymentObligation) throws JAXBException {
        final CreditorPaymentActivationRequestStatusReportV07 creditorPaymentActivationRequestStatusReportV07 =
                creditorPaymentActivationRequestStatusReportMessageBuilder
                        .buildCreditorPaymentActivationRequestStatusReportMessage(grpHdr, groupStatus, paymentObligation);
        messageProducer.sendMessageToSystemClient(buildXml(creditorPaymentActivationRequestStatusReportV07));
    }

    private Optional<RolePlayer> setAndGetOrganisationRolePlayer(List<GenericOrganisationIdentification1> othr, PaymentObligationPartyRole paymentObligationPartyRole) {
        return Optional.ofNullable(othr).map(genericOrganisationIdentification1s -> {
            return genericOrganisationIdentification1s.stream().findFirst().map(genericOrganisationIdentification1 -> {
                final String organisationId = genericOrganisationIdentification1.getId();
                return partyService.getPartyById(organisationId).map(party -> {
                    paymentObligationPartyRole.getPlayer().add(party);
                    return Optional.ofNullable((RolePlayer) party);
                }).orElse(Optional.empty());
            }).orElse(Optional.empty());
        }).orElse(Optional.empty());
    }

    private Optional<RolePlayer> setAndGetPersonRolePlayer(List<GenericPersonIdentification1> othr, PaymentObligationPartyRole paymentObligationPartyRole) {
        return Optional.ofNullable(othr).map(genericPersonIdentification1s -> {
            return genericPersonIdentification1s.stream().findFirst().map(genericPersonIdentification1 -> {
                final String personId = genericPersonIdentification1.getId();
                return clientService.findPersonIdentificationByIdNumber(personId).map(personIdentification -> {
                    final Person person = personIdentification.getPerson();
                    paymentObligationPartyRole.getPlayer().add(person);
                    return Optional.ofNullable((RolePlayer) person);
                }).orElse(Optional.empty());
            }).orElse(Optional.empty());
        }).orElse(Optional.empty());
    }

    private CreditorPaymentActivationRequestV07 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContextForPain013.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));

        final CreditorPaymentActivationRequestV07 cdtrPmtActvtnReq = document.getCdtrPmtActvtnReq();

        return cdtrPmtActvtnReq;
    }

    public static void main(String[] args) throws JAXBException {
        Document document = new Document();
        document.setCdtrPmtActvtnReq(getCreditorPaymentActivationRequestV07());

        Marshaller jaxbMarshaller = jaxbContextForPain013.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);
    }

    private static CreditorPaymentActivationRequestV07 getCreditorPaymentActivationRequestV07() {
        final CreditorPaymentActivationRequestV07 creditorPaymentActivationRequestV07 = new CreditorPaymentActivationRequestV07();
        final GroupHeader78 groupHeader78 = new GroupHeader78();
        groupHeader78.setMsgId(GenerateKey.generateReferenceNumber());
        groupHeader78.setCreDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));
        groupHeader78.setNbOfTxs("2");
        groupHeader78.setCtrlSum(new BigDecimal("1000"));
        final PartyIdentification135 partyIdentification135 = new PartyIdentification135();
        partyIdentification135.setNm("Alfred Samanga");
        final PostalAddress24 postalAddress24 = new PostalAddress24();
        postalAddress24.setAdrTp(new AddressType3Choice());
        postalAddress24.setDept("SW Engineering");
        postalAddress24.setSubDept("Kubernetes");
        postalAddress24.setStrtNm("55");
        postalAddress24.setBldgNb("55");
        postalAddress24.setBldgNm("eSolutions");
        postalAddress24.setFlr("1");
        postalAddress24.setPstBx("55");
        postalAddress24.setRoom("3");
        postalAddress24.setPstCd("263");
        postalAddress24.setTwnNm("Harare");
        postalAddress24.setTwnLctnNm("Harare");
        postalAddress24.setDstrctNm("Harare");
        postalAddress24.setCtrySubDvsn("Mashonaland");
        postalAddress24.setCtry("Zimbabwe");

        partyIdentification135.setPstlAdr(postalAddress24);
        final Party38Choice party38Choice = new Party38Choice();
        final PersonIdentification13 personIdentification13 = new PersonIdentification13();
        final DateAndPlaceOfBirth1 dateAndPlaceOfBirth1 = new DateAndPlaceOfBirth1();
        dateAndPlaceOfBirth1.setBirthDt(XmlDateUtil.getXmlDate(OffsetDateTime.of(LocalDate.of(1994, Month.MAY, 17), LocalTime.NOON, ZoneOffset.UTC)));
        dateAndPlaceOfBirth1.setPrvcOfBirth("Manicaland");
        dateAndPlaceOfBirth1.setCityOfBirth("Mutare");
        dateAndPlaceOfBirth1.setCtryOfBirth("Zimbabwe");
        final GenericPersonIdentification1 genericPersonIdentification1 = new GenericPersonIdentification1();
        genericPersonIdentification1.setId("78-97979777797W90");
        final PersonIdentificationSchemeName1Choice personIdentificationSchemeName1Choice = new PersonIdentificationSchemeName1Choice();
        personIdentificationSchemeName1Choice.setCd("NT");
        personIdentificationSchemeName1Choice.setPrtry("National-ID");

        genericPersonIdentification1.setSchmeNm(personIdentificationSchemeName1Choice);
        genericPersonIdentification1.setIssr("Zimbabwe Govt");

        personIdentification13.getOthr().add(genericPersonIdentification1);

        personIdentification13.setDtAndPlcOfBirth(dateAndPlaceOfBirth1);

        party38Choice.setPrvtId(personIdentification13);

        partyIdentification135.setId(party38Choice);
        partyIdentification135.setCtryOfRes("Zimbabwe");
        final Contact4 contact4 = new Contact4();
        contact4.setNmPrfx(NamePrefix2Code.MISS);
        contact4.setNm("Petronella");
        contact4.setPhneNb("263775775775");
        contact4.setMobNb("263775775775");
        contact4.setFaxNb("263775775775");
        contact4.setEmailAdr("petnella.m@gmail.com");
        contact4.setEmailPurp("Notifications");
        contact4.setJobTitl("Software Engineer");
        contact4.setRspnsblty("SWE");
        contact4.setDept("Software Engineering");
        contact4.setPrefrdMtd(PreferredContactMethod1Code.MAIL);

        partyIdentification135.setCtctDtls(contact4);

        groupHeader78.setInitgPty(partyIdentification135);

        creditorPaymentActivationRequestV07.setGrpHdr(groupHeader78);

        final PaymentInstruction31 paymentInstruction31 = new PaymentInstruction31();
        paymentInstruction31.setPmtInfId(GenerateKey.generateEntityId());
        paymentInstruction31.setPmtMtd(PaymentMethod7Code.TRF);
        final PaymentTypeInformation26 paymentTypeInformation26 = new PaymentTypeInformation26();
        paymentTypeInformation26.setInstrPrty(Priority2Code.NORM);
        final LocalInstrument2Choice localInstrument2Choice = new LocalInstrument2Choice();
        localInstrument2Choice.setCd("CSH");
        localInstrument2Choice.setPrtry("CSH");

        paymentTypeInformation26.setLclInstrm(localInstrument2Choice);
        final CategoryPurpose1Choice categoryPurpose1Choice = new CategoryPurpose1Choice();
        categoryPurpose1Choice.setCd("CSH");
        categoryPurpose1Choice.setPrtry("CSH");

        paymentTypeInformation26.setCtgyPurp(categoryPurpose1Choice);

        paymentInstruction31.setPmtTpInf(paymentTypeInformation26);
        final DateAndDateTime2Choice reqExctnDate = new DateAndDateTime2Choice();
        reqExctnDate.setDt(XmlDateUtil.getXmlDate(OffsetDateTime.now().plusDays(10)));
        reqExctnDate.setDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        paymentInstruction31.setReqdExctnDt(reqExctnDate);
        final DateAndDateTime2Choice expiryDate = new DateAndDateTime2Choice();
        expiryDate.setDt(XmlDateUtil.getXmlDate(OffsetDateTime.now().plusDays(10)));
        expiryDate.setDtTm(XmlDateUtil.getXmlDate(OffsetDateTime.now()));

        paymentInstruction31.setXpryDt(expiryDate);
        final PaymentCondition1 paymentCondition1 = new PaymentCondition1();
        paymentCondition1.setAmtModAllwd(false);
        paymentCondition1.setEarlyPmtAllwd(false);
        paymentCondition1.setDelyPnlty("20");
        final AmountOrRate1Choice amountOrRate1Choice = new AmountOrRate1Choice();
        final ActiveCurrencyAndAmount activeCurrencyAndAmount = new ActiveCurrencyAndAmount();
        activeCurrencyAndAmount.setValue(new BigDecimal("10"));
        activeCurrencyAndAmount.setCcy("ZWL");

        amountOrRate1Choice.setAmt(activeCurrencyAndAmount);
        amountOrRate1Choice.setRate(new BigDecimal("0"));

        paymentCondition1.setImdtPmtRbt(amountOrRate1Choice);
        paymentCondition1.setGrntedPmtReqd(false);

        paymentInstruction31.setPmtCond(paymentCondition1);
        final PartyIdentification135 debtor = new PartyIdentification135();
        debtor.setNm("Providence");
        final PostalAddress24 postalAddress241 = new PostalAddress24();
        postalAddress241.setAdrTp(new AddressType3Choice());
        postalAddress241.setDept("SW");
        postalAddress241.setSubDept("SW");
        postalAddress241.setStrtNm("Rossal");
        postalAddress241.setBldgNb("55");
        postalAddress241.setBldgNm("eSolutions");
        postalAddress241.setFlr("1");
        postalAddress241.setPstBx("55");
        postalAddress241.setRoom("3");
        postalAddress241.setPstCd("263");
        postalAddress241.setTwnNm("Harare");
        postalAddress241.setTwnLctnNm("Harare");
        postalAddress241.setDstrctNm("Harare");
        postalAddress241.setCtrySubDvsn("Mashonaland");
        postalAddress241.setCtry("Zimbabwe");

        debtor.setPstlAdr(postalAddress241);
        final Party38Choice party38Choice1 = new Party38Choice();
        final PersonIdentification13 personIdentification131 = new PersonIdentification13();
        final DateAndPlaceOfBirth1 dateAndPlaceOfBirth11 = new DateAndPlaceOfBirth1();
        dateAndPlaceOfBirth11.setBirthDt(XmlDateUtil.getXmlDate(OffsetDateTime.now().minusYears(35)));
        dateAndPlaceOfBirth11.setPrvcOfBirth("Matabeleland");
        dateAndPlaceOfBirth11.setCityOfBirth("Bulawayo");
        dateAndPlaceOfBirth11.setCtryOfBirth("Zimbabwe");

        personIdentification131.setDtAndPlcOfBirth(dateAndPlaceOfBirth11);
        final GenericPersonIdentification1 genericPersonIdentification11 = new GenericPersonIdentification1();
        genericPersonIdentification11.setId("75-79879879W75");
        genericPersonIdentification11.setIssr("ZW-Govt");

        personIdentification131.getOthr().add(genericPersonIdentification11);

        party38Choice1.setPrvtId(personIdentification131);

        debtor.setId(party38Choice1);
        debtor.setCtryOfRes("Zimbabwe");

        paymentInstruction31.setDbtr(debtor);
        final CashAccount38 debtorAccount = new CashAccount38();
        final AccountIdentification4Choice accountIdentification4Choice = new AccountIdentification4Choice();
        accountIdentification4Choice.setIBAN("987654321");
        final GenericAccountIdentification1 genericAccountIdentification1 = new GenericAccountIdentification1();
        genericAccountIdentification1.setId("987654321");
        genericAccountIdentification1.setIssr("BancABC");

        accountIdentification4Choice.setOthr(genericAccountIdentification1);

        debtorAccount.setId(accountIdentification4Choice);
        final CashAccountType2Choice cashAccountType2Choice = new CashAccountType2Choice();
        cashAccountType2Choice.setCd("CD");
        cashAccountType2Choice.setPrtry("CD");

        debtorAccount.setTp(cashAccountType2Choice);
        debtorAccount.setCcy("ZWL");
        debtorAccount.setNm("Providence Ncube");

        paymentInstruction31.setDbtrAcct(debtorAccount);
        final BranchAndFinancialInstitutionIdentification6 debtorAgent = new BranchAndFinancialInstitutionIdentification6();
        final FinancialInstitutionIdentification18 financialInstitutionIdentification18 = new FinancialInstitutionIdentification18();
        financialInstitutionIdentification18.setBICFI("FS931553862937966122");
        financialInstitutionIdentification18.setLEI("FS931553862937966122");
        financialInstitutionIdentification18.setNm("BancABC");
        financialInstitutionIdentification18.setPstlAdr(new PostalAddress24());
        final GenericFinancialIdentification1 genericFinancialIdentification1 = new GenericFinancialIdentification1();
        genericFinancialIdentification1.setId("FS931553862937966122");
        genericFinancialIdentification1.setIssr("RBZ");

        financialInstitutionIdentification18.setOthr(genericFinancialIdentification1);

        debtorAgent.setFinInstnId(financialInstitutionIdentification18);

        paymentInstruction31.setDbtrAgt(debtorAgent);
        paymentInstruction31.setUltmtDbtr(debtor);
        paymentInstruction31.setChrgBr(ChargeBearerType1Code.DEBT);

        final CreditTransferTransaction35 creditTransferTransaction35 = new CreditTransferTransaction35();
        final PaymentIdentification6 paymentIdentification6 = new PaymentIdentification6();
        paymentIdentification6.setInstrId(GenerateKey.generateEntityId());
        paymentIdentification6.setEndToEndId(GenerateKey.generateEntityId());
        paymentIdentification6.setUETR(GenerateKey.generateEntityId());

        creditTransferTransaction35.setPmtId(paymentIdentification6);
        final PaymentTypeInformation26 paymentTypeInformation261 = new PaymentTypeInformation26();
        paymentTypeInformation261.setInstrPrty(Priority2Code.NORM);
        final LocalInstrument2Choice localInstrument2Choice1 = new LocalInstrument2Choice();
        localInstrument2Choice1.setCd("CD");
        localInstrument2Choice1.setPrtry("CD");

        paymentTypeInformation261.setLclInstrm(localInstrument2Choice1);
        final CategoryPurpose1Choice categoryPurpose1Choice1 = new CategoryPurpose1Choice();
        categoryPurpose1Choice1.setCd("CSH");
        categoryPurpose1Choice1.setPrtry("CSH");

        paymentTypeInformation261.setCtgyPurp(categoryPurpose1Choice1);

        creditTransferTransaction35.setPmtTpInf(paymentTypeInformation261);
        final PaymentCondition1 paymentCondition11 = new PaymentCondition1();
        creditTransferTransaction35.setPmtCond(paymentCondition11);
        final AmountType4Choice amountType4Choice = new AmountType4Choice();
        final ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount = new ActiveOrHistoricCurrencyAndAmount();
        activeOrHistoricCurrencyAndAmount.setValue(new BigDecimal("1230"));
        activeOrHistoricCurrencyAndAmount.setCcy("ZWL");

        amountType4Choice.setInstdAmt(activeOrHistoricCurrencyAndAmount);
        final EquivalentAmount2 equivalentAmount2 = new EquivalentAmount2();
        final ActiveOrHistoricCurrencyAndAmount activeOrHistoricCurrencyAndAmount1 = new ActiveOrHistoricCurrencyAndAmount();
        activeOrHistoricCurrencyAndAmount1.setValue(new BigDecimal("1345"));
        activeOrHistoricCurrencyAndAmount1.setCcy("ZWL");

        equivalentAmount2.setAmt(activeOrHistoricCurrencyAndAmount1);
        equivalentAmount2.setCcyOfTrf("ZWL");

        amountType4Choice.setEqvtAmt(equivalentAmount2);

        creditTransferTransaction35.setAmt(amountType4Choice);
        creditTransferTransaction35.setChrgBr(ChargeBearerType1Code.DEBT);
        creditTransferTransaction35.setUltmtDbtr(debtor);
        creditTransferTransaction35.setIntrmyAgt1(debtorAgent);
        creditTransferTransaction35.setIntrmyAgt2(debtorAgent);
        creditTransferTransaction35.setIntrmyAgt3(debtorAgent);
        final BranchAndFinancialInstitutionIdentification6 creditorAgent = new BranchAndFinancialInstitutionIdentification6();
        final FinancialInstitutionIdentification18 financialInstitutionIdentification181 = new FinancialInstitutionIdentification18();
        financialInstitutionIdentification181.setBICFI("WFA01553611956810974");
        financialInstitutionIdentification181.setLEI("WFA01553611956810974");
        financialInstitutionIdentification181.setNm("ZB");
        final GenericFinancialIdentification1 genericFinancialIdentification11 = new GenericFinancialIdentification1();
        genericFinancialIdentification11.setId("WFA01553611956810974");
        genericFinancialIdentification11.setIssr("RBZ");

        financialInstitutionIdentification181.setOthr(genericFinancialIdentification11);

        creditorAgent.setFinInstnId(financialInstitutionIdentification181);
        creditTransferTransaction35.setCdtrAgt(creditorAgent);
        creditTransferTransaction35.setCdtr(partyIdentification135);
        final CashAccount38 creditorAccount = new CashAccount38();
        final AccountIdentification4Choice accountIdentification4Choice1 = new AccountIdentification4Choice();
        accountIdentification4Choice1.setIBAN("123456789");
        final GenericAccountIdentification1 genericAccountIdentification11 = new GenericAccountIdentification1();
        genericAccountIdentification11.setId("123456789");
        genericAccountIdentification11.setIssr("ZB");

        accountIdentification4Choice1.setOthr(genericAccountIdentification11);

        creditorAccount.setId(accountIdentification4Choice1);
        final CashAccountType2Choice cashAccountType2Choice1 = new CashAccountType2Choice();
        cashAccountType2Choice1.setCd("SAVINGS");
        cashAccountType2Choice1.setPrtry("SAVINGS");

        creditorAccount.setTp(cashAccountType2Choice1);
        creditorAccount.setCcy("ZWL");
        creditorAccount.setNm("Petronella Muzondo");

        creditTransferTransaction35.setCdtrAcct(creditorAccount);
        creditTransferTransaction35.setUltmtCdtr(partyIdentification135);
        final Purpose2Choice purpose2Choice = new Purpose2Choice();
        purpose2Choice.setCd("OTHR");
        purpose2Choice.setPrtry("OTHR");

        creditTransferTransaction35.setPurp(purpose2Choice);

        paymentInstruction31.getCdtTrfTx().add(creditTransferTransaction35);

        creditorPaymentActivationRequestV07.getPmtInf().add(paymentInstruction31);

        return creditorPaymentActivationRequestV07;
    }

    private void addDebtorPaymentRoles(final IndividualPayment payment, CashAccount cashAccount) {
        log.info("Payment source account is: {}", cashAccount);

        /**
         * Configure Bulk Payment Roles
         */
        //The affected Bulk Payments
        //The affected Bulk Payment accounts
        final List<CashAccount> cashAccounts = Arrays.asList(cashAccount);

        final List<AccountPartyRole> accountPartyRoles = cashAccount.getPartyRole();

        //account servicing roles
        final List<AccountPartyRole> accountServicerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountServicerRole);
        }).collect(Collectors.toList());

        //The Role Players servicing this payment
        final List<RolePlayer> accountServicerRolePlayers = accountServicerRoles.stream()
                .flatMap(accountServicerRole -> accountServicerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //account owning roles
        final List<AccountPartyRole> accountOwnerRoles = accountPartyRoles.stream().filter(apr -> {
            return (apr instanceof AccountOwnerRole);
        }).collect(Collectors.toList());

        //The Role Players owning this payment
        final List<RolePlayer> accountOwnerRolePlayers = accountOwnerRoles.stream()
                .flatMap(accountOwnerRole -> accountOwnerRole.getPlayer().stream())
                .collect(Collectors.toList());

        //Debtor Role
        DebtorRole debtorRole = new DebtorRole();
        debtorRole.setPayment(Arrays.asList(payment));
        debtorRole.setCashAccount(cashAccounts);
        debtorRole.setId(GenerateKey.generateEntityId());
        debtorRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorRole.setDateCreated(OffsetDateTime.now());
        debtorRole.setLastUpdated(OffsetDateTime.now());
        debtorRole.setEntityVersion(0L);
        debtorRole.setPlayer(accountOwnerRolePlayers);
        debtorRole.setEntry(null);
        debtorRole.setPartyRoleCode(PartyRoleCode.Custodian);

        //Debtor Agent Role
        DebtorAgentRole debtorAgentRole = new DebtorAgentRole();
        debtorAgentRole.setPayment(Arrays.asList(payment));
        debtorAgentRole.setCashAccount(cashAccounts);
        debtorAgentRole.setId(GenerateKey.generateEntityId());
        debtorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        debtorAgentRole.setDateCreated(OffsetDateTime.now());
        debtorAgentRole.setLastUpdated(OffsetDateTime.now());
        debtorAgentRole.setEntityVersion(0L);
        debtorAgentRole.setPlayer(accountServicerRolePlayers);
        debtorAgentRole.setEntry(null);
        debtorAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Initiating Party Role
        InitiatingPartyRole initiatingPartyRole = new InitiatingPartyRole();
        initiatingPartyRole.setPayment(Arrays.asList(payment));
        initiatingPartyRole.setCashAccount(cashAccounts);
        initiatingPartyRole.setId(GenerateKey.generateEntityId());
        initiatingPartyRole.setEntityStatus(EntityStatus.ACTIVE);
        initiatingPartyRole.setDateCreated(OffsetDateTime.now());
        initiatingPartyRole.setLastUpdated(OffsetDateTime.now());
        initiatingPartyRole.setEntityVersion(0L);
        initiatingPartyRole.setPlayer(accountOwnerRolePlayers);
        initiatingPartyRole.setEntry(null);
        initiatingPartyRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Forwarding Party Role
        ForwardingAgentRole forwardingAgentRole = new ForwardingAgentRole();
        forwardingAgentRole.setPayment(Arrays.asList(payment));
        forwardingAgentRole.setCashAccount(cashAccounts);
        forwardingAgentRole.setId(GenerateKey.generateEntityId());
        forwardingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        forwardingAgentRole.setDateCreated(OffsetDateTime.now());
        forwardingAgentRole.setLastUpdated(OffsetDateTime.now());
        forwardingAgentRole.setEntityVersion(0L);
        forwardingAgentRole.setPlayer(accountServicerRolePlayers);
        forwardingAgentRole.setEntry(null);
        forwardingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);

        //Instructing Agent Role
        InstructingAgentRole instructingAgentRole = new InstructingAgentRole();
        instructingAgentRole.setPayment(Arrays.asList(payment));
        instructingAgentRole.setCashAccount(cashAccounts);
        instructingAgentRole.setId(GenerateKey.generateEntityId());
        instructingAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        instructingAgentRole.setDateCreated(OffsetDateTime.now());
        instructingAgentRole.setLastUpdated(OffsetDateTime.now());
        instructingAgentRole.setEntityVersion(0L);
        instructingAgentRole.setPlayer(accountServicerRolePlayers);
        instructingAgentRole.setEntry(null);
        instructingAgentRole.setPartyRoleCode(PartyRoleCode.FirstAgent);


        List<PaymentPartyRole> paymentPartyRoles = Arrays.asList(
                debtorRole, debtorAgentRole, initiatingPartyRole, forwardingAgentRole, instructingAgentRole
        );
        payment.getPartyRole().addAll(paymentPartyRoles);
    }

    private void addCreditorPaymentPartyRoles(final Optional<FinancialInstitution> financialInstitutionOptional, final RolePlayer client, Payment payment) {

        //Creditor Role
        CreditorRole creditorRole = new CreditorRole();
        creditorRole.setPayment(Arrays.asList(payment));
        creditorRole.setHasCustomParty(false);
        creditorRole.setId(GenerateKey.generateEntityId());
        creditorRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorRole.setDateCreated(OffsetDateTime.now());
        creditorRole.setLastUpdated(OffsetDateTime.now());
        creditorRole.setEntityVersion(0L);
        List<RolePlayer> clientRolePlayers = new ArrayList<>();
        clientRolePlayers.add(client);
        creditorRole.setPlayer(clientRolePlayers);
        creditorRole.setPartyRoleCode(PartyRoleCode.Custodian);


        //Creditor Agent Role
        CreditorAgentRole creditorAgentRole = new CreditorAgentRole();
        creditorAgentRole.setPayment(Arrays.asList(payment));
        financialInstitutionOptional.ifPresent(financialInstitution -> {
            List<RolePlayer> rolePlayers = new ArrayList<>();
            rolePlayers.add(financialInstitution);
            creditorAgentRole.setPlayer(rolePlayers);
        });
        creditorAgentRole.setId(GenerateKey.generateEntityId());
        creditorAgentRole.setEntityStatus(EntityStatus.ACTIVE);
        creditorAgentRole.setDateCreated(OffsetDateTime.now());
        creditorAgentRole.setLastUpdated(OffsetDateTime.now());
        creditorAgentRole.setEntityVersion(0L);
        creditorAgentRole.setEntry(null);
        creditorAgentRole.setPartyRoleCode(PartyRoleCode.FinalAgent);

        List<PaymentPartyRole> individualPaymentPartyRoles = new ArrayList<>();
        individualPaymentPartyRoles.addAll(Arrays.asList(creditorRole, creditorAgentRole));
        payment.getPartyRole().addAll(individualPaymentPartyRoles);
    }

    private String buildXml(CreditorPaymentActivationRequestStatusReportV07 creditorPaymentActivationRequestStatusReportV07) throws JAXBException {
        log.info("Building PAIN.014 XML message..");
        zw.co.esolutions.zpas.iso.msg.pain014_001_07.Document document = new zw.co.esolutions.zpas.iso.msg.pain014_001_07.Document();
        document.setCdtrPmtActvtnReqStsRpt(creditorPaymentActivationRequestStatusReportV07);

        Marshaller jaxbMarshaller = jaxbContextForPain014.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter stringWriter = new StringWriter();
        jaxbMarshaller.marshal(document, stringWriter);

        String xml = stringWriter.toString();

        log.info(xml);

        return xml;
    }
}
