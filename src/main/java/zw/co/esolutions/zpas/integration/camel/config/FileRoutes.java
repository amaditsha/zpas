package zw.co.esolutions.zpas.integration.camel.config;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.integration.camel.processor.incoming.FileProcessor;
import zw.co.esolutions.zpas.model.Payment;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsAcquiringService;
import zw.co.esolutions.zpas.services.impl.payments.converters.SFIDetailRecord;
import zw.co.esolutions.zpas.services.impl.payments.converters.SFIFileContents;
import zw.co.esolutions.zpas.utilities.util.PaymentsUtil;

import java.util.Collection;
import java.util.Optional;


@Component
public class FileRoutes extends RouteBuilder {

    @Value("${XSD.BASE.PATH}")
    String xsdBasePath;

    @Autowired
    private FileProcessor fileProcessor;

    @Autowired
    private PaymentsAcquiringService paymentsAcquiringService;

    @Override
    public void configure() throws Exception {
        onException(Exception.class)
                .log("Received exception trying to open FTP ..")
                .end();

        /**
         * Handling incoming Payment Initiation Request from corporate client
         */

        from("file:" + PaymentsUtil.PAYMENTS_API_REQUESTS+"?delete=true")
                .setProperty("ORIGINAL_FILE", body())
                .wireTap("file:" + PaymentsUtil.PAYMENTS_API_REQUESTS_ARCHIVE)
                .onException(Exception.class)
                .logStackTrace(true)
                .process(exchange -> {
                    final Throwable ex = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
                    ex.printStackTrace();
                })
                .log("Error processing response file, moving file into error folder")
                .handled(true)
                .setBody(exchangeProperty("ORIGINAL_FILE"))
                .to("file:" + PaymentsUtil.PAYMENTS_API_REQUESTS_ERROR)
                .end()
                .process(fileProcessor)
                .to("validator:file:" + xsdBasePath + "/xsd/pain/pain.001.001.08.xsd")
                .process(exchange -> {
                    String xml = exchange.getIn().getBody(String.class);
                    final Optional<Payment> paymentOptional = paymentsAcquiringService.buildPaymentFromXml(xml);
                    paymentOptional.map(payment -> {
                        exchange.getOut().setBody(payment);
                        return payment;
                    }).orElseGet(() -> {
                        exchange.getOut().setBody(null);
                        return null;
                    });
                })
                .choice()
                    .when(body().isNull())
                        .log("Payment processing failed.")
                    .otherwise()
                        .log("Payment processed successfully.")
                .end()
                .end()
                .end();


        /**
         * Handling incoming SFI Request from AQS and check if the bank IF is connected
         */

//        from("file:" + PaymentsUtil.INCOMING_REQUEST_FOLDER + "?delete=true")
//                .setProperty("ORIGINAL_FILE", body())
//                .wireTap("file:" + PaymentsUtil.INCOMING_REQUEST_ARCHIVE_FOLDER)
//                .onException(Exception.class)
//                .logStackTrace(true)
//                .process(exchange -> {
//                    final Throwable ex = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
//                    ex.printStackTrace();
//                })
//                .log("Error processing response file, moving file into error folder")
//                .handled(true)
//                .setBody(exchangeProperty("ORIGINAL_FILE"))
//                .to("file:" + PaymentsUtil.INCOMING_REQUEST_ERROR_FOLDER)
//                .end()
//                .process(fileProcessor)
//                .process(bankAdapterFileForwardingProcessor)
//                .log("Forwarding SFI File ...")
//                .process(outgoingSFIFileWriterProcessor)
//                .end();
    }
}
