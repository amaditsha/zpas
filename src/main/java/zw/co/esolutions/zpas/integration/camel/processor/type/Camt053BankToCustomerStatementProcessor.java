package zw.co.esolutions.zpas.integration.camel.processor.type;
import org.springframework.beans.factory.annotation.Value;
import zw.co.esolutions.zpas.dto.messaging.AccountStatementDTO;
import zw.co.esolutions.zpas.dto.messaging.AccountStatementInfo;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.iso.msg.camt053_001_07.*;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt053_001_07.Document;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.MessageRepository;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.services.iface.statements.StatementsService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.xml.bind.*;
import java.io.StringReader;
import java.lang.System;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@Slf4j
public class Camt053BankToCustomerStatementProcessor implements Processor {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt053_001_07");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired MessageRepository messageRepository;
    @Autowired NotificationService notificationService;
    @Autowired ClientService clientService;
    @Autowired StatementsService statementsService;

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Bank To Customer Statement : \n{}", message);

        BankToCustomerStatementV07 bankToCustomerStatementV07 = parseXml(message);
        log.info("Got message with MsgID : {}", bankToCustomerStatementV07.getGrpHdr().getMsgId());

        BigDecimal openingBalance = new BigDecimal("0");
        BigDecimal closingBalance = new BigDecimal("0");
        String accountNumber = "";
        String accountName = "";
        String accountServicer = "";
        String accountCurrency = "";
        String opbdDbtCrdtIndicator = "";
        String clbdDbtCrdtIndicator = "";

        List<AccountStatementInfo> accountStatementInfos = new ArrayList<>();
        Optional<AccountStatement8> accountStatement8Optional = bankToCustomerStatementV07.getStmt().stream().findFirst();

        /**Populate account infos to populate in account statement dto**/
        if(accountStatement8Optional.isPresent()){
            AccountStatement8 accountStatement8 = accountStatement8Optional.get();

            CashAccount36 cashAccount36 = accountStatement8.getAcct();
            accountNumber = cashAccount36.getId().getOthr().getId();
            accountName = cashAccount36.getNm();
            accountServicer = cashAccount36.getSvcr().getFinInstnId().getNm();
            accountCurrency = cashAccount36.getCcy();

            List<CashBalance8> cashBalances = accountStatement8.getBal();
            for(CashBalance8 cashBalance : cashBalances){
                if(cashBalance.getTp().getCdOrPrtry().getCd().equals("OPBD")){
                    openingBalance = cashBalance.getAmt().getValue();
                    opbdDbtCrdtIndicator = cashBalance.getCdtDbtInd().equals(CreditDebitCode.CRDT) ? "Credit" : "Debit";
                }else if(cashBalance.getTp().getCdOrPrtry().getCd().equals("CLBD")){
                    closingBalance = cashBalance.getAmt().getValue();
                    clbdDbtCrdtIndicator = cashBalance.getCdtDbtInd().equals(CreditDebitCode.CRDT) ? "Credit" : "Debit";
                }
            }

            List<ReportEntry9> entries = accountStatement8.getNtry();
            for (ReportEntry9 reportEntry9 : entries){
                AccountStatementInfo accountStatementInfo = new AccountStatementInfo();
                accountStatementInfo.setBookingDate(XmlDateUtil.getOffsetDateTime(reportEntry9.getBookgDt().getDtTm()));
                accountStatementInfo.setValueDate(XmlDateUtil.getDateNoTime(reportEntry9.getValDt().getDt()));
                accountStatementInfo.setTxnAmount(reportEntry9.getAmt().getValue());
                accountStatementInfo.setDebitCreditIndicator(reportEntry9.getCdtDbtInd().equals(CreditDebitCode.CRDT) ? "Credit" : "Debit");
                accountStatementInfo.setCurrency(reportEntry9.getAmt().getCcy());
                accountStatementInfo.setTxnStatus(reportEntry9.getSts().getCd());

                Optional<EntryTransaction9> entryTxnOptional = reportEntry9.getNtryDtls().stream().flatMap(details -> details.getTxDtls().stream()).findFirst();
                entryTxnOptional.ifPresent(txn -> {
                    accountStatementInfo.setTxnReference(txn.getRefs().getEndToEndId());
                    accountStatementInfo.setTxnNarrative(txn.getPurp().getCd());
                });

                accountStatementInfos.add(accountStatementInfo);
            }
        }

        String personIdNumber = getPersonId(bankToCustomerStatementV07);
        String clientId = "";
        String recipientName = "";
        String recipientEmailAddress = "";
        Optional<PersonIdentification> identificationOptional = clientService.findPersonIdentificationByIdNumber(personIdNumber);

        if(identificationOptional.isPresent()){
            PersonIdentification personIdentification = identificationOptional.get();
            Person person = personIdentification.getPerson();

            /**Get database id of message recipient for notification**/
            clientId = person.getId();

            /**Get email address of message recipient**/
            List<ContactPoint> contactPoints = clientService.getContactPointsByPersonId(person.getId());
            Optional<ContactPoint> contactPointOptional = contactPoints.stream()
                    .filter(contactPoint -> contactPoint instanceof ElectronicAddress).findFirst();
            if(contactPointOptional.isPresent()){
                ElectronicAddress electronicAddress = (ElectronicAddress) contactPointOptional.get();
                recipientEmailAddress = electronicAddress.getEmailAddress();
            }

            /**Get name of message recipient**/
            final Optional<PersonName> personNameOptional = personIdentification.getPersonName().stream().findFirst();
            if(personNameOptional.isPresent()){
                PersonName personName = personNameOptional.get();
                recipientName = personName.getBirthName() + " " + personName.getGivenName();
            }
        }

        AccountStatementDTO accountStatementDTO = AccountStatementDTO.builder()
                .recipient(recipientName)
                .emailAddress(recipientEmailAddress)
                .clientId(clientId)
                .openingBalance(openingBalance)
                .opbdDbtCrdtIndicator(opbdDbtCrdtIndicator)
                .closingBalance(closingBalance)
                .clbdDbtCrdtIndicator(clbdDbtCrdtIndicator)
                .accountNumber(accountNumber)
                .accountName(accountName)
                .accountServicer(accountServicer)
                .accountCurrency(accountCurrency)
                .accountStatementInfoList(accountStatementInfos)
                .build();

        /**Send account statement to email**/
        statementsService.sendAccountStatementInfo(accountStatementDTO);

        Message msg = new Message();
        msg.setId(GenerateKey.generateEntityId());
        msg.setDateCreated(OffsetDateTime.now());
        msg.setUuid(UUID.randomUUID().toString());
        String originalMsgId = "";
        if(bankToCustomerStatementV07.getGrpHdr().getOrgnlBizQry() != null){
            originalMsgId = bankToCustomerStatementV07.getGrpHdr().getOrgnlBizQry().getMsgId();
        }
        msg.setOriginalMsgId(originalMsgId);
        msg.setPayload(message);

        msg = messageRepository.save(msg);

        Notification notification = new Notification();
        notification.setId(GenerateKey.generateEntityId());
        notification.setEntityStatus(EntityStatus.NONE);
        notification.setMessage("Your account statement has arrived. Please check your email.");
        notification.setRead(false);
        notification.setEntityId("");
        notification.setClientId(clientId);
        notification.setNotificationType(NotificationType.NONE);
        notification.setGroupId("");

        notificationService.createNotification(notification);
    }

    private String getPersonId(BankToCustomerStatementV07 bankToCustomerStatementV07){
        String personId = "";

        Optional<PartyIdentification125> accountOwnerOptional = bankToCustomerStatementV07.getStmt().stream()
                .map(report -> report.getAcct().getOwnr()).findFirst();

        PartyIdentification125 msgRecipient = bankToCustomerStatementV07.getGrpHdr().getMsgRcpt();

        if(msgRecipient != null){
            personId = msgRecipient.getId().getPrvtId().getOthr().stream().map(GenericPersonIdentification1::getId).findFirst().orElse("");
        }else {

            personId = accountOwnerOptional
                    .map(accountOwner -> accountOwner.getId().getPrvtId().getOthr().stream()
                            .map(GenericPersonIdentification1::getId).findFirst().orElse(""))
                    .orElse("");
        }

        return personId;
    }

    private BankToCustomerStatementV07 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
        System.out.println(document);

        BankToCustomerStatementV07 bankToCustomerStatementV07 = document.getBkToCstmrStmt();

        return bankToCustomerStatementV07;
    }
}
