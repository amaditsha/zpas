package zw.co.esolutions.zpas.integration.camel.processor.type;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.dto.nssa.ContributionScheduleDTO;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;
import zw.co.esolutions.zpas.enums.PaymentTypeCode;
import zw.co.esolutions.zpas.iso.msg.pain002_001_09.CustomerPaymentStatusReportV09;
import zw.co.esolutions.zpas.iso.msg.pain002_001_09.Document;
import zw.co.esolutions.zpas.iso.msg.pain002_001_09.PaymentTransaction92;
import zw.co.esolutions.zpas.iso.msg.pain002_001_09.StatusReasonInformation11;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentExecutionRepository;
import zw.co.esolutions.zpas.repository.PaymentIdentificationRepository;
import zw.co.esolutions.zpas.repository.PaymentStatusRepository;
import zw.co.esolutions.zpas.services.iface.payments.PaymentsService;
import zw.co.esolutions.zpas.services.impl.payments.zimra.ZimraProcessor;
import zw.co.esolutions.zpas.services.impl.payments.zimra.response.ProcessPaymentResponse;
import zw.co.esolutions.zpas.services.impl.process.pojo.StatusReasonDto;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentStatusUtil;
import zw.co.esolutions.zpas.services.nssa.iface.ContributionsService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.SpecialPayee;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.lang.System;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Handler for PAIN.002
 */

@Component
@Slf4j
public class Pain002CustomerPaymentStatusReportProcessor implements Processor {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.pain002_001_09");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private ZimraProcessor zimraProcessor;

    @Autowired
    private ContributionsService contributionsService;

    @Autowired
    private PaymentsService paymentsService;

    @Autowired
    private PaymentExecutionRepository paymentExecutionRepository;

    @Autowired
    private PaymentStatusRepository paymentStatusRepository;

    @Autowired
    private PaymentStatusUtil paymentStatusUtil;

    @Autowired
    private PaymentIdentificationRepository paymentIdentificationRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Customer Payment Status Report : \n{}", message);

        CustomerPaymentStatusReportV09 customerPaymentStatusReportV09 = parseXml(message);

        log.info("Got message with MsgID : {}", customerPaymentStatusReportV09.getGrpHdr().getMsgId());

        String originalMsgId = customerPaymentStatusReportV09.getOrgnlGrpInfAndSts().getOrgnlMsgId();

        log.info("OrgnlMsgID is : {}", originalMsgId);

        Optional<PaymentExecution> paymentExecutionOptional = paymentExecutionRepository.findById(originalMsgId);

        if (paymentExecutionOptional.isPresent()) {

            PaymentExecution paymentExecution = paymentExecutionOptional.get();

            log.info("Found Payment Execution linked to this MsgID : {}", paymentExecution.getId());

            Optional<Payment> paymentOptional = paymentExecution.getPayment().stream().findFirst();

            //Get the Group Status
            String groupStatusCode = customerPaymentStatusReportV09.getOrgnlGrpInfAndSts().getGrpSts();
            log.info("Group Status Code : {}", groupStatusCode);

            final List<StatusReasonInformation11> grpStsRsnInf = customerPaymentStatusReportV09.getOrgnlGrpInfAndSts().getStsRsnInf();

            List<StatusReasonDto> groupStatusReasons = grpStsRsnInf.stream()
                    .map(statusReasonInfo -> new StatusReasonDto(statusReasonInfo.getRsn().getCd(), statusReasonInfo.getAddtlInf()))
                    .collect(Collectors.toList());

            if (paymentOptional.isPresent()) {
                Payment payment = paymentOptional.get();
                log.info("Referenced payment is present, updating it.. [{}]", payment.getId());
                if (StringUtils.isNotBlank(groupStatusCode)) {
                    log.info("Group Status Code is present, updating the payment");
                    paymentStatusUtil.updatePaymentStatus(payment, PaymentStatusCode.valueOfCodeName(groupStatusCode), groupStatusReasons);
                } else {
                    log.info("Group Status Code is not present, skip update of bulk payment");
                }
            }

            switch (groupStatusCode) {
                case "PART":
                    log.info("{} group status received, now performing update of failed individual payments, if any.");
                    customerPaymentStatusReportV09.getOrgnlPmtInfAndSts().stream()
                            .forEach(originalPaymentInstruction27 -> {

                                List<String> paymentEndToEndIds = new ArrayList<>();
                                originalPaymentInstruction27.getTxInfAndSts().stream()
                                        .forEach(paymentTransaction92 -> {

                                            String originalPaymentId = paymentTransaction92.getOrgnlInstrId() == null ?
                                                    paymentTransaction92.getOrgnlEndToEndId() : paymentTransaction92.getOrgnlInstrId();

                                            log.info("Processing payment with OrgnlInstrID/OrgnlEndToEndID: {}", originalPaymentId);
                                            paymentEndToEndIds.add(originalPaymentId);

                                            String txnStatusCode = paymentTransaction92.getTxSts();
                                            log.info("Transaction Status Code : {}", txnStatusCode);

                                            List<StatusReasonInformation11> txnStatusReasonInformation = paymentTransaction92.getStsRsnInf();

                                            List<StatusReasonDto> txnStatusReasons = txnStatusReasonInformation.stream()
                                                    .map(statusReasonInfo -> new StatusReasonDto(statusReasonInfo.getRsn().getCd(), statusReasonInfo.getAddtlInf()))
                                                    .collect(Collectors.toList());

                                            Optional<Payment> transactionOptional = paymentsService.getPaymentByEndToEndId(originalPaymentId);

                                            if (transactionOptional.isPresent()) {
                                                Payment transaction = transactionOptional.get();
                                                log.info("Found individual payment transaction, updating it ({})", transaction.getId());
                                                if (StringUtils.isNotBlank(txnStatusCode)) {
                                                    log.info("Txn Status Code is present, updating the payment");
                                                    paymentStatusUtil.updateTransactionStatus(transaction, PaymentStatusCode.valueOfCodeName(txnStatusCode), txnStatusReasons);
                                                } else {
                                                    log.info("Txn Status Code is not present, skip update of bulk payment");
                                                }
                                            } else {
                                                log.info("Original payment not found, discarding ({})", paymentTransaction92.getOrgnlInstrId());
                                            }
                                        });
                                paymentOptional.ifPresent(payment -> {
                                    if (payment instanceof BulkPayment) {
                                        final List<IndividualPayment> individualPayments = ((BulkPayment) payment).getGroups();
                                        individualPayments.removeIf(individualPayment -> {
                                            final String endToEndId = individualPayment.getEndToEndId();
                                            return paymentEndToEndIds.contains(endToEndId);
                                        });
                                        updatePaymentStatuses(individualPayments, PaymentStatusCode.Accepted);
                                    } else {
                                        log.info("Updating single payment status");
                                        updatePaymentStatus(payment, PaymentStatusCode.Accepted);
                                    }
                                });
                            });
                    break;
                default: {
                    log.info("Received payment status code: {}", groupStatusCode);
                    final PaymentStatusCode paymentStatusCode = PaymentStatusCode.valueOfCodeName(groupStatusCode);
                    log.info("Parsed payment status code: {}", paymentStatusCode);

                    paymentOptional.ifPresent(payment -> {
                        if (payment instanceof BulkPayment) {
                            final List<IndividualPayment> individualPayments = ((BulkPayment) payment).getGroups();
                            log.info("Updating individual payment statuses");
                            updatePaymentStatuses(individualPayments, paymentStatusCode);
                        } else {
                            log.info("Updating single payment status");
                            updatePaymentStatus(payment, paymentStatusCode);
                        }
                    });
                    break;
                }
            }

            paymentOptional.ifPresent(payment -> {
                log.info("Updating bulk status and other entity statuses.");
                paymentsService.updateBulkPaymentStatuses(payment.getId());
            });

            //handle payment call backs
            paymentOptional.ifPresent(payment -> {
                switch (payment.getSpecialPayee()) {
                    case NSSA:
                        log.info("Handling payment callbacks.");
                        final String endToEndId = payment.getEndToEndId();
                        final Optional<ContributionScheduleDTO> contributionScheduleOptional
                                = contributionsService.handleContributionSchedulePaymentStatusReportReceived(endToEndId);

                        contributionScheduleOptional.ifPresent(contributionSchedule -> {
                            log.info("Contribution schedule for {} handled payment status report successfully.",
                                    contributionSchedule.getMonth().format(DateTimeFormatter.ofPattern("MMMM yyyy")));
                        });

                        if (!contributionScheduleOptional.isPresent()) {
                            log.info("Failed to handle Contribution schedule payment status report, maybe no contribution schedule was associated with " +
                                    "payment reference \"{}\"", endToEndId);
                        }
                        break;
                    case ZIMRA:
                        final Optional<String> processPaymentResponseOptional = zimraProcessor.handlePaymentStatusReport(payment);
                        processPaymentResponseOptional.ifPresent(processPaymentResponse -> {
                            log.info("Got the following response from ZIMRA: {}", processPaymentResponse);
                        });
                        break;
                    case NONE:
                    default:
                        log.info("No candidate callbacks for this payment.");
                }
            });

            log.info("Finished processing CustomerPaymentStatusReport (pain.002) response..");

            // Publish any necessary events e.g CustomerPaymentStatusReportProcessed
            // Consumers of these events can then decide on what to do e.g notify client

        } else {
            log.info("Payment Execution linked to this MsgID not found : {}", originalMsgId);
            log.info("Message discarded : {}", originalMsgId);
        }

    }

    private void updatePaymentStatuses(List<IndividualPayment> individualPayments, PaymentStatusCode paymentStatusCode) {
        individualPayments.removeIf(p -> p.getEntityStatus() == EntityStatus.DISAPPROVED);
        individualPayments.forEach(individualPayment -> {
            updatePaymentStatus(individualPayment, paymentStatusCode);
        });
    }

    private void updatePaymentStatus(Payment payment, PaymentStatusCode paymentStatusCode) {
        log.info("Found individual payment payment, updating it ({})", payment.getId());
        final ArrayList<StatusReasonDto> statusReasonDTOS = new ArrayList<>();
        statusReasonDTOS.add(StatusReasonDto.builder()
                .code(paymentStatusCode.name())
                .narratives(new ArrayList<>(Arrays.asList("Processed.")))
                .build());
        paymentStatusUtil.updateTransactionStatus(payment, paymentStatusCode, statusReasonDTOS);

    }

    private void updateReferences(Payment payment, PaymentTransaction92 paymentTransaction92) {
        PaymentIdentification identification = payment.getPaymentRelatedIdentifications().get(0);
        identification.setClearingSystemReference(paymentTransaction92.getClrSysRef());

        paymentIdentificationRepository.save(identification);
    }

    private CustomerPaymentStatusReportV09 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
        System.out.println(document);

        CustomerPaymentStatusReportV09 customerPaymentStatusReportV09 = document.getCstmrPmtStsRpt();

        return customerPaymentStatusReportV09;
    }

}
