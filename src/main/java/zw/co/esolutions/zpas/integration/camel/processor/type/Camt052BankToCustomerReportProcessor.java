package zw.co.esolutions.zpas.integration.camel.processor.type;
import org.springframework.beans.factory.annotation.Value;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.iso.msg.camt052_001_07.Document;
import zw.co.esolutions.zpas.model.*;
import com.google.common.collect.Lists;
import zw.co.esolutions.zpas.enums.BalanceAdjustmentTypeCode;
import zw.co.esolutions.zpas.enums.BalanceTypeCode;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.enums.BalanceCounterPartyCode;
import zw.co.esolutions.zpas.enums.DebitCreditCode;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.iso.msg.camt052_001_07.PartyIdentification125;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.iso.msg.camt052_001_07.*;
import zw.co.esolutions.zpas.iso.util.XmlDateUtil;
import zw.co.esolutions.zpas.repository.CashAccountRepository;
import zw.co.esolutions.zpas.repository.CashBalanceRepository;
import zw.co.esolutions.zpas.repository.MessageRepository;
import zw.co.esolutions.zpas.services.iface.account.AccountsService;
import zw.co.esolutions.zpas.services.iface.account.BalancesService;
import zw.co.esolutions.zpas.services.iface.alerts.NotificationService;
import zw.co.esolutions.zpas.services.iface.registration.ClientService;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;

import javax.xml.bind.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringReader;
import java.lang.System;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class Camt052BankToCustomerReportProcessor implements Processor {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt052_001_07");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired MessageRepository messageRepository;
    @Autowired NotificationService notificationService;
    @Autowired ClientService clientService;
    @Autowired AccountsService accountsService;
    @Autowired BalancesService balancesService;
    @Autowired CashAccountRepository cashAccountRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Bank To Customer Report : \n{}", message);

        BankToCustomerAccountReportV07 bankToCustomerAccountReportV07 = parseXml(message);
        log.info("Got message with MsgID : {}", bankToCustomerAccountReportV07.getGrpHdr().getMsgId());

        Message msg = new Message();
        msg.setId(GenerateKey.generateEntityId());
        msg.setDateCreated(OffsetDateTime.now());
        msg.setUuid(UUID.randomUUID().toString());
        String originalMsgId = "";
        if(bankToCustomerAccountReportV07.getGrpHdr().getOrgnlBizQry() != null){
            originalMsgId = bankToCustomerAccountReportV07.getGrpHdr().getOrgnlBizQry().getMsgId();
        }
        msg.setOriginalMsgId(originalMsgId);
        msg.setPayload(message);

        msg = messageRepository.save(msg);

        BigDecimal currentBalance = new BigDecimal("0");

        final List<CashBalance8> balance8s = bankToCustomerAccountReportV07.getRpt().stream().flatMap(accountReport22 -> accountReport22.getBal().stream())
                .collect(Collectors.toList());

        final List<CashBalance8> sortedBalances = balance8s.stream().sorted((b1, b2) -> {
            return XmlDateUtil.getOffsetDateTime(b2.getDt().getDtTm()).compareTo(XmlDateUtil.getOffsetDateTime(b1.getDt().getDtTm()));
        }).collect(Collectors.toList());

        CashBalance8 cashBalance8 = null;
        if(sortedBalances.size() > 0) {
            cashBalance8 = sortedBalances.get(0);
            log.info("Balance: " + cashBalance8.getAmt().getValue());
            currentBalance = cashBalance8.getAmt().getValue();
        }

        String accountNumber = bankToCustomerAccountReportV07.getRpt().stream().findFirst()
                .map(report -> report.getAcct().getId().getOthr().getId()).orElse("");

        /**
         * Create/Update cash balances in order to display account balances on My Accounts
         */
        for(AccountReport22 accountReport22 : bankToCustomerAccountReportV07.getRpt()){
            CashAccount cashAccount = getAccount(accountReport22);
            log.info("Account id: " + cashAccount.getId());

            //find account balances for this account
            List<CashBalance> cashBalances = cashAccount.getCashBalance();
            CashBalance cashBalance = new CashBalance();

            log.info("Cash Balances: " + cashBalances);

            //check if account balances exist
            if(cashBalances != null && !cashBalances.isEmpty()){
                log.info("Balance exists, updating...");
                for(CashBalance cashBalance1 : cashBalances){
                    //check if interim available balance exists and update it if it does exist
                    if(cashBalance1.getType().getCodeName().equals("ITAV")){
                        cashBalance8 = getBalance(accountReport22);

                        CurrencyCode currencyCode = new CurrencyCode();
                        currencyCode.setCodeName(cashBalance8.getAmt().getCcy());
                        currencyCode.setName(cashBalance8.getAmt().getCcy());

                        CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();
                        currencyAndAmount.setAmount(cashBalance8.getAmt().getValue());
                        currencyAndAmount.setCurrency(currencyCode);

                        //cashBalance.setId(cashBalance1.getId());
                        cashBalance1.setAmount(currencyAndAmount);
                        cashBalance1.setValueDate(XmlDateUtil.getOffsetDateTime(cashBalance8.getDt().getDtTm()));
                        cashBalance1.setCreditDebitIndicator(DebitCreditCode.valueOfCodeName(cashBalance8.getCdtDbtInd().name()));

                        cashBalance = balancesService.updateCashBalance(cashBalance1);

                        log.info("Updated cash balance: " + cashAccount.getCashBalance());
                    }
                }

            }else{
                //if this account does not have a balance yet, create it
                //List<CashAccount> account = new ArrayList<>();
                //account.add(cashAccount);
                cashBalance8 = getBalance(accountReport22);

                CurrencyCode currencyCode = new CurrencyCode();
                currencyCode.setCodeName(cashBalance8.getAmt().getCcy());
                currencyCode.setName(cashBalance8.getAmt().getCcy());

                CurrencyAndAmount currencyAndAmount = new CurrencyAndAmount();
                currencyAndAmount.setAmount(cashBalance8.getAmt().getValue());
                currencyAndAmount.setCurrency(currencyCode);

                cashBalance.setId(GenerateKey.generateEntityId());
                cashBalance.setAmount(currencyAndAmount);
                cashBalance.setCashAccount(new ArrayList<>());
                cashBalance.setType(BalanceTypeCode.InterimAvailable);
                cashBalance.setValueDate(XmlDateUtil.getOffsetDateTime(cashBalance8.getDt().getDtTm()));
                cashBalance.setCreditDebitIndicator(DebitCreditCode.valueOfCodeName(cashBalance8.getCdtDbtInd().name()));
                cashBalance.setEntityStatus(EntityStatus.ACTIVE);
                log.info("Cash Balance Before Creating: " + cashBalance);

                //Persist Balance
                cashBalance = balancesService.createCashBalance(cashBalance);

                //Update account to include balance
                cashAccount.getCashBalance().add(cashBalance);
                cashAccount = cashAccountRepository.save(cashAccount);

                log.info("Created cash balance: " + cashAccount.getCashBalance());
            }

        }

        /**
         * Get clientId to send notification to.
         */
        String clientId = getClientId(bankToCustomerAccountReportV07);
        log.info("clientId present..." + clientId);

        //Use this route if id number is provided
        /*String personIdNumber = getPersonIdNumber(bankToCustomerAccountReportV07);
        Optional<PersonIdentification> identificationOptional = clientService.findPersonIdentificationByIdNumber(personIdNumber);
        if(identificationOptional.isPresent()){
            clientId = identificationOptional.get().getPerson().getId();
        }*/

        Notification notification = new Notification();
        notification.setId(GenerateKey.generateEntityId());
        notification.setEntityStatus(EntityStatus.NONE);
        notification.setMessage("Your account balance for account: " + accountNumber  + " is: " + MoneyUtil.convertToDollarsPattern(currentBalance));
        notification.setNotificationType(NotificationType.NONE);
        notification.setRead(false);
        notification.setEntityId("");
        notification.setClientId(clientId);
        notification.setGroupId("");

        notificationService.createNotification(notification);

    }

    private CashAccount getAccount(AccountReport22 accountReport22){
        log.info("Inside getAccount()...");
        CashAccount cashAccount = null;
        CashAccount36 cashAccount36 = accountReport22.getAcct();
        log.info("Account Number: " + cashAccount36.getId().getOthr().getId());
        Optional<CashAccount> cashAccountOptional = accountsService.findAccountByNumber(cashAccount36.getId().getOthr().getId());
        if(cashAccountOptional.isPresent()){
            log.info("CashAccount is present...");
            cashAccount = cashAccountOptional.get();
        }
        log.info("Account: " + cashAccount);

        return cashAccount;
    }

    private CashBalance8 getBalance(AccountReport22 accountReport22){
        CashBalance8 cashBalance8 = null;
        final List<CashBalance8> balance8s = accountReport22.getBal();
        for(CashBalance8 balance8 : balance8s){
            if(balance8.getTp().getCdOrPrtry().getCd().equals("ITAV")){
                cashBalance8 = balance8;
            }
        }
        return cashBalance8;
    }

    private String getPersonIdNumber(BankToCustomerAccountReportV07 bankToCustomerAccountReportV07){
        String personId = "";

        Optional<PartyIdentification125> accountOwnerOptional = bankToCustomerAccountReportV07.getRpt().stream()
                .map(report -> report.getAcct().getOwnr()).findFirst();

        PartyIdentification125 msgRecipient = bankToCustomerAccountReportV07.getGrpHdr().getMsgRcpt();

        if(msgRecipient != null){
            personId = msgRecipient.getId().getPrvtId().getOthr().stream().map(GenericPersonIdentification1::getId).findFirst().orElse("");
        }else {

            personId = accountOwnerOptional
                    .map(accountOwner -> accountOwner.getId().getPrvtId().getOthr().stream()
                            .map(GenericPersonIdentification1::getId).findFirst().orElse(""))
                    .orElse("");
        }

        return personId;
    }

    private String getClientId(BankToCustomerAccountReportV07 bankToCustomerAccountReportV07){
        String clientId = "";
        Optional<AccountReport22> accountReport22Optional = bankToCustomerAccountReportV07.getRpt().stream().findFirst();
        CashAccount cashAccount = new CashAccount();
        if(accountReport22Optional.isPresent()){
            cashAccount = getAccount(accountReport22Optional.get());
        }
        clientId = cashAccount.getPartyRole().stream().filter(accountPartyRole -> accountPartyRole instanceof AccountOwnerRole)
                .flatMap(accountPartyRole -> accountPartyRole.getPlayer().stream())
                .map(rolePlayer -> rolePlayer.getId())
                .findFirst().orElse("");
        return clientId;
    }

    private BankToCustomerAccountReportV07 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
        System.out.println(document);

        BankToCustomerAccountReportV07 bankToCustomerAccountReportV07 = document.getBkToCstmrAcctRpt();

        return bankToCustomerAccountReportV07;
    }
}
