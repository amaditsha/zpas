package zw.co.esolutions.zpas.integration.camel.processor.type;
import org.springframework.beans.factory.annotation.Value;
import zw.co.esolutions.zpas.enums.CaseAssignmentRejectionCode;
import zw.co.esolutions.zpas.enums.PaymentCancellationRejectionCode;
import zw.co.esolutions.zpas.model.PaymentInvestigationCaseResolution;
import zw.co.esolutions.zpas.enums.PaymentModificationRejectionV2Code;
import zw.co.esolutions.zpas.enums.InvestigationRejectionCode;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.enums.CaseStatusCode;
import zw.co.esolutions.zpas.enums.InvestigationExecutionConfirmationCode;
import zw.co.esolutions.zpas.iso.msg.camt031_001_05.Document;
import zw.co.esolutions.zpas.iso.msg.camt031_001_05.Party35Choice;
import zw.co.esolutions.zpas.iso.msg.camt031_001_05.RejectInvestigationV05;
import zw.co.esolutions.zpas.model.*;
import zw.co.esolutions.zpas.repository.PaymentInvestigationCaseRejectionRepository;
import zw.co.esolutions.zpas.repository.PaymentInvestigationCaseRepository;
import zw.co.esolutions.zpas.repository.PaymentInvestigationCaseResolutionRepository;
import zw.co.esolutions.zpas.repository.PaymentRepository;
import zw.co.esolutions.zpas.security.repo.ProfileRepository;
import zw.co.esolutions.zpas.services.impl.process.util.InvestigationCaseStatusUtil;
import zw.co.esolutions.zpas.services.impl.process.util.NotificationUtil;
import zw.co.esolutions.zpas.services.impl.process.util.PaymentStatusUtil;
import zw.co.esolutions.zpas.services.impl.process.util.StatusReasonUtil;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import javax.xml.bind.*;
import java.io.StringReader;
import java.lang.System;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Handler for CAMT_031
 */

@Component
@Slf4j
public class Camt031RejectInvestigationProcessor implements Processor {

    @Value("${XML.ENCODING}")
    private String xmlEncoding;

    static JAXBContext jaxbContext;

    static {
        try {
            jaxbContext = JAXBContext.newInstance("zw.co.esolutions.zpas.iso.msg.camt031_001_05");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    PaymentInvestigationCaseRepository paymentInvestigationCaseRepository;

    @Autowired
    InvestigationCaseStatusUtil investigationCaseStatusUtil;

    @Autowired
    StatusReasonUtil statusReasonUtil;

    @Autowired
    PaymentStatusUtil paymentStatusUtil;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    NotificationUtil notificationUtil;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    PaymentInvestigationCaseResolutionRepository paymentInvestigationCaseResolutionRepository;

    @Autowired
    PaymentInvestigationCaseRejectionRepository paymentInvestigationCaseRejectionRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        final String message = exchange.getIn().getBody(String.class);
        log.info("Processing Reject Investigation : \n{}", message);

        RejectInvestigationV05 rejectInvestigationV05 = parseXml(message);
        log.info("Got Reject Investigation (Camt031) message with MsgId : {}", rejectInvestigationV05.getAssgnmt().getId());

        String caseAssignmentId = rejectInvestigationV05.getAssgnmt().getId();
        log.info("caseAssignmentId is for Camt031: {}", caseAssignmentId);

        String assignerId = getAssignerIDOrAssigneeID(rejectInvestigationV05, Assigner.class);
        log.info("assigneeId for Camt031 is : {}", assignerId);

        String assigneeId = getAssignerIDOrAssigneeID(rejectInvestigationV05, Assignee.class);
        log.info("assigneeId for Camt031 is : {}", assigneeId);

        String creDtTm = rejectInvestigationV05.getAssgnmt().getCreDtTm().toXMLFormat();
        log.info("Creation Date Time is : {}", creDtTm);

        String caseId = rejectInvestigationV05.getCase().getId();
        log.info("caseId is for Camt031: {}", caseId);

        Party35Choice cretr = rejectInvestigationV05.getCase().getCretr();
        String cretrId = getPartyId(cretr);
        log.info("creator Id is : {}", cretrId);

        Optional<PaymentInvestigationCase> paymentInvestigationCaseOptional = Optional.ofNullable(paymentInvestigationCaseRepository.findByIdentification(caseId));

        if (paymentInvestigationCaseOptional.isPresent()) {
            PaymentInvestigationCase paymentInvestigationCase = paymentInvestigationCaseOptional.get();
            log.info("Found Payment Investigation Case linked to this caseAssignmentId : {}", paymentInvestigationCase.getId());

            String rjctnRsn;
            InvestigationExecutionConfirmationCode investigationExecutionConfirmationCode;
            switch (rejectInvestigationV05.getJustfn().getRjctnRsn()) {

                case NFND:
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.NONE;

                    rjctnRsn = "UnderlyingPaymentNotFound";
                    log.info(rjctnRsn);
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase, CaseStatusCode.Closed, "Underlying Payment Not Found");
                    break;
                case NAUT:
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.NONE;

                    log.info("NotAuthorisedToInvestigate");
                    rjctnRsn = "Not Authorised To Investigate";
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase, CaseStatusCode.Closed, "Not Authorised To Investigate");
                    break;
                case UKNW:
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.NONE;

                    log.info("UnknownCase");
                    rjctnRsn = "Unknown Case";
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase, CaseStatusCode.Unknown, "Unknown Case");
                    break;
                case PCOR:
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.RejectedCancellationRequest;

                    log.info("PaymentPreviouslyCancelledOrRejected");
                    rjctnRsn = "Payment Previously Cancelled Or Rejected";
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase, CaseStatusCode.Closed, "Payment Previously Cancelled or Rejected");

                    break;
                case WMSG:
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.NONE;

                    log.info("WrongMessage");
                    rjctnRsn = "Wrong Message";
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase, CaseStatusCode.Closed, "Wrong Message");
                    break;
                case RNCR:
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.RejectedCancellationRequest;

                    log.info("RejectNonCashRelated");
                    rjctnRsn = "Reject Non Cash Related";
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase, CaseStatusCode.Closed, "Reject None Cash Related");
                    break;
                case MROI:
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.NONE;

                    log.info("MissingResolutionOfInvestigation");
                    rjctnRsn = "Missing Resolution Of Investigation";
                    investigationCaseStatusUtil.saveCaseStatus(paymentInvestigationCase, CaseStatusCode.Closed, "Missing Resolution Of Investigation");
                    break;
                default:
                    investigationExecutionConfirmationCode = InvestigationExecutionConfirmationCode.NONE;
                    rjctnRsn = "Case Rejection Reason not found";
                    log.info("Case Rejection Reason not found");
            }

            log.info("Saving the payment investigation case resolution for case with id : {} and code : {}",
                    paymentInvestigationCase.getId(), investigationExecutionConfirmationCode);
            generatePaymentResolution(paymentInvestigationCase, investigationExecutionConfirmationCode, rjctnRsn);

            log.info("Getting the role player from the case creator of investigation party role");
            notifingCreater(paymentInvestigationCase, rjctnRsn);
        }
    }



    private void generatePaymentResolution(PaymentInvestigationCase paymentInvestigationCase,
                                           InvestigationExecutionConfirmationCode investigationExecutionConfirmationCode,
                                           String narrative) {

        //initialise the resolution
        PaymentInvestigationCaseResolution paymentInvestigationCaseResolution = new PaymentInvestigationCaseResolution();
        paymentInvestigationCaseResolution.setId(UUID.randomUUID().toString());

        paymentInvestigationCaseResolution.setInvestigationStatus(investigationExecutionConfirmationCode);
        //paymentInvestigationCaseResolution.setDebitAuthorisationConfirmation();
        //paymentInvestigationCaseResolution.setCoverCorrection();
        //paymentInvestigationCaseResolution.setEntryCorrection();
        //paymentInvestigationCaseResolution.setPaymentExecutionCorrection();
        //paymentInvestigationCaseResolution.setDuplicateCase();
        paymentInvestigationCaseResolution.setEntityStatus(EntityStatus.NONE);
        paymentInvestigationCaseResolution.setInvestigationCase(paymentInvestigationCase);
        paymentInvestigationCaseResolution.setInvestigationCaseReference(paymentInvestigationCase.getIdentification());

        if(investigationExecutionConfirmationCode.equals(InvestigationExecutionConfirmationCode.RejectedCancellationRequest)) {
            log.info("This Resolution was REJECTED.. updating resolution with Rejection");

            PaymentInvestigationCaseRejection paymentInvestigationCaseRejection = new PaymentInvestigationCaseRejection();
            paymentInvestigationCaseRejection.setId(UUID.randomUUID().toString());
            paymentInvestigationCaseRejection.setEntityStatus(EntityStatus.NONE);
            //    paymentInvestigationCaseRejection.setRejectedModification();
            //    paymentInvestigationCaseRejection.setRejectedCancellation();
            paymentInvestigationCaseRejection.setRejectedCancellationReason(narrative);
            //    paymentInvestigationCaseRejection.setAssignmentCancellationConfirmation();
            //   paymentInvestigationCaseRejection.setRejectionReason();
            paymentInvestigationCaseRejection.setRelatedInvestigationCaseResolution(paymentInvestigationCaseResolution);
            //    paymentInvestigationCaseRejection.setInvestigationRejection();

            paymentInvestigationCaseResolution.setInvestigationCaseRejection(paymentInvestigationCaseRejection);
        }

        log.info("Payment Resolution \n{}\n", paymentInvestigationCaseResolution);

        paymentInvestigationCaseResolutionRepository.save(paymentInvestigationCaseResolution);
    }



    private String getAssignerIDOrAssigneeID(RejectInvestigationV05 rejectInvestigationV05, Class<?> investigationPartyRoleSubclass) {
        if (investigationPartyRoleSubclass.isInstance(new Assigner())) {
            Party35Choice party35Choice = rejectInvestigationV05.getAssgnmt().getAssgnr();
            return getPartyId(party35Choice);
        } else {
            Party35Choice party35Choice = rejectInvestigationV05.getAssgnmt().getAssgne();
            return getPartyId(party35Choice);
        }
    }

    private String getPartyId(Party35Choice party35Choice) {
        String assignerID;
        if (party35Choice.getPty() != null) {
            if (party35Choice.getPty().getId().getOrgId() != null) {
                assignerID = party35Choice.getPty().getId().getOrgId().getAnyBIC();
            } else {
                assignerID = party35Choice.getPty().getId().getPrvtId().getOthr().get(0).getId();
            }
        } else {
            if (party35Choice.getAgt().getBrnchId() != null) {
                assignerID = party35Choice.getAgt().getBrnchId().getId();
            } else {
                assignerID = party35Choice.getAgt().getFinInstnId().getBICFI();
            }
        }
        return assignerID;
    }

    private void notifingCreater(PaymentInvestigationCase paymentInvestigationCase, String confMsg) {
        Optional<RolePlayer> rolePlayer = paymentInvestigationCase.getInvestigationPartyRole().stream()
                .filter(CaseCreator.class::isInstance)
                .map(Role::getPlayer)
                .map(rolePlayers -> rolePlayers.stream().findFirst().orElseGet(null))
                .findFirst();

        if (rolePlayer.isPresent()) {
            log.info("Found the case creator for investigation case {}", paymentInvestigationCase.getCaseType());
            log.info("Send the system notification ...");
            String messageText = confMsg + "For the case type " + paymentInvestigationCase.getCaseType();
            String subject = "Notification for case type " + paymentInvestigationCase.getCaseType();
            String originator = "System";
            notificationUtil.sendNotification(rolePlayer.get().getId(), "", originator, OffsetDateTime.now(), messageText, MessageChannel.EMAIL, subject);
        }
    }

    private RejectInvestigationV05 parseXml(String message) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(message);
        Document document = (Document) JAXBIntrospector.getValue(jaxbUnmarshaller.unmarshal(reader));
        System.out.println(document);

        RejectInvestigationV05 rejectInvestigationV05 = document.getRjctInvstgtn();

        return rejectInvestigationV05;
    }

}
