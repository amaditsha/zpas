package zw.co.esolutions.zpas.integration.camel.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Header;
import org.springframework.beans.factory.annotation.Autowired;
import zw.co.esolutions.zpas.model.HubConfiguration;
import zw.co.esolutions.zpas.repository.HubConfigurationRepository;

import java.util.Optional;

/**
 * Created by mabuza on 09 Jul 2019
 */
@Slf4j
public class HubDynamicRouterBean {


    @Autowired
    HubConfigurationRepository hubConfigurationRepository;

    public String route(String body, @Header("BIC") String bic) {

        final Optional<HubConfiguration> hubConfigurationOptional = hubConfigurationRepository.findById(bic);

        String URI;
        if (hubConfigurationOptional.isPresent()) {
            URI = hubConfigurationOptional.get().getUri();
        } else {
            URI = "NO_CONFIGURED_URL";
        }
        //String URI = GatewayRouteBuilder.CALL_SOURCE_URL + "/" + bic + "?bridgeEndpoint=true";
        log.info("Route uri---> " + URI);
        return  URI;
    }
}
