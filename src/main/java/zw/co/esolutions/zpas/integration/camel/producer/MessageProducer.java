package zw.co.esolutions.zpas.integration.camel.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessageProducer {

    @Value("${hub.req.queue.url}")
    public String hubRequestQueue;

    @Value("${aqs.in.system.queue.url}")
    public String aqsInSystemQueue;

    @Value("${zimra.req.queue.url}")
    public String zimraRequestQueue;

    @Value("${nssa.req.queue.url}")
    public String nssaRequestQueue;

    @Autowired
    CamelContext camelContext;

    public boolean sendMessageToHub(String message, String bic) {
        try {
            //String bic = "FMBZZWHX";
            ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
            final DefaultExchange exchange = new DefaultExchange(camelContext);
            exchange.getIn().setHeader("BIC", bic);
            exchange.getIn().setBody(message);
//        producerTemplate.sendBodyAndProperty(hubRequestQueue, message, "BIC", bic);
            producerTemplate.send(hubRequestQueue, exchange);
            return true;
        } catch (Exception e) {
            log.error("Failed to Send Message to Hub : {}", e.getMessage());
            return false;
        }
    }

    public boolean sendMessageToSystemClient(String message) {
        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        producerTemplate.sendBody(aqsInSystemQueue, message);
        return true;
    }

    public boolean sendMessageToZimra(String message) {
        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        producerTemplate.sendBody(zimraRequestQueue, message);
        return true;
    }

    public boolean sendMessageToNssa(String message) {
        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
        producerTemplate.sendBody(nssaRequestQueue, message);
        return true;
    }

}
