package zw.co.esolutions.zpas.dto.payments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by alfred on 15 Apr 2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentsStatusDTO {
    private long totalPayments;
    private long totalFailedPayments;
    private long totalSuccessfulPayments;

    private long totalBulkPayments;
    private long totalFailedBulkPayments;
    private long totalSuccessfulBulkPayments;

    private long totalIndividualPayments;
    private long totalFailedIndividualPayments;
    private long totalSuccessfulIndividualPayments;
}
