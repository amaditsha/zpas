package zw.co.esolutions.zpas.dto.nssa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;

/**
 * Create by alfred on 19 Jul 2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class MakeNssaPaymentDTO {
    private String sourceAccountId;
    private String clientId;
    private String clientName;
    private String beneficiaryReference;
    private String clientReference;
    private String fileName;
    private double amount;
    private double apwcs;
    private String valueDate;
    private String month;

    public OffsetDateTime getMonthOffsetDateTime() {

        return LocalDate.parse(month + "-15", DateTimeFormatter.ofPattern("yyyy-MM-dd")).atTime(OffsetTime.now());
    }
}
