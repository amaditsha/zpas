package zw.co.esolutions.zpas.dto.agreement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;

import java.time.OffsetDateTime;
import java.util.Optional;


@Data
public class NotificationDTO {
    private String id;
    private String message;
    private boolean isRead;
    private String entityId;
    private String groupId;
    private String clientId;
    private EntityStatus entityStatus;
    private OffsetDateTime dateCreated;

    public NotificationDTO(){}

    @JsonCreator
    public NotificationDTO(
            @JsonProperty("id")String id,
            @JsonProperty("message") String message,
            @JsonProperty("isRead") boolean isRead,
            @JsonProperty("entityId") String entityId,
            @JsonProperty("groupId") String groupId,
            @JsonProperty("clientId") String clientId,
            @JsonProperty("entityStatus") EntityStatus entityStatus,
            @JsonProperty("dateCreated") OffsetDateTime dateCreated){
        this.id = Optional.ofNullable(id).orElseGet(GenerateKey::generateEntityId);
        this.message = message;
        this.isRead = Optional.of(isRead).orElse(false);
        this.entityId = entityId;
        this.groupId = groupId;
        this.clientId = clientId;
        this.entityStatus = entityStatus;
        this.dateCreated = Optional.ofNullable(dateCreated).orElseGet(OffsetDateTime::now);
    }
}
