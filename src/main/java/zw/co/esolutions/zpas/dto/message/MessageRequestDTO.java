package zw.co.esolutions.zpas.dto.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class MessageRequestDTO implements Serializable {
    private String recipientName;
    private String originator;
    private String destination;
    private String messageText;
    private String messageReference;
    private String messageDate;
    @JsonIgnore
    private List<String> attachmentFilePaths;
    @JsonIgnore
    private MessageChannel messageChannel;
    @JsonIgnore
    private String subject;
    @JsonIgnore
    private String username;
    @JsonIgnore
    private String password;
}


