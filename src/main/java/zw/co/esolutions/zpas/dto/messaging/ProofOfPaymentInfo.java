package zw.co.esolutions.zpas.dto.messaging;

import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.model.Payment;

/**
 * Created by alfred on 14 Mar 2019
 */
@Data
@Builder
public class ProofOfPaymentInfo {
    private Payment payment;
    private String paymentReference = "";
    private String utrReference = "";
    private String debtorName = "";
    private String debtorBank = "";
    private String creditorBank = "";
    private String creditorName = "";
    private String creditorAccountNumber = "";
}
