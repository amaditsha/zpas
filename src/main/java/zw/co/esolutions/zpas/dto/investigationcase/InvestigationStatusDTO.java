package zw.co.esolutions.zpas.dto.investigationcase;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvestigationStatusDTO {
    private long totalInvestigations;
    private long totalOpenInvestigations;
    private long totalClosedInvestigations;
}
