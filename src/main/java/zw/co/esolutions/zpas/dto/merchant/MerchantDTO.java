package zw.co.esolutions.zpas.dto.merchant;

import lombok.Data;
import zw.co.esolutions.zpas.model.Party;
import zw.co.esolutions.zpas.model.PartyMerchantAccount;

@Data
public class MerchantDTO {
    protected String name;
    protected String code;
    protected String accountNumber;
    protected String bank;
    protected PartyMerchantAccount partyMerchantAccount;
    protected Party linkedMerchantId;

}
