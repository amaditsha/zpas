package zw.co.esolutions.zpas.dto.party;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 21 Apr 2019
 */
@Data
@Builder(toBuilder = true)
public class VirtualCodeAvailabilityCheckDTO {
    private String accountId;
    private String accountName;
    private String virtualCode;
    private Boolean available;
}
