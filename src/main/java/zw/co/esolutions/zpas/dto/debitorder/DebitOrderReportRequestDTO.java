package zw.co.esolutions.zpas.dto.debitorder;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred 08/08/2018
 */
@Data
@Builder
public class DebitOrderReportRequestDTO {
    private String debitOrderBankId;
    private int debitOrderDueDate;
    private String companyId;
}
