package zw.co.esolutions.zpas.dto.registration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.validation.UniqueIdentityCardNumber;

import javax.validation.constraints.*;
import java.util.List;

@Data
public class ClientCreationDTO {
    private String auditUsername;
    private String id;

    @NotEmpty(message = "gender cannot be empty")
    private String gender;

    @NotEmpty(message = "language cannot be empty")
    private String language;

    //@Past
    @NotEmpty(message = "date of birth cannot be empty")
    private String birthDate;


    private String profession;

    private String nationality;
    private String businessFunctionTitle;
    private String employingParty;
    private String contactPersonRole;
    private String civilStatus;

    private String deathDate;
    private String citizenshipEndDate;
    private String citizenshipStartDate;

    private String addressType;
    private String streetName;
    private String streetBuildingIdentification;
    private String postCodeIdentification;

    //@Min(4)
    @NotEmpty(message = "town name cannot be empty")
    private String townName;

    private String state;
    private String buildingName;
    private String floor;
    private String districtName;
    private String regionIdentification;
    private String countryIdentification;
    private String postOfficeBox;

    @NotEmpty(message = "province cannot be empty")
    private String province;

    private String department;
    private String subDepartment;
    private String suiteIdentification;
    private String buildingIdentification;
    private String mailDeliverySubLocation;
    private String block;
    private String districtSubDivisionIdentification;
    private String lot;

    //@NotEmpty(message = "country cannot be empty")
    private String country;

    private String sourceOfWealth;
    private String salaryRange;
    private String employeeTerminationIndicator;

    private String virtualId;
    private String socialSecurityNumber;
    private String driversLicenseNumber;
    private String alienRegistrationNumber;
    private String passportNumber;
    private String nationalRegistrationNumber;
    private String taxIdentificationNumber;

    @UniqueIdentityCardNumber
    @NotEmpty(message = "ID Card Number cannot be empty")
    private String identityCardNumber;

    private String employerIdentificationNumber;

    @NotEmpty(message = "birth name cannot be empty")
    private String birthName;

    private String namePrefix;

    @NotEmpty(message = "last name cannot be empty")
    private String givenName;
    private String middleName;
    private String nameSuffix;

    private String phonesJson;
    private String addressesJson;
    private String postalAddressesJson;

    private String partyType;
    private String typeOfIdentification;
    private String residentialStatus;


    private String username;
    private List<String> userRoles;
    private String emailAddress;
    private String mobileNumber;

    /*residence postal address*/
    private String residenceAddressType;
    private String residenceStreetName;
    private String residenceStreetBuildingIdentification;
    private String residencePostCodeIdentification;
    private String residenceTownName;
    private String residenceState;
    private String residenceBuildingName;
    private String residenceFloor;
    private String residenceDistrictName;
    private String residencePostOfficeBox;
    private String residenceProvince;
    private String residenceDepartment;
    private String residenceSubDepartment;
    private String residenceSuiteIdentification;
    private String residenceBuildingIdentification;
    private String residenceMailDeliverySubLocation;
    private String residenceBlock;
    private String residenceDistrictSubDivisionIdentification;
    private String residenceLot;
    private String residenceCountryId;

    private String registrationFI;


}
