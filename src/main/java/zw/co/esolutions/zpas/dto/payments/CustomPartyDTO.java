package zw.co.esolutions.zpas.dto.payments;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 18 Mar 2019
 */
@Builder
@Data
public class CustomPartyDTO {
    private String paymentId;
    private String beneficiaryName;
    private String beneficiaryBank;
    private String beneficiaryAccount;
}
