package zw.co.esolutions.zpas.dto.currency;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CurrencyDTO {
    public String id;
    @NotNull
    @Size(min=3, max = 3, message = "Currency code must be at least 3 characters")
    public String code;

    @NotBlank(message = "Currency Name is required")
    public String name;

    //@NotBlank(message = "Currency Name is required")
    public Boolean defaultCurrency;

    public String dateCreated;

    @NotBlank(message = "Start Date must be specified")
    public String startDate;

    @NotBlank(message = "End Date must be specified")
    public String endDate;
}
