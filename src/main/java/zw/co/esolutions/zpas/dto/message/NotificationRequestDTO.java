package zw.co.esolutions.zpas.dto.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.enums.NotificationType;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;

import java.util.List;

@Builder
@Data
public class NotificationRequestDTO {
    protected String recipientName;
    protected String messageText;
    protected String clientId;
    protected NotificationType notificationType;
    protected String originator;
    protected String destination;
    protected String messageReference;
    protected String messageDate;

    @JsonIgnore
    protected List<String> attachmentFilePaths;
    @JsonIgnore
    protected MessageChannel messageChannel;
    @JsonIgnore
    protected String subject;
    @JsonIgnore
    protected String username;
    @JsonIgnore
    protected String password;
}
