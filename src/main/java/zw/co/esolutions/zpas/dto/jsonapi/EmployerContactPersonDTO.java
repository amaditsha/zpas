package zw.co.esolutions.zpas.dto.jsonapi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployerContactPersonDTO {
    private String emailAddress;
    private String name;
}
