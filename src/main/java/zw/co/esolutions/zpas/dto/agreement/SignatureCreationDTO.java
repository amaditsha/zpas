package zw.co.esolutions.zpas.dto.agreement;

import lombok.Data;

@Data
public class SignatureCreationDTO {
    protected String id;
//    Parameters related to the signature provided.
    protected String conditionsId;
//    Status of the document (eg delivered, paid, etc.)
    protected String documentStatus;
//    Specifies the function of the document.
    protected String documentPurpose;
//    Specifies if this document is a copy, a duplicate, or a duplicate of a copy.
    protected String copyDuplicateCode;
//    Specifies the type of the document, for example commercial invoice.
    protected String documentTypeCode;
//    Indication whether the document must be signed or not.
    protected String signedIndicator;
//    Language used for textual information in the document.
    protected String languageCode;
//    Specifies the type of data set in which the document is included.
    protected String dataSetTypeCode;
//    Unambiguous identification of the version of a document.
    protected String documentVersion;
//    Specifies the type of address.
    protected String addressType;
//    Name of a street or thoroughfare.
    protected String streetName;
//    Number that identifies the position of a building on a street.
    protected String streetBuildingIdentification;
//    Name of a built-up area, with defined boundaries, and a local government.
    protected String townName;
//    Organised political community or area forming a part of a federation.
    protected String state;
//    Name of the building or house.
    protected String buildingName;
//    Floor or storey within a building.
    protected String floor;
//    Name of a district, ie, a part of a town or region.
    protected String districtName;
//    A territory governed as an administrative or political unit of a country.
    protected String province;


//    Name by which a country is known. It is normally the name attached to the ISO country code.
    protected String countryName;

}
