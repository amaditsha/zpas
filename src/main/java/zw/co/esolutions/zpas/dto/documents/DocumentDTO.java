package zw.co.esolutions.zpas.dto.documents;

import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;

/**
 * Created by alfred on 11 Mar 2019
 */
@Data
@Builder
public class DocumentDTO {
    private String documentName;
    private String documentPath;
    private String clientId;
    private String type;
    private String copyDuplicate;
    private String issueDate;
    private String purpose;
    private String dataSetType;
    private String languageCode;
    private CurrencyAndAmount remittedAmount;
}
