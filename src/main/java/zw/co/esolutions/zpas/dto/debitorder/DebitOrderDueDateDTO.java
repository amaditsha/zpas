package zw.co.esolutions.zpas.dto.debitorder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.apache.camel.json.simple.Jsonable;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.SystemConstants;

import java.time.OffsetDateTime;
import java.util.Optional;

@Data
@Builder
public class DebitOrderDueDateDTO{
    private String id;
    private int dueDate;
    private String debitOrderBankId;
    private String status;
    private OffsetDateTime dateCreated;

    public DebitOrderDueDateDTO(){}

    @JsonCreator
    public DebitOrderDueDateDTO(
            @JsonProperty("id") String id,
            @JsonProperty("dueDate") int dueDate,
            @JsonProperty("debitOrderBankId") String debitOrderBankId,
            @JsonProperty("status") String status,
            @JsonProperty("dateCreated") OffsetDateTime dateCreated
    ){
        this.id = Optional.ofNullable(id).orElseGet(GenerateKey::generateEntityId);
        this.dueDate = dueDate;
        this.debitOrderBankId = debitOrderBankId;
        this.status = Optional.ofNullable(status).orElseGet(() -> SystemConstants.STATUS_PENDING);
        this.dateCreated = Optional.ofNullable(dateCreated).orElseGet(OffsetDateTime::now);
    }
}
