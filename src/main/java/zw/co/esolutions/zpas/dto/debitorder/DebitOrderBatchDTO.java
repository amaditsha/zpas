package zw.co.esolutions.zpas.dto.debitorder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.enums.DebitOrderStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.MoneyUtil;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * Created by alfred 14/06/2018
 */
@Data
@Builder(toBuilder = true)
public class DebitOrderBatchDTO {
    private String id;
    private OffsetDateTime dateCreated;
    private OffsetDateTime lastUpdated;
    private String status;
    private String companyId;
    private OffsetDateTime debitOrderDueDate;
    private String batchNumber;
    private String description;
    private Long totalAmount;
    private Integer totalCount;
    private String targetAccount;
    private String targetAccountName;
    private Integer successfulTotalCount;
    private Long successfulTotalAmount;
    private String trailerId;
    private String currencyCode;
    private String headerID;
    private OffsetDateTime processingDate;
    private String senderID;
    private String receiverID;
    private String fileID;
    private String workCode;
    private String fileName;
    private String batchResponseCode;
    private DebitOrderStatus debitOrderStatus;
    private String sortCode;
    private String bankName;
    private String branchName;
    private String clientId;
    private String clientName;
    private String version;

    public DebitOrderBatchDTO() {
    }

    public DebitOrderBatchDTO(
            @JsonProperty(value = "id") String id,
            @JsonProperty(value = "dateCreated") OffsetDateTime dateCreated,
            @JsonProperty(value = "lastUpdated") OffsetDateTime lastUpdated,
            @JsonProperty(value = "status") String status,
            @JsonProperty(value = "companyId") String companyId,
            @JsonProperty(value = "debitOrderDueDate") OffsetDateTime debitOrderDueDate,
            @JsonProperty(value = "batchNumber") String batchNumber,
            @JsonProperty(value = "description") String description,
            @JsonProperty(value = "totalAmount") Long totalAmount,
            @JsonProperty(value = "totalCount") Integer totalCount,
            @JsonProperty(value = "targetAccount") String targetAccount,
            @JsonProperty(value = "targetAccountName") String targetAccountName,
            @JsonProperty(value = "successfulTotalCount") Integer successfulTotalCount,
            @JsonProperty(value = "successfulTotalAmount") Long successfulTotalAmount,
            @JsonProperty(value = "trailerId") String trailerId,
            @JsonProperty(value = "currencyCode") String currencyCode,
            @JsonProperty(value = "headerID") String headerID,
            @JsonProperty(value = "processingDate") OffsetDateTime processingDate,
            @JsonProperty(value = "senderID") String senderID,
            @JsonProperty(value = "receiverID") String receiverID,
            @JsonProperty(value = "fileID") String fileID,
            @JsonProperty(value = "workCode") String workCode,
            @JsonProperty(value = "fileName") String fileName,
            @JsonProperty(value = "batchResponseCode") String batchResponseCode,
            @JsonProperty(value = "debitOrderStatus") DebitOrderStatus debitOrderStatus,
            @JsonProperty(value = "sortCode") String sortCode,
            @JsonProperty(value = "bankName") String bankName,
            @JsonProperty(value = "branchName") String branchName,
            @JsonProperty(value = "clientId") String clientId,
            @JsonProperty(value = "clientName") String clientName,
            @JsonProperty(value = "version") String version
    ) {
        this.id = Optional.ofNullable(id).orElseGet(GenerateKey::generateEntityId);
        this.dateCreated = Optional.ofNullable(dateCreated).orElseGet(OffsetDateTime::now);
        this.lastUpdated = Optional.ofNullable(lastUpdated).orElseGet(OffsetDateTime::now);
        this.status = Optional.ofNullable(status).orElse(EntityStatus.ACTIVE.name());
        this.companyId = Preconditions.checkNotNull(companyId, "Company Id should be provided.");
        this.debitOrderDueDate = Preconditions.checkNotNull(debitOrderDueDate, "Debit Order Due Date should be provided.");
        this.batchNumber = Optional.ofNullable(batchNumber).orElse(RefGen.getReference("DOBN"));
        this.description = Optional.ofNullable(description).orElse(getOptionalDescription(this));
        this.totalAmount = Preconditions.checkNotNull(totalAmount, "Total amount should be specified.");
        this.totalCount = Preconditions.checkNotNull(totalCount, "Total count should be specified.");
        this.targetAccount = Preconditions.checkNotNull(targetAccount, "Target Account should be specified.");
        this.targetAccountName = Preconditions.checkNotNull(targetAccountName, "Target account name should be provided.");
        this.successfulTotalCount = Preconditions.checkNotNull(successfulTotalCount, "Successful total count should be provided.");
        this.successfulTotalAmount = Preconditions.checkNotNull(successfulTotalAmount, "Successful total amount should be provided.");
        this.trailerId = Optional.ofNullable(trailerId).orElse("UTL");;
        this.currencyCode = Optional.ofNullable(currencyCode).orElse("ZWL");
        this.headerID = Optional.ofNullable(headerID).orElse("UHL");
        this.processingDate = Optional.ofNullable(processingDate).orElse(OffsetDateTime.now());
        this.senderID = senderID;
        this.fileID = Optional.ofNullable(fileID).orElse(GenerateKey.generateFileId());
        this.workCode = Optional.ofNullable(workCode).orElse(RefGen.getReference("WC"));
        this.fileName = Optional.ofNullable(fileName).orElse(batchNumber);
        this.batchResponseCode = batchResponseCode;
        this.debitOrderStatus = Preconditions.checkNotNull(debitOrderStatus, "Debit Order status should be provided.");
        this.sortCode = Preconditions.checkNotNull(sortCode, "Debit Order Bank Sort Code should be provided.");
        this.bankName = bankName;
        this.branchName = branchName;
        this.clientId = clientId;
        this.clientName = clientName;
        this.receiverID = Optional.ofNullable(receiverID).orElse("");
        this.version = version;
    }

    private String getOptionalDescription(DebitOrderBatchDTO debitOrderBatchDTO) {
        return String.format("Batch number %s with work code %s, created on %s. Total Amount: %s. Total Count: %s. " +
                        "The Batch is going to branch %s of bank %s",
                batchNumber, workCode, dateCreated.format(DateTimeFormatter.ISO_DATE_TIME),
                MoneyUtil.convertCentsToDollarsPattern(totalAmount), totalCount, branchName,
                bankName);
    }
}
