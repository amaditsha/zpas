package zw.co.esolutions.zpas.dto.profile;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Ncube on 11/18/16.
 */
@Builder
@Data
public class PasswordDTO {
    private String profileId;
    private String oldPassword;
    private String username;
    private String newPassword;
    private String confirmPassword;

    public PasswordDTO(){}

    @JsonCreator
    public PasswordDTO(
            @JsonProperty("profileId") String profileId,
            @JsonProperty("oldPassword") String oldPassword,
            @JsonProperty("username") String username,
            @JsonProperty("newPassword") String newPassword,
            @JsonProperty("confirmPassword") String confirmPassword
    ){
        this.profileId = profileId;
        this.oldPassword = oldPassword;
        this.username = username;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
    }
}
