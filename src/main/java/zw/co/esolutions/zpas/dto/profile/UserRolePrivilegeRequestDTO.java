package zw.co.esolutions.zpas.dto.profile;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 24 Mar 2019
 */
@Data
@Builder
public class UserRolePrivilegeRequestDTO {
    private String userRoleId;
    private String privilegeId;
}
