package zw.co.esolutions.zpas.dto.debitorder;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred 08/08/2018
 */
@Data
@Builder
public class DebitOrderReportDTO {
    private String title;
    private String firstNames;
    private String lastName;
    private String homePhone;
    private String workPhone;
    private String mobileNumber;
    private String email;
    private String gender;

    private String startProcessingDate;
    private String endProcessingDate;
    private String startDate;
    private String endDate;

    public DebitOrderReportDTO(String title, String firstNames, String lastName, String homePhone, String workPhone,
                               String mobileNumber, String email, String gender, String startProcessingDate,
                               String endProcessingDate, String startDate, String endDate) {
        this.title = title;
        this.firstNames = firstNames;
        this.lastName = lastName;
        this.homePhone = homePhone;
        this.workPhone = workPhone;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.gender = gender;
        this.startProcessingDate = startProcessingDate;
        this.endProcessingDate = endProcessingDate;
        this.startDate = startDate;
        this.endDate = endDate;
    }


    @Override
    public String toString() {
        return "DebitOrderReportDTO{" +
                "startProcessingDate='" + startProcessingDate + '\'' +
                ", endProcessingDate=" + endProcessingDate +
                ", startDate='" + startDate + '\'' +
                ", endDate=" + endDate +
                '}';
    }

    /**
     * The payment status based on the current month. If this contribution is for this month, then the user has paid
     */

    public String getFullName() {
        String presentationTitle = title.substring(0, 1).toUpperCase() + title.substring(1);
        String presentationFirstNames = firstNames.substring(0, 1).toUpperCase() + firstNames.substring(1);
        String presentationLastName = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
        return String.format("%s %s %s", presentationTitle, presentationFirstNames, presentationLastName);
    }
}
