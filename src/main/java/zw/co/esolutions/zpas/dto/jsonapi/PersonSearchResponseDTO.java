package zw.co.esolutions.zpas.dto.jsonapi;

import lombok.Builder;
import lombok.Data;

import java.util.Objects;

/**
 * Created by alfred on 20 Mar 2019
 */
@Data
@Builder(toBuilder = true)
public class PersonSearchResponseDTO {
    private String personFullName;
    private String personId;
    private String organisationId;
    private String organisationType;

    private String firstNames;
    private String lastName;
    private String email;
    private String mobileNumber;
    private String phoneNumber;
    private String nationalId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonSearchResponseDTO that = (PersonSearchResponseDTO) o;
        return personId.equals(that.personId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personId);
    }
}
