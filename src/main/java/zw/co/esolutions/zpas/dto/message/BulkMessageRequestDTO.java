package zw.co.esolutions.zpas.dto.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import zw.co.esolutions.zpas.utilities.enums.MessageChannel;

import java.io.Serializable;
import java.util.List;

@Data
public class BulkMessageRequestDTO implements Serializable {
    private String batchNumber;
    @JsonIgnore
    private MessageChannel messageChannel;
    @JsonIgnore
    private String originator;
    @JsonIgnore
    private String messageText;
    @JsonIgnore
    private String username;
    @JsonIgnore
    private String password;
    private List<SingleMessageRequestDTO> messages;
}


