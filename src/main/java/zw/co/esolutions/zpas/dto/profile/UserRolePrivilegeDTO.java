package zw.co.esolutions.zpas.dto.profile;

import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.utilities.enums.RoleLevel;

import java.util.Objects;

/**
 * Created by alfred on 24 Mar 2019
 */
@Data
@Builder(toBuilder = true)
public class UserRolePrivilegeDTO {
    private String roleId;
    private String roleName;
    private RoleLevel roleLevel;

    private String privilegeId;
    private String privilegeName;
    private boolean privilegeEnabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRolePrivilegeDTO that = (UserRolePrivilegeDTO) o;
        return roleId.equals(that.roleId) &&
                roleName.equals(that.roleName) &&
                roleLevel == that.roleLevel &&
                privilegeId.equals(that.privilegeId) &&
                privilegeName.equals(that.privilegeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, roleName, roleLevel, privilegeId, privilegeName);
    }
}
