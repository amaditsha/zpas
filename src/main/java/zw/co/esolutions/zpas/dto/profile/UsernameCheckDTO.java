package zw.co.esolutions.zpas.dto.profile;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 08 May 2019
 */
@Data
@Builder
public class UsernameCheckDTO {
    private String username;
    private String extraInformation;
    private boolean available;
}
