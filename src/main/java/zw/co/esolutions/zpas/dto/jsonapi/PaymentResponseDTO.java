package zw.co.esolutions.zpas.dto.jsonapi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.enums.PaymentStatusCode;

/**
 * Created by alfred on 30 July 2019
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentResponseDTO {
    private String paymentId;
    private String endToEndId;
    private String responseStatus;
    private PaymentStatusCode paymentStatus;
}
