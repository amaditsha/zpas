package zw.co.esolutions.zpas.dto.nssa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContributionScheduleDTO {
    protected String id;
    protected EntityStatus entityStatus;
    protected OffsetDateTime dateCreated;
    protected OffsetDateTime lastUpdated;

    protected OffsetDateTime month;
    protected CurrencyAndAmount amount;
    protected AccountDTO account;

    protected String paymentReference;
    protected String receiptNumber;
    protected String receiptFileName;
    protected String paymentId;
    protected String clientReference;
    protected String additionalInfo;
    protected String destinationBank;
    protected EmployerDTO employer;
    protected double pobs;
    protected double apwcs;
    protected List<ContributionScheduleItemDTO> contributionScheduleItems;
}
