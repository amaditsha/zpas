package zw.co.esolutions.zpas.dto.messaging;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountStatementInfo {
    private OffsetDateTime bookingDate;
    private OffsetDateTime valueDate;
    private BigDecimal txnAmount;
    private String txnReference; //can't find in the message
    private String debitCreditIndicator;
    private String currency;
    private String txnStatus;
    private String txnNarrative; //can't find message

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountStatementInfo that = (AccountStatementInfo) o;
        return Objects.equals(txnReference, that.txnReference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(txnReference);
    }
}
