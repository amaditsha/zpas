package zw.co.esolutions.zpas.dto.messaging;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by alfred on 14 Mar 2019
 */
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProofOfPaymentDTO {
    private String clientId;
    private String paymentId;
    private String email;
    private String recipientName;
    private String status;
    private String message;
}
