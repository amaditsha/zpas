package zw.co.esolutions.zpas.dto.agreement;

import lombok.Data;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.time.OffsetDateTime;

@Data
public class SigningRuleDTO {
    protected String id;
    protected String numberOfApprovers;
    protected String minAmount;
    protected String maxAmount;
    protected String activityName;
    protected String entityType;
    protected String entityId;
    protected String startDate;
    protected String endDate;
}
