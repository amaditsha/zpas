package zw.co.esolutions.zpas.dto.investigationcase;

import lombok.Data;
import zw.co.esolutions.zpas.enums.*;

import java.util.Optional;

@Data
public class InvestigationCaseStatusDTO {
    private String caseStatusCode;
    private String paymentInvestigationCaseId;
    private String narrative;
    private String rejectedStatusReasonCode;
    private String failingReasonCode;
    private String rejectionReasonV2Code;
    private String cancellationProcessingStatusCode;
    private String instructionProcessingStatus;
    private String statusCode;

    public InvestigationCaseStatusDTO(String caseStatusCode,
                                      String paymentInvestigationCaseId,
                                      String narrative,
                                      String rejectedStatusReasonCode,
                                      String failingReasonCode,
                                      String rejectionReasonV2Code,
                                      String cancellationProcessingStatusCode,
                                      String instructionProcessingStatus,
                                      String statusCode) {
        this.caseStatusCode = Optional.ofNullable(caseStatusCode).orElse(CaseStatusCode.Unknown.name());
        this.paymentInvestigationCaseId = paymentInvestigationCaseId;
        this.narrative = narrative;
        this.rejectedStatusReasonCode = Optional.ofNullable(rejectedStatusReasonCode).orElse(RejectedStatusReasonCode.None.name());
        this.failingReasonCode = Optional.ofNullable(failingReasonCode).orElse(PendingFailingReasonCode.None.name());
        this.rejectionReasonV2Code = Optional.ofNullable(rejectionReasonV2Code).orElse(RejectionReasonV2Code.None.name());
        this.cancellationProcessingStatusCode = Optional.ofNullable(cancellationProcessingStatusCode).orElse(CancellationProcessingStatusCode.None.name());
        this.instructionProcessingStatus = Optional.ofNullable(instructionProcessingStatus).orElse(InstructionProcessingStatusCode.None.name());
        this.statusCode = Optional.ofNullable(statusCode).orElse(StatusCode.None.name());
    }
}