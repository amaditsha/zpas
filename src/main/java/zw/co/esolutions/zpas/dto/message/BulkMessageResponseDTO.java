package zw.co.esolutions.zpas.dto.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Data
public class BulkMessageResponseDTO implements Serializable {
    private String batchNumber;
    private String batchId;
    private String status;
    private String narrative;
    @JsonIgnore
    private HttpStatus responseCode;
    @JsonIgnore
    private String responseText;
}


