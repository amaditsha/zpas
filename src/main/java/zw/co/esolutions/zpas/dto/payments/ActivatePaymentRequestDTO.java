package zw.co.esolutions.zpas.dto.payments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by alfred on 26 Apr 2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActivatePaymentRequestDTO {
    private String sourceFinancialInstitutionId;//
    private String destinationAccountId;//
    private String clientId;//
    private String clientName;//
    private String destinationFinancialInstitutionId;//
    private String sourceAccountNumber;//
    private String debtorId;//
    private String debtorName;//
    private String nationalId;
    private String passportNumber;
    private String currencyName;
    private String currencyCode;
    private String purpose;//
    private double amount;//
//    private String valueDate;//
    private String dueDate;//
//    private String paymentType;//
//    private String paymentInstrument;//
//    private String priority;//
    private String address;
}
