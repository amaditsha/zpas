package zw.co.esolutions.zpas.dto.payments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.utilities.enums.SpecialPayee;

/**
 * Created by alfred on 12 Mar 2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MakePaymentDTO {
    private String sourceFinancialInstitutionId;
    private String sourceAccountId;
    private String clientId;
    private String clientName;
    private String destinationFinancialInstitutionId;
    private String contributionScheduleId;
    private String destinationAccountNumber;
    private String beneficiaryName;
    private String nationalId;
    private String passportNumber;
    private String currencyName;
    private String currencyCode;
    private String purpose;
    private String beneficiaryReference;
    private String clientReference;
    private double amount;
    private String valueDate;
    private String paymentType;
    private String paymentInstrument;
    private String priority;
    private String address;
    private String specialPayee;

    private String makePaymentRequestType;

    private String taxAuthorityId;
    private String taxIdentificationNumber;
    private String businessPartnerNumber;
    private String zimraAssessmentNumber;
    private String zimraOfficeCode;
    private String merchantId;
    private String taxTypeCode;
    private String taxAuthoritySubOfficeCode;
    private String merchantAccountNumber;
}
