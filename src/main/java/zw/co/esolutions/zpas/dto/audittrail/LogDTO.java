package zw.co.esolutions.zpas.dto.audittrail;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import lombok.Builder;
import lombok.Data;

import java.util.Optional;

/**
 * Created by Ncube on 24/05/16.
 */
@Data
@Builder
public class LogDTO{
    private String userName;
    private String activityName;
    private String entityId;
    private String entityName;
    private String oldObject;
    private String newObject;
    private String instanceName;
    private String narrative;

    public LogDTO(){}

    public LogDTO(
            @JsonProperty("userName") String userName,
            @JsonProperty("activityName") String activityName,
            @JsonProperty("entityId")String entityId,
            @JsonProperty("entityName")String entityName,
            @JsonProperty("oldObject")String oldObject,
            @JsonProperty("newObject")String newObject,
            @JsonProperty("instanceName")String instanceName,
            @JsonProperty("narrative")String narrative){
        this.userName = Preconditions.checkNotNull(userName, "username");
        this.activityName = Preconditions.checkNotNull(activityName, "activityName");
        this.entityId = Optional.ofNullable(entityId).orElse(null);
        this.entityName = Optional.ofNullable(entityName).orElse(null);
        this.oldObject = Optional.ofNullable(oldObject).orElse(null);
        this.newObject = Optional.ofNullable(newObject).orElse(null);
        this.instanceName = Optional.ofNullable(instanceName).orElse(null);
        this.narrative = Optional.ofNullable(narrative).orElse(null);
    }
}
