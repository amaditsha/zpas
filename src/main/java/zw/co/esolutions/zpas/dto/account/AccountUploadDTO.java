package zw.co.esolutions.zpas.dto.account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by anotida on 26 Feb 2019
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class AccountUploadDTO {
    private String financialInstitutionId;
    private String username;
    private String clientId;
    private String purpose;
    private String valueDate;
    private String fileName;
    private String uploadFileId;
}
