package zw.co.esolutions.zpas.dto.nssa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.codeset.CurrencyCode;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO {
    protected String id;
    protected EntityStatus entityStatus;
    protected OffsetDateTime dateCreated;
    protected OffsetDateTime lastUpdated;

    protected String number;
    protected String name;
    protected CurrencyCode baseCurrency;
    protected EmployerDTO accountOwner;
    protected List<ContributionScheduleDTO> contributionSchedules;
}
