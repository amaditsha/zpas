package zw.co.esolutions.zpas.dto.tax;

import lombok.Data;

@Data
public class TaxAuthorityDTO {
    protected String id;
    protected String name;
    protected String code;
    protected String accountNumber;
    protected String bank;
    protected String linkedPartyTaxAuthority;

}
