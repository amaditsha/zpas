package zw.co.esolutions.zpas.dto.agreement;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class MandateRegistrationDTO {
    protected String id;
    protected String entityStatus;
//    Unique and unambiguous identification of the mandate.
    protected String mandateIdentification;
//    Specifies the number of days the mandate must be tracked.
    protected String trackingDays;
//    Specifies whether the direct debit instructions should be automatically re-submitted periodically when bilaterally agreed.
    protected String trackingIndicator;
//    Pre-agreed increase or decrease rate that will be applied to the collection amount.
    protected BigDecimal rate;
//    Full name of an agreement, annexes and amendments in place between the principals.
    protected String description;
//    Version number of a contract or of a  legal agreement.
    protected String version;

    protected String fromDateTime;

    protected String toDateTime;
//    Date on which the agreement was signed by all parties.
    protected String dateSigned;

    protected String originalMandateId;
    protected String amendment;
    protected List<String> mandateStatuses;
    protected String accountContractId;

    protected Boolean accepted;
    protected String instructionProcessingStatus;
    protected String settlementStatus;
    protected String cancellationProcessingStatus;
    protected String transactionProcessingStatus;
    protected String modificationProcessingStatus;

    protected List<String> signatureConditionIds;

    protected List<String> mandateHolderIds;
    protected String mandateIssuerId;

    protected String statusDescription;


    protected String cashAccountId;
//    Date on which the account and related services are expected to cease to be operational for the account owner.
    protected String targetClosingDate;
//    Indicator that the change to the contract needs to be treated urgently.
    protected String urgencyFlag;
//    Indicates removal of the account. After removal, an account will not appear anymore in reports.
    protected String removalIndicator;
//    Date on which the account and related services are expected to cease/to be operational for the account owner.
    protected String targetGoLiveDate;
//    Date of the request.
    protected String requestDate;

    protected String accountContractFromDate;
    protected String accountContractToDate;


    //    Full name of an agreement, annexes and amendments in place between the principals.
    protected String accountContractDescription;
    //    Version number of a contract or of a  legal agreement.
    protected String accountContractVersion;
//   Specifies the means by which the account owner submits the open account form.
    protected String transactionChannelCode;

    //    Number of account owners or related parties required to authorise transactions on the account.
    protected String requiredSignatureNumber;
    //    Indicates whether the signature of the account owner is required to authorise transactions on the account.
    protected String signatoryRightIndicator;
    //    Indicator whether a certain order of signatures has to be respected or not.
    protected String signatureOrderIndicator;
    //    Indicates the order in which the mandate holders are allowed to sign.
    protected String signatureOrder;
    protected String signatureDescription;

    protected Map<String, String> mandateHolderRoles;
    protected List<String> mandateHolderRolesTemp;

    public Map<String,String> getMandateHolderRolesMap() {
        Map<String, String> mandateHolderRoles = new HashMap<>();
        mandateHolderRolesTemp.forEach(roleTemp -> {
            List<String> role = Arrays.asList(roleTemp.split("\\|"));
            mandateHolderRoles.put(role.get(0), role.get(1));
        });
        return mandateHolderRoles;
    }
}

