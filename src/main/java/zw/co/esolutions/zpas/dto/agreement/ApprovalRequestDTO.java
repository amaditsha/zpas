package zw.co.esolutions.zpas.dto.agreement;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.utilities.enums.EntityType;

@Data
@Builder
public class ApprovalRequestDTO {
    private String entityId;
    private EntityType entityType;
    private long amount;
    private String profileId;
    private String clientId;


    private String personId;
    private String cashAccountId;
    private String paymentId;

    public ApprovalRequestDTO() {
    }

    public ApprovalRequestDTO(@JsonProperty(value = "entityId") String entityId,
                               @JsonProperty(value = "entityType") EntityType entityType,
                               @JsonProperty(value = "amount") long amount,
                                   @JsonProperty(value = "personId") String profileId,
                              @JsonProperty(value = "clientId") String clientId,
                              @JsonProperty(value = "personId") String personId,
                              @JsonProperty(value = "cashAccountId") String cashAccountId,
                              @JsonProperty(value = "paymentId") String paymentId) {
        this.entityId = entityId;
        this.entityType = entityType;
        this.amount = amount;
        this.profileId = profileId;
        this.clientId = clientId;
        this.personId = personId;
        this.cashAccountId = cashAccountId;
        this.paymentId = paymentId;
    }
}
