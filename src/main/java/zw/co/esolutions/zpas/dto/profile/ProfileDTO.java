package zw.co.esolutions.zpas.dto.profile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.security.model.Profile;
import zw.co.esolutions.zpas.security.model.UserRole;
import zw.co.esolutions.zpas.utilities.enums.ProfileType;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mabuza on 07 Mar 2019
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileDTO {
    private String firstNames;
    private String lastName;
    private String email;
//    private String username;
    private String personId;
    private String financialInstitutionId;
    private String profileLevel;
    private String status;
    private String dateCreated;
    private boolean enabled;
    private boolean locked;
    private String mobileNumber;
    private String phoneNumber;
    private List<String> userRoles;

    public ProfileType getProfileType() {
        try {
            return ProfileType.valueOf(profileLevel);
        } catch (RuntimeException re) {
            return ProfileType.CLIENT;
        }
    }
}
