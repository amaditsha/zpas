package zw.co.esolutions.zpas.dto.jsonapi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by alfred on 12 Mar 2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MakePaymentApiRequestDTO {
    private String sourceAccountNumber;
    private String clientSsr;
    private String clientName;
    private String destinationAccountNumber;
    private String contributionScheduleId;
    private String beneficiaryName;
    private String purpose;
    private String beneficiaryReference;
    private String clientReference;
    private double amount;
    private String valueDate;
    private String paymentType;
    private String paymentInstrument;
    private String priority;
    private String address;

    private String makePaymentRequestType;
}
