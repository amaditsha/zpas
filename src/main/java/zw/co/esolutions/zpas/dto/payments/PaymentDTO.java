package zw.co.esolutions.zpas.dto.payments;

import lombok.Builder;
import lombok.Data;

import java.time.OffsetDateTime;


@Data
@Builder(toBuilder = true)
public class PaymentDTO {
    private String id;
    private OffsetDateTime valueDate;
    private String beneficiaryName;
    private String beneficiaryBank;
}
