package zw.co.esolutions.zpas.dto.messaging;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class AccountStatementDTO {
    private String recipient;
    private String emailAddress;
    private String clientId;

    private BigDecimal openingBalance;
    private String opbdDbtCrdtIndicator;
    private BigDecimal closingBalance;
    private String clbdDbtCrdtIndicator;
    private String accountNumber;
    private String accountName;
    private String accountServicer;
    private String accountCurrency;
    private List<AccountStatementInfo> accountStatementInfoList;
}
