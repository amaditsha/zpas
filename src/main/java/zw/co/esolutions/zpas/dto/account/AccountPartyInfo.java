package zw.co.esolutions.zpas.dto.account;

import lombok.Data;

@Data
public class AccountPartyInfo {
    protected String partyType;
    protected String partyRoleId;
}
