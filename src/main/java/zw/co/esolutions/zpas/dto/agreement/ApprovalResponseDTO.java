package zw.co.esolutions.zpas.dto.agreement;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.utilities.enums.EntityType;

import java.util.Map;

@Data
@Builder
public class ApprovalResponseDTO {
    private String entityId;
    private EntityType entityType;
    private String roleId;
    private int remaining;
    private boolean done;
    private String narrative;
    private Map<Integer, String> remainingRoleOrderings;

    public ApprovalResponseDTO() {
    }

    public ApprovalResponseDTO(@JsonProperty(value = "entityId") String entityId,
                               @JsonProperty(value = "entityType") EntityType entityType,
                               @JsonProperty(value = "roleId") String roleId,
                               @JsonProperty(value = "remaining") int remaining,
                               @JsonProperty(value = "done") boolean done,
                               @JsonProperty(value = "narrative") String narrative,
                               @JsonProperty(value = "remainingRoleOrderings") Map<Integer, String> remainingRoleOrderings) {
        this.entityId = entityId;
        this.entityType = entityType;
        this.roleId = roleId;
        this.remaining = remaining;
        this.done = done;
        this.narrative = narrative;
        this.remainingRoleOrderings = remainingRoleOrderings;
    }
}
