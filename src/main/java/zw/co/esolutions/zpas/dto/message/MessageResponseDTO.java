package zw.co.esolutions.zpas.dto.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageResponseDTO implements Serializable {
    private String originator;
    private String destination;
    private String messageText;
    private String messageId;
    private String messageReference;
    private String status;
    private String messageDate;
    private HttpStatus responseCode;
    private String responseText;
}
