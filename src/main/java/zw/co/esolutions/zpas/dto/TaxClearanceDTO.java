package zw.co.esolutions.zpas.dto;

import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.model.NonFinancialInstitution;

import java.time.OffsetDateTime;

/**
 * Created by alfred on 22 May 2019
 */
@Data
@Builder
public class TaxClearanceDTO {
    private String taxClearanceId;
    private String bpn;
    private OffsetDateTime expiryDate;
    private String companyName;
    private NonFinancialInstitution nonFinancialInstitution;
}
