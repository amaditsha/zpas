package zw.co.esolutions.zpas.dto.registration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployingPartyRolePersonRequestDTO {
    private String employingPartyRoleId;
    private String personId;
}
