package zw.co.esolutions.zpas.dto.tax;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaxAuthorityPaymentOfficeDTO {
    protected String taxAuthorityId;
    protected String name;
}
