package zw.co.esolutions.zpas.dto.agreement;

import lombok.Data;

@Data
public class SignatureConditionDTO {
    protected String id;
//    Number of account owners or related parties required to authorise transactions on the account.
    protected String requiredSignatureNumber;
//    Indicates whether the signature of the account owner is required to authorise transactions on the account.
    protected String signatoryRightIndicator;
    protected String mandateId;
//    Indicator whether a certain order of signatures has to be respected or not.
    protected String signatureOrderIndicator;
//    Indicates the order in which the mandate holders are allowed to sign.
    protected String signatureOrder;
    protected String description;
}
