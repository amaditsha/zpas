package zw.co.esolutions.zpas.dto.merchant;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PartyMerchantAccountDTO {
    protected String id;
    protected String clientId;
    protected String accountNumber;
    protected String identification;
    protected String linkedPartyId;
    protected String merchantId;
}
