package zw.co.esolutions.zpas.dto.registration;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmployerDetailsDTO {
    private String id;
    private String name;
    private String search;
}
