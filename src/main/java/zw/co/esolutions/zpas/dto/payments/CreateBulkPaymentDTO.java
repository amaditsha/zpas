package zw.co.esolutions.zpas.dto.payments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.services.impl.payments.PaymentEntry;

import java.util.List;

/**
 * Created by jnkomo on 12 Mar 2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class CreateBulkPaymentDTO {
    private String bulkPaymentId;
    private PaymentUploadDTO paymentUploadDTO;
    private List<PaymentEntry> payments;
}
