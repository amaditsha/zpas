package zw.co.esolutions.zpas.dto.payments;

import lombok.Data;

@Data
public class RequestInfo {
    protected String id;
    protected String reason;
    protected String requestType;
    protected String clientId;
    protected Object other;
}
