package zw.co.esolutions.zpas.dto.debitorder;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Created by alfred 03/07/2018
 */
@Data
@Builder
public class DebitOrderBatchAndItemsResponseDTO {
    private DebitOrderBatchRequestDTO debitOrderBatchRequestDTO;
    private DebitOrderBatchDTO debitOrderBatchDTO;
    private List<DebitOrderBatchItemDTO> batchItemDTOS;

    public DebitOrderBatchAndItemsResponseDTO() {
    }

    public DebitOrderBatchAndItemsResponseDTO(
            @JsonProperty("debitOrderBatchRequestDTO") DebitOrderBatchRequestDTO debitOrderBatchRequestDTO,
            @JsonProperty("debitOrderBatchDTO") DebitOrderBatchDTO debitOrderBatchDTO,
            @JsonProperty("batchItemDTOS") List<DebitOrderBatchItemDTO> batchItemDTOS) {
        this.debitOrderBatchRequestDTO = debitOrderBatchRequestDTO;
        this.debitOrderBatchDTO = debitOrderBatchDTO;
        this.batchItemDTOS = batchItemDTOS;
    }
}
