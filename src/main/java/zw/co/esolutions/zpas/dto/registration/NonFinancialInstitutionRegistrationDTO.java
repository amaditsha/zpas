package zw.co.esolutions.zpas.dto.registration;

import lombok.Data;

import java.util.List;

@Data
public class NonFinancialInstitutionRegistrationDTO {
    private String id;
    private String purpose;
    private String enableDebitOrder;
    private String registrationDay;
    private String parentOrganisationId;
    private String description;
    private String establishmentDate;
    private String roles;
    private String bicFIIdentifier;
    private String anyBICIdentifier;
    private String bICNonFI;
    private String virtualId;
    private String legalName;
    private String tradingName;
    private String shortName;
    private String phonesJson;
    private String addressesJson;
    private List<String> otherBIC;
    private String ssr;
    private String nssaBusinessPartnerNumber;

    //place of operation
    private String placeOfOperationAddressType;
    private String placeOfOperationStreetName;
    private String placeOfOperationStreetBuildingIdentification;
    private String placeOfOperationPostCodeIdentification;
    private String placeOfOperationTownName;
    private String placeOfOperationState;
    private String placeOfOperationBuildingName;
    private String placeOfOperationFloor;
    private String placeOfOperationDistrictName;
    private String placeOfOperationPostOfficeBox;
    private String placeOfOperationProvince;
    private String placeOfOperationDepartment;
    private String placeOfOperationSubDepartment;
    private String placeOfOperationSuiteIdentification;
    private String placeOfOperationBuildingIdentification;
    private String placeOfOperationMailDeliverySubLocation;
    private String placeOfOperationBlock;
    private String placeOfOperationDistrictSubDivisionIdentification;
    private String placeOfOperationLot;
    private String placeOfOperationCountryId;
    //End place of operation

    private String plcOfRegAddressType;
    private String streetName;
    private String streetBuildingIdentification;
    private String postCodeIdentification;
    private String townName;
    private String state;
    private String buildingName;
    private String floor;
    private String districtName;
    private String postOfficeBox;
    private String province;
    private String department;
    private String subDepartment;
    private String suiteIdentification;
    private String buildingIdentification;
    private String mailDeliverySubLocation;
    private String block;
    private String districtSubDivisionIdentification;
    private String lot;
    private String plcOfRegCountryId;
    private String taxConditionsId;
    private String validityPeriodStartDate;
    private String validityPeriodEndDate;

    //Tax fields
    protected String taxIdentificationNumber;
}
