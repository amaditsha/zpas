package zw.co.esolutions.zpas.dto.investigationcase;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentInvestigationCaseDTO {
    private String paymentId;
    private String cancellationReason;
    private Boolean missingCoverIndication;
    private String incorrectInformationReason;
    private String missingInformationReason;
    private String caseType;
    private String assignmentIdentification;
    private String creationDateTime;
    private String identification;
    private String reassignment;
    private String underlyingInstructionId;

}
