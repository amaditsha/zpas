package zw.co.esolutions.zpas.dto.profile;

import lombok.Data;

/**
 * Created by mabuza on 11 Mar 2019
 */
@Data
public class UserRoleDTO {
    private String name;
    private String entityStatus;
    private String roleLevel;
    private String dateCreated;
}
