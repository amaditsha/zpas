package zw.co.esolutions.zpas.dto.nssa;

import lombok.Data;
import zw.co.esolutions.zpas.datatype.CurrencyAndAmount;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.time.OffsetDateTime;

@Data
public class ContributionScheduleItemDTO {
    protected String id;
    protected EntityStatus entityStatus;
    protected OffsetDateTime dateCreated;
    protected OffsetDateTime lastUpdated;

    protected CurrencyAndAmount amount;
    protected ContributionScheduleDTO contributionSchedule;
    protected EmployeeDTO employee;
    protected String name;
    protected String nationalId;
    protected String ssn;
    protected double pobs;
}
