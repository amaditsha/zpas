package zw.co.esolutions.zpas.dto.operator;

import lombok.Builder;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@Builder(toBuilder = true)
public class OperatorDTO {
    public String id;
    public String name;
    public String code;
    public boolean licenceEnabled;
    public String settlementAccount;
    public String contactPhone;
    public String emailAddress;
    public String status;
    public OffsetDateTime dateCreated;
}
