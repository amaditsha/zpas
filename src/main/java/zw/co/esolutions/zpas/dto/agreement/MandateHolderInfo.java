package zw.co.esolutions.zpas.dto.agreement;

import lombok.Data;

@Data
public class MandateHolderInfo {
    protected String mandateHolderType;
    protected String rolePlayerId;
    protected String cashAccountMandateId;
    protected String cashAccountId;
}
