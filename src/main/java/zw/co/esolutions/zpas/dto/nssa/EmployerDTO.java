package zw.co.esolutions.zpas.dto.nssa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployerDTO {
    private String id;
    private String ssr;
    private String bic;
    private String emailAddress;
    private String telephoneNumber;
    private String anyBIC;
    private String name;
    private String physicalAddress;
    private String businessPartnerNumber;
    private List<EmployeeDTO> employees;
}
