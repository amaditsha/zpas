package zw.co.esolutions.zpas.dto.party;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 21 Apr 2019
 */
@Data
@Builder(toBuilder = true)
public class VirtualIdAvailabilityCheckDTO {
    private String partyId;
    private String partyName;
    private String virtualId;
    private Boolean available;
}
