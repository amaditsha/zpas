package zw.co.esolutions.zpas.dto.debitorder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.enums.DebitOrderStatus;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;
import zw.co.esolutions.zpas.utilities.util.GenerateKey;
import zw.co.esolutions.zpas.utilities.util.RefGen;

import java.time.OffsetDateTime;
import java.util.Optional;

/**
 * Created by alfred 14/06/2018
 */
@Data
@Builder
public class DebitOrderBatchItemDTO {
    private String id;
    private OffsetDateTime dateCreated;
    private OffsetDateTime lastUpdated;
    private String status;
    private String companyId;
    private String sourceAccount;
    private Long amount;
    private DebitOrderStatus itemStatus;
    private String reference;
    private String responseCode;
    private String transactionReference;

    private String recordType;
    private String destinationSortCode;
    private String destinationAccount;
    private String destinationAccountType;
    private String originSortCode;
    private String originAccount;
    private String originAccountType;
    private String referenceID;
    private String destinationName;
    private OffsetDateTime processingDate;
    private String unpaidReason;
    private String narrative;
    private String amountCurrencyCode;
    private String originCurrencyRate ;
    private String response;
    private String requestStatus;

    private DebitOrderBatchDTO debitOrderBatchDTO;


    public DebitOrderBatchItemDTO() {
    }

    public DebitOrderBatchItemDTO(
            @JsonProperty(value = "id") String id,
            @JsonProperty(value = "dateCreated") OffsetDateTime dateCreated,
            @JsonProperty(value = "lastUpdated") OffsetDateTime lastUpdated,
            @JsonProperty(value = "status") String status,
            @JsonProperty(value = "companyId") String companyId,
            @JsonProperty(value = "sourceAccount") String sourceAccount,
            @JsonProperty(value = "amount") Long amount,
            @JsonProperty(value = "itemStatus") DebitOrderStatus itemStatus,
            @JsonProperty(value = "reference") String reference,
            @JsonProperty(value = "responseCode") String responseCode,
            @JsonProperty(value = "transactionReference") String transactionReference,
            @JsonProperty(value = "recordType") String recordType,
            @JsonProperty(value = "destinationSortCode") String destinationSortCode,
            @JsonProperty(value = "destinationAccount") String destinationAccount,
            @JsonProperty(value = "destinationAccountType") String destinationAccountType,
            @JsonProperty(value = "originSortCode") String originSortCode,
            @JsonProperty(value = "originAccount") String originAccount,
            @JsonProperty(value = "originAccountType") String originAccountType,
            @JsonProperty(value = "referenceID") String referenceID,
            @JsonProperty(value = "destinationName") String destinationName,
            @JsonProperty(value = "processingDate") OffsetDateTime processingDate,
            @JsonProperty(value = "unpaidReason") String unpaidReason,
            @JsonProperty(value = "narrative") String narrative,
            @JsonProperty(value = "amountCurrencyCode") String amountCurrencyCode,
            @JsonProperty(value = "originCurrencyRate") String originCurrencyRate,
            @JsonProperty(value = "response") String response,
            @JsonProperty(value = "requestStatus") String requestStatus,
            @JsonProperty(value = "debitOrderBatchDTO") DebitOrderBatchDTO debitOrderBatchDTO
    ) {
        this.id = Optional.ofNullable(id).orElseGet(GenerateKey::generateEntityId);
        this.dateCreated = Optional.ofNullable(dateCreated).orElseGet(OffsetDateTime::now);
        this.lastUpdated = Optional.ofNullable(lastUpdated).orElseGet(OffsetDateTime::now);
        this.status = Optional.ofNullable(status).orElse(EntityStatus.PENDING_APPROVAL.name());
        this.companyId = Preconditions.checkNotNull(companyId, "Company Id should be provided.");
        this.sourceAccount = Preconditions.checkNotNull(sourceAccount, "Source account should be provided.");
        this.amount = Optional.ofNullable(amount).orElse(0L);
        this.itemStatus = Preconditions.checkNotNull(itemStatus, "Item status should be provided.");
        this.reference = Optional.ofNullable(reference).orElse(RefGen.getReference("DOI"));
        this.responseCode = responseCode;
        this.transactionReference = transactionReference;
        this.recordType = recordType;
        this.destinationSortCode = destinationSortCode;
        this.destinationAccount = destinationAccount;
        this.destinationAccountType = destinationAccountType;
        this.originSortCode = originSortCode;
        this.originAccount = originAccount;
        this.originAccountType = originAccountType;
        this.referenceID = referenceID;
        this.destinationName = destinationName;
        this.processingDate = processingDate;
        this.unpaidReason = unpaidReason;
        this.narrative = narrative;
        this.amountCurrencyCode = amountCurrencyCode;
        this.originCurrencyRate = originCurrencyRate;
        this.response = response;
        this.requestStatus = requestStatus;
        this.debitOrderBatchDTO = Preconditions.checkNotNull(debitOrderBatchDTO, "Debit Order Batch should be specified for every item.");
    }
}
