package zw.co.esolutions.zpas.dto.account;

import com.opencsv.bean.CsvBindByName;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Builder
public class CashAccountEntry {
    @CsvBindByName(column = "RegistrationNumber", required = true)
    private String registrationNumber;
    @CsvBindByName(column = "Name", required = true)
    private String name;
    @CsvBindByName(column = "BBAN", required = true)
    private String bBAN;
    @CsvBindByName(column = "UPIC", required = true)
    private String uPIC;
    @CsvBindByName(column = "IBAN", required = true)
    private String iBAN;
    @CsvBindByName(column = "CashAccountType", required = true)
    private String cashAccountType;
    @CsvBindByName(column = "Level", required = true)
    private String level;
    @CsvBindByName(column = "Purpose", required = true)
    private String purpose;
    @CsvBindByName(column = "BaseCurrency", required = true)
    private String baseCurrency;
    @CsvBindByName(column = "ReportingCurrency", required = false)
    private String reportingCurrency;
    @CsvBindByName(column = "SettlementCurrency", required = true)
    private String settlementCurrency;
    @CsvBindByName(column = "OpeningDate", required = false)
    private String openingDate;
    @CsvBindByName(column = "LiveDate", required = false)
    private String liveDate;
    @CsvBindByName(column = "ClosingDate", required = false)
    private String closingDate;




    public CashAccountEntry(String registrationNumber, String name, String bBAN, String uPIC, String iBAN, String cashAccountType, String level, String purpose, String baseCurrency, String reportingCurrency, String settlementCurrency, String openingDate, String liveDate, String closingDate) {
        this.registrationNumber = registrationNumber;
        this.name = name;
        this.bBAN = bBAN;
        this.uPIC = uPIC;
        this.iBAN = iBAN;
        this.cashAccountType = cashAccountType;
        this.level = level;
        this.purpose = purpose;
        this.baseCurrency = baseCurrency;
        this.reportingCurrency = reportingCurrency;
        this.settlementCurrency = settlementCurrency;
        this.openingDate = openingDate;
        this.liveDate = liveDate;
        this.closingDate = closingDate;
    }

    public CashAccountDTO getCashAccountDTO() {
        CashAccountDTO cashAccountDTO = new CashAccountDTO();
        cashAccountDTO.setCashAccountType(cashAccountType);
        cashAccountDTO.setLevel(level);
        cashAccountDTO.setPurpose(purpose);
        cashAccountDTO.setBaseCurrency(baseCurrency);
        cashAccountDTO.setReportingCurrency(reportingCurrency);
        cashAccountDTO.setSettlementCurrency(settlementCurrency);
        cashAccountDTO.setOpeningDate(openingDate);
        cashAccountDTO.setLiveDate(liveDate);
        cashAccountDTO.setClosingDate(closingDate);
        cashAccountDTO.setIBAN(iBAN);
        cashAccountDTO.setBBAN(bBAN);
        cashAccountDTO.setUPIC(uPIC);
        cashAccountDTO.setName(name);
        cashAccountDTO.setRegistrationNumber(registrationNumber);

        return cashAccountDTO;
    }
}
