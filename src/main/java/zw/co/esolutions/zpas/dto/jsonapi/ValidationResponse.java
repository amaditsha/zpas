package zw.co.esolutions.zpas.dto.jsonapi;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 08 May 2019
 */
@Data
@Builder
public class ValidationResponse {
    private String value;
    private boolean valid;
}
