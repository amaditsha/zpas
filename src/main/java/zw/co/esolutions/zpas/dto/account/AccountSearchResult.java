package zw.co.esolutions.zpas.dto.account;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 23 Apr 2019
 */
@Data
@Builder(toBuilder = true)
public class AccountSearchResult {
    private String financialInstitutionId;
    private String financialInstitutionName;
    private String virtualId;
    private String bpn;
    private String accountName;
    private String accountOwnerName;
    private String accountOwnerId;
    private String accountId;
    private String accountNumber;
    private String narrative;
}
