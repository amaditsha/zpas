package zw.co.esolutions.zpas.dto.payments;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import zw.co.esolutions.zpas.enums.PaymentInstrumentCode;
import zw.co.esolutions.zpas.enums.PaymentTypeCode;

import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * Created by alfred on 25 Mar 2019
 */
@Data
@Builder
public class SearchDTO {
    private String searchPhrase;
    private String accountId;
    private String paymentInstrument;
    private String paymentType;
    private String valueDateFrom;
    private String valueDateTo;
    private String dateCreatedFrom;
    private String dateCreatedTo;
    private String clientId;

    public PaymentInstrumentCode getPaymentInstrumentCode() {
        try {
            return PaymentInstrumentCode.valueOf(paymentInstrument);
        } catch (RuntimeException re) {
            return null;
        }
    }

    public PaymentTypeCode getPaymentTypeCode() {
        try {
            return PaymentTypeCode.valueOf(paymentType);
        } catch (RuntimeException re) {
            return PaymentTypeCode.None;
        }
    }

    public OffsetDateTime getOffsetValueDateFrom() {
        try {
            final LocalDate localDate = LocalDate.parse(valueDateFrom, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            return LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
        } catch (RuntimeException re) {
            return null;
        }
    }

    public OffsetDateTime getOffsetValueDateTo() {
        try {
            final LocalDate localDate = LocalDate.parse(valueDateTo, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            return  LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
        } catch (RuntimeException re) {
            return null;
        }
    }

    public OffsetDateTime getOffsetDateCreatedFrom() {
        try {
            final LocalDate localDate = LocalDate.parse(dateCreatedFrom, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            return LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
        } catch (RuntimeException re) {
            return null;
        }
    }

    public OffsetDateTime getOffsetDateCreatedTo() {
        try {
            final LocalDate localDate = LocalDate.parse(dateCreatedTo, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            return LocalDateTime.of(localDate, LocalTime.now()).atOffset(ZoneOffset.UTC);
        } catch (RuntimeException re) {
            return null;
        }
    }

    public boolean isValid() {
        if (!StringUtils.isEmpty(searchPhrase)) {
            return true;
        }
        if (!StringUtils.isEmpty(accountId)) {
            return true;
        }
        if (!StringUtils.isEmpty(paymentInstrument)) {
            return true;
        }
        if (!StringUtils.isEmpty(paymentType)) {
            return true;
        }
        if (!StringUtils.isEmpty(valueDateFrom)) {
            return true;
        }
        if (!StringUtils.isEmpty(valueDateTo)) {
            return true;
        }
        if (!StringUtils.isEmpty(dateCreatedFrom)) {
            return true;
        }
        if (!StringUtils.isEmpty(dateCreatedTo)) {
            return true;
        }
        return false;
    }
}
