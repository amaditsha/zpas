package zw.co.esolutions.zpas.dto.profile;

import lombok.Data;

/**
 * Created by mabuza on 18 Mar 2019
 */
@Data
public class OtpDTO {
    private String value;
}
