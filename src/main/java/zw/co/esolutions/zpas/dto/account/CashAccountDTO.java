package zw.co.esolutions.zpas.dto.account;

import lombok.Data;

@Data
public class CashAccountDTO {
    private String id;
    private String cashAccountType;
    private String level;
    private String purpose;
    private String baseCurrency;
    private String reportingCurrency;
    private String settlementCurrency;
    private String openingDate;
    private String liveDate;
    private String closingDate;
    private String identification;
    private String issueDate;
    private String expiryDate;
    private String fromDateTime;
    private String toDateTime;
    private String numberOfDays;
    private String iBAN;
    private String bBAN;
    private String uPIC;
    private String number;
    private String virtualCode;
    private String name;
    private String proprietaryIdentificationId;
    private String proprietaryIdentificationIssueDate;
    private String proprietaryIdentificationExpiryDate;
    private String costReferencePatternId;
    private String costReferencePatternIssueDate;
    private String costReferencePatternExpiryDate;
    private String status;
    private String managementStatus;
    private String entryStatus;
    private String balanceStatus;
    private String blocked;
    private String blockedReason;

    private String uploadAccountsId;
    private String registrationNumber;
    private String clientId;
    private String financialInstitutionId;

    private String username;
}
