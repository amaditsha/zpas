package zw.co.esolutions.zpas.dto.nssa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.io.Serializable;
import java.time.OffsetDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    protected EntityStatus entityStatus;
    protected OffsetDateTime dateCreated;
    protected OffsetDateTime lastUpdated;
    private String title;
    private String ssn;
    private String firstName;
    private String lastName;
    private String otherNames;
    private String gender;
    private String dateOfBirthString;
    private OffsetDateTime dateOfBirth;
    private String maritalStatus;
    private String maidenName;
    private String nationality;
    private String nationalId;
    private String birthCertificateNumber;
    private String citizenship;
    private String passportNumber;
    private String driversLicenceNumber;

    /*Emmployee Occupation Details*/
    private String occupationCategory;
    private String occupation;
    private String worksNumber;
    private String workStationCity;
    private String ministry;
    private String natureOfEmployment;
    private String statusOfEmployment;
    private String employmentStartDateString;
    private OffsetDateTime employmentStartDate;
    private String salaryOnCommencement;

    /*EmployeeDTO Contact Details*/
    private String flatName1;
    private String flatName2;
    private String streetNumber;
    private String streetName;
    private String suburb;
    private String cityTown;
    private String emailAddress;
    private String boxBag;
    private String phoneNumber;
    private String mobileNumber;
    private String website;
    private String contactPerson;
    private String country;
    private String province;
    private String postalAddress;

    private EmployerDTO employer;
    private String clientId;

    @Override
    public String toString() {
        return getFullName();
    }

    public String getFullName() {
        return String.format("%s %s", firstName, lastName);
    }
}
