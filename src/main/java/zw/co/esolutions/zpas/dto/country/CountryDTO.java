package zw.co.esolutions.zpas.dto.country;

import lombok.Data;

@Data
public class CountryDTO {
    public String id;
    public String code;
    public String name;
}
