package zw.co.esolutions.zpas.dto.debitorder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import lombok.Builder;
import lombok.Data;

import java.time.OffsetDateTime;

/**
 * Created by alfred 03/07/2018
 */
@Data
@Builder
public class DebitOrderBatchRequestDTO {
    private String companyId;
    private String bankId;
    private String bankSortCodeId;
    private OffsetDateTime dueDate;

    public DebitOrderBatchRequestDTO() {
    }

    public DebitOrderBatchRequestDTO(@JsonProperty("companyId") String companyId,
                                     @JsonProperty("bankId") String bankId,
                                     @JsonProperty("bankSortCodeId") String bankSortCodeId,
                                     @JsonProperty("dueDate") OffsetDateTime dueDate) {
        this.companyId = Preconditions.checkNotNull(companyId, "Company ID is required.");
        this.bankSortCodeId = Preconditions.checkNotNull(bankSortCodeId, "Sort Code ID is required");
        this.bankId = Preconditions.checkNotNull(bankId, "Bank ID is required.");
        this.dueDate = Preconditions.checkNotNull(dueDate, "Due date should be specified.");
    }
}
