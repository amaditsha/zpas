package zw.co.esolutions.zpas.dto.jsonapi;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alfred on 20 Mar 2019
 */
@Data
@Builder(toBuilder = true)
public class PersonSearchDTO {
    private String search;
    private String organisationId;
    private String personId;
    private String personType;

    private String partyType;
    private String partyRoleId;
}
