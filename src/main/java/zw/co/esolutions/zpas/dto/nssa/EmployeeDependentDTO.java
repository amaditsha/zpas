package zw.co.esolutions.zpas.dto.nssa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.esolutions.zpas.utilities.enums.EntityStatus;

import java.time.OffsetDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDependentDTO {
    private String id;
    protected EntityStatus entityStatus;
    protected OffsetDateTime dateCreated;
    protected OffsetDateTime lastUpdated;
    private String dependentType;
    private String lastName;
    private String firstName;
    private String otherNames;
    private String gender;
    private String dateOfBirthString;
    private OffsetDateTime dateOfBirth;
    private String nationality;
    private String citizenship;
    private String nationalId;
    private boolean education;
    private boolean disable;
    private String ssn;
    private String title;
    private String dateOfMarriageString;
    private OffsetDateTime dateOfMarriage;
    private String passportNumber;
    private String driversLicence;
    private String employeeid;
}
