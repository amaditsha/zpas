package zw.co.esolutions.zpas.dto.currency;

import lombok.Builder;
import lombok.Data;
import zw.co.esolutions.zpas.enums.CurrencyCode;

@Data
@Builder(toBuilder = true)
public class CurrencyCodeAvailabilityCheckDTO {

    private String currencyCode;
    private Boolean available;
}
