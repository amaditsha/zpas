package zw.co.esolutions.zpas.dto.registration;

import lombok.Builder;
import lombok.Data;

import java.util.Objects;

@Data
@Builder(toBuilder = true)
public class OrganisationDTO {
    private String organisationName;
    private String organisationId;
    private String organisationType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganisationDTO that = (OrganisationDTO) o;
        return organisationId.equals(that.organisationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(organisationId);
    }
}
