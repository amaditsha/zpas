package zw.co.esolutions.zpas.dto.message;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class SingleMessageRequestDTO implements Serializable {
    String originator;
    String destination;
    String messageText;
    String messageReference;
}