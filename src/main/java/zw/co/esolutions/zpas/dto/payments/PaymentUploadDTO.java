package zw.co.esolutions.zpas.dto.payments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by alfred on 26 Feb 2019
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class PaymentUploadDTO {
    private String clientId;
    private String accountId;
    private String purpose;
    private String clientReference;
    private String valueDate;
    private String paymentType;
    private String paymentInstrument;
    private String priority;
    private String fileName;
    private boolean verifyAccountInfo;
    private boolean stopWhenFailedVerification;
    private boolean batchBookingEnabled;
}
