package zw.co.esolutions.zpas.dto.nssa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class UploadEmployeesDTO {
    private String clientId;
    /*private String id;
    private String employeeId;
    private String employeeName*/;
    private String fileName;

}
