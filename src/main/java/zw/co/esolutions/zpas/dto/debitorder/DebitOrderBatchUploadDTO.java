package zw.co.esolutions.zpas.dto.debitorder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class DebitOrderBatchUploadDTO {

        private String fileName;
}
