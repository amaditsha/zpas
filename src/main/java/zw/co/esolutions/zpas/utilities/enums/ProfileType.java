package zw.co.esolutions.zpas.utilities.enums;

public enum ProfileType {
    CLIENT,
    OPERATOR,
    SYSTEM,
    FINANCIAL_INSTITUTION
}