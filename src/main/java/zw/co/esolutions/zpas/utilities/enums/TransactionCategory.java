package zw.co.esolutions.zpas.utilities.enums;

public enum TransactionCategory {
	MAIN, CHARGE, COMMISSION, ADJUSTMENT, ARREARS, OTHER, ALL
}
