package zw.co.esolutions.zpas.utilities.enums;

public enum RoleLevel {
    CLIENT(1),
    FINANCIAL_INSTITUTION(2),
    SYSTEM(3);

    private int weight;

    RoleLevel(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
}
