package zw.co.esolutions.zpas.utilities.util.nssa;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.dto.nssa.EmployeeDTO;
import zw.co.esolutions.zpas.services.nssa.iface.NssaClientsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Slf4j
public class NssaReferenceRegexUtil {
    @Autowired
    NssaClientsService nssaClientsService;

    @Value("${NSSAREFERENCEREGEX}")
    private String nssaReferenceRegex;

    public boolean validateNssaReference(String nssaReference){
        log.info("in validate reference {}", nssaReference);
        boolean b = nssaReference.matches(nssaReferenceRegex);
        log.info("the ssn {} matches: {}",nssaReference, b);
        return b;
    }

}
