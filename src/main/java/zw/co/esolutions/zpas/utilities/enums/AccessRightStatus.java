package zw.co.esolutions.zpas.utilities.enums;

public enum AccessRightStatus {
	ENABLED,
	DISABLED,
	DELETED
}
