package zw.co.esolutions.zpas.utilities.enums;

public enum LimitPeriodType {

	TRANSACTION, DAILY, MONTHLY, YEARLY
}
