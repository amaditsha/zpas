package zw.co.esolutions.zpas.utilities.enums;

public enum TariffValueType {

	PERCENTAGE, ABSOLUTE
}
