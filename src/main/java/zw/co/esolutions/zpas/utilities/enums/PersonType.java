package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 21 Mar 2019
 */
public enum PersonType {
    ALL,
    FI_EMPLOYEE,
    ACCOUNT_HOLDER,
    NON_FI_EMPLOYEE
}
