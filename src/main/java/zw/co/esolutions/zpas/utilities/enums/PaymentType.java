package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 12 Nov 2018
 */
public enum PaymentType {
    REAL_TIME,
    DEBIT_REQUEST,
    CREDIT_REQUEST
}
