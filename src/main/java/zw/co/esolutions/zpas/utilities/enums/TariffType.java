package zw.co.esolutions.zpas.utilities.enums;

/**
 * @author tauttee
 *
 */
public enum TariffType {
	SCALED,
	FIXED_AMOUNT,
	INCLINE_BLOCK_TARIFF,
	PER_BULK_ENTRY
}
