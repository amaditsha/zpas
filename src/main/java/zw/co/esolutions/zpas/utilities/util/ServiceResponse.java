package zw.co.esolutions.zpas.utilities.util;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ServiceResponse {
    private final String responseCode;
    private final String errorCode;
    private final String narrative;
}
