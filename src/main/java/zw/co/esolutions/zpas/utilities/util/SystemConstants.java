package zw.co.esolutions.zpas.utilities.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created by stanford on 4/19/16.
 */
public class SystemConstants {

    public static final List<String> ENCRYPTION_ALGORITHM = Arrays.asList("07","09");
    public static final List<String> TOKEN_CARRIER_TYPE = Arrays.asList("01","02");

    public static final String ZEEPAY_DOCUMENTS_ROOT_FOLDER = "ZEEPAY_DOCUMENTS_ROOT_FOLDER";


    //public static final String

    public static final String PASSWORD_REGEX = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})";

    public static final String CLIENT_REGISTRATION_SUCCESS = "CLIENT_REGISTRATION_SUCCESS";
    public static final String CLIENT_REGISTRATION_ERROR = "CLIENT_REGISTRATION_ERROR";
    public static final String CLIENT_REGISTRATION_DUPLICATE = "CLIENT_REGISTRATION_DUPLICATE";

    public static final String RC_SUCCESS = "00";
    public static final String RC_ERROR = "06";
    public static final String RC_INVALID_AMOUNT = "13";
    public static final String RC_DUPLICATE_TRANSACTION = "94";
    public static final String RC_DUPLICATE_TRANSMISSION = "94";

    public static final String STATUS_PENDING_APPROVAL = "PENDING APPROVAL";
    public static final String STATUS_PROCESSED = "PROCESSED";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_DISAPPROVED = "DISAPPROVED";
    public static final String STATUS_ACTIVE = "ACTIVE";
    public static final String STATUS_PENDING_PAYMENT = "PENDING PAYMENT";
    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_INACTIVE = "INACTIVE";
    public static final String STATUS_DELETED = "DELETED";
    public static final String STATUS_REJECTED = "REJECTED";
    public static final String STATUS_BLOCKED = "BLOCKED";
    public static final String STATUS_AVAILABLE = "AVAILABLE";

    public static final String STATUS_APPROVED_PENDING_PAYMENT = "APPROVED PENDING PAYMENT";
    public static final String STATUS_IN_PROGRESS = "IN PROGRESS";
    public static final String STATUS_AWAITING_CLOSURE_APPROVAL = "AWAITING CLOSURE APPROVAL";
    public static final String STATUS_CLOSED = "CLOSED";

    public static final String STATUS_FAILED = "FAILED";

    public static final String AUTH_STATUS_AUTHENTICATED = "Authenticated";
    public static final String AUTH_STATUS_INVALID_CREDENTIALS = "Invalid username or password";
    public static final String AUTH_STATUS_NETWORK_PROBLEM = "Network Problem";
    public static final String AUTH_STATUS_ACCOUNT_LOCKED = "Account has been locked";
    public static final String AUTH_STATUS_CHANGE_PASSWORD = "Should change password";
    public static final String AUTH_STATUS_SYSTEM_ERROR = "System error occured";
    public static final String CHANGE_PASSWORD_SUCCESS = "Change password success";
    public static final String CHANGE_PASSWORD_FAILURE = "Change password failure";
    public static final String INVALID_OLD_PASSWORD = "Incorrect old password";
    public static final String PASSWORD_IN_HISTORY = "Password has already been used";
    public static final String RESET_PASSWORD_SUCCESS = "Reset password success";
    public static final String RESET_PASSWORD_FAILURE = "Reset password failure";
    public static final String AUTH_STATUS_PROFILE_EXPIRED = "Profile Expired";

    public static final String FLASH_WARNING = "warning";
    public static final String FLASH_SUCCESS = "success";
    public static final String FLASH_GOOD_BYE = "goodbye";
    public static final String FLASH_ERROR = "error";
    public static final String FLASH_INFO = "info";

    public static final String NATIONAL_ID = "NATIONAL_ID";

    public static final String APPROVE_ROLE = "APPROVE_ROLE";
    public static final String EDIT_ROLE = "EDIT_ROLE";
    public static final String DELETE_ROLE = "DELETE_ROLE";
    public static final String VIEW_ALL_ROLES = "VIEW_ALL_ROLES";
    public static final String SET_UNSET_ROLE_RESTRICTED = "SET_UNSET_ROLE_RESTRICTED";
    public static final String MANAGE_ROLES = "MANAGE_ROLES";
    public static final String CREATE_ROLE = "CREATE_ROLE";

    public static final String CREATE_ACCESS_RIGHT = "CREATE_ACCESS_RIGHT";
    public static final String DELETE_ACCESS_RIGHT = "DELETE_ACCESS_RIGHT";
    public static final String EDIT_ACCESS_RIGHT = "EDIT_ACCESS_RIGHT";
    public static final String VIEW_ACCESS_RIGHTS = "VIEW_ACCESS_RIGHTS";
    public static final String MANAGE_ACCESS_RIGHTS = "MANAGE_ACCESS_RIGHTS";
    public static final String ENABLE_DISABLE_ACCESS_RIGHT = "ENABLE_DISABLE_ACCESS_RIGHT";
    public static final String SET_ROLE_CAN_DO_ON_ACCESS_RIGHT = "SET_ROLE_CAN_DO_ON_ACCESS_RIGHT";

    public static final String VIEW_ALL_PROFILES = "VIEW_ALL_PROFILES";
    public static final String CREATE_PROFILE = "CREATE_PROFILE";
    public static final String UPDATE_PROFILE = "UPDATE_PROFILE";
    public static final String VIEW_PROFILE = "VIEW_PROFILE";
    public static final String MANAGE_PROFILES = "MANAGE_PROFILES";
    public static final String VIEW_PROFILE_CHANGE_LOG = "VIEW_PROFILE_CHANGE_LOG";
    public static final String DELETE_PROFILE = "DELETE_PROFILE";
    public static final int SMALLER_POOL_SIZE = 5;
    public static final int DEFAULT_POOL_SIZE = 5;
    public static final String ALLOWED_BULK_FILEL_EXTENSIONS = ".csv,.xls,.xlsx,.xml";

    public static String INSURE_ACCESS_DOCUMENTS_ROOT_FOLDER = "INSURE_ACCESS_DOCUMENTS_ROOT_FOLDER";

    public static final String VIEW_AUDITTRAIL = "VIEW_AUDITTRAIL";
    public static final String VIEW_AUDITTRAIL_REPORTS = "VIEW_AUDITTRAIL_REPORTS";

    public static final String CACHE_NAME_REQUEST = "CACHE_NAME_REQUEST";

    public static final String STATUS_PENDING_PROCESSING = "PENDING PROCESSING";

    public static final String PENDING_CLIENT_REQUEST_RESPONSE = "PENDING_CLIENT_REQUEST_RESPONSE";

    public static final String ROLE_NAME_CLIENT = "CLIENT";

    public static final String CACHE_NAME_PAYMENT_INFO = "paymentInfoCache";
    public static final String CACHE_NAME_GENERATION_NUMBER = "generationNumberCache";
    public static final String CACHE_NAME_TAX_CLEARANCE_INFO = "taxClearanceInfoCache";


}
