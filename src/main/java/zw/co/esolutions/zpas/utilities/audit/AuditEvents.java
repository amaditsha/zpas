package zw.co.esolutions.zpas.utilities.audit;

public class AuditEvents {
    public static final String CREATE_PROFILE = "Create Profile";
    public static final String EDIT_PROFILE = "Modification of profile";
    public static final String APPROVE_PROFILE = "Approve profile";
    public static final String REJECT_PROFILE = "Rejection of profile";
    public static final String DELETE_PROFILE = "Delete profile";

    public static final String ACTIVATE_LIMIT = "Activation of limit";

    public static final String CREATE_ROLE = "Create role";
    public static final String APPROVE_ROLE = "Approve role";
    public static final String REJECT_ROLE = "Reject role";
    public static final String EDIT_ROLE = "Modification of role";
    public static final String DELETE_ROLE = "Delete role";

    public static final String CREATE_PREFIX = "CREATE ";
    public static final String EDIT_PREFIX = "EDIT ";
    public static final String DELETE_PREFIX = "DELETE ";
    public static final String APPROVE_PREFIX = "APPROVE ";
    public static final String REJECT_PREFIX = "REJECT ";

    public static final String DELETE_SWITCH_OPERATOR = "DELETE_SWITCH_OPERATOR";
    public static final String EDIT_SWITCH_OPERATOR = "EDIT_SWITCH_OPERATOR";
    public static final String CREATE_SWITCH_OPERATOR = "CREATE_SWITCH_OPERATOR";

    public static final String CREATE_CLIENT_REQUEST_ITEM = "CREATE_CLIENT_REQUEST_ITEM";
    public static final String EDIT_CLIENT_REQUEST_ITEM = "EDIT_CLIENT_REQUEST_ITEM";
    public static final String DELETE_CLIENT_REQUEST_ITEM = "DELETE_CLIENT_REQUEST_ITEM";

    public static final String CREATE_CLIENT_REQUEST =  "CREATE_CLIENT_REQUEST";
    public static final String EDIT_CLIENT_REQUEST = "EDIT_CLIENT_REQUEST";
    public static final String DELETE_CLIENT_REQUEST = "DELETE_CLIENT_REQUEST" ;

    public static final String CREATE_ACCOUNT = "CREATED ACCOUNT";
    public static final String UPDATE_ACCOUNT = "UPDATED ACCOUNT";
    public static final String DELETED_ACCOUNT = "DELETED ACCOUNT";

    public static final String CREATE_PERSON = "CREATED PERSON";
    public static final String UPDATE_PERSON = "UPDATED PERSON";
    public static final String DELETE_PERSON = "DELETED PERSON";
}
