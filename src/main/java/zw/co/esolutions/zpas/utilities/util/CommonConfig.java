package zw.co.esolutions.zpas.utilities.util;

import java.util.Properties;

public class CommonConfig {

    static Properties config = Utils.configParams;

    public static long DEFAULT_MINIMUN_TOKEN_VALUE_CENTS = Long.parseLong(config.getProperty(
            "DEFAULT_MIN_TOKEN_VALUE_CENTS", "200"));
}
