package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 20 Nov 2018
 */
public enum RequestStatus {
    DRAFT,
    ACTIVE,
    PENDING,
    PENDING_APPROVAL,
    DELETED,
    BLOCKED,
    OUTSTANDING,
    COMPLETED,
    IN_PROGRESS,
    FAILED
}
