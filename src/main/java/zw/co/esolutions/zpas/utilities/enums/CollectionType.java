package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by stanford on 12/1/16.
 */

public enum CollectionType {
    PER_TRANSACTION,
    PER_MONTH
}
