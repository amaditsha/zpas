package zw.co.esolutions.zpas.utilities.enums;

public enum TariffFeeType {
    FIXED_AMOUNT,
    PERCENTAGE
}
