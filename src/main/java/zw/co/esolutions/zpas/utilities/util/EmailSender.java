package zw.co.esolutions.zpas.utilities.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailSender {

    /**
     * @param args
     */
    public static void main(String[] args) {

        String[] to = {"zpas.esolutions@gmail.com"};
        String from = "zeepaysolutions@gmail.com";
        String subj = "ZeePay Email Test";
        String msgText = "Just A Test\nZeePay Test\n";
        //String fileId = "D:\\MyProjects\\mail\\EmailSender\\src\\sender\\email\\EmailSender.java ";
        //String file1 = "/home/zviko/FirstPdf.pdf";
        //String file2 = "/home/zviko/StatementPdf.pdf";
        //String[] fileNames = {file1,file2};
        EmailSender sender = new EmailSender();
		//sender.postMail(to, from, subj, msgText, null);
//		sender.sendCommonsMail(to, from, subj, msgText, fileNames);
 //       System.out.println(sender.validateEmail("zviko.kanyume@gmail.co.zw"));
    }

/*    public boolean postMail(String[] recipients, String from, String subject,
                            String message, String fileId) {
        boolean result=false;

        for(String s:recipients)
            System.out.println("Sending Email To:"+s);
        System.out.println("Message"+message);
        boolean debug = false;
        Session session = null;
        try {
            // Set the host smtp address
            Properties props = new Properties();
            props.put("mail.smtp.host", config.getProperty("SYSTEM_SMTP_HOST_NAME"));
            props.put("mail.smtp.port", config.getProperty("SYSTEM_SMTP_PORT"));
            props.put("mail.smtp.auth", config.getProperty("SYSTEM_SMTP_AUTH_STATUS"));
            props.put("mail.smtp.starttls.enable", config.getProperty("SYSTEM_SMTP_TLS_ENABLE"));
//            props.put("mail.smtp.socketFactory.port", config.getProperty("SYSTEM_SMTP_PORT"));
//            props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
//            props.put("mail.smtp.socketFactory.fallback", "false");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

            Authenticator auth = new SMTPAuthenticator();

            session = Session.getDefaultInstance(props, auth);
            //session=Session.getInstance(props);

            session.setDebug(debug);

            // create message
            Message msg = new MimeMessage(session);

            // Set internet address from
            InternetAddress addressFrom = new InternetAddress(from);
            msg.setFrom(addressFrom);

            // Set electronicAddresses to
            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            msg.setRecipients(Message.RecipientType.TO, addressTo);

            // Seting the subject
            msg.setSubject(subject);


            //Create and fill in message text
            MimeBodyPart msgText = new MimeBodyPart();
            msgText.setText(message);

            //Create multipart and add the body parts
            MimeMultipart mp = new MimeMultipart();
            mp.addBodyPart(msgText);

            if(fileId!=null){
                //Set the attatchment
                MimeBodyPart msgAtt = new MimeBodyPart();
                File file = new File(fileId);
                FileDataSource fds = new FileDataSource(fileId);
                msgAtt.setHeader("Content-Type", "application/pdf");
                System.out.println("After setting content type Now sending ");
                msgAtt.setDataHandler(new DataHandler(fds));
                msgAtt.setFileName(file.getName());
                mp.addBodyPart(msgAtt);
            }

            //Add the multipart to the message
            msg.setContent(mp);
            msg.setContent(message, "text/html; charset=utf-8");
            Transport.send(msg);
            result=true;
            //System.out.println("Sending succeded");
        } catch (Exception e) {
            System.out.println("email error email exception ::::::::::::::::::::::::::::::"+e.getMessage());

            e.printStackTrace();
        }
        System.out.println("Email sending result is :::::::"+result);
        return result;
    }

    class SMTPAuthenticator extends Authenticator {

        public PasswordAuthentication getPasswordAuthentication() {
            String username = config.getProperty("SYSTEM_SMTP_USER_NAME");
            String password = config.getProperty("SYSTEM_SMTP_PASSWORD");
            return new PasswordAuthentication(username, password);
        }

    }*/

//    public void sendCommonsMail(String to[],String cc,String from,String subj, String msgText,String fileNames[]){
//
//        Properties props = new Properties();
//        props.put("mail.smtp.host", config.getProperty("SYSTEM_SMTP_HOST_NAME"));
//        props.put("mail.smtp.port", config.getProperty("SYSTEM_SMTP_PORT"));
//        props.put("mail.smtp.auth", config.getProperty("SYSTEM_SMTP_AUTH_STATUS"));
//        String path = config.getProperty("PROCESS_MODULE_PATH");
//
//        String recipients = "";
//        try{
//            MultiPartEmail email = new MultiPartEmail();
//            System.out.println(fileNames.length);
//            for(int i=0 ;i<fileNames.length;i++){
//                System.out.println(fileNames[i]);
//            }
//            for(int i=0 ;i<fileNames.length;i++){
//
//                File file = new File(path+"pdfs/"+fileNames[i]);
//                System.out.println("File path :"+file.getAbsolutePath()+" name :"+file.getName());
//                // Create the attachment
//                EmailAttachment attachment = new EmailAttachment();
//                attachment.setPath(file.getAbsolutePath());
//                attachment.setDisposition(EmailAttachment.ATTACHMENT);
//                attachment.setDescription("PDF Report");
//                attachment.setName(file.getName());
//
//                // add the attachment
//                email.attach(attachment);
//            }
//
//            for(int a=0;a<to.length;a++){
//                recipients=to[a]+",";
//            }
//
//            // Create the email message
//
//            email.setHostName(config.getProperty("SYSTEM_SMTP_HOST_NAME"));
//            email.addTo(recipients);
//            email.addCc(cc);
//            email.setFrom(from);
//            email.setSubject(subj);
//            email.setMsg(msgText);
//
//
//            // send the email
//            email.send();
//            System.out.println("Email Sending successful ::::::::::::::::::::: true");
//
//        }catch (Exception e) {
//            System.out.println("Email Sending failed :::::::::::::::::::::"+e.getMessage());
//            e.printStackTrace();
//        }
//
//    }

    public boolean validateEmail(String email){
        if(email==null){
            return false;
        }
        Pattern pattern;
        Matcher matcher;

        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        pattern = Pattern.compile(EMAIL_PATTERN);

        matcher = pattern.matcher(email);
        return matcher.matches();
    }


}
