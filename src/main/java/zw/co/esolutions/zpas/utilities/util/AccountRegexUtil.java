package zw.co.esolutions.zpas.utilities.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;

import java.util.*;
import java.util.regex.*;

@Component
@Slf4j
public class AccountRegexUtil {
    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private Environment environment;

    static Map<String, String> bicToAccountRegexMap = new HashMap<>();
    static Map<String, String> accountRegexToBicMap = new HashMap<>();

    private void loadMaps(){
        if(bicToAccountRegexMap.isEmpty()){
            log.info("Initializing Account Regex Map..");
            final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();

            //load bicToAccountRegexMap
            for(FinancialInstitution financialInstitution: financialInstitutions){
                String accountRegexes = environment.getProperty("ACCOUNT.REGEX." + financialInstitution.getBIC());

                if (accountRegexes != null){
                    bicToAccountRegexMap.putIfAbsent(financialInstitution.getBIC(), accountRegexes);
                }
            }

            //load accountRegexToBicMap
           /* for (String key: bicToAccountRegexMap.keySet()){
                final List<String> acregexes = bicToAccountRegexMap.get(key);

                for (String regex: acregexes){
                    accountRegexToBicMap.putIfAbsent(regex, key);
                }
            }*/

        }
    }

    private Map<String, String> getBicToAccountRegexMap(){
        if (bicToAccountRegexMap.isEmpty()){
            loadMaps();
        }
        return bicToAccountRegexMap;
    }

    private Map<String, String> getAccountRegexToBicMap(){
        if (accountRegexToBicMap.isEmpty()){
            loadMaps();
        }
        return accountRegexToBicMap;
    }

    public Boolean validateAccount(String bicfi, String account){

        final Map<String, String> regex = getBicToAccountRegexMap();
        final String regexPattern = regex.get(bicfi);
        if(StringUtils.isEmpty(regexPattern)) {
            return false;
        }
        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(account);
        boolean b = m.matches();

        return b;
    }


    public Optional<String> getBIC(String accountRegex){
        if(StringUtils.isEmpty(accountRegex)){
            return Optional.empty();
        }
        return Optional.ofNullable(getAccountRegexToBicMap().get(accountRegex));
    }

}
