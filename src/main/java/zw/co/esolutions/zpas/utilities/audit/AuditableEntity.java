package zw.co.esolutions.zpas.utilities.audit;

public interface AuditableEntity {
    String stringifyAuditAttributes();
}
