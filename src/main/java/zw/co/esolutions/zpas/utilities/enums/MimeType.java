package zw.co.esolutions.zpas.utilities.enums;

public enum MimeType {
    APPLICATION_PDF,
    APPLICATION_XLS,
    APPLICATION_XML,
    APPLICATION_CSV,
    IMAGE_JPG,
    IMAGE_PNG,
    IMAGE_GIF,
    VIDEO_MP4,
    VIDEO_AVI,
    VIDEO_WEBM,
    VIDEO_MKV,
    MUSIC_MP3,
    MUSIC_WAV,
    MUSIC_WMV,
    OTHER
}
