package zw.co.esolutions.zpas.utilities.util;

import zw.co.esolutions.zpas.utilities.enums.CommissionType;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberUtil {

    public static final double roundDouble(double d, int places) {
        return Math.round(d * Math.pow(10, (double) places)) / Math.pow(10, (double) places);
    }

    public static Long getCommissionAmount(String commissionType, String valueString, long premiumAmt) {

        if(commissionType.equalsIgnoreCase("null")) {
            return 0L;
        }
        CommissionType type = CommissionType.valueOf(commissionType);

        //System.out.printf("Calculationg commission type | {} | for value | {} | and premium | {}", commissionType, value, premiumAmt);

        if(type.equals(CommissionType.AMOUNT)) {
            return (long)(Float.parseFloat(valueString));
        } else if(type.equals(CommissionType.PERCENTAGE)){
            return (long)(Float.parseFloat(valueString) * premiumAmt / 100);
        } else {
            return 0L;
        }
    }

    public static String formatMobileNumber(String mobileNumber) throws Exception {
        if (mobileNumber == null) {
            throw new Exception("Mobile number is NULL");
        }

        mobileNumber = mobileNumber.trim().replace(" ", "");

        if (mobileNumber.startsWith("+263")) {
            mobileNumber = mobileNumber.substring(1);
        }

        if (mobileNumber.startsWith("07")) {
            mobileNumber = "263" + mobileNumber.substring(1);
        }

        if (mobileNumber.startsWith("2637")) {
            if (mobileNumber.length() != 12) {
                throw new Exception("Mobile number length must be 12: yours has " + mobileNumber.length());
            }
        } else {
            throw new Exception("Mobile Number is invalid: does not contain 07 prefix.");
        }
        return mobileNumber;
    }

    public static String formatMobile(String mobileNumber) throws Exception {
        if (mobileNumber == null) {
            throw new Exception("Mobile number is NULL");
        }

        mobileNumber = mobileNumber.trim().replace(" ", "");

        if (mobileNumber.startsWith("+263")) {
            mobileNumber = mobileNumber.substring(1);
        }

        if (mobileNumber.startsWith("07")) {
            mobileNumber = "263" + mobileNumber.substring(1);
        }

        return mobileNumber;
    }

    public static boolean validateMobileNumber(String mobileNumber) {
        try {
            formatMobileNumber(mobileNumber);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String validateAccountNumber(String accountNumber) throws Exception {

        if(accountNumber == null){
            throw new Exception("Account Number is null");
        }

        if(!accountNumber.startsWith("4")){
            throw new Exception("Account number is not valid");
        }
        if(accountNumber.length()!=13){
            throw new Exception("Account number length not valid");
        }
        Pattern notInts = Pattern.compile("[^0123456789]");
        Matcher makeMatch = notInts.matcher(accountNumber);

        if(makeMatch.find()){
            throw new Exception("Account number should contain only digits");
        }

        return accountNumber;
    }

    public static void main(String[] args) {
        try {
            System.out.println("Date: " + new Date().getTime());
            Thread.sleep(1*20);
            System.out.println("Date: " + new Date().getTime());
        }catch (Exception e) {
            // TODO: handle exception
        }

    }

}
