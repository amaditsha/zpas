package zw.co.esolutions.zpas.utilities.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;
import java.util.Properties;
import org.springframework.util.StringUtils;

/**
 * Created by stanford on 1/28/16.
 */
public class Utils {

    public static Properties configParams;
    public static String SYSTEM_CONFIG_FOLDER;
    public static String SYSTEM_CONFIG_FILE;

    static {
        System.getenv().entrySet().stream().forEach(envEntry ->
                System.setProperty(envEntry.getKey(), envEntry.getValue()));
        configParams = System.getProperties();
        //System.out.println("Config: " + configParams);
    }
//    static {
//        String fileSep = System.getProperties().getProperty("file.separator");
//        String root = fileSep;
//        if (fileSep.equals("\\")) {
//            fileSep = "\\\\";
//            root = "c:\\";
//        }
//        SYSTEM_CONFIG_FOLDER = root + "opt" + fileSep + "eSolutions" + fileSep + "ppus" + fileSep + "conf" + fileSep;
//        SYSTEM_CONFIG_FILE = SYSTEM_CONFIG_FOLDER + "main.conf";
//
//        // read the system configurations
//        Properties config = new Properties();
//        try {
//            config.load(new FileInputStream(SYSTEM_CONFIG_FILE));
//            configParams = config;
//            //System.out.println("config:: " + config);
//        } catch (Exception ex) {
//            System.out.println("Error: Could not read the system configuration file '" + SYSTEM_CONFIG_FILE + "'. Make sure it exists.");
//            System.out.println("Specific error: " + ex);
//        }
//    }

    public static BigDecimal parseBigDecimal(String val) {
        if (StringUtils.isEmpty(val)) {
            return BigDecimal.ZERO;
        }
        return new BigDecimal(val);
    }

    public static boolean isSuperAdminRole(String roleName) {
        return "SuperAdmin".toUpperCase().equals(roleName.toUpperCase());
    }

    public static String convertMapToQueryString(Map<String, Object> map) {
        String queryString = "";
        boolean appendAmpersand = false;
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (queryString.contains("=")) appendAmpersand = true;
            queryString += appendAmpersand ? "&" : "";
            queryString += (entry.getKey() + "=" + entry.getValue());
            appendAmpersand = false;
        }
        return queryString;
    }

    public static String stripJsonSlashes(String json) {
        if (json == null) return null;
        return json.replace("\\", "");
    }

    public static String formatFinancialInstitutionId(String financialInstitutionId) {
        if (financialInstitutionId == null || financialInstitutionId.equals("null") || financialInstitutionId.equals("none") || financialInstitutionId.trim().equals("")) {
            return null;
        }
        return financialInstitutionId;
    }

    public static void main(String... args) {
//        ZonedDateTime zonedDateTime = ZonedDateTime.now();
//        System.out.println("ZonedDateTime : " + zonedDateTime);
//        OffsetDateTime offsetDateTime = OffsetDateTime.now();
//        System.out.println("OffsetDateTime : " + offsetDateTime);

        System.out.println("");
    }
    public static int getThisYear() {
        return LocalDate.now().getYear();
    }

    public static String getLabelCodeForStatus(String status) {
        switch (status) {
            case SystemConstants.STATUS_AVAILABLE:
            case SystemConstants.STATUS_ACTIVE:
            case SystemConstants.STATUS_PROCESSED:
                return "label-success";
            case SystemConstants.STATUS_DELETED:
                return "label-danger";
            case SystemConstants.STATUS_APPROVED:
                return "label-success";
            case SystemConstants.STATUS_PENDING_APPROVAL:
                return "label-primary";
            case SystemConstants.STATUS_BLOCKED:
                return "label-danger";
            case SystemConstants.STATUS_FAILED:
                return "label-warning";
            case SystemConstants.STATUS_PENDING_PAYMENT:
                return "label-info";
            case SystemConstants.STATUS_IN_PROGRESS:
                return "label-success";
            case SystemConstants.STATUS_APPROVED_PENDING_PAYMENT:
                return "label-info";
            case SystemConstants.STATUS_AWAITING_CLOSURE_APPROVAL:
                return "label-info";
            case SystemConstants.STATUS_CLOSED:
                return "label-success";

            default:
                return "label-default";
        }
    }

}
