package zw.co.esolutions.zpas.utilities.util;

/**
 * Created by stanford on 2/27/17.
 */

public class MessageType {
    public static final String SMS = "SMS";
    public static final String EMAIL = "EMAIL";
    public static final String GET_CONSUMERS = "GET_CONSUMERS";
    public static final String GET_CONSUMER_BY_ACCOUNT = "GET_CONSUMER_BY_ACCOUNT";
    public static final String GET_CONSUMER_BY_METER = "GET_CONSUMER_BY_METER";
    public static final String GET_CONSUMERS_REGISTERED_BETWEEN = "GET_CONSUMERS_REGISTERED_BETWEEN";
    public static final String UPDATE_CONTACT_DETAILS = "UPDATE_CONTACT_DETAILS";

    public static final String GET_BALANCES = "GET_BALANCES";
    public static final String GET_BALANCES_BY_ACCOUNT = "GET_BALANCES_BY_ACCOUNT";
    public static final String GET_BALANCES_BY_METER = "GET_BALANCES_BY_METER";
    public static final String GET_BALANCES_BETWEEN = "GET_BALANCES_BETWEEN";
    public static final String GET_BALANCES_AS_AT = "GET_BALANCES_AS_AT";

    public static final String GET_INVOICES = "GET_INVOICES";
    public static final String GET_INVOICES_BY_ACCOUNT = "GET_INVOICES_BY_ACCOUNT";
    public static final String GET_INVOICES_BY_METER = "GET_INVOICES_BY_METER";
    public static final String GET_INVOICES_BETWEEN = "GET_INVOICES_BETWEEN";
    public static final String GET_INVOICES_AS_AT = "GET_INVOICES_AS_AT";

    public static final String CREATE_RECEIPT = "CREATE_RECEIPT";
    public static final String GET_RECEIPTS = "GET_RECEIPTS";
    public static final String GET_RECEIPTS_BY_ACCOUNT = "GET_RECEIPTS_BY_ACCOUNT";
    public static final String GET_RECEIPTS_BY_METER = "GET_RECEIPTS_BY_METER";
    public static final String GET_RECEIPTS_BETWEEN = "GET_RECEIPTS_BETWEEN";
    public static final String GET_RECEIPTS_AS_AT = "GET_RECEIPTS_AS_AT";
    public static final String GET_RECEIPTS_BY_REFERENCE = "GET_RECEIPTS_BY_REFERENCE";

    public static final String GET_INCOME_CODES = "GET_INCOME_CODES";
}
