package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by stanford on 12/1/16.
 */
public enum TariffItemType {
    STEP_RATE,
    STEP_UNITS,
    STEP_FEE,
    TRANSACTION_FEE,
    MONTHLY_FEE,
    FBU,
    VAT,
    ARREARS,
    COMMISSION,
    VENDOR_TRANSACTION
}
