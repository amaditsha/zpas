package zw.co.esolutions.zpas.utilities.audit;

import zw.co.esolutions.zpas.utilities.util.MapUtil;

import java.util.Map;

public interface Auditable {

	default String getEntityName() {
		return getClass().getSimpleName();
	}

	String getId();

	Map< String, String>  getAuditableAttributesMap();

	default String getAuditableAttributesString() {
		return MapUtil.convertAttributesMapToString(getAuditableAttributesMap());
	}

	String getInstanceName();
}
