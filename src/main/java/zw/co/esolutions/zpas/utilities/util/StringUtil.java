package zw.co.esolutions.zpas.utilities.util;

import org.apache.commons.lang3.StringUtils;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

public class StringUtil {

    private static List<String> validNumbers;
    private static Map<Integer, Character> lettersMap;
    private static Properties config;
    private static final String VALID_MOBILE_PREFIX = "VALID_MOBILE_PREFIX";
    private static final String MIN_MOBILE_NUMBER_LENGTH = "MIN_MOBILE_NUMBER_LENGTH";
    private static final String MAX_MOBILE_NUMBER_LENGTH = "MAX_MOBILE_NUMBER_LENGTH";
    private static final String MOBILE_ACCESS_CODE = "MOBILE_ACCESS_CODE";
    private static final String USER_SERVICE_PASSWORD = "USER_SERVICE_PASSWORD";
    private static String userServicePassword;
    private static int minMobileNumberLength;
    private static int maxMobileNumberLength;
    private static String accessCode;
    private static final String COUNTRY_CODE = "263";
    private static final String OLD_ACCESS_CODE = "91";

    public static String formatCamelCaseToSpacedString(String camelCase) {
        if (StringUtils.isEmpty(camelCase)) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        for (String w : camelCase.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
            result.append(w + " ");
        }
        return result.toString().replace('_', ' ').trim();
    }

    public static String toCamelCase(String msg) {
        if (msg == null) {
            return null;
        }
        msg = msg.toLowerCase();
        int start = 0;
        int end = 0;
        int spaceIndex;
        String temp = "";
        String firstLetter = "";
        do {
            spaceIndex = msg.indexOf(" ", start);
            if (spaceIndex > 0) {
                end = spaceIndex;
            } else {
                end = msg.length();
            }
            if (start < msg.length()) {
                firstLetter = msg.substring(start, start + 1);
                temp = temp + firstLetter.toUpperCase() + msg.substring(start + 1, end) + " ";
                start = spaceIndex + 1;
            }

        } while (spaceIndex > 0);

        return temp.trim();
    }

    public static String formatNationalId(String idNumber, String idType) {
        if (SystemConstants.NATIONAL_ID.equals(idType)) {
            idNumber = idNumber.toUpperCase();
            StringBuffer temp = new StringBuffer();
            for (int i = 0; i < idNumber.length(); i++) {
                Character c = idNumber.charAt(i);
                if (Character.isDigit(c) || Character.isLetter(c)) {
                    if (Character.isLetter(c)) {
                        temp.append(c);
                    } else {
                        temp.append(c);
                    }
                }
            }
            return temp.toString();
        }
        return idNumber;
    }

    public static List<String> getValidNumbers() {
        if (validNumbers == null) {
//			initConfigFile();
            String validNumber = config.getProperty(VALID_MOBILE_PREFIX);
            String[] validArr = validNumber.split(",");
            validNumbers = Arrays.asList(validArr);
        }
        return validNumbers;
    }

    public static boolean validateEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public static boolean validateBIC(String bic) {
        String regex = "([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)";
        return bic.matches(regex);
    }

    // Method to convert the string
    public static String capitaliseWord(String str) {
        StringBuffer s = new StringBuffer();

        // Declare a character of space
        // To identify that the next character is the starting
        // of a new word
        char ch = ' ';
        for (int i = 0; i < str.length(); i++) {

            // If previous character is space and current
            // character is not space then it shows that
            // current letter is the starting of the word
            if (ch == ' ' && str.charAt(i) != ' ')
                s.append(Character.toUpperCase(str.charAt(i)));
            else
                s.append(str.charAt(i));
            ch = str.charAt(i);
        }

        // Return the string with trimming
        return s.toString().trim();
    }

    public static void main(String[] args) {
        String bic = "DABAIE2D";
        System.out.printf("%s is a valid BIC? %s", bic, String.valueOf(validateBIC(bic)));
    }

//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        while(true) {
//            System.out.println("Type an email:");
//            final String email = scanner.nextLine();
//            System.out.println("The E-mail ID is: " + email);
//            System.out.println("Is the above E-mail ID valid? " + validateEmail(email));
//        }
//    }

    public static void setValidNumbers(List<String> validNumbers) {
        StringUtil.validNumbers = validNumbers;
    }

    public static boolean validateNationalID(String nationalID) {
        StringBuffer buf = new StringBuffer();
        Character character = ' ';
        int length = nationalID.length();
        if (lastCharacterIsLetter(nationalID, length)) {
            return false;
        }

        if (nationalID == null) {
            return false;
        } else {
            nationalID = nationalID.toUpperCase();
            int digitsCount = 0;
            for (int i = 0; i < nationalID.length(); i++) {
                Character c = nationalID.charAt(i);

                if (Character.isLetter(c)) {
                    character = c;
                    for (int j = i; j < nationalID.length(); j++) {
                        if (Character.isDigit(nationalID.charAt(j))) {
                            digitsCount++;
                        }
                    }
                    if (digitsCount != 2) {
                        return false;
                    } else {
                        try {
                            long number = new Long(buf.toString());
                            int checksum = (int) (number % 23);
                            Character correct = getLettersMap().get(checksum);
                            if (character.equals(correct)) {
                                return true;
                            } else {
                                String withoutLeadingZeroes = removeLeadingZeroes(buf.substring(2));
                                number = new Long(buf.substring(0, 2) + withoutLeadingZeroes);
                                checksum = (int) (number % 23);
                                correct = getLettersMap().get(checksum);
                                return character.equals(correct);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        }
                    }
                } else if (Character.isDigit(c)) {
                    buf.append(c);
                }

            }
            return false;
        }
    }


    private static boolean lastCharacterIsLetter(String nationalID, int length) {
        Character c = nationalID.charAt(length - 1);

        return Character.isLetter(c);
    }

    public static Map<Integer, Character> getLettersMap() {
        if (lettersMap == null) {
            lettersMap = new HashMap<Integer, Character>();
            lettersMap.put(new Integer(0), new Character('Z'));
            lettersMap.put(new Integer(1), new Character('A'));
            lettersMap.put(new Integer(2), new Character('B'));
            lettersMap.put(new Integer(3), new Character('C'));
            lettersMap.put(new Integer(4), new Character('D'));
            lettersMap.put(new Integer(5), new Character('E'));
            lettersMap.put(new Integer(6), new Character('F'));
            lettersMap.put(new Integer(7), new Character('G'));
            lettersMap.put(new Integer(8), new Character('H'));
            lettersMap.put(new Integer(9), new Character('J'));
            lettersMap.put(new Integer(10), new Character('K'));
            lettersMap.put(new Integer(11), new Character('L'));
            lettersMap.put(new Integer(12), new Character('M'));
            lettersMap.put(new Integer(13), new Character('N'));
            lettersMap.put(new Integer(14), new Character('P'));
            lettersMap.put(new Integer(15), new Character('Q'));
            lettersMap.put(new Integer(16), new Character('R'));
            lettersMap.put(new Integer(17), new Character('S'));
            lettersMap.put(new Integer(18), new Character('T'));
            lettersMap.put(new Integer(19), new Character('V'));
            lettersMap.put(new Integer(20), new Character('W'));
            lettersMap.put(new Integer(21), new Character('X'));
            lettersMap.put(new Integer(22), new Character('Y'));
        }
        return lettersMap;
    }

    public static void setLettersMap(Map<Integer, Character> lettersMap) {
        StringUtil.lettersMap = lettersMap;
    }

    public static String removeLeadingZeroes(String value) {
        while (value.startsWith("0")) {
            value = value.substring(1);
        }
        return value;
    }

    public static void setUserServicePassword(String userServicePassword) {
        StringUtil.userServicePassword = userServicePassword;
    }


    public static String getAccessCode() {
        return accessCode;
    }


    public static void setAccessCode(String accessCode) {
        StringUtil.accessCode = accessCode;
    }

    public static String getDateInOracleFormate(Date date) {
        if (date == null) {
            date = new Date();
        }
        Format formatter = new SimpleDateFormat("dd-MMM-yy");
        String s = formatter.format(date);
        return s;

    }

    public static String validateString(String value) {
        value = value.trim();
        //value=value.replaceAll("/[^a-zA-Z 0-9]+/g","");
        //value=value.replaceAll(",","");
        value = value.replaceAll("[^a-zA-Z]", "");

        return value;
    }
}
