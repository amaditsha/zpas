package zw.co.esolutions.zpas.utilities.enums;

public enum MessageChannel {
    EMAIL,
    SMS,
    BOTH
}
