package zw.co.esolutions.zpas.utilities.enums;

public enum  AccountInfoRequestType {
    BalanceEnquiry,
    AccountStatement
}
