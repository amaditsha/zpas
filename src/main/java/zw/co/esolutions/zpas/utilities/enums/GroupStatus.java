package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 14 May 2019
 */
public enum GroupStatus {
    RECEIVED,
    SUCCESS,
    FAIL,
    REJECTED,
    APPROVED
}
