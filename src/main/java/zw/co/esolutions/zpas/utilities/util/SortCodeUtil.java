package zw.co.esolutions.zpas.utilities.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;
import zw.co.esolutions.zpas.model.FinancialInstitution;
import zw.co.esolutions.zpas.services.iface.registration.FinancialInstitutionService;

import java.util.*;

/**
 * Created by mabuza on 13 Jun 2019
 */
@Component
@Slf4j
public class SortCodeUtil {
    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private Environment environment;

    static Map<String, List<String>> bicToSortCodeMap = new HashMap<>();
    static Map<String, String> sortCodeToBicMap = new HashMap<>();

    private void loadMaps() {
        if (bicToSortCodeMap.isEmpty()) {
            log.info("Initializing Sort Codes Map..");
            final List<FinancialInstitution> financialInstitutions = financialInstitutionService.getFinancialInstitutions();

            //load bicToSortCodeMap
            for (FinancialInstitution financialInstitution: financialInstitutions) {
                String sortCodes = environment.getProperty("SORTCODE."  + financialInstitution.getBIC());

                if (sortCodes != null) {
                    List<String> bicSortCodeList = Arrays.asList(sortCodes.split(","));
                    bicToSortCodeMap.putIfAbsent(financialInstitution.getBIC(), bicSortCodeList);
                }
            }

            //load sortCodeToBicMap
            for (String key: bicToSortCodeMap.keySet()) {

                final List<String> scodes = bicToSortCodeMap.get(key);

                for (String code: scodes) {
                    sortCodeToBicMap.putIfAbsent(code, key);
                }
            }
        }
    }

    private Map<String, List<String>> getBicToSortCodeMap() {
        if (bicToSortCodeMap.isEmpty()) {
            loadMaps();
        }
        return bicToSortCodeMap;
    }

    private Map<String, String> getSortCodeToBicMap() {
        if (sortCodeToBicMap.isEmpty()) {
            loadMaps();
        }
        return sortCodeToBicMap;
    }

    public List<String> getListOfSortCodes(String bicfi) {
        return getBicToSortCodeMap().get(bicfi);
    }

    public Optional<String> getSingleSortCode(String bicfi) {
        final List<String> bicSortCodes = getBicToSortCodeMap().get(bicfi);
        if (bicSortCodes != null && !bicSortCodes.isEmpty()) {
            return Optional.of(bicSortCodes.get(0));
        }
        return Optional.empty();
    }

    public Optional<String> getBIC(String sortCode) {
        if(StringUtils.isEmpty(sortCode)) {
            return Optional.empty();
        }
       return Optional.ofNullable(getSortCodeToBicMap().get(sortCode.trim()));
    }
}
