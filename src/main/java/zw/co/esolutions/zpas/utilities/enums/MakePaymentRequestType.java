package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 03 Apr 2019
 */
public enum MakePaymentRequestType {
    COMPANY,
    INDIVIDUAL,
    MERCHANT_PAYMENT,
    TAX_PAYMENT,
    ZIMRA_PAYMENT
}
