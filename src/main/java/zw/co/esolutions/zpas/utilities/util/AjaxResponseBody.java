package zw.co.esolutions.zpas.utilities.util;

import lombok.Data;

import java.util.List;

@Data
public class AjaxResponseBody {

    String responseCode;
    String narrative;
    Object object;

    //getters and setters

}

