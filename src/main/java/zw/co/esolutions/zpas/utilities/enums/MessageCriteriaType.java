package zw.co.esolutions.zpas.utilities.enums;

public enum MessageCriteriaType {
    POLICY_GROUP,
    POLICY_STATUS,
    PRODUCT
}
