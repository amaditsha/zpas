package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 19 Nov 2018
 */
public enum PaymentCategory {
    PAYMENT,
    MERCHANT_PAYMENT
}
