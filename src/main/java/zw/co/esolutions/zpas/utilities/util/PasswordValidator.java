package zw.co.esolutions.zpas.utilities.util;

/**
 * Created by stanford on 1/27/17.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator{

    private Pattern pattern;
    private Matcher matcher;

    /**
     * Regex - password must have at least ONE LowerCase Char, ONE UpperCase Char,
     *   ONE Digit and ONE Special Char
     */
    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})";

    public PasswordValidator(){
        pattern = Pattern.compile(PASSWORD_PATTERN);
    }

    /**
     * Validate password with regular expression
     * @param password password for validation
     * @return true valid password, false invalid password
     */
    public boolean validate(final String password){
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
}