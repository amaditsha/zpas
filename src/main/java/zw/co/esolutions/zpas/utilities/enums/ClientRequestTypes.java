package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by tanatsa on 03 Dec 2018
 */

public enum ClientRequestTypes {
    WITHDRAW_BATCH,
    RECALL_BATCH,
    CLAIM_EFT_CREDITS
}
