package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 27 August 2019
 */
public enum SpecialPayee {
    NSSA, ZIMRA, NONE
}
