package zw.co.esolutions.zpas.utilities.util;

import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * Created by tarisai on 5/13/16.
 */
public class DateUtil {

    public static OffsetDateTime getBeginningOfDay(OffsetDateTime dateTime){

        if(dateTime == null){
            return null;
        }
        return dateTime.withHour(0).withMinute(0).withSecond(0).withNano(0).withOffsetSameLocal(dateTime.getOffset());

    }

    public static OffsetDateTime getEndDate(){

        OffsetDateTime dateTime = OffsetDateTime.now().plusYears(10);
        return dateTime.withHour(0).withMinute(0).withSecond(0).withNano(0).withOffsetSameLocal(dateTime.getOffset());

    }

    public static OffsetDateTime getEndOfDay(OffsetDateTime dateTime){
        if(dateTime == null){
            return null;
        }
        return dateTime.withHour(23).withMinute(59).withSecond(59).withNano(999999999);
    }

    public static OffsetDateTime addHours(OffsetDateTime dateTime, int hours){
        if(dateTime == null){
            return null;
        }
        return dateTime.plusHours(hours);
    }

    public static long daysBetween(OffsetDateTime day1, OffsetDateTime day2) {
        OffsetDateTime smaller,bigger;
        if(day1 != null && day2 != null){
            if(day1.isBefore(day2)){
                smaller = day1;
                bigger = day2;
            }else{
                bigger = day1;
                smaller = day2;
            }
            double milliSecondsInOneDay = 1000000000 * 60 * 60 * 24;
            double daysBetweenAsDouble = (bigger.toLocalTime().toNanoOfDay() - smaller.toLocalTime().toNanoOfDay()) / milliSecondsInOneDay;
            return Math.round(daysBetweenAsDouble);
        }
        return 0;
    }

    public static boolean isEndDate(OffsetDateTime dateTime) {
        long value = 0;
        try {
            value = daysBetween(dateTime, DateUtil.getEndOfDay(dateTime));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value == 0;
    }

    public static OffsetDateTime convertTransmissionDateToInstant(String transmissionDate) {
        //format MMddHHmmss
        transmissionDate = transmissionDate.trim();
        return OffsetDateTime.now()
                .withMonth(Integer.parseInt(transmissionDate.substring(0, 2)))
                .withDayOfMonth(Integer.parseInt(transmissionDate.substring(2, 4)))
                .withHour(Integer.parseInt(transmissionDate.substring(4, 6)))
                .withMinute(Integer.parseInt(transmissionDate.substring(6, 8)))
                .withSecond(Integer.parseInt(transmissionDate.substring(8, 10)));
    }

    public  static String convertTransmissionDateToString(OffsetDateTime dateTime) {
        String format = "MMddHHmmss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(dateTime);
    }

    public  static String convertDateToYYYYMMDDFormat(OffsetDateTime dateTime) {
        String format = "YYYYMMdd";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(dateTime);
    }

    public static String convertDateToFormatString(String date) { //MMddHHmmss
        OffsetDateTime offsetDateTime = convertTransmissionDateToInstant(date);
        String format = "d MMM HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(offsetDateTime);
        //"EEE, d MMM yyyy HH:mm:ss Z"	Wed, 4 Jul 2001 12:08:56 -0700
    }

    public static long secondsBetween(OffsetDateTime date1, OffsetDateTime date2) {
        return date1.toEpochSecond() - date2.toEpochSecond();
    }

    public static OffsetDateTime parseOffsetDateTime(String offsetDateTime) { //2011-12-03T10:15:30+01:00
        return parseOffsetDateTime(offsetDateTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public static OffsetDateTime parseOffsetDateTime(String offsetDateTime, DateTimeFormatter formatter) {
        if (offsetDateTime == null) return null;
        return OffsetDateTime.parse(offsetDateTime.replace(" ", "+"), formatter);
    }

    public static OffsetDateTime parseLocalDate(String date, DateTimeFormatter formatter) {

        LocalDate date1 = LocalDate.parse(date, formatter);
        OffsetDateTime offsetDateTime1 = OffsetDateTime.now()
                .withYear(date1.getYear())
                .withMonth(date1.getMonthValue())
                .withDayOfMonth(date1.getDayOfMonth());
        return offsetDateTime1;
    }

    public static LocalDateTime toLocalDateTime(OffsetDateTime offsetDateTime) {
        if (offsetDateTime == null) return null;
        return LocalDateTime.ofInstant(offsetDateTime.toInstant(), ZoneId.systemDefault());
    }

    public static LocalDateTime toLocalDateTime(Long longDate) {
        if (longDate == null) return null;
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(longDate), ZoneId.systemDefault());
    }

    public static OffsetDateTime toOffsetDateTime(Long longDate) {
        if (longDate == null) return null;
        return OffsetDateTime.ofInstant(Instant.ofEpochMilli(longDate), ZoneId.systemDefault());
    }

    public static String convertToProgressFormat(String date)  {
        LocalDate localDate = LocalDate.parse(date);//"EEE, MMM d, ''yy"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yy");
        String formattedDate = formatter.format(localDate);
        System.out.println("Progress Formatted Date: " + formattedDate);
        return formattedDate;
    }

    public  static String convertToBMGFormat(OffsetDateTime dateTime) {
        String format = "YYYYMMddHHmmss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(dateTime);
    }

    public static void main(String[] args) {
        //LocalDate localDate = LocalDate.parse(date);//"EEE, MMM d, ''yy"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, MMM d, ''yyyy");
        String formattedDate = formatter.format(OffsetDateTime.now());
        System.out.println(formattedDate);
    }

}
