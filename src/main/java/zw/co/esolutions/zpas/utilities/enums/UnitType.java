package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by stanford on 12/2/16.
 */
public enum UnitType {
    DOLLARS,
    CENTS,
    SERVICE_UNITS,
    NONE
}
