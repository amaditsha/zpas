package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by tanatsa on 08 Jan 2019
 */

public enum AddressType {
    HOME,
    BUSINESS,
    BILLING,
    SHIPPING,
    CONTACT
}
