package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 14 Dec 2018
 */
public enum ClientType {
    COMPANY,
    INDIVIDUAL
}
