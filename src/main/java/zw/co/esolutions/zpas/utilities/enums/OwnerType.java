/**
 *
 */
package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred
 */
public enum OwnerType {
    FINANCIAL_INSTITUTION,
    OPERATOR,
    PAYMENT,
    CLIENT,
    BULK_PAYMENT
}
