package zw.co.esolutions.zpas.utilities.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import zw.co.esolutions.zpas.model.ObligationFulfilment;
import zw.co.esolutions.zpas.utilities.storage.StorageProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by alfred on 14 Mar 2019
 */
@Slf4j
public class PaymentsUtil {
    private static final String OUTPUT_FOLDER = StorageProperties.BASE_LOCATION;
    public static final String PAYMENTS_API_REQUESTS = StorageProperties.PAYMENTS_API_REQUESTS;
    public static final String PAYMENTS_API_REQUESTS_ARCHIVE = StorageProperties.PAYMENTS_API_REQUESTS_ARCHIVE;
    public static final String PAYMENTS_API_REQUESTS_ERROR = StorageProperties.PAYMENTS_API_REQUESTS_ERROR;
    public static final String PAYMENTS_API_RESPONSES = StorageProperties.PAYMENTS_API_RESPONSES;
    public static final String PAYMENTS_API_RESPONSES_ARCHIVE = StorageProperties.PAYMENTS_API_RESPONSES_ARCHIVE;

    public static Class<?> getObligationFulfilmentClass(final ObligationFulfilment obligationFulfilment) {
        return obligationFulfilment.getClass();
    }

    public static String getFileExtension(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            return "";
        }
        if (!fileName.contains(".")) {
            return "";
        }
        final String fileExtension = fileName.substring(fileName.lastIndexOf('.'));
        return fileExtension;
    }


    public static Map<Integer, List<String>> readExcelFile(String fileName) {
        switch (getFileExtension(fileName)) {
            case ".xls":
                return readXLSExcelFile(fileName);
            case ".xlsx":
                readXLSXExcelFile(fileName);
            default:
                throw new RuntimeException("Excel Processor could not read unknown file extension: " + getFileExtension(fileName));
        }
    }

    public static Map<Integer, List<String>> readXLSExcelFile(String fileName) {
        Path excelFile = Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName);

//        HSSFWorkbook, HSSFSheet, HSSFRow, and HSSFCell
        Map<Integer, List<String>> data = new HashMap<>();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(Files.newInputStream(excelFile));
            HSSFSheet sheet = workbook.getSheetAt(0);

            int i = 0;
            final Iterator<Row> hssRows = sheet.iterator();
            while (hssRows.hasNext()) {
                final HSSFRow row = (HSSFRow) hssRows.next();
                data.put(i, new ArrayList<>());
                final Iterator<Cell> cells = row.iterator();
                while (cells.hasNext()) {
                    final HSSFCell cell = (HSSFCell) cells.next();
                    switch (cell.getCellType()) {
                        case STRING: {
                            data.get(i).add(cell.getRichStringCellValue().getString());
                            break;
                        }
                        case NUMERIC: {
                            if (org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)) {
                                data.get(i).add(cell.getDateCellValue() + "");
                            } else {
                                data.get(i).add(cell.getNumericCellValue() + "");
                            }
                            break;
                        }
                        case BOOLEAN: {
                            data.get(i).add(cell.getBooleanCellValue() + "");
                            break;
                        }
                        case FORMULA: {
                            data.get(i).add(cell.getCellFormula() + "");
                            break;
                        }
                        default:
                            data.get(i).add(" ");
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static Map<Integer, List<String>> readXLSXExcelFile(String fileName) {
        Path excelFile = Paths.get(StorageProperties.BASE_LOCATION).resolve(fileName);

        Map<Integer, List<String>> data = new HashMap<>();
        try {
            Workbook workbook = new XSSFWorkbook(Files.newInputStream(excelFile));
            Sheet sheet = workbook.getSheetAt(0);

            int i = 0;
            for (Row row : sheet) {
                data.put(i, new ArrayList<>());
                for (Cell cell : row) {
                    switch (cell.getCellType()) {
                        case STRING: {
                            data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
                            break;
                        }
                        case NUMERIC: {
                            if (DateUtil.isCellDateFormatted(cell)) {
                                data.get(i).add(cell.getDateCellValue() + "");
                            } else {
                                data.get(i).add(cell.getNumericCellValue() + "");
                            }
                            break;
                        }
                        case BOOLEAN: {
                            data.get(i).add(cell.getBooleanCellValue() + "");
                            break;
                        }
                        case FORMULA: {
                            data.get(i).add(cell.getCellFormula() + "");
                            break;
                        }
                        default:
                            data.get(new Integer(i)).add(" ");
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * Unzip it
     *
     * @param zipFile input zip file
     */
    public static String unZipIt(String zipFile, String outputFolder) {

        byte[] buffer = new byte[1024];

        try {

            //create output directory is not exists
            File folder = new File(OUTPUT_FOLDER);
            if (!folder.exists()) {
                folder.mkdir();
            }

            //get the zip file content
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));

            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();
            List<String> fileNames = new ArrayList<>();

            while (ze != null) {
                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);

                log.info("file unzip : " + newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
                fileNames.add(newFile.getName());
            }

            zis.closeEntry();
            zis.close();

            return fileNames.get(0);

        } catch (IOException ex) {
            ex.printStackTrace();
            return "";
        }
    }
}
