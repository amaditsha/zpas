package zw.co.esolutions.zpas.utilities.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private static String location = "/opt/eSolutions/zpas/files/";
    private static String nssaRootLocation = "/opt/eSolutions/nssacontrib/files/";
    public static final String BASE_LOCATION = location;
    public static final String PAYMENTS_API_REQUESTS = location + "payments-api/requests/";
    public static final String PAYMENTS_API_REQUESTS_ARCHIVE = location + "payments-api/requests/archive/";
    public static final String PAYMENTS_API_REQUESTS_ERROR = location + "payments-api/requests/error/";
    public static final String PAYMENTS_API_RESPONSES = location + "payments-api/responses/";
    public static final String PAYMENTS_API_RESPONSES_ARCHIVE = location + "payments-api/responses/archive/";
    public static final String DEBIT_ORDER_BASE_LOCATION = location + "debit_order/";
    public static final String DEBIT_ORDER_LOCATION_REQUEST = location + "debit_order/sfi/request/";
    public static final String DEBIT_ORDER_LOCATION_RESPONSE = location + "debit_order/sfi/response/";
    public static final String DEBIT_ORDER_LOCATION_ARCHIVE= location + "debit_order/sfi/archive/";

    public static final String OUTGOING_DEBIT_ORDER_REQUEST=  location + "sfi/incoming/request/";

    public static final String INCOMING_ISO_ARCHIVE_FOLDER = location + "iso20022/incoming/archive/";
    public static final String OUTGOING_ISO_ARCHIVE_FOLDER = location + "iso20022/outgoing/archive/";

    public String getLocation() {
        return location;
    }

    public String getNssaRootLocation() {
        return nssaRootLocation;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
