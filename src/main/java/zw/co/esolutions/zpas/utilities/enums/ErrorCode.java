package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by stanford on 3/7/17.
 */

public enum ErrorCode {

        ZPE100("Transaction not honoured"),
        ZPE101("Duplicate reference"),
        ZPE102("Insufficient funds"),
        ZPE103("No account held"),
        ZPE104("Amount is below minimum"),
        ZPE105("Amount is above maximum"),
        ZPE106("Voucher not found"),
        ZPE107("Voucher not redeemable"),
        ZPE108("Meter not found"),
        ZPE109("Invalid transaction"),
        ZPE110("Customer not active"),
        ZPE111("Transaction error"),
        ZPE112("Original transaction failed"),
        ZPE113("Transaction state invalid"),
        ZPE114("Transaction not found"),
        ZPE115("Unsupported operation"),
        ZPE116("Insufficient parameters passed"),
        ZPE117("Invalid amount"),
        ZPE118("Request validation failed"),
        ZPE119("Configuration error"),
        ZPE120("Acquirer not registered"),
        ZPE121("Acquirer licensing failed"),
        ZPE122("Severe error"),
        ZPE123("Transaction timeout"),
        ZPE124("Payment provider not available"),
        ZPE125("Invalid reversal"),
        ZPE126("Insufficient funds acquirer"),
        ZPE127("Insufficient funds participant"),
        ZPE128("Meter is not allocated");

        ErrorCode(String description) {
            this.description = description;
        }

        private String description;

        public  String getDescription() {
            return this.description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
}
