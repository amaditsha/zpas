package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 20 Nov 2018
 */
public enum RequestType {
    CUSTOMER_INFO,
    BULK_PAYMENT,
    PAYMENT,
    BATCH_DISPATCH,
    WITHDRAW_BATCH,
    CLAIM_NON_RECEIPT,
    RECALL_BATCH,
    ACCOUNT_VALIDATION
}
