package zw.co.esolutions.zpas.utilities.enums;

public enum AccountType {
    COMPANY_CONTRIBUTIONS,
    COMPANY_CASH,
    PURCHASE_CONTROL,
    COMPANY_PAYOUTS,
    COMPANY_CASHIN,
    POLICY_CONTRIBUTIONS,
    GROUP_ALLOCATIONS,
    COMPANY_STOP_ORDER_CONTROL,
    STOP_ORDER_OPERATOR_MAIN,
    STOP_ORDER_OPERATOR_COMMISSION,
    ADJUSTMENT_CONTROL,
    ADJUSTMENT_CASH,
    ACQUIRER_CASH,
    BROKER_COMMISSION,
    AGENT_COMMISSION,
    COMPANY_COMMISSION_PAYOUTS,
    VAT
}
