package zw.co.esolutions.zpas.utilities.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class MoneyUtil {

    public static long convertToCents(double dollars) {
        long value = (long) (dollars * 200 + 1) / 2;
        return value;
    }

    public static double convertToDollars(long cents) {
        double dollars = (double) (cents);
        return dollars / 100;
    }

    public static String convertDollarsToPattern(double dollars) {
        DecimalFormat dFormat = Formats.moneyFormat;
        String formattedString = dFormat.format(dollars);
        return formattedString;
    }

    public static String convertCentsToDollarsPattern(long cents) {
        double dollars;
        try {
            dollars = (double) (cents);
        } catch (Exception e) {
            dollars = 0;
        }

        DecimalFormat dFormat = Formats.moneyFormat;
        String formattedString = dFormat.format(dollars / 100);
        return "$" + formattedString;
    }

    public static String convertToDollarsPattern(BigDecimal dollars) {
        if (dollars == null)
            dollars = BigDecimal.ZERO;
        long cents = dollars.multiply(BigDecimal.valueOf(100)).longValue();
        DecimalFormat dFormat = Formats.moneyFormat;

        String formattedString = dFormat.format(cents / 100);
        return "$" + formattedString;
    }

    public static String convertCentsToDollarsPatternNoCurrency(double amount) {
        DecimalFormat dFormat = Formats.moneyFormat;
        String formattedString = dFormat.format(amount);
        return formattedString;
    }

    public static String convertCentsToDollarsPatternNoCurrency(long cents) {
        double dollars = (double) (cents);
        return Formats.moneyFormat.format(dollars / 100);
    }

    public static String convertCentsToDollarsPatternCurrency(Double cents) {
        double dollars = (cents);
        DecimalFormat dFormat = Formats.moneyFormat;

        String formattedString = dFormat.format(dollars / 100);
        return "$" + formattedString;
    }


    public static void main(String... args) {
        double cents = 0.00;

        long dollars = convertToCents(cents);
        String test = "47.2";
        String test1 = convertCentsToDollarsPatternCurrency(Double.valueOf(test));
//		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>cents>>"+cents);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>dollars>>" + test1);

    }

}
