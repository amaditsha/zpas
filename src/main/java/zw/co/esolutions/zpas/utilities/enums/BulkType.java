package zw.co.esolutions.zpas.utilities.enums;

/**
 * Created by alfred on 09 Nov 2018
 */
public enum BulkType {
    CREDIT,
    DEBIT,
    NONE
}
