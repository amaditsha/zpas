$("#non-financial-institutions-link").addClass("mm-active");
$("#non-financial-institutions-link-search").addClass("mm-active");


function edit_tax_details(final_id) {
    var id = final_id.replace(/EditBtn/g, '');
    console.log('The organisation id for edit tax details is ' + final_id);

    console.log('tax id ' + id);

    $("#editTaxId").val(id);

    //Getting the details from the view
    var bICFI = $("#bICFI" + id).text();

    //Setting the details in the modal form
    $("#editBICFI").val(bICFI);


    $('#editTaxDetails').modal('show');
}

$("#virtualId").on("keyup", function () {
    var virtualIdInfoDanger = $("#virtualIdInfoErrors");
    var virtualIdInfo = $("#virtualIdInfo");
    var virtualId = this.value;
    if(virtualId) {
        if(virtualId.length < 2) {
            virtualIdInfoDanger.text("Virtual Id too short. Please provide a valid Id.");
        } else {
            $.ajax({
                url: "/zpas/api/json/virtual-ids/check/" + virtualId,
                method: "get",
                dataType: "json",
                contentType: "application/json",
                success: function(data) {
                    console.log("Received data from", JSON.stringify(data));
                    if(data.available == false) {
                        console.log("Data Available is false: ", data.available);
                        virtualIdInfoDanger.text("Virtual Id \"" + data.virtualId + "\" already taken by " + data.partyName);
                    } else {
                        console.log("Data Available is true: ", data.available);
                        virtualIdInfoDanger.text("");
                        virtualIdInfo.text("Virtual Id is available.");
                    }
                },
                error: function(data) {
                    console.log("Error: ", JSON.stringify(data));
                }
            });
        }
    } else {
        virtualIdInfoDanger.text("Virtual Id invalid. Please provide a valid Id.");
    }
});

$("#editTaxDetailForm").submit(function (e) {
    e.preventDefault();
    $.post('/zpas/financial-institutions/edit/organisation/identification/details',
        $(this).serialize(),
        function (response) {
            console.log("Response | " + JSON.stringify(response));
            // when there are an error then show error
            if (response.responseCode === '00') {
                //Notification
                console.log("Success");
                console.log("Response object ID: " + response.object.id)
                $('#bICFI' + response.object.id).text(response.object.bICFI);

            } else {
                //Notification
            }
            $('#editTaxDetails').modal('hide');
        }
    );
    return false;
});

$("#submitNonFinancialInstitutionTriggerButton").on("click", function () {
    console.log("Trigger clicked.");
    var nonFinancialInstitutionId = $("#financialInstitutionIdSpan").text();
    $("#confirmationMessage").html("<span>You are about to submit non financial institution.</span><br><br><span>Are you sure?</span>");
    $("#actionSpan").text("submit");
});

$("#approveNonFinancialInstitutionTriggerButton").on("click", function () {
    console.log("Trigger clicked.");
    var nonFinancialInstitutionId = $("#financialInstitutionIdSpan").text();
    $("#confirmationMessage").html("<span>You are about to approve this non financial institution.</span><br><br><span>Are you sure?</span>");
    $("#actionSpan").text("approve");
});

$("#rejectNonFinancialInstitutionTriggerButton").on("click", function () {
    console.log("Trigger clicked.");
    var nonFinancialInstitutionId = $("#financialInstitutionIdSpan").text();
    $("#confirmationMessage").html("<span style='color: red'>You are about to reject a non financial institution.</span>" +
        "<br><br><span style='color: red'>Are you sure?</span>");
    $("#actionSpan").text("reject");
});

$("#deleteNonFinancialInstitutionTriggerButton").on("click", function () {
    console.log("Trigger clicked.");
    var nonFinancialInstitutionId = $("#financialInstitutionIdSpan").text();
    $("#confirmationMessage").html("<span style='color: red'>You are about to delete financial institution</span>" +
        "<br><br><span style='color: red'>Are you sure?</span>");
    $("#actionSpan").text("delete");
});

$("#confirmActionButton").on("click", function () {
    console.log("Confirm clicked.");
    var requestedAction = $("#actionSpan").text();
    switch (requestedAction) {
        case "submit":
            submitNonFinancialInstitution();
            break;
        case "approve":
            approveNonFinancialInstitution();
            break;
        case "reject":
            rejectNonFinancialInstitution();
            break;
        case "delete":
            deleteNonFinancialInstitution();
            break;
    }
});

function rejectNonFinancialInstitution() {
    var nonFinancialInstitutionId = $("#financialInstitutionIdSpan").text();
    var rejectFinancialInstitutionLink = "/zpas/non-financial-institutions/reject/" + nonFinancialInstitutionId;
    var actionLink = $("#financialInstitutionActionLink");
    console.log("Action Link is:", actionLink);
    actionLink.prop("href", rejectFinancialInstitutionLink);
    actionLink.find("span").click();
}

function deleteNonFinancialInstitution() {
    var nonFinancialInstitutionId = $("#financialInstitutionIdSpan").text();
    var deleteFinancialInstitutionLink = "/zpas/non-financial-institutions/delete/" + nonFinancialInstitutionId;
    var actionLink = $("#financialInstitutionActionLink");
    console.log("Action Link is:", actionLink);
    actionLink.prop("href", deleteFinancialInstitutionLink);
    actionLink.find("span").click();
}

function approveNonFinancialInstitution() {
    var nonFinancialInstitutionId = $("#financialInstitutionIdSpan").text();
    var approveFinancialInstitutionLink = "/zpas/non-financial-institutions/approve/" + nonFinancialInstitutionId;
    var actionLink = $("#financialInstitutionActionLink");
    console.log("Action Link is:", actionLink);
    actionLink.prop("href", approveFinancialInstitutionLink);
    actionLink.find("span").click();
}

function submitNonFinancialInstitution() {
    var nonFinancialInstitutionId = $("#financialInstitutionIdSpan").text();
    var submitFinancialInstitutionLink = "/zpas/non-financial-institutions/submit/" + nonFinancialInstitutionId;
    var actionLink = $("#financialInstitutionActionLink");
    console.log("Action Link is:", actionLink);
    actionLink.prop("href", submitFinancialInstitutionLink);
    actionLink.find("span").click();
}