/**
 Created by alfred on 28 Mar 2019
 */

$(document).ready(function () {

    $("#profiles-link").addClass("mm-active");
    $("#profiles-link-users").addClass("mm-active");

    $("#person").on("change", function () {
        console.log("Something happened on the client id.");
        var personId = this.value;
        $.ajax({
            url: "/zpas/api/json/clients/find/single",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                personId: personId
            }),
            success: function (data) {
                if (data) {
                    $("#firstNames").val(data.firstNames);
                    $("#lastName").val(data.lastName);
                    $("#username").val(data.email);
                    $("#email").val(data.email);
                    $("#phoneNumber").val(data.phoneNumber);
                    $("#mobileNumber").val(data.mobileNumber);
                } else {
                    console.log("Person not found.");
                }
            },
            error: function (data) {
                console.log("An error occurred while trying to fetch person. Error Message:", JSON.stringify(data));
            }
        });
    });

    $("#profileLevel").on("change", function () {
        if (this.value == 'FINANCIAL_INSTITUTION') {
            $("#financialInstitutionDiv").show();
            $("#searchPersonDiv").hide();
        } else if (this.value == 'SYSTEM') {
            $("#financialInstitutionDiv").hide();
        } else {
            $("#searchPersonDiv").show();
        }
    });

    var userRoleLevel = $("#userRoleLevelSpan").text();
    $.ajax({
        url: "/zpas/profile/applicable/roles/" + userRoleLevel,
        method: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            var roleHtml = "";
            $.each(data, function (idx, role) {
                roleHtml += "<option value='" + role.id + "'>" + role.name + "</option>";
            });
            $("#userRoles").html(roleHtml);
        },
        error: function (data) {

        }
    });

    $("#email").on("input", function() {
        var email = this.value;
        if(email) {
            $.ajax({
                url: "/zpas/api/json/validation/email",
                method: "get",
                contentType: "application/json",
                dataType: "json",
                data: {
                    email: email
                },
                success: function (data) {
                    if (data.valid) {
                        $("#emailInfo").text("Email is valid");
                        $("#emailError").text("");

                        $.ajax({
                            url: "/zpas/api/json/usernames/check",
                            method: "get",
                            contentType: "application/json",
                            dataType: "json",
                            data: {
                                username: email
                            },
                            success: function (data) {
                                if (data.available) {
                                    $("#emailInfo").text(data.extraInformation);
                                    $("#emailError").text("");
                                    $("#submitButton").prop("disabled", false);
                                } else {
                                    $("#emailInfo").text("");
                                    $("#emailError").text(data.extraInformation);
                                    $("#submitButton").prop("disabled", true);
                                }
                            },
                            error: function (data) {
                                console.log("An error occurred. Failed to get email validation response. Error: ", JSON.stringify(data));
                            }
                        });

                    } else {
                        $("#emailInfo").text("");
                        $("#emailError").text("Email \"" + email + "\" is invalid");
                        $("#submitButton").prop("disabled", true);
                    }
                },
                error: function (data) {
                    console.log("An error occurred. Failed to get email validation response. Error: ", JSON.stringify(data));
                }
            });
        } else {
            $("#emailError").text("This field is required.");
            $("#submitButton").prop("disabled", true);
        }
    });

    $("#username").on("input", function() {
        var username = this.value;
        if(username) {
            $.ajax({
                url: "/zpas/api/json/usernames/check",
                method: "get",
                contentType: "application/json",
                dataType: "json",
                data: {
                    username: username
                },
                success: function (data) {
                    if (data.available) {
                        $("#usernameInfo").text(data.extraInformation);
                        $("#usernameError").text("");
                        $("#submitButton").prop("disabled", false);
                    } else {
                        $("#usernameInfo").text("");
                        $("#usernameError").text(data.extraInformation);
                        $("#submitButton").prop("disabled", true);
                    }
                },
                error: function (data) {
                    console.log("An error occurred. Failed to get email validation response. Error: ", JSON.stringify(data));
                }
            });
        } else {
            $("#usernameError").text("This field is required.");
            $("#submitButton").prop("disabled", true);
        }
    });

    $("#person").select2({
        minimumInputLength: 1,
        placeholder: "Search for a person",
        allowClear: true,
        ajax: {
            url: "/zpas/api/json/clients/search",
            dataType: "json",
            contentType: "application/json",
            delay: 250,
            data: function (params) {
                var personType = "";
                var financialInstitutionId = $("#financialInstitution").val();
                if (!financialInstitutionId) {
                    financialInstitutionId = "";
                }
                console.log("FI ID is:", financialInstitutionId);
                var profileLevel = $("#profileLevel").val();
                if (userRoleLevel == 'SYSTEM' && profileLevel == 'FINANCIAL_INSTITUTION') {
                    personType = 'FI_EMPLOYEE';
                } else if (userRoleLevel == 'FINANCIAL_INSTITUTION' && profileLevel == 'FINANCIAL_INSTITUTION') {
                    personType = 'FI_EMPLOYEE';
                } else if (userRoleLevel == 'FINANCIAL_INSTITUTION' && profileLevel == 'CLIENT') {
                    personType = 'ACCOUNT_HOLDER';
                } else if (userRoleLevel == 'CLIENT') {
                    personType = "NON_FI_EMPLOYEE";
                }

                var queryParameters = {
                    search: params.term,
                    personType: personType,
                    organisationId: financialInstitutionId
                }

                return queryParameters;
            },
            processResults: function (data) {
                console.log("Received data is:", JSON.stringify(data));
                var formattedData = [];
                $.each(data, function (idx, dataItem) {
                    var d = {
                        id: dataItem.personId,
                        text: dataItem.personFullName
                    };
                    formattedData.push(d);
                });
                return {
                    results: formattedData
                };
            }
        }
    });
});