
$("#generalDetailsSbmtBtn").click(function (e) {
    $("#editGeneralDetailForm").submit();
});

$("#rejectMandate").click(function () {
    var id = $("#globalCashAccountMandateId").text();
    console.log('The reject mandate id is ' + id);

    console.log('Reject id ' + id);

    $("#rejectOrDeleteMandateTitle").text("Confirm Reject");
    // $("#rejectOrDeleteMandateSubTitle").text("Are you sure, you want to reject this?");
    $("#rejectOrDeleteMandateId").val(id);

    $("#submitRejectMandate").show();
    $("#submitDeleteMandate").hide();
});

$("#deleteMandate").click(function () {
    var id = $("#globalCashAccountMandateId").text();
    console.log('The delete mandate id is ' + id);

    console.log('Delete id ' + id);

    $("#rejectOrDeleteMandateTitle").text("Confirm Delete");
    // $("#rejectOrDeleteMandateSubTitle").text("Are you sure, you want to reject this?");
    $("#rejectOrDeleteMandateId").val(id);

    $("#submitRejectMandate").hide();
    $("#submitDeleteMandate").show();
});

$("#submitRejectMandate").click(function () {
    console.log('Rejecting the mandate');
    var description = $("#rejectOrDeleteMandateStatusDescription").val();
    var id = $("#globalCashAccountMandateId").text();
    var cashAccountId = $("#globalCashAccountId").text();

    $("#rejectOrDeleteMandatLink").prop("href", "/zpas/cash-account-mandate/reject/" + id + "/account/" + cashAccountId + "/zpas/" + description);
    $("#rejectOrDeleteMandatLink").find("span").click();
});


$("#submitDeleteMandate").click(function () {
    console.log('Rejecting the mandate');
    var description = $("#rejectOrDeleteMandateStatusDescription").val();
    var id = $("#globalCashAccountMandateId").text();
    var cashAccountId = $("#globalCashAccountId").text();

    $("#rejectOrDeleteMandatLink").prop("href", "/zpas/cash-account-mandate/delete/" + id + "/account/" + cashAccountId + "/" + description);
    $("#rejectOrDeleteMandatLink").find("span").click();
});

$("#EditGeneralDetail").click(function () {
    var id = $("#EditGeneralDetailID").text();
    console.log('The edit for general details id is ' + id);

    console.log('Edit id ' + id);


    $("#editId").val(id);
});

$("#signatureConditionSmtBtn").click(function (e) {
    $("#editSignatureConditionForm").submit();
});

$("#EditSignatureCondition").click(function () {
    var id = $("#signatureConditionIdC").text();
    console.log('The edit for signature condition id is ' + id);

    console.log('Edit signature condition id ' + id);

    $("#signatureConditionId").val(id);
    var rightIndicator = $("#signatoryRightIndicatorC").html();
    console.log("Right indicator value " + rightIndicator);
    if(rightIndicator === "true") {
        $("#signatoryRightIndicator").prop('checked', true);
    }
    var orderIndicator = $("#signatureOrderIndicatorC").html();
    console.log("Order indicator value " + orderIndicator);
    if(orderIndicator === "true") {
        $("#signatureOrderIndicator").prop('checked', true);
    }
    $("#requiredSignatureNumber").val($("#requiredSignatureNumberC").html());
    $("#signatureOrder").val($("#signatureOrderC").html());

    get_signatories();
});

$(document).ready(function () {
    var mandateIds = [];
    $.each($(".mandate-row"), function (index, mandateRow) {
        var mandateId = mandateRow.id;
        mandateIds.push(mandateId);
    });
    console.log("Mandate IDs are: ", mandateIds);

    if (mandateIds.length > 0) {
        $.ajax({
            url: "/zpas/cash-account-mandate/get/mandate/holder",
            dataType: "json",
            contentType: "application/json",
            method: "post",
            data: JSON.stringify(mandateIds),
            success: function (persons) {
                console.log("Number of mandate infos:", persons.length);
                $.each(persons, function (index, person) {
                    console.log("Person Info:", JSON.stringify(person));

                    if(person.title === 'Holder') {
                        var term = " (" + person.extra + ") " + person.name;
                        $("#ID" + person.id).text(term);
                        $("#TITLE" + person.id).text('Holder');

                    } else {
                        $("#ID" + person.id).text(person.name);
                        $("#TITLE" + person.id).text(person.title);
                    }
                });

            },
            error: function (data) {
                console.log("An error occurred. Message was ", JSON.stringify(data));
            }
        }).done(function () {
            console.log("Done filling in mandate information");
        });
    }

    getPersonsNameForArraysUserId();
});

function getPersonsNameForArraysUserId() {
    var signatureOrderStr = $("#signatureOrderC").text();
    var signatureOrdeIds = signatureOrderStr.split(",");

    console.log("The order ids " + signatureOrdeIds);

    if (signatureOrdeIds.length > 0) {
        $.ajax({
            url: "/zpas/cash-account-mandate/get/mandate/person",
            dataType: "json",
            contentType: "application/json",
            method: "post",
            data: JSON.stringify(signatureOrdeIds),
            success: function (persons) {
                console.log("Number of mandate infos:", persons.length);
                var personInfo = [];
                $.each(persons, function (index, person) {
                    console.log("Person Info:", JSON.stringify(person));
                    personInfo.push(person.name)
                });
                $("#signatureOrderC").text(personInfo.toString());
            },
            error: function (data) {
                console.log("An error occurred. Message was ", JSON.stringify(data));
            }
        }).done(function () {
            console.log("Done filling in mandate information");
        });
    }
}

function edit_accountcontract(final_id) {
    var id = final_id.replace(/EditBtn/g, '');
    console.log('The edit for account contract id is ' + final_id);

    console.log('Edit id ' + id);

    var description = $("#description" + id).text();
    var version = $("#version" + id).text();
    var urgencyFlag = $("#urgencyFlag" + id).text();
    var removalIndicator = $("#removalIndicator" + id).text();
    var transactionChannel = $("#transactionChannel" + id).text();
    var requestDate = $("#requestDate" + id).text();
    var targetGoLiveDate = $("#targetGoLiveDate" + id).text();
    var targetClosingDate = $("#targetClosingDate" + id).text();
    //requestDate targetGoLiveDate targetClosingDate


    $("#editContractId").val(id);
    $("#accountContractDescription").val(description);
    $("#accountContractVersion").val(version);
    $("#urgencyFlag").val(urgencyFlag);
    $("#removalIndicator").val(removalIndicator);
    // $("#transactionChannel").val(transactionChannel);
    // $("#requestDate").val(requestDate);
    // $("#targetGoLiveDate").val(targetGoLiveDate);
    // $("#targetClosingDate").val(targetClosingDate);
    // $("#editName").val(collegeName);
    $('#editAccountContract').modal('show');
}

/*<![CDATA[*/
$(document).ready(function () {
    $("#editGeneralDetailForm").submit(function (e) {
        console.log('The mandate general details form submitted');
        e.preventDefault();
        $.post('/zpas/cash-account-mandate/edit/generaldetails',
            $(this).serialize(),
            function (response) {
                console.log("Response | " + JSON.stringify(response));
                // when there are an error then show error
                if (response.responseCode === '00') {
                    //Notification
                    console.log("Success");
                    $('#description' + response.object.id).text(response.object.description);
                    $('#version' + response.object.id).text(response.object.version);
                    $('#identification' + response.object.id).text(response.object.mandateIdentification);
                    $('#status' + response.object.id).text(response.object.entityStatus);
                    // $('#trackingDays' + response.object.id).text(response.object.trackingDays);
                    // $('#rate' + response.object.id).text(response.object.rate);
                    // $('#name' + response.object.id).text(response.object.name);
                } else {
                    //Notification
                    console.log('Error while editing the mandate');
                }
                // $('#editGeneralDetails').modal('hide');
            }
        );
        return false;
    });
});

$("#editSignatureConditionForm").submit(function (e) {
    console.log('The mandate signature condition form submitted');
    e.preventDefault();
    $.post('/zpas/signature-condition/edit/ajax',
        $(this).serialize(),
        function (response) {
            console.log("Response | " + JSON.stringify(response));
            // when there are an error then show error
            if (response.responseCode === '00') {
                //Notification
                console.log("Success");
                $("#signatoryRightIndicatorC").text(response.object.signatoryRightIndicator);
                $("#signatureOrderC").text(response.object.signatureOrder);
                $("#globalSignatureCondition").text(response.object.signatureOrder);
                $("#signatureOrderIndicatorC").text(response.object.signatureOrderIndicator);
                $("#requiredSignatureNumberC").text(response.object.requiredSignatureNumber);

                getPersonsNameForArraysUserId();
            } else {
                //Notification
                console.log('Error while editing the mandate');
            }
            // $('#editGeneralDetails').modal('hide');
        }
    );
    return false;
});

$(document).ready(function () {
    $("#editAccountContractForm").submit(function (e) {
        e.preventDefault();
        $.post('/zpas/cash-account-mandate/edit/accountcontract',
            $(this).serialize(),
            function (response) {
                console.log("Response | " + JSON.stringify(response));
                // when there are an error then show error
                if (response.responseCode === '00') {
                    //Notification
                    console.log("Success");
                    $('#description' + response.object.id).text(response.object.description);

                    $("#version" + response.object.id).text(response.object.version);
                    $("#urgencyFlag" + response.object.id).text(response.object.urgencyFlag);
                    $("#removalIndicator" + response.object.id).text(response.object.removalIndicator);
                    // $("#transactionChannel" + response.object.id).text(response.object.transactionChannel);
                } else {
                    //Notification
                }
                $('#editGeneralDetails').modal('hide');
            }
        );
        return false;
    });
});
