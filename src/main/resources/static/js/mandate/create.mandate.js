var mandateHoldersTemp = [];
var formattedData = [];
var result = [];
var mandateHolderRoles = [];

$("#signatureOrderIndicator").change(function () {
    console.log('Signature indicator changed ...');
    if (this.checked) {
        //Do stuff
        $("#signatureOrderDiv").show();

    } else {
        $("#signatureOrder").val("");
        $("#signatureOrderDiv").hide();
    }
});

$("#cashAccountMandate").on("click", function () {
    buildForm();
});

function buildForm() {
    var cashAccountMandateForm = $("#cashAccountMandateForm");
    $("#cashAccountMandateFormElements .form-control").each(function () {
        var input = $(this); // This is the jquery object of the input, do what you will
        cashAccountMandateForm.append(input);
    });
    console.log("Generated form is " + cashAccountMandateForm);
    cashAccountMandateForm.submit();
}

function getPersonOptionObject(mandateId) {
    for (i = 0; i < formattedData.length; i++) {
        if (formattedData[i].id == mandateId) {
            return formattedData[i];
        }
    }
    return {};
}

$("#mandateHolderIds").on("change", function () {
    var mandateIds = $("#mandateHolderIds").val();

    $("#signatureOrder").empty();
    $("#mandateRoleTable tr").remove();
    html = '';
    var i = 1;
    mandateHolderRoles = [];

    $.each(mandateIds, function (idx, mandateId) {
        var mandateObject = getPersonOptionObject(mandateId);
        // $("#signatureOrder").append("<option value='" + mandateObject.id + "'>" + mandateObject.text + "</option>");

        var roleTemp = mandateObject.id + "|" + "INITIATE";
        mandateHolderRoles.push(roleTemp);

        html += "<tr id='s" +mandateObject.id+ " '><td>" + i + "</td><td>" + mandateObject.text + "</td>" +
            "<td>" +
            "<select type='select' class='custom-select' onchange='selectedMandateHolderType(this);'>" +
            "    <option value='INITIATE' id='"+mandateObject.id+"'>" + "INITIATE" + "</option>" +
            "    <option value='AUTHORISE' id='"+mandateObject.id+"'>" + "AUTHORISE" + "</option>" +
            "    <option value='BOTH' id='"+mandateObject.id+"'>" + "BOTH" + "</option>" +
            "</select>" +
            "</td>" +
            "</tr>";
        i = i + 1;

    });
    $("#mandateHolderRolesTemp").val(mandateHolderRoles);
    console.log("The role values | " + JSON.stringify(mandateHolderRoles));
    $("#mandateRoleTable").append(html);
    console.log("==========================================");
    console.log("The mandate holder ids are:", mandateIds);
    console.log("==========================================");
});

function selectedMandateHolderType(value) {
    var val = $(value).children("option:selected").val();
    var id = $(value).children("option:selected").attr("id");

    if(val === "AUTHORISE" || val === "BOTH") {
        console.log("Adding the value to the signature order | " + value);
        var mandateObject = getPersonOptionObject(id);
        $("#signatureOrder").append("<option value='" + mandateObject.id + "'>" + mandateObject.text + "</option>");
    } else {
        $("#signatureOrder option[value='" + id + "']").remove();
    }

    var tempRoles = [];
    mandateHolderRoles.forEach(function (value1, index) {
        if(value1.includes(id)) {
            return;
        }
        tempRoles.push(value1)
    });
    mandateHolderRoles = tempRoles;
    var roleTemp = id + "|" + val;
    mandateHolderRoles.push(roleTemp);
    console.log("The map values | " + JSON.stringify(mandateHolderRoles));
    $("#mandateHolderRolesTemp").val(mandateHolderRoles);
}

$("#mandateHolderIds").select2({
    minimumInputLength: 1,
    placeholder: "Search for a person",
    allowClear: true,
    ajax: {
        url: "/zpas/cash-account-mandate/search/persons/",
        dataType: "json",
        contentType: "application/json",
        delay: 250,
        data: function (params) {
            var cashAccountId = $("#cashAccountId").val();

            var queryParameters = {
                search: params.term,
                cashAccountId: cashAccountId
            };
            return queryParameters;
        },
        processResults: function (data) {
            // formattedData = [];
            $.each(data, function (idx, dataItem) {
                var d = {
                    id: dataItem.personId,
                    text: dataItem.personFullName + " [" + dataItem.nationalId + "]"
                };
                addUniqueMandateHolder(d);
            });
            return {
                results: formattedData
            };
        }
    }
});

$("#signatureOrder").select2();

function addUniqueMandateHolder(mandateHolder) {
    var accountOwnerPersonId = $("#accountOwnerPersonId").text();
    console.log("The account owner person id is " + accountOwnerPersonId.toString() + " and the mandate id is "  + mandateHolder.id);
    for (i = 0; i < formattedData.length; i++) {
        if (formattedData[i].id == mandateHolder.id) {
            return;
        }
    }
    if(mandateHolder.id == accountOwnerPersonId.toString()) {
        console.log("Return without adding to the results");
        return;
    }
    formattedData.push(mandateHolder);
    return {};
}

$(document).ready(function () {

    $("#mandateIdentificationtd").html($("#mandateIdentification").val());

    $("#description").on('change', function (e) {
        $("#descriptiontd").html($("#description").val());
    });

    $("#version").on('change', function (e) {
        $("#versiontd").html($("#version").val());
    });

    $("#mandateHolderIds").on('change', function (e) {
        var order = [];
        var mandateHolder = $("#mandateHolderIds").val();
        console.log("mandate holder -- " + mandateHolder);
        var mandateHolderArr = mandateHolder.toString().split(",");
        mandateHolderArr.forEach(function (value, index) {

            var mandateObject = getPersonOptionObject(value);
            console.log("index -- " + index + " | -- value " + mandateObject.text);
            order.push(mandateObject.text);
        });
        console.log("mandate holder list arr : " + order.toString());
        $("#mandateHolderIdstd").html(order.toString());
    });

    $("#signatureOrderIndicator").on('change', function (e) {
        if (this.checked) {
            $("#signatureOrderIndicatortd").html("true");
        } else {
            $("#signatureOrderIndicatortd").html("false");
        }
    });

    $("#signatoryRightIndicator").on('change', function (e) {
        if (this.checked) {
            $("#signatoryRightIndicatortd").html("true");
        } else {
            $("#signatoryRightIndicatortd").html("false");
        }
    });

    $("#requiredSignatureNumber").on('change', function (e) {
        $("#requiredSignatureNumbertd").html($("#requiredSignatureNumber").val());
    });

    $("#signatureOrder").on('change', function (e) {
        var order = [];
        var signatureOrder = $("#signatureOrder").val();
        console.log("signature order " + signatureOrder);
        var signatureOrderArr = signatureOrder.toString().split(",");
        var requiredSignatureNumber = $("#requiredSignatureNumber").val();
        console.log('The required signature number is ' + requiredSignatureNumber + ' length of arr is ' + signatureOrderArr.length);
        if(signatureOrderArr.length <= requiredSignatureNumber) {
            signatureOrderArr.forEach(function (value, index) {
                var mandateObject = getPersonOptionObject(value);
                order.push(mandateObject.text);
            });
            $("#signatureOrdertd").html(order.toString());

        } else {
            console.log("Reset the signature order");
            $("#signatureOrdertd").html("");
            $("#signatureOrder").val("");
        }
    });

    $("#transactionalChannelCodetd").html($("#transactionChannelCode").val());

    $("#requestDate").on('change', function (e) {
        var requestDate = $("#requestDate").val();
        $("#requestDatetd").html(requestDate);
    });


    $("#toDateTime").on('change', function (e) {
        var toDateTime = $("#toDateTime").val();
        $("#toDateTimetd").html(toDateTime.toString().replace("T", " "));
    });



    $("#fromDateTime").on('change', function (e) {
        var fromDateTime = $("#fromDateTime").val();
        $("#fromDateTimetd").html(fromDateTime.toString().replace("T", " "));
    });

});
