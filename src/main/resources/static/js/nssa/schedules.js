/**
 * Create by alfred on 22 Jul 2019
 */
$("#submitContributionScheduleTriggerButton").on("click", function () {
    var contributionScheduleId = $("#contributionScheduleIdSpan").text();
    var contributionMonth = $("#contributionMonthSpan").text();
    $("#confirmationMessage").html("<b>You are about to submit contribution schedule for "
        + contributionMonth + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("submit");
});

$("#approveContributionScheduleTriggerButton").on("click", function () {
    var contributionScheduleId = $("#contributionScheduleIdSpan").text();
    var contributionMonth = $("#contributionMonthSpan").text();
    $("#confirmationMessage").html("<b>You are about to approve contribution schedule for "
        + contributionMonth + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("approve");
});

$("#rejectContributionScheduleTriggerButton").on("click", function () {
    contributionScheduleActionId = $("#contributionScheduleIdSpan").text();
    var contributionMonth = $("#contributionMonthSpan").text();
    $("#confirmationMessage").html("<b style='color: red'>You are about to reject contribution schedule for "
        + contributionMonth + " </b><br><br><b style='color: red'>Are you sure?</b>");
    $("#actionSpan").text("reject");
});

$("#deleteContributionScheduleTriggerButton").on("click", function () {
    var contributionScheduleId = $("#contributionScheduleIdSpan").text();
    var contributionMonth = $("#contributionMonthSpan").text();
    $("#confirmationMessage").html("<b style='color: red'>You are about to delete contribution schedule for "
        + contributionMonth + " </b><br><br><b style='color: red'>Are you sure?</b>");
    $("#actionSpan").text("delete");
});

$("#confirmActionButton").on("click", function () {
    console.log("Confirm action button clicked.");
    var requestedAction = $("#actionSpan").text();
    switch (requestedAction) {
        case "submit":
            submitContributionSchedule();
            break;
        case "approve":
            approveContributionSchedule();
            break;
        case "reject":
            rejectContributionSchedule();
            break;
        case "delete":
            deleteContributionSchedule();
            break;
    }
});

function rejectContributionSchedule() {
    var contributionScheduleId = contributionScheduleActionId;
    var rejectContributionScheduleLink = "/zpas/nssa/payments/reject/" + contributionScheduleId;
    var actionLink = $("#contributionScheduleActionLink");
    actionLink.prop("href", rejectContributionScheduleLink);
    actionLink.find("span").click();
}

function deleteContributionSchedule() {
    var contributionScheduleId = $("#contributionScheduleIdSpan").text();
    var deleteContributionScheduleLink = "/zpas/nssa/payments/delete/" + contributionScheduleId;
    var actionLink = $("#contributionScheduleActionLink");
    actionLink.prop("href", deleteContributionScheduleLink);
    actionLink.find("span").click();
}

function approveContributionSchedule() {
    var contributionScheduleId = $("#contributionScheduleIdSpan").text();
    var approveContributionScheduleLink = "/zpas/nssa/payments/approve/" + contributionScheduleId;
    var actionLink = $("#contributionScheduleActionLink");
    actionLink.prop("href", approveContributionScheduleLink);
    actionLink.find("span").click();
}

function submitContributionSchedule() {
    var contributionScheduleId = $("#contributionScheduleIdSpan").text();
    var submitContributionScheduleLink = "/zpas/nssa/payments/submit/" + contributionScheduleId;
    var actionLink = $("#contributionScheduleActionLink");
    actionLink.prop("href", submitContributionScheduleLink);
    actionLink.find("span").click();
}