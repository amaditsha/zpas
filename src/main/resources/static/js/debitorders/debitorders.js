$("#submitDebitOrderTriggerButton").on("click", function() {
    var debitOrdersId = $("#debitOrderBatchNumberSpan").text();
    $("#confirmationMessage").html("<b>You are about to submit debit order batch with reference "
        + debitOrdersId + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("submit");
});

$("#approveDebitOrderTriggerButton").on("click", function() {
    var debitOrdersId = $("#debitOrderBatchNumberSpan").text();
    $("#confirmationMessage").html("<b>You are about to approve debit order batch with reference "
        + debitOrdersId + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("approve");
});

$("#initiateDebitOrderTriggerButton").on("click", function() {
    var debitOrdersId = $("#debitOrderBatchNumberSpan").text();
    $("#confirmationMessage").html("<b>You are about to initiate debit order batch with reference "
        + debitOrdersId + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("initiate");
});

$("#rejectDebitOrderTriggerButton").on("click", function() {
    var debitOrdersId = $("#debitOrderBatchNumberSpan").text();
    $("#confirmationMessage").html("<b style='color: red'>You are about to reject debit order batch with reference "
        + debitOrdersId + " </b><br><br><b style='color: red'>Are you sure?</b>");
    $("#actionSpan").text("reject");
});

$("#deleteDebitOrderTriggerButton").on("click", function() {
    var debitOrdersId = $("#debitOrderBatchNumberSpan").text();
    $("#confirmationMessage").html("<b style='color: red'>You are about to delete debit order batch with reference "
        + debitOrdersId + " </b><br><br><b style='color: red'>Are you sure?</b>");
    $("#actionSpan").text("delete");
});

$("#confirmActionButton").on("click", function() {
    var requestedAction = $("#actionSpan").text();
    switch (requestedAction) {
        case "submit":
            submitDebitOrderBatch();
            break;
        case "approve":
            approveDebitOrderBatch();
            break;
        case "initiate":
            initiateDebitOrderBatch();
            break;
        case "reject":
            rejectDebitOrderBatch();
            break;
        case "delete":
            deleteDebitOrderBatch();
            break;
    }
});

function rejectDebitOrderBatch() {
    var debitOrdersId = $("#debitOrdersIdSpan").text();
    var rejectDebitOrdersLink = "/zpas/debit-orders/reject/" + debitOrdersId;
    var actionLink = $("#debitOrderActionLink");
    actionLink.prop("href", rejectDebitOrdersLink);
    actionLink.find("span").click();
}

function deleteDebitOrderBatch() {
    var debitOrdersId = $("#debitOrdersIdSpan").text();
    var deleteDebitOrdersLink = "/zpas/debit-orders/delete/" + debitOrdersId;
    var actionLink = $("#debitOrderActionLink");
    actionLink.prop("href", deleteDebitOrdersLink);
    actionLink.find("span").click();
}

function approveDebitOrderBatch() {
    var debitOrdersId = $("#debitOrdersIdSpan").text();
    var approveDebitOrdersLink = "/zpas/debit-orders/approve/" + debitOrdersId;
    var actionLink = $("#debitOrderActionLink");
    actionLink.prop("href", approveDebitOrdersLink);
    actionLink.find("span").click();
}

function initiateDebitOrderBatch() {
    var debitOrdersId = $("#debitOrdersIdSpan").text();
    var initiateDebitOrdersLink = "/zpas/debit-orders/initiate/" + debitOrdersId;
    var actionLink = $("#debitOrderActionLink");
    actionLink.prop("href", initiateDebitOrdersLink);
    actionLink.find("span").click();
}

function submitDebitOrderBatch() {
    var debitOrdersId = $("#debitOrdersIdSpan").text();
    var submitDebitOrdersLink = "/zpas/debit-orders/submit/" + debitOrdersId;
    var actionLink = $("#debitOrderActionLink");
    actionLink.prop("href", submitDebitOrdersLink);
    actionLink.find("span").click();
}