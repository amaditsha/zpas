$("#code").on("keyup", function () {
    var virtualCodeInfoDanger = $("#virtualCodeInfoErrors");
    var virtualCodeInfo = $("#virtualCodeInfo");
    var code = this.value;
    if(code) {
        if(code.length != 3) {
            virtualCodeInfoDanger.text("Currency Code should be 3 characters.");
        } else {
            $.ajax({
                url: "/zpas/api/json/currency-codes/check/" + code,
                method: "get",
                dataType: "json",
                contentType: "application/json",
                success: function(data) {
                    console.log("Received data from", JSON.stringify(data));
                    if(data.available == false) {
                        console.log("Data Available is false: ", data.available);
                        virtualCodeInfoDanger.text("Currency Code \"" + data.code + "\" already taken."+ "\" already taken by ", data.codeName);
                    } else {
                        console.log("Data Available is true: ", data.available);
                        virtualCodeInfoDanger.text("");
                        virtualCodeInfo.text("Currency Code is available.");
                    }
                },
                error: function(data) {
                    console.log("Error: ", JSON.stringify(data));
                }
            });
        }
    } else {
        virtualCodeInfoDanger.text("Currency Code invalid. Please provide a valid Currency Code.");
    }
});