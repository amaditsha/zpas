var colors = ['#007bff','#ff0000','#00ff00','#c3e6cb','#dc3545','#6c757d'];

$(document).ready(function() {
    $.ajax({
        url: "/zpas/resolution/center/status/dashboard/fetch",
        method: "get",
        dataType: "json",
        contentType: "application/json",
        success: function(data) {
            console.log("Fetched data successfully.", JSON.stringify(data));
            loadDonuts(data);
        },
        error: function (data) {
            console.log("An error occurred.", JSON.stringify(data));
        }
    });
});

/* 3 donut charts */
function loadDonuts(data) {

// donut 1
    var chDonutData1 = {
        labels: ['Open', 'Closed','Total'],
        datasets: [
            {
                backgroundColor: colors.slice(0, 3),
                borderWidth: 0,
                data: [data.totalOpenInvestigations, data.totalClosedInvestigations, data.totalInvestigations]
            }
        ]
    };

    var chDonut1 = document.getElementById("chDonut1");
    if (chDonut1) {
        new Chart(chDonut1, {
            type: 'pie',
            data: chDonutData1,
            options: {
                cutoutPercentage: 85,
                legend: {position: 'bottom', padding: 5, labels: {pointStyle: 'circle', usePointStyle: true}},
                title: {
                    display: true,
                    text: 'All Cases'
                }
            }
        });
    }

}

$("#submitCaseSearchBtn").click(function (e) {

    $("#caseInput").val($("#caseInputTemp").val());
    $("#searchDateRange").val($("#searchDateRangeTemp").val());
    $("#openedOn").val($("#openedOnTemp").is(":checked"));

    $( "#caseResolutionForm" ).submit();

    /*$.ajax({
        url: "/zpas/resolution/center",
        method: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            caseInput: searchValue,
            caseFilter: searchFilter,
            caseRange: searchDateRange
        }),
        success: function (data) {
            if(data) {
                console.log("Returned success response is | " + JSON.stringify(data));
            } else {
                console.log("Cases were not found");
            }
        },
        error: function (data) {
            console.log("An error occurred while trying to fetch case resolutions. Error Message:", JSON.stringify(data));
        }
    });*/

});

$("#extraFilterCaseSearchBtn").click(function (e) {
    console.log("The extra filter button pressed. Values below");
    $( "#caseResolutionForm" ).submit();
});

$("#openedOnTemp").change(function () {
    if(this.checked) {
        $("#filterSection").show();
    } else {
        $("#filterSection").hide();
    }

});

/*Getting the statuses for the investigation cases*/

function display_status(tmpId) {
    var id = tmpId.replace(/STAT/g,'');
    $.ajax({
        url: "/zpas/resolution/center/case/status/" + id,
        dataType: "json",
        contentType: "application/json",
        method: "get",
        success: function (statuses) {
            console.log("Number of case status :", statuses.length);
            $("#totalNumberOfCases").html(statuses.length);
            $("#progressBarForCases").attr("style", "width:"+statuses.length);
            html = '';
            var i = 1;
            $.each(statuses, function (index, status) {
                console.log("Case Status Info:", JSON.stringify(status));
                html += "<tr><td>" + i + "</td><td>" + status.caseStatusCode + "</td><td>" + status.statusDescription + "</td><td>" + status.date + "</td></tr>";
                i = i + 1;
            });

            if(statuses.length === 0){
                $(".no-case-status").show();
            }
            $("#investigationCaseStatusTable tr").remove();
            $("#investigationCaseStatusTable").append(html);
        },
        error: function (data) {
            console.log("An error occurred. Message was ", JSON.stringify(data));
        }
    }).done(function () {
        console.log("Done filling in case status information");
    });



    $("#BtnToTriggerCase")[0].click();
}


/*Getting the resolution for the investigation cases*/

function display_resolutions(tmpId) {
    var id = tmpId.replace(/RESOL/g, '');

    $.ajax({
        url: "/zpas/resolution/center/case/resolution/" + id,
        dataType: "json",
        contentType: "application/json",
        method: "get",
        success: function (statuses) {
            console.log("Number of case resolutions :", statuses.length);
            $("#totalNumberOfResolutions").html(statuses.length);
            $("#progressBarForResolutions").attr("style", "width:"+statuses.length);
            html = '';
            var i = 1;
            $.each(statuses, function (index, status) {
                console.log("Case Resolution Info:", JSON.stringify(status));
                html += "<tr><td>" + i + "</td><td>" + status.investigationStatus + "</td><td>" + status.investigationCaseReference + "</td><td>" + status.date + "</td></tr>";
                i = i + 1;
            });

            if(statuses.length === 0){
                $(".no-case-resolution").show();
            }
            $("#investigationCaseResolutionTable tr").remove();
            $("#investigationCaseResolutionTable").append(html);
        },
        error: function (data) {
            console.log("An error occurred. Message was ", JSON.stringify(data));
        }
    }).done(function () {
        console.log("Done filling in case resolution information");
    });

    $("#BtnToTriggerResolution")[0].click();
}