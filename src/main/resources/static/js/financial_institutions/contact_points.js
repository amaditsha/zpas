/**
 Created by alfred on 15 Feb 2019
 */
var LOCAL_STORAGE_PHONES = "phones";
var LOCAL_STORAGE_ELECTRONIC_ADDRESSES = "electronicAddresses";

var phones = [];
var electronicAddresses = [];

function restoreContactPoints() {
    //fetch this from local storage
    var phonesJson = localStorage.getItem(LOCAL_STORAGE_PHONES);
    var electronicAddressesJson = localStorage.getItem(LOCAL_STORAGE_ELECTRONIC_ADDRESSES);
    if (phonesJson) {
        phones = JSON.parse(phonesJson);
    } else {
        phones = [];

    }
    if (electronicAddressesJson) {
        electronicAddresses = JSON.parse(electronicAddressesJson);
    } else {
        electronicAddresses = [];
    }
    refreshPhonesTable();
    refreshElectronicAddressesTable();
}

$(document).ready(function () {
    console.log("Page reloaded");
    restoreContactPoints();
});

$(window).bind('beforeunload', function () {
    //save info somewhere
    //return 'are you sure you want to leave?';
    // savePhones();
    // saveElectronicAddresses();
});


$("#submitButton").on("click", function () {
    var addressesJson = JSON.stringify(electronicAddresses);
    var phonesJson = JSON.stringify(phones);
    $("#addressesJson").val(addressesJson);
    $("#phonesJson").val(phonesJson);
    clearContactInformation();
});

function clearContactInformation() {
    clearElectronicAddresses();
    clearPhones();
}

function setLocalStorageItem(key, value) {
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        localStorage.setItem(key, value);
    } else {
        // Sorry! No Web Storage support..
        console.log("Sorry! No Web Storage support...");
    }
}

function clearElectronicAddresses() {
    //fetch this from local storage
    electronicAddresses = [];
    localStorage.clear();
    refreshElectronicAddressesTable();
}

function clearPhones() {
    //fetch this from local storage
    phones = [];
    localStorage.clear();
    refreshPhonesTable();
}

$("#addAddressBtn").on("click", function () {
    addElectronicAddress();
});

$("#addPhoneBtn").on("click", function () {
    addPhone();
});

function addPhone() {
    var phoneNumber = $("#phoneNumber").val();
    var mobileNumber = $("#mobileNumber").val();
    var faxNumber = $("#faxNumber").val();

    function noPhoneFieldDefined() {
        var noPhoneField = (!phoneNumber && !mobileNumber && !faxNumber);
        if(noPhoneField) {
            $("#phonesInfo").show("slow");
        } else {
            $("#phonesInfo").hide("slow");
        }
        return noPhoneField;
    }

    if(noPhoneFieldDefined()) {
        return;
    }

    var phone = {};
    if(!phones) {
        phones = [];
    }
    phone.id = phones.length;
    phone.phoneNumber = phoneNumber;
    phone.mobileNumber = mobileNumber;
    phone.faxNumber = faxNumber;

    phones.push(phone);
    clearPhoneFields();
    savePhones();
    refreshPhonesTable();
}

function clearPhoneFields() {
    $("#phoneNumber").val("");
    $("#mobileNumber").val("");
    $("#faxNumber").val("");
}

function addElectronicAddress() {
    var emailAddress = $("#emailAddress").val();
    var uRLAddress = $("#uRLAddress").val();
    var telexAddress = $("#telexAddress").val();
    var teletextAddress = $("#teletextAddress").val();
    var iSDNAddress = $("#iSDNAddress").val();

    function noAddressFieldDefined() {
        var noAddressField = (!emailAddress && !uRLAddress && !teletextAddress && !teletextAddress && !iSDNAddress);
        if(noAddressField) {
            $("#addressesInfo").show("slow");
        } else {
            $("#addressesInfo").hide("slow");
        }
        return noAddressField;
    }

    if(noAddressFieldDefined()) {
        return;
    }

    var electronicAddress = {};
    if(!electronicAddresses) {
        electronicAddresses = [];
    }

    electronicAddress.id = electronicAddresses.length;
    electronicAddress.emailAddress = emailAddress;
    electronicAddress.uRLAddress = uRLAddress;
    electronicAddress.telexAddress = telexAddress;
    electronicAddress.teletextAddress = teletextAddress;
    electronicAddress.iSDNAddress = iSDNAddress;

    electronicAddresses.push(electronicAddress);
    clearElectronicAddressFields();
    saveElectronicAddresses();
    refreshElectronicAddressesTable();
}

function clearElectronicAddressFields() {
    $("#emailAddress").val("");
    $("#uRLAddress").val("");
    $("#telexAddress").val("");
    $("#teletextAddress").val("");
    $("#iSDNAddress").val("");
}

function refreshPhonesTable() {
    var phonesTable = $("#phone-addresses-table-body");
    var phonesHtml = "";
    $.each(phones, function (index, phone) {
        var rowHtml = "<tr>" +
            "<td>" + phone.phoneNumber + "</td>" +
            "<td>" + phone.mobileNumber + "</td>" +
            "<td>" + phone.faxNumber + "</td>" +
            "<td><button onclick=\"deletePhone(" + phone.id + ")\" class=\"mb-2 mr-2 btn btn-danger\">Delete</button></td>" +
            "</tr>";
        phonesHtml += rowHtml;
    });
    phonesTable.html(phonesHtml);
}

function refreshElectronicAddressesTable() {
    var electronicAddressesTable = $("#electronic-addresses-table-body");
    var electronicAddressesHtml = "";
    $.each(electronicAddresses, function (index, address) {
        var rowHtml = "<tr>" +
            "<td>" + address.emailAddress + "</td>" +
            "<td>" + address.uRLAddress + "</td>" +
            "<td>" + address.telexAddress + "</td>" +
            "<td>" + address.teletextAddress + "</td>" +
            "<td>" + address.iSDNAddress + "</td>" +
            "<td><button onclick=\"deleteElectronicAddress(" + address.id + ")\" class=\"mb-2 mr-2 btn btn-danger\">Delete</button></td>" +
        "</tr>";
        electronicAddressesHtml += rowHtml;
    });
    electronicAddressesTable.html(electronicAddressesHtml);
}

function savePhones() {
    setLocalStorageItem(LOCAL_STORAGE_PHONES, JSON.stringify(phones));
}

function saveElectronicAddresses() {
    setLocalStorageItem(LOCAL_STORAGE_ELECTRONIC_ADDRESSES, JSON.stringify(electronicAddresses));
}

function deletePhone(phoneId) {
    if(phones.length == 1) {
        phones.splice(0, 1);
    }
    phones.splice(phoneId, 1);
    savePhones();
    refreshPhonesTable();
}

function deleteElectronicAddress(addressId) {
    if(electronicAddresses.length == 1) {
        electronicAddresses.splice(0, 1);
    }
    electronicAddresses.splice(addressId, 1);
    saveElectronicAddresses();
    refreshElectronicAddressesTable();
}