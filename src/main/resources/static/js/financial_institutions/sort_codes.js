var LOCAL_STORAGE_SORT_CODES = "sortCodes";

var sortCodes = [];

function restoreSortCodes() {
    //fetch this from local storage
    var sortCodesJson = localStorage.getItem(LOCAL_STORAGE_SORT_CODES);
    if (sortCodesJson) {
        sortCodes = JSON.parse(sortCodesJson);
    } else {
        sortCodes = [];

    }
    refreshSortCodesTable();
}

$(document).ready(function () {
    console.log("Page reloaded");
    restoreSortCodes();
});

$("#submitButton").on("click", function () {
    var sortCodesJson = JSON.stringify(sortCodes);
    $("#sortCodesJson").val(sortCodesJson);
    clearSortCodeInformation();
});

function clearSortCodeInformation() {
    clearSortCodes();
}

function setLocalStorageItem(key, value) {
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        localStorage.setItem(key, value);
    } else {
        // Sorry! No Web Storage support..
        console.log("Sorry! No Web Storage support...");
    }
}

function clearSortCodes() {
    //fetch this from local storage
    sortCodes = [];
    localStorage.clear();
    refreshSortCodesTable();
}

$("#addSortCodeBtn").on("click", function () {
    addSortCode();
});

function addSortCode() {
    var sortCode = $("#sortCode").val();

    function noSortCodeFieldDefined() {
        var noSortCodeField = (!sortCode);
        if(noSortCodeField) {
            $("#sortCodesInfo").show("slow");
        } else {
            $("#sortCodesInfo").hide("slow");
        }
        return noSortCodeField;
    }

    if(noSortCodeFieldDefined()) {
        return;
    }

    var scode = {};
    if(!sortCodes) {
        sortCodes = [];
    }
    scode.id = scode.length;
    scode.sortCode = sortCode;

    sortCodes.push(scode);
    clearSortCodesField();
    saveSortCodes();
    refreshSortCodesTable();
}

function clearSortCodesField() {
    $("#sortCode").val("");
}

function refreshSortCodesTable() {
    var sortCodesTable = $("#sort-codes-table-body");
    var sortCodesHtml = "";
    $.each(sortCodes, function (index, sortCode) {
        var rowHtml = "<tr>" +
            "<td>" + sortCode.sortCode + "</td>" +
            "<td><button onclick=\"deleteSortCode(" + sortCode.id + ")\" class=\"mb-2 mr-2 btn btn-danger\">Remove</button></td>" +
            "</tr>";
        sortCodesHtml += rowHtml;
    });
    sortCodesTable.html(sortCodesHtml);
}

function saveSortCodes() {
    setLocalStorageItem(LOCAL_STORAGE_SORT_CODES, JSON.stringify(sortCodes));
}

function deleteSortCode(sortCodeId) {
    if(sortCodes.length == 1) {
        sortCodes.splice(0, 1);
    }
    sortCodes.splice(sortCodeId, 1);
    saveSortCodes();
    refreshSortCodesTable();
}

