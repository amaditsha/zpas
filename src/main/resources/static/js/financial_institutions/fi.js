$("#financial-institutions-link").addClass("mm-active");
$("#financial-institutions-link-search").addClass("mm-active");

function edit_general_information_details(final_id) {
    var id = final_id.replace(/EditBtn/g, '');
    console.log('The FI edit ID for general information details id is ' + final_id);

    console.log('person id ' + id);
    var collegeName = $("#name" + id).text();

    $("#editGeneralInformationId").val(id);

    //Getting the details from the view
    var purpose = $("#purpose" + id).text();


    //Setting the details in the modal form
    $("#editPurpose").val(purpose);


    $('#editGeneralInformationDetails').modal('show');
}

$("#bicFIIdentifier").on("input", function() {
    var bic = this.value;
    $.ajax({
        url: "/zpas/api/json/validation/bic",
        method: "get",
        contentType: "application/json",
        dataType: "json",
        data: {
            bic: bic
        },
        success: function(data) {
            if(data.valid) {
                $("#bicFIIdentifierInfo").text("BIC is valid");
                $("#bicFIIdentifierError").text("");
            } else {
                $("#bicFIIdentifierInfo").text("");
                $("#bicFIIdentifierError").text("BIC \"" + bic + "\" is invalid");
            }
        },
        error: function(data) {
            console.log("An error occurred. Failed to get email validation response. Error: ", JSON.stringify(data));
        }
    });
});

$("#anyBICIdentifier").on("input", function() {
    var bic = this.value;
    $.ajax({
        url: "/zpas/api/json/validation/bic",
        method: "get",
        contentType: "application/json",
        dataType: "json",
        data: {
            bic: bic
        },
        success: function(data) {
            if(data.valid) {
                $("#anyBICIdentifierInfo").text("BIC is valid");
                $("#anyBICIdentifierError").text("");
            } else {
                $("#anyBICIdentifierInfo").text("");
                $("#anyBICIdentifierError").text("BIC \"" + bic + "\" is invalid");
            }
        },
        error: function(data) {
            console.log("An error occurred. Failed to get email validation response. Error: ", JSON.stringify(data));
        }
    });
});

// $("input[type=date]").datepicker({
//     dateFormat: 'yy-mm-dd',
//     onSelect: function(dateText, inst) {
//         $(inst).val(dateText); // Write the value in the input
//     }
// });
//
// // Code below to avoid the classic date-picker
// $("input[type=date]").on('click', function() {
//     return false;
// });

$("#editGeneralInformationDetailForm").submit(function (e) {
    e.preventDefault();
    $.post('/zpas/financial-institutions/edit/general/information/details',
        $(this).serialize(),
        function (response) {
            console.log("Response | " + JSON.stringify(response));
            // when there are an error then show error
            if (response.responseCode === '00') {
                //Notification
                console.log("Success");
                console.log("Response object ID: " + response.object.id)
                $('#purpose' + response.object.id).text(response.object.purpose);

            } else {
                //Notification
            }
            $('#editGeneralInformationDetails').modal('hide');
        }
    );
    return false;
});

function edit_organisation_identification_details(final_id) {
    var id = final_id.replace(/EditBtn/g, '');
    console.log('The organisation id for edit identification details is ' + final_id);

    console.log('person identification id ' + id);
    var collegeName = $("#name" + id).text();

    $("#editOrganisationIdentificationId").val(id);

    //Getting the details from the view
    var bICFI = $("#bICFI" + id).text();
    var anyBIC = $("#anyBIC" + id).text();
    var legalName = $("#legalName" + id).text();
    var shortName = $("#shortName" + id).text();
    var tradingName = $("#tradingName" + id).text();

    //Setting the details in the modal form
    $("#editBICFI").val(bICFI);
    $("#editAnyBIC").val(anyBIC);
    $("#editLegalName").val(legalName);
    $("#editTradingName").val(tradingName);
    $("#editShortName").val(shortName);


    $('#editOrganisationIdentificationDetails').modal('show');
}

$("#virtualSuffix").on("keyup", function () {
    var virtualSuffixInfoDanger = $("#virtualSuffixInfoErrors");
    var virtualSuffixInfo = $("#virtualSuffixInfo");
    var virtualSuffix = this.value;
    if(virtualSuffix) {
        if(virtualSuffix.length < 2) {
            virtualSuffixInfoDanger.text("Virtual Suffix too short. Please provide a valid suffix.");
        } else {
            $.ajax({
                url: "/zpas/api/json/virtual-suffices/check/" + virtualSuffix,
                method: "get",
                dataType: "json",
                contentType: "application/json",
                success: function(data) {
                    console.log("Received data from", JSON.stringify(data));
                    if(data.available == false) {
                        console.log("Data Available is false: ", data.available);
                        virtualSuffixInfoDanger.text("Virtual suffix \"" + data.virtualSuffix + "\" already taken by ", data.financialInstitutionName);
                    } else {
                        console.log("Data Available is true: ", data.available);
                        virtualSuffixInfoDanger.text("");
                        virtualSuffixInfo.text("Virtual suffix is available.");
                    }
                },
                error: function(data) {
                    console.log("Error: ", JSON.stringify(data));
                }
            });
        }
    } else {
        virtualSuffixInfoDanger.text("Virtual Suffix invalid. Please provide a valid suffix.");
    }
});

$("#virtualId").on("keyup", function () {
    var virtualIdInfoDanger = $("#virtualIdInfoErrors");
    var virtualIdInfo = $("#virtualIdInfo");
    var virtualId = this.value;
    if(virtualId) {
        if(virtualId.length < 2) {
            virtualIdInfoDanger.text("Virtual Id too short. Please provide a valid Id.");
        } else {
            $.ajax({
                url: "/zpas/api/json/virtual-ids/check/" + virtualId,
                method: "get",
                dataType: "json",
                contentType: "application/json",
                success: function(data) {
                    console.log("Received data from", JSON.stringify(data));
                    if(data.available == false) {
                        console.log("Data Available is false: ", data.available);
                        virtualIdInfoDanger.text("Virtual Id \"" + data.virtualId + "\" already taken by " + data.partyName);
                    } else {
                        console.log("Data Available is true: ", data.available);
                        virtualIdInfoDanger.text("");
                        virtualIdInfo.text("Virtual Id is available.");
                    }
                },
                error: function(data) {
                    console.log("Error: ", JSON.stringify(data));
                }
            });
        }
    } else {
        virtualIdInfoDanger.text("Virtual Id invalid. Please provide a valid Id.");
    }
});

$("#editOrganisationIdentificationDetailForm").submit(function (e) {
    e.preventDefault();
    $.post('/zpas/financial-institutions/edit/organisation/identification/details',
        $(this).serialize(),
        function (response) {
            console.log("Response | " + JSON.stringify(response));
            // when there are an error then show error
            if (response.responseCode === '00') {
                //Notification
                console.log("Success");
                console.log("Response object ID: " + response.object.id)
                $('#bICFI' + response.object.id).text(response.object.bICFI);
                $('#anyBIC' + response.object.id).text(response.object.anyBIC);
                $('#legalName' + response.object.id).text(response.object.legalName);
                $('#shortName' + response.object.id).text(response.object.shortName);
                $('#tradingName' + response.object.id).text(response.object.tradingName);

            } else {
                //Notification
            }
            $('#editOrganisationIdentificationDetails').modal('hide');
        }
    );
    return false;
});

function edit_tax_details(final_id) {
    var id = final_id.replace(/EditBtn/g, '');
    console.log('The organisation id for edit tax details is ' + final_id);

    console.log('tax id ' + id);

    $("#editTaxId").val(id);

    //Getting the details from the view
    var bICFI = $("#bICFI" + id).text();

    //Setting the details in the modal form
    $("#editBICFI").val(bICFI);


    $('#editTaxDetails').modal('show');
}

$("#editTaxDetailForm").submit(function (e) {
    e.preventDefault();
    $.post('/zpas/financial-institutions/edit/organisation/identification/details',
        $(this).serialize(),
        function (response) {
            console.log("Response | " + JSON.stringify(response));
            // when there are an error then show error
            if (response.responseCode === '00') {
                //Notification
                console.log("Success");
                console.log("Response object ID: " + response.object.id)
                $('#bICFI' + response.object.id).text(response.object.bICFI);

            } else {
                //Notification
            }
            $('#editTaxDetails').modal('hide');
        }
    );
    return false;
});

$("#submitFinancialInstitutionTriggerButton").on("click", function () {
    console.log("Trigger clicked.");
    var financialInstitutionId = $("#financialInstitutionIdSpan").text();
    $("#confirmationMessage").html("<span>You are about to submit financial institution.</span><br><br><span>Are you sure?</span>");
    $("#actionSpan").text("submit");
});

$("#approveFinancialInstitutionTriggerButton").on("click", function () {
    console.log("Trigger clicked.");
    var financialInstitutionId = $("#financialInstitutionIdSpan").text();
    $("#confirmationMessage").html("<span>You are about to approve this financial institution.</span><br><br><span>Are you sure?</span>");
    $("#actionSpan").text("approve");
});

$("#rejectFinancialInstitutionTriggerButton").on("click", function () {
    console.log("Trigger clicked.");
    var financialInstitutionId = $("#financialInstitutionIdSpan").text();
    $("#confirmationMessage").html("<span style='color: red'>You are about to reject a financial institution.</span>" +
        "<br><br><span style='color: red'>Are you sure?</span>");
    $("#actionSpan").text("reject");
});

$("#deleteFinancialInstitutionTriggerButton").on("click", function () {
    console.log("Trigger clicked.");
    var financialInstitutionId = $("#financialInstitutionIdSpan").text();
    $("#confirmationMessage").html("<span style='color: red'>You are about to delete financial institution</span>" +
        "<br><br><span style='color: red'>Are you sure?</span>");
    $("#actionSpan").text("delete");
});

$("#confirmActionButton").on("click", function () {
    console.log("Confirm clicked.");
    var requestedAction = $("#actionSpan").text();
    switch (requestedAction) {
        case "submit":
            submitFinancialInstitution();
            break;
        case "approve":
            approveFinancialInstitution();
            break;
        case "reject":
            rejectFinancialInstitution();
            break;
        case "delete":
            deleteFinancialInstitution();
            break;
    }
});

function rejectFinancialInstitution() {
    var financialInstitutionId = $("#financialInstitutionIdSpan").text();
    var rejectFinancialInstitutionLink = "/zpas/financial-institutions/reject/" + financialInstitutionId;
    var actionLink = $("#financialInstitutionActionLink");
    console.log("Action Link is:", actionLink);
    actionLink.prop("href", rejectFinancialInstitutionLink);
    actionLink.find("span").click();
}

function deleteFinancialInstitution() {
    var financialInstitutionId = $("#financialInstitutionIdSpan").text();
    var deleteFinancialInstitutionLink = "/zpas/financial-institutions/delete/" + financialInstitutionId;
    var actionLink = $("#financialInstitutionActionLink");
    console.log("Action Link is:", actionLink);
    actionLink.prop("href", deleteFinancialInstitutionLink);
    actionLink.find("span").click();
}

function approveFinancialInstitution() {
    var financialInstitutionId = $("#financialInstitutionIdSpan").text();
    var approveFinancialInstitutionLink = "/zpas/financial-institutions/approve/" + financialInstitutionId;
    var actionLink = $("#financialInstitutionActionLink");
    console.log("Action Link is:", actionLink);
    actionLink.prop("href", approveFinancialInstitutionLink);
    actionLink.find("span").click();
}

function submitFinancialInstitution() {
    var financialInstitutionId = $("#financialInstitutionIdSpan").text();
    var submitFinancialInstitutionLink = "/zpas/financial-institutions/submit/" + financialInstitutionId;
    var actionLink = $("#financialInstitutionActionLink");
    console.log("Action Link is:", actionLink);
    actionLink.prop("href", submitFinancialInstitutionLink);
    actionLink.find("span").click();
}