$("#mandateHoldersButton").click(function (e) {
    var id = $("#cashAccountId").text();
    console.log('The cash account id is ' + id);
    get_mandate_holders(id);

    get_initiators(id);
});

function get_mandate_holders(cashAccountId) {
    console.log('Get the mandate holders ... with id ' + cashAccountId);
    $.ajax({
        url: "/zpas/cash-account-mandate/get/mandate/holders/" + cashAccountId,
        dataType: "json",
        contentType: "application/json",
        method: "get",
        success: function (persons) {
            console.log("Number of mandate infos:", persons.length);
            html = '';
            var i = 1;
            $.each(persons, function (index, person) {
                console.log("Person Info:", JSON.stringify(person));
                html += "<tr>" +
                    "<td class='text-center text-muted'>" + i + "</td>" +
                    "<td><div class='widget-heading'>" + person.name + "</div></td>" +
                    "<td class='text-center'> <div class='badge badge-pill pl-2 pr-2 badge-warning'>" + person.extra.toUpperCase() + "</div></td></tr>";
                i = i + 1;
            });
            $("#signaturesSignatoriesTable tbody > tr").remove();
            $("#signaturesSignatoriesTable tbody").append(html);
        },
        error: function (data) {
            console.log("An error occurred. Message was ", JSON.stringify(data));
        }
    }).done(function () {
        console.log("Done filling in mandate information");
    });
}



$("#signatoryBtn").click(function () {
    $(".signatories-table").show();
    $(".initiator-table").hide();
    $(this).addClass("active");
    $("#initiatorBtn").removeClass("active");
});

$("#initiatorBtn").click(function () {
    $(".signatories-table").hide();
    $(".initiator-table").show();
    $(this).addClass("active");
    $("#signatoryBtn").removeClass("active");
});


function get_initiators(cashAccountId) {
    console.log('Get the initiators ... with id ' + cashAccountId);
    $.ajax({
        url: "/zpas/cash-account-mandate/get/initiators/" + cashAccountId,
        dataType: "json",
        contentType: "application/json",
        method: "get",
        success: function (persons) {
            console.log("Number of initiator infos:", persons.length);
            html = '';
            var i = 1;
            $.each(persons, function (index, person) {
                console.log("Person Info:", JSON.stringify(person));
                html += "<tr>" +
                    "<td class='text-center text-muted'>" + i + "</td>" +
                    "<td><div class='widget-heading'>" + person.name + "</div></td>" +
                    "<td class='text-center'> <div class='badge badge-pill pl-2 pr-2 badge-success'>" + person.extra.toUpperCase() + "</div></td></tr>";
                i = i + 1;
            });
            $("#signaturesInitiatorTable tbody > tr").remove();
            $("#signaturesInitiatorTable tbody").append(html);
        },
        error: function (data) {
            console.log("An error occurred. Message was ", JSON.stringify(data));
        }
    }).done(function () {
        console.log("Done filling in mandate information");
    });
}

