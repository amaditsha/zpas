/**
 Created by alfred on 23 Apr 2019
 */

$("#virtualCode").on("keyup", function () {
    var virtualCodeInfoDanger = $("#virtualCodeInfoErrors");
    var virtualCodeInfo = $("#virtualCodeInfo");
    var virtualCode = this.value;
    if(virtualCode) {
        if(virtualCode.length < 2) {
            virtualCodeInfoDanger.text("Currency Code too short. Please provide a valid Code.");
        } else {
            $.ajax({
                url: "/zpas/api/json/virtual-codes/check/" + virtualCode,
                method: "get",
                dataType: "json",
                contentType: "application/json",
                success: function(data) {
                    console.log("Received data from", JSON.stringify(data));
                    if(data.available == false) {
                        console.log("Data Available is false: ", data.available);
                        virtualCodeInfoDanger.text("Virtual Code \"" + data.virtualCode + "\" already taken.");
                    } else {
                        console.log("Data Available is true: ", data.available);
                        virtualCodeInfoDanger.text("");
                        virtualCodeInfo.text("Virtual Code is available.");
                    }
                },
                error: function(data) {
                    console.log("Error: ", JSON.stringify(data));
                }
            });
        }
    } else {
        virtualCodeInfoDanger.text("Virtual Code invalid. Please provide a valid Code.");
    }
});