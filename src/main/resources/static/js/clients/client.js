var formattedData = [];
$(document).ready(function () {
    console.log("Document ready!");
});

$("#finishCreateClient").on("click", function () {
    buildForm();
});

function buildForm() {
    var clientForm = $("#clientForm");

    $("#clientFormElements .form-control").each(function(){
        var input = $(this); // This is the jquery object of the input, do what you will
        clientForm.append(input);
    });
    console.log("Generated form is " + clientForm);
    clientForm.submit();
}

$("#employingParty").select2({
    minimumInputLength: 1,
    placeholder: "Search for an employer",
    allowClear: true,
    ajax: {
        url: "/zpas/clients/search/employers/",
        dataType: "json",
        contentType: "application/json",
        delay: 250,
        data: function (params) {
            var queryParameters = {
                search: params.term
            };
            return queryParameters;
        },
        processResults: function (data) {
            formattedData = [];
            $.each(data, function (idx, dataItem) {
                var d = {
                    id: dataItem.organisationId,
                    text: dataItem.organisationName
                };
                console.log("The response data | " + d);
                formattedData.push(d);
            });
            return {
                results: formattedData
            };
        }
    }
});

$("#virtualId").on("keyup", function () {
    var virtualIdInfoDanger = $("#virtualIdInfoErrors");
    var virtualIdInfo = $("#virtualIdInfo");
    var virtualId = this.value;
    if(virtualId) {
        if(virtualId.length < 2) {
            virtualIdInfoDanger.text("Virtual Id too short. Please provide a valid Id.");
        } else {
            $.ajax({
                url: "/zpas/api/json/virtual-ids/check/" + virtualId,
                method: "get",
                dataType: "json",
                contentType: "application/json",
                success: function(data) {
                    console.log("Received data from", JSON.stringify(data));
                    if(data.available == false) {
                        console.log("Data Available is false: ", data.available);
                        virtualIdInfoDanger.text("Virtual Id \"" + data.virtualId + "\" already taken by " + data.partyName);
                    } else {
                        console.log("Data Available is true: ", data.available);
                        virtualIdInfoDanger.text("");
                        virtualIdInfo.text("Virtual Id is available.");
                    }
                },
                error: function(data) {
                    console.log("Error: ", JSON.stringify(data));
                }
            });
        }
    } else {
        virtualIdInfoDanger.text("Virtual Id invalid. Please provide a valid Id.");
    }
});


$("#identityCardNumber").on("input", function() {
    var nationalId = this.value;
    if(nationalId) {
        $.ajax({
            url: "/zpas/api/json/validation/national-id",
            method: "get",
            contentType: "application/json",
            dataType: "json",
            data: {
                nationalId: nationalId
            },
            success: function (data) {
                if (data.valid) {
                    $("#identityCardNumberInfo").text("National ID number is valid");
                    $("#identityCardNumberError").text("");

                } else {
                    $("#identityCardNumberInfo").text("");
                    $("#identityCardNumberError").text("National ID \"" + nationalId + "\" is invalid");
                    $("#submitButton").prop("disabled", true);
                }
            },
            error: function (data) {
                console.log("An error occurred. Failed to get National Id number validation response. Error: ", JSON.stringify(data));
            }
        });
    } else {
        $("#identityCardNumberError").text("This field is required.");
        $("#submitButton").prop("disabled", true);
    }
});
