/**
 Created by alfred on 15 Feb 2019
 */
var LOCAL_STORAGE_PHONES = "phones";
var LOCAL_STORAGE_ELECTRONIC_ADDRESSES = "electronicAddresses";
var LOCAL_STORAGE_POSTAL_ADDRESSES = "postalAddresses"

var phones = [];
var electronicAddresses = [];
var postalAddresses = [];

function restoreContactPoints() {
    //fetch this from local storage
    var phonesJson = localStorage.getItem(LOCAL_STORAGE_PHONES);
    var electronicAddressesJson = localStorage.getItem(LOCAL_STORAGE_ELECTRONIC_ADDRESSES);
    var postalAddressesJson = localStorage.getItem(LOCAL_STORAGE_POSTAL_ADDRESSES);
    if (phonesJson) {
        phones = JSON.parse(phonesJson);
    } else {
        phones = [];

    }
    if (electronicAddressesJson) {
        electronicAddresses = JSON.parse(electronicAddressesJson);
    } else {
        electronicAddresses = [];
    }
    if (postalAddressesJson){
        postalAddresses = JSON.parse(postalAddressesJson);
    } else {
        postalAddresses = [];
    }
    refreshPhonesTable();
    refreshElectronicAddressesTable();
    refreshPostalAddressesTable();
}

$(document).ready(function () {
    console.log("Page reloaded");
    restoreContactPoints();
});

$(window).bind('beforeunload', function () {
    //save info somewhere
    //return 'are you sure you want to leave?';
    savePhones();
    saveElectronicAddresses();
    savePostalAddresses();
});

$("#apdelrejbtn").on("click", function () {
    var addressesJson = JSON.stringify(electronicAddresses);
    var phonesJson = JSON.stringify(phones);
    $("#addressesJson").val(addressesJson);
    $("#phonesJson").val(phonesJson);
    clearContactInformation();
});

function clearContactInformation() {
    clearElectronicAddresses();
    clearPhones();
    clearPostalAddresses();
}

function setLocalStorageItem(key, value) {
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        localStorage.setItem(key, value);
    } else {
        // Sorry! No Web Storage support..
        console.log("Sorry! No Web Storage support...");
    }
}

function clearElectronicAddresses() {
    //fetch this from local storage
    electronicAddresses = [];
    localStorage.clear();
    refreshElectronicAddressesTable();
}

function clearPhones() {
    //fetch this from local storage
    phones = [];
    localStorage.clear();
    refreshPhonesTable();
}

function clearPostalAddresses(){
    postalAddresses = [];
    localStorage.clear();
    refreshPostalAddressesTable();
}

$("#addAddressBtn").on("click", function () {
    addElectronicAddress();
});

$("#addPhoneBtn").on("click", function () {
    addPhone();
});

$("#addPostalAddressBtn").on("click", function () {
    addPostalAddress();
})

$("#finishFiReg").on("click", function () {
    //clearContactInformation();
});



function addPhone() {
    var phoneNumber = $("#phoneNumber").val();
    var mobileNumber = $("#mobileNumber").val();
    var faxNumber = $("#faxNumber").val();

    function noPhoneFieldDefined() {
        var noPhoneField = (!phoneNumber && !mobileNumber && !faxNumber);
        if(noPhoneField) {
            $("#phonesInfo").show("slow");
        } else {
            $("#phonesInfo").hide("slow");
        }
        return noPhoneField;
    }

    if(noPhoneFieldDefined()) {
        return;
    }

    var phone = {};
    if(!phones) {
        phones = [];
    }
    phone.id = phones.length;
    phone.phoneNumber = phoneNumber;
    phone.mobileNumber = mobileNumber;
    phone.faxNumber = faxNumber;

    phones.push(phone);
    savePhones();
    refreshPhonesTable();
}

function addElectronicAddress() {
    var emailAddress = $("#emailAddress").val();
    var uRLAddress = $("#uRLAddress").val();
    var telexAddress = $("#telexAddress").val();
    var teletextAddress = $("#teletextAddress").val();
    var iSDNAddress = $("#iSDNAddress").val();

    function noAddressFieldDefined() {
        var noAddressField = (!emailAddress && !uRLAddress && !teletextAddress && !teletextAddress && !iSDNAddress);
        if(noAddressField) {
            $("#addressesInfo").show("slow");
        } else {
            $("#addressesInfo").hide("slow");
        }
        return noAddressField;
    }

    if(noAddressFieldDefined()) {
        return;
    }

    var electronicAddress = {};
    if(!electronicAddresses) {
        electronicAddresses = [];
    }

    electronicAddress.id = electronicAddresses.length;
    electronicAddress.emailAddress = emailAddress;
    electronicAddress.uRLAddress = uRLAddress;
    electronicAddress.telexAddress = telexAddress;
    electronicAddress.teletextAddress = teletextAddress;
    electronicAddress.iSDNAddress = iSDNAddress;

    electronicAddresses.push(electronicAddress);
    saveElectronicAddresses();
    refreshElectronicAddressesTable();
}

function addPostalAddress() {
    var addressType = $("#plcOfOpAddressType").val();
    var streetName = $("#plcOfOpStreetName").val();
    var streetBuildingIdentification = $("#plcOfOpStreetBuildingIdentification").val();
    var postCodeIdentification = $("#plcOfOpPostCodeIdentification").val();
    var townName = $("#plcOfOpTownName").val();
    var state = $("#plcOfOpState").val();
    var buildingName = $("#plcOfOpBuildingName").val();
    var floor = $("#plcOfOpFloor").val();
    var districtName = $("#plcOfOpDistrictName").val();
    var countryIdentification = $("#plcOfOpCountyIdentification").val();
    var postOfficeBox = $("#plcOfOpPostOfficeBox").val();
    var province = $("#plcOfOpProvince").val();
    var department = $("#plcOfOpDepartment").val();
    var subDepartment = $("#plcOfOpSubDepartment").val();
    var suiteIdentification = $("#plcOfOpSuiteIdentification").val();
    var buildingIdentification = $("#plcOfOpBuildingIdentification").val();
    var mailDeliverySubLocation = $("#plcOfOpMailDeliverySubLocation").val();
    var block = $("#plcOfOpBlock").val();
    var districtSubDivisionIdentification = $("#plcOfOpDistrictSubDivisionIdentification").val();
    var lot = $("#plcOfOpLot").val();
    var country = $("#plcOfOpCountry").val();

    function noPostalAddressFieldDefined() {
        var noPostalAddressField = (!addressType && !streetName && !streetBuildingIdentification && !postCodeIdentification && !townName && !state &&
                                    !buildingName && !floor && !districtName && !countryIdentification && !postOfficeBox && !province && !department &&
                                    !subDepartment && !suiteIdentification && !buildingIdentification && !mailDeliverySubLocation && !block &&
                                    !districtSubDivisionIdentification && !lot && !country);
        if(noPostalAddressField) {
            $("#postalAddressesInfo").show("slow");
        } else {
            $("#postalAddressesInfo").hide("slow");
        }
        return noPostalAddressField;
    }

    if(noPostalAddressFieldDefined()) {
        return;
    }

    var postalAddress = {};
    if(!postalAddresses) {
        postalAddresses = [];
    }
    postalAddress.id = postalAddresses.length;
    postalAddress.addressType = addressType;
    postalAddress.streetName = streetName;
    postalAddress.streetBuildingIdentification = streetBuildingIdentification;
    postalAddress.postCodeIdentification = postCodeIdentification;
    postalAddress.townName = townName;
    postalAddress.state = state;
    postalAddress.buildingName = buildingName;
    postalAddress.floor = floor;
    postalAddress.districtName = districtName;
    postalAddress.countryIdentification = countryIdentification;
    postalAddress.postOfficeBox = postOfficeBox;
    postalAddress.province = province;
    postalAddress.department = department;
    postalAddress.subDepartment = subDepartment;
    postalAddress.suiteIdentification = suiteIdentification;
    postalAddress.buildingIdentification = buildingIdentification;
    postalAddress.mailDeliverySubLocation = mailDeliverySubLocation;
    postalAddress.block = block;
    postalAddress.districtSubDivisionIdentification = districtSubDivisionIdentification;
    postalAddress.lot = lot;
    postalAddress.country = country;

    postalAddresses.push(postalAddress);
    savePostalAddresses();
    refreshPostalAddressesTable();
}

function refreshPhonesTable() {
    var phonesTable = $("#phone-addresses-table-body");
    var phonesHtml = "";
    $.each(phones, function (index, phone) {
        var rowHtml = "<tr>" +
            "<td>" + phone.phoneNumber + "</td>" +
            "<td>" + phone.mobileNumber + "</td>" +
            "<td>" + phone.faxNumber + "</td>" +
            "<td><button onclick=\"deletePhone(" + phone.id + ")\" class=\"mb-2 mr-2 btn btn-danger\">Delete</button></td>" +
            "</tr>";
        phonesHtml += rowHtml;
    });
    var phonesJson = JSON.stringify(phones);
    $("#phonesJson").val(phonesJson);
    phonesTable.html(phonesHtml);
}

function refreshElectronicAddressesTable() {
    var electronicAddressesTable = $("#electronic-addresses-table-body");
    var electronicAddressesHtml = "";
    $.each(electronicAddresses, function (index, address) {
        var rowHtml = "<tr>" +
            "<td>" + address.emailAddress + "</td>" +
            "<td>" + address.uRLAddress + "</td>" +
            "<td>" + address.telexAddress + "</td>" +
            "<td>" + address.teletextAddress + "</td>" +
            "<td>" + address.iSDNAddress + "</td>" +
            "<td><button onclick=\"deleteElectronicAddress(" + address.id + ")\" class=\"mb-2 mr-2 btn btn-danger\">Delete</button></td>" +
        "</tr>";
        electronicAddressesHtml += rowHtml;
    });
    var addressesJson = JSON.stringify(electronicAddresses);
    $("#addressesJson").val(addressesJson);
    electronicAddressesTable.html(electronicAddressesHtml);
}

function refreshPostalAddressesTable(){
    var postalAddressesTable = $("#postal-addresses-table-body");
    var postalAddressesHtml = "";
    $.each(postalAddresses, function (index, postalAddress) {
        var rowHtml = "<tr>" +
            "<td>"+ postalAddress.addressType +"</td>" +
            "<td>"+ postalAddress.streetName +"</td>" +
            "<td>"+ postalAddress.streetBuildingIdentification + "</td>" +
            "<td>"+ postalAddress.postCodeIdentification +"</td>" +
            "<td><button onclick=\"deletePostalAddresses(" + postalAddress.id + ")\" class=\"mb-2 mr-2 btn btn-danger\">Danger</button></td>" +
            "</tr>" ;
        postalAddressesHtml += rowHtml;
    });
    var postalAddressesJson = JSON.stringify(postalAddresses);
    $("#postalAddressesJson").val(postalAddressesJson);
    postalAddressesTable.html(postalAddressesHtml);
}

function savePhones() {
    setLocalStorageItem(LOCAL_STORAGE_PHONES, JSON.stringify(phones));
}

function saveElectronicAddresses() {
    setLocalStorageItem(LOCAL_STORAGE_ELECTRONIC_ADDRESSES, JSON.stringify(electronicAddresses));
}

function savePostalAddresses() {
    setLocalStorageItem(LOCAL_STORAGE_POSTAL_ADDRESSES, JSON.stringify(postalAddresses))
}

function deletePhone(phoneId) {
    if(phones.length == 1) {
        phones.splice(0, 1);
    }
    phones.splice(phoneId, 1);
    savePhones();
    refreshPhonesTable();
}

function deleteElectronicAddress(addressId) {
    if(electronicAddresses.length == 1) {
        electronicAddresses.splice(0, 1);
    }
    electronicAddresses.splice(addressId, 1);
    saveElectronicAddresses();
    refreshElectronicAddressesTable();
}

function deletePostalAddresses(postAddressId) {
    if (postalAddresses.length == 1){
        postalAddresses.splice(0, 1);
    }
    postalAddresses.splice(postAddressId, 1);
    savePostalAddresses();
    refreshPostalAddressesTable();
}