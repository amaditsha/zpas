$(document).ready(function () {
    $.ajax({
        url: "/zpas/get-roles",
        method: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            console.log("Number of party info:", data.length);
            $( ".role" ).remove();
            var html = "";
            $.each(data, function (i, partyInfo) {
                console.log("Party info details | " + partyInfo);
                var url = "/zpas/update-session/" + partyInfo.partyId;
                if(partyInfo.status === "ACTIVE") {
                    html = "<li class='nav-item role'>\n" +
                        "<a href='" + url + "' class='nav-link'>" + partyInfo.name + "\n" +
                        "<div class='ml-auto badge badge-success'>" + partyInfo.status + "</div>\n" +
                        "</a>\n" +
                        "</li>";
                } else {
                    html = "<li class='nav-item role'>\n" +
                        "<a href='" + url + "' class='nav-link'>" + partyInfo.name + "\n" +
                        "<div class='ml-auto badge badge-warning'>" + partyInfo.status + "</div>\n" +
                        "</a>\n" +
                        "</li>";
                }
                $("#profileMenuSection").append(html);
            });
        },
        error: function (data) {
            console.log("An error occurred while fetching the roles ");
        }
    });

    /*var clickedNotify = false;
    $('.tooltip-loginas').tooltip({placement: 'bottom'}).tooltip('show');
    $('.tooltip-loginas').mouseleave(function() {
        if (!clickedNotify) {
            $('.tooltip-loginas').tooltip({placement: 'bottom'}).tooltip('show');
        }
    });
    $('.tooltip-loginas').click(function() {
        clickedNotify = true;
        $(this).tooltip('destroy');
    });*/
});

