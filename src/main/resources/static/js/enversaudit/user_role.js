function get_user_role_audit(userRoleId) {
    console.log('Get the user roles ... with id ' + userRoleId);
    $.ajax({
        url: "/zpas/audit-trails/userrole/" + userRoleId,
        dataType: "json",
        contentType: "application/json",
        method: "get",
        success: function (userRoles) {
            console.log("Number of userRoles infos:", userRoles.length);
            html = '';
            var i = 1;
            $.each(userRoles, function (index, userrole) {
                console.log("User Role Info:", JSON.stringify(userrole));
                html += "<tr><td>" + i + "</td><td>" + userrole.userRole.name + "</td><td>" + userrole.auditedRevisionEntity.timestamp + "</td><td>" + userrole.auditedRevisionEntity.username + "</td></tr>";
                i = i + 1;
            });
            $("#enversAuditViewTable tr").remove();
            $("#enversAuditViewTable").append(html);
        },
        error: function (data) {
            console.log("An error occurred. Message was ", JSON.stringify(data));
        }
    }).done(function () {
        console.log("Done filling in mandate information");
    });
}

$("#enversAuditViewButton").click(function (e) {
    var id = $("#userRoleIdSpan").text();
    console.log('User role Id id is ' + id);
    get_user_role_audit(id);
});