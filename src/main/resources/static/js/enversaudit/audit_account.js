function get_account_audit(accountId) {
    console.log('Get the account... with id ' + accountId);
    $.ajax({
        url: "/zpas/audit-trails/account/" + accountId,
        dataType: "json",
        contentType: "application/json",
        method: "get",
        success: function (accounts) {
            console.log("Number of accounts infos:", accounts.length);
            html = '';
            var i = 1;
            $.each(accounts, function (index, account) {
                console.log("Account Info:", JSON.stringify(account));
                html += "<tr><td>" + i + "</td><td>" + account.cashAccount.cashAccountType + "</td><td>" + account.auditedRevisionEntity.timestamp + "</td><td>" + account.auditedRevisionEntity.username + "</td></tr>";
                i = i + 1;
            });
            $("#enversAuditViewTable tr").remove();
            $("#enversAuditViewTable").append(html);
        },
        error: function (data) {
            console.log("An error occurred. Message was ", JSON.stringify(data));
        }
    }).done(function () {
        console.log("Done filling in mandate information");
    });
}

$("#enversAuditViewButton").click(function (e) {
    var id = $("#cashAccountId").text();
    console.log('account id is ' + id);
    get_account_audit(id);
});