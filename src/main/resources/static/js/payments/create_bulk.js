var LOCAL_STORAGE_BULK_OBJECT = "bulkObject";
var LOCAL_STORAGE_PAYMENTS = "payments";
var payments = [];

var bulkPayment = {};

function populateBulkPayment() {
    console.log("Populate bulk called.");
    var purpose = $("#purpose").val();
    var financialInstitutionId = $("#financialInstitutionId :selected").val();
    var accountId = $("#accountId :selected").val();
    var totalAmount = $("#totalAmount").val();
    var currencyName = $("#currencyName").val();
    var currencyCode = $("#bulkCurrencyCode").val();
    var paymentType = $("#paymentType :selected").val();
    var priority = $("#priority :selected").val();
    var valueDate = $("#valueDate").val();
    var paymentInstrument = $("#paymentInstrument :selected").val();

    bulkPayment.purpose = purpose;
    bulkPayment.financialInstitutionId = financialInstitutionId;
    bulkPayment.accountId = accountId;
    bulkPayment.totalAmount = totalAmount;
    bulkPayment.currencyName = currencyName;
    bulkPayment.currencyCode = currencyCode;
    bulkPayment.paymentType = paymentType;
    bulkPayment.priority = priority;
    bulkPayment.valueDate = valueDate;
    bulkPayment.paymentInstrument = paymentInstrument;

}

$('#submitBulk').click(function() {
    populateBulkPayment();
    var createBulkPaymentLink = "/zpas/payments/create";
    var requestJson = JSON.stringify({
        bulkPayment: bulkPayment,
        payments: payments
    });

    $.ajax({
        url: createBulkPaymentLink,
        dataType: "json",
        contentType: "application/json",
        method: "POST",
        data: requestJson,
        success: function(data) {
            var bulkPaymentId = data.bulkPaymentId;
            console.log("Data received: ", JSON.stringify(data));
            if(bulkPaymentId) {
                console.log("Created bulk payment id is:", bulkPaymentId);
                window.location = "/zpas/payments/view/" + bulkPaymentId;
            } else {
                console.log("An error occurred. Could not save bulk payment.");
            }
        },
        error: function(message) {
            console.log("An error occurred. Message was", JSON.stringify(message));
        }
    });
});

function restorePayments() {
    //fetch this from local storage
    var paymentsJson = localStorage.getItem(LOCAL_STORAGE_PAYMENTS);
    var bulkJson = localStorage.getItem(LOCAL_STORAGE_BULK_OBJECT);
    if (paymentsJson) {
        payments = JSON.parse(paymentsJson);
    } else {
        payments = [];
    }
    if(bulkJson) {
        bulkJson = JSON.parse(bulkJson)
    }
    refreshPaymentsTable();
}

function restoreBulk() {
    //fetch this from local storage
    var bulkJson = localStorage.getItem(LOCAL_STORAGE_BULK_OBJECT);
    if(bulkJson) {
        bulkPayment = JSON.parse(bulkJson)
    }
    restoreBulkForm();
}

function restoreBulkForm() {
    $("#purpose").val(bulkPayment.purpose);
    $("#financialInstitutionId").val(bulkPayment.financialInstitutionId);
    $("#accountId").val(bulkPayment.accountId);
    $("#totalAmount").val(bulkPayment.totalAmount);
    $("#currencyName").val(bulkPayment.currencyName);
    $("#bulkCurrencyCode").val(bulkPayment.currencyCode);
    $("#paymentType").val(bulkPayment.paymentType);
    $("#priority").val(bulkPayment.priority);
    $("#valueDate").val(bulkPayment.valueDate);
    $("#paymentInstrument").val(bulkPayment.paymentInstrument);
}

$(document).ready(function () {
    console.log("Page reloaded");
    restorePayments();
    restoreBulk()
});

$(window).bind('beforeunload', function () {
    //save info somewhere
    //return 'are you sure you want to leave?';
    saveBulkPayment();
    savePayments();
});

function setLocalStorageItem(key, value) {
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        localStorage.setItem(key, value);
    } else {
        // Sorry! No Web Storage support..
        console.log("Sorry! No Web Storage support...");
    }
}

$("#addPayment").on("click", function () {
    console.log("Trying to add a payment");
    addPayment();
});

function addPayment() {
    var destinationBin = $("#destinationBin").val();
    var beneficiaryAccount = $("#beneficiaryAccount").val();
    var beneficiaryName = $("#beneficiaryName").val();
    var nationalId = $("#nationalId").val();
    var amount = $("#amount").val();
    var paymentTypeCode = $("#paymentTypeCode :selected").val();
    var paymentTypeCodeText = $("#paymentTypeCode :selected").text();
    var currencyCode = $("#currencyCode").val();
    var paymentPurpose = $("#paymentPurpose").val();



    var payment = {};
    if(!payments) {
        payments = [];
    }
    payment.id = payments.length;
    payment.destinationBin = destinationBin;
    payment.beneficiaryName = beneficiaryName;
    payment.nationalId = nationalId;
    payment.beneficiaryAccount = beneficiaryAccount;
    payment.amount = amount;
    payment.currencyCode = currencyCode;
    payment.paymentPurpose = paymentPurpose;
    payment.paymentTypeCode = paymentTypeCode;
    payment.paymentTypeCodeText = paymentTypeCodeText;

    console.log("Amount is : " + amount);
    payments.push(payment);
    savePayments();
    refreshPaymentsTable();
    clearCapturePaymentDetails();
}

function clearCapturePaymentDetails() {
    $("#destinationBin").val("");
    $("#beneficiaryAccount").val("");
    $("#beneficiaryName").val("");
    $("#nationalId").val("");
    $("#amount").val("");
    $("#paymentTypeCode").val("");
    $("#currencyCode").val("");
    $("#paymentPurpose").val("");
}

function refreshPaymentsTable() {
    var paymentsTable = $("#payment-entries-section");
    var paymentsHtml = "";
    $.each(payments, function (index, payment) {
        var rowHtml =
            "<tr>" +
            "<td>" + payment.beneficiaryName + "</td>" +
            "<td>"+ payment.nationalId + "</td>" +
            "<td>"+ payment.destinationBin + "</td>" +
            "<td>"+ payment.beneficiaryAccount +  "</td>" +
            "<td>" + payment.paymentTypeCodeText + "</td>" +
            "<td>" + payment.paymentPurpose + "</td>" +
            "<td>" + payment.amount + "</td>" +
            "<td>" +
            "<button onclick=\"deletePayment('" + payment.id + "')\" class=\"mb-2 mr-2 btn-icon btn btn-danger\"><i class=\"pe-7s-trash btn-icon-wrapper\"> </i>Delete</button>" +
            "</td>" +
            "</tr>";
        paymentsHtml += rowHtml;
    });
    paymentsTable.html(paymentsHtml);
}

// function loadSummary() {
//     console.log("Load summary called.");
//     var bulkJson = localStorage.getItem(LOCAL_STORAGE_BULK_OBJECT);
//     var paymentUploadDTO = JSON.parse(bulkJson);
//
//     $("#tableBulkType").text(paymentUploadDTO.bulkType);
//     $("#tableClientAccountNumber").text(paymentUploadDTO.clientAccountNumber);
//     $("#tableDebitDate").text(paymentUploadDTO.debitDateString);
//     $("#tableDescription").text(paymentUploadDTO.description);
//     $("#tableNumberOfEntries").text(paymentUploadDTO.numberOfEntries);
//     populateSummaryTable();
// }

// function populateSummaryTable() {
//     console.log("Loading summary payments table");
//     //
//     //
//     // var paymentsJson = localStorage.getItem(LOCAL_STORAGE_PAYMENTS);
//     // if (paymentsJson) {
//     //     payments = JSON.parse(paymentsJson);
//     // } else {
//     //     payments = [];
//     // }
//
//     var paymentsTable = $("#summary-table-body");
//     var paymentsHtml = "";
//     $.each(payments, function (index, payment) {
//         console.log("Payment for summary is", payment);
//         var rowHtml = "<tr><td>" + payment.paymentDate + "</td><td>" + payment.fiName + "</td><td>"
//             + payment.beneficiaryName + "</td><td>" + payment.nationalId + "</td><td>"
//             + payment.beneficiaryAccount + "</td><td>" + payment.paymentPurpose + "</td><td>"
//             + payment.amount + "</td></tr>";
//         paymentsHtml += rowHtml;
//     });
//     paymentsTable.html(paymentsHtml);
// }

function savePayments() {
    setLocalStorageItem(LOCAL_STORAGE_PAYMENTS, JSON.stringify(payments));
}

function saveBulkPayment() {
    setLocalStorageItem(LOCAL_STORAGE_BULK_OBJECT, JSON.stringify(bulkPayment));
}

function deletePayment(paymentId) {
    console.log("About to delete payment id", paymentId);
    console.log("Before delete payments are:");
    $.each(payments, function (index, payment) {
        console.log("Payment", index, JSON.stringify(payment));
    });
    if(payments.length == 1) {
        payments.splice(0, 1);
    }
    payments.splice(paymentId, 1);
    console.log("After delete payments are:");
    $.each(payments, function (index, payment) {
        console.log("Payment", index, JSON.stringify(payment));
    });
    savePayments();
    refreshPaymentsTable();
}