/**
 Created by alfred on 24 Apr 2019
 */

var accountDetailsMap = [];
var timer;
var destinationAccountInfo = $("#destinationAccountInfo");
var destinationAccountDanger = $("#destinationAccountDanger");

$("#payments-link").addClass("mm-active");
$("#payments-link-make-payment").addClass("mm-active");

$("#destinationAccountNumber").on('input', function (e) {
    $(".accountNumberInfo").show();
    var accountNumber = $("#destinationAccountNumber").val();
    $(".destinationAccountInfo").text(accountNumber);
    clearTimeout(timer);
    if (accountNumber) {
        if (accountNumber.length > 4) {
            timer = setTimeout(function () {
                verifyAccountNumber();
            }, 500);
        }
    }
});

$("#amount").on("input", function (e) {
    $(".amountInfo").text(this.value);
});


function verifyAccountNumber() {
    var accountNumber = $("#destinationAccountNumber").val();
    var financialInstitution = $("#destinationFinancialInstitutionId").val();
    destinationAccountInfo.text("Verifying account number...");
    $.ajax({
        url: "/zpas/api/json/accounts/account-info",
        method: "get",
        dataType: "json",
        contentType: "application/json",
        data: {
            accountNumber: accountNumber,
            financialInstitutionId: financialInstitution
        },
        success: function (data) {
            console.log("Received Account info data:", JSON.stringify(data));
            var info = "";
            var errorInfo = "";
            var accountOwnerName = data.accountOwnerName;
            console.log("Account owner name is", accountOwnerName);
            if (accountOwnerName) {
                info = "Account \"" + data.accountNumber + "\" belongs to " + data.accountOwnerName + " of " + data.financialInstitutionName;
            } else {
                errorInfo = "Account \"" + data.accountNumber + "\" not found";
            }
            destinationAccountInfo.text(info);
            destinationAccountDanger.text(errorInfo);

        },
        error: function (data) {
            console.log("An error occurred:", JSON.stringify(data));
            destinationAccountInfo.text("");
            destinationAccountDanger.text("Sorry, we could not verify the account number. It's not your fault.");
        }
    });
}

function handleBPNVerificationData(data) {
    if (data.bpn) {
        console.log("Handling bpn verification data");
        var verificationCompleted = data.verificationCompleted;
        if (verificationCompleted == 'true' || verificationCompleted == true) {
            bpnVerificationDone();
        } else {
            bpnVerificationInProgress();
        }

        var startDate = moment(moment(data.startDate, "DD-MM-YYYY")).format('DD MMM YYYY');
        var expiryDate = moment(moment(data.expiryDate, "DD-MM-YYYY")).format('DD MMM YYYY');

        $("#companyName").text(data.companyName);
        $("#bpn").text(data.bpn);
        $("#validityPeriod").text(startDate + "  to  " + expiryDate);
        $("#exists").text(data.exists ? "BPN Found." : "BPN Not found.");
    } else {
        bpnVerificationInProgress();
        console.log("BPN not found, waiting for ZIMRA response.");
    }
}

function bpnVerificationInProgress() {
    $("#show-info").hide("fast");
    $("#progress-loader").show("slow");
    $("#progress-done").hide();
    $("#verificationInfo").show();
    $("#verificationError").hide();
    $("#verificationSuccess").hide();
}

function bpnVerificationDone() {
    $("#progress-loader").hide("fast");
    $("#progress-done").show();
    $("#show-info").show("slow");
    $("#verificationInfo").hide();
    $("#verificationError").hide();
    $("#verificationSuccess").show();
}

function bpnVerificationReset() {
    $("#progress-loader").show();
    $("#progress-done").hide();
    $("#show-info").hide();
    $("#verificationInfo").hide();
    $("#verificationError").hide();
    $("#verificationSuccess").hide();

    $("#companyName").text("");
    $("#bpn").text("");
    $("#validityPeriod").text("");
    $("#exists").text("");
}

function bpnVerificationError() {
    $("#progress-loader").hide("fast");
    $("#progress-done").hide();
    $("#show-info").hide("fast");
    $("#verificationInfo").hide();
    $("#verificationError").show();
    $("#verificationSuccess").hide();
}

function verifyBPNServerSentEvents() {
    bpnVerificationInProgress();
    if (!!window.EventSource) {
        var eventSource = new EventSource("/zpas/payments/sse/tax-clearances/infinite/" + $("#taxIdentificationNumber").val());

        function add(message) {
            console.log(message);
            // var element = document.createElement("li");
            // element.innerHTML = message;
            // elements.appendChild(element);
        }

        eventSource.onmessage = function (e) {
            var data = JSON.parse(e.data);
            var verificationCompleted = data.verificationCompleted;
            if (verificationCompleted == 'true' || verificationCompleted == true) {
                eventSource.close();
            }

            handleBPNVerificationData(data);

            add(JSON.stringify(data));
        };

        eventSource.onopen = function (e) {
            add('connection was opened');
        };

        eventSource.onerror = function (e) {
            if (e.readyState == EventSource.CONNECTING) {
                add('event: CONNECTING');
            } else if (e.readyState == EventSource.OPEN) {
                add('event: OPEN');
            } else if (e.readyState == EventSource.CLOSING) {
                add('event: CLOSING');
                eventSource.close();
            } else if (e.readyState == EventSource.CLOSED) {
                add('event: CLOSED');
                eventSource.close();
            }
        };
    } else {
        alert('The browser does not support Server-Sent Events');
    }
}

$(document).ready(function () {
    var makePaymentRequestType = $("#makePaymentRequestType").val();
    configurePaymentType(makePaymentRequestType);
    if (makePaymentRequestType) {
        console.log("Clicking next...");
        $("#next-btn").click();
    }

    $("#virtualIdCapture").select2({
        minimumInputLength: 1,
        placeholder: "Search for an account",
        allowClear: true, // need to override the changed default
        ajax: {
            url: "/zpas/api/json/accounts/search",
            dataType: "json",
            contentType: "application/json",
            delay: 250,
            data: function (params) {
                var queryParameters = {
                    search: params.term
                }

                return queryParameters;
            },
            processResults: function (data) {
                console.log("Received data is:", JSON.stringify(data));
                var formattedData = [];
                $.each(data, function (idx, dataItem) {
                    var d = {
                        id: dataItem.accountNumber,
                        text: dataItem.accountNumber + "(" + dataItem.accountName + ")"
                    };
                    formattedData.push(d);
                    var accFi = {
                        accountNumber: dataItem.accountNumber,
                        fiId: dataItem.financialInstitutionId,
                        debtorName: dataItem.accountOwnerName,
                        accountOwnerId: dataItem.accountOwnerId,
                        bpn: dataItem.bpn
                    };
                    accountDetailsMap.push(accFi);
                });
                console.log("Formatted data is", formattedData);
                return {
                    results: formattedData
                };
            }
        }
    });

    $("#verifyBpnTriggerBtn").on("click", function () {
        var bpn = $("#taxIdentificationNumber").val();
        if (bpn) {
            $.ajax({
                url: "/zpas/payments/tax-clearances/verify/" + bpn,
                method: "get",
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    console.log("Company Tax Clearance info:", JSON.stringify(data));
                    if (data.bpn) {
                        console.log("BPN Set");
                        handleBPNVerificationData(data);
                    } else {
                        console.log("BPN not set");
                        verifyBPNServerSentEvents();
                    }
                },
                error: function (data) {
                    console.log("An error occurred:", JSON.stringify(data));
                    verifyBPNServerSentEvents();
                }
            });
        } else {
            console.log("BPN empty");
            bpnVerificationError();
        }
    });

    $("#closeVerifyModal").on("click", function () {
        bpnVerificationReset();
    });
});

$("#merchantAccountId").on("change", function () {
    var partyMerchantAccountId = this.value;
    $.ajax({
        url: "/zpas/party-merchant-accounts/get/" + partyMerchantAccountId,
        method: "get",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            console.log("Party Merchant account details are:", JSON.stringify(data));
            $("#merchantId").val(data.merchantId);
            $("#merchantAccountNumber").val(data.accountNumber);
            $("#savePartyMerchantAccountDetailsDiv").hide();
        },
        error: function (message) {
            console.log("An error occurred.", JSON.stringify(message));
        }
    });
});

$("#virtualIdCapture").on("change", function () {
    console.log("Change occurred on virtual ID capture");

    var accountNumber = $("#virtualIdCapture").val();
    console.log("Account number is " + accountNumber);
    var details = getDetailsForAccountNumber(accountNumber);
    $("#destinationAccountNumber").val(accountNumber);
    verifyAccountNumber();
    $("#beneficiaryName").val(details.debtorName);
    $("#taxIdentificationNumber").val(details.bpn);
    $("#destinationFinancialInstitutionId").val(details.fiId);
});

function setAssessmentInfo(assessmentNumber, officeCode, amount) {
    $("#zimraAssessmentNumber").val(assessmentNumber);
    $("#zimraOfficeCode").val(officeCode);

    $("#amount").val(amount);
    $("#beneficiaryReference").val("ZIMRA Payment");
    $("#clientReference").val("ZIMRA Payment");

    $("#zimraAssessmentButton").hide();
}

function getDetailsForAccountNumber(accountNumber) {
    for (i = 0; i < accountDetailsMap.length; i++) {
        if (accountDetailsMap[i].accountNumber == accountNumber) {
            console.log("FI ID. " + accountDetailsMap[i].fiId);
            return accountDetailsMap[i];
        }
    }
    console.log("ID not found.");
    return {};
}

$("#priority").on("change", function () {
    console.log("priority changed. Value is: ", this.value);
    if (this.value == 'High') {
        $("#priorityMessage").show();
    } else {
        $("#priorityMessage").hide();
    }
});

$("#merchantId").on("change", function () {
    $("#savePartyMerchantAccountDetailsDiv").show();
});

$("#taxAuthorityId").on("change", function () {
    var taxAuthorityId = this.value;
    $.ajax({
        url: "/zpas/tax-authority/payment-office/" + taxAuthorityId,
        method: "get",
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            var html = "";
            $.each(data, function (index, paymentOffice) {
                html += "<option value='" + paymentOffice.code + "'>" + paymentOffice.name + "</option>";
            });
            $("#taxAuthoritySubOfficeCode").append(html);
        },
        error: function (data) {
            console.log("An error occurred while fetching tax authority payment offices.")
        }
    });
});

$("#capturePartyMerchantIdentificationModalTriggerBtn").on("click", function () {
    console.log("Modal trigger button clicked.");
});

$("#savePartyMerchantAccountDetailsBtn").on("click", function () {
    console.log("Trying to save Party Merchant account details.");
    // copy necessary fields
    var merchantId = $("#merchantId").val();
    var identification = $("#identification").val();
    var accountNumber = $("#merchantAccountNumber").val();

    $.ajax({
        url: "/zpas/party-merchant-accounts/create",
        contentType: "application/json",
        dataType: "json",
        method: "post",
        data: JSON.stringify({
            merchantId: merchantId,
            identification: identification,
            accountNumber: accountNumber
        }),
        success: function (data) {
            console.log("Data saved.");
            //show message to the user
        },
        error: function (message) {
            console.log("An error occurred while trying to save Party Merchant Account details");
            console.log("Error was: ", JSON.stringify(message));
        }
    });
});

$("#makePaymentRequestType").on("change", function () {
    console.log("Make Payment Request Type Value is: ", this.value);
    configurePaymentType(this.value);
});

$("#zimraAssessmentNumber").on("click", function () {
    $("#zimraAssessmentButton").show();
});

$("#zimraAssessmentNumber").on("keyup", function () {
    $("#zimraAssessmentButton").show();
});

$("#zimraAssessmentButton").on("click", function () {
    var zimraAssessmentNumber = $("#zimraAssessmentNumber").val();
    if(zimraAssessmentNumber) {
        console.log("About to request ZIMRA Assessment information for assessment number:", zimraAssessmentNumber);
        $.ajax({
            url: "/zpas/api/json/zimra/assessment/" + zimraAssessmentNumber,
            method: "get",
            success: function (data) {
                if(data.assessmentValidationGetResponse) {
                    var validationResponse = data.assessmentValidationGetResponse;
                    var year = validationResponse.year;
                    var assNo = validationResponse.assNo;
                    var office = validationResponse.office;
                    var amount = validationResponse.amount;
                    var currency = validationResponse.currency;
                    var found = validationResponse.found;
                    if(found == 'true' || found == true) {
                        console.log("Info found");
                        console.log("Data:", JSON.stringify(validationResponse));
                        $("#amount").val(amount);
                        $("#zimraOfficeCode").val(office);
                        $("#beneficiaryReference").val("ZIMRA Payment");
                        $("#clientReference").val("ZIMRA Payment");
                        $(".amountInfo").text(currency + amount);
                        $(".yearInfo").text(year);
                        $(".officeInfo").text(office);
                        $(".assessment-info").text("Assessment Number Validation was successful. Amount: "
                            + currency + amount + " Year: " + year);
                        $("#assessment-info-table").show();
                    } else {
                        console.log("ZIMRA Assessment Number", zimraAssessmentNumber, " not found.");
                        $(".assessment-info").text("Assessment Number Validation not found.");
                        $("#assessment-info-table").hide();
                    }
                } else {
                    console.log("Invalid response, no data returned for ZIMRA assessment validation request.");
                    $(".assessment-info").text("No data returned for ZIMRA assessment validation request.");
                    $("#assessment-info-table").hide();
                }
            },
            error: function (data) {
                console.log("There was an error processing ZIMRA assessment validation request: ", JSON.stringify(data));
                $(".assessment-info").text("There was an error processing ZIMRA assessment validation request.");
                $("#assessment-info-table").hide();
            }
        });
    } else {
        console.log("ZIMRA Assessment Number not present, please provide one.")
    }
});

function configurePaymentType(makePaymentRequestType) {
    var paymentRequestType = makePaymentRequestType.replace('_', ' ');
    $(".paymentType").text(paymentRequestType);
    switch (makePaymentRequestType) {
        case 'COMPANY':
            $(".credit-transfer-info").show();
            $(".tax-authority-info").hide();
            $(".zimra-payment-info").hide();
            $(".company-info").show();
            $(".merchant-info").hide();
            $("#savePartyMerchantAccountDetailsDiv").hide();
            break;
        case 'INDIVIDUAL':
            $(".credit-transfer-info").show();
            $(".tax-authority-info").hide();
            $(".zimra-payment-info").hide();
            $(".company-info").hide();
            $(".merchant-info").hide();
            $("#savePartyMerchantAccountDetailsDiv").hide();
            break;
        case 'TAX_PAYMENT':
            $("#paymentType").val('TaxPayment');
            $("#paymentTypeDiv").hide();
            $(".zimra-payment-info").hide();
            $("#purpose").val('Tax Payment');
            $(".credit-transfer-info").hide();
            $(".tax-authority-info").show();
            $(".company-info").show();
            $(".merchant-info").hide();
            $("#savePartyMerchantAccountDetailsDiv").hide();
            $("#paymentInstrumentDiv").hide();
            break;
        case 'MERCHANT_PAYMENT':
            $("#paymentType").val('CommercialCredit');
            $("#paymentTypeDiv").hide();
            $(".zimra-payment-info").hide();
            $("#purpose").val('Merchant Payment');
            $(".credit-transfer-info").hide();
            $(".tax-authority-info").hide();
            $(".company-info").hide();
            $(".merchant-info").show();
            $("#savePartyMerchantAccountDetailsDiv").show();
            $("#paymentInstrumentDiv").show();
            break;
        case 'ZIMRA_PAYMENT':
            $(".zimra-payment-info").show();
            $("#paymentTypeDiv").hide();
            $(".credit-transfer-info").hide();
            $(".tax-authority-info").hide();
            $(".company-info").hide();
            $(".merchant-info").hide();
            $("#savePartyMerchantAccountDetailsDiv").hide();
            $("#paymentInstrumentDiv").hide();
            $("#purpose").val('Zimra Payment');
            break;
    }
}