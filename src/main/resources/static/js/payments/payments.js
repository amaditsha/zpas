/**
 Created by alfred on 18 Mar 2019
 */
var paymentActionId = "";
var paymentReference = "";

var LOCAL_STORAGE_SOURCE_ACCOUNT_KEY = "accountId";
var LOCAL_STORAGE_PAYMENT_INSTRUMENT_KEY = "paymentInstrument";
var LOCAL_STORAGE_PAYMENT_TYPE_KEY = "paymentType";
var LOCAL_STORAGE_VALUE_DATE_FROM_KEY = "valueDateFrom";
var LOCAL_STORAGE_VALUE_DATE_TO_KEY = "valueDateTo";
var LOCAL_STORAGE_DATE_CREATED_FROM_KEY = "dateCreatedFrom";
var LOCAL_STORAGE_DATE_CREATED_TO_KEY = "dateCreatedTo";
var LOCAL_STORAGE_SEARCH_INPUT_KEY = "searchInput";

$(document).ready(function () {
    restoreSearchAndFilterCriteria();

    $("#clearSearchAndFilterCriteria").on("click", function () {
        clearCriteria();
        window.location.href = '/zpas/payments/';
    });
});

$(window).bind('beforeunload', function () {
    //save info somewhere
    //return 'are you sure you want to leave?';
    saveSearchAndFilterCriteria();
});

function setLocalStorageItem(key, value) {
    if (typeof (Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        localStorage.setItem(key, value);
    } else {
        // Sorry! No Web Storage support..
        console.log("Sorry! No Web Storage support...");
    }
}

function getZimraPayments() {
    $("#specialPayee").val("ZIMRA");
    $("#searchForm").submit();
}

function getNssaPayments() {
    $("#specialPayee").val("NSSA");
    $("#searchForm").submit();
}

function clearCriteria() {
    localStorage.clear();
    $("#" + LOCAL_STORAGE_SOURCE_ACCOUNT_KEY).val("");
    $("#" + LOCAL_STORAGE_PAYMENT_INSTRUMENT_KEY).val("");
    $("#" + LOCAL_STORAGE_PAYMENT_TYPE_KEY).val("");
    $("#" + LOCAL_STORAGE_VALUE_DATE_FROM_KEY).val("");
    $("#" + LOCAL_STORAGE_VALUE_DATE_TO_KEY).val("");
    $("#" + LOCAL_STORAGE_DATE_CREATED_FROM_KEY).val("");
    $("#" + LOCAL_STORAGE_DATE_CREATED_TO_KEY).val("");
    $("#" + LOCAL_STORAGE_SEARCH_INPUT_KEY).val("");
}

function saveSearchAndFilterCriteria() {
    //save info somewhere
    //return 'are you sure you want to leave?';
    setLocalStorageItem(LOCAL_STORAGE_SOURCE_ACCOUNT_KEY, $("#" + LOCAL_STORAGE_SOURCE_ACCOUNT_KEY).val());
    setLocalStorageItem(LOCAL_STORAGE_PAYMENT_INSTRUMENT_KEY, $("#" + LOCAL_STORAGE_PAYMENT_INSTRUMENT_KEY).val());
    setLocalStorageItem(LOCAL_STORAGE_PAYMENT_TYPE_KEY, $("#" + LOCAL_STORAGE_PAYMENT_TYPE_KEY).val());
    setLocalStorageItem(LOCAL_STORAGE_VALUE_DATE_FROM_KEY, $("#" + LOCAL_STORAGE_VALUE_DATE_FROM_KEY).val());
    setLocalStorageItem(LOCAL_STORAGE_VALUE_DATE_TO_KEY, $("#" + LOCAL_STORAGE_VALUE_DATE_TO_KEY).val());
    setLocalStorageItem(LOCAL_STORAGE_DATE_CREATED_FROM_KEY, $("#" + LOCAL_STORAGE_DATE_CREATED_FROM_KEY).val());
    setLocalStorageItem(LOCAL_STORAGE_DATE_CREATED_TO_KEY, $("#" + LOCAL_STORAGE_DATE_CREATED_TO_KEY).val());
    setLocalStorageItem(LOCAL_STORAGE_SEARCH_INPUT_KEY, $("#" + LOCAL_STORAGE_SEARCH_INPUT_KEY).val());
}

function restoreSearchAndFilterCriteria() {
    //fetch this from local storage
    $("#" + LOCAL_STORAGE_SOURCE_ACCOUNT_KEY).val(localStorage.getItem(LOCAL_STORAGE_SOURCE_ACCOUNT_KEY));
    $("#" + LOCAL_STORAGE_PAYMENT_INSTRUMENT_KEY).val(localStorage.getItem(LOCAL_STORAGE_PAYMENT_INSTRUMENT_KEY));
    $("#" + LOCAL_STORAGE_PAYMENT_TYPE_KEY).val(localStorage.getItem(LOCAL_STORAGE_PAYMENT_TYPE_KEY));
    $("#" + LOCAL_STORAGE_VALUE_DATE_FROM_KEY).val(localStorage.getItem(LOCAL_STORAGE_VALUE_DATE_FROM_KEY));
    $("#" + LOCAL_STORAGE_VALUE_DATE_TO_KEY).val(localStorage.getItem(LOCAL_STORAGE_VALUE_DATE_TO_KEY));
    $("#" + LOCAL_STORAGE_DATE_CREATED_FROM_KEY).val(localStorage.getItem(LOCAL_STORAGE_DATE_CREATED_FROM_KEY));
    $("#" + LOCAL_STORAGE_DATE_CREATED_TO_KEY).val(localStorage.getItem(LOCAL_STORAGE_DATE_CREATED_TO_KEY));

    var searchInput = localStorage.getItem(LOCAL_STORAGE_SEARCH_INPUT_KEY);
    if(searchInput) {
        $("#" + LOCAL_STORAGE_SEARCH_INPUT_KEY).val(searchInput);
    }
    if(searchInput == 'undefined') {
        $("#" + LOCAL_STORAGE_SEARCH_INPUT_KEY).val("");
    }
}

$('#pagination-container').pagination({
    dataSource: getPaymentsArray(),
    pageSize: 10,
    autoHidePrevious: true,
    autoHideNext: true,
    showGoInput: true,
    showGoButton: true,
    pageNumber: getPageNumber(),
    callback: function (data, pagination) {
        goToPage(pagination.pageNumber);
    }
});

function goToPage(pageNumber) {
    if (isNaN(pageNumber)) {
        console.log("Page is not a number");
    } else {
        var currentPage = parseInt($("#currentPageSpan").text());
        if(pageNumber == currentPage) {
            console.log("No need to navigate, we are still on the same page.");
        } else {
            var link = $("#goToPageLink").prop("href");
            link = link.replace("replace_page_number", pageNumber);
            $("#goToPageLink").prop("href", link);
            $("#goToPageLink").find("span").click();
        }
    }
}

function getPageNumber() {
    var currentPage = parseInt($("#currentPageSpan").text());
    return currentPage;
}

function getPaymentsArray() {
    var totalElements = parseInt($("#totalElementsSpan").text());
    var ints = [];
    for (i = 0; i < totalElements; i++) {
        ints.push(i);
    }
    return ints;
}

$("#goToPageInput").on("input", function() {
    var totalNumberOfPages = parseInt($("#totalPagesCount").text());
    console.log("Total number of pages is", totalNumberOfPages);
    var value = this.value;
    console.log("Typed in page number is", value);
    if(value > totalNumberOfPages) {
        console.log("Resetting page numbers to max value", totalNumberOfPages);
        $(this).val(totalNumberOfPages);
    }
    if(value < 1) {
        $(this).val(1);
        console.log("Resetting page numbers to min value 1");
    }
});

$("#searchInput").on('keyup', function (e) {
    if (e.keyCode == 13) {
        // searchPayments(this.val());
        $("#searchForm").submit();
    } else {
        var searchPhrase = this.val();
        if (searchPhrase.length > 3) {
            console.log("Search phrase is", searchPhrase);
            // searchPayments(searchPhrase);
        }
    }
});

$("#searchIcon").on("click", function () {
    $("#searchForm").submit();
    // var searchPhrase = $("#searchInput").val();
    // searchPayments(searchPhrase);
});

function searchPayments(searchPhrase) {
    $.ajax({
        url: "/zpas/payments/search",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            searchPhrase: searchPhrase
        }),
        success: function (data) {
            dispaySearchResults(data);
        },
        error: function (data) {
            console.log("An error occurred.");
        }
    });
}

$("#payments-link").addClass("mm-active");

function claim_non_receipt_request(final_id) {
    var id = final_id.replace(/CNRBtn/g, '');
    console.log('Payment request id ' + id);


    $("#reqTitle").text('Claim Non Receipt Request');
    $("#paymentReqId").val(id);
    $("#paymentReqType").val("CNR");
}

$("#customerPaymentCancellationRequestModalBtn").click(function () {
    console.log('The modal show button');
    var final_id = $("#paymentIdForModal").text();
    var id = final_id.replace(/CPCRBtn/g, '');
    console.log('Payment request id ' + id);

    $("#reqTitle").text('Customer Payment Cancellation Request');
    $("#paymentReqId").val(id);
    $("#paymentReqType").val("CPCR");
    // $( "#target" ).click();
});

$("#claimNoneReceiptModalBtn").click(function () {
    console.log('The modal show button');
    var final_id = $("#paymentIdForModal").text();
    var id = final_id.replace(/CNRBtn/g, '');
    console.log('Payment request id ' + id);

    $("#reqTitle").text('Customer Non Receipt Request');
    $("#paymentReqId").val(id);
    $("#paymentReqType").val("CNR");
    // $( "#target" ).click();
});

function customer_payment_cancellation_request(final_id) {
    var id = final_id.replace(/CPCRBtn/g, '');
    console.log('Payment request id ' + id);


    $("#reqTitle").text('Customer Payment Cancellation Request');
    $("#paymentReqId").val(id);
    $("#paymentReqType").val("CPCR");
    //$('#createPaymentRequestModal').modal('show');
}

$("#initiatePaymentTriggerButton").on("click", function () {
    var paymentId = $("#paymentIdSpan").text();
    paymentReference = $("#paymentReferenceSpan").text();
    $("#confirmationMessage").html("<b>You are about to initiate payment with reference "
        + paymentReference + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("initiate");
});

$("#submitPaymentTriggerButton").on("click", function () {
    var paymentId = $("#paymentIdSpan").text();
    paymentReference = $("#paymentReferenceSpan").text();
    $("#confirmationMessage").html("<b>You are about to submit payment with reference "
        + paymentReference + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("submit");
});

$("#approvePaymentTriggerButton").on("click", function () {
    var paymentId = $("#paymentIdSpan").text();
    paymentReference = $("#paymentReferenceSpan").text();
    $("#confirmationMessage").html("<b>You are about to approve payment with reference "
        + paymentReference + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("approve");
});

$("#rejectPaymentTriggerButton").on("click", function () {
    paymentActionId = $("#paymentIdSpan").text();
    paymentReference = $("#paymentReferenceSpan").text();
    $("#confirmationMessage").html("<b style='color: red'>You are about to reject payment with reference "
        + paymentReference + " </b><br><br><b style='color: red'>Are you sure?</b>");
    $("#actionSpan").text("reject");
});

$(".reject-payment-trigger").on("click", function () {
    paymentActionId = $(this).attr("id");
    paymentReference = $(this).attr("data-reference");
    console.log("Payment reference is:", paymentReference);
    console.log("Payment to be rejected is:", paymentActionId);
    $("#confirmationMessage").html("<b style='color: red'>You are about to reject single payment with reference "
        + paymentReference + " </b><br><br><b style='color: red'>Are you sure?</b>");
    $("#actionSpan").text("reject");
});

$("#deletePaymentTriggerButton").on("click", function () {
    var paymentId = $("#paymentIdSpan").text();
    paymentReference = $("#paymentReferenceSpan").text();
    $("#confirmationMessage").html("<b style='color: red'>You are about to delete payment with reference "
        + paymentReference + " </b><br><br><b style='color: red'>Are you sure?</b>");
    $("#actionSpan").text("delete");
});

$("#confirmActionButton").on("click", function () {
    var requestedAction = $("#actionSpan").text();
    switch (requestedAction) {
        case "initiate":
            initiatePayment();
            break;
        case "submit":
            submitPayment();
            break;
        case "approve":
            approvePayment();
            break;
        case "reject":
            rejectPayment();
            break;
        case "delete":
            deletePayment();
            break;
    }
});

function rejectPayment() {
    var paymentId = paymentActionId;
    var rejectPaymentLink = "/zpas/payments/reject/" + paymentId;
    var actionLink = $("#paymentActionLink");
    actionLink.prop("href", rejectPaymentLink);
    actionLink.find("span").click();
}

function deletePayment() {
    var paymentId = $("#paymentIdSpan").text();
    var deletePaymentLink = "/zpas/payments/delete/" + paymentId;
    var actionLink = $("#paymentActionLink");
    actionLink.prop("href", deletePaymentLink);
    actionLink.find("span").click();
}

function approvePayment() {
    var paymentId = $("#paymentIdSpan").text();
    var approvePaymentLink = "/zpas/payments/approve/" + paymentId;
    var actionLink = $("#paymentActionLink");
    actionLink.prop("href", approvePaymentLink);
    actionLink.find("span").click();
}

function submitPayment() {
    var paymentId = $("#paymentIdSpan").text();
    var submitPaymentLink = "/zpas/payments/submit/" + paymentId;
    var actionLink = $("#paymentActionLink");
    actionLink.prop("href", submitPaymentLink);
    actionLink.find("span").click();
}

function initiatePayment() {
    var paymentId = $("#paymentIdSpan").text();
    var submitPaymentLink = "/zpas/payments/initiate/" + paymentId;
    var actionLink = $("#paymentActionLink");
    actionLink.prop("href", submitPaymentLink);
    actionLink.find("span").click();
}

$("#sendProofOfPaymentButton").click(function () {
    console.log("Modal closed.");
    sendProofOfPayment();
});

function sendProofOfPayment() {
    var proofOfPayment = {
        paymentId: $("#paymentId").val(),
        recipientName: $("#recipientName").val(),
        email: $("#email").val()
    };

    $.ajax({
        url: "/zpas/messaging/proof-of-payment",
        method: "post",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(proofOfPayment),
        success: function (proofOfPayment) {
            if (proofOfPayment.status == 'success') {
                swal({
                    title: "Proof Of Payment Sent!",
                    text: proofOfPayment.message,
                    imageUrl: '/zpas/images/thumbs-up.jpg'
                });
            } else {
                swal({
                    title: "Proof Of Payment sending failed!",
                    text: proofOfPayment.message,
                    imageUrl: '/zpas/images/error.png'
                });
            }
        },
        error: function (data) {
            swal({
                title: "Proof Of Payment sending failed!",
                text: "An error occured.",
                imageUrl: '/zpas/images/error.png'
            });
        }
    }).done(function () {
    });
}

function get_signatures(paymentId) {
    console.log('Get the signature ... with id ' + paymentId);
    $.ajax({
        url: "/zpas/cash-account-mandate/get/mandate/signatures/" + paymentId,
        dataType: "json",
        contentType: "application/json",
        method: "get",
        success: function (persons) {
            console.log("Number of signature infos:", persons.length);
            html_1 = '';
            html_2 = '';
            var i = 1;
            var j = 1;
            $.each(persons, function (index, person) {
                console.log("Person Info:", JSON.stringify(person));
                if (person.title == 'Signed') {
                    html_1 += "<tr>" +
                        "<td class='text-center text-muted'>" + i + "</td>" +
                        "<td><div class='widget-heading'>" + person.name + "</div></td>" +
                        "<td class='text-center'> <div class='badge badge-pill pl-2 pr-2 badge-success'>" + person.title.toUpperCase() + "</div></td>" +
                        "<td class='text-center'>" + person.date + "</td></tr>";
                    i = i + 1;
                } else {
                    html_2 += "<tr>" +
                        "<td class='text-center text-muted'>" + j + "</td>" +
                        "<td><div class='widget-heading'>" + person.name + "</div></td>" +
                        "<td class='text-center'> <div class='badge badge-pill pl-2 pr-2 badge-warning'>" + person.title.toUpperCase() + "</div></td></tr>";
                    j = j + 1;
                }

            });
            $("#signaturesSignedTable tbody > tr").remove();
            $("#signaturesSignedTable tbody").append(html_1);

            $("#signaturesPendingTable tbody > tr").remove();
            $("#signaturesPendingTable tbody").append(html_2);
        },
        error: function (data) {
            console.log("An error occurred. Message was ", JSON.stringify(data));
        }
    }).done(function () {
        console.log("Done filling in signature information");
    });
}

$("#signedBtn").click(function () {
    $(".signed-table").show();
    $(".pending-table").hide();
    $(this).addClass("active");
    $("#pendingBtn").removeClass("active");
});

$("#pendingBtn").click(function () {
    $(".signed-table").hide();
    $(".pending-table").show();
    $(this).addClass("active");
    $("#signedBtn").removeClass("active");
});


$("#signaturesButton").click(function (e) {
    var id = $("#paymentIdSpan").text();
    console.log('The payment id is ' + id);
    get_signatures(id);
});