/**
 Created by alfred on 15 Apr 2019
 */
/* chart.js chart examples */

// chart colors
var colors = ['#007bff','#ff0000','#00ff00','#c3e6cb','#dc3545','#6c757d'];

$(document).ready(function() {
    $.ajax({
        url: "/zpas/payments/status/dashboard/fetch",
        method: "get",
        dataType: "json",
        contentType: "application/json",
        success: function(data) {
            console.log("Fetched data successfully.", JSON.stringify(data));
            loadDonuts(data);
        },
        error: function (data) {
            console.log("An error occurred.", JSON.stringify(data));
        }
    });
});

/* 3 donut charts */
function loadDonuts(data) {


// donut 1
    var chDonutData1 = {
        labels: ['Total', 'Failed', 'Successful'],
        datasets: [
            {
                backgroundColor: colors.slice(0, 3),
                borderWidth: 0,
                data: [data.totalPayments, data.totalFailedPayments, data.totalSuccessfulPayments]
            }
        ]
    };

    var chDonut1 = document.getElementById("chDonut1");
    if (chDonut1) {
        new Chart(chDonut1, {
            type: 'pie',
            data: chDonutData1,
            options: {
                cutoutPercentage: 85,
                legend: {position: 'bottom', padding: 5, labels: {pointStyle: 'circle', usePointStyle: true}},
                title: {
                    display: true,
                    text: 'All Payments'
                }
            }
        });
    }

// donut 2
    var chDonutData2 = {
        labels: ['Total', 'Failed', 'Successful'],
        datasets: [
            {
                backgroundColor: colors.slice(0, 3),
                borderWidth: 0,
                data: [data.totalBulkPayments, data.totalFailedBulkPayments, data.totalSuccessfulBulkPayments]
            }
        ]
    };
    var chDonut2 = document.getElementById("chDonut2");
    if (chDonut2) {
        new Chart(chDonut2, {
            type: 'pie',
            data: chDonutData2,
            options: {
                cutoutPercentage: 85,
                legend: {position: 'bottom', padding: 5, labels: {pointStyle: 'circle', usePointStyle: true}},
                title: {
                    display: true,
                    text: 'Bulk Payments'
                }
            }
        });
    }

// donut 3
    var chDonutData3 = {
        labels: ['Total', 'Failed', 'Successful'],
        datasets: [
            {
                backgroundColor: colors.slice(0, 3),
                borderWidth: 0,
                data: [data.totalIndividualPayments, data.totalFailedIndividualPayments, data.totalSuccessfulIndividualPayments]
            }
        ]
    };
    var chDonut3 = document.getElementById("chDonut3");
    if (chDonut3) {
        new Chart(chDonut3, {
            type: 'pie',
            data: chDonutData3,
            options: {
                cutoutPercentage: 85,
                legend: {position: 'bottom', padding: 5, labels: {pointStyle: 'circle', usePointStyle: true}},
                title: {
                    display: true,
                    text: 'Individual Payments'
                }
            }
        });
    }
}