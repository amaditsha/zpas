/**
 Created by alfred on 26 Apr 2019
 */

var accountDetailsMap = [];

$("#amount").on("keyup", function () {
    $("#amountConfirmation").text(this.value);
});

$("#debtorName").on("keyup", function () {
    $("#debtorNameConfirmation").text(this.value);
});

$("#approvePaymentActivationRequestBtn").on("click", function() {
    console.log("About to approve payment obligation.");
    var paymentObligationId = $("#paymentObligationIdSpan").text();
    $("#confirmationMessage").html("<b>You are about to approve Payment Request with reference "
        + paymentObligationId + " </b><br><br><b>Are you sure?</b>");
    $("#actionSpan").text("approve");
});

$("#rejectPaymentActivationRequestBtn").on("click", function() {
    console.log("About to reject payment obligation.");
    var paymentObligationId = $("#paymentObligationIdSpan").text();
    $("#confirmationMessage").html("<b style='color: red'>You are about to reject Payment Request with reference "
        + paymentObligationId + " </b><br><br><b style='color: red'>Are you sure?</b>");
    $("#actionSpan").text("reject");
});



$("#confirmActionButton").on("click", function() {
    var requestedAction = $("#actionSpan").text();
    switch (requestedAction) {
        case "approve":
            approvePaymentRequest();
            break;
        case "reject":
            rejectPaymentRequest();
            break;
    }
});

function approvePaymentRequest() {
    console.log("Approve payment request.");
    var paymentObligationId = $("#paymentObligationIdSpan").text();
    var approvePaymentRequestLink = "/zpas/payments/requests/approve/" + paymentObligationId;
    var actionLink = $("#paymentRequestActionLink");
    actionLink.prop("href", approvePaymentRequestLink);
    actionLink.find("span").click();
}

function rejectPaymentRequest() {
    console.log("Reject payment request.");
    var paymentObligationId = $("#paymentObligationIdSpan").text();
    var rejectPaymentRequestLink = "/zpas/payments/requests/reject/" + paymentObligationId;
    var actionLink = $("#paymentRequestActionLink");
    actionLink.prop("href", rejectPaymentRequestLink);
    actionLink.find("span").click();
}



$("#virtualIdCapture").on("change", function () {
    console.log("Change occurred on virtual ID capture");
    var accountNumber = $("#virtualIdCapture").val();
    console.log("Account number is " + accountNumber);
    var details = getDetailsForAccountNumber(accountNumber);
    $("#sourceAccountNumber").val(accountNumber);
    $("#debtorName").val(details.debtorName);
    $("#debtorNameConfirmation").text(details.debtorName);
    $("#debtorId").val(details.accountOwnerId);
    $("#sourceFinancialInstitutionId").val(details.fiId);
});

function getDetailsForAccountNumber(accountNumber) {
    for(i = 0; i < accountDetailsMap.length; i++) {
        if(accountDetailsMap[i].accountNumber == accountNumber) {
            console.log("FI ID. " + accountDetailsMap[i].fiId);
            return accountDetailsMap[i];
        }
    }
    console.log("ID not found.");
    return {};
}

$("#virtualIdCapture").select2({
    minimumInputLength: 1,
    placeholder: "Search for an account",
    allowClear: true,
    ajax: {
        url: "/zpas/api/json/accounts/search",
        dataType: "json",
        contentType: "application/json",
        delay: 250,
        data: function (params) {
            var queryParameters = {
                search: params.term
            }

            return queryParameters;
        },
        processResults: function (data) {
            console.log("Received data is:", JSON.stringify(data));
            var formattedData = [];
            $.each(data, function (idx, dataItem) {
                var d = {
                    id: dataItem.accountNumber,
                    text: dataItem.accountNumber + "(" + dataItem.accountName + ")"
                };
                formattedData.push(d);
                var accFi = {
                    accountNumber: dataItem.accountNumber,
                    fiId: dataItem.financialInstitutionId,
                    debtorName: dataItem.accountOwnerName,
                    accountOwnerId: dataItem.accountOwnerId
                };
                accountDetailsMap.push(accFi);
            });
            console.log("Formatted data is", formattedData);
            return {
                results: formattedData
            };
        }
    }
});

$("#priority").on("change", function () {
    console.log("priority changed. Value is: ", this.value);
    if (this.value == 'High') {
        $("#priorityMessage").show();
    } else {
        $("#priorityMessage").hide();
    }
});