$(document).ready(function () {
    $("#numberOfUnreadNotifications").html(0);
    $('.notification-pico').removeClass('icon-anim-pulse');
    $.ajax({
        url: "/zpas/notification/unread",
        method: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            if (data.responseCode === "00") {
                console.log("The size of the notification is " + data.object.length);
                $("#numberOfUnreadNotifications").html(data.object.length);
                $('#notificationAlertsDiv div').empty();

                $('.notification-pico').addClass('icon-anim-pulse');
                $.each(data.object, function (i, notification) {

                    $("#notificationAlertsDiv").append("<div style='cursor: pointer;' onclick='mark_notification_as_read(this.id)'  id='" + notification.id + "' class='vertical-timeline-item dot-success vertical-timeline-element'> " +
                        "<div>" +
                        "<span class='vertical-timeline-element-icon bounce-in'></span>" +
                        "<div class='vertical-timeline-element-content bounce-in'>" +
                        "<p>" + notification.message + "<br><span class='text-success'>" + formatDateTime(notification.dateCreated) + "</span> " +
                        "</p>" +
                        "<span class='vertical-timeline-element-date'></span>" +
                        "</div>" +
                        "</div>");
                });
            } else {
                $("#notificationAlertsDiv").html("<strong>Error</strong>");
            }
        },
        error: function (data) {
            console.log("An error occurred while fetching the notifications ");
        }
    });
});

function getNowTime() {
    var d = new Date();
    var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
    return time;
}

function formatDateTime(offsetDate) {
    return offsetDate.split(".")[0].replace("T"," ");
}

function mark_notification_as_read(id) {
    var final_id = id.replace(/anchor/g, '');

    console.log("The notification is " + id);

    $('#' + id).addClass('dot-dark').removeClass('dot-success');
    $('.notification-pico').removeClass('icon-anim-pulse');

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/zpas/notification/read/" + id,
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            if (data.responseCode === '00') {
                // $('#anchor' + final_id).text('Read');
                console.log("The update read message was successful.")
            }
        },
        error: function (e) {
        }
    });
}

function connect() {

    // Create and init the SockJS object
    var socket = new SockJS('/ws');
    var stompClient = Stomp.over(socket);

    // Subscribe the '/notify' channell
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/user/queue/notify', function (notification) {

            // Call the notify function when receive a notification
            notify(notification);

        });
    });

    return;
} // function connect

/**
 * Display the notification message.
 */
function notify(payload) {
    var notification = JSON.parse(payload.body);
    console.log("New notification | " + notification);
    $('.notification-pico').addClass('icon-anim-pulse');
    // $('#notificationAlertsDiv div').empty();
    // $.each(data.object, function (i, notification) {
    $("#notificationAlertsDiv").append("<div style='cursor: pointer;' onclick='mark_notification_as_read(this.id)'  id='" + notification.id + "' class='vertical-timeline-item dot-success vertical-timeline-element'> " +
        "<div>" +
        "<span class='vertical-timeline-element-icon bounce-in'></span>" +
        "<div class='vertical-timeline-element-content bounce-in'>" +
        "<p>" + notification.message + "<br><span class='text-success'>" + formatDateTime(notification.dateCreated) + "</span> " +
        "</p>" +
        "<span class='vertical-timeline-element-date'></span>" +
        "</div>" +
        "</div>");
    // });
    return;
}

/**
 * Init operations.
 */
$(document).ready(function () {

    // Start the web socket connection.
    connect();

});